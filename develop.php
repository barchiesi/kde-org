<?php
	require('aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "KDE Developers Area",
		'cssFile' => 'content/develop/portal.css',
		'section' => 'develop',
        'description' => 'KDE provides libraries for other KDE projects and third-parties. Learn how to use them here.',
	]);

	require('aether/header.php');

?>
<style>
.bottomAlignRow section {
	position: relative;
	padding-bottom: 50px;
	
}
.bottomAlignRow section>a {
	position: absolute;
	bottom: 0px;
	left: 15px;
	right: 15px;
}
</style>
<div id="develop-hero" style="padding: 20px; height: auto;">


    <div class="container" style="background-color: #333; padding: 40px; text-align: center;">
        <h1 style="color: #EEE; font-size: 22px; font-weight: bold;">Develop Convergent Apps with Kirigami</h1>
        <p style="color: #CCC; padding: 10px; max-width: 500px; margin: 0px auto;">Kirigami is a new, convergent, responsive, elegant, and open cross-platform toolkit using QML.</p>
        <a href="https://www.kde.org/products/kirigami/">Learn more</a>
    </div>

</div>

	<main class="container" id="community-lists">
		<!-- div class="row">
			<section class="col-md">
				<h3>Developer Archives</h3>
				
				<p>There is a large amount of KDE documentation for developers,
				while migrating the website it may be easier for people familliar with
				the KDE website to visit our older pages during this transition.</p>
				
				<a href="https://www.kde.org/developerplatform/" class="learn-more">View older Developer Portal</a>
			</section>
		</div -->
		
		<div class="row bottomAlignRow" style="border-bottom: solid 1px #EEE; padding-bottom: 20px;">
			<section class="col-md">
				<h3>TechBase Wiki</h3>
				<p>The KDE TechBase is primarily aimed at external developers. 
				You can find documentation to help to build or extend 
				your own projects with KDE software.
				</p>
				<a href="https://techbase.kde.org/" class="learn-more button expand">Visit TechBase</a>
			</section>
			
			<section class="col-md">
				<h3>API Documentation</h3>
				<p>
					Documentation for various KDE frameworks and tools for developers,
					great for both external 3rd-party developers and KDE contributors.
				</p>
				<a href="https://api.kde.org/" class="learn-more button expand">Visit API Docs</a>
			</section>
			
			<section class="col-md">
				<h3>Inqlude Documentation</h3>
				<p>
					KDE isn't the only community developing with Qt, Inqlude is
					an archive containing many Qt-based libraries, not just those
					by the KDE community.
				</p>
				<a href="https://inqlude.org/" class="learn-more button expand" target="_blank">Visit Inqlude</a>
			</section>
			
			<section class="col-md">
				<h3>Qt Documentation</h3>
				<p>
					KDE technology heavily depends on and contributes to Qt. If you
					are not experienced in Qt you should first examine Qt documentaiton.
				</p>
				<a href="https://www.qt.io" class="learn-more button expand" target="_blank">View Qt Documentation</a>
			</section>
		</div>
		
		<h2 style="margin-bottom: -20px;">Contributors Zone</h2>
		
		<div class="row bottomAlignRow" style="border-bottom: solid 1px #EEE; padding-bottom: 20px;">

			<section class="col-sm">
				<h3>Plasma and Theme Development</h3>
				<p>
					Plasma is made to be flexible; learn how to develop widgets, themes and other enhancements to your workspace.
				</p>
				<a href="https://techbase.kde.org/Special:MyLanguage/Development/Tutorials/Plasma5" class="learn-more button">View Documentation</a>
			</section>
			
			<section class="col-sm">
				<h3>Contributor Accounts</h3>
				<p>
					How to get an account to contribute to the KDE.
				</p>
				<a href="https://community.kde.org/Infrastructure/Get_a_Developer_Account" class="learn-more button">Information on Contributor Accounts</a>
			</section>
			
			<section class="col-sm">
				<h3>Invent</h3>
				<p>
					KDE Invent is the new way to review, manage, and work with
					KDE projects.
				</p>
				<a href="https://invent.kde.org/" class="learn-more button">Visit KDE Invent</a>
			</section>
			
		</div>
	</main>
<?php


	require('aether/footer.php');

?>
