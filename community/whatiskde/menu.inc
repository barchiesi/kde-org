<?php

$this->appendLink("Software Compilation","softwarecompilation.php");
$this->appendLink("Project Management","management.php");
$this->appendLink("Development Model","devmodel.php");
$this->appendLink("Internationalization","i18n.php");
$this->appendLink("KDE e.V. Foundation","http://ev.kde.org", false);
$this->appendLink("Free Qt Foundation","kdefreeqtfoundation.php");
$this->appendDir("History", "../history");
$this->appendDir("Awards", "../awards");
$this->appendDir("Press Contact", "../../contact");
$this->appendDir("Press Information", "../../presspage");

?>
