<?php
// default settings
$pageConfig = [
    'title' => "The KDE Website",
    'section' => 'home',
    'description' => 'KDE is an open community of friendly people who want to create a world in which everyone has control over their digital life and enjoys freedom and privacy.', // description should be between 50 and 160 characters
    'image' => 'https://kde.org/stuff/clipart/logo/kde-logo-white-blue-rounded-128x128.png',
    'cdnCSSFiles' => [],
    'cdnJSFiles' => [],
];
