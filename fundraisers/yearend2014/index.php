<?php
    // Don't show the global donation square, it's confusing in this page
    $page_disablekdeevdonatebutton = true;

    include_once ("functions.inc");

    $translation_file = "kde-org";

    $page_title = i18n_noop("Make the World a Better Place! - KDE End of Year 2014 Fundraising");

    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => i18n_noop("Make the World a Better Place! - KDE End of Year 2014 Fundraising"),
        'cssFile' => '/css/announce.css'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";

    require("www_config.php");

    $countStmt = $dbConnection->prepare("SELECT COUNT(*) FROM yearend2014donations ORDER BY CREATED_AT DESC;");
    $countStmt->execute();
    $n = $countStmt->fetchColumn();

    $res = $dbConnection->query("SELECT *, UNIX_TIMESTAMP(CREATED_AT) AS date_t FROM yearend2014donations ORDER BY CREATED_AT DESC;");
    $total = 0;
    $table = "<table border='1' width='100%'>";
    $table.="<tr><th width='20'></th><th width='180'>".i18n_var("Date")."</th><th width='80'>".i18n_var("Amount")."</th><th style='text-align:left'>".i18n_var("Donor Name")."</th></tr>";
    while ($row = $res->fetch()) {
        $msg = htmlspecialchars($row["message"]);
        if ($msg == "") {
            $msg = "<i>".i18n_var("Anonymous donation")."</i>";
        }
        $amount = $row["amount"];
        $table.="<tr>";
        $table.="<td align='center'>".$index."</td>";
        $table.="<td align='center'>".date("jS F Y", $row["date_t"])."</td>";
        if ($row["currency"] == "EUR") {
            $table.="<td align='right'>&euro;&nbsp;".number_format($amount,2)."</td>";
            $total += $amount;
        } else {
            $table.="<td align='right'>$&nbsp;".number_format($amount,2)."</td>";
            $total += $amount * 0.78;
        }
        $table.="<td>".$msg."</td>";
        $table.="</tr>";
        $index--;
    }
    $table.="</table><br/>";
    $total = number_format($total, 0, ".", "&#8201;");
?>

<?php if (isset($_GET["origin"]) && $_GET["origin"] == "fb") { ?>
<script type="text/javascript">(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '783578604989266']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=783578604989266&amp;ev=PixelInitialized" /></noscript>

<script type="text/javascript">
    function fbDonate() {
        window._fbq.push(['track', '6019624944423', {'value':'1','currency':'USD'}]);
    }
</script>
<?php } ?>


<script type="text/javascript">
        function changeCurrency(elem) {
//             if (elem.selectedIndex == 0) {
//                 document.getElementById('card').innerHTML = "€&#8202;30";
//                 document.getElementById('extracard').innerHTML = "€&#8202;10";
//                 document.getElementById('generous').innerHTML = "€&#8202;1000";
//             } else {
//                 document.getElementById('card').innerHTML = "$&#8202;40";
//                 document.getElementById('extracard').innerHTML = "$&#8202;15";
//                 document.getElementById('generous').innerHTML = "$&#8202;1250";
//             }
        }
        function changeItemNumber(elem) {
//             if (elem.checked) {
//                 document.getElementById('item_number').value = "Donation to End of Year 2014 Campaign by KDE e.V.";
//             } else {
//                 document.getElementById('item_number').value = "Anonymous donation to End of Year 2014 Campaign by KDE e.V.";
//             }
        }
        $(document).ready(function() {
            changeCurrency(document.getElementById("currency_code"));
            changeItemNumber(document.getElementById("showdonorlist"));
        });
</script>

<main class="container">

<h1>Make the World a Better Place! - KDE End of Year 2014 Fundraising</h1>

<!--<div class="formholder" style='float:right;'>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" style='padding-left:1em; padding-right:1em;'>
<input type="hidden" name="business" value="kde-ev-paypal@kde.org" />-->

<?php
    echo "<p style='font-size:xx-large' align='center'>".i18n_var("%1 raised", "€&#8202;$total")."</p>";
?>
<!-- <input type="hidden" name="cmd" value="_donations" />
 <input type="hidden" name="lc" value="GB" />
 <input type="hidden" name="item_name" value="KDE e.V." />
 <input type="hidden" id="item_number" name="item_number" value="Donation to End of Year 2014 Campaign by KDE e.V." />
 <input type="hidden" name="cbt" value="Return to www.kde.org" />
 <input type="hidden" name="no_note " value="1" />
 <input type="hidden" name="return" value="https://www.kde.org/fundraisers/yearend2014/thanks_paypal.php" />
 <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/yearend2014/notify.php" />
 <br/>
 <div align="center">
 <select id="currency_code" name="currency_code" onChange="changeCurrency(this);">
    <option value="EUR">€</option>
    <option value="USD">$</option>
 </select>
 <input type='text' name="amount" value='40' style="vertical-align:middle;text-align:right;" size="4" />
 <?php if (isset($_GET["origin"]) && $_GET["origin"] == "fb") { ?>
    <button style='cursor: pointer; background-color: #0070BB; border: 1px solid #0060AB; color: #FFF; height: 27px;' type='submit' onclick="fbDonate()"><?php i18n("Donate"); ?></button>
    <div style="font-size:x-small"><?php i18n("We will notify Facebook of your<br>donation for campaign analytics."); ?></div>
 <?php } else { ?>
    <button style='cursor: pointer; background-color: #0070BB; border: 1px solid #0060AB; color: #FFF; height: 27px;' type='submit'><?php i18n("Donate"); ?></button>
    <br/>
    <br/>
 <?php } ?>
 <input id="showdonorlist" type="checkbox" checked="checked" onclick="changeItemNumber(this)" /><?php print i18n_var("Show my name on the <a href='%1'>donor list</a>", "#donorlist");?>
 </div>
</form>
</div>-->

<p>
<b><?php print i18n_var("The KDE End of Year 2014 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. Go to the <a href='%1'>KDE donation page</a> if you want to support us further.</b>", "https://www.kde.org/community/donations/index.php"); ?></b></p>

<p style="font-size:x-small">
<?php i18n("Also available in: "); ?> <a href="?site_locale=en">English</a> | <a href="?site_locale=ca">Català</a> | <a href="?site_locale=es">Español</a> | <a href="?site_locale=nl">Nederlands</a> | <a href="?site_locale=pl">Polski</a> | <a href="?site_locale=pt">Português</a> | <a href="?site_locale=pt_BR">Português brasileiro</a> | <a href="?site_locale=sv">Svenska</a> | <a href="?site_locale=uk">Українська</a>
</p>

<p>
<?php i18n("As we approach the end of the year we begin the season of giving. What would suit the holiday better than giving to the entire world?"); ?>
</p>

<p>
<?php i18n("Here is a unique way to give back to KDE allowing us to keep giving free software to humankind."); ?>
</p>

<p>
<?php i18n("KDE is committed to improving technology and software to make the world a better place. We produce great quality free software that everyone is free to use or modify without any cost or restriction."); ?>
</p>

<script type="text/javascript">
    (function($) {
        var cache = [];
        // Arguments are image paths relative to the current page.
        $.preLoadImages = function() {
            var args_len = arguments.length;
            for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
            }
            }
    })(jQuery)

    $(document).ready(function() {
        jQuery.preLoadImages("fundraiser-calligrasheets.jpg", "fundraiser-gwenview.jpg", "fundraiser-kalgebra.jpg", "fundraiser-kate.jpg", "fundraiser-klettres.jpg", "fundraiser-okular.jpg");
        $('.images').cycle({
            fx: 'fade',
            timeout: 6000,
            speed: 500,
            next : $('#next'),
            prev : $('#previous')
        });

        var show = function(e) {
            var teaserPosition = $('.images').offset();
            var next = $('#next');
            var prev = $('#previous');
            var left = (teaserPosition.left + $('.images').width()) / 2;
            next.css({top:teaserPosition.top, left:left}).show();
            prev.css({top:teaserPosition.top+$('.images').height()-prev.height(), left:left}).show();
        };

        var hide = function(time) {
            $('#next').hide();
            $('#previous').hide();
        };

        var pause = function() {
            $('.images').cycle('pause');
        }

        var resume = function() {
            $('.images').cycle('resume');
        }

        $('.images').mouseover(show);
        $('.images').mouseout(hide);
        $('.images').mouseover(pause);
        $('.images').mouseout(resume);
        $('#next').mouseover(show);
        $('#previous').mouseover(show);
    });
</script>
<p>
<div class="images">
    <img width="750" height="500" src="fundraiser-klettres.jpg" alt="" />
    <img width="750" style="display: none;" src="fundraiser-kalgebra.jpg" alt="" />
    <img width="750" style="display: none;" src="fundraiser-gwenview.jpg" alt="" />
    <img width="750" style="display: none;" src="fundraiser-kate.jpg" alt="" />
    <img width="750" style="display: none;" src="fundraiser-okular.jpg" alt="" />
    <img width="750" style="display: none;" src="fundraiser-calligrasheets.jpg" alt="" />
</div>
</p>

<p>
<?php i18n("We want to bring the solutions we are offering to the next step. By participating in this fundraiser, you'll be part of the improvements we'll put into our educational software, so kids can have better tools for school; our office suite, so we have the best tools for the workplace; and our desktop so we can all experience a fun and productive experience when interacting with our computers."); ?>
</p>

<p><b><em>
<?php i18n("Donating to KDE is not for you, it is for the entire world."); ?>
</em></b></p>

<p>
<?php print i18n_var("As a way to say thank you, starting with %1 we will send a KDE themed postcard to any given address. You will get an extra card for every additional %2 donation. Get cards for yourself and for your family and friends to show them you care for freedom. It's the perfect way to spread the festive cheer and donate to your favorite project at the same time.", "<em id='card'>€&#8202;30</em>", "<em id='extracard'>€&#8202;10</em>"); ?>
</p>

<p>
<a href="postcard01.png"><img height="200" src="postcard01_small.jpg" alt="Postcard Design #1" /></a>
<a href="postcard02.png"><img width="200" src="postcard02_small.jpg" alt="Postcard Design #2" /></a>
<a href="postcard03.png"><img height="200" src="postcard03_small.jpg" alt="Postcard Design #3" /></a>
</p>

<p>
<?php print i18n_var("For those of you that are very generous and donate more than %1 we want to thank you by being the guides to our cities. We would like to spend an evening showing you the more interesting spots in the cities we live in, have a chat about KDE, life and everything. At the moment this offer extends to the following cities:", "<em id='generous'>€&#8202;1000</em>"); ?>
</p>
<ul>
    <li>Barcelona, Spain</li>
    <li>Burgdorf, Switzerland</li>
    <li>Nijmegen, The Netherlands</li>
    <li>Nuremberg, Germany</li>
    <li>Pune, India</li>
    <li>Toulouse, France</li>
    <li>Washington, DC, USA</li>
</ul>

<p>
<?php i18n("This campaign will end on January 15th 2015."); ?>
</p>

<h3><?php i18n("Where Your Donations Go"); ?></h3>
<p>
<?php print i18n_var("Last year KDE spent about %1 on travel and accomodation of more than 100 contributors for various sprints throughout the year.", "€&#8202;30&thinsp;000"); ?>
</p>

<p>
<?php print i18n_var("<a href='%1'>Sprints</a> are in person meetings and are really important for a team of hardworking volunteers around the world to focus their efforts and discuss technical matters around the project. The amount of output we get from the sprints is really worthwhile for all users of the software.", "https://community.kde.org/Sprints"); ?>
</p>

<p>
<?php print i18n_var("Remaining money gets spent on our infrastructure, we have a large portfolio of servers hosting websites, code, continuous integration and a lot more. A full breakdown can be seen in our <a href='%1'>quarterly reports</a>.", "http://ev.kde.org/reports/"); ?>
</p>

<h4><?php i18n("Notes:"); ?></h4>
<ul>
    <li><?php i18n("We will use your paypal email address to contact you and ask for the addresses to send the postcards mid-November."); ?></li>
    <li><?php i18n("You will be able to choose between the generic thank you or custom text for the postcard."); ?></li>
    <li><?php i18n("The first shipment of cards will be December 1st, after that there will be a shipment every week until the end of the Fundraiser."); ?></li>
    <li><?php i18n("The city guide evening will have to be coordinated with the local people, so we can find a date that suits all of us."); ?></li>
</ul>

<p>
<?php print i18n_var("If you prefer to use international bank transfers please <a href='%1'>see this page</a>.", "/community/donations/#moneytransfer"); ?>
<br />
<?php print i18n_var("Please write us <a href='%1'>an email</a> so we can add you to the list of donors manually.", "mailto:kde-ev-campaign@kde.org"); ?>
</p>

<h3><a name="donorlist"></a><?php i18n("List of donations"); ?></h3>
<?php echo $table; ?>

</main>
<?php
  require('../../aether/footer.php');
