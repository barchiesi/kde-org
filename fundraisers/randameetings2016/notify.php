<?php

$do_debug = true;
die("Donation to the Randa Meeting 2016 are not supported anymore. See <a href="/donations">Donnations</a>");

// STEP 1: read POST data
 
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}

// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
    if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}

if ($do_debug) {
    $debug = fopen('/tmp/randa2016.txt', 'a+');
    fwrite($debug, "verify: $req\n");
}


$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
// the directory path of the certificate as shown below:
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if( !($res = curl_exec($ch)) ) {
    if ($do_debug) {
        fwrite($debug, "Error in calling curl_exec\n");
    }
    curl_close($ch);
    exit;
}
curl_close($ch);

if (strcmp ($res, "VERIFIED") == 0) {
    // assign posted variables to local variables
    $item_name = $_POST['item_name'];
    $item_number = $_POST['item_number'];
    $payment_status = $_POST['payment_status'];
    $payment_amount = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
    $txn_id = $_POST['txn_id'];
    $receiver_email = $_POST['receiver_email'];
    $payer_email = $_POST['payer_email'];
    if ($_POST['option_selection1'] === "Yes") {
        $memo = $_POST['address_name'];
    } else {
        $memo = "";
    }
    
    require("www_config.php");
    
    // check the payment_status is Completed
    if ( $payment_status != "Completed") {
        if ($do_debug) {
            fwrite($debug, "Unexpected payment status: ".$payment_status."\n");
        }
        die("Payment status is ".$payment_status);
    } 
    
    // check that receiver_email is your Primary PayPal email
    if ( $receiver_email != "kde-ev-board@kde.org") {
        if ($do_debug) {
            fwrite($debug, "Unexpected receiver email: ".$receiver_email."\n");
        }
        die("Unknown email");
    }

    // check that payment_amount/payment_currency are correct
    if ( $payment_currency != "EUR" ) {
        if ($do_debug) {
            fwrite($debug, "Unexpected payment currency: ".$payment_currency."\n");
        }
        die("Unknown currency used");
    }

    // sanitise date
    $date = strtotime( $_POST["payment_date"] );
    if ( $date === false ) {
        echo "Date parsing failed, assuming now()";
        $date = time();
    }
    $date = date("Y-m-d H:i:s", $date);

    // process payment
    $query = "REPLACE into randameetings2016donations VALUES( '', \"".$date."\"";
    $query .= ", ".$payment_amount.", \"".addslashes( $memo )."\"";
    $query .= ",\"".$txn_id."\" )";
    mysql_query($query, $sq);
    
    if ($do_debug) {
        fwrite( $debug, "\nQuery:".$query."\n" );
        fwrite( $debug, "Error:".mysql_error()."\n" );
    }


} else if (strcmp ($res, "INVALID") == 0) {
    if ($do_debug) {
        fwrite($debug, "Invalid transacion\n");
    }
} else {
    if ($do_debug) {
        fwrite($debug, "Paypal returned neither VERIFIED nor INVALID\n");
        fwrite($debug, $res."\n");
    }
}

if ($do_debug) {
    fwrite( $debug, "\n---------------------------------------\n" );
    fclose($debug);
}
?>
