
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/attica-5.65.0.tar.xz">attica-5.65.0</a></td>
   <td align="right">62kB</td>
   <td><tt>ec877595082d975520df2e74dfd809a6fadf8b6e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/baloo-5.65.0.tar.xz">baloo-5.65.0</a></td>
   <td align="right">271kB</td>
   <td><tt>64d05dd7f34e61c5537c39a82181d0dc2e9b57de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/bluez-qt-5.65.0.tar.xz">bluez-qt-5.65.0</a></td>
   <td align="right">96kB</td>
   <td><tt>ef83c1ca19a61b729aeabe6b02ebc0fd25418f5b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/breeze-icons-5.65.0.tar.xz">breeze-icons-5.65.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>4893ba85cce5877d39a0eabc90016c8c609a320c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/extra-cmake-modules-5.65.0.tar.xz">extra-cmake-modules-5.65.0</a></td>
   <td align="right">332kB</td>
   <td><tt>4f0b41a5e28ac420042f224d7aa04523e1c9f1ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/frameworkintegration-5.65.0.tar.xz">frameworkintegration-5.65.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>556ccbe79a59b5a62b9710ca0ea01743d7134667</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kactivities-5.65.0.tar.xz">kactivities-5.65.0</a></td>
   <td align="right">60kB</td>
   <td><tt>e44de700139267aa315fe055a9ac62b057b2ea03</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kactivities-stats-5.65.0.tar.xz">kactivities-stats-5.65.0</a></td>
   <td align="right">61kB</td>
   <td><tt>24308c760df3dda09a7836d23d8eb6452af9e7f8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kapidox-5.65.0.tar.xz">kapidox-5.65.0</a></td>
   <td align="right">387kB</td>
   <td><tt>acec9ab548af71a9178548b1e01a8ba4d5a56c64</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/karchive-5.65.0.tar.xz">karchive-5.65.0</a></td>
   <td align="right">448kB</td>
   <td><tt>cdcf8c96374d11324e16312051e793ccda937963</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kauth-5.65.0.tar.xz">kauth-5.65.0</a></td>
   <td align="right">84kB</td>
   <td><tt>f1f46c29823db1d1db540eeba863e70cb45c7448</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kbookmarks-5.65.0.tar.xz">kbookmarks-5.65.0</a></td>
   <td align="right">118kB</td>
   <td><tt>02dbe60f7da18d7e3dad862fa875db9c6205c434</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcalendarcore-5.65.0.tar.xz">kcalendarcore-5.65.0</a></td>
   <td align="right">244kB</td>
   <td><tt>f0685ebecc0bf4b6b636deef81807eed600ed616</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcmutils-5.65.0.tar.xz">kcmutils-5.65.0</a></td>
   <td align="right">233kB</td>
   <td><tt>acb479c086b397558f072f7b35a4d5220931c24c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcodecs-5.65.0.tar.xz">kcodecs-5.65.0</a></td>
   <td align="right">216kB</td>
   <td><tt>7c80cb82101501c5c9d10a9658d8a610678e872d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcompletion-5.65.0.tar.xz">kcompletion-5.65.0</a></td>
   <td align="right">117kB</td>
   <td><tt>792d20c604194acd9524dced532279ad121dfde8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kconfig-5.65.0.tar.xz">kconfig-5.65.0</a></td>
   <td align="right">240kB</td>
   <td><tt>3a561cb07e4af81c7a5da3ad119cdb5b0cd836e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kconfigwidgets-5.65.0.tar.xz">kconfigwidgets-5.65.0</a></td>
   <td align="right">370kB</td>
   <td><tt>86e504644f5b9a6d3c53480a7e4cfa0cdb2eac48</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcontacts-5.65.0.tar.xz">kcontacts-5.65.0</a></td>
   <td align="right">529kB</td>
   <td><tt>ac76493d3e1050583c1250181cf64f046945bf84</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcoreaddons-5.65.0.tar.xz">kcoreaddons-5.65.0</a></td>
   <td align="right">367kB</td>
   <td><tt>cdb4117d2b968694f3308c393ed12be945102bf2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kcrash-5.65.0.tar.xz">kcrash-5.65.0</a></td>
   <td align="right">22kB</td>
   <td><tt>3e7bdfb9b62b105faacefd74ea1594732d9942f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kdbusaddons-5.65.0.tar.xz">kdbusaddons-5.65.0</a></td>
   <td align="right">39kB</td>
   <td><tt>17806275ea09a449b89418ceca57f912715ab5de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kdeclarative-5.65.0.tar.xz">kdeclarative-5.65.0</a></td>
   <td align="right">170kB</td>
   <td><tt>764db57b9b3c9f374cf34f146f7bcd21ec9a2374</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kded-5.65.0.tar.xz">kded-5.65.0</a></td>
   <td align="right">37kB</td>
   <td><tt>3734c457771dfbe530775a99cfc12fbf1e3e57bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kdesu-5.65.0.tar.xz">kdesu-5.65.0</a></td>
   <td align="right">46kB</td>
   <td><tt>3c8b2183c0b5ef7cab9f0ad81383e5c345ef2a8f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kdnssd-5.65.0.tar.xz">kdnssd-5.65.0</a></td>
   <td align="right">57kB</td>
   <td><tt>93db2ec43cecbea6edfca6c4985878877bf2f485</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kdoctools-5.65.0.tar.xz">kdoctools-5.65.0</a></td>
   <td align="right">418kB</td>
   <td><tt>8077563d8ede2677207791ec2461cb4d8882b989</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kemoticons-5.65.0.tar.xz">kemoticons-5.65.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>b764bd4e4caa09a9ba902637059ae16cac63237e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kfilemetadata-5.65.0.tar.xz">kfilemetadata-5.65.0</a></td>
   <td align="right">411kB</td>
   <td><tt>fdd5a839ca169e87ebc935f129cca912e68aeec5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kglobalaccel-5.65.0.tar.xz">kglobalaccel-5.65.0</a></td>
   <td align="right">82kB</td>
   <td><tt>9c64962765b2409403e94e0fc76b237b4d02a34f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kguiaddons-5.65.0.tar.xz">kguiaddons-5.65.0</a></td>
   <td align="right">39kB</td>
   <td><tt>3264485d25c16808e84a6bf939935ffa3c52e523</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kholidays-5.65.0.tar.xz">kholidays-5.65.0</a></td>
   <td align="right">201kB</td>
   <td><tt>cc8c8f1e4e169aa54a46ddf659e549a883f22d1c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/ki18n-5.65.0.tar.xz">ki18n-5.65.0</a></td>
   <td align="right">571kB</td>
   <td><tt>e72875c08b381c77a2ba3b112b8c020ad0ac2705</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kiconthemes-5.65.0.tar.xz">kiconthemes-5.65.0</a></td>
   <td align="right">204kB</td>
   <td><tt>a504d3be56b8f07cb94fc4faf9a7541fe8233943</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kidletime-5.65.0.tar.xz">kidletime-5.65.0</a></td>
   <td align="right">26kB</td>
   <td><tt>daad0a5f3768d745adfaf2be6a29ad17666e52bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kimageformats-5.65.0.tar.xz">kimageformats-5.65.0</a></td>
   <td align="right">239kB</td>
   <td><tt>f7fe7d3a580bd3cde996953b7986a98c0ebf45a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kinit-5.65.0.tar.xz">kinit-5.65.0</a></td>
   <td align="right">116kB</td>
   <td><tt>2d9fe11fa52735e11a26f412aa1b22ba17c953a6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kio-5.65.0.tar.xz">kio-5.65.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>e3c6b1abc5308cf092761b00f63974976e21519c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kirigami2-5.65.0.tar.xz">kirigami2-5.65.0</a></td>
   <td align="right">154kB</td>
   <td><tt>ca626137b4c3db4595122c90ca06ff55805e8310</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kitemmodels-5.65.0.tar.xz">kitemmodels-5.65.0</a></td>
   <td align="right">383kB</td>
   <td><tt>598735d27db3a9c645a5de7f7105669e4db26615</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kitemviews-5.65.0.tar.xz">kitemviews-5.65.0</a></td>
   <td align="right">74kB</td>
   <td><tt>2e9149cd6c6f3ec8f2258ae99ed578ce17ee6012</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kjobwidgets-5.65.0.tar.xz">kjobwidgets-5.65.0</a></td>
   <td align="right">86kB</td>
   <td><tt>7e76806394a915b4e83b59ab76e1ef07c11292d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/knewstuff-5.65.0.tar.xz">knewstuff-5.65.0</a></td>
   <td align="right">1.0MB</td>
   <td><tt>8e10361a88d05c2f82755fd9ea0780b1cb579f67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/knotifications-5.65.0.tar.xz">knotifications-5.65.0</a></td>
   <td align="right">114kB</td>
   <td><tt>e8829ddf1836a36c6f16054dcc50366c84aa9993</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/knotifyconfig-5.65.0.tar.xz">knotifyconfig-5.65.0</a></td>
   <td align="right">82kB</td>
   <td><tt>026e42bb3c8bcc856ca32da7181ded5f62408870</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kpackage-5.65.0.tar.xz">kpackage-5.65.0</a></td>
   <td align="right">134kB</td>
   <td><tt>40004dc6f5c9b83c1fbcd710afa1be4daabbc981</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kparts-5.65.0.tar.xz">kparts-5.65.0</a></td>
   <td align="right">172kB</td>
   <td><tt>755fb15fa0446839bc2f04a5bfc5b08574826d38</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kpeople-5.65.0.tar.xz">kpeople-5.65.0</a></td>
   <td align="right">60kB</td>
   <td><tt>6f9ab582eacf842e62db9c1a795c362f32bb3807</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kplotting-5.65.0.tar.xz">kplotting-5.65.0</a></td>
   <td align="right">29kB</td>
   <td><tt>2035bf24a16a9e7db9295b2c85325b533cf0aee3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kpty-5.65.0.tar.xz">kpty-5.65.0</a></td>
   <td align="right">57kB</td>
   <td><tt>bb531bb4b94535170098abc6210f0bd338814fbd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kquickcharts-5.65.0.tar.xz">kquickcharts-5.65.0</a></td>
   <td align="right">91kB</td>
   <td><tt>bdaae27fec95c1d354dd306469676d6ea6c0fef9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/krunner-5.65.0.tar.xz">krunner-5.65.0</a></td>
   <td align="right">61kB</td>
   <td><tt>cce07d3ceeaa77edecba9abc7aead6308470959c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kservice-5.65.0.tar.xz">kservice-5.65.0</a></td>
   <td align="right">245kB</td>
   <td><tt>5d4a596083ac5823e5675ff9d857a364d78d1dee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/ktexteditor-5.65.0.tar.xz">ktexteditor-5.65.0</a></td>
   <td align="right">2.2MB</td>
   <td><tt>23f2dfa077e6f93f252cc4191691caab1782d51e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/ktextwidgets-5.65.0.tar.xz">ktextwidgets-5.65.0</a></td>
   <td align="right">304kB</td>
   <td><tt>25b3f9e2fb6f51acd0a90a2024b3a78fb31fa162</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kunitconversion-5.65.0.tar.xz">kunitconversion-5.65.0</a></td>
   <td align="right">812kB</td>
   <td><tt>6f36761e41a69b0f059365439c5457d21c029410</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kwallet-5.65.0.tar.xz">kwallet-5.65.0</a></td>
   <td align="right">288kB</td>
   <td><tt>80cdb2b17d6a5e18f2e1a30d2d2c273e7e8d157c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kwayland-5.65.0.tar.xz">kwayland-5.65.0</a></td>
   <td align="right">332kB</td>
   <td><tt>4cff5a2f6195124a708271ca142aa88ddca24b06</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kwidgetsaddons-5.65.0.tar.xz">kwidgetsaddons-5.65.0</a></td>
   <td align="right">2.1MB</td>
   <td><tt>5cac22d381b649d58e455f3fcd2b58f9d8064534</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kwindowsystem-5.65.0.tar.xz">kwindowsystem-5.65.0</a></td>
   <td align="right">168kB</td>
   <td><tt>4e074ea036d702f6003c224dffa4876e23e0ff56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kxmlgui-5.65.0.tar.xz">kxmlgui-5.65.0</a></td>
   <td align="right">840kB</td>
   <td><tt>7767ad5527324041a68d4ad9d42d49634f0cff97</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/kxmlrpcclient-5.65.0.tar.xz">kxmlrpcclient-5.65.0</a></td>
   <td align="right">29kB</td>
   <td><tt>a70b6d616c92a77d0b94381c578c16a427dbc6ed</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/modemmanager-qt-5.65.0.tar.xz">modemmanager-qt-5.65.0</a></td>
   <td align="right">99kB</td>
   <td><tt>9c16c8093c053040732fd2bb0f8cbbb583d92ee3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/networkmanager-qt-5.65.0.tar.xz">networkmanager-qt-5.65.0</a></td>
   <td align="right">181kB</td>
   <td><tt>5cf9b1d79b95e8faba0d7790563b1d6d59e7fb5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/oxygen-icons5-5.65.0.tar.xz">oxygen-icons5-5.65.0</a></td>
   <td align="right">224MB</td>
   <td><tt>5b557fc46d4a7ecb8727676fea69337ab76c5916</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/plasma-framework-5.65.0.tar.xz">plasma-framework-5.65.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>6dfb717ce68f51f63bbc30971a7dd89c218df176</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/prison-5.65.0.tar.xz">prison-5.65.0</a></td>
   <td align="right">41kB</td>
   <td><tt>ce91cde66ab273b134e1092379ae01cf502c11d1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/purpose-5.65.0.tar.xz">purpose-5.65.0</a></td>
   <td align="right">152kB</td>
   <td><tt>181772e02efd908d9de9619763d6159bac7a57c2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/qqc2-desktop-style-5.65.0.tar.xz">qqc2-desktop-style-5.65.0</a></td>
   <td align="right">44kB</td>
   <td><tt>525a6d75a330a7e4c7ac78bd45abdbe99e9c8ba5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/solid-5.65.0.tar.xz">solid-5.65.0</a></td>
   <td align="right">258kB</td>
   <td><tt>e7029628468d8a4544418d949fbb72eccd291420</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/sonnet-5.65.0.tar.xz">sonnet-5.65.0</a></td>
   <td align="right">282kB</td>
   <td><tt>dd2bda011e14a81589b925f3f31f05b8157919d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/syndication-5.65.0.tar.xz">syndication-5.65.0</a></td>
   <td align="right">496kB</td>
   <td><tt>df1575c1d68a895715c22d175a5a59d4c322d7e3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/syntax-highlighting-5.65.0.tar.xz">syntax-highlighting-5.65.0</a></td>
   <td align="right">1.5MB</td>
   <td><tt>a27ac7c740f1af2ed1d57780a895260328603432</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.65/threadweaver-5.65.0.tar.xz">threadweaver-5.65.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>d44372f9cf0d7a0d6e2601a70fd086c62182f127</tt></td>
</tr>

</table>
