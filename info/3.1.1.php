<?php
  $page_title = "KDE 3.1.1 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
KDE 3.1.1 was released on March 20th, 2003. Read the <a
href="/announcements/announce-3.1.1.php">official announcement</a>.
A KDE 3.1.1a update for the kdelibs, kdebase and kdegraphics
packages was released on April 9th, 2003</p>

<?php include "unmaintained.inc" ?>

<!-- <h2>Updates</h2> -->

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#00097;&#105;&#0108;to&#058;s&#x65;c&#117;ri&#x74;y&#x40;&#0107;&#x64;&#101;&#x2e;&#0111;&#0114;&#0103;">se&#99;&#00117;r&#105;&#00116;y&#064;k&#100;e.or&#103;</a>.</p>

<ul>
<li>
Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="security/advisory-20030409-1.txt">detailed advisory</a>. KDE 3.1.1a, consisting
of updated kdelibs, kdebase and kdegraphics packages, has been released to address this problem.
It is strongly recommended to update to KDE 3.1.1a
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>


<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
fixed after the release date:</p>

<ul>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=55571">Bug 55571: https authentication through proxy fails</a></li>
<li><a href="http://lists.kde.org/?l=kde-core-devel&amp;m=104221693026288&amp;w=2">KZip fails for some .zip archives</a></li>
<li><a href="http://lists.kde.org/?l=kde-cvs&amp;m=104766756731187&amp;w=2">Minicli layout problem with Qt 3.1.2</a></li>
<li><a href="http://lists.kde.org/?l=kde-cvs&amp;m=104758242610505&amp;w=2">KDM user window width is incorrect with Qt 3.1.2</a></li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bugs database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>
and sound related questions are answered <a
href="http://www.arts-project.org/doc/handbook/faq.html">in the FAQ of
the aRts Project</a>

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/requirements/3.1.php">KDE 3.1 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.1.1 is available for download:
</p>

<?php
include "source-3.1.1.inc"
?>

<!-- Comment the following if Konstruct is not up-to-date -->
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p>

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.1.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/">http</a> or
  <a href="/mirrors/ftp.php">FTP mirrors</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p>
  At the time of this release, pre-compiled packages are available for:
</p>

<?php
include "binary-3.1.1.inc"
?>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.1-api/">
programming interface of KDE 3.1</a>.
</p>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
