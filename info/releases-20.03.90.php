<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $version = "20.03.90";
  $pageConfig = array_merge($pageConfig, [
      'title' => $version." Releases Source Info Page",
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $version_link = "releases/20.03.90"; // filename used for announcement
  $signer = "Christoph Feck";
  $signing_fingerprint = "F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87"
?>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><?php print $version ?> Release</h1>

<h2>Download and Compilation</h2>

<p>
 <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source">Build instructions</a>
 are available on our Community wiki.
</p>

<h2><a name="source">Source Code</a></h2>
<p>
  The complete source code for <?php echo $version; ?> releases is available for download:
</p>

<p>
  The tarballs have been signed by <?php print $signer ?> <a href="http://pgp.mit.edu/pks/lookup?op=get&amp;search=0x<?php print $signing_fingerprint ?>"><?php print $signing_fingerprint ?></a>.
</p>

<?php
include "source-releases-$version.inc"
?>

</main>
<?php
    require('../aether/footer.php');
