<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/bluedevil-5.15.2.tar.xz">bluedevil-5.15.2</a></td>
   <td align="right">153kB</td>
   <td><tt>fc4bda560ff743cd5db70071811418beb5e19c5814bbc68a8427dafee6c1cdb4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/breeze-5.15.2.tar.xz">breeze-5.15.2</a></td>
   <td align="right">23MB</td>
   <td><tt>682bc6c7669f5440b64157e57b2b1a157234dca1c7e721874a52bb1117bb4e54</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/breeze-grub-5.15.2.tar.xz">breeze-grub-5.15.2</a></td>
   <td align="right">2.9MB</td>
   <td><tt>e51f6b28940405d02ab385c71109f3e7c7880520a3d01cb31f1df192062435a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/breeze-gtk-5.15.2.tar.xz">breeze-gtk-5.15.2</a></td>
   <td align="right">46kB</td>
   <td><tt>5cba7b4b0db8cc36cff4390bae197163cab2474fbcfc2833d469caeea5767d39</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/breeze-plymouth-5.15.2.tar.xz">breeze-plymouth-5.15.2</a></td>
   <td align="right">102kB</td>
   <td><tt>f093566ae51218b4461d1dbebb7a8ee402077bda2e67d840dc71309926c9d758</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/discover-5.15.2.tar.xz">discover-5.15.2</a></td>
   <td align="right">9.9MB</td>
   <td><tt>c2f8e10208ddc90edcdc82d0aba0eaba216e2129f53eddde5e9ba88e9fb63c6d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/drkonqi-5.15.2.tar.xz">drkonqi-5.15.2</a></td>
   <td align="right">721kB</td>
   <td><tt>bb7dd043465132bb1673a274aa9e57a568244b24a96c7088667ebfa31ed9ad20</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kactivitymanagerd-5.15.2.tar.xz">kactivitymanagerd-5.15.2</a></td>
   <td align="right">82kB</td>
   <td><tt>7e4645f87882858d0707215b5b8d2182ab4b0ccd6d1d04400d9c3f43bcba8dae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kde-cli-tools-5.15.2.tar.xz">kde-cli-tools-5.15.2</a></td>
   <td align="right">571kB</td>
   <td><tt>62304fd27b9e78fce90507bd2d92de4ec6c018fe39c899756ada2ad0bf4627ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kde-gtk-config-5.15.2.tar.xz">kde-gtk-config-5.15.2</a></td>
   <td align="right">152kB</td>
   <td><tt>449528ef124835386f12db86dd349e582369652796aba5f51fc2ba6abd3dcf1e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kdecoration-5.15.2.tar.xz">kdecoration-5.15.2</a></td>
   <td align="right">41kB</td>
   <td><tt>79b089269ce2fb830f3c3814b952dde3b64b90f092238ce96b2ae8056429debf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kdeplasma-addons-5.15.2.tar.xz">kdeplasma-addons-5.15.2</a></td>
   <td align="right">588kB</td>
   <td><tt>9e360c532f3a386959a93f4d46ea87d0bffa609f9252e6abc47e170b064a3230</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kgamma5-5.15.2.tar.xz">kgamma5-5.15.2</a></td>
   <td align="right">77kB</td>
   <td><tt>5ea3d42ede065b71de0e015d76c42fe8fbb930e390c7041c5be0e16d1b10171b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/khotkeys-5.15.2.tar.xz">khotkeys-5.15.2</a></td>
   <td align="right">1.7MB</td>
   <td><tt>71527e48d3dbd7e4f7d2a5e7d7506f51534adf2ca1647bcf91a645bbf6a612a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kinfocenter-5.15.2.tar.xz">kinfocenter-5.15.2</a></td>
   <td align="right">1.2MB</td>
   <td><tt>fc967e7a16e2e305037e56bb148f8eacd6a85fc465147d12fa7637215a054f71</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kmenuedit-5.15.2.tar.xz">kmenuedit-5.15.2</a></td>
   <td align="right">790kB</td>
   <td><tt>d3ef0d95ea9863654386f6a577e7cdb8da4a2a90abd49e94f54d1b54019156f6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kscreen-5.15.2.tar.xz">kscreen-5.15.2</a></td>
   <td align="right">120kB</td>
   <td><tt>5b676ea66b623d60a50402fc5c9f822447cf6e683128978b29775d28194af7ef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kscreenlocker-5.15.2.tar.xz">kscreenlocker-5.15.2</a></td>
   <td align="right">119kB</td>
   <td><tt>baf2d72a9594e837ce78fa9f84695be7516a78d6d8321a1e7fe3e95bd2fa6325</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/ksshaskpass-5.15.2.tar.xz">ksshaskpass-5.15.2</a></td>
   <td align="right">20kB</td>
   <td><tt>f728d22d161f3a21b877dd4b19b085c15fc95c1f85bba4bd4653add736d2c8d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/ksysguard-5.15.2.tar.xz">ksysguard-5.15.2</a></td>
   <td align="right">488kB</td>
   <td><tt>b6f107d7bbe050fd41228326b331b84a0af47f720e530073af71ca53139b68d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kwallet-pam-5.15.2.tar.xz">kwallet-pam-5.15.2</a></td>
   <td align="right">18kB</td>
   <td><tt>df606bea17d96d6d2114dac4f30d34dadc14d96d7570ef604190c0c79979128a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kwayland-integration-5.15.2.tar.xz">kwayland-integration-5.15.2</a></td>
   <td align="right">18kB</td>
   <td><tt>5b55f8e8acf18998d3fa2cbf27c2c6686a82fd81bfce9bab0f35d4372b3507c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kwin-5.15.2.tar.xz">kwin-5.15.2</a></td>
   <td align="right">5.9MB</td>
   <td><tt>de4997170d11adb175e0c9499e03c06b4d83aa572a5f71040a873bf7b0f65d84</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/kwrited-5.15.2.tar.xz">kwrited-5.15.2</a></td>
   <td align="right">19kB</td>
   <td><tt>1e16904abea59c663fea97d97866ce914334392b774cd0d48e435c617c0ed546</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/libkscreen-5.15.2.tar.xz">libkscreen-5.15.2</a></td>
   <td align="right">79kB</td>
   <td><tt>a46e0aeab9ccf98a212b37c906e0c7f54b290928d1b4cd8ab0032e3d6f562c14</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/libksysguard-5.15.2.tar.xz">libksysguard-5.15.2</a></td>
   <td align="right">563kB</td>
   <td><tt>8cbc587a32fdd7c6f0f56b569376aeaa7821ebeadeedb4fdfea71d25a3541e15</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/milou-5.15.2.tar.xz">milou-5.15.2</a></td>
   <td align="right">55kB</td>
   <td><tt>282349371d55994ac101a9fbc81d14408d386e6b5958ddafb4efccc5a15ced78</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/oxygen-5.15.2.tar.xz">oxygen-5.15.2</a></td>
   <td align="right">4.2MB</td>
   <td><tt>72471ec234d71985ceea722ffdd1dba4a05d19737ac98eb90e69c4188c6c2cef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-browser-integration-5.15.2.tar.xz">plasma-browser-integration-5.15.2</a></td>
   <td align="right">109kB</td>
   <td><tt>e9d8d14455cd6f7851e1e3e6cba46fa4705d152ad8971867e6e74b30393fb702</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-desktop-5.15.2.tar.xz">plasma-desktop-5.15.2</a></td>
   <td align="right">8.7MB</td>
   <td><tt>40576c38428c96903bf085e06729521bcce8ea0e498e40797a900df7aa9cb4ab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-integration-5.15.2.tar.xz">plasma-integration-5.15.2</a></td>
   <td align="right">53kB</td>
   <td><tt>7d7a38cc0763e5cacd8472dfdd0a801ec876ec7512cbd8d85bdd24a8f433923e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-nm-5.15.2.tar.xz">plasma-nm-5.15.2</a></td>
   <td align="right">753kB</td>
   <td><tt>5d495058730df7156c8f6df5017d7db5d280d5d6f1df985e596892a2b186ca89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-pa-5.15.2.tar.xz">plasma-pa-5.15.2</a></td>
   <td align="right">92kB</td>
   <td><tt>03871b5e54b18bfc4b2f493da690f8c0c843bb41c95670bf335ed0ab096283b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-sdk-5.15.2.tar.xz">plasma-sdk-5.15.2</a></td>
   <td align="right">243kB</td>
   <td><tt>f9a349ae5ae0b8a3df3f86d91bfe7432c8ede82656d7ef2a0bc185e7be80054f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-tests-5.15.2.tar.xz">plasma-tests-5.15.2</a></td>
   <td align="right">1kB</td>
   <td><tt>515846facd2046b4a25b64d40a86db6650cf625ff6478e63d3741cbe6ba657af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-vault-5.15.2.tar.xz">plasma-vault-5.15.2</a></td>
   <td align="right">118kB</td>
   <td><tt>97e668c756831e7ce94b625ed054e3cdf40a45eb502f30fbc2e2f89d3480ac3e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-workspace-5.15.2.tar.xz">plasma-workspace-5.15.2</a></td>
   <td align="right">4.4MB</td>
   <td><tt>5f0dd03c3e2aa845634e6199f3fbc18cb14a0f580367982aca43223f58058dd9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plasma-workspace-wallpapers-5.15.2.tar.xz">plasma-workspace-wallpapers-5.15.2</a></td>
   <td align="right">23MB</td>
   <td><tt>5aefa25cb27d3052b2540aabcb774398fd395b158bc7ead466923dcac64a8101</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/plymouth-kcm-5.15.2.tar.xz">plymouth-kcm-5.15.2</a></td>
   <td align="right">38kB</td>
   <td><tt>f153b9056a1329a9ecde18160215dfbfdad020368dfd8fa19d47ca29aa9f79ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/polkit-kde-agent-1-5.15.2.tar.xz">polkit-kde-agent-1-5.15.2</a></td>
   <td align="right">41kB</td>
   <td><tt>0ac68a30fa4e7939e0743cae125e6485562bc566d2554632c163025dcf7110c4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/powerdevil-5.15.2.tar.xz">powerdevil-5.15.2</a></td>
   <td align="right">585kB</td>
   <td><tt>a9cd03c3e236d85297cd3437d5ea3d68c70df140f0beb457beb2aea311e4853e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/sddm-kcm-5.15.2.tar.xz">sddm-kcm-5.15.2</a></td>
   <td align="right">59kB</td>
   <td><tt>3ae1ff30f41a604fb80231da88b008ac8dc10f61a2b6a6c863f0607c275015d1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/systemsettings-5.15.2.tar.xz">systemsettings-5.15.2</a></td>
   <td align="right">177kB</td>
   <td><tt>d450b06fc4a75fdba42380cb26719f7def1c2a0dae78847c288500bfc256a367</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/user-manager-5.15.2.tar.xz">user-manager-5.15.2</a></td>
   <td align="right">532kB</td>
   <td><tt>809a9f6dce41d47884b648d1098df9685f8041a448d71f744997a4a9dbb7b427</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.15.2/xdg-desktop-portal-kde-5.15.2.tar.xz">xdg-desktop-portal-kde-5.15.2</a></td>
   <td align="right">58kB</td>
   <td><tt>e4f6688c219d4be2a9ee9afd7f145514bd3ffad50bfaf4e1f887d9ff6800feff</tt></td>
</tr>

</table>
