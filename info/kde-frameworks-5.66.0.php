<?php
    require('../aether/config.php');

    $version = "5.66.0";
    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Frameworks ".$version." Source Info and Download",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="releaseAnnouncment container">

<h1>KDE Frameworks <?php print $version ?></h1>

<p>
For new features, bugfixes and changes read the <a href="../announcements/kde-frameworks-<?php echo $version; ?>.php">KDE Frameworks <?php echo $version; ?> announcement</a>.
</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a
href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.
Security issues are listed on the <a
href="http://kde.org/info/security/">KDE Security Advisories</a> page.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
    <li>No significant bugs.</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>Install Packages</h2>

<p> The easiest way to install and get started developing with KDE
 Frameworks is to install pre-built packages.  For details of Linux
 distributions which do so see the <a
 href="https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro">
 Get KDE Software on Your Linux distro wiki page</a>.</p>

<h2>Source Install and Use</h2>

<p>You can compile Frameworks yourself to include them in your applications.  See the
 <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source">Build instructions</a>
 on our developer wiki.
</p>

<p>The <a
href="https://api.kde.org/frameworks/">KDE
Frameworks API Reference</a> lists the Frameworks, documents their
Tiers which indicate the compile-time dependencies, and Types which
indicate the run-time dependencies.  It has complete API documentation
and example code so you can easily make use of these Frameworks in
your Qt applications.</p>

<h2><a name="source">Download Source Code</a></h2>
<p>
  The complete source code for KDE Frameworks <?php echo $version; ?> is available for download:
</p>

<?php
include "source-kf-$version.inc"
?>

</main>
<?php
  require('../aether/footer.php');
