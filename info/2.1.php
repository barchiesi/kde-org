<?php
  $page_title = "KDE 2.1 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>KDE 2.1 was released on February 26, 2001. Read the
<a href="/announcements/announce-2.1.php">official announcement</a>.
</p>

<p>For a list of changes since KDE 2.0, see the
<a href="/announcements/changelogs/changelog2_0to2_1.php">list of changes</a></p>

<p>For a high-level overview of the features of KDE, see the
<a href="/info">KDE info page</a></p>

<p>For a graphical tutorial on using KDE 2, see this
<a href="http://www.linux-mandrake.com/en/demos/Tutorial/">tutorial page</a> from Linux Mandrake</p>

<h2>Updates</h2>

<p>Meanwhile, <a href="2.1.1.php">KDE 2.1.1</a> has been released. It contains several bug fixes, so we
encourage to upgrade to 2.1.1. This page will no longer be updated.</p>
 
<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed to the
<a href="http://konqueror.kde.org/faq/">Konqueror FAQ</a> and sound related
questions are answered in the <a href="http://www.arts-project.org/doc/handbook/faq.html">Arts FAQ</a>

<h2>Download and Installation</h2>

See the links listed in the <a
href="/announcements/announce-2.1.php">announcement</a>. The KDE
<a href="/documentation/faq/install.html">Installation FAQ</a>
provides generic instruction about installation issues.

<p>If you want to compile from sources we offer
<a href="http://developer.kde.org/build/">instructions</a> and help for common
problems in the <a href="http://developer.kde.org/build/compilationfaq.html">Compilation
FAQ</a>.</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 2.1 see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/2.2/kdelibs/KDE2PORTING.html?rev=2.5">
porting guide</a> or discuss your problems with fellow developers
on the k&#x64;e&#x32;-&#112;&#0111;rtin&#103;&#x40;kde.org mailing list. 

<p>There is also info on the <a
href="http://developer.kde.org/documentation/kde2arch.html">architecture</a>
and the programming <a
href="http://developer.kde.org/documentation/library/2.0-api/classref/index.html">
interface</a>.</p>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
