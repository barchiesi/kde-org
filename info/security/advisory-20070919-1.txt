KDE Security Advisory: KDM passwordless login vulnerability
Original Release Date: 2007-09-19
URL: http://www.kde.org/info/security/advisory-20070919-1.txt

0. References
        CVE-2007-4569


1. Systems affected:

	KDM as shipped with KDE 3.3.0 up to including 3.5.7. KDE 3.2.x and
	older and newer versions than KDE 3.5.7 are not affected. 


2. Overview:

	KDM can be tricked into performing a password-less login even for
	accounts with a password set under certain circumstances, namely
        autologin to be configured and "shutdown with password" enabled.

        This vulnerability was discovered and reported by Kees Huijgen.


3. Impact:

	KDM might allow a normal user to login as another user or even
	root without properly supplying login credentials.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.5.0 - KDE 3.5.7 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        ee6c57046902c5b5a32a4699558baafc  post-3.5.7-kdebase-kdm.diff

        A patch for KDE 3.3.0 - KDE 3.4.2 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        ad7333a336bdbaef7fae5e74cd12119b  post-3.4.2-kdebase-kdm.diff
