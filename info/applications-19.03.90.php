<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $version = "19.03.90";
  $pageConfig = array_merge($pageConfig, [
      'title' => "KDE Applications ".$version." Source Info Page",
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $version_link = "announce-applications-19.04-rc.php"; // filename used for announcement
  $signer = "Christoph Feck";
  $signing_fingerprint = "F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87"
?>

<main class="releaseAnnouncment container">

<h1 class="announce-title">KDE Applications <?php print $version ?> Release</h1>

<p>
For new features, bugfixes and changes read the <a href="../announcements/<?php print $version_link ?>">KDE Applications <?php print $version; ?> announcement</a>.
</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls surfacing after the release was packaged:</p>

<ul>
    <li>None.</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>Download and Compilation</h2>

<p>
 <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source">Build instructions</a>
 are available on our Community wiki.
</p>

<h2><a name="source">Source Code</a></h2>
<p>
  The complete source code for KDE Applications <?php echo $version; ?> is available for download:
</p>

<p>
  The tarballs have been signed by <?php print $signer ?> <a href="http://pgp.mit.edu/pks/lookup?op=get&amp;search=0x<?php print $signing_fingerprint ?>"><?php print $signing_fingerprint ?></a>.
</p>

<?php
include "source-applications-$version.inc"
?>

</main>
<?php
    require('../aether/footer.php');
