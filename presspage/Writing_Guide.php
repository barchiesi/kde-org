<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Writing Guide"
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="container">

<h1>Write about KDE</h1>

<p>
  The purpose of this page is to help anyone who wants to write or talk about
  the KDE community and it's products, be it for a blog or a magazine or 
  anything else. We will give you some general tips and ideas, show
  you how to find information and help you contact the community with questions.
</p>
<h3>
  General tips
</h3>
<br>
Free Software has different ideas and principles guiding it's development
compared to the usual proprietary software. Being aware of these differences can
make the difference between an uninformed and an informed article. The following
tips can help you write a better and more complete article by pointing out the
differences in culture between proprietary and free software communities.<br>
<br>
<ul>
  <li>
    First of all, the <a href="http://www.fsf.org/">site of the Free Software
    Foundation</a> can give you more information on our roots and the principles
    of Free Software. Don't skip the
    <a href="http://www.gnu.org/philosophy">philosophy section</a>!
  </li>
  <li>
    <i>Search out the defining features, rather than comparing to commercial
    products.</i> Many people in the commercial world view open source projects
    exclusively as replacements for their Windows equivalents. While some
    projects exist that do precisely this, the vast majority have unique
    features that help to identify the program. Focusing on what makes the
    program unique will produce a better response.<br>
  </li>
  <li>
    <i>Don't hesitate to contact the program developers directly.</i> Direct
    contact often produces the best answers. Usually programs will have an
    "About" screen (in KDE applications it's under the help menu) that lists the
    contact information for the programmers that were actually involved in the
    creation of that software. Most programmers are very happy to answer press
    inquiries, or will redirect your questions to someone who can better deal with
    any specific topic.
  </li>
  <li>
    <i>Informal language works best when communicating. </i>Most open source programmers do not have a
    degree in business, journalism, etc. and will respond faster and with more
    details if they are approached as though the interview was taking place in a
    coffee house. If you ask formal questions, do not be put off by the informal
    response you may get in return. Ask for definitions of any jargon that you
    do not understand as some words, especially the word "Hacker", have a very
    different meaning in the open source context.
  </li>
  <li>
    <i>Don't expect immediate responses.</i> Many open source programmers are
    coding in their spare time and work elsewhere to pay the bills. They will
    usually respond, but you need to give them time.
  </li>
  <li>
    <i>Discover people's motivations.</i> While some people work on free
    software for altruistic reasons, you will find that many do not. Many people
    are paid by a company to implement features that this company would find
    useful. Others simply find it an enjoyable hobby, and like being a part of
    the community. A person's motivation will often dictate what sort of
    programming they will do. If you discover what motivates a person or
    project, you will better understand their goals, which isn't always to
    compete with commercial software offerings...
  </li>
  <li>
    <i>Play up collaboration, not division.</i> While arguments between Linus
    Torvalds and Sun's CEO may make the rounds in popular press, cooperation is
    the prevalent mode of operation within the open source world. Free software
    projects cover a wide range of applications, from web servers to games, and
    quite often there is more than one product being developed under so called
    'co-opetition'. Since the source code is available, projects readily and
    freely borrow ideas from one another, and even though the implementation
    details may be different, standards are developed for communications, data
    formats, and so forth. Open source projects are often stricter adherents to
    interoperability standards than their commercial counterparts.
  </li>
  <br>
</ul>
<h3>
  Finding information.
</h3>
<p>
  As most work in KDE is done over the Internet, 95% of the information can be
  found there as well. But much of this information is hidden away in
  mailing lists, chat channels and blogs. It is hard to extract information out
  of those in an efficient way, so we will give you a few pointers on how to
  find information about a topic efficiently.
</p>
<h3>
  The KDE website
</h3>
<p>
  The first source of information about the KDE community can be found on the
  <a href="http://www.kde.org/ title=KDE website">KDE website</a>.
  <a href="http://kde.org/whatiskde/">What is KDE</a> and
  <a href="http://kde.org/info/">general information about KDE</a> are of most
  interest.
</p>
<h3>
  Release announcements
</h3>
Often, the
<a href="http://www.kde.org/announcements/ id=s4xq title=release
announcements">release
announcements</a> come with a nice, graphical overview of what's new. Based on
this, one can quickly write an interesting piece showcasing the newest and
greatest features in the latest KDE products.<br>
<h3>
  Websites
</h3>
<p>
  Most individual KDE sub-projects have their own website under the
  <a href="http://www.kde.org/">kde.org</a> umbrella. For example, the
educational
  project can be found on <a href="http://edu.kde.org/">edu.kde.org</a> and the
KDE
  office team has their webpage on <a
href="http://koffice.kde.org/">koffice.kde.org</a>. These
  sites are aggregated on the <a
href="http://techbase.kde.org/Projects">Projects
  page</a>. It is a good place to start and find the basic information, but be
  aware that it can be seriously outdated!<br>
</p>
<h3>
  Techbase
</h3>
<p>
  Much more technical information can be found on the
  <a href="http://techbase.kde.org/">techbase site</a>. KDE gathers all relevant
  developer information here. Interesting pages can be:
</p>
<ul>
  <li>
    Development frameworks in KDE Software Compilation 4:
    <a
href="http://techbase.kde.org/Development/Architecture/KDE4">Architecture/KDE4</
a>
  </li>
  <li>
    Release schedulers and feature plans:
    <a href="http://techbase.kde.org/Schedules">Schedules</a>
  </li>
  <li>
    Overview of the KDE projects:
    <a href="http://techbase.kde.org/Projects">Projects</a>
  </li>
</ul>
<h3>
  Userbase
</h3>
<p>
  Less technical but more user oriented information can be found on the
  <a href="http://userbase.kde.org/">userbase site</a>. KDE gathers all relevant
  end user information here. Interesting pages can be:
</p>
<ul>
  <li>
    an introduction to KDE Software Compilation 4:
    <a href="http://userbase.kde.org/An_introduction_to_KDE">An introduction to
KDE SC 4</a>
  </li>
  <li>
    a list of the KDE applications:
    <a href="http://userbase.kde.org/Applications">Applications</a>
  </li>
  <li>
    Overview the KDE Workspaces (including the Plasma desktop and KWin):
    <a href="http://userbase.kde.org/Applications/Desktop">Desktop</a>
  </li>
</ul>

<h3>
  The KDE news site
</h3>
<p>
  A premier source off information about KDE is the KDE news site,
  <a href="http://dot.kde.org/">the Dot</a>. It offers
  <a href="http://dot.kde.org/searchForm">search functionality</a>, and we can
  give a recommendation: The
  <a
href="http://www.googlesyndicatedsearch.com/u/dot?as_q=road+to+kde&amp;as_epq=&
amp;as_oq=&amp;as_eq=&amp;num=100">'Road
  to KDE 4'</a> series by Troy Unrau are an excellent starting point on the many
  new technologies available in KDE SC 4. Further, just searching for the
  technology you are looking for, like 'Akonadi' will help you find what you are
  looking for.
</p>
<h3>
  People
</h3>
<p>
  For information about the KDE developers, we recommend
  <a href="http://www.behindkde.org/">People behind KDE</a>. It offers
interviews
  with many KDE developers. Further, you can find their blogs mostly on the
  <a href="http://planetkde.org/">Planet</a>. Look under subscriptions for
  individual feeds.
</p>
<h3>
  Commit Digest
</h3>
<p>
  The <a href="http://www.commit-digest.org/">Commit Digest</a> is a very
valuable
  source of more detailed information, but it can be hard to extract due to the
  sheer amount of information. A good tip is to quickly read the 'This Week'
  section at the top to get a quick overview of "what's hot". Further, you can
  use the search functionality in your web-browser... The Commit Digest is not
  being updated anymore but will be picked up in the future again.
</p>
<h3>
  Mailing List Archives
</h3>
<p>
  The most detailed information generally available online is to be found in the
  mailing-list archives of the several KDE projects. Links to these
  mailing-lists can be found on their respective websites (see the
  <a href="http://techbase.kde.org/Projects">Projects</a> site).
</p>
<p>
  An example of the edu mailing-list archives can be found
  <a href="http://mail.kde.org/pipermail/kde-edu/">here</a>. Using the search
  functionality is often required to find anything useful in a decent amount of
  time!
</p>
<h3>
  Contacting the community
</h3>
<p>
  If you want to have the latest information, or verify what you're writing, the
  best place to ask is on the
  <a href="http://en.wikipedia.org/wiki/Mailinglist">mailing-list</a> or the
  <a href="http://en.wikipedia.org/wiki/Internet_Relay_Chat">IRC channels</a>.
You
  can subscribe to the mailing-list (but don't have to, just be sure to mention
  it if you're not so they include you personally in the replies), information
  about this can be found on the individual
  <a href="http://techbase.kde.org/Projects">projects'</a> websites. The same
goes
  for IRC, the channel-names and server information.
</p>
<p>
  If you want to get in contact with individual developers, you can send them an
  email. Names can often be found on the
  <a href="http://techbase.kde.org/Projects">Project</a> website or on
  <a href="http://www.behindkde.org/">People behind KDE</a>, and google-ing
often
  brings up their email address (tip: Google the name + "KDE").
</p>
<br>
<h2>
  Conclusion
</h2>
<p>
  If you want to write about the KDE community or KDE software, it is often advisable to find a specific
  source of interest, as the whole of KDE is a lot to write about. Picking one
  of the technologies developed by KDE and writing an article about them is made easy
  by the huge amount of information available online. The above guide can help
  you quickly gather the basic information, and it tells you where you can find
  or ask about the current status.
</p>

</main>
<?php
  require('../aether/footer.php');
