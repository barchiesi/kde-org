﻿<?php
  $page_title = "KDE SC 4.4.0 Caikaku Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>Auch verfügbar auf:
<?php
  $release = '4.4';
  include "../announce-i18n-bar.inc";
?>
</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Software Compilation 4.4.0 bringt euch ein neues Netbook Interface, Window Tabbing und ein Authentifizierungs Framework
</h3>

<p align="justify">
  <strong>
    KDE Software Compilation 4.4.0 (Codename: <i>"Caikaku"</i>) veröffentlicht
  </strong>
</p>

<p align="justify">
9. Februar, 2010. Heute wurde die Veröffentlichung der <a href="http://www.kde.org/">KDE</a> Software Compilation in der Hauptversion 4.4, codename „Caikaku“, bekannt gegeben welches euch eine neue Sammlung an innovativen Anwendung für Benutzer von freier Software bringt. Die größten Neuheiten sind das, speziell für Netbooks angepasste, Netbook-Interface sowie das Kauth Authentifizierung Framework. Dem KDE Bug-Tracker folgend wurden in dieser Veröffentlichung 7293 Bugs behoben und 1433 gewünschte Funktionen eingeführt. Die KDE Community möchte jedem danken der geholfen hat all dies möglich zu machen.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/general-desktop.jpg"><img src="screenshots/thumbs/general-desktop_thumb.jpg" align="center" width="540" height="338" alt="The KDE Plasma Desktop Workspace" title="The KDE Plasma Desktop Workspace" /></a>
<br />
<em>The KDE Plasma Desktop Workspace</em></div>

<p align="justify">
 Für mehr Details über die Verbesserungen in der 4.4 Veröffentlichung kannst du die <a href="http://www.kde.org/announcements/4.4/guide.php">Visuelle Tour zur KDE Software Compiliation 4.4</a> lesen.
</p>

<h3>
  Die Plasma Arbeitsfläche bringt neue Social-Network und Internet Features
</h3>
<br />
<p align="justify">
 Die KDE Plasma Arbeitsfläche bietet die grundlegend Funktionalität um Anwendungen zu starten und Einstellungen an Dateien und Globalen Eigenschaften vorzunehmen. Die KDE Plasma Entwickler bringen ein neues Netbook Interface mit einem neuen Design das sich besser für die Bedürfnisse von Netbook Benutzern anpasst. Die Einführung von Social-Network und Internet Funktionen in Plasma passen perfekt zur KDE Gemeinschaft, dem Freien Software Team.
</p>

<!-- Plasma Screencast -->
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<p>
<embed src="http://blip.tv/play/hZElgcKJVgA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Improved Interaction with the Plasma Desktop Shell (<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">download</a>)</em>
</p>
</div>


<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong>, das mit 4.4.0 Einzug hält, ist ein alternatives Interface zu dem normalen Plasma Desktop welches speziell auf de Bedürfnisse von Netbook Benutzern zugeschnitten ist. Plasma Netbook versucht so viele Möglichkeiten von Plasma Desktop zu nutzen, allerdings angepasst an den typischen Platzmangel von Netbook Bildschirmen sowie von Touchscreens. Die Plasma Netbook Shell ist ein Vollbild Anwendungsstarter mit Suchfunktion sowie ein „Newspaper“ welches viele Widgets bietet um Inhalt aus dem Internet auf der Arbeitsfläche zu plazieren.
  </li>
  <li>
    Die <strong>Social Desktop</strong>-Initiative bringt viele Verbesserungen die in das Community Widget (früher bekannt als Social Desktop Widget) eingeflossen sind welche dem Benutzer erlauben andern Benutzern Nachrichten zu schicken und neue Freunde zu finden. Das Social-News Widget zeigt einen Livestream von dem was gerade auf dem Social-Network des Benutzers passiert. Das Knowledge Base Widget erlaubt dem Benutzer das Suchen von Fragen und Antworten von verschiedenen Plätzen wie openDesktop.org´s eigener Knowledge Base.
  </li>
  <li>
    Die neue Tab Funktion von KWin erlaubt es dem Benutzer, <strong>Fenster in einem „Tabbed Interface“ zu gruppieren</strong> was das benutzen von einer Großzahl von Anwendungen erleichtert und effizienter macht. Zudem gibt es Neuerungen wie das „Snapping Windows“ welches die größe der Fenster beim Ziehen an eine Bildschirmseite verändert um den Platz sinnvoller zu nutzen. Das KWin Team arbeitet zudem enger mit dem Plasma Team zusammen was nicht nur der Geschwindigkeit zu gute kommt sondern auch neue Funktionen bietet um zwischen Anwendungen und der Arbeitsfläche selbst besser zu agieren. Letztendlich können Künstler nun ihre Themes, dank der Entwicklung an  einem besser konfigurierbarem Fenster Derkorator mit der fähigkeit von Skaliberbaren Grafiken,  einfacher entwickeln und Teilen.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<div align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<p>
<embed src="http://blip.tv/play/hZElgcKPKwA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Managing Windows Made Easier  (<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">download</a>)</em>
</p>
</div>



<h3>
  KDE Anwendungen entwickeln sich
</h3>
<p align="justify">
Die KDE Gemeinschaft bietet euch eine große Anzahl an mächtigen und trotzdem einfach benutzbaren Anwendungen. Diese Veröffentlichung führt eine Vielzahl von Verbesserungen sowie neue Innovative Technologien ein.
</p>
<p align="justify">
<ul>
  <li>
    Mit dieser Veröffentlichung der KDE Software Compilation wurde dank einem Langzeit Aufbau und Planungs Prozess das <strong>GetHotNewStuff-Interface<strong> stark verbessert. Dieses Framework hilft den vielen Drittanbietern ihre Software einfacher den Benutzer zur Verfügung zu stellen. Benutzer können z.b. <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">neue Level in KAtomic</a> oder erweiterte Funktionen direkt über die Anwendung selbst beziehen. Neu sind z.b. die Kommentar Funktion oder die Möglichkeit ein „Fan“ eines Produktes zu werden welches neue Updates über das Social-News Widget zeigt. Benutzer können das Produkt ihrer eigenen Kreativität nun von verschiedenen Anwendungen aus direkt in das Internet hochladen ohne den langweiligen Weg des von Hand Packens + Hochladen über eine Webseite zu vollziehen.
	  </li>
  <li>
    Zwei weitere Langzeit Projekte tragen mit dieser Veröffentlichung Früchte. Nepomuk, dessen Internationaler Forschungsaufwand von der Europäischen Gemeinschaft finanziert wird, hat einen Punkt der akzeptablen Geschwindigkeit und Stabilität erreicht. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Dolphins integrierte Suchfunktion</a> </strong> benutzt Nepomuk's semantisches Framework um Benutzern beim finden und organisieren ihrer Daten zu helfen. Die neue Zeitleiste zeigt kürzlich veränderte Dateien chronologisch und geordnet. Unterdessen hat das KDE PIM Team die erste Anwendung portiert um nutzen von der neuen Datenbankverwaltung <strong>Akonadi</strong> zu machen. Das KDE Adressbuch wurde komplett neu geschrieben und bietet nun eine einfache 3-spaltige Oberfläche.
  </li>
  <li>
    Neben der Integration dieser Haupt-Technologien haben die Entwickler ihre Anwendungen in vielen wegen verbessert. Die KGet Entwickler haben z.b. die Unterstützung für das Überprüfen von digitalen Signaturen hinzugefügt sowie die Möglichkeit um von mehreren Quellen gleichzeitig herunterzuladen. Gwenview beinhaltet nun ein einfach zu bedienendes Werkzeug um Photos zu importieren. Zudem sehen neue oder komplett überarbeitete Anwendungen mit dieser Veröffentlichung das Licht der Welt. Palapeli ist ein Spiel das Benutzern erlaubt Puzzle direkt auf ihrem Computer zu lösen. Zusätzlich gibt es die Funktion eigene Puzzle zu erstellen und diese zu teilen. Cantor ist eine einfache und intuitive Oberfläche für mächtige Statistik- und Forschungssoftware wie <a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a> und <a href="http://maxima.sourceforge.net/">Maxima</a>. Die KDE PIM Suite hat Blogilo eingeführt, eine neue Blogging-Anwendung.
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/dolphin-search.jpg"><img src="screenshots/thumbs/dolphin-search_thumb.jpg" align="center" width="540" height="338"  alt="Desktop Search integrated in KDE's Dolphin File Manager" title="Desktop Search integrated in KDE's Dolphin File Manager" /></a><br />
<em>Desktop Search integrated in KDE's Dolphin File Manager</em></div>

<h3>
  Plattform beschleunigt Entwicklung
</h3>
<p align="justify">
Das strenge streben nach exzellenten Technologien in der KDE Gemeinschaft hat einer der funktionstüchtigen, beständigsten und effizientesten Entwicklungsplattformen als Ergebnis. Mit der 4.4 Veröffentlichung wurde viele neue Social-Network und Technologien für die Gemeinsame Arbeit in die KDE Bibliotheken aufgenommen. Wir bieten nun ein mächtige und flexible Quelloffene Alternativen zu existierenden Technologien. Anstelle Benutzer an unsere Produkte zu binden konzentrieren wir uns darauf Innovationen einzuführen im Desktop und Internet Bereich.
</p>
<p align="justify">
<ul>
  <li>
    Die zugrunde liegende Infrastruktur der KDE Software hat einige nennenswerte Updates erfahren. Vor allem das neue <strong>Qt 4.6</strong> das Unterstützung für die Symbian Plattform anbietet sowie ein neues Animations-Framework, Multitouch und verbesserte Geschwindigkeit. Die <strong>Social Desktop Technologie</strong>, welche mit der letzten Veröffentlichung eingeführt wurde, wurde mit einem Zentralen Management verbessert und benutzt jetzt libattica als eine Bibliothek zum Zugriff auf Web-Services. <strong>Nepomuk</strong> benutzt jetzt ein wesentlich stablieres Backend was es für die perfekte Wahl um in Anwendungen zum Suchen und indizieren von Anwendungen benutzt zu werden.
  </li>
  <li>
    Mit KAuth wurde ein neues Authentifizierungs-Framework eingeführt. <strong>KAuth bietet ein sichere Authentifizierung</strong> und eine dazugehörige Oberflächenelemente für Entwickeler die Prozesse mit ausgewählten rechten starten möchten. Unter Linux benutzt KAuth, PolicyKit als Backend welches <strong>nahtlose Integration in verschiedene Desktop Oberflächen</strong> bietet. KAuth wird bereits in ein ausgewählten Dialogen der Anwendung „Systemeinstellungen“ verwendet und wird in den nächsten Monat weiter in Plasma und KDE Anwendungen integriert werden. KAuth unterstützt gewähren und verwähren über Authorisierungsregeln, dem zwischenspeichern von Passwörtern und einige dynamische Oberflächenelemente die in Anwendungen genutzt werden können.
  </li>
  <li>
    <strong>Akonadi</strong>, die Freie Desktop Groupware zieht mit KDE 4.4.0 endgültig einzug in KDE. Das Adressbuch das mit KDE SC 4.4 veröffentlicht wurde ist die erste KDE Anwendungen die, die neue Akonadi Infrastruktur benutzt. KAdressbook unterstützt lokale Adressbücher ebenso wie unterschiedliche Groupware Server. KDE SC 4.4.0 besigelt somit den Beginn der Akonadi-era in der KDE SC mit mehr Anwendungen in den kommenden Veröffentlichungen. Akonadi ist eine zentrale Schnittstelle für e-Mails, Kontakte, Kalenderdaten und andere persönliche Informationen. Es handelt als ein transparenter Zwischenspeicher für e-Mail-, GroupwareServer oder anderen Onlinequellen.
  </li>
</ul>
</p>
<p>
Liberal lizensiert unter der LGPL welche das benutzen in Proprietärer- und OpenSource Entwicklung unter verschiedenen Systemenen (Linux, UNIX, Mac und MS Windows).
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social-web-widgets.jpg"><img src="screenshots/thumbs/social-web-widgets_thumb.jpg" align="center" width="540" height="338" alt="The Web and Social Networks on the Plasma Desktop" title="The Web and Social Networks on the Plasma Desktop"/></a><br />
<em>The Web and Social Networks on the Plasma Desktop</em></div>

<h4>
Weitere Änderungen
</h4>
<p align="justify">

Wie bereits angedeutet sind die oben genannten Änderungen und Verbesserungen die, die KDE Desktop Workspace, die KDE Anwendungen und das KDE Anwendungs-Entwicklungs Framework erfahren haben. Eine erweiterte, aber trotzdem momentan noch nicht komplette Liste, kann in dem <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">KDE SC 4.4 Zukunftsplan</a> auf der <a href="http://techbase.kde.org">TechBase</a> gefunden werden. Informationen über Anwendungen die von der KDE Gemeinschaft außerhalb der KDE SC entwickelt wurden können auf der <a href="http://kde-apps.org">kde-apps</a> Homepage gefunden werden.
</p>


<h4>
    Verbreite das Wort und sehe was passiert
</h4>
<p align="justify">
Die KDE Gemeinschaft möchte jeden ermutigen KDE in die Welt zu tragen. <strong>Erzähl deine Geschichte</strong> in Blogs und benutze Kanäle wie  digg, reddit, twitter, identi.ca um zu zeigen wie du zu dem KDE Projekt stehst. Lade Screenshots auf Facebook, FlickR, ipernity, Picasa & Co. und veröffentliche diese in Foren oder Chats. Erstelle Videos und lade sie auf YouTube, Blip.tv, Vimeo, MyVideo.de oder andere Webseiten und teile sie mit deinen Freunden. Vergiss nicht alles mit <em><strong>kde</strong> zu taggen/keyworden</em> damit andere es einfahc haben all das zu finden. Das KDE Team schenkt dir diese Software und wie du es geschenkt bekommst kannst du KDE und anderen helfen indem du es verteilst oder Informationen darüber verbreitest.
</p>

<p align="justify">
You can follow what is happening around the KDE SC 4.4 release on the social web live on
the brand-new <a href="http://buzz.kde.org"><strong>KDE Community livefeed</strong></a>. This site aggregates what happens on
identi.ca, twitter, youtube, flickr, picasaweb, blogs and many other social networking sites
in real-time. The livefeed can be found on <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>


<h4>
  KDE SC 4.4.0 installieren
</h4>
<p align="justify">
 KDE, inklusive all seiner Programmbibliotheken und Anwendungen, ist gratis unter freien Softwarelizenzen verfügbar. 
KDE läuft auf verschiedenen Betriebssystemen wie GNU/Linux oder anderen Unix-ähnlichen Systemen sowie auf Microsoft Windows (<a href="http://windows.kde.org">KDE auf Windows</a>) oder Mac OS X (<a href="http://mac.kde.org/">KDE auf Mac</a>). KDE wird unter Linux allerdings nicht als Binär-Paket angeboten sondern es wird empfohlen eine Distribution zu benutzen die KDE anbietet oder schon vorinstalliert hat. 

<br />
  KDE kann im Quelltext und in zahlreichen Binärformaten unter <a
href="http://download.kde.org/stable/4.4.0/">http://download.kde.org</a> heruntergeladen werden, außerdem gibt es es auch auf <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>. Auch in allen <a href="http://www.kde.org/download/distributions.php">großen GNU/Linux- und UNIX-System</a>, die heutzutage angeboten werden ist KDE enthalten.
</p>
<p align="justify">
  <em>Paket-Hersteller</em>.
  Einige Linux- und Unix-Betriebssystemhersteller haben freundlicherweise Binärpakete von KDE 4.2.0 für einige Versionen ihrer Distributionen erstellt. In einigen anderen Fällen wurde dies von freiwilligen Mitarbeitern erledigt. Manche dieser Binärpakete können gratis von den Servern des KDE-Projekts (<a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">http://download.kde.org</a>) heruntergeladen werden. Zusätzliche Binärpakete, sowie Aktualisierungen der bereits verfügbaren Pakete werden höchstwahrscheinlich in den kommenden Wochen verfügbar sein.
</p>

<p align="justify">
  <a name="package_locations"><em>Wo finde ich fertige Pakete?</em></a>.
  Für eine aktuelle Liste von verfügbaren Binärpaketen, über die das KDE-Projekt informiert wurde, werfen Sie bitte einen Blick auf die <a href="/info/4.4.0.php">KDE-4.4.0-Infoseite</a>.
</p>

<h4>
  KDE SC 4.4.0 Kompilieren
</h4>
<p align="justify">
  <a name="source_code"></a>
  Der vollständige Quelltext von KDE 4.4.0 kann <a
href="http://download.kde.org/stable/4.4.0/src/">frei heruntergeladen werden</a>.
Informationen und Anweisungen zum Kompilieren und Installieren von KDE 4.4.0 finden Sie auf der <a href="/info/4.4.0.php#binary">KDE-4.4.0-Infoseite</a>.
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
