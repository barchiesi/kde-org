<?php

  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = 'plasma2tp';
  $page_title = i18n_noop("Plasma 2 Technology Preview and Plasma Media Center 1.2.0");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<p>
<font size="+1">
<?php i18n("December 20, 2013. The KDE Community is proud to announce two milestones of the Plasma Workspaces.
<ul>
    <li><strong><a href=\"http://dot.kde.org/2013/12/20/plasma-2-technology-preview\">Plasma 2 Technology Preview</a></strong> -- A first glance at the evolution of the Plasma Workspaces</li>
    <li><strong><a href=\"http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas\">Plasma Media Center 1.2.0</a></strong> -- Many new features and improvements in Plasma's Media Center user experience.</li>
</ul>
");?>
</font>
<h2><a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview"><img src="images/plasma2tp-desktop-with-stuff.png" class="app-icon" alt="<?php i18n("Plasma 2 Technology Preview");?>"/> <?php i18n("Plasma 2 Technology Preview");?></a></h2>
<?php i18n("KDE's Plasma Team presents a first glance at the evolution of the Plasma Workspaces. <strong>Plasma 2 Technology Preview</strong> demonstrates the current development status. The Plasma 2 user interfaces are built using QML and run on top of a fully hardware accelerated graphics stack using Qt5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma 2's converged workspace shell allows you to run and switch between user interfaces for different form factors, and makes the workspace adaptable to the given target device. The workspace demonstrated in this technology preview is Plasma Desktop, an incremental evolution to known desktop and laptop paradigms. The user experience aims at keeping existing workflows intact, while providing incremental visual and interactive improvements. <a href=\"http://dot.kde.org/2013/12/20/plasma-2-technology-preview\">More info...</a>");?>
</p>


<p>
<h2><a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas"><img src="images/plasma-mediacenter.png" class="app-icon" alt="<?php i18n("Plasma Media Center 1.2.0");?>"/> <?php i18n("Plasma Media Center 1.2.0");?></a></h2>
<?php i18n("The KDE community has a Christmas gift for you! We are happy to announce the release of KDE's <strong>Plasma Media Center 1.2.0</strong>  - your first stop for media and entertainment created by the Elves at KDE. We have designed it to provide an easy and  comfortable way to watch your videos, browse your photo collection and listen to your music, all in one place. New in Plasma Media Center 1.2.0 is improved navigation in music mode, fetching of album covers, picture previews while browsing folders, support for multiple playlist, improved key bindings and new artwork.  <a href=\"http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas\">More info...</a>");?>

</p>

<h2><?php i18n("Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.");?>
</p>
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/plasma2tp&amp;title=KDE%20releases%20Plasma%202%20Technology%20Preview" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/plasma2tp/" data-text="#KDE releases Plasma 2 Technology Preview →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/plasma2tp/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2Fplasma2tp%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/plasma2tp/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://www.flickr.com/photos/tags/kdeplasma2tp"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kdeplasma2tp"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kdeplasma2tp"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h4><?php i18n("Support KDE");?></h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='http://jointhegame.kde.org/'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
