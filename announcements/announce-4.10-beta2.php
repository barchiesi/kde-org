<?php
  $page_title = "KDE Ships Second Beta of Plasma Workspaces, Applications and Platform 4.10";
  $site_root = "../";
  include "header.inc";
?>

<p align="justify">


December 4, 2012. Today KDE released the second beta for its renewed Workspaces, Applications, and Development Platform. Thanks to the feedback from the first beta, KDE already improved the quality noticably. Further polishing new and old functionality will lead to a rock-stable, fast and beautiful release in January, 2013. One outstanding freeze is the artwork freeze, which is planned to bring an updated look to Plasma workspaces.

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.10-beta1.png"><img src="announce-4.10-beta1_thumb.png" align="center" width="640" alt="Plasma Desktop with the Dolphin file manager" title="Plasma Desktop with the Dolphin file manager" /></a>
<br />
<em>Plasma Desktop with Dolphin</em>
</div>



Highlights of 4.10 include, but are not limited to:

<ul>
    <li>
    <strong>Qt Quick in Plasma Workspaces</strong> -- Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE's extensions on top of QtQuick allow deeper integration with the system and more powerful apps and Plasma components. Plasma Containments can now be written in QtQuick. Various Plasma widgets have been rewritten in QtQuick, notably the system tray, pager, notifications, lock & logout, weather and weather station, comic strip and calculator plasmoids. Many performance, quality and usability improvements make Plasma Desktop and Netbook workspaces easier to use.
    </li>
    <li>
    <strong>New Screen Locker</strong> -- A new screen locking mechanism based on QtQuick brings more flexibility and security to Plasma Desktop.
    </li>
    <li>
    <strong>Animated Wallpapers</strong> -- Thanks to a new QtQuick-based wallpaper engine, animated wallpapers are now much easier to create.
    </li>
    <li>
    <strong>Improved Zooming in Okular</strong> -- A technique called tiled rendering allows Okular to zoom in much further while reducing memory consumption. Okular Active, the touch-friendly version of the powerful document reader is now part of KDE SC.
    </li>
    <li>
    <strong>Faster indexing</strong> -- Improvements in the Nepomuk semantic engine allow faster indexing of files. The new Tags kioslave allows users to browse their files by tags in any KDE-powered application.
    </li>
    <li>
    <strong>Color Correction</strong> -- Gwenview, KDE's smart image viewer and Plasma's window manager now support color correction and can be adjusted to the color profile of different monitors, allowing for more natural representation of photos and graphics.
    </li>
    <li>
    <strong>Notifications</strong> -- Plasma's notifications are now rendered using QtQuick, notifications themselves, especially concerning power management have been cleaned up.
    </li>
    <li>
    <strong>New Print Manager</strong> -- Setup of printers and monitoring jobs was improved thanks to a new implementation of the Print Manager.
    </li>
    <li>
    <strong>Kate, KDE's Advanced Text Editor</strong> received multiple improvements regarding user feedback. It is now extensible using Python plugins.
    </li>
    <li>
    <strong>KTouch</strong> -- KDE's touch-typing learning utility has been rewritten and features a cleaner, more elegant user interface.
    </li>
    <li>
    <strong>libkdegames improvements</strong> -- Many parts of libkdegames have been rewritten, porting instructions for 3rd party developers are <a href="http://techbase.kde.org/Projects/Games/Porting_to_libkdegames_v5">available</a>.
    </li>
    <li>
    <strong>KSudoku</strong> now allows printing puzzles.
    </li>
    <li>
    <strong>KJumpingCube</strong> has seen a large number of improvements making the game more enjoyable.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan">4.10 Feature Plan</a>. As with any large number of changes, we need to give 4.10 a good testing in order to maintain and improve the quality and user experience when users install the update.

Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining 4.10 thoroughly and report any bugs you find to <a href="http://bugs.kde.org">bugs.kde.org</a>.


<h3>KDE Software Compilation 4.10 Beta2</h3>
<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.9.90/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing 4.10 Beta2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.10 Beta2 (internally 4.9.90)
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.10_Beta_1_.284.9.90.29">Community Wiki</a>.
</p>

<h4>
  Compiling 4.10 Beta2
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for 4.10 Beta2 may be <a
href="http://download.kde.org/unstable/4.9.90/src/">freely downloaded</a>.
Instructions on compiling and installing 4.9.90
  are available from the <a href="/info/4.9.90.php">4.9.90 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or
become a KDE e.V. supporting member through our new
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
