<?php
  $page_title = "An&uacute;ncio de Lan&ccedil;amento do KDE 4.1";
  $site_root = "../";
  include "header.inc";
?>

Tamb&eacute;m dispon&iacute;vel em:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
A Comunidade do KDE Anuncia o Lan&ccedil;amento do 4.1.0
</h3>

<p align="justify">
  <strong>
    O KDE Lan&ccedil;a um Ambiente de Trabalho e Aplica&ccedil;&otilde;es Dedicadas a Uwe Thiem
  </strong>
</p>

<p align="justify">
29 de Julho de 2008.
A <a href="http://www.kde.org/">Comunidade do KDE</a> lan&ccedil;ou hoje o KDE 4.1.0.
Esta vers&atilde;o &eacute; o segundo grande grupo de funcionalidades disponibilizadas para
a s&eacute;rie do KDE 4, contendo novas aplica&ccedil;&otilde;es e potencialidades desenvolvidas
sobre os pilares do KDE4. O KDE 4.1 &eacute; a primeira vers&atilde;o do KDE4 a conter o
pacote de Gest&atilde;o de Informa&ccedil;&atilde;o Pessoal KDE-PIM, com o cliente de e-mail KMail,
o organizador KOrganizer, o Akregator, que &eacute; o leitor de fontes de RSS,
o KNode, usado para ler not&iacute;cias em f&oacute;runs e muitos outros componentes
integrados na consola do Kontact.
Para al&eacute;m disso, o novo ambiente de trabalho Plasma, que foi introduzido no
KDE 4.0, amadureceu ao ponto em que pode substituir o ambiente do KDE 3 para
os utilizadores mais casuais. Como aconteceu na vers&atilde;o anterior, muito do tempo
foi dedicado &agrave; melhoria da plataforma e das bibliotecas subjacentes em que o
KDE se apoia.
<br />
Dirk M&uuml;ller, um dos gestores de vers&otilde;es do KDE, providencia alguns
n&uacute;meros: <em>"Ocorreram 20803 actualiza&ccedil;&otilde;es, feitas entre o KDE 4.0 e o 4.1,
em conjunto com 15432 actualiza&ccedil;&otilde;es de tradu&ccedil;&otilde;es. Foram feitas assim quase
35000 modifica&ccedil;&otilde;es nas vers&otilde;es de trabalho, tendo algumas sido reunidas no
KDE 4.1, sendo que estas tamb&eacute;m ainda n&atilde;o foram sequer tidas em conta."</em>
M&uuml;ller diz-nos tamb&eacute;m que a equipa de administra&ccedil;&atilde;o de sistemas do KDE
criou 166 contas novas para colaboradores no servidor de SVN do KDE.

<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"O ambiente de trabalho do KDE 4.1", "405");
?>

</p>
<p>
<strong>As melhorias-chave no KDE 4.1 s&atilde;o:</strong>
<ul>
    <li>O pacote KDE-PIM est&aacute; de volta</li>
    <li>O Plasma vai amadurecendo</li>
    <li>Muitas aplica&ccedil;&otilde;es e plataformas novas ou melhoradas</li>
</ul>

</p>


<h3>
  <a name="changes">Em mem&oacute;ria de: Uwe Thiem</a>
</h3>
<p align="justify">
A comunidade do KDE dedica esta vers&atilde;o a Uwe Thiem, um colaborador de longa
data do KDE que acabou por falecer recentemente, ap&oacute;s problemas renais
repentinos. A morte de Uwe veio de forma totalmente inesperada e foi um
choque para os seus colaboradores e amigos. O Uwe, praticamente at&eacute; ao fim
dos seus dias da sua vida, contribuiu para o KDE, n&atilde;o s&oacute; a n&iacute;vel de
programa&ccedil;&atilde;o. O Uwe desempenhou tamb&eacute;m um papel importante na educa&ccedil;&atilde;o
dos utilizadores em &Aacute;frica, a n&iacute;vel do Software Livre. Com a morte repentina
de Uwe, o KDE perdeu uma parte importante da sua comunidade e um amigo.
Os nossos pensamentos destinam-se &agrave; sua fam&iacute;lia e aos seus entes queridos.</p>


<h3>
  <a name="changes">Passado, presente e futuro</a>
</h3>
<p align="justify">
Embora o KDE 4.1 pretenda ser a primeira vers&atilde;o adequada &agrave; adop&ccedil;&atilde;o pr&eacute;via
de utilizadores, algumas das funcionalidades a que estava habituado no KDE
3.5 ainda n&atilde;o est&atilde;o implementadas.
A equipa do KDE est&aacute; a trabalhar sobre estas e luta para que estas fiquem
dispon&iacute;veis numa das pr&oacute;ximas vers&otilde;es. Embora n&atilde;o exista uma garantia que
cada uma das funcionalidades do KDE 3.5 venha a ser implementada, o KDE 4.1
j&aacute; oferece um ambiente poderoso e rico em funcionalidades de trabalho.
<br />
Repare que algumas op&ccedil;&otilde;es na interface foram movidas para um local com base
no contexto dos dados que manipulam, por isso certifique-se bem antes de
comunicar que algo desapareceu a n&iacute;vel de funcionalidades.<p />
O KDE 4.1 &eacute; um enorme passo em frente na s&eacute;rie do KDE 4, e espera-se que
imponha o ritmo para desenvolvimentos futuros. O KDE 4.2 espera a sua
conclus&atilde;o para Janeiro de 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"O KDE PIM est&aacute; de volta", "305");
?>

<h3>
  <a name="changes">Melhorias</a>
</h3>
<p align="justify">
Enquanto se estabilizaram as novas plataformas no KDE 4.1, foi aplicada mais
&ecirc;nfase nas partes vis&iacute;veis para o utilizador. Poder&aacute; obter uma lista das
melhorias no KDE 4.1. Poder&aacute; encontrar informa&ccedil;&atilde;o mais completa nos
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Objectivos da Vers&atilde;o KDE 4.1</a>
e na p&aacute;gina mais detalhada com o
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">Planeamento de Funcionalidades do 4.1</a>.
</p>

<h4>
  Para os utilizadores
</h4>
<p align="justify">

<ul>
    <li>
        O <strong>KDE-PIM</strong> est&aacute; de volta com o 4.1, contendo as
	aplica&ccedil;&otilde;es necess&aacute;rias para a sua informa&ccedil;&atilde;o e comunica&ccedil;&atilde;o pessoais.
	O KMail como cliente de e-mail, o KOrganizer como componente de
	planeamento e agenda, o Akregator como leitor de fontes RSS, entre
	outros, est&atilde;o agora outra vez dispon&iacute;veis com a apar&ecirc;ncia do KDE 4.
        reader and others are now available again in KDE 4 look.
    </li>
    <li>
        O <strong>Dragon Player</strong>, um leitor de v&iacute;deo f&aacute;cil de usar, entra em cena
    </li>
    <li>
        O <strong>Okteta</strong> &eacute; o novo editor hexadecimal bem integrado e rico em funcionalidades
    </li>
    <li>
        O <strong>Step</strong>, um emulador de f&iacute;sica, torna a aprendizagem de f&iacute;sica engra&ccedil;ada e simples
    </li>
    <li>
        O <strong>KSystemLog</strong> ajuda-o a manter um registo do que se passa no seu sistema
    </li>
    <li>
        <strong>Novos jogos</strong>, como o KDiamond (um clone do Bejeweled), o Kollision, o KBreakOut e o Kubrick tornam irresist&iacute;vel uma pausa no seu trabalho
    </li>
    <li>
        O <strong>Lokalize</strong> ajuda os tradutores a tornarem o KDE 4
	dispon&iacute;vel na sua l&iacute;ngua (se n&atilde;o estiver j&aacute; entre as cerca de 50 que
	o KDE 4 j&aacute; suporta)
    </li>
    <li>
        O <strong>KSCD</strong>, o seu leitor de CDs, foi ressuscitado
    </li>
</ul>

As respostas &agrave;s perguntas mais comuns foram recolhidas na <a href="http://www.kde.org/users/faq.php">FAQ do Utilizador do KDE4</a>,
que tamb&eacute;m &eacute; uma boa leitura, caso queira aprender algo mais sobre o KDE4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"Novo mecanismo de selec&ccedil;&atilde;o do Dolphin", "483");
?>

<ul>
    <li>
        <strong>Dolphin</strong>, o gestor de ficheiros do KDE, tem uma nova
	vista em &aacute;rvore na janela principal, sendo tamb&eacute;m novidade o suporte
	para p&aacute;ginas separadas. Uma nova e inovadora selec&ccedil;&atilde;o ao carregar uma
	vez o bot&atilde;o do rato, permite uma experi&ecirc;ncia de utilizador mais
	consistente, sendo que as op&ccedil;&otilde;es de c&oacute;pia e de contexto tornam estas
	ac&ccedil;&otilde;es ainda mais acess&iacute;veis. Obviamente, o Konqueror continua
	dispon&iacute;vel como alternativa para o Dolphin, tirando partido das
	funcionalidades acima apresentadas.
	[<a href="#screenshots-dolphin">Imagens do Dolphin</a>]
    </li>
    <li>
        O navegador Web do KDE <strong>Konqueror</strong> tem agora suporte
	para abrir de novo as p&aacute;ginas e janelas j&aacute; fechadas, sendo que se
	desloca tamb&eacute;m mais suavemente pelas p&aacute;ginas Web.
    </li>
    <li>
        O <strong>Gwenview</strong>, o visualizador de imagens do KDE, tem
	agora uma vista em ecr&atilde; completo, uma barra de miniaturas para aceder
	facilmente &agrave;s outras fotografias, um sistema para Desfazer inteligente
	e o suporte para classificar imagens.
	[<a href="#screenshots-gwenview">Imagens do Gwenview</a>]
    </li>
    <li>
        <strong>KRDC</strong>, o cliente de liga&ccedil;&otilde;es remotas do KDE, agora
	detecta os ambientes de trabalho remotos na rede local automaticamente,
	usando o protocolo ZeroConf.
    </li>

    <li>
        <strong>Marble</strong>, o globo para o ambiente do KDE, agora
	integra-se com o
	<a href="http://www.openstreetmap.org/">OpenStreetMap</a>, para que
	voc&ecirc; possa encontrar o seu caminho, usando os mapas livres.
	[<a href="#screenshots-marble">Imagens do Marble</a>]
    </li>
    <li>
        O <strong>KSysGuard</strong> agora suporta a monitoriza&ccedil;&atilde;o do resultado
	dos processos no 'stdout' ou as aplica&ccedil;&otilde;es em execu&ccedil;&atilde;o, pelo que n&atilde;o
	&eacute; mais necess&aacute;rio reiniciar as suas aplica&ccedil;&otilde;es a partir de um
	terminal quando quiser saber o que se passa.
    </li>
    <li>
        O gestor de janelas por composi&ccedil;&atilde;o <strong>KWin</strong> tem um
	conjunto de funcionalidades extendidas e estabilizadas. O efeito
	novo de mudan&ccedil;a de janelas por Capas e as famosas "janelas tr&eacute;mulas"
	foram adicionados. [<a href="#screenshots-kwin">Imagens do KWin</a>]
    </li>
    <li>
        A configura&ccedil;&atilde;o do painel do <strong>Plasma</strong> foi extendida.
	O novo controlador do painel facilita a personaliza&ccedil;&atilde;o do seu painel,
	dando logo uma reac&ccedil;&atilde;o visual. Poder&aacute; tamb&eacute;m adicionar pain&eacute;is e
	coloc&aacute;-los em diferentes extremos dos seus ecr&atilde;s. A nova 'applet'
	de visualiza&ccedil;&atilde;o de pastas permite-lhe guardar ficheiros no seu ecr&atilde;
	(de facto, oferece uma vista de uma pasta no seu sistema). Poder&aacute;
	colocar zero, uma ou mais vistas de pastas no seu ecr&atilde;, oferecendo
	uma acesso mais simples e flex&iacute;vel aos ficheiros em que est&aacute;
	a trabalhar.
        [<a href="#screenshots-plasma">Imagens do Plasma</a>]
    </li>
</ul>
</p>


<h4>
  Para os programadores
</h4>
<p align="justify">

<ul>
    <li>
        A plataforma de armazenamento de PIM <strong>Akonadi</strong> oferece
	uma forma eficiente de guardar e obter dados de e-mail e de contactos
	por v&aacute;rias aplica&ccedil;&otilde;es. O
	<a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> suporta
	a pesquisa por dados e notifica modifica&ccedil;&otilde;es nas aplica&ccedil;&otilde;es que os
	usam.
    </li>
    <li>
        As aplica&ccedil;&otilde;es do KDE podem ser escritas em Python e Ruby. Estas
	<strong>interfaces para linguagens</strong> s&atilde;o
        <a href="http://techbase.kde.org/Development/Languages">consideradas</a>
	est&aacute;veis, maduras e adequadas para os programadores de aplica&ccedil;&otilde;es.
    </li>
    <li>
        A <strong>Libksane</strong> oferece um acesso simples &agrave;s aplica&ccedil;&otilde;es
	de digitaliza&ccedil;&atilde;o de imagens, como a nova aplica&ccedil;&atilde;o de digitaliza&ccedil;&atilde;o
	Skanlite.
    </li>
    <li>
        Um sistema de <strong>&iacute;cones emotivos</strong> partilhados, que &eacute;
	usado pelo KMail e pelo Kopete.
    </li>
    <li>
        Novas infra-estruturas multim&eacute;dia do <strong>Phonon</strong>
	para o GStreamer, QuickTime e DirectShow9, o que melhora
        o suporte multim&eacute;dia do KDE no Windows e no Mac OS.
    </li>

</ul>
</p>

<h3>
  Novas plataformas
</h3>
<p align="justify">
<ul>
    <li>
        O suporte no KDE para o <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>
	est&aacute; neste momento a ser resolvido. O KDE funciona na sua grande
	maioria no OSOL, ainda que existam alguns erros cr&iacute;ticos por resolver.
    </li>
    <li>
        Os programadores em <strong>Windows</strong> t&ecirc;m a possibilidade
	de <a href="http://windows.kde.org">obter</a> vers&otilde;es de antevis&atilde;o
	das aplica&ccedil;&otilde;es do KDE para a sua plataforma. As bibliotecas j&aacute;
	est&atilde;o relativamente est&aacute;veis, ainda que nem todas as funcionalidades
	do 'kdelibs' j&aacute; estejam dispon&iacute;veis no Windows. Algumas aplica&ccedil;&otilde;es
	poder&atilde;o correr bastante bem no Windows, enquanto outras n&atilde;o.
    </li>
    <li>
        O <strong>Mac OSX</strong> &eacute; tamb&eacute;m outra plataforma nova onde o KDE
	est&aacute; a come&ccedil;ar a entrar.
        O <a href="http://mac.kde.org">KDE no Mac</a> ainda n&atilde;o est&aacute; pronto
	para usar em produ&ccedil;&atilde;o. Embora o suporte multim&eacute;dia atrav&eacute;s do Phonon
	j&aacute; esteja dispon&iacute;vel, o suporte de 'hardware' e a integra&ccedil;&atilde;o da
	pesquisa ainda n&atilde;o est&atilde;o terminados.
    </li>
</ul>
</p>


<a name="screenshots-dolphin"></a>
<h3>
  Imagens
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "A nova &aacute;rvore do Dolphin d&aacute;-lhe um acesso mais r&aacute;pido
	       &agrave;s pastas. Repare que est&aacute; desactivado na configura&ccedil;&atilde;o
	       predefinida.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "O Nepomuk permite a marca&ccedil;&atilde;o e classifica&ccedil;&atilde;o no KDE --
	       como tal, no Dolphin.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "A antevis&atilde;o nos &iacute;cones e as barras de informa&ccedil;&atilde;o oferecem
	       uma reac&ccedil;&atilde;o e apresenta&ccedil;&atilde;o geral visuais.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Procure mais facilmente os ficheiros com a barra de
	       filtragem.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "You can browse directories with images with Gwenview. Hover
               actions put common tasks at your fingertips.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "A abertura de ficheiros no seu disco ou na rede &eacute; igualmente
	       simples, gra&ccedil;as &agrave; infra-estrutura do KDE.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "A nova barra de miniaturas permite-lhe mudar de imagens
	       facilmente. Tamb&eacute;m est&aacute; dispon&iacute;vel no modo de ecr&atilde;
	       completo.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "A barra lateral do Gwenview permite o acesso &agrave;s informa&ccedil;&otilde;es
	       adicionais e &agrave;s op&ccedil;&otilde;es de manipula&ccedil;&atilde;o de imagens.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "O globo Marble para o ambiente de trabalho.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "A nova integra&ccedil;&atilde;o do OpenStreetMap no Marble tamb&eacute;m
	       permite a informa&ccedil;&atilde;o sobre transportes p&uacute;blicos.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "A grelha de ecr&atilde;s do KWin visualiza o conceito de ecr&atilde;s
	       virtuais e facilita a recorda&ccedil;&atilde;o de onde deixou a janela
	       que anda a procurar.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "A mudan&ccedil;a de Capas faz com que a mudan&ccedil;a de janelas com
	       o Alt+Tab seja cativante. Pod&ecirc;-la-&aacute; escolher na configura&ccedil;&atilde;o
	       dos efeitos de ecr&atilde; do KWin.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "O KWin tamb&eacute;m tem agora as 'janelas tr&eacute;mulas' obrigat&oacute;rias
	       (se bem que est&aacute; desactivado por omiss&atilde;o).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "A nova vista de pastas permite-lhe apresentar o conte&uacute;do
	       de pastas arbitr&aacute;rias no seu ecr&atilde;. Largue uma pasta sobre
	       o seu ecr&atilde; desbloqueado, de modo a criar uma nova vista
	       de pastas. Uma vista de pastas n&atilde;o s&oacute; pode mostrar as
	       pastas locais, mas tamb&eacute;m pode lidar com pastas na rede.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "O novo controlador do painel permite-lhe mudar de posi&ccedil;&atilde;o
	       e de tamanho os seus pain&eacute;is. Poder&aacute; tamb&eacute;m mudar a posi&ccedil;&atilde;o
	       das 'applets' no painel, se as arrastar para a sua posi&ccedil;&atilde;o
	       nova.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "Com o KRunner, poder&aacute; iniciar aplica&ccedil;&otilde;es, enviar e-mails
	       directamente para os seus amigos e desempenhar muitas outras
	       pequenas tarefas.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "O lan&ccedil;ador de aplica&ccedil;&otilde;es Kickoff do Plasma levou agora
	       uma remodela&ccedil;&atilde;o visual.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Poder&aacute; escolher entre o lan&ccedil;ador de aplica&ccedil;&otilde;es Kickoff
	       e o estilo de menu cl&aacute;ssico", "344");
?>

</p>

<h4>
  Problemas conhecidos
</h4>
<p align="justify">
<ul>
    <li>Os utilizadores de placas <strong>NVidia</strong> com o controlador
    bin&aacute;rio poder&atilde;o ter problemas de performance na mudan&ccedil;a de janelas e no
    seu dimensionamento. Os engenheiros da NVidia foram notificados destes
    problemas. Contudo, ainda n&atilde;o foi lan&ccedil;ado nenhum controlador da NVidia
    com uma correc&ccedil;&atilde;o dos mesmos. Poder&aacute; encontrar mais informa&ccedil;&otilde;es sobre
    como melhorar a performance gr&aacute;fica na
    <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
    ainda que em &uacute;ltima inst&acirc;ncia se tenha que confiar na resolu&ccedil;&atilde;o do
    controlador por parte da NVidia.</li>
</ul>

</p>



<!-- ==================================================================================== -->
<h4>
  Obtenha-o, execute-o, teste-o
</h4>
<p align="justify">
  Os volunt&aacute;rios da comunidade e os distribuidores de SO's Linux/UNIX
  t&ecirc;m gentilmente oferecido pacotes bin&aacute;rios do KDE 4.1.0 para algumas
  distribui&ccedil;&otilde;es de Linux, bem como para o Mac OS X e Windows.
  Verifique o seu sistema de gest&atilde;o de aplica&ccedil;&otilde;es do seu sistema operativo.
</p>
<h4>
  Compilar o KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>C&oacute;digo-fonte</em>.
  O c&oacute;digo-fonte completo do KDE 4.1.0 poder&aacute; ser <a
  href="http://www.kde.org/info/4.1.0.php">obtido de forma livre</a>
  por completo.
  As instru&ccedil;&otilde;es de compila&ccedil;&atilde;o e instala&ccedil;&atilde;o do KDE 4.1.0
  est&atilde;o dispon&iacute;veis na <a href="/info/4.1.0.php">P&aacute;gina de Informa&ccedil;&atilde;o
  do KDE 4.1.0</a> ou ainda na
  <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de Imprensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
