<?php
  $page_title = "KDE 4.1 lanseringssida";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE 4.1 lanserad
</h3>

<p align="justify">
  <strong>
KDE-projektet lanserar förbättrat skrivbord och applikationer till minne av Uwe Thiem.</strong>
</p>

<p align="justify">
29 juli, 2008 (INTERNET).
<a href="http://www.kde.org/">KDE Projektet</a> tillkännager stolt KDE 4.1. Det här är den andra
funktionella versionen i KDE4-serien och innehåller ett antal nya applikationer och ny
funktionalitet baserade på KDE4:s grundpelare. KDE 4.1 är den första versionen i KDE4-serien där PIM-sviten KDE-PIM
ingår. KDE-PIM består av epostklienten KMail, planeringsverktyget KOrganizer, RSS-läsaren Akregator, nyhetsklienten
KNode och flera andra komponenter, alla integrerade i Kontact som är ett skal för alla KDE-PIM applikationer. Det nya
skrivbordshanteraren Plasma har vuxit till sig ordentligt, så pass att det är redo att ersätta KDE3:s skrivbord för
de flesta vanliga användare. Som i alla föregående versioner av KDE har mycket tid lagts ner på att förbättra
de underliggande ramverken och biblioteken.
<br />
Dirk M&uuml;ller, en av KDE:s release managers har sammanställt följande siffror: <em>"Totalt har 20803 incheckningar och
15432 översättningsincheckningar gjorts under arbetet med KDE 4.1. Utöver detta har nästan 35000 incheckningar skett i olika
arbetsgrenar av vilka en del har mergats in i KDE 4.1. Enligt M&uuml;ller har KDE:s sysadmins skapat upp 166 nya SVN-konton för
utvecklare under arbetet med KDE 4.1.
</p>


<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"The KDE 4.1 desktop", "405");
?>

</p>
<p>
<strong>De viktigaste nyheterna i KDE 4.1:</strong>
<ul>
    <li>KDE-PIM är tillbaka</li>
    <li>Plasma har mognat</li>
    <li>Många nya och förbättrade applikationer och ramverk</li>
</ul>

</p>


<h3>
  <a name="changes">In memoriam: Uwe Thiem</a>
</h3>
<p align="justify">
KDE-projektet dedikerar den här versionen till Uwe Thiem, en veteran i projektet
som nyligen avled av plötslig leversvikt. Uwes död var helt oväntad och kom som
en chock för hans kollegor. Ända fram till sin död bidrog Uwe till KDE, inte bara
genom programmering, Uwe spelade en viktig roll i spridningen av Fri Mjukvara i Afrika.
Uwes död har tagit en ovärderlig del av communityn och en vän från KDE-projektet. Våra
tankar är hos hans familj och de som han lämnade kvar.
</p>


<h3>
  <a name="changes">Vad har hänt och vad planeras?</a>
</h3>
<p align="justify">
KDE 4.1 är den första versionen av KDE4 som lämpar sig för de slutanvändare som vill
ha det senaste, fortfarande fattas en del funktioner från KDE 3.5 men teamet jobbar med
dom och strävar efter att bli klar med dom till någon av de följande versionerna.
Dock finns ingen garanti att exakt alla funktioner i KDE 3.5 kommer att bli implementerade
i KDE4. KDE 4.1 tillhandahåller redan en kraftfull och funktionell arbetsmiljö.<br/>

Några av inställningarna i användargränssnittet har flyttats till platser som bättre
passar den data de manipulerar så om du upptäcker något som fattas, ta en närmare titt innan
du rapporterar ett fel.<p/>
KDE 4.1 är ett stort steg framåt i KDE4-serien och sätter standarden för framtida utveckling.
KDE 4.2 förväntas bli klart i januari 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"KDE PIM är tillbaka", "305");
?>

<h3>
  <a name="changes">Förbättringar</a>
</h3>
<p align="justify">
Under arbetet med att stabilisera de nya ramverken i KDE 4.1 flyttades fokus till
mer användarsynliga delar. Mer komplett information om vad som hänt finns på
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 Release Goals</a>
och i utförligare form på
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">4.1 Feature Plan</a>.
</p>

<h4>
  För användare
</h4>
<p align="justify">

<ul>
    <li>
        <strong>KDE-PIM</strong> är tillbaka, med alla de nödvändiga applikationerna
	för att hålla reda på din personliga information och kommunikation. KMail som epostklient,
	KOrganizer som planerare, Akregator som RSS-läsare med flera finns nu tillgängliga igen i
	KDE4-utförande.
    </li>
    <li>
        <strong>Dragon Player</strong>, en enkel och lättanvänd videospelare gör entré.
    </li>
    <li>
        <strong>Okteta</strong>, den nya välintegrerade och funktionella hexeditorn.
    </li>
    <li>
        <strong>Step</strong>, fysikemulatorn, gör fysiklektionerna roliga och enkla.
    </li>
    <li>
        <strong>KSystemLog</strong>, hjälper dig att hålla reda på vad som händer i ditt system.
    </li>
    <li>
        <strong>Nya spel</strong> som KDiamond (en klon av bejeweled), Kollision, KBreakOut
        och Kubrick gör pauserna i arbetet åtråvärda.
    </li>
    <li>
        <strong>Lokalize</strong>, hjälper översättare att göra KDE4 tillgängligt på ditt språk
        (om det inte redan finns bland de drygt 50 språken redan).
    </li>
    <li>
        <strong>KSCD</strong>, CD-spelaren har återvänt.
    </li>
</ul>

Svar på vanliga frågor finns på
<a href="http://www.kde.org/users/faq.php">KDE4 End User FAQ</a>, som också
är väldigt läsvärd om du vill veta mer om KDE4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"Dolphins nya valmekanism", "483");
?>

<ul>
    <li>
        <strong>Dolphin</strong>, KDEs filhanterare har fått en ny trädvy samt stöd för flikar.
	Tack vare en ny och innovativ metod för att välja filer i en-klicksläge samt kopiera-till
	och flytta-till genvägar har Dolphin blivit både effektivare och enklare. Konqueror finns
	självklart kvar som alternativ till Dolphin och drar fördel av de flesta av ovan nämnda
	förbättringar. [<a href="#screenshots-dolphin">Skärmdumpar av Dolphin</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, KDEs webbläsare har fått stöd för att återöppna redan stängda
	fönster och flikar och har även fått förbättrat scrollstöd.
    </li>
    <li>
        <strong>Gwenview</strong>, KDEs bildvisare har fått ett nytt fullskärmsläge en minibildvisare, ett
	smart ångrasystem samt stöd för att betydsätta bilder.
	[<a href="#screenshots-gwenview">Skärmdumpar av Gwenview</a>]
    </li>
    <li>
        <strong>KRDC</strong>, KDEs fjärrskrivbordsklient har fått stöd för att hitta fjärrskrivbord på det
	lokala nätverket via Zeroconf.
    </li>

    <li>
        <strong>Marble</strong>, KDEs jordglob integrerar nu med <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a> så du kan hitta vägen
	med Fria kartor. [<a href="#screenshots-marble">Skärmdumpar av Marble</a>]
    </li>
    <li>
        <strong>KSysGuard</strong>, har fått stöd för att övervaka utdata från processer eller
	startade applikationer så att man inte behöver starta upp en applikation från en terminal för att
	se vad som händer.
    </li>
    <li>
        <strong>KWin</strong>, fönsterhanteraren har blivit stabilare och utökad. Nya effekter som Coverswitch
	fönsterbytare och de berömda "dallrande fönster" har tillkommit. [<a href="#screenshots-kwin"> Skärmdumpar av KWin</a>]
    </li>
    <li>
        <strong>Plasma</strong>s panelkonfiguration har utökats. Den nya kontrollen
	gör det enkelt att ändra storlek på panelen med direkt visuell återkoppling.
	Det har även tillkommit stöd för flera flyttbara paneler. Den nya mappvisarappleten
	gör det möjligt att visa vilken mapp på ditt eller ett fjärrsystem som helst på skrivbordet.
        [<a href="#screenshots-plasma">Skärmdumpar av Plasma</a>]
    </li>
</ul>
</p>


<h4>
  För utvecklare
</h4>
<p align="justify">

<ul>
    <li>
        Ramverket för PIM-data <strong>Akonadi</strong> ger enkel åtkomst till email och kontaktdata över
	flera applikationer. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> har stöd för att söka
	genom data och notifierar applikationer om förändringar.
    </li>
    <li>
	KDE-applikationer kan nu skriva i både Python och Ruby.
	<a href="http://techbase.kde.org/Development/Languages">Stödet</a> för dessa <strong>språkbindningar</strong>
	är stabilt och moget och de lämpar sig mycket väl för applikationsutvecklare.
    </li>
    <li>
        <strong>Libksane</strong> ger enkel åtkomst till bildscanningapplikationer, exempelvis den nya scanningapplikationen Skanlite.
    </li>
    <li>
	Ett delat system för att konfigurera <strong>uttryckssymboler</strong> har introducerats vilket för närvarande
	används av KMail och Kopete.
    </li>
    <li>
	Nya <strong>Phonon</strong> implementationer av GStreamer, QuickTime och DirectShow9 utökar KDEs
	multimediastöd på Windows och Mac OS.
    </li>

</ul>
</p>

<h3>
  Nya plattformar
</h3>
<p align="justify">
<ul>
    <li>
        Stödet för <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>
        i KDE håller för närvarande på att färdigställas. Fokus ligger på OSOL, dock återstår några kritiska buggar.
    </li>
    <li>
        <strong>Windows</strong>utvecklare kan <a href="http://windows.kde.org">ladda ner</a> förhandsversioner
        av KDE-applikationer för sin plattform. De underliggande biblioteken är relativt stabila även om inte alla
	funktioner finns för Windows ännu. Vissa applikationer fungerar bra andra mindre bra.
    </li>
    <li>
        <strong>Mac OSX</strong> är ännu en ny plattform för KDE.
        <a href="http://mac.kde.org">KDE på Mac</a> är dock inte redo för slutanvändare ännu.
	Multimediastöd genom Phonon fungerar men stöd för hårdvara och sökning är inte färdigt.
    </li>
</ul>
</p>


<a name="screenshots-dolphin"></a>
<h3>
  Skärmdumpar
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "Dolphins nya trädvy ge snabbare åtkomst genom kataloger. Notera att det är avstängt i grundutförandet.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk ger taggningsmöjligheter och betygsättning i KDE -- och därmed i Dolphin.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "Förhandvisning och informationsfält ger visuell feedback och översikt.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Hitta dina filer snabbare med filterfältet.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "Man kan bläddra i kataloger med Gwenview och visa dess bilder.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Att öppna filer från din egen hårddisk eller en nätverksenhet är lika enkelt
		tack vare KDEs infrastruktur.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "Det nya minibildsfältet gör det enkelt att växla mellan bilder. Den är också
		tillgänglig i fullskärmsläget.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "Gwenviews sidofält ger tillgång till ytterligare information och bildredigering.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "Marble: skrivbordsjordgloben", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "Marbles nya OpenStreetMap-läge ger även tillgång till transportinformation.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "KWins skrivbordsrutnät ger överblick över de virtuella skrivborden och gör det enklare
		att hitta det där fönstret..", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "Coverswitcher gör Alt+Tab till en riktig läckerhet. Slå på den i KWins inställningar.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "KWin har nu stöd för dallrande fönster (avstängt som standard).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "Den nya mappvisarappleten ger dig möjlighet att visa valfri katalog på skrivbordet.
		Drag och släpp en katalog på ditt upplåsta skrivbord för att skapa en ny mappvisare.
		Självklart går det även bra med mappar på andra maskiner.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "De nya panelinställningarna gör det enkelt att ändra storlek på och flytta
		paneler. Det går även att flytta paneler genom att dra och släppa.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "Med KRunner startar man applikationer, skickar epost och andra små enkla saker.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Kickoff-menyn har fått en ansiktlyftning.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Det går enkelt att byta mellan Kickoff och den klassiska menyn.", "344");
?>

</p>

<h4>
  Kända problem
</h4>
<p align="justify">
<ul>
    <li>Om man har ett <strong>NVidia</strong>kort med den binära drivrutinen från NVidia
    kan man få hastighetsproblem när man byter fönster och ändrar storlek på fönster.
    NVidia är medvetna om problemet men tyvärr har ingen lösning publicerats ännu. Mer information
    om hur man kan förbättra grafikhastigheten finns på
    <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
    men slutligen är det upp till NVidia att lösa problemet..</li>
</ul>

</p>



<!-- ==================================================================================== -->
<h4>
  Hämta, testa, installera
</h4>
<p align="justify">
  Medlemmar i communityn och Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.1.0
  för en del Linuxdistributioner samt för Max OS X och Windows. Konsultera ditt operativsystems
  mjukvaruhanteringssystem.</p>
<h4>
  Kompilera KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Källkod</em>.
  Den kompletta källkoden finns<a
  href="http://www.kde.org/info/4.1.0.php">tillgänglig här</a>.
Instruktioner för att kompilera och installera KDE 4.1.0
  finns tillgängliga på <a href="/info/4.1.0.php">KDE 4.1.0 Info
  Page</a>, eller på <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Presskontakter</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
