<?php
  $page_title = "KDE 2.1.2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<TT>
<P>DATELINE APRIL 30, 2001</P>
<P>FOR IMMEDIATE RELEASE</P> 
</TT>
<H3 ALIGN="center">SECURITY: New KDE Libraries Released</H3>
<P><STRONG>KDE Adds Security and Bug Fixes to Core Libraries</STRONG></P>
<P>April 30, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of kdelibs 2.1.2,
a security and bugfix release of the core KDE libraries.  The other
core KDE packages, including kdebase, have not been updated.  The KDE Project
recommends that all KDE users upgrade to kdelibs 2.1.2 and KDE 2.1.1.</P>
<P>
This release provides the following fixes:
<UL>
<LI><EM>Security fixes</EM>:</LI>
<UL>
<LI><EM>KDEsu</EM>.  The KDEsu which shipped with earlier releases of KDE 2
writes a (very) temporary but world-readable file with authentication
information.  A local user can potentially abuse this behavior to gain
access to the X server and, if KDEsu is used to perform tasks that require
root-access, can result in comprimise of the root account.</LI>
</UL>
<LI><EM>Bug fixes</EM>:</LI>
<UL>
<LI><EM>kio_http</EM>.  Fixed problems with "protocol for http://x.y.z died unexpectedly" and with proxy authentication with Konqueror.</LI>
<LI><EM>kparts</EM>.  Fixed crash in KOffice 1.1 when splitting views.</LI>
<LI><EM>khtml</EM>.  Fixed memory leak in Konqueror.  Fixed minor HTML
rendering problems.</LI>
<LI><EM>kcookiejar</EM>.  Fixed minor problems with HTTP cookies.</LI>
<LI><EM>kconfig</EM>.  Fixed problem with leading/trailing spaces in
configuration values.</LI>
<LI><EM>kdebug</EM>.  Fixed memory leak in debug output.</LI>
<LI><EM>klineedit</EM>.   Fixed problem with klineedit emitting "return
pressed" twice.</LI>
</UL>
</UL>
<P>
For more information about the KDE 2.1 series, please see the
<A HREF="http://www.kde.org/announcements/announce-2.1.1.html">KDE 2.1.1
press release</A> and the <A HREF="http://www.kde.org/info/2.1.1.html">KDE
2.1.1 Info Page</A>, which is an evolving FAQ about the latest stable release.
Information on using anti-aliased fonts with KDE is available
<A HREF="http://dot.kde.org/984693709/">here</A>.
</P>
<P>
<H4>Downloading and Compiling kdelibs 2.1.2</H4>
</P>
<P>
The source package for kdelibs 2.1.2 (including a diff file against 2.1.1) is
available for free download at
<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/src/">http://ftp.kde.org/stable/2.1.2/distribution/src/</A>
or in the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.  KDE 2.1.2 requires
qt-2.2.3, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.4.tar.gz">qt-2.2.4</A>or
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>is recommended (for anti-aliased fonts,
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>and <A HREF="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</A> or
newer is required).
kdelibs 2.1.2 will not work with versions of Qt older than 2.2.3.
</P>
<P>
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for
kdelibs 2.1.2 will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/">http://ftp.kde.org/stable/2.1.2/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you have any questions, please read the
<A HREF="http://dot.kde.org/986933826/">KDE Binary Packages Policy</A>).
</P>
<P>kdelibs 2.1.2 requires qt-2.2.3, the free version of which is available
from the above locations usually under the name qt-x11-2.2.3, although
qt-2.2.4 or qt-2.3.0 is recommended (for anti-aliased fonts,
qt-2.3.0 and XFree 4.0.3 or newer is required).
KDE 2.1.2 will not work with versions of Qt older than 2.2.3.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
<LI><A HREF="http://www.caldera.com/">Caldera</A> eDesktop 2.4: <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Caldera/eDesktop-2.4/">i386</A></LI>
<!--
<LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> stable (2.2):  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-powerpc/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-all/">main</A> directory for common files</LI>
<LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A> 7.2:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Mandrake/7.2/RPMS/">i586</A></LI>
-->
<LI><A HREF="http://www.redhat.com/">RedHat Linux</A>:  7.1:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/RedHat/7.1/i386/">i386</A></LI>
<!--
<UL>
<LI>Wolverine:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</A> directory for common files</LI>
<LI>6.x:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</A> directory for common files</LI>
</UL>
-->
<LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/README">README</A>):
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/sparc/7.1/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.1/">PPC</A></LI>
 
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.0/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.0/">PPC</A>, and <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/s390/">S390</A></LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.4/">i386</A></LI>
<LI>6.3:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.3/">i386</A></LI>
</LI>
</UL>
<LI>Tru64 Systems:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/README.Tru64">README</A>)</LI>
<!--
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</A></LI>
-->
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages may become available over the
coming days and weeks.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
KDE and all its components are available for free under
Open Source licenses from the KDE <A HREF="http://ftp.kde.org/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
As a result of the dedicated efforts of hundreds of translators,
KDE is available in <A HREF="http://i18n.kde.org/teams/distributed.html">34
languages and dialects</A>.  KDE includes the core KDE libraries, the core
desktop environment (including
<A HREF="http://konqueror.kde.org/">Konqueror</A>), developer packages
(including <A HREF="http://www.kdevelop.org/">KDevelop</A>), as well as the
over 100 applications from the other standard base KDE packages
(administration, games, graphics, multimedia, network, PIM and utilities).
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
More information about KDE 2 is available in two
(<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</A>,
<A HREF="http://mandrakesoft.com/~david/OSDEM/">2</A>) slideshow
presentations and on
<A HREF="http://www.kde.org/">KDE's web site</A>, including an evolving
<A HREF="http://www.kde.org/info/2.1.html">FAQ</A> to answer questions about
migrating to KDE 2.1 from KDE 1.x,
<A HREF="http://dot.kde.org/984693709/">anti-aliased font tutorials</A>, a
number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;ra&#x6e;&#x72;&#0111;t&#104;&#064;&#x6b;&#x64;&#101;.&#x6f;rg<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
&#112;our&#64;kde.&#00111;&#x72;g<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#x66;&#097;&#0117;re&#064;&#107;&#x64;e.&#x6f;&#0114;&#103;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#111;no&#x6c;&#x64;&#64;&#00107;d&#x65;.&#x6f;&#x72;&#103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php
  include "footer.inc"
?>
