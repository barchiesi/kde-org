<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.31.0");
  $site_root = "../";
  $release = '5.31.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
February 11, 2017. KDE today announces the release
of KDE Frameworks 5.31.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General");?></h3>

<p>Many modules now have python bindings.</p>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("add support for display_name in categories");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("too many icon changes to list them here");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Enable -Wsuggest-override for g++ &gt;= 5.0.0");?></li>
<li><?php i18n("Pass -fno-operator-names when supported");?></li>
<li><?php i18n("ecm_add_app_icon : ignore SVG files silently when unsupported");?></li>
<li><?php i18n("Bindings: Many fixes and improvements");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Support some of the KNSCore questions using notifications");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix KCompressionDevice (seeking) to work with Qt &gt;= 5.7");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Update most of the examples, drop outdated ones");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix linking on Windows: don't link kentrymaptest to KConfigCore");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Do nothing in ShowMenubarActionFilter::updateAction if there are no menubars");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix Bug 363427 - unsafe characters incorrectly parsed as part of URL (bug 363427)");?></li>
<li><?php i18n("kformat: Make it possible to properly translate relative dates (bug 335106)");?></li>
<li><?php i18n("KAboutData: Document that bug email address can also be a URL");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[IconDialog] Set proper icons group");?></li>
<li><?php i18n("[QuickViewSharedEngine] Use setSize instead of setWidth/setHeight");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Sync KDE4Defaults.cmake from kdelibs");?></li>
<li><?php i18n("Fix HAVE_TRUNC cmake check");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("KEmoticons: use DBus to notify running processes of changes made in the KCM");?></li>
<li><?php i18n("KEmoticons: major performance improvement");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("KIconEngine: Center icon in requested rect");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Add KUrlRequester::setMimeTypeFilters");?></li>
<li><?php i18n("Fix parsing of directories listing on a specific ftp server (bug 375610)");?></li>
<li><?php i18n("preserve group/owner on file copy (bug 103331)");?></li>
<li><?php i18n("KRun: deprecate runUrl() in favor of runUrl() with RunFlags");?></li>
<li><?php i18n("kssl: Ensure user certificate directory has been created before use (bug 342958)");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("Apply filter to proxy immediately");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Make it possible to adopt resources, mostly for system-wide settings");?></li>
<li><?php i18n("Don't fail when moving to the temp directory when installing");?></li>
<li><?php i18n("Deprecate the security class");?></li>
<li><?php i18n("Don't block when running the post-install command (bug 375287)");?></li>
<li><?php i18n("[KNS] Take into account the distribution type");?></li>
<li><?php i18n("Don't ask if we're getting the file in /tmp");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Re-add logging notifications to files (bug 363138)");?></li>
<li><?php i18n("Mark non-persistent notifications as transient");?></li>
<li><?php i18n("Support \"default actions\"");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Don't generate appdata if it's marked as NoDisplay");?></li>
<li><?php i18n("Fix listing when the requested path is absolute");?></li>
<li><?php i18n("fix handling of archives with a folder in them (bug 374782)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("fix minimap rendering for HiDPI envs");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Add methods to hide the reveal password action");?></li>
<li><?php i18n("KToolTipWidget: don't take ownership of the content widget");?></li>
<li><?php i18n("KToolTipWidget: hide immediately if content gets destroyed");?></li>
<li><?php i18n("Fix focus override in KCollapsibleGroupBox");?></li>
<li><?php i18n("Fix warning when destructing a KPixmapSequenceWidget");?></li>
<li><?php i18n("Install also CamelCase forward headers for classes from multi-class headers");?></li>
<li><?php i18n("KFontRequester: Find the nearest match for a missing font (bug 286260)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Allow Tab as being modified by Shift (bug 368581)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Bug reporter: Allow a URL (not just an email address) for custom reporting");?></li>
<li><?php i18n("Skip empty shortcuts on ambiguity check");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Containment Interface] No need for values() as contains() looks up keys");?></li>
<li><?php i18n("Dialog: Hide when focus changes to ConfigView with hideOnWindowDeactivate");?></li>
<li><?php i18n("[PlasmaComponents Menu] Add maximumWidth property");?></li>
<li><?php i18n("Missing icon when connected to openvpn via bluetooth network (bug 366165)");?></li>
<li><?php i18n("Make sure we display enabled ListItem on hover");?></li>
<li><?php i18n("make all heights in the calendar header to be even (bug 375318)");?></li>
<li><?php i18n("fix color styling in network plasma icon (bug 373172)");?></li>
<li><?php i18n("Set wrapMode to Text.WrapAnywhere (bug 375141)");?></li>
<li><?php i18n("update kalarm icon (bug 362631)");?></li>
<li><?php i18n("correctly forward status from applets to containment (bug 372062)");?></li>
<li><?php i18n("Use KPlugin to load Calendar plugins");?></li>
<li><?php i18n("use the highlight color for selected text (bug 374140)");?></li>
<li><?php i18n("[Icon Item] Round size we want to load a pixmap in");?></li>
<li><?php i18n("portrait prop is not relevant when there is no text (bug 374815)");?></li>
<li><?php i18n("Fix the renderType properties for various components");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Work round DBus property fetching bug (bug 345871)");?></li>
<li><?php i18n("Treat no passphrase as Solid::UserCanceled error");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Added Greek trigram data file");?></li>
<li><?php i18n("Fix segfault in trigrams generation and expose MAXGRAMS constant in the header");?></li>
<li><?php i18n("Look for non-versioned libhunspell.so, should be more future proof");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("C++ highlighting: update to Qt 5.8");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.31");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
