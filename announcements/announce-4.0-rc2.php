<?php

  $page_title = "KDE 4.0 Release Candidate 2";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Second Release Candidate for Leading Free Software Desktop,
Codename "Coenig"
</h3>
<p align="justify">
  <strong>
    With the second release candidate, the KDE project would like to collect feedback to
ensure the quality of KDE 4.0.
  </strong>
</p>
<p align="justify">
December 11, 2007 (The INTERNET).
</p>

<p>
The KDE Community is happy to announce the immediate availability of <a
href="http://www.kde.org/announcements/announce-4.0-rc2.php">the
second release candidate for KDE 4.0</a>. This release candidate marks the last
mile on the road to KDE 4.0.
</p>

<div align="center">
    <a href="announce_4.0-rc2/konqueror.jpg">
        <img width="400" height="300" src="announce_4.0-rc2/konqueror_thumb.jpg" />
    </a><br />
    <em>KDE's webbrowser Konqueror</em>
</div>

<p>
While progress on the quality and completeness of what is to become the KDE 4.0 desktop
has been great, the KDE Community decided to have another release candidate before releasing
KDE 4.0 on January, 11th. The codebase is now feature-complete. Some work is still being done
to put the icing on the KDE 4.0 cake. This includes fixing some major and minor bugs,
finishing off artwork and smoothening out the user experience.
</p>

<div align="center">
    <a href="announce_4.0-rc2/dolphin.jpg">
        <img width="400" height="300" src="announce_4.0-rc2/dolphin_thumb.jpg" />
    </a><br />
    <em>Dolphin, the KDE 4.0 filemanager</em>
</div>

<p>
The second release candidate incorporates many improvements from the
previous Release Candidate, Beta and alpha releases:
<a href="http://www.kde.org/announcements/announce-4.0-rc1.php">RC1</a>,
<a href="http://www.kde.org/announcements/announce-4.0-beta4.php">Beta
4</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta3.php">Beta
3</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta2.php">Beta
2</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta1.php">Beta
1</a>, <a href="http://www.kde.org/announcements/announce-4.0-alpha2.php">Alpha
2</a> and <a
href="http://www.kde.org/announcements/visualguide-4.0-alpha1.php">Alpha 1</a>.
</p>

<div align="center">
    <a href="announce_4.0-rc2/systemsettings.jpg">
        <img width="400" height="300" src="announce_4.0-rc2/systemsettings_thumb.jpg" />
    </a><br />
    <em>Configure your system with Systemsettings</em>
</div>


<h2>RC 2</h2>
<p>
With this second release candidate, the KDE developers hope to collect comments
and bug reports from the wider KDE community. With their help, we hope to solve the most
pressing problems with the current KDE 4 codebase to ensure the final 4.0
release is stable, usable and fun to work with.
We would like to encourage anyone who is willing and able to
spend some time on testing to find and <a href="http://bugs.kde.org">report</a>
problems to the KDE developers. It is recommended to have a current snapshot of the
codebase handy. That makes trying things easier, it also helps the process by
not having  to hunt down bugs that have already been fixed and makes it easier to
test patches proposed by developers.
</p>
<div align="center">
    <a href="announce_4.0-rc2/krunner.jpg">
        <img width="400" height="300" src="announce_4.0-rc2/krunner_thumb.jpg" />
    </a><br />
    <em>KRunner, part of the Plasma workspace</em>
</div>
<p>
You can find <a
href="http://techbase.kde.org/Contribute/Bugsquad/How_to_create_useful_crash_reports">some documentation</a> on the pages of our
<a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> and <a
href="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">in other
places</a>. There will be weekly <a
href="http://aseigo.blogspot.com/2007/10/kde4-krush-days-saturday.html">KDE 4
Krush Days on Saturdays</a> and everyone is invited to help out. Having a recent
SVN checkout (follow instructions on <a
href="http://techbase.kde.org">TechBase</a>) is great, but using the regularly
updated <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE KDE 4 Live CD</a> 
<!--or the <a href="http://pkg-kde.alioth.debian.org/kde4livecd.html">Debian KDE4 Beta4 Live CD</a>-->, 
while very easy, is also a big help to us.
</p>


<h4>Get it, run it, test it...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0-rc2 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/3.97.php">KDE 4.0-rc2 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>

<br>
<i>Compiled by Sebastian K&uuml;gler with extensive help from the KDE community</i>

<h2>About KDE 4</h2>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks
to fulfill the need for a powerful yet easy to use desktop for UNIX and LINUX
workstations. The aim of the KDE project for the 4.0 release is to put the
foundations in place for future innovations on the Free Desktop. The many newly
introduced technologies incorporated in the KDE libraries will make it easier
for developers to add rich functionality to their applications, combining and
connecting different components in any way they want.
</p>

<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
