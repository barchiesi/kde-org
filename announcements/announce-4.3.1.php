<?php
  $page_title = "KDE 4.3.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE 4.3.1 Provides a Wave of Improvements
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Translation and Service Release of the 4.3
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
September 1st, 2009. A month has passed since the release of KDE 4.3.0, so today the  <a href="http://www.kde.org/">KDE
Community</a> announces the immediate availability of KDE 4.3.1, a
bugfix, translation and maintenance update for the latest generation
of the most advanced and powerful
free desktop. KDE 4.3.1 is a monthly update to <a href="4.3/">KDE 4.3</a>. It
ships with a desktop workspace and many cross-platform applications such as administration
programs, network tools, educational applications, utilities, multimedia software, games, artwork,
development tools and more. KDE's award-winning tools and applications are
available in more than 50 languages.</p>

<p>KDE 4.3.1 is dedicated to Emma Hope Pyne, the daughter of Michael Pyne. Emma Hope suddenly passed away last week. The KDE community feels incredibly sad about this loss and wishes Michael and the family and friends all the strength needed to cope with the loss of Emma Hope.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="http://kde.org/announcements/4.3/">
    <img src="http://kde.org/announcements/4.3/images/kde430-desktop.png" alt="KDE 4.3.0 Release Notes" align="center"  height="337"  />
</a>
    <br />
    <em>The KDE 4.3 Desktop</em>
</div>




<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.3.1/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
As a service release, the
<a href="http://www.kde.org/announcements/changelogs/changelog4_3_0to4_3_1.php">changelog</a>
contains a list of bugfixes and improvements</a>.

Notable improvements include, but are not limited to:
<ul>
    <li>KDE 4.3 is now also available in <strong>Croatian</strong>
    <li>A crash when editing toolbar setup has been fixed
    <li>Support for transferring files through SSH using KIO::Fish has been fixed
    <li>A number of bugs in KWin, KDE's window and compositing manager has been fixed
    <li>A large number of bugs in KMail, KDE's email client are now gone

</ul>


Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE 4.3.1, you can browse the Subversion log.
KDE 4.3.1 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.3 desktop and applications, please refer to the
<a href="http://www.kde.org/announcements/4.3/">KDE 4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">KDE 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.3.1 is a recommended update for everyone running KDE 4.3.0 or earlier versions.
<p />

<!-- // Boilerplate again -->

<h4>
  Installing KDE 4.3.1 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.3.1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.1/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.3.1.php">KDE 4.3.1 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.3.1
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for KDE 4.3.1 may be <a
href="http://download.kde.org/stable/4.3.1/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.3.1
  are available from the <a href="/info/4.3.1.php#binary">KDE 4.3.1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
