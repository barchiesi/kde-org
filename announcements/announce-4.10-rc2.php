<?php
  $page_title = "KDE Ships Second Release Candidate of Applications, Platform and Plasma Workspaces 4.10";
  $site_root = "../";
  include "header.inc";
?>

<p align="justify">


January 4, 2013. Today KDE released the second release candidate for its renewed Workspaces, Applications, and Development Platform. Meanwhile, the KDE release team has decided to schedule a third release candidate to allow for more testing, 
pushing the 4.10 releases into the first week of February 2013. Further testing and bug fixing will allow KDE to deliver rock-stable products to millions of users world wide.

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.10-beta1.png"><img src="announce-4.10-beta1_thumb.png" align="center" width="640" alt="Plasma Desktop with the Dolphin file manager" title="Plasma Desktop with the Dolphin file manager" /></a>
<br />
<em>Plasma Desktop with Dolphin</em>
</div>



Highlights of 4.10 include, but are not limited to:

<ul>
    <li>
    <strong>Qt Quick in Plasma Workspaces</strong> -- Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE's extensions on top of QtQuick allow deeper integration with the system and more powerful apps and Plasma components. Plasma Containments can now be written in QtQuick. Various Plasma widgets have been rewritten in QtQuick, notably the system tray, pager, notifications, lock & logout, weather and weather station, comic strip and calculator plasmoids. Many performance, quality and usability improvements make Plasma Desktop and Netbook workspaces easier to use.
    </li>
    <li>
    <strong>New Screen Locker</strong> -- A new screen locking mechanism based on QtQuick brings more flexibility and security to Plasma Desktop.
    </li>
    <li>
    <strong>Animated Wallpapers</strong> -- Thanks to a new QtQuick-based wallpaper engine, animated wallpapers are now much easier to create.
    </li>
    <li>
    <strong>Improved Zooming in Okular</strong> -- A technique called tiled rendering allows Okular to zoom in much further while reducing memory consumption. Okular Active, the touch-friendly version of the powerful document reader is now part of KDE SC.
    </li>
    <li>
    <strong>Faster indexing</strong> -- Improvements in the Nepomuk semantic engine allow faster indexing of files. The new Tags kioslave allows users to browse their files by tags in any KDE-powered application.
    </li>
    <li>
    <strong>Color Correction</strong> -- Gwenview, KDE's smart image viewer and Plasma's window manager now support color correction and can be adjusted to the color profile of different monitors, allowing for more natural representation of photos and graphics.
    </li>
    <li>
    <strong>Notifications</strong> -- Plasma's notifications are now rendered using QtQuick, notifications themselves, especially concerning power management have been cleaned up.
    </li>
    <li>
    <strong>New Print Manager</strong> -- Setup of printers and monitoring jobs was improved thanks to a new implementation of the Print Manager.
    </li>
    <li>
    <strong>Kate, KDE's Advanced Text Editor</strong> received multiple improvements regarding user feedback. It is now extensible using Python plugins.
    </li>
    <li>
    <strong>KTouch</strong> -- KDE's touch-typing learning utility has been rewritten and features a cleaner, more elegant user interface.
    </li>
    <li>
    <strong>libkdegames improvements</strong> -- Many parts of libkdegames have been rewritten, porting instructions for 3rd party developers are <a href="http://techbase.kde.org/Projects/Games/Porting_to_libkdegames_v5">available</a>.
    </li>
    <li>
    <strong>KSudoku</strong> now allows printing puzzles.
    </li>
    <li>
    <strong>KJumpingCube</strong> has seen a large number of improvements making the game more enjoyable.
    </li>
</ul>
More improvements can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.10_Feature_Plan">4.10 Feature Plan</a>. As with any large number of changes, we need to give 4.10 a good testing in order to maintain and improve the quality and user experience when users install the update.

<h2>Testing</h2>
<p align="justify">
KDE is running an extra detailed beta-testing program throughout the
4.10 beta and RC releases. To participate, get the latest KDE 4.10.
Instructions for installing the latest test release can be found 
<a href="http://community.kde.org/Get_Involved/Quality/Beta/4.10/Installing">here</a>.
</p>
<p align="justify">
Some areas of KDE deserve extra testing attention this time around. We have
checklists for testing in general, and recommendations for special attention
on the <a href=http://community.kde.org/Get_Involved/Quality/Beta/4.10#Testing">wiki</a>.
</p>

<h2>Get Involved</h2>
<p align="justify">
The KDE beta-testing program is a way for everyone to contribute to the
ongoing quality of KDE software. You can make a difference by carefully testing
the programs that are part of the KDE 4.10 beta and assist developers by
identifying legitimate bugs. This means higher quality software, more
features and happier users and developers.
</p>
<p align="justify">
Beta Testing Program is structured so that any KDE user can give back
to KDE, regardless of their skill level. If you want to be part of
this quality improvement program, please contact the Team on the IRC
channel #kde-quality on freenode.net. The Team Leaders want to
know ahead of time who is involved in order to coordinate all of the
testing activities. They are also committed to having this project be
fun and rewarding. After checking in, you can install the beta through
your distribution package manager. The KDE Community wiki has
instructions. This page will be updated as beta packages for other
distributions become available. With the beta installed, you can
proceed with testing. Please contact the Team on IRC #kde-quality if
you need help getting started.
</p>


<h3>KDE Software Compilation 4.10 RC2</h3>
<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.9.97/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing 4.10 RC2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.10 RC2 (internally 4.9.97)
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.10_RC_2_.284.9.97.29">Community Wiki</a>.
</p>

<h4>
  Compiling 4.10 RC2
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for 4.10 RC2 may be <a
href="http://download.kde.org/unstable/4.9.97/src/">freely downloaded</a>.
Instructions on compiling and installing 4.9.97
  are available from the <a href="/info/4.9.97.php">4.9.97 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or
become a KDE e.V. supporting member through our new
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
