<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.54.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.54.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
January 12, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.54.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Notify if a default provider failed to download");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Move typesForMimeType helper from BasicIndexingJob to anonymous namespace");?></li>
<li><?php i18n("Add \"image/svg\" as Type::Image to the BasicIndexingJob");?></li>
<li><?php i18n("Use Compact json formatting for storing document metadata");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Change preferences-system-network to symlink");?></li>
<li><?php i18n("Use a Kolf icon with nice background and shadows");?></li>
<li><?php i18n("Copy some changed icons from Breeze to Breeze Dark");?></li>
<li><?php i18n("Add YaST and new preference icons");?></li>
<li><?php i18n("Add a proper python-bytecode icon, use consistent color in python icons (bug 381051)");?></li>
<li><?php i18n("Delete python bytecode symlinks in preparation for them becoming their own icons");?></li>
<li><?php i18n("Make the python mimetype icon the base, and the python bytecode icon a link to it");?></li>
<li><?php i18n("Add device icons for RJ11 and RJ45 ports");?></li>
<li><?php i18n("Add missing directory separator (bug 401836)");?></li>
<li><?php i18n("Use correct icon for Python 3 scripts (bug 402367)");?></li>
<li><?php i18n("Change network/web color icons to consistent style");?></li>
<li><?php i18n("Add new name for sqlite files, so the icons actually show up (bug 402095)");?></li>
<li><?php i18n("Add drive-* icons for YaST Partitioner");?></li>
<li><?php i18n("Add view-private icon (bug 401646)");?></li>
<li><?php i18n("Add flashlight action icons");?></li>
<li><?php i18n("Improve symbolism for off and muted status icon (bug 398975)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add find module for Google's libphonenumber");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fix the version in the pkgconfig file");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Fix doxygen markdown rendering");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Escape bytes that are larger than or equal to 127 in config files");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("cmake macros: Port away from deprecated ECM var in kcoreaddons_add_plugin (bug 401888)");?></li>
<li><?php i18n("make units and prefixes of formatValue translatable");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("don't show separators on mobile");?></li>
<li><?php i18n("root.contentItem instead of just contentItem");?></li>
<li><?php i18n("Add the missing api for multilevel KCMs to control the columns");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Fail writing test if mime type is not supported by the extractor");?></li>
<li><?php i18n("Fix ape disc number extraction");?></li>
<li><?php i18n("Implement cover extraction for asf files");?></li>
<li><?php i18n("Extend list of supported mimetypes for embedded image extractor");?></li>
<li><?php i18n("Refactor embedded image extractor for greater extensibility");?></li>
<li><?php i18n("Add missing wav mimetype");?></li>
<li><?php i18n("Extract more tags from exif metadata (bug 341162)");?></li>
<li><?php i18n("fix extraction of GPS altitude for exif data");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Fix KGlobalAccel build with Qt 5.13 prerelease");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("README.md - add basic instructions for testing holiday files");?></li>
<li><?php i18n("various calendars - fix syntax errors");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("ksvg2icns : use Qt 5.9+ QTemporaryDir API");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("[xscreensaverpoller] Flush after reset screensaver");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Use soft rlimit for number of open handles. This fixes very slow Plasma startup with latest systemd.");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Revert \"Hide file preview when icon is too small\"");?></li>
<li><?php i18n("Display error instead of silently failing when asked to create folder that already exists (bug 400423)");?></li>
<li><?php i18n("Change the path for every item of the subdirectories in a directory rename (bug 401552)");?></li>
<li><?php i18n("Extend getExtensionFromPatternList reg exp filtering");?></li>
<li><?php i18n("[KRun] when asked to open link in external browser, fall back to mimeapps.list if nothing is set in kdeglobals (bug 100016)");?></li>
<li><?php i18n("Make the open url in tab feature a bit more discoverable (bug 402073)");?></li>
<li><?php i18n("[kfilewidget] Return editable URL navigator to breadcrumb mode if it has focus and everything is selected and when Ctrl+L is pressed");?></li>
<li><?php i18n("[KFileItem] Fix isLocal check in checkDesktopFile (bug 401947)");?></li>
<li><?php i18n("SlaveInterface: Stop speed_timer after a job is killed");?></li>
<li><?php i18n("Warn user before copy/move job if the file size exceeds the maximum possible file size in FAT32 filesystem(4 GB) (bug 198772)");?></li>
<li><?php i18n("Avoid constantly increasing Qt event queue in KIO slaves");?></li>
<li><?php i18n("Support for TLS 1.3 (part of Qt 5.12)");?></li>
<li><?php i18n("[KUrlNavigator] Fix firstChildUrl when going back from archive");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Make sure we don't override QIcon::themeName when we should not");?></li>
<li><?php i18n("Introduce a DelegateRecycler attached object");?></li>
<li><?php i18n("fix gridview margins considering scrollbars");?></li>
<li><?php i18n("Make AbstractCard.background react to AbstractCard.highlighted");?></li>
<li><?php i18n("Simplify code in MnemonicAttached");?></li>
<li><?php i18n("SwipeListItem: always show the icons if !supportsMouseEvents");?></li>
<li><?php i18n("Consider whether needToUpdateWidth according to widthFromItem, not height");?></li>
<li><?php i18n("Take the scrollbar into account for the ScrollablePage margin (bug 401972)");?></li>
<li><?php i18n("Don't try to reposition the ScrollView when we get a faulty height (bug 401960)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Change default sort order in the download dialog to \"Most downloads\" (bug 399163)");?></li>
<li><?php i18n("Notify about the provider not being loaded");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[Android] Fail more gracefully when building with API &lt; 23");?></li>
<li><?php i18n("Add Android notification backend");?></li>
<li><?php i18n("Build without Phonon and D-Bus on Android");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("applications.menu: remove unused category X-KDE-Edu-Teaching");?></li>
<li><?php i18n("applications.menu: remove &lt;KDELegacyDirs/&gt;");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix scripting for Qt 5.12");?></li>
<li><?php i18n("Fix emmet script by using HEX instead of OCT numbers in strings (bug 386151)");?></li>
<li><?php i18n("Fix broken Emmet (bug 386151)");?></li>
<li><?php i18n("ViewConfig: Add 'Dynamic Wrap At Static Marker' option");?></li>
<li><?php i18n("fix folding region end, add ending token to the range");?></li>
<li><?php i18n("avoid ugly overpainting with alpha");?></li>
<li><?php i18n("Don't re-mark words added/ignored to the dictionary as misspelled (bug 387729)");?></li>
<li><?php i18n("KTextEditor: Add action for static word wrap (bug 141946)");?></li>
<li><?php i18n("Don't hide 'Clear Dictionary Ranges' action");?></li>
<li><?php i18n("Don't ask for confirmation when reloading (bug 401376)");?></li>
<li><?php i18n("class Message: Use inclass member initialization");?></li>
<li><?php i18n("Expose KTextEditor::ViewPrivate:setInputMode(InputMode) to KTextEditor::View");?></li>
<li><?php i18n("Improve performance of small editing actions, e.g. fixing large replace all actions (bug 333517)");?></li>
<li><?php i18n("Only call updateView() in visibleRange() when endPos() is invalid");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add clarifying about using both KDE's ServerDecoration and XdgDecoration");?></li>
<li><?php i18n("Xdg Decoration Support");?></li>
<li><?php i18n("Fix XDGForeign Client header installs");?></li>
<li><?php i18n("[server] Touch drag support");?></li>
<li><?php i18n("[server] Allow multiple touch interfaces per client");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KMessageBox] Fix minimum dialog size when details are requested (bug 401466)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Added DCB, macsrc, match, tc, ovs-patch and ovs-port settings");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Calendar] Expose firstDayOfWeek in MonthView (bug 390330)");?></li>
<li><?php i18n("Add preferences-system-bluetooth-battery to preferences.svgz");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Add plugin type for sharing URLs");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix menu item width when the delegate is overridden (bug 401792)");?></li>
<li><?php i18n("Rotate busy indicator clockwise");?></li>
<li><?php i18n("Force checkboxes/radios to be square");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[UDisks2] Use MediaRemovable to determine if media can be ejected");?></li>
<li><?php i18n("Support Bluetooth batteries");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Add method to BackgroundChecker to add word to session");?></li>
<li><?php i18n("DictionaryComboBox: Keep user preferred dictionaries on top (bug 302689)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Update php syntax support");?></li>
<li><?php i18n("WML: fix embedded Lua code &amp; use new default styles");?></li>
<li><?php i18n("Highlight CUDA .cu and .cuh files as C++");?></li>
<li><?php i18n("TypeScript &amp; TS/JS React: improve types detection, fix float-points &amp; other improvements/fixes");?></li>
<li><?php i18n("Haskell: Highlight empty comments after 'import'");?></li>
<li><?php i18n("WML: fix infinite loop in contexts switch &amp; only highlight tags with valid names (bug 402720)");?></li>
<li><?php i18n("BrightScript: Add workaround for QtCreator 'endsub' highlighting, add function/sub folding");?></li>
<li><?php i18n("support more variants of C number literals (bug 402002)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.54");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.9");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
