<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.15: Lightweight, Usable and Productive.",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.15.0'; // for i18n
    $version = "5.15.0";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>


<main class="releaseAnnouncment container">

    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>
<!--
    <figure class="videoBlock">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/C2kR1_n_d-g?rel=0" allowfullscreen='true'></iframe>
    </figure>
-->
    <figure class="topImage">
        <a href="plasma-5.15/plasma-5.15-apps.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.15/plasma-5.15-apps-wee.png" width="600" height="338" alt="Plasma 5.15">
        </a>
        <figcaption><?php print i18n_var("KDE Plasma %1", "5.15")?></figcaption>
    </figure>

    <p>
        <?php i18n("Tuesday, 12 February 2019.")?>
    </p>
    <p>
        <?php // print i18n_var("Today KDE launches the %1 %2 release of Plasma in %3: Say hello to Plasma %4.", "first", "stable", "2019", "5.15");?>
        <?php print i18n_var("Today KDE launches the first stable release of Plasma in %3: Say hello to Plasma %4.", "first", "stable", "2019", "5.15");?>
    </p>

    <p>
        <?php i18n("For the first production release of 2019, the Plasma team has embraced KDE's <a href=\"https://community.kde.org/Goals/Usability_%26_Productivity\">Usability &amp; Productivity goal</a> and has been working on hunting down and removing all the papercuts that slow you down.");?>
    </p>
        
        
    <p>
        <?php i18n("With this in mind, we teamed up with the VDG (Visual Design Group) contributors to get feedback on all the annoying problems in our software, and fixed them to ensure an intuitive and consistent workflow for your daily use.");?>
    </p>

    <p>
        <?php i18n("Plasma 5.15 brings a number of changes to the configuration interfaces, including more options for complex network configurations. Many icons have been added or redesigned to make them clearer. Integration with third-party technologies like GTK and Firefox has been improved substantially.");?>
    </p>

    <p>
        <?php i18n("Discover, Plasma's software and add-on installer, has received tonnes of improvements to help you stay up-to-date and find the tools you need to get your tasks done.");?>
    </p>
    
    <p>
        <?php i18n("Install Plasma 5.15 and let us know what you think.");?>
    </p>

    <p>
        <?php print i18n_var("Browse the <a href='plasma-5.14.5-5.15.0-changelog.php'>full Plasma %1 changelog</a> to learn more about other tweaks and bug fixes included in this release: ", "5.15");?>
    </p>

    <br clear="all" />

<h2><?php print i18n_var("New in Plasma %1", "5.15");?></h2>



<h3 id="widgets"><?php i18n("Plasma Widgets");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/bluetooth-battery.png" data-toggle="lightbox">
<img src="plasma-5.15/bluetooth-battery-wee.png" style="margin-left: 10px; border: 0px" width="350" height="318" alt="<?php i18n("Bluetooth Battery Status");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Bluetooth Battery Status");?></figcaption>
</figure>

<li><?php i18n("Bluetooth devices now show their battery status in the power widget. Note that this cutting-edge feature requires the latest versions of the <tt>upower</tt> and <tt>bluez packages</tt>."); ?></li>

<li><?php i18n("It is now possible to download and install new <a data-toggle='lightbox' href='plasma-5.15/wallpaper-plugins.png'>wallpaper plugins</a> straight from the wallpaper configuration dialog."); ?></li>

<li><?php i18n("Filenames on desktop icons now have enough <a data-toggle='lightbox' href='plasma-5.15/desktop-icons2.png'>horizontal space to be legible</a> even when their icons are tiny, and are <a data-toggle='lightbox' href='plasma-5.15/desktop-icons.png'>easier to read</a> when the wallpaper is very light-colored or visually busy."); ?></li>

<li><?php i18n("Visually impaired users can now read the icons on the desktop thanks to the newly-implemented screen reader support for desktop icons."); ?></li>

<li><?php i18n("The Notes widget now has a <a data-toggle='lightbox' href='plasma-5.15/notes-widget.png'>\"Transparent with light text\" theme</a> theme."); ?></li>

<li><?php i18n("It is now possible to configure whether scrolling over the virtual desktop Pager widget will \"wrap around\" when reaching the end of the virtual desktop list."); ?></li>

<li><?php i18n("The padding and appearance of <a data-toggle='lightbox' href='plasma-5.15/notification-popup.png'>notification pop-ups</a> have been improved."); ?></li>

<li><?php i18n("KRunner has received several usability improvements. It now handles duplicates much better, no longer showing <a data-toggle='lightbox' href='plasma-5.15/krunner-bookmarts.png'>duplicate bookmarks from Firefox</a> or duplicate entries when the same file is available in multiple categories. Additionally, the layout of the standalone search widget now matches KRunner's appearance."); ?></li>

<li><?php i18n("The Devices Notifier is now much smarter. When it is configured to display all disks instead of just removable ones, it will recognize when you try to unmount the root partition and prevent you from doing so."); ?></li>
</ul>
<br clear="all" />



<h3 id="settings"><?php i18n("Settings");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/virtual-desktops.png" data-toggle="lightbox">
<img src="plasma-5.15/virtual-desktops-wee.png" style="margin-left: 10px; border: 0px" width="350" height="252" alt="<?php i18n("Redesigned Virtual Desktop Settings");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Redesigned Virtual Desktop Settings");?></figcaption>
</figure>

<li><?php i18n("The System Settings Virtual Desktops page has been redesigned and rewritten for Wayland support, and is now more usable and visually consistent."); ?></li>

<li><?php i18n("The user interface and layout for the <a data-toggle='lightbox' href='plasma-5.15/systemsettings-digital-clock.png'>Digital Clock</a> and <a data-toggle='lightbox' href='plasma-5.15/systemsettings-folderview-settings.png'>Folder View</a> settings pages have been improved to better match the common style."); ?></li>

<li><?php i18n("Many <a data-toggle='lightbox' href='plasma-5.15/systemsettings-font.png'>System Settings pages</a> have been tweaked with the goal of standardizing the icons, wording, and placement of the bottom buttons, most notably the \"Get New [thing]…\" buttons."); ?></li>

<li><?php i18n("New desktop effects freshly installed from store.kde.org now appear in the list on the System Settings Desktop Effects page."); ?></li>

<li><?php i18n("The native display resolution is now indicated with a star icon in the System Settings Displays page."); ?></li>

<li><?php i18n("The <a data-toggle='lightbox' href='plasma-5.15/systemsettings-login.png'>System Settings Login Screen</a> page has received plenty of visual improvements. The image preview of the default Breeze theme now reflects its current appearance, the background color of the preview matches the active color scheme, and the sizes and margins have been adjusted to ensure that everything fits without being cut off."); ?></li>

<li><?php i18n("The System Settings Desktop Effects page has been ported to QtQuickControls 2. This fixes a number of issues such as bad fractional scaling appearance, ugly dropdown menu checkboxes, and the window size being too small when opened as a standalone app."); ?></li>
</ul>
<br clear="all" />



<h3 id="cross"><?php i18n("Cross-Platform Integration");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/firefox-portals.png" data-toggle="lightbox">
<img src="plasma-5.15/firefox-portals-wee.png" style="margin-left: 10px; border: 0px" width="350" height="203" alt="<?php i18n("Firefox with native KDE open/save dialogs");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Firefox with native KDE open/save dialogs");?></figcaption>
</figure>
<li><?php i18n("Firefox 64 can now optionally use native KDE open/save dialogs. This is a bleeding-edge functionality that is not yet included in distributions. However, it can be enabled by installing the <tt>xdg-desktop-portal</tt> and <tt>xdg-desktop-portal-kde</tt> packages and setting <tt>GTK_USE_PORTAL=1</tt> in Firefox's    .desktop file."); ?></li>

<li><?php i18n("Integration modules <tt>xdg-desktop-portal-kde</tt> and <tt>plasma-integration</tt> now support the Settings portal. This allows sandboxed Flatpak and Snap applications to respect your Plasma configuration — including fonts, icons, widget themes, and color schemes — without requiring read permissions to the kdeglobals configuration file."); ?></li>

<li><?php i18n("The global scale factor used by high-DPI screens is now respected by GTK and GNOME apps when it is an integer."); ?></li>

<li><?php i18n("A wide variety of issues with the Breeze-GTK theme has been resolved, including the inconsistencies between the light and dark variants. We have also made the theme more maintainable, so future improvements will be much easier."); ?></li>
</ul>
<br clear="all" />



<h3 id="discover"><?php i18n("Discover");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/discover-release-upgrade.png" data-toggle="lightbox">
<img src="plasma-5.15/discover-release-upgrade-wee.png" style="margin-left: 10px; border: 0px" width="350" height="334" alt="<?php i18n("Distro Release Upgrade Notification");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Distro Release Upgrade Notification");?></figcaption>
</figure>

<li><?php i18n("Options for upgrading your distribution are now included in Discover's Update Notifier widget. The widget will also display a \"Restart\" button if a restart is recommended after applying all updates, but the user hasn't actually restarted yet."); ?></li>

<li><?php i18n("On Discover's Updates page, it is now possible to uncheck and re-check all available updates to make it easier to pick and choose the ones you want to apply."); ?></li>

<li><?php i18n("<a data-toggle='lightbox' href='plasma-5.15/discover-settings.png' data-toggle='lightbox'>Discover’s Settings page</a> has been renamed to \"Sources\" and now has pushbuttons instead of hamburger menus."); ?></li>

<li><?php i18n("Distribution repository management in Discover is now more practical and usable, especially for Ubuntu-based distros."); ?></li>

<li><?php i18n("Discover now supports app extensions offered with Flatpak packages, and lets you choose which ones to install."); ?></li>

<li><?php i18n("Handling of local packages has been improved: Discover can now indicate the dependencies and will show a \"Launch\" button after installation."); ?></li>

<li><?php i18n("When performing a search from the Featured page, Discover now only returns apps in the search results. Add-ons will appear in search results only when a search is initiated from the add-ons section."); ?></li>

<li><?php i18n("Discover's search on the Installed Apps page now works properly when the Snap backend is installed."); ?></li>

<li><?php i18n("Handling and presentation of errors arising from misconfigured add-on repos has also been improved."); ?></li>

<li><?php i18n("Discover now respects your locale preferences when displaying dates and times."); ?></li>

<li><?php i18n("The \"What's New\" section is no longer displayed on app pages when it doesn't contain any relevant information."); ?></li>

<li><?php i18n("Application and Plasma add-ons are now listed in a separate category on Discover's Updates page."); ?></li>
</ul>
<br clear="all" />

<h3 id="window"><?php i18n("Window Management");?></h3>
<ul>
<!--
<figure style="float: right; text-align: right">
<a href="plasma-5.15/xxx.png" data-toggle="lightbox">
<img src="plasma-5.15/xxx.png" style="border: 0px" width="350" height="264" alt="<?php i18n("TITLE");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("TITLE");?></figcaption>
</figure>
-->
<li><?php i18n("The Alt+Tab window switcher now supports screen readers for improved accessibility, and allows you to use the keyboard to switch between items."); ?></li>

<li><?php i18n("The KWin window manager no longer crashes when a window is minimized via a script."); ?></li>

<li><?php i18n("Window closing effects are now applied to dialog boxes with a parent window (e.g. an app's Settings window, or an open/save dialog)."); ?></li>

<li><?php i18n("Plasma configuration windows now raise themselves to the front when they get focus."); ?></li>
</ul>
<br clear="all" />



<h3 id="wayland"><?php i18n("Wayland");?></h3>
<ul>
<!--
<figure style="float: right; text-align: right">
<a href="plasma-5.15/xxx.png" data-toggle="lightbox">
<img src="plasma-5.15/xxx.png" style="border: 0px" width="350" height="264" alt="<?php i18n("TITLE");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("TITLE");?></figcaption>
</figure>
-->
<li><?php i18n("More work has been done on the foundations: <tt>XdgStable</tt>, <tt>XdgPopups</tt> and <tt>XdgDecoration</tt> protocols are now fully implemented."); ?></li>

<li><?php i18n("Wayland now supports virtual desktops, and they work in a more fine-grained way than on X11. Users can place a window on any subset of virtual desktops, rather than just on one or all of them."); ?></li>

<li><?php i18n("Touch drag-and-drop is now supported in Wayland."); ?></li>
</ul>
<br clear="all" />



<h3 id="network"><?php i18n("Network Management");?></h3>
<ul>

<figure style="float: right; text-align: right">
<a href="plasma-5.15/wireguard.png" data-toggle="lightbox">
<img src="plasma-5.15/wireguard-wee.png" style="margin-left: 10px; border: 0px" width="350" height="264" alt="<?php i18n("WireGuard VPN Tunnels");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("WireGuard VPN Tunnels");?></figcaption>
</figure>

<li><?php i18n("Plasma now offers support for WireGuard VPN tunnels when the appropriate Network Manager plugin is installed."); ?></li>

<li><?php i18n("It is now possible to mark a network connection as \"metered\"."); ?></li>
</ul>
<br clear="all" />



<h3 id="breeze"><?php i18n("Breeze Icons");?></h3>
<p><?php i18n("Breeze Icons are released with <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> but are extensively used throughout Plasma, so here's a highlight of some of the improvements made over the last three months."); ?></p>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/breeze-emblems.png" data-toggle="lightbox">
<img src="plasma-5.15/breeze-emblems-wee.png" style="margin-left: 10px; border: 0px" width="350" height="175" alt="<?php i18n("Icon Emblems in Breeze");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Icon Emblems in Breeze");?></figcaption>
</figure>
<li><?php i18n("A variety of <a data-toggle='lightbox' href='plasma-5.15/breeze-device.png'>Breeze device and preference icons</a> have been improved, including the multimedia icons and all icons that depict a stylized version of a Plasma wallpaper."); ?></li>

<li><?php i18n("<a data-toggle='lightbox' href='plasma-5.15/breeze-emblems2.png'>The Breeze emblem</a> and <a data-toggle='lightbox' href='plasma-5.15/breeze-package.png'>package</a> icons have been entirely redesigned, resulting in a better and more consistent visual style, plus better contrast against the icon they're drawn on top of."); ?></li>

<li><?php i18n("In new installs, the <a data-toggle='lightbox' href='plasma-5.15/breeze-places.png'>Places panel</a> now displays a better icon for the Network place."); ?></li>

<li><?php i18n("The <a data-toggle='lightbox' href='plasma-5.15/breeze-vault.png'>Plasma Vault icon</a> now looks much better when using the Breeze Dark theme."); ?></li>

<li><?php i18n("<a data-toggle='lightbox' href='plasma-5.15/breeze-python.png'>Python bytecode files</a> now get their own icons."); ?></li>
</ul>
<br clear="all" />


<h3 id="other"><?php i18n("Other");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.15/ksysguard.png" data-toggle="lightbox">
<img src="plasma-5.15/ksysguard-wee.png" style="margin-left: 10px; border: 0px" width="350" height="180" alt="<?php i18n("KSysGuard’s optional menu bar");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("KSysGuard’s optional menu bar");?></figcaption>
</figure>
<li><?php i18n("It is now possible to hide KSysGuard's menu bar — and it reminds you how to get it back, just like Kate and Gwenview do."); ?></li>

<li><?php i18n("The <tt>plasma-workspace-wallpapers</tt> package now includes some of the best recent Plasma wallpapers."); ?></li>
</ul>

<br clear="all" />
    <!-- // Boilerplate again -->
    <section class="row get-it">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try out Plasma 5.15 is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='http://community.kde.org/Frameworks/Building' class="learn-more"><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on <a class='shareFacebook' href='https://www.facebook.com/kde/' rel='nofollow'>Facebook</a>
            or <a class='shareTwitter' href='https://twitter.com/kdecommunity' rel='nofollow'>Twitter</a>
            or <a class='shareGoogle' href='https://plus.google.com/105126786256705328374/posts' rel='nofollow'>Google+</a>."); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows thanks to the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions. Whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, or donating money, all contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/"); ?>
    </p>

    <?php
    include($site_root . "/contact/about_kde.inc");
    ?>

    <h2><?php i18n("Press Contacts");?></h2>

    <?php
    include($site_root . "/contact/press_contacts.inc");
    ?>

</main>
<?php
  require('../aether/footer.php');
