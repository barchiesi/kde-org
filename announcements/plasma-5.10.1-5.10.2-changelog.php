<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.10.2 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.10.2";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Use fontMetrics.height() rather than boundingRect(...).height(). <a href='https://commits.kde.org/breeze/4f0940b79001ae8b400d01a8abf63d03b8dbd9fc'>Commit.</a> See bug <a href='https://bugs.kde.org/380391'>#380391</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Don't compare a bool to an enum. <a href='https://commits.kde.org/discover/ee961278f00aac862b42944bdd86bcd44964b25c'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/discover/6cd1526841f8d19df60caa3dbf609b11880cf127'>Commit.</a> </li>
<li>Also show a header when doing a global search. <a href='https://commits.kde.org/discover/2d1b0a7ec341a18163e0cd82b9d4883a9e0daebe'>Commit.</a> </li>
<li>Fix typos on warnings. <a href='https://commits.kde.org/discover/97eddeaf6c84ab35690c24ea9fef0797aeee561e'>Commit.</a> </li>
<li>Agilize search. <a href='https://commits.kde.org/discover/5ccbabd1e42c3f3d648f5728fb0c9d9ddd09ee44'>Commit.</a> </li>
<li>Don't read a file to put it in a temporary file again. <a href='https://commits.kde.org/discover/614756d260665f21dc0d01f69a00ecb1696eb182'>Commit.</a> </li>
<li>Fix test. <a href='https://commits.kde.org/discover/62cfe9150a223f69c56cde11ff79ed708b27b6cd'>Commit.</a> </li>
<li>Fix typo. <a href='https://commits.kde.org/discover/620c7d6746b23623529f5ada33294fec653f8784'>Commit.</a> </li>
<li>Populate the actionsMenu right in the SourcesPage. <a href='https://commits.kde.org/discover/dd636a90c334948dee03af10af41b71f824f3d2f'>Commit.</a> </li>
<li>Split qml and image qrc files. <a href='https://commits.kde.org/discover/8feafd8a6d7968f2c7e0a033f03ceb7a0cf35c1d'>Commit.</a> </li>
<li>Improve hiding of scrolling controls in the UpdatesPage. <a href='https://commits.kde.org/discover/64aa23558ce953637e5b2ad66c69d1c1e2f5a24a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380993'>#380993</a></li>
<li>Better integrate smaller ApplicationDelegate. <a href='https://commits.kde.org/discover/0c236c192a068ca7899ac48f611f427002eec163'>Commit.</a> </li>
<li>Don't mark used as unused. <a href='https://commits.kde.org/discover/c3b23492f59f3b7c286259f54838864fa19b2232'>Commit.</a> </li>
<li>Fix snap backend build. <a href='https://commits.kde.org/discover/a0dfea1762b68e27e4c53f18fc1242115295d6a3'>Commit.</a> </li>
<li>Hopefully fix build in build.ko. <a href='https://commits.kde.org/discover/0569267079850e43548e5525aab1d47d437fb39c'>Commit.</a> </li>
<li>Include missing margins in the UpdatesPage header controls. <a href='https://commits.kde.org/discover/da68d7f33464109ac1dd160147e0f508ecf16b52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380992'>#380992</a></li>
<li>Improve apt-config integration in pknotifier. <a href='https://commits.kde.org/discover/92df6add4cb39a1e51f91317a9706b37eea3ecb3'>Commit.</a> </li>
<li>Integrate apt setting for periodicity in the notifier. <a href='https://commits.kde.org/discover/b62332a25db1fc81aa87574e49da38d3ed3162a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/337752'>#337752</a></li>
<li>Remove unneeded fwd declaration. <a href='https://commits.kde.org/discover/19359378299f2207b5a8e1fbfd90123658ba8c37'>Commit.</a> </li>
<li>Centralize reload actions into a generic one that will call the backends. <a href='https://commits.kde.org/discover/a38129b2c41ac42f0bca4c729d823dcd332869fb'>Commit.</a> </li>
<li>Readability. <a href='https://commits.kde.org/discover/b28651540ad02428a1bc3e3e254c1f2168b8a223'>Commit.</a> </li>
<li>Also check the provider like we do in the knshandler. <a href='https://commits.kde.org/discover/16f76c0d5fccb5969007ddb45e9ef63521d7c79a'>Commit.</a> </li>
<li>Remove unused declaration. <a href='https://commits.kde.org/discover/e0a46853c5219703deaa78128978417260355036'>Commit.</a> </li>
<li>Make the PackageKit backend more resistant to crashes in PackageKit. <a href='https://commits.kde.org/discover/64c8778d4cd5180dfa13dd75fed808de9271cedc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376538'>#376538</a></li>
<li>Include a header for the settings page for better consistency. <a href='https://commits.kde.org/discover/bffd42fbaac59f262f6d61b8a25cbea5abb12701'>Commit.</a> </li>
<li>Don't leak QNAM factories. <a href='https://commits.kde.org/discover/f102fe38abfa020b5d0bcfca4b87e0b950080ac7'>Commit.</a> </li>
<li>Make sure the status is initialized before using it. <a href='https://commits.kde.org/discover/e80405c42de936f323abc1a78899ee0224b45e21'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Let's continue in debug code instead of returning from XRandRConfig::applyKScreenConfig. <a href='https://commits.kde.org/libkscreen/e1355808be3953d75d0d720e5b569d761d2844dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6011'>D6011</a></li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Fixed calculation of top border. <a href='https://commits.kde.org/oxygen/b03c3cb52ad3599659f975ae660f0bbd60eff52d'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix warning: Typo in enum notation. <a href='https://commits.kde.org/plasma-desktop/269fff37f6494d4595a9734a8bcccbe4bb7c5f32'>Commit.</a> </li>
<li>Fix warning: Not all models implement AppPid. <a href='https://commits.kde.org/plasma-desktop/8e770121c77d1ebbc7f457ae65a476cb4447acff'>Commit.</a> </li>
<li>Fix warning: x has no RESET, and this is unnecessary. <a href='https://commits.kde.org/plasma-desktop/e0bf9e6e1b73db222bce781c490f2bf0899c72bc'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Openconnect: make sure the UI fits into the password dialog. <a href='https://commits.kde.org/plasma-nm/285c7ae37a3f6149b866dfb887bcb62ca6ce1046'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380399'>#380399</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Revert "Make the applet DBus activated". <a href='https://commits.kde.org/plasma-pa/e32c8904683d4ccf6362d30236425fb3d2b54438'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381010'>#381010</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6211'>D6211</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Icon Applet] Repopulate if URL didn't change. <a href='https://commits.kde.org/plasma-workspace/45e4f998e9e8835c019ff07079d746cbd2f6621b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380922'>#380922</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6148'>D6148</a></li>
<li>Fix binding loop in ClipboardItemDelegate. <a href='https://commits.kde.org/plasma-workspace/d134f8d3121e693f8276df805fa55f37b10b3667'>Commit.</a> </li>
<li>Squelch warning. <a href='https://commits.kde.org/plasma-workspace/063202dde066b39efe66dcb58b46c853cf05f390'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
