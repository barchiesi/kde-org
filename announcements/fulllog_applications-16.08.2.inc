<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>ErrorOverlay: fix dialogs being disabled by the mainwindow. <a href='http://commits.kde.org/akonadi/4e5c3eb76d70f88eb7e7caef3454d07362e2bddb'>Commit.</a> </li>
<li>Fix crash on shutdown. <a href='http://commits.kde.org/akonadi/bb2497052b415f2af0bf62a939eca4abb97d3b88'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Create AgePostingSource on heap. <a href='http://commits.kde.org/akonadi-search/1e70d63a9439f48b5f1a70accac531a10f4e4239'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363741'>#363741</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Clizip: Fix detection of unsupported zip archives. <a href='http://commits.kde.org/ark/3fda7d607210e2e0c57cb0d5c43f64cfa16f3a50'>Commit.</a> </li>
<li>Fix drag-and-drop extraction of parentless files. <a href='http://commits.kde.org/ark/bb9ede753623f7d3958f7a78985bb4142fbc0bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367549'>#367549</a></li>
<li>Fix drag-and-drop extraction of entries from certain archive formats. <a href='http://commits.kde.org/ark/593568d0c489a4ecffb5c53bc54cef64c33cca40'>Commit.</a> </li>
<li>Add test cases for RPM and AppImage. <a href='http://commits.kde.org/ark/868bc931ae33efc261eddfccc5960897be93eca6'>Commit.</a> </li>
<li>Fix drag-and-drop with RPM archives. <a href='http://commits.kde.org/ark/94d8567d46b5529e65805ba8d95611bcc53d8aa2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369535'>#369535</a></li>
<li>Improve focus handling of internal previewer. <a href='http://commits.kde.org/ark/0ec7aa626901c9349f2ba8f878f35f7748b125e5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369390'>#369390</a>. Fixes bug <a href='https://bugs.kde.org/369401'>#369401</a></li>
<li>Fix potential crash when aborting a ListJob. <a href='http://commits.kde.org/ark/c866108ceecdffa6e0263294153cad00eb297d69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369283'>#369283</a></li>
<li>Accept drop events when creating a new archive. <a href='http://commits.kde.org/ark/e90f49e0b66ee1ae4cc421ecfc4ba36c7581c2c3'>Commit.</a> </li>
<li>Show extraction dialog only after archive has been loaded. <a href='http://commits.kde.org/ark/1b41f96eb77004022336c0d6fa0f0330d50067a4'>Commit.</a> </li>
<li>Stop crashing when dropping on empty ArchiveView. <a href='http://commits.kde.org/ark/1f0753660f13ad7e2a2b51aa34b7580118bcbd33'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368807'>#368807</a></li>
<li>Add support for application/vnd.rar, fixes opening RAR with shared-mime-info 1.7. <a href='http://commits.kde.org/ark/9a9a851049cd904f08c9594cc6aacf696886404e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368786'>#368786</a></li>
</ul>
<h3><a name='blinken' href='https://cgit.kde.org/blinken.git'>blinken</a> <a href='#blinken' onclick='toggle("ulblinken", this)'>[Show]</a></h3>
<ul id='ulblinken' style='display: none'>
<li>Fix appdata file (namespace, installation directory). <a href='http://commits.kde.org/blinken/a5df375b8252dfa6b1cc5575cb66aae02d3c57fa'>Commit.</a> </li>
</ul>
<h3><a name='cervisia' href='https://cgit.kde.org/cervisia.git'>cervisia</a> <a href='#cervisia' onclick='toggle("ulcervisia", this)'>[Show]</a></h3>
<ul id='ulcervisia' style='display: none'>
<li>Fix QUrl creation with local file path. <a href='http://commits.kde.org/cervisia/1dd311a6fa5b8ad6830a57ba2532ce2a8e815dcc'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Hide message widgets only when reloading the view. <a href='http://commits.kde.org/dolphin/0e5e43aa9d84b2a38db99c469de91e4de8017fc7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357651'>#357651</a>. Code review <a href='https://git.reviewboard.kde.org/r/129061'>#129061</a></li>
<li>Properly check Shift toggling in DolphinRemoveAction. <a href='http://commits.kde.org/dolphin/3775ef19eaca057985b92cfa3716d3c3a1d22f0f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354301'>#354301</a>. Code review <a href='https://git.reviewboard.kde.org/r/128972'>#128972</a></li>
<li>Don't load twice kfileitemaction plugins with json metadata. <a href='http://commits.kde.org/dolphin/e93e6d4cc5231c5ee209f1ac9df70bbb253ce786'>Commit.</a> </li>
<li>Fix logic for loading kfileitemaction plugins. <a href='http://commits.kde.org/dolphin/abaf20f11409657888b1442c3d6c745de1d8f0c9'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Show]</a></h3>
<ul id='ulfilelight' style='display: none'>
<li>Make about data translated. <a href='http://commits.kde.org/filelight/c143e7cdd3c653273ea22e867b49bfc900f2ac7d'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Directly rename the appdata file in the repository. <a href='http://commits.kde.org/gwenview/cd1a85be06a08a9eaf027776d379b74e8d14c792'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Show]</a></h3>
<ul id='ulincidenceeditor' style='display: none'>
<li>Restore icon theme. <a href='http://commits.kde.org/incidenceeditor/6f795cc0db982a2a03e77ee7209d69f947858bc3'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Show]</a></h3>
<ul id='uljuk' style='display: none'>
<li>Fix MPRIS DesktopEntry after .desktop file rename. <a href='http://commits.kde.org/juk/c6917619f74562fe73b36a8b4374a3cde416e2bb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128998'>#128998</a></li>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/juk/543698607ae9e0f39b4ae3fdea76e279a60f8d70'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Show]</a></h3>
<ul id='ulkalzium' style='display: none'>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/kalzium/23b12cd98c21216ffe312bbf15209261b0f344d2'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Show]</a></h3>
<ul id='ulkapptemplate' style='display: none'>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/kapptemplate/26be69c2b15389676738dc935cbb0cb375002f98'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Session chooser: Ensure no unnecessary horizontal scroll bar. <a href='http://commits.kde.org/kate/97116ac0a3ac98ce265fe648928988ba68ea4cd1'>Commit.</a> </li>
<li>Fix crash in tab switcher plugin when using split views. <a href='http://commits.kde.org/kate/ef0cd0dc67d12afbb7bad64bd34a1160790154c6'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Backport 39b33ddd1e21c017bea3e0e1ad9ac7c4cd2ace83 from KF5. <a href='http://commits.kde.org/kdelibs/d3ea57b7ee2819e6ab1ac7c5e94f7b046db3d218'>Commit.</a> </li>
<li>Backport compile fix from kjs framework. <a href='http://commits.kde.org/kdelibs/1e9ba7d00bb870c7f5b7d2af2ccd31799e1b1910'>Commit.</a> </li>
<li>Fix compilation: Adapt to kentities.cc change. <a href='http://commits.kde.org/kdelibs/87cb36e4ea25be529a4fd0d7af4fae3b29a34104'>Commit.</a> </li>
<li>Fix linking of kdecore. <a href='http://commits.kde.org/kdelibs/155bdfb470dd9e00814597b48c9560bfd4fcd69e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128750'>#128750</a></li>
<li>Allow building without strigi on Linux. <a href='http://commits.kde.org/kdelibs/ba77ceb18bea3cb5c9d5207b2988f7d7e169d1fb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127823'>#127823</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix MLT Playlist proxying after recent proxy profile changes. <a href='http://commits.kde.org/kdenlive/b67c1a0ed6e84c630007568f6f0490960687b33d'>Commit.</a> </li>
<li>Fix proxy profiles to keep aspect ratio. <a href='http://commits.kde.org/kdenlive/e2ccb6684d909cf5ebdb0508fa15e4c101dc13b4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370326'>#370326</a></li>
<li>Fix broken keyframes on image/title clips. <a href='http://commits.kde.org/kdenlive/9621c8f5a89d746341edc1c66fb4d452003c8b39'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364318'>#364318</a></li>
<li>Fix automatic playback of file with space in name. <a href='http://commits.kde.org/kdenlive/185e7551a451b9de15bcd5891b6043dda048f145'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369653'>#369653</a></li>
<li>Fix simplekeyframe effects (eg. blur) allowing keyframe one frame after end of clip after resize. <a href='http://commits.kde.org/kdenlive/2c7f4bca78e8b65b339ebdc585637e4c256e3cbe'>Commit.</a> </li>
<li>Change defaults for Color selection effect so that we don't get a black screen at startup. <a href='http://commits.kde.org/kdenlive/f1105425a8834b3da6f143b7c43d2fa7c92a2f34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367686'>#367686</a></li>
<li>Fix "Make DocTools dep optional". <a href='http://commits.kde.org/kdenlive/49f9e3da3cb1921855cb15d037f5b6d59bbf2d78'>Commit.</a> </li>
<li>Fix scalable application icon. <a href='http://commits.kde.org/kdenlive/94b84e1d219a880c73a9c726b681e9206be1d265'>Commit.</a> </li>
<li>Make DocTools dep optional. <a href='http://commits.kde.org/kdenlive/954117f3548a5386bb06170e924681c5215a890a'>Commit.</a> </li>
<li>Warn about resize failure. <a href='http://commits.kde.org/kdenlive/d788c0ad3482852f0b0da7f75829f0f33199dd07'>Commit.</a> </li>
<li>Don't unnecessarily expand effect stack when unselecting a transition. <a href='http://commits.kde.org/kdenlive/ea60a6cba12d9430f9a73d6f01fd1f2a93ac5769'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368518'>#368518</a></li>
<li>Fix several geometry effects broken on locale with comma separator (french, german,...). <a href='http://commits.kde.org/kdenlive/aed3cd0a8a65f3021c71ec92df35ace52953d9d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369455'>#369455</a></li>
<li>Fix importing library clip hanging with relative paths, improve error reporting. <a href='http://commits.kde.org/kdenlive/e9818f33b3bf14ddaf4e12fc15ff1e1b7396a23e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129011'>#129011</a></li>
<li>Add the namespace to the appdata file. <a href='http://commits.kde.org/kdenlive/8d0de8c91bd80c416b5d1da93e20c201611dba92'>Commit.</a> </li>
<li>Fix copy/paste of keyframes in transitions. <a href='http://commits.kde.org/kdenlive/317ae5975ce6173e747697687cdd8b89f8d45d25'>Commit.</a> </li>
<li>* Add option to remove keyframes after cursor position. <a href='http://commits.kde.org/kdenlive/80b6dae1bd292825d150307dd8efcccb5732bc2a'>Commit.</a> </li>
<li>Only allow importing position if keyframes have no width/height info. <a href='http://commits.kde.org/kdenlive/29a56701fc5914819f857fbeb03ed834512b4741'>Commit.</a> </li>
<li>Fix header. <a href='http://commits.kde.org/kdenlive/889c68703c16f005c1ffbc0069e74ab33d3683c4'>Commit.</a> </li>
<li>Fix compilation. <a href='http://commits.kde.org/kdenlive/74c7f4861fcea26f278150a362ac5a73124fe088'>Commit.</a> </li>
<li>Fix copy/paste of keyframes. <a href='http://commits.kde.org/kdenlive/c0c83897129200f80cdf5bf3e6328bac7825bb50'>Commit.</a> </li>
<li>Fix several issues when editing an animation parameter in timeline (keyframes corruption). <a href='http://commits.kde.org/kdenlive/ddae0fb1bd2f53c9b3ad66d55e35350fc7bb9125'>Commit.</a> </li>
<li>Fix disappearing keyframes in animated parameters (Transform effect and Composite+Transform transition). <a href='http://commits.kde.org/kdenlive/0ad47529b3346f76c9a9d3ac2883503917a72466'>Commit.</a> </li>
<li>Fix keyframe type icons. <a href='http://commits.kde.org/kdenlive/5933f52d32c43fd69e1c4c15cd0b5c5fbaf90f19'>Commit.</a> </li>
<li>Fix incorrect initialization of Composite transition. <a href='http://commits.kde.org/kdenlive/36e7b36115621486c05551622254ba798033ecd3'>Commit.</a> </li>
<li>Fix crash when changing project fps while a clip was selected in timeline. <a href='http://commits.kde.org/kdenlive/54554d178e00bcee5deb588a2ad1cb522d5e2c3a'>Commit.</a> See bug <a href='https://bugs.kde.org/364559'>#364559</a></li>
<li>Slow motion clips: don't mix clip state with strobe param from older project files. <a href='http://commits.kde.org/kdenlive/8f018b08e7895f988a306d7c681b0e56bc08b5a7'>Commit.</a> </li>
<li>Fix possible crash when closing a project or deleting a clip with subclips. <a href='http://commits.kde.org/kdenlive/80b983fa003949ebcead17d323db2c3e71b4793d'>Commit.</a> </li>
<li>Fix some clip jobs incorrectly adding new clip. <a href='http://commits.kde.org/kdenlive/7f68d55310f229b686ed653c1ef7391c104cdb01'>Commit.</a> </li>
<li>Fix snapping when moving keyframe in effect stack. <a href='http://commits.kde.org/kdenlive/e027cb4c5a72ee9c50b2fcc819bb858b25339f2b'>Commit.</a> </li>
<li>Do not silently overwrite reversed clip. <a href='http://commits.kde.org/kdenlive/b8e066af9fc8eb7ed7e4b192891f25b181bebd03'>Commit.</a> </li>
<li>Fix Recent regression - groups lost on project opening. <a href='http://commits.kde.org/kdenlive/b7425707071a6955d50be6f58a5d86dc173bb25d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368342'>#368342</a></li>
<li>Fix clip monitor starting to play after drag. <a href='http://commits.kde.org/kdenlive/bed3443687b4115bab2ed86a1a6b88d3771e8680'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368569'>#368569</a></li>
<li>Fix track effect added to wrong track. <a href='http://commits.kde.org/kdenlive/8cba12fa71beac6ecb194fb8b588abe8914b6f51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368558'>#368558</a></li>
<li>Fix crash when changing project fps if timeline contains groups. <a href='http://commits.kde.org/kdenlive/18e1be8058f90c5a735415498d85f22e6a446116'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368601'>#368601</a></li>
<li>Fix groups on upper track disappearing when inserting a new track. <a href='http://commits.kde.org/kdenlive/9f1789865c6b2192c0af4d324ac231aae94f51ba'>Commit.</a> </li>
<li>FIx proxy used for rendering when app started from home dir. <a href='http://commits.kde.org/kdenlive/b00844ae8a3f779dd298c63e9d7a0cacceb4385e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368491'>#368491</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Fix Bug 369146 - Reading email advancing with "space" doesn't jump to next unread at end of current email. <a href='http://commits.kde.org/kdepim/bd04911af38d9aaa5d43d51b8b07a9101084f8ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369146'>#369146</a></li>
<li>Increase maximum value. <a href='http://commits.kde.org/kdepim/e308f18314a5d16326d0b2d2cbd92fdb80c20668'>Commit.</a> </li>
<li>Make it compiles. <a href='http://commits.kde.org/kdepim/a523b187b6702270354cde905d4e11ece1f03c73'>Commit.</a> </li>
<li>Fix Bug 369387 - Paste HTML tables in composer as HTML tables (not as picture). <a href='http://commits.kde.org/kdepim/870a47cc539d077e8726b1cd03a0c488822c9d11'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369387'>#369387</a></li>
<li>Fix "kmail --view foo.mbox", relative paths were not supported. <a href='http://commits.kde.org/kdepim/7fd4496b2f268855a89b91b9c52f467c0dbdcd98'>Commit.</a> </li>
<li>Check for invalid filename or non-existent local file. <a href='http://commits.kde.org/kdepim/9eeab83ba59f8296cfa2272914789ebe5cc3d9ee'>Commit.</a> </li>
<li>Hide configure button. <a href='http://commits.kde.org/kdepim/542eae12a3206e173becef32f0d3f202b121adcc'>Commit.</a> </li>
<li>Hide theses buttons, I don't understand why it was created but never implemented... <a href='http://commits.kde.org/kdepim/4c859a714b8d8cfa52687d773a1bf04a66d356a0'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/5ae9a5f748b0bf4aae7599dc5103975feaf86a0e'>Commit.</a> </li>
<li>Put close button to bottom of dialog. <a href='http://commits.kde.org/kdepim/4b9736d7b2d67391f88b60f4e87ffdb49b0afbe9'>Commit.</a> </li>
<li>Use default locale, not system one. <a href='http://commits.kde.org/kdepim/86597aadc9a8fb0d883b5513ae1f269e31d1962b'>Commit.</a> </li>
<li>Fix hdpi support. <a href='http://commits.kde.org/kdepim/c6dfbdba84b43d1354dcc0d82bfb39e6d67cb587'>Commit.</a> </li>
<li>Fix hdpi support. <a href='http://commits.kde.org/kdepim/26071ad26297bf1f847a7d29d52d3bbc605e6766'>Commit.</a> </li>
<li>Fix hdpi support. <a href='http://commits.kde.org/kdepim/b64c3d92396d2959c77c1f538e49a87e8dcfc04b'>Commit.</a> </li>
<li>Fix Browser model crash when email is missing a header. <a href='http://commits.kde.org/kdepim/00db9bd7154d7b4785a6f4ac3da8ee6a81830754'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356747'>#356747</a></li>
<li>Fix potential crash if ProviderPage::fillModel() is called twice. <a href='http://commits.kde.org/kdepim/1e38a73a143552c4438543ce6d11b22d50a8927f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/291474'>#291474</a></li>
<li>KMKernel: use the global identitymanager rather than creating another one. <a href='http://commits.kde.org/kdepim/3bbd5447d5f3cd5a3b4e9bbd435bd78c6c545ee6'>Commit.</a> </li>
<li>Fix Bug 368242 - Default feeds still use kde-apps and kde-look. <a href='http://commits.kde.org/kdepim/3f3265e99cdf57659aed826d5e19878e41d446a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368242'>#368242</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>Use scriptWordId. <a href='http://commits.kde.org/kdepim-addons/417a2a6fb3e7c2be810e0e92f97dedda255e6018'>Commit.</a> </li>
<li>Show emoticon in these themes. <a href='http://commits.kde.org/kdepim-addons/c7b8bffe5df6ca663cc1308bf336c15e0562d30e'>Commit.</a> See bug <a href='https://bugs.kde.org/369555'>#369555</a></li>
<li>[Plasma Calendar] Fix fetching CollectionColorAttribute through Monitor. <a href='http://commits.kde.org/kdepim-addons/61944f348f1bd8701b9cf33dc03e900e90961ec9'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Forward the error to the user via a KMessageBox. <a href='http://commits.kde.org/kdepim-runtime/d9091d8525f844c1dfa3383ef4c523e05482d460'>Commit.</a> </li>
<li>Fix build - ContactGroupSerializer needs Akonadi::Contact. <a href='http://commits.kde.org/kdepim-runtime/acb496c86356d2b2fb6df23c108436db879a58ab'>Commit.</a> </li>
<li>Don't link AkonadiContact from serializer plugins. <a href='http://commits.kde.org/kdepim-runtime/c25dcbd8852879a2f7ee102ef0f6555583b3dad7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364342'>#364342</a></li>
<li>[Kolab] Fix Configuration folder discovery. <a href='http://commits.kde.org/kdepim-runtime/5f67eab0ce7c17021f473e180633053ce9ca15bc'>Commit.</a> </li>
</ul>
<h3><a name='kdgantt2' href='https://cgit.kde.org/kdgantt2.git'>kdgantt2</a> <a href='#kdgantt2' onclick='toggle("ulkdgantt2", this)'>[Show]</a></h3>
<ul id='ulkdgantt2' style='display: none'>
<li>I forgot to increase version here. <a href='http://commits.kde.org/kdgantt2/ea3adc63b467db319206c99516f2ae9e2f513a39'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Do not set subkey fingerprint as key fingerprint. <a href='http://commits.kde.org/kgpg/4b01a60a1f43f2e72b082c815646183593f40ad0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367689'>#367689</a></li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Show]</a></h3>
<ul id='ulkiten' style='display: none'>
<li>Fix appdata file (namespace, installation directory). <a href='http://commits.kde.org/kiten/d6155703d3d69de0f8a37a3eb7729c54d8728185'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Fix appstream test. <a href='http://commits.kde.org/kolourpaint/c4f01f8a61ea50fe234c2baeb1810e066b88df84'>Commit.</a> </li>
<li>Make about dialog tabs fully translated and enable language switching for Kolourpaint. <a href='http://commits.kde.org/kolourpaint/50e1465ae97c7f76b381943ad9e8c6a967aa5354'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128877'>#128877</a>. Fixes bug <a href='https://bugs.kde.org/368504'>#368504</a>. Fixes bug <a href='https://bugs.kde.org/368505'>#368505</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Fix updating of tab title. <a href='http://commits.kde.org/konsole/dca6479a86f17b357365f1a598b8deeb0d602dbe'>Commit.</a> See bug <a href='https://bugs.kde.org/368785'>#368785</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Fix infinity loop when we right click on an url in a table. <a href='http://commits.kde.org/kpimtextedit/7c5598fe8d725a1fbef6c27d121cfde454ed21ca'>Commit.</a> </li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Tests: Use QTEST_GUILESS_MAIN. <a href='http://commits.kde.org/kstars/f9e75a6a5cdc5d742f80e546586b5193979eff65'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128676'>#128676</a></li>
</ul>
<h3><a name='ksudoku' href='https://cgit.kde.org/ksudoku.git'>ksudoku</a> <a href='#ksudoku' onclick='toggle("ulksudoku", this)'>[Show]</a></h3>
<ul id='ulksudoku' style='display: none'>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/ksudoku/cb1cd7761e5a3398276de94e61cf1510dbb4da50'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Show]</a></h3>
<ul id='ulktouch' style='display: none'>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/ktouch/c072859d2ee55db0cf7b11eac435e93be9ea0fca'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Show]</a></h3>
<ul id='ullibkdepim' style='display: none'>
<li>Workaround when we dnd email from viewer. <a href='http://commits.kde.org/libkdepim/d2290646a9f095ef7aa67daed232e02dc87f20f0'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Fix ackwardly indented code that newer gcc warns about. <a href='http://commits.kde.org/lokalize/5740876442e5c0f9c8562d2499f91e4f95f40967'>Commit.</a> </li>
<li>Appdata: use the screenshot from www.kde.org. <a href='http://commits.kde.org/lokalize/bce6b5a7c80d1a8a10b4ca74d96bfcf4af8c9854'>Commit.</a> </li>
<li>Rename the appdata file (namespace) and install it. <a href='http://commits.kde.org/lokalize/05b8a591aa6d6fae76d4923ce16f003c84d52397'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Show]</a></h3>
<ul id='ulmailcommon' style='display: none'>
<li>Fix Bug 370184 - Existing Filter search value field gets deleted when clicking on it. <a href='http://commits.kde.org/mailcommon/2cc7f743cc28a32604a934605c588932bdbd164e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370184'>#370184</a></li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Version bump to 0.25.2 (stable release). <a href='http://commits.kde.org/marble/e3d68b167c7dfe57640c605d7720e0af451a9885'>Commit.</a> </li>
<li>Explicitly link marblewidget to qtnetwork, for build without qtwebkit. <a href='http://commits.kde.org/marble/c4a0fe56ff73def588e17a903c54fe46909eec90'>Commit.</a> </li>
<li>Set Qt::AA_UseHighDpiPixmaps true for marble desktop apps. <a href='http://commits.kde.org/marble/3fdb1bdcb9c3d1cdda26d90ed8cfeb6ad727fe84'>Commit.</a> </li>
<li>Scale pixmap cache of MarbleGraphicsItem. <a href='http://commits.kde.org/marble/5b194b02ead2122b15470f2b6eaabe83d296dee4'>Commit.</a> </li>
<li>Fix vectorosm/legend.html, align with the others. <a href='http://commits.kde.org/marble/44019592e5a2ac940a72b4bf4350e3009767b0d6'>Commit.</a> </li>
<li>Fix: legends item either have a pixmap or a color, not both. <a href='http://commits.kde.org/marble/b5c9e0e9941bc52ebdcc9dbff70d65a724c48ee0'>Commit.</a> </li>
<li>Libmarblewidget: Make map legend translatable. <a href='http://commits.kde.org/marble/a74688d37aa896cecbac3c8eb89fa2d024e8b6be'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Don't enable add button when str is empty. <a href='http://commits.kde.org/messagelib/1ee25d6cf2cddd7ed8cb70bce46c68d5ef922f54'>Commit.</a> </li>
<li>Fix compiles. <a href='http://commits.kde.org/messagelib/7e0eccf49454a7cfc62c188df172efd62663b2bb'>Commit.</a> </li>
<li>Add missing script. <a href='http://commits.kde.org/messagelib/f4cd25052b2f80e12f7463bbbb0544df0896023e'>Commit.</a> </li>
<li>Fix Bug 369146 - Reading email advancing with "space" doesn't jump to next unread at end of current email. <a href='http://commits.kde.org/messagelib/31bdc626f6e4cf4a4ff55f01e2d9c415f4996cfa'>Commit.</a> See bug <a href='https://bugs.kde.org/369146'>#369146</a></li>
<li>Use script word id. <a href='http://commits.kde.org/messagelib/b060f7a9af61eeb0b628049b6932d448f36217fd'>Commit.</a> </li>
<li>Fix Bug 369555 - Emotions not shown in HTML message, only in subject field. <a href='http://commits.kde.org/messagelib/3cfee356ff445aac89eaba72ab7a6d92755227c3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369555'>#369555</a></li>
<li>Allow to show emoticon in these themes. <a href='http://commits.kde.org/messagelib/67066ddbb7f62b977bad1007896e0cae1170702e'>Commit.</a> See bug <a href='https://bugs.kde.org/369555'>#369555</a></li>
<li>Now we can use messageviewer without javascript enabled. <a href='http://commits.kde.org/messagelib/f601f9ffb706f7d3a5893b04f067a1f75da62c99'>Commit.</a> </li>
<li>Look at to execute javascript in specific wordid. <a href='http://commits.kde.org/messagelib/265b8b8e669ccb0a27ca3d5b6ee8b92d7838715a'>Commit.</a> </li>
<li>Fix Bug 369072 - Regression kMail 5.3.x: Plain-text quoting display is messed up. <a href='http://commits.kde.org/messagelib/cea1557b67d87709091acdae49987e0278a7c384'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369072'>#369072</a></li>
<li>Make it insensitive case. <a href='http://commits.kde.org/messagelib/0402c17a8ead92188971cb604d905b3072d56a73'>Commit.</a> </li>
<li>Add comment. <a href='http://commits.kde.org/messagelib/be7c6c11777b76c8dd44db805b98d187c35ad86e'>Commit.</a> </li>
<li>Remove script when we have multiline. <a href='http://commits.kde.org/messagelib/fb1be09360c812d24355076da544030a67b736fc'>Commit.</a> </li>
<li>Remove more <script>. <a href='http://commits.kde.org/messagelib/77976584a4ed2797437a2423704abdd7ece7834a'>Commit.</a> </li>
<li>Remove all <script*>*</script>. <a href='http://commits.kde.org/messagelib/a8744798dfdf8e41dd6a378e48662c66302b0019'>Commit.</a> </li>
<li>Remove script from mail directly. <a href='http://commits.kde.org/messagelib/3503b75e9c79c3861e182588a0737baf165abd23'>Commit.</a> </li>
<li>Add data test. <a href='http://commits.kde.org/messagelib/222f6732b14bd345699b0af8083ca36784e0cfce'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/0e67b543f8408213bd1601b0b04c197cddce69db'>Commit.</a> </li>
<li>Disable some js feature. <a href='http://commits.kde.org/messagelib/7fd908349fd633059ae3997877e280a0066478c6'>Commit.</a> </li>
<li>Add new test apps. <a href='http://commits.kde.org/messagelib/c6f320a02fb8a3de1f81142de16f3dfba69dc488'>Commit.</a> </li>
<li>MTP: fix crash when top-level node is generated by a formatter plugin. <a href='http://commits.kde.org/messagelib/c59f7ab4df81cb8e2f3c6cfee2ff6385c39ced83'>Commit.</a> </li>
<li>Fix show "To" in second line when first line is set as BCC (We have this. <a href='http://commits.kde.org/messagelib/f5947ef8c660a36bc5342526b1ce4a40538afbff'>Commit.</a> </li>
<li>Disable JavascriptCanAccessClipboard. <a href='http://commits.kde.org/messagelib/2e9e466fc6a338d2b9146ad0e61d85cecb8bc8e8'>Commit.</a> </li>
<li>Fix scroll to position. <a href='http://commits.kde.org/messagelib/56bebd82c58b846471d1a7964fbbd75670ab5d32'>Commit.</a> </li>
<li>Fix build with Qt 5.6. <a href='http://commits.kde.org/messagelib/afc933323d34c5c9cb94f6a0e997c381469bdf00'>Commit.</a> </li>
<li>Disable some feature not necessary in mail. <a href='http://commits.kde.org/messagelib/e7ecde76e9dfdf6c85928b8686bc20401c060f68'>Commit.</a> </li>
<li>Expire dying parent from threading cache before processing children. <a href='http://commits.kde.org/messagelib/c335c60684fb6de58fae567234c72277a3b1bf58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364994'>#364994</a></li>
<li>Hide accesskey when we switch message. <a href='http://commits.kde.org/messagelib/65a412f9f11e2914c57a021d02608155b69c1844'>Commit.</a> </li>
<li>NetworkAccessManagerWebEngine doesn't need to be a QNAM. <a href='http://commits.kde.org/messagelib/5f18931795db817b644660c5a03eb1c0a0dcc984'>Commit.</a> </li>
<li>Fix mem leak. <a href='http://commits.kde.org/messagelib/3f92e557a8678379002741be6aae92dbfae90d20'>Commit.</a> </li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Show]</a></h3>
<ul id='ulminuet' style='display: none'>
<li>Bump version to 0.2.1. <a href='http://commits.kde.org/minuet/190e16db9647b788595d00879b8eea27582e79f4'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Use SERVICE_TYPES arg with kcoreaddons_desktop_to_json. <a href='http://commits.kde.org/okteta/c57bb8a40a087fa2e534b8290e4263ee44d23c6c'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Open CBR files with shared-mime-info >= 1.7. <a href='http://commits.kde.org/okular/f2b2d94897cfedcec91b82a6bed0450546686b51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369078'>#369078</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Fix FTBFS against cups-2.2. <a href='http://commits.kde.org/print-manager/971e75934bc64627226d61565f68aff3c9b0000b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366483'>#366483</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'java import resolves templated class type into wrong java source'. <a href='http://commits.kde.org/umbrello/c990500e7204e5bd92a7ab5f94d95f2ebc8ea98d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368484'>#368484</a></li>
<li>Fix crash on document close caused by a dangled pointer in UMLListViewItem::m_object. <a href='http://commits.kde.org/umbrello/4ae4ef19e22403887bf7b19aff71ad1acb54b945'>Commit.</a> </li>
<li>Fix crash on document close caused by dangled pointer in UMLObject::m_pUmlPackage. <a href='http://commits.kde.org/umbrello/d7c8fa9f8374e3bad2898e7aa455fe7a789872fc'>Commit.</a> </li>
</ul>
