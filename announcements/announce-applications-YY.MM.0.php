<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => i18n_noop("KDE Ships KDE Applications YY.MM.0"),
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $release = 'applications-YY.MM.0';
  $version_text = "YY.MM";
  $version_number = "YY.MM.0";
?>

<script src="/js/use-ekko-lightbox.js" defer></script>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Applications %1", $version_text)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<?php i18n("January 01, 20YY.")?>
<br />
<?php print i18n_var("KDE Applications %1 are now released.", $version_text);?>
</p>

<!--figure class="videoBlock">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/VIDEO" allowfullscreen='true'></iframe>
</figure-->

<p>
<?php i18n("We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!");?>
</p>

<h2><?php print i18n_var("What's new in KDE Applications %1", $version_text);?></h2>

<p>
<?php i18n("More than 140 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello and more!");?>
</p>
<p>
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications-aether.php?version=".$version_number);?>
</p>

<h3 style="clear:both;"><?php i18n("System")?></h3>
<figure style="float: right; margin: 0px"><a href="dolphin1808-settings.png" data-toggle="lightbox"><img src="dolphin1808-settings.png" width="250" /></a></figure>

<?php print i18n_var("<a href='%1'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:", "https://www.kde.org/applications/system/dolphin/");?>
<ul>
<li><?php i18n("Improvement 1.");?></li>
<li><?php i18n("Improvement 2.");?></li>
</ul>

<h3 style="clear:both;"><?php i18n("Graphics")?></h3>

<h3 style="clear:both;"><?php i18n("Education")?></h3>

<h3 style="clear:both;"><?php i18n("Office")?></h3>

<h3 style="clear:both;"><?php i18n("Utilities")?></h3>


<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Linux App Stores");?></h2>
        <p>
            <?php print i18n_var("Flathub contains <a href='https://flathub.org/apps/search/kde'>%1 packages from KDE applications</a> updated at release time.", 35);?>
        </p>
        <a class="text-center" href="https://flathub.org/apps/search/kde"><img alt="<?php i18n("Get it from the Flathub");?>" src="/announcements/flathub.png" class="img-fluid mb-2" /></a>
        <p class="mt-4">
            <?php print i18n_var("The Snap Store contains <a href='https://snapcraft.io/publisher/kde'>%1 packages from KDE applications</a> updated at release time.", 52);?>
        </p>
        <a class="text-center" href="https://snapcraft.io/publisher/kde"><img alt="<?php i18n("Get it from the Snap Store");?>" src="/announcements/snapcraft.png" class="img-fluid" /> </a>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='http://download.kde.org/stable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version_text, $version_number, $version_number, $version_text);?></p>
    </article>
</section>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
?>
