<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.3.0");
  $site_root = "../";
  $release = '5.3.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
October 07, 2014. KDE today announces the release
of KDE Frameworks 5.3.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<?php i18n("
<h3>KActivities</h3>

<ul>
<li>Added DBus interface to load plugins at runtime</li>
</ul>

<h3>KArchive</h3>

<ul>
<li>Add convenience method KArchive::file()</li>
<li>Compilation fixes for MSVC</li>
</ul>

<h3>KBookmarks</h3>

<ul>
<li>Fix encoding issue in KIO places dialog (bug 337642)</li>
</ul>

<h3>KCMUtils</h3>

<ul>
<li>Fix initial size of KCMultiDialog (bugs 337494, 325897)</li>
</ul>

<h3>KCompletion</h3>

<ul>
<li>Fixed size hint and positioning of clear button in highDpi mode</li>
</ul>

<h3>KConfig</h3>

<ul>
<li>KConfigLoader: fix sharing of KSharedConfig objects</li>
</ul>

<h3>KConfigWidgets</h3>

<ul>
<li>Now provides the kf5_entry.desktop files it needs for KLanguageButton</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>Kdelibs4Migration: allow distributions to set KDE4_DEFAULT_HOME_POSTFIX
so that the kde4 home is found properly.</li>
<li>Compilation fixes for MSVC and gcc 4.5</li>
<li>Turn KFormat into a Q_GADGET so we can expose its properties to QML indirectly</li>
</ul>

<h3>KEmoticons</h3>

<ul>
<li>Add unicode characters to Glass emoticon theme</li>
</ul>

<h3>KGuiAddons</h3>

<ul>
<li>Make KFontUtils::adaptFontSize be a bit more exact</li>
</ul>

<h3>KI18n</h3>

<ul>
<li>Remove leftover Perl dependency</li>
</ul>

<h3>KIO</h3>

<ul>
<li>Now includes kio_trash </li>
<li>Add new KIO job, KIO::fileSystemFreeSpace, that allows you to get a filesystem's total and available space.</li>
<li>kshorturifilter: Remove redundant forward slashes from the beginning of an URI</li>
<li>Add searchprovider definitions for the qwant search engine</li>
<li>File dialog: fix relative paths being turned into HTTP URLs</li>
<li>Fix thumbnails for mimetype groups.</li>
</ul>

<h3>KJS</h3>

<ul>
<li>Implement Math.Clz32</li>
<li>U+0000 through U+001F is not allowed as char, but as escaped unicode sequence (bug 338970)</li>
</ul>

<h3>KNotifications</h3>

<ul>
<li>Avoid infinite recursion in KStatusNotifierItem / QSystemTray.</li>
</ul>

<h3>KService</h3>

<ul>
<li>Many many fixes to KPluginInfo</li>
<li>Add functions to convert between lists KPluginMetaData and KPluginInfo</li>
</ul>

<h3>KTextEditor</h3>

<ul>
<li>Multiple memory leaks fixed</li>
<li>Avoid auto-completion to interfere with search/replace text (bug 339130), and more autocompletion fixes</li>
<li>Many fixes to the VIM mode</li>
</ul>

<h3>KWidgetAddons</h3>

<ul>
<li>KPageListView: fixes for high-dpi mode</li>
<li>KPageWidget: margin fixes</li>
</ul>

<h3>KWindowSystem</h3>

<ul>
<li>NETWinInfo now provides convenient wrapper for WM_PROTOCOLS.</li>
<li>NETWinInfo now provides convenient wrapper for input and urgency hints of WM_HINTS property.</li>
</ul>

<h3>Solid</h3>

<ul>
<li>New freedesktop backend, not yet default but due to replace the upower/udev/systemd backends</li>
</ul>

<h3>Extra-cmake-modules</h3>

<ul>
<li>New module ECMGeneratePkgConfigFile, for frameworks to install a pkgconfig file.</li>
<li>New option ECM_ENABLE_SANITIZERS, to enable clang sanitizers. Example: ECM_ENABLE_SANITIZERS='address;undefined'</li>
<li>New option BUILD_COVERAGE, to enable code coverage.</li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>Fixed for compilation with Qt 5.4</li>
<li>Fixed a few standard shortcuts</li>
</ul>
");?>

<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>
<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%1/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release);?>

<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
