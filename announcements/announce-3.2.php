<?php
  $page_title = "Announcing KDE 3.2";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE FEBRUARY 3, 2004</p>

<h3 align="center">KDE Project Ships New Major Release Of Leading Open Source Desktop Environment</h3>

<img src="announce-3.2.jpeg" align="right" width="250" height="155" hspace="10" alt="Splash"/>

<p>February 3, 2004 (The Internet) - The <a href="http://www.kde.org">KDE Project</a> is pleased to announce the immediate availability of KDE 3.2, the third major release of the <a href="http://www.kde.org/awards">award-winning</a> KDE3 desktop platform. KDE 3.2 is the result of a combined year-long effort by hundreds of individuals and corporations from around the globe. This diverse team has been working successfully together since 1997 to make KDE the leading Open Source desktop software for Linux and UNIX.</p>

<p>As with previous KDE releases, version 3.2 provides an integrated desktop and a comprehensive set of applications that combine to create an environment that is usable for a wide variety of tasks right out of the box. In addition to the many new applications making their debut in KDE 3.2, the veteran applications have been refined and augmented generously. By installing some or all of these applications common desktop tasks such as web browsing, file management, email, <a href="http://kontact.org">personal information management</a>, <a href="http://kopete.kde.org">instant messaging</a>, <a href="http://www.kdevelop.org">software</a> and <a href="http://quanta.sf.net">web</a> development, multimedia, education and entertainment can be accomplished quickly and easily. This impressive collection of software is complemented by a recent update to the <a href="http://www.koffice.org/announcements/announce-1.3.php">KOffice </a> integrated office suite. </p>

<p>Reflecting its international team and focus, KDE 3.2 is currently available in 42 different languages. Partial translations into 32 other languages are also available, many of which are expected to be completed during the KDE 3.2 life cycle. With 74 different languages and full localization support, no other desktop is as ready to serve the needs of today's global community.</p>

<p>KDE 3.2 also provides improvements in usability and performance. Noticeable speed boosts in application start up times and webpage rendering together with many interface refinements make KDE 3.2 the most usable and performant KDE ever. Attention was also paid to ensuring that KDE is accessible to those with disabilities. Several accessibility related applications are included with 3.2 and work on integrating <a href="http://accessibility.kde.org">accessibility technologies</a> directly into KDE's foundations is ongoing.</p>

<p>KDE has earned a reputation for quality and a comprehensive feature set among its global user base that is estimated to number in the millions. KDE is also proud to be the default user interface for several operating systems including Ark Linux, Conectiva, Knoppix, Lindows, Lycoris, Mandrake Linux, SUSE Linux, TurboLinux and Xandros. KDE is also  available as a part of Debian, Free/Open/NetBSD, Gentoo, Libranet, Red Hat Linux, Slackware and Solaris, among others. In addition to these operating system vendors, more and more companies are offering commercial support for KDE, some of which are listed in the <a href="http://enterprise.kde.org/bizdir/">business directory</a> of the <a href="http://enterprise.kde.org">KDE::Enterprise website</a>. With the release of KDE 3.2, the KDE Project looks to enhance and grow this ecosystem of users and supporters.</p>

<h3>Highlights At A Glance</h3>

<p>Some of the highlights in KDE 3.2 are listed below. <!-- A more comprehensive look at what's new in 3.2 <a href="whatsnew/">can be found here</a> -->.</p>

<ul>
<li><em>Increased performance and standards compliance</em>
    <ul>
        <li>Lowered start up times for applications and hundreds of optimizations make KDE 3.2 the fastest KDE ever!</li>
        <li>Working in concert with Apple Computer Inc.'s Safari web browser team, KDE's web support has seen huge performance boosts as well as increased compliance with widely accepted web standards</li>
        <li>Increased support for <a href="http://freedesktop.org">FreeDesktop.org</a> standards in KDE 3.2 strengthens interoperability with other Linux and UNIX software.</li>
    </ul>
</li>
<li><em>New applications</em>
    <ul>
    <li>JuK: a jukebox-style music player</li>
    <li>Kopete: an instant messenger with support for AOL Instant Messenger, MSN, Yahoo Messenger, ICQ, Gadu-Gadu, Jabber, IRC, SMS and WinPopup</li>
    <li>KWallet: providing integrated, secure storage of passwords and web form data</li>
    <li>Kontact: a unified interface that draws KDE's email, calendaring, address book, notes and other PIM features together into a familiar configuration</li>
    <li>KGpg: providing an easy-to-use KDE interface to industry-standard encryption tools</li>
    <li>KIG: an interactive geometry program</li>
    <li>KSVG: a viewer for SVG files</li>
    <li>KMag, KMouseTool and KMouth: accessibility tools for KDE</li>
    <li>KGoldRunner: a new riff on a classic game</li>
    <li>... and many more!</li>
    </ul>
</li>
<li><em>Thousands of incremental improvements and bug fixes</em>
    <ul>
        <li>During the development of KDE 3.2 nearly 10,000 bug reports were processed via the <a href="http://bugs.kde.org">KDE Bug Tracking System</a></li>
        <li>Approximately 2,000 feature requests were also processed, with hundreds of requested features added to KDE applications and components</li>
        <li>An improved configuration system that opens the door to new installation management possibilities, improved roaming support and many improvements to the &quot;KDE Kiosk&quot; environment management system</li>
        <li>Inline spell checking for web forms and emails</li>
        <li>Improved email and calendaring support</li>
        <li>Powerful tabbed interface for the Konqueror file manager and web browser</li>
        <li>Support for Microsoft Windows desktop sharing protocol (RDP)</li>
    </ul>
</li>
<li><em>Improved Usability</em>
    <ul>
        <li>Reduced clutter in many menus and toolbars</li>
        <li>Many applications, dialogs and control panels reworked for clarity and utility</li>
    </ul>
</li>
<li><em>Enhanced appearance</em>
    <ul>
        <li>Plastik, a tastefully understated new look, debuts in KDE 3.2</li>
        <li>Hundreds of new icons improve the consistency and beauty of KDE</li>
        <li>Tweaks to the default look including new splash screens, (optionally) animated progress bars, styled panels and more!</li>
    </ul>
</li>
<li><em>New Tools for Software Developers</em>
    <ul>
        <li><a href="http://developer.kde.org/documentation/library/3.2-api/">Comprehensive API documentation</a> extended for 3.2</li>
        <li>Language bindings for ECMAScript (aka Javascript), Python, Java and Ruby</li>
        <li>New versions of the powerful <a href="http://www.kdevelop.org">KDevelop IDE</a> and <a href="http://quanta.sf.net">Quanta</a> web development suite</li>
        <li>Umbrello brings UML modeling for 11 different languages including C++, Java, SQL, PHP, Python and Perl to KDE</li>
    </ul>
</li>
</ul>

<p>
 For a more detailed list of improvements since the KDE 3.1 release, please refer to the <a href="changelogs/changelog3_1_5to3_2.php">KDE 3.2 Changelog</a>.
</p>

<h3>Getting KDE 3.2</h3>

<p>KDE 3.2 can be downloaded over the Internet by visiting <a href="http://download.kde.org/stable/3.2/">download.kde.org</a>. Source code and vendor supplied and supported binary packages are available. KDE 3.2 will also be featured in upcoming releases of various Linux and UNIX operating systems and can be purchased separately on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>. For additional information on package availability and to read further release notes, please
visit the <a href="http://www.kde.org/info/3.2.php">KDE 3.2 information page</a>.</p>

<p>KDE is available at no cost and all source code, art and documentation is freely available
under Open Source licenses. Additional third-party KDE applications can be downloaded from <a href="http://kde-apps.org">kde-apps.org</a>. KDE 3.2 is also compatible with other Linux and UNIX software packages including popular Open Source applications such as Open Office and the Mozilla web browser.</p>

<h3>Supporting KDE</h3>

<p>KDE is supported through voluntary contributions of time, money and resources by individuals
and companies from around the world. To discover how you or your company can join in and help
support KDE please visit the <a href="http://www.kde.org/community/donations/">Supporting KDE</a> web page. There
may be more ways to support KDE than you imagine, and every bit of support helps make KDE
a better project and a better product for everyone. Communicate your support today with a monetary donation,
new hardware or a few hours of your time!</p>

<h3>Further Information</h3>

<p>Want to explore KDE 3.2 further while waiting for your download to complete? Discover what's new in 3.2, learn about the community that is responsible for KDE and find out what KDE has to offer you as a user or a developer by following the links below.</p>

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="announce-3.2_users/whykde.php">Why KDE?</a><br/>
<a href="announce-3.2_users/deploying.php">Deploying KDE</a><br/>
<a href="announce-3.2_users/supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="announce-3.2_developers/whykde.php">Why KDE?</a><br/>
<a href="announce-3.2_developers/developing.php">Developing With KDE</a><br/>
<a href="announce-3.2_developers/supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>

<hr/>
<p style="font-size: 85%">
  <em>Press Release</em>:  Written by <a href="mail&#116;o&#x3a;&#00097;seig&#111;&#64;&#x6b;de&#x2e;o&#114;g">Aaron J. Seigo</a>.
  <br />
  <em>Release Coordinator</em>: <a href="mai&#108;to:&#x63;&#x6f;&#0111;l&#111;&#x40;k&#100;e&#46;&#111;&#x72;g">Stephan Kulow</a>.
</p>

<p style="font-size: 85%">
  <em>Trademarks Notices.</em>

  KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
  Incorporated.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
</p>

<hr/>

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;ai&#x6c;t&#x6f;&#00058;&#105;n&#102;&#x6f;&#45;&#x61;&#x66;r&#105;c&#0097;&#x40;k&#100;&#x65;&#x2e;o&#114;g">&#105;&#110;&#0102;&#00111;-&#x61;&#102;ri&#99;a&#x40;kde.o&#114;g</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#109;&#00097;&#105;l&#116;&#111;:info&#x2d;&#00097;&#115;ia&#64;&#x6b;&#100;&#0101;.o&#x72;g">&#x69;&#110;fo&#x2d;as&#105;a&#x40;&#107;&#x64;e&#x2e;&#111;rg</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="mai&#108;to&#x3a;in&#x66;o&#x2d;e&#0117;ro&#112;&#x65;&#00064;kde.o&#x72;g">&#x69;n&#102;&#111;-&#x65;u&#114;&#x6f;&#x70;&#101;&#0064;&#107;&#x64;e.&#x6f;&#x72;g</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="ma&#105;l&#116;&#x6f;:i&#x6e;&#x66;o&#x2d;no&#x72;&#0116;h&#00097;&#x6d;e&#114;i&#099;&#0097;&#64;k&#100;&#101;&#x2e;&#0111;&#114;&#x67;">&#105;&#110;&#102;o-&#00110;or&#116;h&#x61;me&#114;&#105;c&#097;&#x40;&#0107;de.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#97;&#105;&#x6c;&#x74;&#111;&#00058;i&#x6e;f&#111;-&#x6f;&#x63;&#x65;&#x61;n&#x69;a&#x40;&#00107;&#x64;&#x65;.or&#0103;">&#105;&#110;fo&#045;oc&#x65;a&#110;&#x69;&#97;&#00064;&#x6b;de&#46;o&#x72;&#103;</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;a&#105;&#108;&#x74;&#x6f;&#058;&#105;&#110;f&#x6f;-s&#x6f;u&#x74;&#104;&#x61;m&#x65;&#0114;ica&#x40;k&#x64;e.or&#x67;">&#x69;&#110;&#x66;o-&#115;ou&#00116;ha&#x6d;e&#x72;ica&#064;&#x6b;d&#101;&#0046;&#111;&#114;&#x67;</a><br />
</td>


</tr></table>

<?php
  include("footer.inc");
?>

