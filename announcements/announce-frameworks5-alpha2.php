<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Second Alpha of Frameworks 5");
  $site_root = "../";
  $release = '4.97.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<img src="frameworks5TP/images/KDE_QT.jpg" style="float: right; padding: 1ex; margin: 1ex; width: 300px;  border: 0; background-image: none; " alt="<?php i18n("Collaboration between Qt and KDE");?>" />
<?php i18n("March 3, 2014. Today KDE released the second alpha of Frameworks 5, part of a series of releases leading up to the final version planned for June 2014. This release includes progress since the <a href='announce-frameworks5-alpha.php'>previous alpha</a> in February.");?>
<br /><br />
<?php i18n("Efforts were directed towards getting frameworks to work on the MacOSX platform and <a href='http://blog.martin-graesslin.com/blog/2014/02/running-frameworks-powered-applications-on-wayland/'>Wayland on Linux</a>. Moreover, kprintutils is no more, kwallet-framework was renamed kwallet and some new frameworks have been added.");?>
<br /><br />
<p><?php i18n("For information about Frameworks 5, see <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article on the dot news site</a>. Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href='irc://#kde-devel@freenode.net'>#kde-devel IRC channel on freenode.net</a>.");?>
</p>

<h2><?php i18n("Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.");?>
</p>
<p>
<?php i18n("You can discuss this news story <a href='http://dot.kde.org/2014/03/04/kde-frameworks-5-alpha-two-out'>on the Dot</a>, KDE's news site.");?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing frameworks Alpha 2 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("A variety of distributions offers frequently updated packages of Frameworks 5. This includes Arch Linux, AOSC, Fedora, Kubuntu and openSUSE. See <a href='http://community.kde.org/Frameworks/Binary_Packages'>this wikipage</a> for an overview.");?>
</p>

<h4>
  <?php i18n("Compiling frameworks");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for frameworks %1 may be <a href='http://download.kde.org/unstable/frameworks/%1/'>freely downloaded</a>.", $release);?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
