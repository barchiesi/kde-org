<?php

  $page_title = "Anunci de publicació de KDE 3.5.7";
  $site_root = "../";
  include "header.inc";
?>

<!-- Other languages translations -->
També disponible en:
<a href="announce-3.5.7.php">Anglès</a>
<a href="announce-3.5.7-es.php">Espanyol</a>
<!--<a href="http://fr.kde.org/announcements/announce-3.5.7-fr.php">Francès</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.7.php">Islandès</a>-->
<a href="announce-3.5.7-it.php">Italià</a>
<a href="announce-3.5.7-nl.php">Holandès</a>
<a href="announce-3.5.7-pt.php">Portuguès</a>
<!--<a href="announce-3.5.7-pt-br.php">Portuguès del Brasil</a>
<a href="announce-3.5.7-sl.php">Eslovè</a>
<a href="announce-3.5.7-sv.php">Suec</a>
-->

<h3 align="center">
   El Projecte KDE publica la setena versió que millora les traduccions i els serveis
   de l'escriptori líder del programari lliure.
</h3>

<p align="justify">
  <strong>
    El KDE 3.5.7 incorpora traduccions a 65 idiomes, millores al conjunt de programes 
    de gestió d'informació personal del KDE i a altres aplicacions.
  </strong>
</p>

<p align="justify">
  22 de maig del 2007 (INTERNET).  Avui, el <a href="http://www.kde.org/">Projecte KDE</a> ha anunciat la disponibilitat immediata del KDE 3.5.7,
  una versió de manteniment de l'última generació de l'escriptori <em>lliure</em> més
  avançat i potent per al GNU/Linux i altres UNIX. El KDE té ara traducció a 
  <a href="http://l10n.kde.org/stats/gui/stable/">65 idiomes</a>,
  la qual cosa el fa disponible per a més persones que la majoria dels
  programes no lliures, i pot ser ampliat fàcilment a altres comunitats que vulguin contribuir al projecte de codi obert.
</p>

<p align="justify">
  Aquesta versió posa un èmfasi especial en les aplicacions de
  <a href="http://pim.kde.org/">gestió d'informació personal del KDE</a>. 
  S'han esmenat errors al 
  <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>, al
  <a href="http://kontact.kde.org/korganizer/">KOrganizer</a> i al
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a>, mentre que el 
  <a href="http://pim.kde.org/components/kmail.php">KMail</a> addicionalment 
  ha incorporat noves funcionalitats i millores, tant a la interfície de 
  treball com a la gestió de l'IMAP: pot gestionar quotes de l'IMAP i moure 
  totes les carpetes.
</p>

<p>
  Altres aplicacions han ampliat les funcionalitats:
</p>
<ul>
<li>
  El <a href="http://kpdf.kde.org/">KPDF</a> mostra rètols en 
  moure's per sobre dels enllaços, visualitza correctament els fitxers 
  PDF més complexos, com aquest 
  <a href="http://kpdf.kde.org/stuff/nytimes-firefox-final.pdf">
  anunci del Firefox</a> i reacciona a les ordres per obrir la subfinestra 
  de la taula de continguts.
</li>

<li>
  L'<a href="http://uml.sourceforge.net/">Umbrello</a> ara pot generar i 
  exportar codi C# i s'ha afegit la implementació dels "generics" del Java 5.
</li>

<li>
  El <a href="http://www.kdevelop.org/">KDevelop</a> té una actualització 
  important a la versió 3.4.1. Les noves funcionalitats inclouen moltes millores 
  a la compleció de codi i la navegació, una interfície de depuració més 
  segura, pot treballar amb el Qt4, i millora el desenvolupament en Ruby i KDE4.
</li>
</ul>

<p>
  Addicionalment a les noves funcionalitats, hi ha molts errors solucionats 
  a tot el projecte, especialment als paquets 
  d'<a href="http://edu.kde.org/">Educació</a>
  i <a href="http://games.kde.org/">Jocs</a> i al 
  <a href="http://kopete.kde.org/">Kopete</a>. A més de les esmenes d'errors, 
  el Kopete també ha tingut millores de rendiment en la representació dels xats.
</p>

<p>
  Com els usuaris del KDE ja esperaven, en aquesta nova versió continua la 
  feina en el KHTML i el KJS, els motors de l'HTML i el Javascript del KDE. Hi ha una nova i interessant funcionalitat d'usabilitat en el KHTML que fa que el 
  cursor del ratolí indiqui si un enllaç s'ha d'obrir en una finestra nova del 
  navegador o no.
</p>

<p align="justify">
  Per a una llista més detallada de millores des de la 
  <a href="http://www.kde.org/announcements/announce-3.5.6.php">versió KDE 3.5.6</a>
  del 25 de gener del 2007, si us plau, consulteu la pàgina de canvis del 
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_6to3_5_7.php">KDE 3.5.7</a>.
</p>

<p align="justify">
  El KDE 3.5.7 es distribueix amb un escriptori bàsic i quinze paquets addicionals 
  (gestió d'informació personal, administració, xarxes, educació, utilitats,  multimèdia, jocs, art, desenvolupament web, i altres). Les eines del KDE més 
  premiades i les aplicacions estan disponibles en <strong>65 idiomes</strong>.
</p>

<h4>
  Distribucions amb el KDE
</h4>
<p align="justify">
  La majoria de distribucions de Linux i sistemes operatius UNIX no incorporaran
  immediatament les noves versions de KDE, sinó que integraran els paquets del KDE
  3.5.7 en les seves properes versions. Consulteu
  <a href="http://www.kde.org/download/distributions.php">aquesta llista</a>
  per veure quines distribucions porten el KDE.
</p>

<h4>
  Instal·lació de paquets binaris del KDE 3.5.7
</h4>
<p align="justify">
  <em>Creadors de paquets</em>.
  Alguns proveïdors de sistemes operatius han proporcionat amablement els
  paquets binaris del KDE 3.5.7 per a algunes versions de les seves distribucions,
  en altres casos han estat voluntaris de la comunitat els que ho han fet. 
  Alguns d'aquests paquets es poden descarregar lliurement del servidor de 
  descàrregues del KDE a 
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/">http://download.kde.org</a>.
  En properes setmanes poden haver-hi paquets nous així com millores als paquets
  disponibles. 
</p>

<p align="justify">
  <a name="package_locations"><em>Ubicació dels paquets</em></a>.
  Consulteu la pàgina d'informació del 
  <a href="/info/3.5.7.php">KDE 3.5.7</a> per obtenir una llista dels paquets 
  binaris disponibles actualment dels que s'ha informat al Projecte KDE.
</p>

<h4>
  Com compilar el KDE 3.5.7
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codi font</em>.
  El codi complet del KDE 3.5.7 es pot
  <a href="http://download.kde.org/stable/3.5.7/src/">descarregar 
  lliurement</a>. Les instruccions de com compilar i instal·lar el KDE 3.5.7
  estan disponibles a la pàgina d'informació del <a href="/info/3.5.7.php">KDE
  3.5.7</a>.
</p>

<h4>
  Com col·laborar amb el KDE
</h4>
<p align="justify">
  El KDE és un projecte de
  <a href="http://www.gnu.org/philosophy/free-sw.html">programari lliure</a>
  que existeix i creix gràcies a molts voluntaris que donen el seu temps i
  esforç. El KDE sempre busca nous voluntaris i contribucions, ja sigui per
  programar, corregir o informar d'errors, escriure documentació, traduir, 
  ajudar en la promoció, aportar diners, etc. Totes les contribucions s'accepten 
  amb agraïment. Si us plau, per a més informació llegiu <a href="/community/donations/">Com col·laborar amb el KDE</a>.</p>

<p align="justify">
Esperem tindre notícies vostres ben aviat!
</p>

<h4>
  Quant al KDE
</h4>
<p align="justify">
  El KDE és un projecte <a href="/awards/">premiat</a> i independent format per
  <a href="/people/">centenars</a> de desenvolupadors, traductors, artistes i
  altres professionals d'arreu del món, col·laborant gràcies a Internet per
  crear un entorn d'escriptori i d'oficina de lliure distribució, sofisticat,
  personalitzable i estable que utilitza una arquitectura basada en components,
  transparent a la xarxa i ofereix una magnífica plataforma de desenvolupament.</p>

<p align="justify">
  El KDE proporciona un escriptori madur i estable que inclou un navegador 
  (<a href="http://konqueror.kde.org/">Konqueror</a>), un conjunt d'utilitats de
  gestió d'informació personal (<a href="http://kontact.org/">Kontact</a>), un
  conjunt d'aplicacions d'oficina complet
  (<a href="http://www.koffice.org/">KOffice</a>), una gran quantitat
  d'aplicacions i utilitats de xarxa, i un eficient i intuïtiu entorn integrat de
  desenvolupament (<a href="http://www.kdevelop.org/">KDevelop</a>).</p>

<p align="justify">
  KDE és la prova real que el model de desenvolupament de programari lliure
  "estil Basar" pot crear tecnologies de primer nivell a la par o superiors que
  el més complex del programari comercial.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Notes en quant a marques registrades.</em>
  KDE<sup>&#174;</sup> i el logo K Desktop Environment<sup>&#174;</sup> són
  marques registrades de KDE e.V.

  Linux és una marca registrada de Linus Torvalds.

  UNIX és una marca registrada de The Open Group als Estats Units i d'altres
  països.

  Totes les altres marques registrades i copyrights esmentats en aquest anuncis
  són propietat dels respectius propietaris.
  </font>
</p>

<hr />

<h4>Contactes de premsa</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia and India</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
