<?php
  $page_title = "KDE 1.90 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<P>DATELINE MAY 12, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KDE Desktop Available for Linux<SUP>&reg;</SUP></H3>
<P><STRONG>Beta Preview of Advanced Linux<SUP>&reg;</SUP> Desktop</STRONG></P>
<P>May 11, 2000 (world, Internet).  The <A HREF="http://www.kde.org">KDE
Team</A> today announced the release of KDE 1.90, codenamed "Konfucious",
a beta preview of KDE's
next-generation, powerful, modular desktop.  Following on the heels of the
release of KDE 1.89 (Krash) in December 1999, Konfucious is based on
<A HREF="http://www.trolltech.com">Qt 2.1</A> and will include the core
libraries, the core desktop environment, the KOffice suite, as well
as most of the other standard base KDE packages:  kdegames,
kdenetwork, kdetoys and kdeutils.  Konfucious is targeted at
<A HREF="#developer">developers</A> and interested
<A HREF="#user">users</A>.  For those compiling from source, please consult
the <A HREF="http://developer.kde.org/build/index.html">compilation
instructions</A>.
</P>
<P>
"With KDE 2.0, Linux will be ready to tackle the corporate and
consumer desktop," predicted Waldo Bastian, a core KDE developer.
"This release presents a great opportunity for developers to prepare for the
KDE 2.0 release and for users to preview and contribute to
one of the most anticipated Open Source releases," added Kurt Granroth,
KDE core developer and evangelist.
</P>
<P>
<A NAME="developer"><STRONG>For the developer</STRONG></A>, KDE 1.90
provides a stable API
which will enable developers to commence serious development of their
application so they may time the release of their software to coincide
with the release of KDE 2.0, scheduled for September 2000. It is
anticipated that with the exception of aRts, the budding KDE 2.0 multimedia
engine, and the <A HREF="#Style engine">KDE style engine</A>, there will
be few binary incompatible and fewer,
if any, source incompatible changes in the core libraries through
the 2.0 release.  Further development will focus on finalizing
aRts, fixing bugs, complying with the
<A HREF="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guides</A> and maximizing performance, as well as completing
testing of the other KDE base packages (kdenetwork, kdegames, etc.).
</P>
<P>
Konfucious offers a large number of major technological improvements
to developers compared to
the critically acclaimed KDE 1.x series.  Chief among these are
the Desktop COmmunication Protocol (DCOP), the i/o libraries (KIO),
the component object model (KParts), an XML-based GUI class, and 
the standards-compliant HTML rendering engine (KHTML).
</P>
<UL>
<LI>
DCOP is a client-to-client communications protocol intermediated by a
server.  The protocol supports both message passing and remote
procedure calls.  The technology is used in KDE 1.90, for example,
to direct application requests to instances of the application which
are already running, thereby preventing multiple occurrences of the
same application from running concurrently.
</LI>
<BR> <BR>
<LI>
<A NAME="KIO">KIO</A> implements i/o in a separate process
to permit a non-blocking
GUI.  The class is network transparent and hence can be used seamlessly
to access HTTP, FTP, Gopher, POP, IMAP, NFS, SMB, LDAP and local files. 
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.  KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology (described
<A HREF="#KParts">below</A>).
</LI>
<BR> <BR>
<LI>
<A NAME="KParts">KParts</A>, the KDE component object model, allows
one process to embed another within itself.  The technology handles
all aspects of the embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with
the <A HREF="#KIO">KIO</A> trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<A HREF="#KOffice">KOffice suite</A> and
<A HREF="#Konqueror">Konqueror</A>.
</LI>
<BR> <BR>
<LI>
The XML GUI employs XML to create and position menus, toolbars and possibly
other aspects of the GUI. This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<A HREF="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</A> irrespective of modifications to the standards.
</LI>
<BR> <BR>
<LI>
<A NAME="KHTML">KHTML</A> is an HTML 4.0 compliant
rendering and drawing engine.  The class
will support the full gamut of current Internet technologies, including
JavaScript<SUP>TM</SUP>, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<SUP>&reg;</SUP> plugins (for
viewing Flash<SUP>TM</SUP>,
RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP> and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal X Window
parenting) or as a component (using the <A HREF="#KParts">KParts</A>
technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.
</LI>
</UL>
<P>
<A NAME="user"><STRONG>For the interested user</STRONG></A>, 
KDE 1.90 offers a fairly stable desktop suitable for a
non-critical environment. Users who
would like the opportunity to contribute to the further development of
KDE can use this release as a basis for offering suggestions and bug
reports, or those who are curious can evaluate the new frontier of
the *nix desktop.
</P>
<P>
The attractions of Konfucious to users are manifold and impressive. 
The principal
benefits to users lie in KDE's enhanced customizability, the
<A HREF="http://koffice.kde.org">KOffice suite</A>, the cutting-edge
technologies provided by <A HREF="http://www.konqueror.org">Konqueror</A>,
and full Unicode support.
</P>
<UL>
<LI>
KDE's customizability touches every aspect of this next-generation
desktop.  <A NAME="Style engine">Konfucious benefits from Qt's
style engine, which permits developers and artists to create their
own widget designs down to the precise appearance of a scrollbar,
a button, a menu and more, combined with development tools which will
largely automate the creation of these widget sets (note that the configuration
files for the style engine will change in an incompatible way prior to
the next KDE release scheduled for June 2000).</A>  Just to
mention a few of the legion configuration options,
users can choose among: numerous types of menu effects; a menu
bar atop the display (Macintosh<SUP>&reg;</SUP>-style) or atop each individual
window (Windows-style); icon styles; system sounds; key bindings;
languages; toolbar and menu composition; and much much more.
</LI>
<BR> <BR>
<LI>
<A NAME="KOffice">The KOffice suite</A>, long ago heralded as a
<A HREF="http://www.mieterra.com/article/koffice.html">"killer app"</A>,
is one of the most-anticipated Open Source projects.  The suite consists
of a spreadsheet application (KSpread), a vector drawing application
(KIllustrator), a bitmap drawing application (KImageShop), a frame-based
word-processing application (KWord), a chart and diagram application
(KChart), a formula editor (KFormula) and a simple image
viewer (KImage).  Native file formats will use XML, and work on
filters for proprietary binary file formats is progressing.
Combined with a powerful scripting language and the
ability to embed individuals components within each other using the
<A HREF="#KParts">KParts</A> technology, the KOffice
suite will provide all the necessary functionality to all but the most
demanding power users, at an unbeatable price -- free.
</LI>
<BR> <BR>
<LI>
<A NAME="Konqueror">Konqueror</A> stands tall as the next-generation
web browser, file manager and
document viewer for KDE 2.0.  Widely acclaimed as a technological
break-through for the Linux desktop, Konqueror has a component-based
architecture which combines the features and functionality of Internet
Explorer<SUP>&reg;</SUP>/Netscape Communicator<SUP>&reg;</SUP> and
Windows Explorer<SUP>&reg;</SUP>.  Konqueror supports
all major Internet technologies supported by <A HREF="#KHTML">KHTML</A>.
In addition,
Konqueror's network transparency offers seamless support for browsing
Linux<SUP>&reg;</SUP> NFS shares, Windows<SUP>&reg;</SUP> SMB shares,
HTTP pages, FTP directories as well as any other protocol for which
a <A HREF="#KIO">KIO</A> plug-in is available.
</LI>
<BR> <BR>
<LI>
<A NAME="Unicode">KDE 2.0 will support Unicode</A> at its very core,
the outstanding Qt toolkit.  In addition, KHTML support includes
bidirectional scripts, such as Arabic and Hebrew, and
Far Eastern languages (Chinese/Japanese/Korean).  Combined with the 21
separate teams actively translating KDE into other languages, KDE 2.0
will truly be an international desktop.
</LI>
</UL>

<h4>WHERE TO DOWNLOAD SOURCE PACKAGES</h4>
 
<p>
Konfucious source packages can be downloaded from: <a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/src">
ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/src</a></p>

<p>
Or one of its <A HREF="../mirrors/ftp.php">mirror</A> sites.
</p>

<p> 
Konfucious requires the recently released version 2.1 of the <A HREF="http://www.trolltech.com">Qt</A> toolkit.
 
The source package of Qt 2.1 is available at this <A href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.1.0.tar.gz">location</A> for your <br>
convenience.  Please note that this stable version of Qt has been officially released by Trolltech and is not part of the beta testing of KDE 1.90.<br>

<P> Konfucious does NOT work with Qt 1.x or Qt 2.0.
</p>

<P> If your compilation of the sources does fail at some point please have a look at the <A HREF="http://www.kde.org/compilationfaq.html">Compilation FAQ</A>.

<h4>WHERE TO DOWNLOAD BINARY PACKAGES</h4>
 
<p>
Konfucious binary packages will are be available later this week from:
</p>

<p><a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/">
ftp://ftp.kde.org/pub/kde/unstable/distribution</a></p>

Currently you can find there rpms for 
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/rpm/Caldera-2.4/">
Caldera 2.4</A> and <A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/rpm/RedHat-6.2">Redhat 6.2</A>.

<p>
Or one of its <A HREF="../mirrors/ftp.php">mirror</A> sites.
</p>

<H4>About KDE</H4>
<P>KDE is a collaborative project by hundreds of developers worldwide to
create a sophisticated, customizable and stable desktop environment
employing a network-transparent, intuitive user interface.  Currently
development is focused on KDE 2, which will for the first time offer a
free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create technologies on par with and superior
to even the most complex commercial software.</P>

<P>For more information about KDE, please visit KDE's <A HREF="../whatiskde/">web site</A>.</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;&#x72;&#97;n&#114;o&#x74;h&#x40;kde.&#00111;&#x72;&#x67;<BR>
(1) 480 732 1752
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
fa&#x75;re&#064;k&#00100;e.&#111;&#114;g<BR>
(44) 1225 471 300
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (German and English):
</TD><TD NOWRAP>
Martin Konold<BR>
&#00107;o&#x6e;&#x6f;ld&#x40;kde&#0046;&#x6f;rg<BR>
(49) 177 7473 202
</TD></TR>
</TABLE>

<?php
  include "footer.inc"
?>
