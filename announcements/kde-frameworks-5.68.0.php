<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.68.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.68.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
March 07, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.68.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[ModifiedFileIndexer] Correct time checks for new files");?></li>
<li><?php i18n("[ModifiedFileIndexer] Omit BasicIndexingJob run when not required");?></li>
<li><?php i18n("Sync IndexerConfig on exit (bug 417127)");?></li>
<li><?php i18n("[FileIndexScheduler] Force evaluation of indexerState after suspend/resume");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Fix errors in the QRegularExpression porting commit");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add network-wireless-hotspot icon");?></li>
<li><?php i18n("Move telegram panel icons to status category");?></li>
<li><?php i18n("[breeze-icons] Add telegram-desktop tray icons (bug 417583)");?></li>
<li><?php i18n("[breeze-icons] New 48px telegram icon");?></li>
<li><?php i18n("Add rss icons in action");?></li>
<li><?php i18n("Remove 48px telegram icons");?></li>
<li><?php i18n("Hotfix to make sure validation is not done in parallel to generation");?></li>
<li><?php i18n("New yakuake logo/icon");?></li>
<li><?php i18n("Fix inconsistencies and duplicates in network-wired/wireless icons");?></li>
<li><?php i18n("Fix old text color values for osd-* icons");?></li>
<li><?php i18n("only install generated icons if they were generated");?></li>
<li><?php i18n("escape all paths to ensure the CI system works");?></li>
<li><?php i18n("set -e on the generator script so it properly errors out on errors");?></li>
<li><?php i18n("build: fix the build where install prefix is not user-writable");?></li>
<li><?php i18n("hotfix new 24px generator to use bash instead of sh");?></li>
<li><?php i18n("Also auto-generate 24@2x compatibility symlinks");?></li>
<li><?php i18n("Auto-generate 24px monochrome icons");?></li>
<li><?php i18n("Add icons that were only in actions/24 to actions/22");?></li>
<li><?php i18n("Set document scale to 1.0 for all actions/22 icons");?></li>
<li><?php i18n("Add new <code>smiley-add</code> icons");?></li>
<li><?php i18n("Make shapes and shape-choose icons consistent with other -shape icons");?></li>
<li><?php i18n("Make smiley-shape consistent with other -shape icons");?></li>
<li><?php i18n("Make flower-shape and hexagon-shape icons consistent with other -shape icons");?></li>
<li><?php i18n("Replace &lt;use/&gt; with &lt;path/&gt; in muondiscover.svg");?></li>
<li><?php i18n("Add status icons: data-error, data-warning, data-information");?></li>
<li><?php i18n("Add icon for org.kde.Ikona");?></li>
<li><?php i18n("add vvave icon");?></li>
<li><?php i18n("add puremaps icon");?></li>
<li><?php i18n("Unify the look of all icons containing 🚫  (no sign)");?></li>
<li><?php i18n("New icon for KTimeTracker (bug 410708)");?></li>
<li><?php i18n("Optimize KTrip and KDE Itinerary icons");?></li>
<li><?php i18n("update travel-family icons");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Support NDK r20 and Qt 5.14");?></li>
<li><?php i18n("Load QM files from assets: URLs on Android");?></li>
<li><?php i18n("Add ecm_qt_install_logging_categories &amp; ecm_qt_export_logging_category");?></li>
<li><?php i18n("ECMGeneratePriFile: unbreak for usages with LIB_NAME not a target name");?></li>
<li><?php i18n("ECMGeneratePriFile: Fix static configurations");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("[KStyle] Set the color of KMessageWidgets to the correct one from the current color scheme");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fix issue finding the Boost include directories");?></li>
<li><?php i18n("Use exposed DBus methods to switch activities in CLI");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("[KAuth] Add support for action details in Polkit1 backend");?></li>
<li><?php i18n("[policy-gen] Fix the code to actually use the correct capture group");?></li>
<li><?php i18n("Drop Policykit backend");?></li>
<li><?php i18n("[polkit-1] Simplify Polkit1Backend action exists lookup");?></li>
<li><?php i18n("[polkit-1] Return an error status in actionStatus if there is an error");?></li>
<li><?php i18n("Calculate KAuthAction::isValid on demand");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Rename actions to be consistent");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Update visibility cache when notebook visibility is changed");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Check activeModule before using it (bug 417396)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("[KConfigGui] Clear styleName font property for Regular font styles (bug 378523)");?></li>
<li><?php i18n("Fix code generation for entries with min/max (bug 418146)");?></li>
<li><?php i18n("KConfigSkeletonItem : allow to set a KconfigGroup to read and write items in nested groups");?></li>
<li><?php i18n("Fix is&lt;PropertyName&gt;Immutable generated property");?></li>
<li><?php i18n("Add setNotifyFunction to KPropertySkeletonItem");?></li>
<li><?php i18n("Add an is&lt;PropertyName&gt;Immutable to know if a property is immutable");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Change \"Redisplay\" to \"Refresh\"");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("add hint that QIcon can be used as a program logo");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Deprecate KDBusConnectionPool");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Expose capture signal on KeySequenceItem");?></li>
<li><?php i18n("Fix size of the header in GridViewKCM (bug 417347)");?></li>
<li><?php i18n("Allow ManagedConfigModule derived class to register explicitly KCoreConfigSkeleton");?></li>
<li><?php i18n("Allow to use KPropertySkeletonItem in ManagedConfigModule");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Add a --replace option to kded5");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("[UrlHandler] Handle opening the online docs for KCM modules");?></li>
<li><?php i18n("[KColorUtils] Change getHcy() hue range to [0.0, 1.0)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Update Japanese holidays");?></li>
<li><?php i18n("holiday_jp_ja - fix spelling for National Foundation Day (bug 417498)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Support Qt 5.14 on Android");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Make kwrapper/kshell spawn klauncher5 if needed");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[KFileFilterCombo] Don't add invalid QMimeType to mimes filter (bug 417355)");?></li>
<li><?php i18n("[src/kcms/*] Replace foreach (deprecated) with range/index-based for");?></li>
<li><?php i18n("KIO::iconNameForUrl(): handle the case of a file/folder under trash:/");?></li>
<li><?php i18n("[krun] Share implementation of runService and runApplication");?></li>
<li><?php i18n("[krun] Drop KToolInvocation support from KRun::runService");?></li>
<li><?php i18n("Improve KDirModel to avoid showing '+' if there are no subdirs");?></li>
<li><?php i18n("Fix running konsole on Wayland (bug 408497)");?></li>
<li><?php i18n("KIO::iconNameForUrl: fix searching for kde protocol icons (bug 417069)");?></li>
<li><?php i18n("Correct capitalization for \"basic link\" item");?></li>
<li><?php i18n("Change \"AutoSkip\" to \"Skip All\" (bug 416964)");?></li>
<li><?php i18n("Fix memory leak in KUrlNavigatorPlacesSelector::updateMenu");?></li>
<li><?php i18n("file ioslave: stop copying as soon as the ioslave is killed");?></li>
<li><?php i18n("[KOpenWithDialog] Automatically select the result if the model filter has only one match (bug 400725)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Show tooltip with full URL for URL button with overridden text");?></li>
<li><?php i18n("Have pullback toolbars on scrollable pages also for footers");?></li>
<li><?php i18n("Fix PrivateActionToolButton behaviour with showText vs IconOnly");?></li>
<li><?php i18n("Fix ActionToolBar/PrivateActionToolButton in combination with QQC2 Action");?></li>
<li><?php i18n("Move checked menu item always into range");?></li>
<li><?php i18n("Watch for language change events, and forward those to the QML engine");?></li>
<li><?php i18n("Support Qt 5.14 on Android");?></li>
<li><?php i18n("don't have overlaysheets under page header");?></li>
<li><?php i18n("use fallback when icon failed to load");?></li>
<li><?php i18n("Missing links to pagepool source files");?></li>
<li><?php i18n("Icon: fix rendering of image: urls on High DPI (bug 417647)");?></li>
<li><?php i18n("Do not crash when icon's width or height is 0 (bug 417844)");?></li>
<li><?php i18n("fix margins in OverlaySheet");?></li>
<li><?php i18n("[examples/simplechatapp] Always set isMenu to true");?></li>
<li><?php i18n("[RFC] Reduce size of Level 1 headings and increase left padding on page titles");?></li>
<li><?php i18n("properly sync size hints with state machine (bug 417351)");?></li>
<li><?php i18n("Add support for static platformtheme plugins");?></li>
<li><?php i18n("make headerParent correctly aligned when there is a scrollbar");?></li>
<li><?php i18n("Fix tabbar width computation");?></li>
<li><?php i18n("Add PagePoolAction to QRC file");?></li>
<li><?php i18n("allow toolbar style on mobile");?></li>
<li><?php i18n("Make the api docs reflect that Kirigami is not only a mobile toolkit");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRearrangeColumnsProxyModel: temporarily disable assert due to QTreeView bug");?></li>
<li><?php i18n("KRearrangeColumnsProxyModel: reset in setSourceColumns()");?></li>
<li><?php i18n("Move Plasma's SortFilterProxyModel into KItemModel's QML plugin");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Expose the evaluation timeout management functions in public API");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix clicking thumb-only delegate (bug 418368)");?></li>
<li><?php i18n("Fix scrolling on the EntryDetails page (bug 418191)");?></li>
<li><?php i18n("Don't double delete CommentsModel (bug 417802)");?></li>
<li><?php i18n("Cover also the qtquick plugin in the installed categories file");?></li>
<li><?php i18n("Use the right translation catalog to show translations");?></li>
<li><?php i18n("Fix the KNSQuick Dialog's close title and basic layout (bug 414682)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Make kstatusnotifieritem available without dbus");?></li>
<li><?php i18n("Adapt action numbering in Android to work like in KNotifications");?></li>
<li><?php i18n("Write down Kai-Uwe as the knotifications maintainer");?></li>
<li><?php i18n("Always strip html if server does not support it");?></li>
<li><?php i18n("[android] Emit defaultActivated when tapping the notification");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("fix pri file generation");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Do not print errors about invalid roles when roleName is not set");?></li>
<li><?php i18n("Use offscreen platform for tests on Windows");?></li>
<li><?php i18n("Remove glsl validator download from validation script");?></li>
<li><?php i18n("Fix validation error in line chart shader");?></li>
<li><?php i18n("Update linechart core profile shader to match compat");?></li>
<li><?php i18n("Add comment about bounds checking");?></li>
<li><?php i18n("LineChart: Add support for min/max y bounds checking");?></li>
<li><?php i18n("Add sdf_rectangle function to sdf library");?></li>
<li><?php i18n("[linechart] Guard against divide by 0");?></li>
<li><?php i18n("Line charts: Reduce the number of points per segment");?></li>
<li><?php i18n("Don't lose points at the end of a line chart");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Qt5::UiTools is not optional in this module");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("New query mechanism for applications: KApplicationTrader");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add an option to dynamic-break inside words");?></li>
<li><?php i18n("KateModeMenuList: don't overlap the scroll bar");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add application menu dbus paths to org_kde_plasma_window interface");?></li>
<li><?php i18n("Registry: don't destroy the callback on globalsync");?></li>
<li><?php i18n("[surface] Fix buffer offset when attaching buffers to surfaces");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KMessageWidget] Allow the style to change our palette");?></li>
<li><?php i18n("[KMessageWidget] Draw it with QPainter instead of using stylesheet");?></li>
<li><?php i18n("Slightly reduce level 1 heading size");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Drop qmake pri file generation &amp; installation, currently broken");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Support SAE in securityTypeFromConnectionSetting");?></li>
<li><?php i18n("Drop qmake pri file generation &amp; installation, currently broken");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Support data-error/warning/information also in 32,46,64,128 sizes");?></li>
<li><?php i18n("Add \"plugins\" action item, to match Breeze icons");?></li>
<li><?php i18n("Add status icons: data-error, data-warning, data-information");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Buttons: allow to scale up icons");?></li>
<li><?php i18n("Try to apply the colorscheme of the current theme to QIcons (bug 417780)");?></li>
<li><?php i18n("Dialog: disconnect from QWindow signals in destructor");?></li>
<li><?php i18n("Fix memory leak in ConfigView and Dialog");?></li>
<li><?php i18n("fix layout size hints for button labels");?></li>
<li><?php i18n("make sure the size hints are integer and even");?></li>
<li><?php i18n("support icon.width/height (bug 417514)");?></li>
<li><?php i18n("Remove hardcoded colors (bug 417511)");?></li>
<li><?php i18n("Construct NullEngine with KPluginMetaData() (bug 417548)");?></li>
<li><?php i18n("Slightly reduce level 1 heading size");?></li>
<li><?php i18n("Vertically center tooltip icon/image");?></li>
<li><?php i18n("support display property for Buttons");?></li>
<li><?php i18n("Don't warn for invalid plugin metadata (bug 412464)");?></li>
<li><?php i18n("tooltips always have normal colorgroup");?></li>
<li><?php i18n("[Tests] Make radiobutton3.qml use PC3");?></li>
<li><?php i18n("Optimize code when dropping files into the desktop (bug 415917)");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Fix pri file to not fail with CamelCase includes");?></li>
<li><?php i18n("Fix pri file to have qmake name of QtGui as dependency");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Rewrite nextcloud plugin");?></li>
<li><?php i18n("Kill twitter support");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("ScrollView: Use scrollbar height as bottom padding, not width");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Fix inverted logic in IOKitStorage::isRemovable");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix segfault at exit");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix out-of-memory due to too large context stacks");?></li>
<li><?php i18n("General update for CartoCSS syntax highlighting");?></li>
<li><?php i18n("Add syntax highlighting for Java Properties");?></li>
<li><?php i18n("TypeScript: add private fields and type-only imports/exports, and some fixes");?></li>
<li><?php i18n("Add FreeCAD FCMacro extension to the python highlighting definition");?></li>
<li><?php i18n("Updates for CMake 3.17");?></li>
<li><?php i18n("C++: constinit keyword and std::format syntax for strings. Improvement printf format");?></li>
<li><?php i18n("RPM spec: various improvements");?></li>
<li><?php i18n("Makefile highlight: fix variable names in \"else\" conditionals (bug 417379)");?></li>
<li><?php i18n("Add syntax highlighting for Solidity");?></li>
<li><?php i18n("Small improvements in some XML files");?></li>
<li><?php i18n("Makefile highlight: add substitutions (bug 416685)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.68");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
