<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.12.0");
  $site_root = "../";
  $release = '5.12.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
July 10, 2015. KDE today announces the release
of KDE Frameworks 5.12.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Improve error reporting of query_qmake macro");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Remove all devices from adapter before removing the adapter (bug 349363)");?></li>
<li><?php i18n("Update links in README.md");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Adding the option not to track the user when in specific activities (similar to the 'private browsing' mode in a web browser)");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Preserve executable permissions from files in copyTo()");?></li>
<li><?php i18n("Clarify ~KArchive by removing dead code.");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Make it possible to use kauth-policy-gen from different sources");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Don't add a bookmark with url is empty and text is empty");?></li>
<li><?php i18n("Encode KBookmark URL to fix compatibility with KDE4 applications");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Remove x-euc-tw prober");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Install kconfig_compiler into libexec");?></li>
<li><?php i18n("New code generation option TranslationDomain=, for use with TranslationSystem=kde; normally needed in libraries.");?></li>
<li><?php i18n("Make it possible to use kconfig_compiler from different sources");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KDirWatch: Only establish a connection to FAM if requested");?></li>
<li><?php i18n("Allow filtering plugins and applications by formfactor");?></li>
<li><?php i18n("Make it possible to use desktoptojson from different sources");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Clarify exit value for Unique instances");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Add QQC clone of KColorButton");?></li>
<li><?php i18n("Assign a QmlObject for each kdeclarative instance when possible");?></li>
<li><?php i18n("Make Qt.quit() from QML code work");?></li>
<li><?php i18n("Merge branch 'mart/singleQmlEngineExperiment'");?></li>
<li><?php i18n("Implement sizeHint based on implicitWidth/height");?></li>
<li><?php i18n("Subclass of QmlObject with static engine");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix KMimeType::Ptr::isNull implementation.");?></li>
<li><?php i18n("Reenable support for KDateTime streaming to kDebug/qDebug, for more SC");?></li>
<li><?php i18n("Load correct translation catalog for kdebugdialog");?></li>
<li><?php i18n("Don't skip documenting deprecated methods, so that people can read the porting hints");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("Fix CMakeLists.txt to pass KDESU_USE_SUDO_DEFAULT to the compilation so it is used by suprocess.cpp");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update K5 docbook templates");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("private runtime API gets installed to allow KWin to provide plugin for Wayland.");?></li>
<li><?php i18n("Fallback for componentFriendlyForAction name resolving");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Don't try to paint the icon if the size is invalid");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("New proxy model: KRearrangeColumnsProxyModel. It supports reordering and hiding columns from the source model.");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix pixmap types in org.kde.StatusNotifierItem.xml");?></li>
<li><?php i18n("[ksni] Add method to retrieve action by its name (bug 349513)");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Implement PersonsModel filtering facilities");?></li>
</ul>

<h3><?php i18n("KPlotting");?></h3>

<ul>
<li><?php i18n("KPlotWidget: add setAutoDeletePlotObjects, fix memory leak in replacePlotObject");?></li>
<li><?php i18n("Fix missing tickmarks when x0 &gt; 0.");?></li>
<li><?php i18n("KPlotWidget: no need to setMinimumSize or resize.");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("debianchangelog.xml: add Debian/Stretch, Debian/Buster, Ubuntu-Wily");?></li>
<li><?php i18n("Fix for UTF-16 surrogate pair backspace/delete behavior.");?></li>
<li><?php i18n("Let QScrollBar handle the WheelEvents (bug 340936)");?></li>
<li><?php i18n("Apply patch from KWrite devel top update pure basic HL, \"Alexander Clay\" &lt;tuireann@EpicBasic.org&gt;");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Fix enable/disable ok button");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Imported and improved the kwallet-query command-line tool.");?></li>
<li><?php i18n("Support to overwrite maps entries.");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Don't show \"KDE Frameworks version\" in the About KDE dialog");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Make the dark theme completely dark, also the complementary group");?></li>
<li><?php i18n("Cache naturalsize separately by scalefactor");?></li>
<li><?php i18n("ContainmentView: Do not crash on an invalid corona metadata");?></li>
<li><?php i18n("AppletQuickItem: Do not access KPluginInfo if not valid");?></li>
<li><?php i18n("Fix occasional empty applet config pages (bug 349250)");?></li>
<li><?php i18n("Improve hidpi support in the Calendar grid component");?></li>
<li><?php i18n("Verify KService has valid plugin info before using it");?></li>
<li><?php i18n("[calendar] Ensure the grid is repainted on theme changes");?></li>
<li><?php i18n("[calendar] Always start counting weeks from Monday (bug 349044)");?></li>
<li><?php i18n("[calendar] Repaint the grid when show week numbers setting changes");?></li>
<li><?php i18n("An opaque theme is now used when only the blur effect is available (bug 348154)");?></li>
<li><?php i18n("Whitelist applets/versions for separate engine");?></li>
<li><?php i18n("Introduce a new class ContainmentView");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Allow to use highlight spellchecking in a QPainTextEdit");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
