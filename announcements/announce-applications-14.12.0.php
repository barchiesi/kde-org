<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 14.12.0");
  $site_root = "../";
  $release = "applications-14.12.0";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("December 17, 2014. Today KDE released KDE Applications 14.12. This release brings new features and bug fixes to more than a hundred applications. Most of these applications are based on the KDE Development Platform 4; some have been converted to the new <a href='%1'>KDE Frameworks 5</a>, a set of modularized libraries that are based on Qt5, the latest version of this popular cross-platform application framework.", "https://dot.kde.org/2013/09/25/frameworks-5");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>Libkface</a> is new in this release; it is a library to enable face detection and face recognition in photographs.", "http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html");?>
</p>

<p align="justify">
<?php print i18n_var("The release includes the first KDE Frameworks 5-based versions of <a href='%1'>Kate</a> and <a href='%2'>KWrite</a>, <a href='%3'>Konsole</a>, <a href='%4'>Gwenview</a>, <a href='%5'>KAlgebra</a>, <a href='%6'>Kanagram</a>, <a href='%7'>KHangman</a>, <a href='%8'>Kig</a>, <a href='%9'>Parley</a>", "http://www.kate-editor.org", "https://www.kde.org/applications/utilities/kwrite/", "https://www.kde.org/applications/system/konsole/", "https://www.kde.org/applications/graphics/gwenview/", "http://edu.kde.org/kalgebra", "http://edu.kde.org/kanagram", "http://edu.kde.org/khangman", "http://edu.kde.org/kig", "http://edu.kde.org/parley");
// This is because i18n_var doesn't support farher than %9
print i18n_var(", <a href='%1'>KApptemplate</a> and <a href='%2'>Okteta</a>. Some libraries are also ready for KDE Frameworks 5 use: analitza and libkeduvocdocument.", "https://www.kde.org/applications/development/kapptemplate/", "https://www.kde.org/applications/utilities/okteta/");?>
</p>

<p align="justify">
<?php print i18n_var("The <a href='%1'>Kontact Suite</a> is now in Long Term Support in the 4.14 version while developers are using their new energy to port it to KDE Frameworks 5", "http://kontact.kde.org");?>
</p>

<p align="justify">
<?php i18n("Some of the new features in this release include:");
print "<ul>";
print "<li>".i18n_var("<a href='%1'>KAlgebra</a> has a new Android version thanks to KDE Frameworks 5 and is now able to <a href='%2'>print its graphs in 3D</a>", "http://edu.kde.org/kalgebra", "http://www.proli.net/2014/09/18/touching-mathematics/")."</li>";
print "<li>".i18n_var("<a href='%1'>KGeography</a> has a new map for Bihar.", "http://edu.kde.org/kgeography")."</li>";
print "<li>".i18n_var("The document viewer <a href='%1'>Okular</a> now has support for latex-synctex reverse searching in dvi and some small improvements in the ePub support.", "http://okular.kde.org")."</li>";
print "<li>".i18n_var("<a href='%1'>Umbrello</a> --the UML modeller-- has many new features too numerous to list here.", "http://umbrello.kde.org")."</li>";
print "</ul>";
?>
<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications-14.12.0.php");?>
</p>

<p align="justify">
<?php i18n("The April release of KDE Applications 15.04 will include many new features as well as more applications based on the modular KDE Frameworks 5.");?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 14.12 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 14.12 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 14.12 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 14.12");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 14.12 may be <a href='http://download.kde.org/stable/applications/14.12.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-14.12.0.php'>KDE Applications 14.12.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
