<?php

  $page_title = "KDE 4.0 Beta 3 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations 
Also available in:
<a href="announce-4.0-beta1-ca.php">Catalan</a>
// No translations available yet.
-->

<h3 align="center">
   KDE Project Ships Third Beta Release for Leading Free Software Desktop,
Codename "Cicker"
</h3>
<p align="justify">
  <strong>
    With the third Beta, the KDE project has begun to finalize KDE 4.0.
  </strong>
</p>
<p align="justify">
October 17, 2007 (The INTERNET). The KDE Community is happy to release the third
Beta for KDE 4.0. This Beta is aimed at further polishing of the KDE codebase and
also marks the freeze of the KDE Development Platform. We are joined in this
release by the <a href="http://www.koffice.org/">KOffice project</a> which
releases its <a
href="http://www.koffice.org/announcements/announce-2.0alpha4.php">4th alpha
release</a>, bringing many improvements in OpenDocument support, a KChart Flake
shape and much more to those willing to test.
</p>
<p>
Since the last Beta, most of KDE has been frozen for new features, instead
receiving the necessary polish and bugfixing. The components which were exempt
from this freeze saw significant improvements as planned, and Aaron Seigo notes:
"It is amazing to see the Plasma community growing. The pace of development is
amazing, and we're getting really close to having all the features we want for
KDE 4.0 available. After that, we have a solid foundation for implementing new
and exciting user interface concepts for the Free Desktop".
</p>
<h2>About KDE 4</h2>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks
to fulfill the need for a powerful yet easy to use desktop for both personal and
enterprise computing. The aim of the KDE project for the 4.0 release is to put
the foundations in place for future innovations on the Free Desktop. The many
newly introduced technologies incorporated in the KDE libraries will make it
easier for developers to add rich functionality to their applications, combining
and connecting different components in any way they want.
</p>
<p>
The new Beta 3 incorporates many improvements from previous Beta and alpha
releases: <a
href="http://www.kde.org/announcements/visualguide-4.0-alpha1.php">Alpha 1</a>,
<a href="http://www.kde.org/announcements/announce-4.0-alpha2.php">Alpha 2</a>, <a
href="http://www.kde.org/announcements/announce-4.0-beta1.php">Beta 1</a> and <a
href="http://www.kde.org/announcements/announce-4.0-beta2.php">Beta 2</a>.
</p>
<h2>Beta 3</h2>
<p>
With the third Beta, KDE aims to show the progress on their new development
platform and desktop. We hope to collect comments and ideas, and inspire
developers to use our technology for their applications. Most applications in
KDE 4.0 Beta 3 are ready for <a href="http://bugs.kde.org">bug reports</a>, like
Dolphin, Konsole, Okular and most of the Games and Educational applications. Of
course, the parts which aren't released with the Beta like KDevplatform,
KDevelop and kdewebdev generally aren't ready for a load of bugreports, and the
same goes for the components of KDE which are still exempt from the feature
freeze like <a href="http://Plasma.kde.org">Plasma</a>. We would really
appreciate if you would have a look at <a
href="http://techbase.kde.org/Contribute/Bugsquad/
How_to_create_useful_crash_reports">the relevant documentation</a> if you start
<a href="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">reporting
bugs</a>! And remember, if you have a little time to kill, our <a
href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> can really use
your help. All this feedback and support will help us to polish KDE and ensure
stability of the final release.
</p>
<h3>Improvements</h3>
<p>
This Beta has mostly been focussing on finalizing the design of the libraries in
preparation for the release of the KDE Development Platform. Yet, many
user-visible changes have gotten in as well. While new features and usability
improvements were added, bugs were found and fixed in KDE and the stability of
Beta 3 has improved much compared to the previous Betas. In the following
sections, we will try to highlight some visible improvements in Beta 3 compared
to Beta 2. Since work has been done all over KDE, please note that this is not a
complete overview of all changes!
</p>
<h4>Plasma</h4>

<div align="center">	
    <a href="announce_4.0-beta3/plasma.png">
		<img src="announce_4.0-beta3/plasma_small.png" />
	</a><br />
	<em>Plasma in Beta 3</em>
</div>

<p>
<a href="http://Plasma.kde.org">Plasma</a>, one of the more exciting components
of KDE 4 has seen a lot of work again. Aaron Seigo noted the changes since last
Beta:
</p>
<p>
<i>
Plasma now provides a basic set of working components such as taskbars,
panels, desktop wallpapers, etc. Most of the foundational work for 4.0 has
been completed and now the focus is on integrating all the individual pieces
we've been working on for the last months into a single whole. Given that
Plasma is highly modular, supporting feature adds through six different kinds
of plugins and scripting languages, this task is made easier. The <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/716466/">new menu</a>,
based on the KickOff design developed in the openSUSE labs, has been
successfully developed as a separate component due to this approach. The bulk
of this integration work, and therefore the emergence of the Plasma concept,
will be seen in the upcoming release candidates.
<br>
</i>
</p>
Further, a <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/717167/">Plasma applet
browser</a> has been added, and the KGet hackers have worked on integrating <a
href="http://commit-digest.org/issues/2007-09-09/">KGet and Plasma</a>. The work
which still has to be done on Plasma is currently aggregated on the <a
href="http://techbase.kde.org/Projects/Plasma/Tasks">Plasma-Tasks</a> page on <a
href="http://techbase.kde.org">Techbase</a>.
<p>
</p>
<h4>KDE PIM status update</h4>
<p>
The <a href="http://pim.kde.org/">KDE PIM</a> project has been busy with their
new infrastructure, <a href="http://pim.kde.org/akonadi/">Akonadi</a>, which is
planned for KDE 4.1. Their plans for the KDE 4.0 release ranged from
improvements (KBlog, KAlarm, <a
href="http://mikearthur.co.uk/index.php/2007/08/09/korganizer-journal-
improvements/">KOrganizer</a>), feature-parity with KDE 3.5 (KAddressbook,
KMail) to "probably won't be ready" (KPilot). Users would be able to use KDE PIM
applications from the 3.5 series, which have incorporated many fixes and
improvements in the last year, thanks to the work of <a
href="http://www.kdab.com/">KDAB</a> and other developers on the enterprise
branch. Those changes, mostly small features and many bugfixes, will <a
href="http://lists.kde.org/?l=kde-pim&m=119205551912005&w=2">probably be merged
back</a> into the KDE 3.5 series for a future release, and the branch is already
shipped by default by many distributions. Of course, those improvements also
made it into the KDE 4 versions of the KDE PIM applications. In the following
paragraphs, I will expand on several of the applications in KDE-PIM.
</p>
<p>
<a href="http://www.astrojar.org.uk/kalarm/">KAlarm</a> has been completely
ported to KDE 4, and has already received several new features and improvements.
It now supports multiple alarm resources and sharing these, allowing for sharing
alarm calendars between laptop and desktop computer. It also correctly handles
timezone and daylight savings changes, and can restrict alarms to working hours.
Display alarms became more flexible, as the alarm message window can now show
text from the output of a command. The edit alarm dialog has seen some usability
work as well.
</p>
<p>
KPilot's hard-working team of 2.5 have been spending their time getting as much
done in preparation for KDE4 as possible.  KPilot's library, daemon, main UI,
and configuration screens have been ported and are operational. Due to a very
productive Summer of Code project and lots of Real Life (TM) thrown in, not all
of the old conduits have been ported yet, which means that calendar, todo, and
address syncing won't be functional until KDE's 4.1 release. The good news is
that 3.5.8's KPilot is the most stable release to date. The focus in KPilot for
KDE4 has been to clean up as much cruft as possible, to establish a solid
foundation that all of the conduits will now share, and to create a new conduit
in place to exercise that base code (namely, Bertjan's new keyring conduit).
</p>
<p>
Due to some works in the KDE libraries by <a
href="http://www.astrojar.org.uk/kalarm/">KAlarm's</a> David Jarvie, KOrganizer
now has better timezone support. For each event, you can now express the dates
and times in the timezone of your choice. Or you can say "I have a phone meeting
at 10:00am, Moscow time" and it will appear in your own timezone on your schedule.
In the agenda view, you can display timescales from different timezones so you
can see when your events occur, both in your local and the other timezones.
</p>
<p>
<h4>Educational Software</h4>
<p>
As we have come to expect, a lot of work has been going on in the educational
area. Last month saw improvements in Marble, Parley (formerly known as
KVoctrain) and bugfixes in various other applications. The KDE EDU community is
also planning a meeting which will be held in Paris and supported by <a
href="http://www.mandriva.com/">Mandriva</a>.
<div align="center">
	<a href="announce_4.0-beta3/edu_parley.png">
		<img src="announce_4.0-beta3/edu_parley_small.png" />
	</a><br />
	<em>Parley exercise</em>
</div>
</p>

<p>
<a href="http://edu.kde.org/kvoctrain/">KVoctrain</a>, an application which can
be used to train language vocabulary, received usability work, features and a
namechange. The <a href="http://blue-gnu.biz/content/parley_new_kvoctrain">new
name</a>, <a href="http://edu.kde.org/parley/">Parley</a>, <a
href="http://en.wikipedia.org/wiki/Parley">points to</a> a discussion or
conference, especially one between enemies over terms of a ceasefire or other
matters. This fresh name is fitting for the shiny new interface. It became much
easier and intuitive to start and display lectures, and the practice GUI got
improved as well. You can now <a
href="http://bugs.kde.org/show_bug.cgi?id=125488">skip configuration</a> - just
enter some words or get a vocabulary file through <a
href="http://techbase.kde.org/Development/Tutorials/
Introduction_to_Get_Hot_New_Stuff">Get Hot New Stuff</a> to practice, hit
"practice" and you are on your way. <a
href="http://cia.vc/stats/author/gladhorn/">Frederik Gladhorn</a> also got to
implement a <a href="http://www.kdedevelopers.org/node/2984">three</a> <a
href="http://bugs.kde.org/show_bug.cgi?id=93446">year</a> old wish thanks to the
new and easy "start test" dialog, making it possible to use arbitrary language
combinations. Finally, Parley became more colorful when <a
href="http://edu.kde.org/parley/news.php#itemParleyincolorImagesforallpractices"
>the picture support was added</a>, which enables users to practise with
pictures as well.
</p>
<p>
The <a href="http://edu.kde.org/marble/">Marble</a> project <a
href="http://contest.qtcentre.org/results">won a prize</a> at the QtCenter
contest: it was first in the Desktop Application category and third in the
Custom Widget category. Work was done as well, thanks to the work by <a
href="http://carloslicea.blogspot.com/">Carlos Licea</a>, it sports a new <a
href="http://code.google.com/soc/2007/kde/appinfo.html?csaid=DCE4DBD4A0509DC7">
2D projection</a> where you can view the earth like a map. Marble also retrieves
Wikipedia articles about cities you select.
<div align="center">
	<a href="announce_4.0-beta3/edu_marble.jpg">
		<img src="announce_4.0-beta3/edu_marble_small.png" />
	</a><br />
	<em>Marble with 2D projection</em>
</div>
</p>

<p>
<a href="http://edu.kde.org/step/">Step</a>, an interactive physics simulator,
received the ability to embed formulas in notes on the scene. Teachers can use
this to annotate demonstrations of physical problems for their pupils, including
the relevant formulas.
<div align="center">
    <a href="announce_4.0-beta3/edu_step.png">
		<img src="announce_4.0-beta3/edu_step_small.png" />
	</a><br />
	<em>Kstep now supports formulas</em>
</div>
</p>

<p>
On Saturday the 15th of October, the KDE-Edu team had a <a
href="http://dot.kde.org/1189514559/">polish</a> <a
href="http://wiki.30doradus.org/index.php/Main_Page">day</a>. This lead to many
fixes and improvements, most notably in <a
href="http://edu.kde.org/blinken/">Blinken</a>, <a
href="http://edu.kde.org/kgeography/">KGeography</a>, <a
href="http://edu.kde.org/khangman/">KHangMan</a> and <a
href="http://edu.kde.org/blinken/">Marble</a>. Also a global sound issue has
been fixed in all Edu apps, and currently there is <a
href="http://wiki.30doradus.org/index.php/KNew_Stuff_2_problems">investigation</
a> going on in the <a
href="http://techbase.kde.org/Development/Tutorials/
Introduction_to_Get_Hot_New_Stuff">Get Hot New Stuff</a> area.
</p>
<p>
After all this work, the edu team is now shifting gears and focusing on
stabilization and translation work, though there still are some things in the
pipeline.
</p>
<h4>Various other improvements</h4>
<div align="center">
	<a href="announce_4.0-beta3/konqueror.png">
		<img src="announce_4.0-beta3/konqueror_small.png" />
	</a><br />
	<em>Konqueror with a new default profile</em>
</div>

<p>
At first, it seemed Kopete (KDE's multi-protocol instant messenging application
<a href="http://lists.kde.org/?l=kopete-devel&m=118670727507071&w=2">would not
make it for KDE 4.0</a>, but luckily, the developers <a
href="http://matt.rogers.name/blog/2007/09/28/who-wants-to-be-a-kopete-developer
/">decided</a> to make KDE 4.0 with Kopete happen. So we can expect Kopete to
ship with KDE 4.0, basically as a straight port from KDE 3 to KDE 4. The <a
hrev="http://techbase.kde.org/Projects/Kopete/Roadmap">Roadmap</a> already
describes the status and many plans for 4.1 (Kopete 1.0) and further.
</p>
<p>
The KDE image viewer <a href="http://gwenview.sourceforge.net/">Gwenview</a>,
moved to the kdegraphics module for KDE 4, received lots of integration and
usability work. It now uses the location widget introduced in the filemanager
Dolphin, and has now support for image resizing and cropping. A sidebar has been
added showing metadata, and the slideshow feature is now configurable within
fullscreen mode. Gwenview has also been optimized for speed, it starts up much
faster than its KDE 3 counterpart.
</p>
<p>
<a href="http://konqueror.kde.org/">Konqueror</a> received a <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/716179/">cleaned
up</a> <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/718002/">default
profile</a> and <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/716637/">more
polishing</a> <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/718809/">work</a>.
</p>
<p>
Further <a
href="http://rivolaks.blogspot.com/2007/09/kwin-improvements.html">time was
spent</a> on KWin, the KDE window manager. It now <a
href="http://commit-digest.org/issues/2007-09-30/moreinfo/717759/">autodetects</a> 
your graphics hardware, and configures itself automatically for optimal
performance. Changing the KWin settings by hand, will give you a confirmation
dialog which automaticall reverts back to previous settings after some time,
making it more robust by automatically canceling broken settings. When KWin
crashes due to problems with compositing, it will turn it off and restart
itself. Finally, speed has been improved and many effect plugins have been
updated.
</p>

<div align="center">
	<a href="announce_4.0-beta3/kwin-composite.png">
		<img src="announce_4.0-beta3/kwin-composite_small.png" />
	</a><br />
	<em>Kwin with composite</em>
</div>
<p>
<a href="http://klinkstatus.kdewebdev.org/">KLinkStatus</a> from <a
href="http://kdewebdev.org/">kdewebdev</a>, useful for anyone who builds
websites is included in KDE 4 Beta 3, and in a much improved version compared to
KDE 3. It has seen many speed optimizations, but also many new options to
further specify how it should do its work. It can now find (and eventually
remove) unreferred documents, has the possibility to recheck individual links or
the whole session and respects the encoding of the HTML pages when showing the
links. The UI got some improvements as well, making using the application easier
and faster.
</p>
<p>
Users looking for documentation and information should have a look at the KDE <a
href="http://www.kde.org/documentation/">documentation page</a>.
</p>
<h3>For developers</h3>
<p>
As the KDE libraries were in freeze for big additions since the first Beta, work
was mostly focussing on fine-tuning and fixing everything. With the upcoming KDE
Development Platform release, the developers have really shifted gear to getting
everything stable.
</p>
<p>
Marble is now <a
href="http://commit-digest.org/issues/2007-09-09/moreinfo/708818/">available for
runtime embedding</a> in other applications throught the <a
href="http://techbase.kde.org/Development/Architecture/KDE4/KParts">KPart
technology</a> so in time, it will be possible to use it to view GPS datafiles
in Konqueror. Kalzium got <a
href="http://commit-digest.org/issues/2007-09-02/moreinfo/705730/">KPartified</a
> as well.
</p>
<p>
Between all the other work and fixes, Plasma saw the addition of <a
href="http://pdamsten.blogspot.com/2007/09/svgwidget-testing.html">SVG</a> <a
href="http://pdamsten.blogspot.com/2007/09/learning-Plasma-part-ii-widgets.html"
>Widgets</a>, which are somewhat similar to the Superkaramba bar meters. These
widgets can be easily used by developers in plasmoids.
</p>
<p>
<a href="http://kolourpaint.sourceforge.net/">Kolourpaint</a> now <a
href="http://commit-digest.org/issues/2007-09-16/moreinfo/710934/">supports
remote colour schemes</a>. KGet was <a
href="http://liquidat.wordpress.com/2007/09/04/kde-4-screencast-Plasma-and-kget-
interaction/">integrated with Plasma</a>, and Kate welcomes <a
href="http://tsdgeos.blogspot.com/2007/09/kate-welcomes-find-selected.html">Find
Selected</a> function and <a href="http://blog.hartwork.org/?p=83">Search
Highlighting</a>.
<div align="center">
	<a href="announce_4.0-beta3/kate.png">
		<img src="announce_4.0-beta3/kate_small.png" />
	</a><br />
	<em>Kate with extended Find dialog</em>
</div>
</p>

<p>
Documentation for developers can be found on <a
href="http://techbase.kde.org/">TechBase</a>. Here you will be able to find an
architectural overview and plans for the future of KDE. Further, there are
tutorials, manuals and API references.
</p>
<h3>Next up</h3>
<p>
While the last bits and pieces will be set in place in the coming weeks, as soon
as the <a
href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Beta_Goals">Betacycle
objectives</a> are met, we will start preparing our first Release Candidate.
This marks the beginning of a short period where we will allow only the most
important bugfixes in preparation for the release. But shortly before that, we
will see the release of the KDE Development Platform, targeted at developers
interested in KDE development.
</p>
<p>
<h3>Get it, run it, test it...</h3>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0 Beta 3 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/3.94.php">KDE 4 Beta 3 Info Page</a>, where
you can also find links to the source code, information about compiling,
security and other issues.
</p>
<h3>KOffice Alpha 4</h3>

<div align="center">
	<a href="announce_4.0-beta3/koffice.png">
		<img src="announce_4.0-beta3/koffice_small.png" />
	</a><br />
	<em>Karbon in Alpha 4</em>
</div>
<p>
<a href="http://koffice.kde.org">KOffice</a> has just released <a
href="http://koffice.org/announcements/announce-2.0alpha4.php">Alpha 4</a>. This
is mainly a technology preview for those that are interested in the new ideas
and technologies of the KOffice 2 series. This release has seen many
improvements in <a
href="http://www.opendocumentfellowship.org/">OpenDocument</a> support for <a
href="http://www.koffice.org/kword/">KWord</a>, <a
href="http://www.koffice.org/kspread/">KSpread</a> and <a
href="http://www.koffice.org/kformula/">KFormula</a>, many new formula functions
for KSpread, fixes in <a href="http://www.koffice.org/kexi/">Kexi</a> and KWord
and support for templates and animations in KSpread. Kword now also <a
href="http://www.kdedevelopers.org/node/2997">runs on Windows</a>, and others
will follow soon. Last but not least, <a
href="http://www.koffice.org/kplato/">KPlato</a> has been improved and <a
href="http://www.koffice.org/kchart/">KChart</a> now provides a <a
href="http://dot.kde.org/1149518002/">Flake</a> shape so that it can be embedded
in any other KOffice application.
</p>
<p>
KOffice 2 is currently under heavy development. It is not meant as something to
be used for any real work and can crash at any point. However, here are some of
the highlights of the upcoming KOffice 2 series. Note that not all of the new
technologies will be fully implemented in the first release, 2.0. 
</p>
<p>
Also note that although KOffice is included in KDE 4.0 Beta 3, it has its own
release cycle, and the first version of the KOffice version 2 series is expected
around <a href="http://wiki.koffice.org/index.php?title=KOffice2/Schedule">New
Year 2007-2008</a>.
</p>

<p><em>Compiled by Jos Poortvliet with extensive help from the KDE community</em></p>

<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
