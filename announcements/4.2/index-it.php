<?php
  $page_title = "KDE 4.2.0 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

Disponibile anche in:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE 4.2 migliora la qualità di interazione dell'utente con il rilascio della versione 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (nome in codice: <em>"The Answer"</em>) segna l'introduzione di molte migliorie in campi quali usabilità, applicazioni disponibili e piattaforma di sviluppo.
  </strong>
</p>

<p align="justify">
27 Gennaio 2009. La <a href="http://www.kde.org/"> comunità KDE
</a> annuncia la disponibilità di KDE 4.2, nome in codice <em>"The Answer"</em> (la risposta), che rende l'ambiente desktop libero finalmente pronto per gli utenti finali. KDE 4.2 è stato basato sulle tecnologie introdotte con KDE 4.0 nel Gennaio 2008. Dopo il rilascio di KDE 4.1, dedicato principalmente ad utenti esperti, la comunità KDE è ora certa di avere un'offerta concreta per la maggior parte degli utenti.
</p>

<p><strong><a href="guide.php">Guarda la guida visuale a KDE 4.2</a></strong> per i dettagli sulle innovazioni introdotte in KDE 4.2 oppure continua a leggere per un'anteprima.</p>

<?php
    screenshot("desktop_thumb.png", "", "center",
"L'ambiente desktop di KDE 4.2", "337");
?>

<h3>
    Il desktop migliora la qualità dell'esperienza utente
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
  Numerose migliorie dell'interfaccia desktop Plasma permettono una migliore organizzazione dell'ambiente di lavoro. Tra le applet disponibili, <strong>nuove o migliorate</strong>, ricordiamo QuickLaunch, per lanciare le applicazioni in maniera veloce, una stazione meteorologica, un lettore di notizie via RSS, un lettore di fumetti e anche un'applet per condivisione testo tramite servizi di tipo "pastebin". Le applet di Plasma possono ora essere utilizzate anche sopra il salvaschermo, per esempio per farsi lasciare una nota quando si è assenti. Plasma include peraltro la possibilità di essere utilizzato <strong>per la tradizionale gestione delle icone sul desktop</strong>, per la quale sono state aggiunte le anteprime dei file e la possibilità di posizionare liberamente le icone sul desktop.<br />
  Il pannello di Plasma supporta ora <strong>il raggruppamento delle applicazioni</strong> e la visualizzazione su più righe. L'<strong>avanzamento delle operazioni lunghe</strong>, come lo scaricamento di file, è gestito dal nuovo vassoio di sistema, che si occupa anche di visualizzare, in maniera unificata, le <strong>notifiche</strong> di sistema e delle applicazioni. Per guadagnare spazio le icone nel vassoio di sistema possono essere ora <strong>nascoste</strong>. Il pannello può ora essere configurato per <strong>nascondersi automaticamente</strong> per permettere una migliore gestione dello spazio del desktop. Le applet che lo permettono possono essere utilizzate sia nei pannelli che sul desktop.
        </p>
    </li>
    <li>
        <p align="justify">
KWin fornisce una piacevole e efficiente gestione delle finestre. In KDE 4.2, grazie all'utilizzo di modelli di <strong>fisica dinamica</strong> le animazioni delle finestre, vecchie e di recente implementazione come "Cubo" e "Lampada magica", hanno dei movimenti naturali. KWin è ora in grado di abilitare automaticamente gli effetti grafici solo sulle schede che lo supportano. Una <strong>configurazione facilitata</strong> permette agli utenti di scegliere e configurare gli effetti che preferiscono in una maniera facile ed intuitiva.
        </p>
    </li>
    <li>
        <p align="justify">
Nuovi e migliorati strumenti permettono di aumentare la produttività. PowerDevil è un supporto alla vita mobile che introduce una moderna e non intrusiva <strong>gestione dell'energia</strong> nei portatili e nei dispositivi mobili in generale. Ark offre una facile <strong>estrazione e creazione</strong> di archivi compressi e il nuovo strumento di stampa permette di <strong>gestire le stampanti</strong> e le code di stampa.
        </p>
    </li>
</ul>
Inoltre, KDE 4.2 è disponibile in un numero addirittura maggiore di lingue, aumentando così il numero degli utenti per i quali KDE è disponibile nel loro linguaggio nativo, raggiungendo quota 700 milioni. Tra le lingue introdotte possiamo trovare l'arabo, l'islandese, il basco, l'ebraico, il rumeno, il tagico e addirittura alcune lingue indiane (bengali indiano, gujarati, kannada, maithili, marathi) che indicano con la loro presenza una evidente crescita di popolarità di KDE in quest'area dell'Asia.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>
<a href="http://blip.tv/file/get/Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Versione Ogg Theora</a></div>

<h3>
    Avanzamento delle applicazioni
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
La gestione dei file diventa più veloce ed efficiente. In Dolphin, il gestore dei file predefinito di KDE 4.2, è stata aggiunta una barra per la modifica delle <strong>dimensioni delle icone</strong> predefinite.
        Ulteriori miglioramenti includono <strong>suggerimenti con le anteprime</strong> e un indicatore dello spazio occupato per i dispositivi rimovibili. Questi cambiamenti si riflettono anche sulle <strong>finestre di dialogo file</strong>, rendendo più semplice trovare il file cercato.
        </p>
    </li>
    <li>
        <p align="justify">
In KMail, la visualizzazione della lista delle email è stata ridisegnata da uno degli studenti dello scorso "Google Summer of Code". L'utente è ora in grado di configurare la <strong>visualizzazione di più informazioni</strong> ottimizzando e velocizzando la fruizione dei dati di ogni singola cartella. Il supporto ad <strong>IMAP e altri protocolli</strong> è stato migliorato rendendo KMail più veloce.
        </p>
    </li>
    <li>
        <p align="justify">
Navigare in rete è ancora più piacevole. Konqueror ha ora un miglior supporto per la <strong>grafica SVG</strong> ed è stato reso ancora più veloce. La nuova <strong> finestra di dialogo "Trova..."</strong> permette una ricerca più semplice all'interno delle pagine web. Inoltre, Konqueror può anche <strong>mostrare i segnalibri come schermata di avvio</strong>.
        </p>
    </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<embed src="http://blip.tv/play/golB5aR7joEn" type="application/x-shockwave-flash" width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>
<a href="http://blip.tv/file/get/Sebasje-WindowManagementInKDE42153.ogv">Versione Ogg Theora</a>
</div>

<h3>
    Piattaforma di sviluppo
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Migliorato il supporto ai linguaggi di scripting</strong>. Le applet di Plasma possono ora essere scritte in JavaScript, Python e Ruby. Le applet scritte con queste tecniche potranno essere distribuite facilmente attraverso servizi online come OpenDesktop.org. Anche le applet GoogleGadgets possono essere usate in Plasma, e il supporto ai widgets della dashboard di Mac OS X è stato migliorato.
        </p>
    </li>
    <li>
        <p align="justify">
Sono altresì disponibili versioni di anteprima di alcune applicazioni KDE per <strong>Windows</strong> e <strong>Mac OS X</strong>, alcune delle quali sono già abbastanza stabili e vicine ad una qualità da rilascio, mentre altre hanno ancora bisogno di lavoro. OpenSolaris è quasi completamente supportato e il supporto per FreeBSD continua a migliorare.
        </p>
    </li>
    <li>
        <p align="justify">
Dal momento che le librerie Qt saranno rilasciate sotto i termini della licenza <strong>LGPL</strong>, anche le librerie KDE saranno disponibili sotto questi termini legali meno restrittivi, diventando una piattaforma ancora più appetibile per lo sviluppo di applicazioni commerciali.
        </p>
    </li>
</ul>
</p>


<h4>
  Installare KDE 4.2.0
</h4>
<p align="justify">
KDE, con tutte le librerie e le applicazioni, è liberamente scaricabile sotto i termini di varie licenze Open Source. KDE può essere ottenuto sotto forma di codice sorgente come anche sotto forma di pacchetti binari precompilati da <a
href="http://download.kde.org/stable/4.2.0/">http://download.kde.org</a>, sotto forma di <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
o assieme ad una delle <a href="http://www.kde.org/download/distributions.php">maggiori distribuzioni GNU/Linux e UNIX</a> disponibili attualmente.
</p>
<p align="justify">
  <em>Pacchetti</em>.
  Alcune distribuzioni Linux/UNIX hanno gentilmente fornito pacchetti binari di KDE 4.2.0 per alcune versioni delle loro distribuzioni. In altri casi dei volontari hanno fatto lo stesso. Alcuni di questi pacchetti binari sono liberamente scaricabili all'indirizzo <a href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://download.kde.org</a>. Altri pacchetti, come aggiornamenti ai pacchetti già rilasciati, saranno resi disponibili nelle prossime settimane.
</p>

<p align="justify">
  <a name="package_locations"><em>Dove trovare i pacchetti</em></a>.
  Per una lista esauriente di pacchetti binari dei quali la comunità KDE è stata informata visitare la <a href="/info/4.2.0.php">pagina di informazioni di KDE 4.2.0</a>.
</p>

<p align="justify">
I problemi di prestazioni con i driver <em>NVidia</em> sono stati <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">risolti</a> nell'ultimo rilascio beta dei driver da parte di NVidia.</p>

<h4>
  Compilare KDE 4.2.0
</h4>
<p align="justify">
  <a name="source_code"></a>
  Tutto il codice sorgente di KDE 4.2.0 può essere <a href="http://download.kde.org/stable/4.2.0/src/">liberamente scaricato</a>. Istruzioni su come compilare ed installare KDE 4.2.0 possono essere trovate nella <a href="/info/4.2.0.php">pagina di informazioni di KDE 4.2.0</a>.
</p>

<h4>
    Spargete la voce!
</h4>
<p align="justify">
La comunità KDE incoraggia chiunque a pubblicizzare il rilascio di KDE 4.2.0 anche in altre comunità on line. Inviate articoli, utilizzare siti come delicious, digg, reddit, twitter, identi.ca. Caricate istantanee dei vostri schermi con KDE 4.2.0 su siti come Facebook, Flickr, ipernity o Picasa e pubblicizzatele nei gruppi appropriati. Create video che mostrano come usate KDE 4.2.0 e caricateli su youtube Blip.tv, Vimeo e simili. Non dimenticate di taggare il materiale che caricate con la parola <strong>kde42</strong> garantendo così un facile accesso al vostro materiale a chiunque sia interessato, nonché per facilitare alla comunità KDE la compilazione del rapporto sulla copertura raggiunta dall'annuncio del rilascio di KDE 4.2.0. Questa è la prima volta che la comunità KDE tenta di coordinarsi con i "media sociali" per la pubblicizzazione di un evento. <br />
Aiutaci a spargere la voce! Sii anche tu parte di questo successo!
<p />

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
