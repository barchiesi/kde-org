<?php
  $page_title = "Najava izdaje KDE 4.2.0";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Skupnost KDE je s KDE 4.2 izboljšala uporabniško doživetje
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (kodno ime <em>"The Answer"</em>) prinaša izboljšano uporabniško doživetje
    na namizju, programe in razvojno platformo
  </strong>
</p>

<p align="justify">
27. januarja 2009. <a href="http://www.kde.org/">Skupnost KDE</a> danes najavlja takojšnjo razpoložljivost KDE 4.2.0
<em>"The Answer"</em>, s čimer je prosto namizje nared za končne uporabnike. KDE 4.2 gradi
na tahnologijah, ki so bile januarja 2008 predstavljene s KDE 4.0. Po izidu KDE 4.1, ki je
bil namenjen zgodnjim uporabnikom, je skupnost KDE sedaj prepričana, da je ponudba primerna
za večino končnih uporabnikov.
</p>

<?php
    screenshot("desktop_thumb.png", "", "center",
"Namizje v KDE 4.2", "337");
?>

<h3>
    Namizje izboljšuje uporabniško doživetje
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        Nadaljnja izpopolnitev namizja Plasma olajša organizacijo delovnega prostora.
        Med <strong>nove in izboljšane gradnike</strong> spadajo hitri zaganjalnik, vremenske
        informacije, viri novic, stripi in hitro deljenje datotek prek storitev »pastebin«.
        Plasma gradnike je sedaj možno uporabiti na ohranjevalniku zaslona. Tako lahko uporabnik
        ob odsotnosti pusti notico.
        Plasma se lahko po želji obnaša kot <strong>tradicionalno namizje, podobno upravljalniku
        datotek</strong>. Dodana je bila možnost ogledov namesto ikon datotek in pomnenje položajev
        ikon.<br />
        Opravilna vrstica lahko <strong>združuje opravila</strong> in jih prikazuje v več vrsticah.
        Izboljšana sistemska vrstica lahko <strong>spremlja dlje trajajoča opravila</strong>, kot so
        na primer prenosi datotek. Sistemska in programska <strong>obvestila</strong> so sedaj
        s pomočjo sistemske vrstice prikazana na enoten način. Ikone v sistemski vrstici je možno
        skriti in tako prihraniti prostor. Pulti se lahko po novem <strong>samodejno skrijejo</strong>,
        kar spet prihrani prostor na zaslonu. Gradnike lahko postavite na pulte ali pa na namizje.
        </p>
    </li>
    <li>
        <p align="justify">
        KWin omogoča tekoče in učinkovito upravljanje z okni. V KDE 4.2 za <strong>premikanje
        uporablja fiziko</strong> in tako daje naraven občutek starim in <strong>novim
        učinkom</strong> kot sta »Kocka« in »Magična svetilka«. KWin privzeto omogoči namizne učinke
        samo na računlnikih, ki jih podpirajo. <strong>Poenostavljena nastavitev</strong>
        omogoča uporabniku izbiro različnih efektov za učinkovitejše preklapljanje med okni.
        </p>
    </li>
    <li>
        <p align="justify">
        Nova in izboljšana orodja delovnega okolja povečajo produktivnost. PowerDevil ponuja
        podporo za delo na terenu, saj prinaša moderno in nemoteče <strong>upravljanje z
        energijo</strong> na prenosnikih in mobilnih napravah. Ark omogoča pametno <strong>
        razširjanje in ustvarjanje</strong> arhivov, nova tiskalna orodja pa uporabniku
        omogočajo preprosto <strong>upravljanje tiskalnikov</strong> in tiskalnih opravil.
        </p>
    </li>
</ul>
Dodani so bili prevodi v več novih jezikov, s čimer se je število uporabnikov, za katere je
paket KDE na voljo v materinem jeziku, povečalo za približno 700 milijonov. Večina na novo
podprtih jezikov je azijskih, kar nakazuje na močan porast priljubljenosti KDE-ja v tem
delu sveta.
</p>
<p>
Seveda je KDE v veliki meri preveden tudi v <strong>slovenščino</strong>. Prevajanje poteka
v okviru društva <em>Lugos</em>. Zahvaliti se moramo tudi <em>Ministrstvu za kulturo</em>, ki
je v preteklosti finančno podpiralo prevajanje. Vabimo vas, da nam pomagate in se nam pridružite
pri <a href="https://wiki.lugos.si/slovenjenje:kde:kde">slovenjenju KDE-ja</a>.
</p>

<p align="right">
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed></p>

<h3>
    Programi napredujejo
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        Upravljanje z datotekami je postalo hitrejše in učinkovitejše. Upravljalnik datotek
        Dolphin ima nov drsnik, s katerim preprosto <strong>spremenite velikost ikon</strong>.
        Med drugimi izboljšavami uporabniškega vmesnika so <strong>namigi z ogledi</strong> in
        prikaz porabe odstranljivih nosilcev. Te spremembe so postale tudi del <strong>pogovornih
        oken za odpiranje datotek</strong>, zaradi česar je lažje najti pravo datoteko.
        </p>
    </li>
    <li>
        <p align="justify">
        Študent v programu Google Summer of Code predelal prikaze seznama e-pošte v
        KMailu. Uporabnik lahko sedaj nastavi <strong>prikaz dodatnih podatkov</strong> in
        zoptimizira delovni tok za vsako mapo posebaj. Izboljšana je tudi podpora za
        <strong>IMAP in ostale protokole</strong>. KMail je zato precej hitrejši.
        </p>
    </li>
    <li>
        <p align="justify">
        Brskanje po spletu je hitrejše. Spletni brskalnik Konqueror ima izboljšano podporo za
        <strong>raztegljivo vektorsko grafiko SVG</strong> in je doživel več pohitritev.
        <strong>Nova iskalna vrstica</strong> omogoča manj moteče iskanje po spletnih
        straneh. Ob zagonu Konqueror po novem prikaže <strong>vaše zaznamke</strong>.
        </p>
    </li>
</ul>
</p>

<p align="right">
<embed src="http://blip.tv/play/golB5aR7joEn" type="application/x-shockwave-flash" width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>

<h3>
    Platforma pospešuje razvoj
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Izboljšana podpora za skripte</strong>. Plasma gradnike lahko sedaj pišete
        v JavaScriptu, Pythonu in Rubyu. Te gradnike lahko razširjate prek spletnih
        storitev in orodij za sodelovanje prek spleta, kakršen je OpenDesktop.org.
        V Plasmi lahko uporabite gradnike Google Gadgets, podpora za gradnike Mac OS X Dashboard
        pa je bila še izboljšana.
        </p>
    </li>
    <li>
        <p align="justify">
        Na voljo so poskusne različice raznih KDE-jevih progtamov za <strong>Mac OS X</strong>
        in <strong>Windows</strong>. Kvaliteta nekaterih programov je že skoraj primerna
        za končno izdajo, nekateri programi za dosego te ravni kvalitete potrebujejo še nekaj
        dela. Podpora za OpenSolaris je v delu in je že skoraj primerna za končno izdajo.
        Napreduje tudi KDE 4 za FreeBSD.
        </p>
    </li>
    <li>
        <p align="justify">
        Ko bo Qt 4.5 izdan z možnostjo uporabe licence <strong>LGPL</strong>, bodo tako KDE-jeve
        knjižnice, kot tudi Qt, na voljo pod manj strogimi licenčnimi pogoji, ki so primerni
        tudi za razvoj komercialne programske opreme.
        </p>
    </li>
</ul>
</p>


<h4>
  Namestitev KDE 4.2.0
</h4>

<p align="justify">
KDE, vključno z vsemi knjižnicami in programi, je za prost prenos na voljo pod
odpro-kodnimi licencami. KDE lahko dobite v obliki izvorne kode in v že prevedeni
obliki s strani <a href="http://download.kde.org/stable/4.2.0/">download.kde.org</a>.
Lahko ga dobite tudi <a href="http://www.kde.org/download/cdrom.php">na zgoščenki</a>
ali pa skupaj s katero koli <a href="http://www.kde.org/download/distributions.php">pomembnejšo
distribucijo sistema GNU/Linux ali UNIX</a>, ki je na voljo danes.
</p>
<p align="justify">
  <em>Binarni paketi</em>
Nekateri ponudniki operacijskega sistema Linux, oziroma UNIX, so pripravili binarne pakete
za KDE 4.2.0 za nekatere različice svojih distribucij. V drugih primerih so to storili
prostovoljci iz skupnosti. Nenateri binarni paketi so za prost prenos na voljo s strani
<a href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">download.kde.org</a>.
Dodatni binarni paketi in popravki za trenutno razpoložljive pakete bodo morda na voljo
v prihodnjih dneh.
</p>

<p align="justify">
  <a name="package_locations"><em>Lokacija paketov</em></a>.
Za trenuten seznam razpoložljivih paketov, o katerih je bila skupnost KDE obveščena,
obiščite angleško stran <a href="/info/4.2.0.php">KDE 4.2.0 Info Page</a>.
</p>

<p align="justify">
Hitrostne težave z zaprto-kodnimi grafičnimi gonilniki podjetja <em>NVidia</em> so bile
<a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">odpravljene</a> v najnovejših
beta izdajah gonilnikov, ki jih dobite na spletni strani NVidie.
</p>

<h4>
  Izvorna koda KDE 4.2.0
</h4>
<p align="justify">
  <a name="source_code"></a>
Celotno izvorno kodo za KDE 4.2.0 lahko <a
href="http://download.kde.org/stable/4.2.0/src/">prosto prenesete</a>.
Navodila za prevajanje in namestitev KDE 4.2.0 so na voljo na strani
<a href="/info/4.2.0.php#binary">KDE 4.2.0 Info Page</a>.
</p>

<h4>
    Razširite novico
</h4>
<p align="justify">
Skupnost KDE vsakogar spodbuja, da razširi novico prek socialnih omrežij. Pošljite
novico na strani kot so delicious, digg, reddit ali prek twitter in identi.ca.
Zaslonske posnetke objavite na Facebook, Flickr, ipernity in Picasa. Pri tem
izberite ustrezno skupino. Posnemite video in ga pošljite na YouTube, Blip.tv, Vimeo
ali drugo podobno stran. Vse kar objavite označite z oznako <strong>kde42</strong></em>,
da bodo vsi lahko lažje našli vse te prispevke. Skupnost KDE bo na ta način tudi
lažje setavila poročila o odzivu na izid KDE 4.2. Tokrat skupnost KDE prvič koordinirano
poskuša za sporočanje javnosti uporabiti socialne medije. Pomagajte nam razširiti novico
in postanite del nje.
</p>
<p>
Na spletnih forumih obvestite ostale o zanimivih novostih v KDE, pomagajte ostalim pri
pričetku uporabe novega namizja in razširjajte novico.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Stiki z mediji</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
