<?php
  $page_title = "Aankondiging uitgave KDE 4.2.0";
  $site_root = "../";
  include "header.inc";
?>

Ook beschikbaar in:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE-gemeenschap verbetert gebruikerservaring met KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (codenaam: <em>"Het Antwoord"</em>) brengt een verbeteringen in de gebruikerservaring op de desktop, in de applicaties en in het ontwikkelplatform.
  </strong>
</p>

<p align="justify">
27 januari 2009. De <a href="http://www.kde.org/">KDE-gemeenschap</a> kondigt de onmiddelijke beschikbaarheid aan van <em>"Het Antwoord"</em> (oftewel KDE 4.2.0), waarmee de vrije desktop geschikt gemaakt is voor de eindgebruiker. KDE 4.2 bouwt voort op de technologie die in januari 2008 werd ge&iuml;ntroduceerd met KDE 4.0. Na de uitgave van KDE 4.1, waarmee werd gedoeld op de dagelijkse computergebruiker, biedt de KDE-gemeenschap met vertrouwen een geschikte gebruikerservaring aan voor de eindgebruiker.
</p>

<a href="guide.php">
<?php
    screenshot("desktop_thumb.png", "", "center",
"De KDE 4.2 Desktop", "337");
?>
</a>

<h3>
    Desktop verbetert gebruikerservaring
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        De bureaubladinterface Plasma is verder uitgewerkt en maakt het daarom gemakkelijker om uw werkruimte makkelijker te beheren.
	<strong>Nieuwe en verbeterde applets</strong>, waaronder een snelstarter, weerdienst, strips en het snel delen van bestanden met services als PasteBin. Plasma-applets kunnen nu ook gebruikt worden binnen de schermbeveiliging, zodat u bijvoorbeeld een notitie kunt achterlaten wanneer de gebruiker afwezig is.
	Plasma kan eventueel dienen als <strong>een traditionele, op bestanden gericht bureaublad</strong>.
	Op het bureaublad worden nu ook voorbeelden van de bestandsinhoud getoond en de locaties van de pictogrammen worden onthouden.<br />
	Het paneel van Plasma kan nu <strong>taken groeperen</strong> en meerdere rijen tonen. Het verbeterde systeemvak <strong>houdt langer opende taken bij</strong>, zoals downloads. Systeem- en programmameldingen worden allemaal via het systeemvak getoond.
	Om ruimte te besparen kunnen systeemvakpictogrammen nu ook verborgen worden. Bovendien kan het paneel <strong>automatisch verbergen</strong> om zo meer ruimte op uw scherm vrij te hebben. Applets kunnen zowel op een paneel geplaatst worden als op het bureaublad.<br />
        </p>
    </li>
    <li>
        <p align="justify">
	KWin biedt een gestroomlijnd en effici&euml;nt vensterbeheer. In KDE 4.2 heeft de wetten van de <strong>bewegingsfysica</a> ingebouwd om zo een natuurlijke ervaring tot stand te brengen bij de oude en <strong>nieuwe effecten</strong>, zoals de "Kubus" en de "Magische Lamp". KWin zal deze effecten alleen standaard inschakelen als de computer in staat is ze weer te geven.
	KWin is nu <strong>makkelijker in te stellen</strong> en stelt de gebruiker in staat om een ander effect te gebruiken bij het wisselen van vensters, zodat dit nog effici&euml;nter kan.
        </p>
    </li>
    <li>
        <p align="justify">
	Nieuwe hulpmiddelen zullen uw productiviteit verhogen. PowerDevil biedt voor uw laptop en mobiele telefoon een modern <strong>energiebeheer</strong> dat niet in de weg zit. Ark kan slim archieven <strong>in- en uitpakken</strong> en de nieuwe afdrukhulpmiddelen stellen de gebruiker in staat om eenvoudig <strong>printers en printtaken te beheren</strong>.
        </p>
    </li>
</ul>

Er is ook ondersteuning voor enkele nieuwe talen, zodat ongeveer 700 miljoen mensen KDE kunnen gebruiken in hun moedertaal. De nieuwe talen zijn Arabisch, IJslands, Baskisch, Hebreeuws, Roemeens, Tadzjieks en enkele Indiase talen (Bengaals, Gujarati, Kannada, Maithili en Marathini). Hieruit blijkt dat de populariteit van KDE aan het stijgen is in dit deel van Azi&euml;.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<em>De bureaubladinterface Plasma in KDE 4.2</em>
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed></div>

<h3>
    KDE-programma's hebben een grote stap vooruit gemaakt
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
	Bestandsbeheer is nu sneller en effici&euml;nter. De bestandsbeheerder Dolphin bevat nu een schuifbalk waarmee u gemakkelijk <strong>de pictogramgrootte kunt verstellen</strong>.
	Andere verbeteringen zijn de <strong>hulpballonnen met voorbeelden</strong> en een indicator voor de hoeveelheid vrije ruimte op verwijderbare media. Deze veranderingen zijn ook aangebracht in de <strong>bestandsdialogen</strong> in KDE, zodat het eenvoudiger wordt om het juiste bestand te vinden.
        </p>
    </li>
    <li>
        <p align="justify">
	De berichtweergaven in KMail zijn volledig op de schop gegaan dankzij een student die deelnam aan de Google Summer Of Code. De gebruiker kan nu instellen om <strong>extra gegevens te tonen</strong>, zodat de workflow per map verbeterd kan worden. De ondersteuning voor <strong>IMAP en andere protocollen</strong> is verbeterd waardoor KMail nog sneller wordt.
        </p>
    </li>
    <li>
        <p align="justify">
	Het websurfen wordt nu nog beter. De webbrowser Konqueror verbetert de ondersteuning voor <strong>schaalbare vectorafbeeldingen</strong> (SVG) en heeft veel veranderingen ondergaan ten behoeve van de prestaties. Een <strong>nieuw zoekdialoogvenster</strong> maakt het zoeken binnen webpagina's prettiger. Konqueror toont nu <strong>uw bladwijzers</strong> bij het opstarten.
        </p>
    </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<embed src="http://blip.tv/play/golB5aR7joEn" type="application/x-shockwave-flash" width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>
<em>Vensterbeheer in KDE 4.2</em></div>

<h3>
    Platform versnelt ontwikkeling
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
	<strong>Verbeterde ondersteuning voor scripts</strong>. Plasma-applets kunnen nu in JavaScript, Python en Ruby geschreven worden. Deze applets kunnen online verspreid worden met web-services en online samenwerkingsinitiatieven zoals OpenDesktop.org.
	GoogleGadgets kunnen nu ook in Plasma gebruikt worden en ondersteuning voor de dashboard widgets van Mac OS X is verder verbeterd.
        </p>
    </li>
    <li>
        <p align="justify">
	Er zijn technologische 'previews' van verschillende KDE-programma's beschikbaar voor <strong>Windows</strong> en <strong>Mac OS X</strong>. Enkele programma's benaderen de kwaliteit voor een offici&euml;le uitgave, terwijl bij anderen nog werk verricht moet worden, afhankelijk van de functionaliteit. Ondersteuning voor OpenSolaris is in de pijplijn en benadert de kwaliteit voor een stabiele werkomgeving. Er wordt ook gewerkt om het draaien van KDE4 op FreeBSD verder te perfectioneren.
        </p>
    </li>
    <li>
        <p align="justify">
	Nadat Qt is uitgebracht onder de <strong>LGPL</strong>-licentie, zullen zowel de KDE-bibliotheken als de onderliggende bibliotheken verkrijgbaar zijn onder deze lossere licentievoorwaarden. Dit maakt het platform uitermate geschikt voor commerci&euml;le software-ontwikkeling.
        </p>
    </li>
</ul>
</p>


<h4>
  Installeren van KDE 4.2.0
</h4>
<p align="justify">
Alle bibliotheken en programma's van KDE zijn vrij beschikbaar onder openbronlicenties. KDE kan verkregen worden in broncode en verscheidene binaire formaten van <a
href="http://download.kde.org/stable/4.2.0/">http://download.kde.org</a>. Bovendien is het verkrijgbaar op <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> of bij de <a href="http://www.kde.org/download/distributions.php">grotere distributies van Linux- of Unix-systemen</a> die momenteel in omloop zijn.
</p>
<p align="justify">
  <em>Pakketbouwers</em>.
Sommige Linux/UNIX-distribiteurs hebben binaire pakketten aangeleverd van KDE 4.2.0, geschikt voor enkele versies van hun distributie. In andere gevallen hebben vrijwilligers uit de gemeenschap pakketten aangeleverd.
Enkele van deze binaire pakketten zijn vrij te downloaden van <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://download.kde.org</a>.
Andere binaire pakketten en bijbehorende updates zullen in de komende weken beschikbaar komen.
</p>

<p align="justify">
  <a name="package_locations"><em>Pakketlocaties</em></a>.
Voor een actuele lijst van de beschikbare binaire pakketten waar het KDE-project van op te hoogte is, kunt u de <a href="/info/4.2.0.php">KDE 4.2.0 Informatiepagina</a> (Engels) raadplegen.
</p>

<p align="justify">
Prestatieproblemen met de binaire videokaartdrivers van <em>NVidia</em> zijn <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">verholpen</a> in de laatste beta-uitgaven van de drivers. Deze zijn beschikbaar bij NVidia.
</p>

<h4>
  Compileren van KDE 4.2.0
</h4>
<p align="justify">
  <a name="source_code"></a>
U kunt de broncode van KDE 4.2.0 <a
href="http://download.kde.org/stable/4.2.0/src/">vrij downloaden</a>. Instructies om KDE 4.2.0 te compileren en te installeren zijn beschikbaar op de <a href="/info/4.2.0.php#binary">KDE 4.2.0 Informatiepagina</a>.
</p>

<h4>
    Zegt het voort!
</h4>
<p align="justify">
Het KDE-team moedigt iedereen aan om deze aankondiging op de sociale websites te verspreiden. Dien artikels in bij websites, gebruik kanalen als Delicious, Digg, Reddit, Twitter of Identi.ca. Upload schermafbeeldingen op Facebook, FlickR, ipernity en Picasa en plaats deze in de juiste groepen. Of maak een screencast en zet ze op YouTube, Blip.tv, Vimeo of andere sites. Vergeet niet het materiaal te voorzien van het <em>label <strong>kde42</strong></em>, zodat iedereen snel het materiaal kan terugvinden en het KDE-team kan zien hoe goed de boodschap is verspreid. Dit is de eerste maal dat het KDE-team een geco&ouml;rdineerde poging doet om de sociale media in te zetten bij haar berichtgeving. Help ons daarbij, het wordt zeer gewaardeerd.
</p>
<p>
Kondig de nieuwe mogelijkheden van KDE op webfora, en help uw medegebruikers bij het gebruiken van deze nieuwe bureaubladomgeving. Help ons de informatie te verspreiden.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Perscontacten</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
