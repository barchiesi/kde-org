<?php
  $page_title = "KDE 4.2.0 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE satsar på användaren med KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (Kodnamn: <em>"Lösningen"</em>) förbättrar för användaren samtidigt som man levererar
	nya versioner av sina applikationer och utvecklingsplattform.
  </strong>
</p>

<p align="justify">
27 januari, 2009. <a href="http://www.kde.org/">KDE projektet</a> tillkännager idag <em>"Lösningen"</em>
(KDE 4.2.0), vilket är den första versionen av KDE 4 som är redo för slutanvändare. KDE 4.2 bygger på den
teknik som introducerades i och med KDE 4.0 i januari 2008. Efter KDE 4.1 som släpptes i augusti 2001 
och som hade de mer avancerade användarna som målgrupp, är nu projektet redo att släppa en version för
alla de "vanliga" användarna.
</p>

<a href="guide.php">

<?php
    screenshot("desktop_thumb.png", "", "center",
"KDE 4.2:s skrivbordsmiljö", "337");
?>
</a>
<h3>
    En bättre miljö att jobba i
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
		Ytterligare förbättringar i skrivbordsskalet Plasma gör det enklare än
		någonsin att organisera sitt arbete.
        <strong>Nya förbättrade widgets</strong> som exempelvis en snabbstartswidget, väderinformation,
		nyhetsläsare, tecknader serier och snabb fildelning via s.k "pastebin"-tjänster.
		Plasma gör det nu möjligt att använda widgets ovanpå sin skärmsläckare, exempelvis
		kan man låta folk lämna meddelanden på skärmsläckare när man är borta från sin dator.
		Plasma kan även fungera som ett traditionellt ikonskrivbord.
		Panelen grupperar nu de program man har uppe och kan visa multipla rader. Den nya systembrickan
		håller nu reda på mer tidskrävande jobb som ex nedladdningar. System- och applikationsnotifieringar
		visas på ett enhetligt sätt i systembrickan som även låter dig gömma ikoner du inte vill se.
		Panelen har även möjlighet att dölja sig själv för att frigöra skärmutrymme. Widgets fungerar
		lika bra i panelen som på skrivbordet.
    </li>
    <li>
        <p align="justify">
		Fönsterhanteraren KWin har lagt till rörelsefysik för att ge en mer naturlig känsla
		till sina effekter som ex. kuben och den magiska lampan. KWin har även
		fått ett bättre stöd för att detektera vilka effekter datorn klarar av och slår
		bara på de som den klarar. Man har även lagt till konfiguration för att enkelt
		kunna välja vilka effekter som ska användas för ex fönsterbyte.
        </p>
    </li>
    <li>
        <p align="justify">
		<strong>Nya verktyg för att öka produktiviteten.</strong>
        PowerDevil ger stöd för mobila enheter genom att tillhandahålla modern och icke-störande
		hantering av strömspararfunktioner. Ark ger smidigt stöd för att extrahera filer och
		skapa komprimerade arkiv. Det finns även nya verktyg för att hantera skrivare och utskrifter.
        </p>
    </li>
</ul>
Stöd har lagts till för ett antal nya språk vilkjet har ökat antalet användare
som kan använda KDE på sitt modersmål med cirka 700 miljoner. De nya språken är 
bland annat: Arabiska, Isländska, Baskiska, Hebreiska, Rumänska, Tajik och ett 
antal indiska språk (Bengalisk indiska, Gujarati, Kannade, Maithili och Marathi)
vilket indikerar ett stort intresse i dessa delar av Asien.
part of Asia.
</p>

<p align="right">
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed></p>

<h3>
    Applikationerna tar ett språng framåt
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
		Filhanteringen blir snabbar och mer effektiva med de nya funktionerna i Dolphin.
		Dolphin har bland annat fått ett skjutreglage för att styra ikonstorleken samt
		tooltips med förhandsvisning av filerna. Man har även lagt till en kapacitetsindikator
		för borttagbara enheter. De här förbättringarna har även lagts till KDE's fildialoger
		vilket gör det enklare att hitta den fil man söker.
        </p>
    </li>
    <li>
        <p align="justify">
		KMail:s listvy har blivit ordentligt omarbetade av en av Google Summer of Code
		studenterna. Användaren kan nu konfigurera vilken information som visas om varje
		meddelande för varje katalog. Stödet för IMAP och andra protokoll har också blivit
		förbättrat för att göra KMail snabbare.
        </p>
    </li>
    <li>
        <p align="justify">
		Webbläsaren Konqueror får bättre stöd för skalbar grafik och ett stort antal
		andra prestandaförbättringar. Man har även lagt till en ny sökdialog som är
		mindre i vägen.
        </p>
    </li>
</ul>
</p>

<p align="right">
<embed src="http://blip.tv/play/golB5aR7joEn" type="application/x-shockwave-flash" width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>

<h3>
    Nytt i plattformen
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Förbättrat stöd för skriptspråk</strong>. 
		Widgets för Plasma kan numera skrivas i JavaScript, Python och Ruby. Dessa kan sedan
		distribueras på nätet via webbservice och andra verktyg som exempelvis OpenDesktop.org.
		GoogleGadgets och Mac OS X dashboard widgets kan också laddas i Plasma.
        </p>
    </li>
    <li>
        <p align="justify">
		Förhandsversioner av olika KDE-applikationer finns för <strong>Windows</strong> och
        <strong>Mac OS X</strong>. Vissa av dom börjar närma sig skarp kvalité redan medan 
		andra behöver mer putsning. Stödet för OpenSolaris är på god väg och stödet för FreeBSD
		fortsätter att stabiliseras.
        </p>
    </li>
    <li>
        <p align="justify">
		Efter att Qt släpps under <strong>LGPL</strong>, så kommer både
		KDE:s bibliotek och Qt vara tillgängligt under denna lite mer avslappnade licensform
		vilket gör det hela till en mer attraktiv plattform för kommersiell utveckling.
        </p>
    </li>
</ul>
</p>


<h4>
  Installera KDE 4.2.0
</h4>
<p align="justify">
 KDE, inklusive sina bibliotek och applikationer är tillgängligt gratis under
öppen källkodslicens. KDE kan hämtas i källkods och diverse binära format från<a
href="http://download.kde.org/stable/4.2.0/">http://download.kde.org</a> och kan även
fås på <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
med någon av de <a href="http://www.kde.org/download/distributions.php">populära
GNU/Linux och UNIX systemen</a>.
</p>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer har varit vänliga nog att tillhandahålla binära
  paket av KDE 4.2.0 för några versioner av sina distributioner.
  Några av dessa finns tillgänglia här <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://download.kde.org</a>.
  Ytterligare binära paket såväl som uppdatering kommer antagligen att dyka upp de
närmaste veckorna.
</p>

<p align="justify">
  <a name="package_locations"><em>Var finns paketen</em></a>.
  För en uppdaterad lista av alla de binära paket som KDE-projektet har fått kännedom
se <a href="/info/4.2.0.php">KDE 4.2.0 Informationssida</a>.
</p>

<p align="justify">
De prestandaproblem som tidigare har funnits med <em>NVidias</em> binära drivrutin för
Linux har <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">lösts</a> iom. den 
senaste betaversionen av drivrutinen.
</p>

<h4>
  Kompilera KDE 4.2.0
</h4>
<p align="justify">
  <a name="source_code"></a>
  Komplett källkod för KDE 4.2.0 finns tillgängligt<a
href="http://download.kde.org/stable/4.2.0/src/">här</a>.
Instruktioner för att kompilera och installera KDE 4.2.0 finns på
<a href="/info/4.2.0.php#binary">KDE 4.2.0 Informationssida</a>.
</p>

<h4>
    Propagera
</h4>
<p align="justify">
KDE-projektet uppmanar alla att sprida ordet om KDE 4.2 på de sociala nätverken. 
Skicka in historier till delicious, reddit, twitter och identi.ca. Ladda upp skärmdumpar till Facebook, FlickR
ipernity och Picasa. Skapa filmer till YouTube, Blip.tv, Vimeo och andra. Glöm
inte att tagga materialet med <em><strong>kde42</strong></em> så att det blir lättare
att hitta för andra. Det här är första gången KDE-projektet försöker genomföra
en koordinerad ansträngning för att meddela sig över de sociala nätverken. Var med och
hjälp till!
</p>
<p>
Hjälp till på webbforum att sprida ordet om KDE:s nya funktioner.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Presskontakt</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
