<?php
  $page_title = "Applications Leap Forward";
  $site_root = "../../";
  include "header.inc";
?>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="desktop.php">
                <img src="images/desktop-32.png" />
                Desktop
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="edu_games.php">Educational Applications and Games
                <img src="images/education-32.png" /></a>  
        </td>
    </tr>
</table>

<p>
<div align="right">
<img src="images/dolphin.png" align="right" />
</div>
The <strong>file manager, Dolphin</strong>, has seen several changes that affect your work flow
and make file management more efficient. Folders and tabs can open automatically
when you drag a file over them, making it easier to organize your files. You can
also navigate with the Breadcrumb bar. And if you want, you can have that
Breadcrumb bar always display the full path. To configure this, go to the <em>"Dolphin
Preferences"</em> in the Settings menu and put a tick in front of the <em>"Show full path
inside location bar"</em>. You can enter a custom location in the Breadcrumb bar by
clicking on the right of it, and selecting "edit". Revert it to its former state
by clicking the yellow icon on the far right. Like with all text input fields in
KDE, you can select a line of text and middle click in the field to add the
selected text at that position. If you middle-click-paste text onto the "Clear
text" icon (left facing black arrow) on the right, the current location will be
replaced.
</p>
<?php
    screenshot("dolphin-full_thumb.png", "dolphin-full.png", "", 
            "Dolphin file manager");
?>
<p>The <strong>Dolphin user interface</strong> now provides more information in an unobtrusive way.
Tool tips include large previews, and there is a <strong>capacity indicator</strong> on removable
media devices. Besides the capacity bar for the current drive, the status bar
has <strong>zoom buttons and a slider</strong> to increase and decrease the size of the icons in
your window. This works in all views, including details and columns, and makes
it easier to identify the file you are looking for or present a larger overview
of the contents of a folder.</p>

<?php
    screenshot("dolphin-capacitydrive.png", "", "", 
            "Capacity information of your thumbdrive in the Dolphin file manager");
?>

<p>You can <strong>configure how a folder presents files </strong>by choosing <em>"Adjust View
Properties"</em> in the View menu. You can now choose the view mode and the way items
are sorted in the folder. "Additional Information" allows you to choose which
information you want to show below each icon. The changes can now be applied
to the current folder only (Dolphin will remember them), to all folders or only
below the current folder. You can make these new settings default by clicking
the checkbox on the bottom.</p>

<?php
    screenshot("dolphin-adjust-view-properties.png", "", "", 
            "You can fine-tune Dolphin's visual appearance in its settings");
?>

<p>When the window becomes too small for both the capacity bar and the zoom slider
to fit, Dolphin will automatically hide one or both. Dolphin makes sure you
always have the information about the current selected item(s) at your disposal.
The space information does not show up by default; you can enable this in the
"Dolphin Preferences" in the Settings menu under <em>"General"</em>. You can also
configure prompts for confirmation of deleting or trashing files, and if it
should show the <em>"Copy To ..."</em> and <em>"Move To ..."</em> submenu items on files and
folders. These help you to quickly move files to another folder; Dolphin will
remember the last few locations where files were copied or moved to. You can
also make folders open automatically when you drag a file over them.</p>

<?php
    screenshot("dolphin-copy-to.png", "", "", 
            "Copying a file is also possible from the context menu");
?>

<h2>Other improvements</h2>

<p>Creating and extracting archives is easy with a <em>smarter and faster Ark</em>. It will
detect folders in archives, automatically creating subfolders to prevent
cluttering your working directory. Ark also supports password-protected files
and has improved drag'n'drop support. Dolphin and Konqueror now present the user
with suitable menu options when users select an archive. Click with your right
mouse button on an archive; the context menu will present several choices for
extraction. You can use the <em>"Extract here, autodetect subfolder"</em> option to avoid
getting lots of files in your current folder, yet not create duplicate
subfolders.</p>

<?php
    screenshot("ark.png", "", "", 
            "Zipping and unzipping is provided by Ark and integrated into Dolphin");
?>

<p>Lovers of Vi(m) and similar tools will appreciate the VI input mode in Kate. As
this is actually built into the KDE development platform, all applications
making use of the Kate text component, like KDevelop and KWrite, will support
this. A list of currently implemented commands can be found <a
href="http://hamberg.no/erlend/2008/11/24/the-vi-input-mode-for-kate-is-done/">
here</a>. You can now manage Kate sessions from within Plasma, either with a
widget on the desktop or panel, or directly from KRunner. Hit <em>"ALT-F2"</em> to
activate KRunner, and type (part of) the name of a Kate session, you will be
able to open it. You can add the Kate Plasma widget from the Plasma menu. The
widget will show your Kate sessions; you just click a session to open it. The
snippets and ctags plugins are available in Kate again, and the build plugin has
been improved.</p>

<?php
    screenshot("kate-sessions_thumb.png", "kate-sessions.png", "", 
            "The Kate editor's session handling can be used from KRunner and as Plasma applet as well");
?>

<p>Okular is another application that has received some VI-like features. You can
use the familiar hjkl keys to navigate pages.
</p>

<p><a href"http://utils.kde.org/projects/okteta/">Okteta</a>, a hex editor for the
raw data of files, received an innovative new view mode as an option. In this
view mode, value and char meanings are shown next to each other, as an
alternative to the traditional separation in columns. The decoding table tool
now also lists int64 and UTF-8 values.
</p>

<p>KRDC, the remote desktop tool has better support for <strong>LDAP and Microsoft's Active
Directory </strong> and cleanly separated 'per host' settings. <strong>KSnapshot</strong> now saves the
window title in <strong>snapshot metadata</strong> for easier indexing by desktop search tools
and works better for multi-monitor setups. <strong>Kcharselect</strong> now remembers recently
found characters and has improved search functionality, aiding you in quickly
finding the character you are looking for.</p>


<p>The Gwenview image viewer now can <strong>display animated graphics</strong> and has a new Red
Eye Reduction filter. You can now <strong>tag your files or rate </strong>them. Select a file by
clicking the little plus sign above it. Then enable the sidebar by clicking the
<em>"Show Sidebar"</em> button in the toolbar. On the right you can now choose a rating,
add a description or edit the tags for this picture. This is done through
Nepomuk, one of the Pillars of KDE 4. This makes sure these tags and ratings are
shared between applications using Nepomuk, like Dolphin.
</p>

<!--
Sexy screencast of Gwenview including some tagging, rating, selecting files. Or
2 small screenshots.
-->

<p>The Kopete chat client has seen improvements in its MSN support and in the user
interface. File transfer requests show up in the chatwindow instead of annoying
you with a popup, and there are message delivery notifications. Last but not
least, there is now Jabber Jingle Audio support.
</p>
<h2>Personal Information Management to keep track of your life</h2>

<!--
Generic KDE PIM screenshot (startup of kontact probably works best)
-->

<p>The KDE PIM suite offers a whole range of applications to manage your email,
newsfeeds or agenda. The KDE PIM applications can each be started separately or
work together in the Kontact interface. The Kontact interface has a <strong>new planner
summary and better drag'n'drop support</strong>. KNotes can send notes over a local
network and KJots can import notes from Knowit. The RSS application, Akregator,
saves all open tabs when you end a session so you don't lose the webpages you
were reading. Of course, this assumes session support in SystemSettings is
enabled. If you want to configure this, search for <em>"session"</em> in SystemSettings,
and choose <em>"Session Manager"</em>. You can ask KDE to remember the applications and
documents you have running when you log out and restore them when you come back.
You will be asked before logging out if you want to save unsaved files.</p>

<p>Work from a Summer Of
Code branch was merged into KMail, marking a big step in the efforts to port
KMail to the KDE 4 infrastructure. The changes bring a <strong>reworked folder tree and
a new message list</strong>, much more powerful than the old ones, and better performance
and scalability even in huge folders. It is now possible to configure the
display of additional information in the folderview so you can optimize the view
for each folder.</p>


<?php
    screenshot("kmail-full_thumb.png", "kmail-full.png", "",
                "KMail's in KDE 4.2");
?>


<p>Above the message list, to the right of the search bar you can find a row of
buttons. The first button filters on message status. The second one brings up an
advanced search panel. You can use it to search specified folders for messages
with certain properties. It is possible to save searches as folders, which get
automatically updated when new mail comes in. The third button allows you to
configure message sorting. You can use the fourth button to group mails by
certain properties. You can choose one of the predefined groupings or create
your own. The fifth button allows you to specify the information you want to be
shown. You can choose one of the preset 'themes', or configure one yourself.
With an easy drag and drop interface, you can 'build' the list of messages to
your liking.</p>

<?php
    screenshot("kmail-listview_thumb.png", "kmail-listview.png", "",
                "KMail's reworked email list offers new flexible ways of viewing your emails");
?>


<p>Besides the user interface work, there have been quite some 
<strong>bugfixes</strong> in the IMAP support and for other protocols.</p>

<p>The Konqueror web browser improved support for scalable vector graphics and
became a lot faster thanks to contributions from various developers and code and
ideas from WebKit. A new find dialog makes for less intrusive searching inside
webpages. Press <em>"CTRL-F"</em> in a webpage. The find dialog will appear on the bottom
of the window. You can start typing immediately, and use (SHIFT) "F3" to go to
the (previous) next match. You can still quickly search for words in a webpage
by pressing "/" and typing in the word(s) you are looking for. Again you can use
<em>(SHIFT) "F3"</em> to go to the previous and next match.</p>
<?php
    screenshot("konqueror-search_thumb.png", "konqueror-search.png", "", 
            "The new search bar makes for a clearer presentation in the webbrowser");
?>

<p>When you start Konqueror, it will now show your <strong>bookmarks
</strong>. Of course you can
change this by going to the Settings menu and choosing "Configure Konqueror" on
the bottom of the menu. Just below the Konqueror icon on the top-right is a drop
down menu which allows you to choose what Konqueror shows you when you start it.
Besides the bookmarks, you can opt for a blank page (which leads to faster
startup), show your homepage (the location of which can be configured just below
the drop down menu) or an introduction page.</p>

<p>The Konqueror developers have been working on <strong>Webshortcuts</strong>, short (often two or
three letter) "words" you can type in the location bar, followed by search terms.
Konqueror then searches the Webshortcut site for your search terms. For example,
type in wp:kde and Konqueror will show you the Wikipedia site for KDE. Or you
can type in "imdb:curse of the golden flower" and Konqueror will search imdb.com
for the movie. It is easy to create a webshortcut. On any webpage which features
a search bar (for example, your favorite news site), you can click with your
right mouse button on the search field and choose "Create web shortcut". Now
choose a few logical letters as the webshortcut and give the search a name,
click OK and a new webshortcut has been created! These shortcuts can be
displayed and managed with the Konqueror configuration screen. Go to the
Settings menu and choose "Configure Konqueror" on the bottom of the menu. On the
left you see a list with different groups of settings. Under "Web Browsing",
clicking on "Web Shortcuts" brings up a list of webshortcuts and some settings,
including default search and search delimiter.</p>


<p><strong>Tip:</strong> If you set the "Keyword delimiter" to "Space", you can make Konqueror use
the "Default search engine" if you type in your search terms in the location bar
and hit enter. For example: if you set the "Default search engine" to Wikipedia
(wp), Konqueror will search Wikipedia when you type in one or more words in the
locationbar, instead of a website address. You can still use other shortcuts,
but you don't have to use the semicolon. For example, type "imdb curse of the
golden flower", and Konqueror will search imdb.com for "curse of the golden
flower".</p>

<p><strong>Bonus tip:</strong> These shortcuts also work in 
<strong>KRunner</strong>! Just hit <em>"ALT-F2"</em>, type "imdb
curse of the golden flower", choose the imdb search and Konqueror will pop up
with the imdb.com website, effectively turning Konqueror into a search engine
for movies and TV series. (The KRunner Web Shortcuts plugin must be enabled for
this to work.)</p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="desktop.php">
                <img src="images/desktop-32.png" />
                Desktop
                </a>        
        </td>
        <td align="right" width="50%">
                <a href="edu_games.php">Educational Applications and Games
                <img src="images/education-32.png" /></a>  
        </td>
    </tr>
</table>

<?php
  include("footer.inc");
?>
