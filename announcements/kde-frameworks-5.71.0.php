<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.71.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.71.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
June 06, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.71.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Store filename terms just once");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Improve accuracy of battery percentage icons");?></li>
<li><?php i18n("Fix KML mimetype icon");?></li>
<li><?php i18n("Change mouse icon to have better dark theme contrast (bug 406453)");?></li>
<li><?php i18n("Require in-source build (bug 421637)");?></li>
<li><?php i18n("Add 48px places icons (bug 421144)");?></li>
<li><?php i18n("Adds a missing LibreOffice icon shortcut");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("[android] Use newer Qt version in example");?></li>
<li><?php i18n("[android] Allow specifying APK install location");?></li>
<li><?php i18n("ECMGenerateExportHeader: add generation of *_DEPRECATED_VERSION_BELATED()");?></li>
<li><?php i18n("ECMGeneratePriFile: fix for ECM_MKSPECS_INSTALL_DIR being absolute");?></li>
<li><?php i18n("ECMGeneratePriFile: make the pri files relocatable");?></li>
<li><?php i18n("Suppress find_package_handle_standard_args package name mismatch warning");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Use logo.png file as logo as default, if present");?></li>
<li><?php i18n("Change public_example_dir to public_example_dirs, to enable multiple dirs");?></li>
<li><?php i18n("Drop code for Python2 support");?></li>
<li><?php i18n("Adapt links to repo to invent.kde.org");?></li>
<li><?php i18n("Fix link to kde.org impressum");?></li>
<li><?php i18n("History pages of KDE4 got merged into general history page");?></li>
<li><?php i18n("Unbreak generation with dep diagrams with Python 3 (break Py2)");?></li>
<li><?php i18n("Export metalist to json file");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Use ECMGenerateExportHeader to manage deprecated API better");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Use UI marker context in more tr() calls");?></li>
<li><?php i18n("[KBookmarkMenu] Assign m_actionCollection early to prevent crash (bug 420820)");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Add X-KDE-KCM-Args as property, read property in module loader");?></li>
<li><?php i18n("Fix crash when loading an external app KCM like yast (bug 421566)");?></li>
<li><?php i18n("KSettings::Dialog: avoid duplicate entries due cascading $XDG_DATA_DIRS");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Wrap also API dox with KCOMPLETION_ENABLE_DEPRECATED_SINCE; use per method");?></li>
<li><?php i18n("Use UI marker context in more tr() calls");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Don't try to initialize deprecated SaveOptions enum value");?></li>
<li><?php i18n("Add KStandardShortcut::findByName(const QString&amp;) and deprecate find(const char*)");?></li>
<li><?php i18n("Fix KStandardShortcut::find(const char*)");?></li>
<li><?php i18n("Adjust name of internally-exported method as suggested in D29347");?></li>
<li><?php i18n("KAuthorized: export method to reload restrictions");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("[KColorScheme] Remove duplicated code");?></li>
<li><?php i18n("Make Header colors fallback to Window colors first");?></li>
<li><?php i18n("Introduce the Header color set");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix Bug 422291 - Preview of XMPP URI's in KMail (bug 422291)");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Don't invoke qstring localized stuff in critical section");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Create kcmshell.openSystemSettings() and kcmshell.openInfoCenter() functions");?></li>
<li><?php i18n("Port KKeySequenceItem to QQC2");?></li>
<li><?php i18n("Pixel align children of GridViewInternal");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Fix blurry icons in titlebar appmenu by adding UseHighDpiPixmaps flag");?></li>
<li><?php i18n("Add systemd user service file for kded");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("[TaglibExtractor] Add support for Audible \"Enhanced Audio\" audio books");?></li>
<li><?php i18n("honor the extractMetaData flag");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Fix bug with components containing special characters (bug 407139)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Update Taiwanese holidays");?></li>
<li><?php i18n("holidayregion.cpp - provide translatable strings for the German regions");?></li>
<li><?php i18n("holidays_de-foo - set name field for all German holiday files");?></li>
<li><?php i18n("holiday-de-&lt;foo&gt; - where foo is a region is Germany");?></li>
<li><?php i18n("Add holiday file for DE-BE (Germany/Berlin)");?></li>
<li><?php i18n("holidays/plan2/holiday_gb-sct_en-gb");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Add some sanity and bounds checking");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Introduce KIO::OpenUrlJob, a rewrite and replacement for KRun");?></li>
<li><?php i18n("KRun: deprecate all static 'run*' methods, with full porting instructions");?></li>
<li><?php i18n("KDesktopFileActions: deprecate run/runWithStartup, use OpenUrlJob instead");?></li>
<li><?php i18n("Look for kded as runtime dependency");?></li>
<li><?php i18n("[kio_http] Parse a FullyEncoded QUrl path with TolerantMode (bug 386406)");?></li>
<li><?php i18n("[DelegateAnimationHanlder] Replace deprecated QLinkedList with QList");?></li>
<li><?php i18n("RenameDialog: Warn when file sizes are not the same (bug 421557)");?></li>
<li><?php i18n("[KSambaShare] Check that both smbd and testparm are available (bug 341263)");?></li>
<li><?php i18n("Places: Use Solid::Device::DisplayName for DisplayRole (bug 415281)");?></li>
<li><?php i18n("KDirModel: fix hasChildren() regression for trees with files shown (bug 419434)");?></li>
<li><?php i18n("file_unix.cpp: when ::rename is used as condition compare its return to -1");?></li>
<li><?php i18n("[KNewFileMenu] Allow creating a dir named '~' (bug 377978)");?></li>
<li><?php i18n("[HostInfo] Set QHostInfo::HostNotFound when a host isn't found in the DNS cache (bug 421878)");?></li>
<li><?php i18n("[DeleteJob] Report final numbers after finishing (bug 421914)");?></li>
<li><?php i18n("KFileItem: localize timeString (bug 405282)");?></li>
<li><?php i18n("[StatJob] Make mostLocalUrl ignore remote (ftp, http...etc) URLs (bug 420985)");?></li>
<li><?php i18n("[KUrlNavigatorPlacesSelector] only update once the menu on changes (bug 393977)");?></li>
<li><?php i18n("[KProcessRunner] Use only executable name for scope");?></li>
<li><?php i18n("[knewfilemenu] Show inline warning when creating items with leading or trailing spaces (bug 421075)");?></li>
<li><?php i18n("[KNewFileMenu] Remove redundant slot parameter");?></li>
<li><?php i18n("ApplicationLauncherJob: show the Open With dialog if no service was passed");?></li>
<li><?php i18n("Make sure the program exists and is executable before we inject kioexec/kdesu/etc");?></li>
<li><?php i18n("Fix URL being passed as argument when launching a .desktop file (bug 421364)");?></li>
<li><?php i18n("[kio_file] Handle renaming file 'A' to 'a' on FAT32 filesystems");?></li>
<li><?php i18n("[CopyJob] Check if destination dir is a symlink (bug 421213)");?></li>
<li><?php i18n("Fix service file specifying 'Run in terminal' giving an error code 100 (bug 421374)");?></li>
<li><?php i18n("kio_trash: add support for renaming directories in the cache");?></li>
<li><?php i18n("Warn if info.keepPassword isn't set");?></li>
<li><?php i18n("[CopyJob] Check free space for remote urls before copying and other improvements (bug 418443)");?></li>
<li><?php i18n("[kcm trash] Change kcm trash size percent to 2 decimal places");?></li>
<li><?php i18n("[CopyJob] Get rid of an old TODO and use QFile::rename()");?></li>
<li><?php i18n("[LauncherJobs] Emit description");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Add section headers in sidebar when all the actions are expandibles");?></li>
<li><?php i18n("fix: Padding in overlay sheet header");?></li>
<li><?php i18n("Use a better default color for Global Drawer when used as Sidebar");?></li>
<li><?php i18n("Fix inaccurate comment about how GridUnit is determined");?></li>
<li><?php i18n("More reliable parent tree climbing for PageRouter::pushFromObject");?></li>
<li><?php i18n("PlaceholderMessage depends on Qt 5.13 even if CMakeLists.txt requires 5.12");?></li>
<li><?php i18n("introduce the Header group");?></li>
<li><?php i18n("Don't play close animation on close() if sheet is already closed");?></li>
<li><?php i18n("Add SwipeNavigator component");?></li>
<li><?php i18n("Introduce Avatar component");?></li>
<li><?php i18n("Fix the shaders resource initialisation for static builds");?></li>
<li><?php i18n("AboutPage: Add tooltips");?></li>
<li><?php i18n("Ensure closestToWhite and closestToBlack");?></li>
<li><?php i18n("AboutPage: Add buttons for email, webAddress");?></li>
<li><?php i18n("Always use Window colorset for AbstractApplicationHeader (bug 421573)");?></li>
<li><?php i18n("Introduce ImageColors");?></li>
<li><?php i18n("Recommend better width calculation for PlaceholderMessage");?></li>
<li><?php i18n("Improve PageRouter API");?></li>
<li><?php i18n("Use small font for BasicListItem subtitle");?></li>
<li><?php i18n("Add support for layers to PagePoolAction");?></li>
<li><?php i18n("Introduce RouterWindow control");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("Use UI marker context in more tr() calls");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Don't duplicate error messages in a passive notification (bug 421425)");?></li>
<li><?php i18n("Fix incorrect colours in the KNS Quick messagebox (bug 421270)");?></li>
<li><?php i18n("Do not mark entry as uninstalled if uninstallation script failed (bug 420312)");?></li>
<li><?php i18n("KNS: Deprecate isRemote method and handle parse error properly");?></li>
<li><?php i18n("KNS: Do not mark entry as installed if install script failed");?></li>
<li><?php i18n("Fix showing updates when the option is selected (bug 416762)");?></li>
<li><?php i18n("Fix update auto selection (bug 419959)");?></li>
<li><?php i18n("Add KPackage support to KNewStuffCore (bug 418466)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Implement lock-screen visibility control on Android");?></li>
<li><?php i18n("Implement notification grouping on Android");?></li>
<li><?php i18n("Display rich text notification messages on Android");?></li>
<li><?php i18n("Implement urls using hints");?></li>
<li><?php i18n("Use UI marker context in more tr() calls");?></li>
<li><?php i18n("Implement support for notification urgency on Android");?></li>
<li><?php i18n("Remove checks for notification service and fallback to KPassivePopup");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Simpler background rounding check");?></li>
<li><?php i18n("PieChart: Render a torus segment as background when it isn't a full circle");?></li>
<li><?php i18n("PieChart: Add a helper function that takes care of rounding torus segments");?></li>
<li><?php i18n("PieChart: Expose fromAngle/toAngle to shader");?></li>
<li><?php i18n("Always render the background of the pie chart");?></li>
<li><?php i18n("Always set background color, even when toAngle != 360");?></li>
<li><?php i18n("Allow start/end to be at least 2pi away from each other");?></li>
<li><?php i18n("Rework PieChart implementation");?></li>
<li><?php i18n("Fixup and comment sdf_torus_segment sdf function");?></li>
<li><?php i18n("Don't explicitly specify smoothing amount when rendering sdfs");?></li>
<li><?php i18n("Workaround lack of array function parameters in GLES2 shaders");?></li>
<li><?php i18n("Handle differences between core and legacy profiles in line/pie shaders");?></li>
<li><?php i18n("Fix a few validation errors");?></li>
<li><?php i18n("Update the shader validation script with the new structure");?></li>
<li><?php i18n("Add a separate header for GLES3 shaders");?></li>
<li><?php i18n("Remove duplicate shaders for core profile, instead use preprocessor");?></li>
<li><?php i18n("expose both name and shortName");?></li>
<li><?php i18n("support different long/short labels (bug 421578)");?></li>
<li><?php i18n("guard model behind a QPointer");?></li>
<li><?php i18n("Add MapProxySource");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Do not persist runner whitelist to config");?></li>
<li><?php i18n("KRunner fix prepare/teardown signals (bug 420311)");?></li>
<li><?php i18n("Detect local files and folders starting with ~");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Add X-KDE-DBUS-Restricted-Interfaces to Application desktop entry fields");?></li>
<li><?php i18n("Add missing compiler deprecation tag for 5-args KServiceAction constructor");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add .diff to the file-changed-diff to enable mime detection on windows");?></li>
<li><?php i18n("scrollbar minimap: performance: delay update for inactive documents");?></li>
<li><?php i18n("Make text always align with font base line");?></li>
<li><?php i18n("Revert \"Store and fetch complete view config in and from session config\"");?></li>
<li><?php i18n("Fix modified line marker in kate minimap");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Be noisy about deprecated KPageWidgetItem::setHeader(empty-non-null string)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("[KMainWindow] Invoke QIcon::setFallbackThemeName (later) (bug 402172)");?></li>
</ul>

<h3><?php i18n("KXmlRpcClient");?></h3>

<ul>
<li><?php i18n("Mark KXmlRpcClient as porting aid");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("PC3: Add separator to toolbar");?></li>
<li><?php i18n("support the Header color group");?></li>
<li><?php i18n("Implement scroll and drag adjustment of values for SpinBox control");?></li>
<li><?php i18n("Use font: instead of font.pointSize: where possible");?></li>
<li><?php i18n("kirigamiplasmastyle: Add AbstractApplicationHeader implementation");?></li>
<li><?php i18n("Avoid potential disconnect of all signals in IconItem (bug 421170)");?></li>
<li><?php i18n("Use small font for ExpandableListItem subtitle");?></li>
<li><?php i18n("Add smallFont to Kirigami plasma style");?></li>
<li><?php i18n("[Plasmoid Heading] Draw the heading only when there is an SVG in the theme");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Add ToolSeparator styling");?></li>
<li><?php i18n("Remove \"pressed\" from CheckIndicator \"on\" state (bug 421695)");?></li>
<li><?php i18n("Support the Header group");?></li>
<li><?php i18n("Implement smallFont in Kirigami plugin");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Add a QString Solid::Device::displayName, used in Fstab Device for network mounts (bug 415281)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Allow overriding to disable auto language detection (bug 394347)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Update Raku extensions in Markdown blocks");?></li>
<li><?php i18n("Raku: fix fenced code blocks in Markdown");?></li>
<li><?php i18n("Assign \"Identifier\" attribute to opening double quote instead of \"Comment\" (bug 421445)");?></li>
<li><?php i18n("Bash: fix comments after escapes (bug 418876)");?></li>
<li><?php i18n("LaTeX: fix folding in \end{...} and in regions markers BEGIN-END (bug 419125)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.71");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
