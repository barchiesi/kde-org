<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.8.9 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.8.9";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-plymouth' href='https://commits.kde.org/breeze-plymouth'>Breeze Plymouth</a> </h3>
<ul id='ulbreeze-plymouth' style='display: block'>
<li>Fix overlapping labels on systems with different window sizes. <a href='https://commits.kde.org/breeze-plymouth/cc1019e2938a9ac9bf9a6e7149b55deec90e010a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8721'>D8721</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make sure we remove the Transaction before is destroyed. <a href='https://commits.kde.org/discover/37e8e5db4915f55b2706e635cd14f2226d1698c8'>Commit.</a> </li>
<li>Fix test run. <a href='https://commits.kde.org/discover/4fc2782a3d5de3a484a7e122b0ba3915b3685a03'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Also list GTK 3 themes without "gtk-3.0" subfolder. <a href='https://commits.kde.org/kde-gtk-config/1ba9f200f0fb53b1dab7e810f8a1920f18e55322'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9459'>D9459</a></li>
<li>Fix minimum height of cursor/icon theme comboboxes. <a href='https://commits.kde.org/kde-gtk-config/9615fc3994f962846791f790f9d18e2f42d774d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9457'>D9457</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Correction with the & problem in tabs. <a href='https://commits.kde.org/ksysguard/0ed4aae225c0dd67a0ae86b8e5ffdaf574ac16a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382512'>#382512</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10151'>D10151</a></li>
<li>Use OCS to retrieve Tabs from the KDE store. <a href='https://commits.kde.org/ksysguard/fdead6c30886e17ea0e590df2e7ddb79652fb328'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338669'>#338669</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8734'>D8734</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Remove unnecessary QString::arg call. <a href='https://commits.kde.org/kwin/73f5b09e3bbe62b0a09996d244eec5328f758dcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8764'>D8764</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[kimpanel] Use automoc keyword for kimpanel-scim-panel. <a href='https://commits.kde.org/plasma-desktop/11d25c15254fe7630e46ede8c3cf8b2346bdd3bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9245'>D9245</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Notifications] Fix grouping. <a href='https://commits.kde.org/plasma-workspace/8164beac15ea34ec0d1564f0557fe3e742bdd938'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10315'>D10315</a></li>
<li>Make sure device paths are quoted. <a href='https://commits.kde.org/plasma-workspace/9db872df82c258315c6ebad800af59e81ffb9212'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389815'>#389815</a></li>
<li>Add license header to test. <a href='https://commits.kde.org/plasma-workspace/3b4a81d2fdd2df7e6e94c64a6c9f18103ec5e821'>Commit.</a> </li>
<li>Sanitise notification HTML. <a href='https://commits.kde.org/plasma-workspace/5bc696b5abcdb460c1017592e80b2d7f6ed3107c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10188'>D10188</a></li>
<li>Fixed a freeze caused by certain notifications. <a href='https://commits.kde.org/plasma-workspace/5e230a6290b1ff61e54c43da48821eb2bf3192ae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381154'>#381154</a></li>
<li>Fix for xembedsniproxy crash due to NULL returned from xcb_image_get(). <a href='https://commits.kde.org/plasma-workspace/12e3568042fb365aad3eccf2fefa58bbeb065210'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9732'>D9732</a></li>
<li>Fix compilation with current cmake (git branch release). <a href='https://commits.kde.org/plasma-workspace/d02f03f46e9c380c2a18045b335a8e26f385999d'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
