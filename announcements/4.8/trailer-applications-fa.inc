
<h3>
<a href="applications.php">
برنامه‌های کی‌دی‌ای ۴٫۸ مدیریت پرونده‌ی سریعتر و مقیاس‌پذیرتری را پیشکش میکنند
</a>
</h3>

<p>
<a href="applications.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.8"/>
</a>
￼برنامه‌های کی‌دی‌ای که امروز منتشر شدند شامل <a href="http://userbase.kde.org/Dolphin">دلفین</a> با موتور جدید نمایشش،نسخه‌ی جدید <a href="http://userbase.kde.org/Kate">Kate</a> با قابلیتها و بهینه‌سازی‌ها، <a href="http://userbase.kde.org/Gwenview">Gwenview</a> با پیشرفت در نمایش و تواناییها میشوند. تلپاتی کی‌دی‌ای اولین مراحل بتلی خود را پشت‌سر میگذارد. قابلیت‌های جدید <a href="http://userbase.kde.org/Marble/en">Marble</a> همچنان در حال بیشتر شدن هستند که در میان آن‌ها میتوان به نمایه (پروفایل) ارتفاع، ردیابی ماهواره و پیوستگی با <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> اشاره کرد. <a href="applications-fa.php">متن کامل اعلان برنامه‌های کی‌دی‌ای را بخوانید</a>.
<br /><br />
<br /><br />
</p>
