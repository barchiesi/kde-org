<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.7.2 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.7.2';
include "header.inc";
?>
<p><a href="plasma-5.7.2.php">Plasma 5.7.2</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Fixed kde4 compilation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=47a397ebef7a636497e75a8da81afffbffa30dda'>Commit.</a> </li>
<li>Sync SH_Widget_Animate with internal "enable animations" option. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ea4fdcaf6cac07490712e3bb6aa791960f68e410'>Commit.</a> </li>
<li>Fixed tab order. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1b90d45bfae12b61ab3f5b4f3b2ee82fb03f9731'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365603'>#365603</a></li>
</ul>


<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Don't ignore error notification. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=843654e9a89a8158d16a7e8dca498744ea831922'>Commit.</a> </li>
<li>Don't use qobject_cast when destroying for a look-up. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e7d7b5aaeb8aae9e5fc8dd2396470157ca081de5'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Enable image rotation from exif data. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=167922b445a581d03fd88accf084a102e391e1a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365707'>#365707</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Properly get error message for eglInitialize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aa7fb81e0052af7a91b35b619a16ac16a9f994b9'>Commit.</a> </li>
<li>[platforms/x11] Add more warning on failure to create EGL context. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=70167b748d4c5dd7b0eabcafa6c31b688f52d698'>Commit.</a> </li>
<li>[platforms/drm] Add better warnings in DrmBuffer if things fail. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1c275a02c81bf08e694b0a5a70f272597e3666f9'>Commit.</a> </li>
<li>[platforms/fbdev] Handle error conditions more gracefully. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f0aeda07383533a7908dd287c02e3a4a1f9cb532'>Commit.</a> </li>
<li>Call QCoreApplication::exit instead of stdlib exit to terminate if platform fails. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=56c2e158ee4e8f78aed45d2aa41562961ec28d26'>Commit.</a> </li>
<li>[platforms/drm] Properly handle case that mapping the blank buffer fails. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cd9a0afafa56bd251ef48022b82dd2c037e4f3ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365242'>#365242</a></li>
<li>[platforms/fbdev] Properly detect a BGR image format. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aaf8ce16dfb258a48ea25209b6747f5d9f5a85b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365243'>#365243</a></li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Properly propagate animation config changed events. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=76cfa29ad99ff07d3a5b3dcbe6574121b01c9931'>Commit.</a> </li>
<li>Sync SH_Widget_Animate with internal "enable animations" option. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=a1573b1756fab439c50df0f0ab060899bf568dfd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365628'>#365628</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Simplify and fix circular binding. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d9457f1a22809fada8cb32c8e5f15f65f9255a5b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365639'>#365639</a></li>
<li>Fix managing separateLaunchers prop for Icontasks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a1378f8591691ecf32ab4b87349d2037ae9c569a'>Commit.</a> See bug <a href='https://bugs.kde.org/365617'>#365617</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Switching activity while creating it from PlasmaScript. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a566273eb80dd2eb40c4fdea381d3f97bf57c0aa'>Commit.</a> </li>
<li>ScriptEngine exports the list of activities known to Plasma. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=85d721b91f46db89c24aa20081ddacd1fe29d17c'>Commit.</a> </li>
<li>Removed unused getter for KActivities::Controller. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e386ad6a582ef986145e2b95ed0fdf03dd78e3fa'>Commit.</a> </li>
<li>No need to keep both Consumer and Controller around. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a88d2909db21065f699414fd878bfab2b44820a8'>Commit.</a> </li>
<li>Removing the Activity wrapper class from the Shell. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=176ddeb40c1238781daa7d817b5e5124f621d7bf'>Commit.</a> </li>
<li>Removing dead code - activity-event-handling private slots. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2f7c8bb0f475f720097a166346fe0d390a3d6bbb'>Commit.</a> </li>
<li>The connection to a lambda should get disconnected when the shell is deleted. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fcf5896dcd19e9454c8c34e272ee1a539a8870e3'>Commit.</a> </li>
<li>Memory-optimizing Activity object not to keep duplicate data. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=eb0fc3c242d608de46f029c3aeda511bc8bf54b6'>Commit.</a> </li>
<li>Don't try to recycle activities. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7db10817bdb27f3043d1d66d5a67a4b9d6012c5d'>Commit.</a> </li>
<li>No reason for KActivities::Consumer when we already have a Controller. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fe810eb404d3aa83c67329f72e7cf60a79af5d8'>Commit.</a> </li>
<li>[System Tray] Animate only the icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1caec23e0e9201e7353e15267f9ea28d9c3653f1'>Commit.</a> </li>
<li>[System Tray] Round item size to icon size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a3ea8abb5a545d7cad5c534ad3070e9165f3097'>Commit.</a> </li>
<li>Actually connect the filter bar in holidays plugin. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5150ca7b0fa3317b5c57e16cc21d48a136358293'>Commit.</a> </li>
<li>Convert all state checks to KWindowInfo::hasState(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f4a1d740f1e4428f31fd13f66ec2a54bf467dd35'>Commit.</a> </li>
<li>Adjust indices in manual sort map by removal delta after removal, not leading up to it. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=56ea0f5947ac3ef285acca2981324c9afbb1c5ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365173'>#365173</a></li>
<li>Move formerly launcher-backed window tasks out of launcher area on launcher removal. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3e0c3f4db409d11361162f249ae5006e4a57a324'>Commit.</a> See bug <a href='https://bugs.kde.org/365617'>#365617</a></li>
<li>Fix edge case with launchInPlace + SortManual + group. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c3941de089b886d4fbfcc99bfec0bc62e03db894'>Commit.</a> See bug <a href='https://bugs.kde.org/365617'>#365617</a></li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c7732a7cfbbd52aee3facc426cf1a24f6e21d78a'>Commit.</a> </li>
<li>Create a containment per screen on activity creation. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=70e2684cace606dae1621034dd02f1605b26723c'>Commit.</a> </li>
<li>Fix startup-to-window matchup based on AppName. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fd011ae918d241140dcbbcddf7bde2d06f1b608'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
