<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix bug introduced when we change attribute on dialogbox. <a href='http://commits.kde.org/akonadi/c8411faf2b132c6d832731f71646cb67f6278b9b'>Commit.</a> </li>
<li>Set KAboutData so that DrKonqi can report bugs. <a href='http://commits.kde.org/akonadi/3eb4e23baa4f15509598ecf22299b3bc01d2fc7e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375098'>#375098</a></li>
<li>Fix database deadlock introduced in commit d54d1f241. <a href='http://commits.kde.org/akonadi/6adadc171c24cdf53258939034fc213418d9e9b7'>Commit.</a> </li>
<li>Monitor: fix handling of CollectionRemoved notifications. <a href='http://commits.kde.org/akonadi/5aafccffd5d0b0a041ea0a862a409844861efebf'>Commit.</a> </li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Show]</a></h3>
<ul id='ulakonadiconsole' style='display: none'>
<li>Make "Dump to XML" work again. <a href='http://commits.kde.org/akonadiconsole/1b8339fa45c7e496e89721a31d0ef9cd1dfe4fcb'>Commit.</a> </li>
<li>Fix double-click in instance selector dialog. <a href='http://commits.kde.org/akonadiconsole/70f639360bb5a406176db0a8611bc8ae69da7d0c'>Commit.</a> </li>
<li>Fix at(-1) being called in some cases (e.g. after clear view). <a href='http://commits.kde.org/akonadiconsole/cccc9967e2493ecb1b40ed4dcca5cafdae8a811f'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Stop crashing when loading the konqueror's webengine part. <a href='http://commits.kde.org/ark/b5e59a044d392ede62af23076bbf0a154d00bf60'>Commit.</a> </li>
</ul>
<h3><a name='bovo' href='https://cgit.kde.org/bovo.git'>bovo</a> <a href='#bovo' onclick='toggle("ulbovo", this)'>[Show]</a></h3>
<ul id='ulbovo' style='display: none'>
<li>Add a summary tag. <a href='http://commits.kde.org/bovo/773c1f526eebd93ed1ff522a5d2f9724308c1ee2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129887'>#129887</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>[KStandardItemListWidget] Update icon when palette changes. <a href='http://commits.kde.org/dolphin/86d2aa321d54bf3ae6c95476d649634fd0ff68d5'>Commit.</a> </li>
<li>Hide progress info for rename jobs in rename dialog. <a href='http://commits.kde.org/dolphin/e582b13f8485dc9c9fc4b23f72d007dc99deec9b'>Commit.</a> </li>
<li>Informationpanel: don't change color of scrollarea's viewport. <a href='http://commits.kde.org/dolphin/c1d9becda259c0df58b1447b69f573b6ef13bfa1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366203'>#366203</a>. Code review <a href='https://git.reviewboard.kde.org/r/129861'>#129861</a></li>
<li>Fix missing audio duration in details view. <a href='http://commits.kde.org/dolphin/2e8e30026a207d380379fb2be0e4327bf4f455e8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127799'>#127799</a></li>
<li>Don't overwrite custom icons for the show_hidden_files action. <a href='http://commits.kde.org/dolphin/85121a8115c5a8b6b7c0a01073663d51bd64e6ae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374508'>#374508</a>. Code review <a href='https://git.reviewboard.kde.org/r/129789'>#129789</a></li>
</ul>
<h3><a name='kajongg' href='https://cgit.kde.org/kajongg.git'>kajongg</a> <a href='#kajongg' onclick='toggle("ulkajongg", this)'>[Show]</a></h3>
<ul id='ulkajongg' style='display: none'>
<li>Remove a wrong assertion. <a href='http://commits.kde.org/kajongg/ac615e8dba8dbf90df5664413d8e2924034145ea'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Show]</a></h3>
<ul id='ulkalarm' style='display: none'>
<li>Don't add layout to widget which already has one. <a href='http://commits.kde.org/kalarm/422a3e478ee4e5cf4a568f2ec7a4d6c7bb88dde7'>Commit.</a> </li>
<li>Bug 375615: Don't show misleading error message if command alarm fails. <a href='http://commits.kde.org/kalarm/92b9076a46a8a3f20bb6d2ab37a8e466a0196911'>Commit.</a> </li>
<li>By default, don't show technical Akonadi error descriptions. <a href='http://commits.kde.org/kalarm/6cd6bbae49a4eff15da8d46e11f253b00245693c'>Commit.</a> </li>
<li>Fix system tray icon used for "some alarms disabled". <a href='http://commits.kde.org/kalarm/88d9170a70b926940e37f0f0d2d7af241fb62757'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>[project plugin] Clarify foxus proxies. <a href='http://commits.kde.org/kate/924e7a38f959885e1c1bef236ddb85a847095550'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129903'>#129903</a></li>
<li>Resolve symbolic links in findDocument() just as KTextEditor does. <a href='http://commits.kde.org/kate/ff1ab8bd0fd23f189960d40755354b674ed019ac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374564'>#374564</a>. Code review <a href='https://git.reviewboard.kde.org/r/129792'>#129792</a></li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Remove uncrustify script from stable branch, it'll be in master only. <a href='http://commits.kde.org/kde-dev-scripts/4caf9a0c937bf8d415e73559a2a02e068a78ed9e'>Commit.</a> </li>
<li>Fix unwanted space after * in "Private * const d;". <a href='http://commits.kde.org/kde-dev-scripts/d7bd1425f9a8e6d10f0fd5445b725ce5cd13a6a6'>Commit.</a> </li>
<li>Add a script to run uncrustify for source code reformatting. <a href='http://commits.kde.org/kde-dev-scripts/e93a28b3eb153695e8edb7ae259243b7b409a29d'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Fix build for GCC 7. <a href='http://commits.kde.org/kdelibs/f635d3b2347b400b74f199f1904a5129def28890'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Project archiving: fix color clips incorrectly detected and warn before overwriting project file. <a href='http://commits.kde.org/kdenlive/018de768588be0f90729f5bc2c673036ae057765'>Commit.</a> </li>
<li>Fix crash on razor with multiple clips selected. <a href='http://commits.kde.org/kdenlive/6d71034c432bb009947f6fff5757564521aa66fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376082'>#376082</a></li>
<li>Workaround timeline crash caused by now fixed MLT issue (could be triggered by editing a title clip):. <a href='http://commits.kde.org/kdenlive/885fb4d0eb1224cd7f25eb2c42228433ca16fb9e'>Commit.</a> </li>
<li>Fix various aspect ratio issues and transparency of composite transition in some cases. <a href='http://commits.kde.org/kdenlive/67257981b15cd472a08e0fffc9c54072da6b617a'>Commit.</a> </li>
<li>Add option to add custom mime types for allowed clips. <a href='http://commits.kde.org/kdenlive/e844edc049861686f4a909be33431523bf06a386'>Commit.</a> See bug <a href='https://bugs.kde.org/364269'>#364269</a></li>
<li>Fix title clip items need to be selected twice before allowing move. <a href='http://commits.kde.org/kdenlive/91b268c2727105afcfddf082678c10704c348862'>Commit.</a> </li>
<li>Fix crash when opening titler with movit GPU. <a href='http://commits.kde.org/kdenlive/048a3d746459fc2f0dd68aa350e96c771b1e8a53'>Commit.</a> </li>
<li>Fix play action not pausing when switching between subclips. <a href='http://commits.kde.org/kdenlive/9aca0ee731bcb3db2e9924901e339555bdfa1f20'>Commit.</a> </li>
<li>Fix playing Zone breaks monitor ruler length. <a href='http://commits.kde.org/kdenlive/f540402274e8db9912136e8aacfe60fe124e2c7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375163'>#375163</a></li>
<li>Fix typo breaking playlist clip expand. <a href='http://commits.kde.org/kdenlive/b9ccc837ef9759155d768a40a1f8dd49fd7b9c29'>Commit.</a> </li>
<li>Fix extract frame not remembering folder. <a href='http://commits.kde.org/kdenlive/901c3ecfd2e2455eac6f5dc27ee87d66f2ddb87a'>Commit.</a> </li>
<li>Fix NVIDIA crash with GPU accel (movit). <a href='http://commits.kde.org/kdenlive/754b8eeed8c1a874a5535eb5136b02841a812180'>Commit.</a> </li>
<li>Revert "Fix warning about QOffscreenSurface thread". <a href='http://commits.kde.org/kdenlive/8a20fca86e9e2ce3d04b3e024b0752e19f9d8c8e'>Commit.</a> See bug <a href='https://bugs.kde.org/375094'>#375094</a></li>
<li>Fix slideshow clips displayed as invalid when re-opening project. <a href='http://commits.kde.org/kdenlive/0ce898669ab4efe6d2a6bf6ac2a1edf57c5e3c1f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374916'>#374916</a></li>
<li>Update copyright, update authors and mention FFmpeg in about data. <a href='http://commits.kde.org/kdenlive/00e8696417215ca4e9a31bba39377b27b7bbc97e'>Commit.</a> </li>
<li>Fixes cppcheck error:. <a href='http://commits.kde.org/kdenlive/65681183e290b69c7b427b3e73f167f223a35c6a'>Commit.</a> </li>
<li>Fixes cppcheck warning:. <a href='http://commits.kde.org/kdenlive/df419e2f776c4d159f08f5d659f7bdbaf3e38880'>Commit.</a> </li>
<li>Fixes cppcheck warning:. <a href='http://commits.kde.org/kdenlive/91bd4db97d4f84973c885542a58c563f8b75e407'>Commit.</a> </li>
<li>Fixes cppcheck warning:. <a href='http://commits.kde.org/kdenlive/20a6397a7219b03d9a4c25e56a8eb003d17e123f'>Commit.</a> </li>
<li>Fixes cppcheck error:. <a href='http://commits.kde.org/kdenlive/0eb1d1ef8f5fec49016778881f7fd3de5860867d'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Fix itemmodifytest by changing || to &&. <a href='http://commits.kde.org/kdepim-runtime/f959834caa03bfcb741ba9eca4684d536e3a722d'>Commit.</a> </li>
<li>FileStore::ItemFetchJob: fix confusion between requested items and result items. <a href='http://commits.kde.org/kdepim-runtime/8d391ee469aa0fb0205e57cbd1d6c959496c8b09'>Commit.</a> </li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Fix pascha computation. <a href='http://commits.kde.org/kholidays/637862700fe0d8bd2a8b6e351d5a7fc4a628b969'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129667'>#129667</a></li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Honor the BUILD_TESTING cmake variable. <a href='http://commits.kde.org/kig/cfd3982bcc3fb1edbaa4660e715a635c57d1be89'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129853'>#129853</a></li>
<li>Cleanup kig docbook. <a href='http://commits.kde.org/kig/523e6a3b3a3261c7b7c883335b8cf11dd3ab36a9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129574'>#129574</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Fixed unrar version check. <a href='http://commits.kde.org/kio-extras/c734c5d5a1b47f749ef9230454f963b3b7daf573'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367626'>#367626</a>. Fixes bug <a href='https://bugs.kde.org/341305'>#341305</a>. Code review <a href='https://git.reviewboard.kde.org/r/129255'>#129255</a></li>
</ul>
<h3><a name='kjumpingcube' href='https://cgit.kde.org/kjumpingcube.git'>kjumpingcube</a> <a href='#kjumpingcube' onclick='toggle("ulkjumpingcube", this)'>[Show]</a></h3>
<ul id='ulkjumpingcube' style='display: none'>
<li>Remove unused text from KColorButton widgets. <a href='http://commits.kde.org/kjumpingcube/87e5a58337d0d2fec93b5a883560a0454a7dd7b4'>Commit.</a> </li>
</ul>
<h3><a name='klines' href='https://cgit.kde.org/klines.git'>klines</a> <a href='#klines' onclick='toggle("ulklines", this)'>[Show]</a></h3>
<ul id='ulklines' style='display: none'>
<li>Fix "Show Next" not being visible. <a href='http://commits.kde.org/klines/ad7f844cfcf1c2c54cb0d7edf702ea625a3d127e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374914'>#374914</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'>
<li>Fix Bug 373256 - "Delete duplicate messages" feature does not work. <a href='http://commits.kde.org/kmail/0cbfe7105fb6e8d3ace091185bd10b3d7ae7c47e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373256'>#373256</a></li>
</ul>
<h3><a name='kmail-account-wizard' href='https://cgit.kde.org/kmail-account-wizard.git'>kmail-account-wizard</a> <a href='#kmail-account-wizard' onclick='toggle("ulkmail-account-wizard", this)'>[Show]</a></h3>
<ul id='ulkmail-account-wizard' style='display: none'>
<li>Fix compilation with gcc 4.8. <a href='http://commits.kde.org/kmail-account-wizard/8c3fafd89e9a0efa359e6201608dc721162c609d'>Commit.</a> </li>
<li>Fix icon. <a href='http://commits.kde.org/kmail-account-wizard/25bb2cb910fb8819c126bb82bb6782b237775451'>Commit.</a> </li>
</ul>
<h3><a name='kmines' href='https://cgit.kde.org/kmines.git'>kmines</a> <a href='#kmines' onclick='toggle("ulkmines", this)'>[Show]</a></h3>
<ul id='ulkmines' style='display: none'>
<li>Set a comment. <a href='http://commits.kde.org/kmines/34b7d1c49a33085f9d38ea8efc319b438470a8da'>Commit.</a> </li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Show]</a></h3>
<ul id='ulkonqueror' style='display: none'>
<li>Update the list of KCModules. <a href='http://commits.kde.org/konqueror/7de40923bcf23bf7137722b3e45041deec12716e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129806'>#129806</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>[TerminalDisplay] Abort painting invalid rect. <a href='http://commits.kde.org/konsole/d8b242f3c702bbfd9c8e1cf2d735c439542b689d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129914'>#129914</a></li>
<li>Fix build. <a href='http://commits.kde.org/konsole/3a055ea19d5f458ccf06a33c697fbcda7a7f14df'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 16.12.2. <a href='http://commits.kde.org/kopete/afd61266f67277820f0281b48d945c8f106e72cb'>Commit.</a> </li>
<li>Fix jabber support for XEP-0022 and XEP-0184 for Message Delivery. <a href='http://commits.kde.org/kopete/3abc158b1f0c46b3a48950ddf9d97b7548011cba'>Commit.</a> </li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Show]</a></h3>
<ul id='ulkrfb' style='display: none'>
<li>Set a comment. <a href='http://commits.kde.org/krfb/da4de72d5561d86052e4b0f05b85b63b3e94e807'>Commit.</a> </li>
<li>Silence CMake policy CMP0063 warning. <a href='http://commits.kde.org/krfb/b4713d4755d693083dfff7e6e41dc4fe40cb03a3'>Commit.</a> </li>
</ul>
<h3><a name='ksirk' href='https://cgit.kde.org/ksirk.git'>ksirk</a> <a href='#ksirk' onclick='toggle("ulksirk", this)'>[Show]</a></h3>
<ul id='ulksirk' style='display: none'>
<li>Fix FTBFS with GCC6 on arm64/armhf/ppc64el. <a href='http://commits.kde.org/ksirk/8fdc83b20270c5c307c5edda7c21a351bdf44c97'>Commit.</a> </li>
</ul>
<h3><a name='ksquares' href='https://cgit.kde.org/ksquares.git'>ksquares</a> <a href='#ksquares' onclick='toggle("ulksquares", this)'>[Show]</a></h3>
<ul id='ulksquares' style='display: none'>
<li>Use the genericname as comment. <a href='http://commits.kde.org/ksquares/399c99f82e995bba0681d58ae0ea7fb026621b94'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129888'>#129888</a></li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Fix compilation with strict iterators. <a href='http://commits.kde.org/kstars/5ab5939fff29f4f59caf8874dd0649b72885e267'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Show]</a></h3>
<ul id='ulktouch' style='display: none'>
<li>Fix Typos In Course 'de1.xml'. <a href='http://commits.kde.org/ktouch/42c88ce3822ad1b77bcdd59c8740c7fdb5c59c1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375158'>#375158</a></li>
<li>Fix Crash When Closing Resource Editor. <a href='http://commits.kde.org/ktouch/4e7c7a17237d33753324a21abc34ad9088e35e3a'>Commit.</a> </li>
<li>Fix Keyboard Focus Loss Issue For Trainer. <a href='http://commits.kde.org/ktouch/d4eac6a475681cf50d96e7d3480681f475c416b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374701'>#374701</a></li>
</ul>
<h3><a name='libkcompactdisc' href='https://cgit.kde.org/libkcompactdisc.git'>libkcompactdisc</a> <a href='#libkcompactdisc' onclick='toggle("ullibkcompactdisc", this)'>[Show]</a></h3>
<ul id='ullibkcompactdisc' style='display: none'>
<li>Don't throw away global CMAKE_C_FLAGS. <a href='http://commits.kde.org/libkcompactdisc/973921ac4e31ead4fe04a9de412c20079fe8c174'>Commit.</a> </li>
</ul>
<h3><a name='libkdcraw' href='https://cgit.kde.org/libkdcraw.git'>libkdcraw</a> <a href='#libkdcraw' onclick='toggle("ullibkdcraw", this)'>[Show]</a></h3>
<ul id='ullibkdcraw' style='display: none'>
<li>Fix build. <a href='http://commits.kde.org/libkdcraw/0c7d309289c78631be4b595119d4783fe21cf65b'>Commit.</a> </li>
</ul>
<h3><a name='libkexiv2' href='https://cgit.kde.org/libkexiv2.git'>libkexiv2</a> <a href='#libkexiv2' onclick='toggle("ullibkexiv2", this)'>[Show]</a></h3>
<ul id='ullibkexiv2' style='display: none'>
<li>Fix compilation with clang. <a href='http://commits.kde.org/libkexiv2/2bcdea1a25b7ee69b4db93a12b8197a5bfa0bc1a'>Commit.</a> </li>
</ul>
<h3><a name='libkface' href='https://cgit.kde.org/libkface.git'>libkface</a> <a href='#libkface' onclick='toggle("ullibkface", this)'>[Show]</a></h3>
<ul id='ullibkface' style='display: none'>
<li>Fixes for compilation under -fno-operator-names. <a href='http://commits.kde.org/libkface/0684ee86b6510423bdd79325cd769e7a7c3d0e7c'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Fix warning "QObject::disconnect: Unexpected null parameter". <a href='http://commits.kde.org/marble/b6752627bcf4bd56f96b0225e4b1355057588271'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Fix error reported by David on previous commit. <a href='http://commits.kde.org/messagelib/9210db991fefbeb519d9279dbb92fe3247a1009f'>Commit.</a> </li>
<li>Implement dnd support. <a href='http://commits.kde.org/messagelib/fad0bae8322ce0ec37f5a6707fc2334a42610ce8'>Commit.</a> </li>
<li>Fix save embedded image. <a href='http://commits.kde.org/messagelib/7cee643ebe7334aac53784ad2aca7149b7fae807'>Commit.</a> </li>
<li>Fix copy url when image is embedded. <a href='http://commits.kde.org/messagelib/bf7f6d18531cf15a09419c83bfbb3db59325c807'>Commit.</a> </li>
<li>Allow to select current day. <a href='http://commits.kde.org/messagelib/73d3c37285cb9a38baa68275e09a46d4618bb49f'>Commit.</a> </li>
<li>Make rendertest runsuccessfully again. <a href='http://commits.kde.org/messagelib/f5a3eb4d700a8e7eceb5fc86f6122790b9a8cd40'>Commit.</a> </li>
<li>Do not trigger quotedHTML. <a href='http://commits.kde.org/messagelib/1cd9f2ef26764744a27380958c798bf62aba82e3'>Commit.</a> </li>
<li>Apply patch from Gerd Fleischer. <a href='http://commits.kde.org/messagelib/6a22838e15c9ab4aff659d615241d9e68988dbba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362803'>#362803</a>. Code review <a href='https://git.reviewboard.kde.org/r/129836'>#129836</a></li>
<li>Apply patch from  Matt Whitlock + add autotest to validate it. <a href='http://commits.kde.org/messagelib/48ecc0d629cb55e2c45d4facd62b9012db385cff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/308444'>#308444</a>. Fixes bug <a href='https://bugs.kde.org/355195'>#355195</a>. Fixes bug <a href='https://bugs.kde.org/366768'>#366768</a></li>
<li>Try to test in other timezone. <a href='http://commits.kde.org/messagelib/9a99bb286371fd74a8644fa24b9e799ce6392ee2'>Commit.</a> </li>
<li>Add autotest I need to implement support for timezone. <a href='http://commits.kde.org/messagelib/29f7db279ea71deaebfc713a7dfee710c21c3343'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Fix rendering artifacts in Okular with "draw border around links" turned on. <a href='http://commits.kde.org/okular/af6e9a9b0b7998aeb5bf2a902916f4bba9cc8d71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375719'>#375719</a></li>
<li>Fix Enter turning the thumbnail filter bar into a regular search bar. <a href='http://commits.kde.org/okular/96d8953878e00543194913a674497c1e656c28de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375755'>#375755</a></li>
<li>Fix porting bug regarding the font extraction progress. <a href='http://commits.kde.org/okular/be6b0b6f482dbb307b0bbdc5f42b7ee2fb55acb6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375782'>#375782</a></li>
<li>Remove #ifdefs for Qt we require in cmake anyway. <a href='http://commits.kde.org/okular/f46780750638d3743deb090409b0298898e194c1'>Commit.</a> </li>
<li>Readd test for file existance when opening files with # on the path. <a href='http://commits.kde.org/okular/b13fc1d3be97a8e02884acad6495efa8f13975cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373855'>#373855</a></li>
<li>Invoke the correct KCModule. <a href='http://commits.kde.org/okular/f968296c51889499bc0d42217874ea7c4d191327'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129805'>#129805</a></li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Show]</a></h3>
<ul id='ulpim-data-exporter' style='display: none'>
<li>Fix i18n. <a href='http://commits.kde.org/pim-data-exporter/e1b0e987bef2e9da526ff5e10843714ed43a4f3b'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Show]</a></h3>
<ul id='ulspectacle' style='display: none'>
<li>Set screenshot URL for notification also in stable branch. <a href='http://commits.kde.org/spectacle/6ea1988af9e54f2efa25719a71984ede3c857ca7'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Show]</a></h3>
<ul id='ulstep' style='display: none'>
<li>Some QT_TR_NOOP -> QT_TRANSLATE_NOOP. <a href='http://commits.kde.org/step/ed80be1550c24cb0f9793b9d3971e107cb2fcefe'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'SQL Import probably crashes when enums are used before defined'. <a href='http://commits.kde.org/umbrello/1ffd42f4bf18b254e2b0a39dd859a42e05bfcf62'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375263'>#375263</a></li>
<li>Fix 'IDL import probably crashes when enums are used before defined'. <a href='http://commits.kde.org/umbrello/66631c814e73727b99d87f9cd2a976fd0074814c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375262'>#375262</a></li>
<li>Fix 'Pascal import probably crashes if enums are used before defined'. <a href='http://commits.kde.org/umbrello/086ed55d63964379680f02f59b3660a48beb7f97'>Commit.</a> </li>
<li>Fix 'Ada import probably crashes if enums are used before defined'. <a href='http://commits.kde.org/umbrello/e0ab3633816c6b0aed6c11a718ba5db4b35eb9b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375258'>#375258</a></li>
<li>Revert "Add support for language specific association rules.". <a href='http://commits.kde.org/umbrello/c34a65398d1444979f0448fbe9e395262ec3c938'>Commit.</a> </li>
<li>Revert "Enable/Disable associations in toolbar according to build in association rules.". <a href='http://commits.kde.org/umbrello/a868814854473ad5cf9ca615205b40e47732ea25'>Commit.</a> </li>
<li>Fix 'C# importer crashes on enums when they're used before being defined'. <a href='http://commits.kde.org/umbrello/6c44d5a904c6f2ce4fd20b66f20b5ab14ab6924e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375224'>#375224</a></li>
<li>Enable/Disable associations in toolbar according to build in association rules. <a href='http://commits.kde.org/umbrello/aaa74236fb05a59e80592f5afae7fcfe88fea278'>Commit.</a> </li>
<li>Add support for language specific association rules. <a href='http://commits.kde.org/umbrello/6826e84a63c4a45cbf2612c4fd434c31746e59db'>Commit.</a> See bug <a href='https://bugs.kde.org/131591'>#131591</a></li>
<li>Use gcc buildin null pointer check to remove x86 limitation of previous implementation. <a href='http://commits.kde.org/umbrello/ce147ad93100a0d296f1c5958fcb7bf70c9d70af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374530'>#374530</a></li>
<li>Fix `Can't import java classes that have the same name as it's package`. <a href='http://commits.kde.org/umbrello/d6a53823f69f958e0d5e560ee510b8ee4ab0a8d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359487'>#359487</a></li>
<li>Fix 'Diamond placed at wrong end on creating multiple mutual compositions/aggregations'. <a href='http://commits.kde.org/umbrello/a14a450b7495d179c1bb47ea07406d3b5a829df2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/146071'>#146071</a></li>
</ul>
