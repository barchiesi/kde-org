<?php
  $page_title = "KDE 4.2.4 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Community Improves Desktop with KDE 4.2.4
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fourth Translation and Service Release of the 4.2
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
June 3rd, 2009. Another month has passed, and the <a href="http://www.kde.org/">KDE
Community</a> announces the immediate availability of KDE 4.2.4, another
bugfix and maintenance update for the latest generation of the most advanced and powerful
free desktop. This is a monthly update to <a href="4.2/">KDE 4.2</a>. It
ships with a desktop workspace and many cross-platform applications such as administration
programs, network tools, educational applications, utilities, multimedia software, games, artwork,
development tools and more. KDE's award-winning tools and applications are
available in more than 50 languages.<br />
KDE 4.2.4 is the last release planned for the KDE 4.2 series, which will be followed up
by KDE 4.3 at the end of July, 6 months after KDE 4.2 was released. In the case that
a security issue or another grave bug arises, there might be a KDE 4.2.5, of course.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="4.2/guide.php">
    <img src="4.2/screenshots/desktop_thumb.png" align="center"  height="337"  />
</a>
    <br />
    <em>The KDE 4.2 Desktop</em>
</div>




<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.2.4/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
As a service release, the
<a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php">changelog</a>
contains a list of bugfixes and improvements</a>.

Notable improvements include, but are not limited to:
<ul>
    <li><strong>Okular</strong> (KDE's document viewer) now correctly supports selecting the pages to print using the PostScript backend.
    <li><strong>KMail</strong> has received a whole slew of bug fixes
    <li>So has KHTML and therefore the <strong>Konqueror</strong> web browser

</ul>


Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE 4.2.4, you can browse the Subversion log.
KDE 4.2.4 also ships a more complete set of translations.
<p />
To find out more about the KDE 4.2.x desktop and applications, please refer to the
<a href="http://www.kde.org/announcements/4.2/">KDE 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.2.4 is a recommended update for everyone running KDE 4.2.2 or earlier.
<p />

<!-- // Boilerplate again -->

<h4>
  Installing KDE 4.2.4 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.2.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.4/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.2.4.php">KDE 4.2.4 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.2.4
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for KDE 4.2.4 may be <a
href="http://download.kde.org/stable/4.2.4/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.2.4
  are available from the <a href="/info/4.2.4.php#binary">KDE 4.2.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
