<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.16.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.16.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Re-read color palettes when application color changes. <a href='https://commits.kde.org/breeze/9d6c7c7f3439941a6870d6537645297683501bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408416'>#408416</a>. See bug <a href='https://bugs.kde.org/382505'>#382505</a>. See bug <a href='https://bugs.kde.org/355295'>#355295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21646'>D21646</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Flatpak: Indicate that updates are being fetched. <a href='https://commits.kde.org/discover/67b313bdd6472e79e3d500f8b32d0451c236ce84'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408608'>#408608</a></li>
<li>--verbose. <a href='https://commits.kde.org/discover/2c8eaf6b30b2d5cf0870cd61dfc6e78df039ef50'>Commit.</a> </li>
<li>Kns (mostly): Don't show weird padding if the thumbnails are too small. <a href='https://commits.kde.org/discover/cf8606eb89f70011edc170a5a2ec5a699933cfcf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408109'>#408109</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Move various properties away from qapp and to kaboutdata. <a href='https://commits.kde.org/drkonqi/b428aa049ee62481790e4da4961ea59933d8e601'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21802'>D21802</a></li>
<li>Actually set kaboutdata as application aboutdata. <a href='https://commits.kde.org/drkonqi/bd205d4d2b75164794de834eab38ab7b71377b66'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383842'>#383842</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21799'>D21799</a></li>
<li>The Developer Information tab now correctly resizes to fit the content. <a href='https://commits.kde.org/drkonqi/bf5d10ab09461516cd5230c6dc12ec67e074c15d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406748'>#406748</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21642'>D21642</a></li>
<li>Saving backtraces now overwrites files when necessary. <a href='https://commits.kde.org/drkonqi/0d35055d867acd8c2a7db4d13cf9bd13d18fe558'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405597'>#405597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21645'>D21645</a></li>
<li>Error message when failing to save backtraces are more helpful. <a href='https://commits.kde.org/drkonqi/c8f8b9d962eb7c0b638fbe6ded725a46cb5bbf84'>Commit.</a> See bug <a href='https://bugs.kde.org/405597'>#405597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21644'>D21644</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Update default Samba log path. <a href='https://commits.kde.org/kinfocenter/6243a402c926ad92f641a1c03f68037f48024f18'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21552'>D21552</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Really use the translations. Patch by Victor Ryzhykh <victorr2007@yandex.ru>. <a href='https://commits.kde.org/kwin/b24aa90af7fb5f8c71035dc368734325377715fb'>Commit.</a> </li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Enforce use of the reset timer for clearing the model. <a href='https://commits.kde.org/milou/9d5921fb21c7aa4d1cd81255bbebf3fd4c7161af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21670'>D21670</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[desktoppackage] Re-add spacing between inline message and first UI element. <a href='https://commits.kde.org/plasma-desktop/c11527c04b1ae96389667e7cc4f404efb955c6c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408712'>#408712</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21810'>D21810</a></li>
<li>Doc: Write docbook date in standard format. <a href='https://commits.kde.org/plasma-desktop/5d0ba9313167276d76afd88e88bb7daa1fa9260a'>Commit.</a> </li>
<li>Replace the excludeRange mode setting when already available. <a href='https://commits.kde.org/plasma-desktop/08ec9a036bf5b3ee102938ad332f624adc1060b9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/244857'>#244857</a>. Fixes bug <a href='https://bugs.kde.org/408415'>#408415</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21748'>D21748</a></li>
<li>[minimzeall] Change panel icon size to smallMedium. <a href='https://commits.kde.org/plasma-desktop/fff556af1939d17453540f4388b27650359258dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21685'>D21685</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Handle Esc properly when focus is in searchbox. <a href='https://commits.kde.org/plasma-nm/5b45ac0edc140518769eda0b7a7ff59b936a5027'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408290'>#408290</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21620'>D21620</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Powerdevil runner] Make Sleep/Suspend command work again. <a href='https://commits.kde.org/plasma-workspace/6a5a38f1281f630edc8fda18523fe9dceef22377'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408735'>#408735</a></li>
<li>PanelView: align setting of masks with how it's done for dialogs/tooltips. <a href='https://commits.kde.org/plasma-workspace/f65a0eee09dabc66d9d7acf6ddda6bcb03888794'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21803'>D21803</a></li>
<li>[sddm-theme] Style the session and keyboard layout selectors to be more Breeze. <a href='https://commits.kde.org/plasma-workspace/16d3eb60a9849151aa92c2a100872ab12bb3855d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408702'>#408702</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21794'>D21794</a></li>
<li>Improved notification identification for Flatpak applications. <a href='https://commits.kde.org/plasma-workspace/9258ef586324a74d5333faa1429f6a2a80cafadc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21779'>D21779</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Ignore power management inhibition in battery critical timeout. <a href='https://commits.kde.org/powerdevil/82db1f446966a4a66324befbc60e6a0c0b7735f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408398'>#408398</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21648'>D21648</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
