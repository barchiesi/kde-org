<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.61.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.61.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
August 10, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.61.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Link against KIOCore instead of KIOWidgets in kioslaves");?></li>
<li><?php i18n("[IndexCleaner] ignore non-existent entries inside config");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Fix crash due to the q pointer never being initialized");?></li>
<li><?php i18n("Don't include bluezqt_dbustypes.h from installed headers");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add \"user-others\" icon (bug 407782)");?></li>
<li><?php i18n("Make \"edit-none\" a symlink to \"dialog-cancel\"");?></li>
<li><?php i18n("Delete redundant and monochrome versions of applications-internet");?></li>
<li><?php i18n("Add view-pages-* icons, as needed in Okular for page layout selection (bug 409082)");?></li>
<li><?php i18n("Use clockwise arrows for *refresh* and update-* icons (bug 409914)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("android: Allow overriding ANDROID_ARCH and ANDROID_ARCH_ABI as envvars");?></li>
<li><?php i18n("Notify users when not using KDE_INSTALL_USE_QT_SYS_PATHS about prefix.sh");?></li>
<li><?php i18n("Provide a more sensible CMAKE_INSTALL_PREFIX default");?></li>
<li><?php i18n("Make the default build type \"Debug\" when compiling a git checkout");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Add Date term to KActivities Stats to filter on resource event date");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Simplify previous-/nextActivity code in kactivities-cli");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Fix checking dirs for metainfo.yaml with non-ascii chars with Python 2.7");?></li>
<li><?php i18n("Log bad pathnames (via repr()) instead of crashing entirely");?></li>
<li><?php i18n("generate list of data files on the fly");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("KTar::openArchive: Don't assert if file has two root dirs");?></li>
<li><?php i18n("KZip::openArchive: Don't assert when opening broken files");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("adapt to UI changes in KPageView");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Security: remove support for $(...) in config keys with [$e] marker");?></li>
<li><?php i18n("Include definition for class used in header");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add KFileUtils::suggestName function to suggest a unique filename");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Scrollview - Don't fill the parent with the view (bug 407643)");?></li>
<li><?php i18n("introduce FallbackTapHandler");?></li>
<li><?php i18n("KRun QML proxy: fix path/URL confusion");?></li>
<li><?php i18n("Calendar events: allow plugins to show event details");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("kded5 desktop file: use valid type (Service) to suppress warning from kservice");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Designer plugin: use consistently \"KF5\" in group names &amp; texts");?></li>
<li><?php i18n("Don't advertise using KPassivePopup");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("expose new KBusyIndicatorWidget");?></li>
<li><?php i18n("Remove designer plugin generation for KF5WebKit");?></li>
</ul>

<h3><?php i18n("KDE WebKit");?></h3>

<ul>
<li><?php i18n("Use preview of ECMAddQtDesignerPlugin instead of KF5DesignerPlugin");?></li>
<li><?php i18n("Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Get mobipocket extractor up-to-date, but keep disabled");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Add public holidays' substitute days in Russia, for 2019-2020");?></li>
<li><?php i18n("Update holidays in Russia");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Restore \"Check if group &lt; LastGroup, as KIconEffect doesn't handle UserGroup anyway\"");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Deprecate suggestName in favour of the one in KCoreAddons");?></li>
<li><?php i18n("Fix can't enter directory error on some FTP servers with Turkish locale (bug 409740)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Revamp Kirigami.AboutPage");?></li>
<li><?php i18n("Consistently use Units.toolTipDelay instead of hardcoded values");?></li>
<li><?php i18n("properly size the card contents when the card size is constrained");?></li>
<li><?php i18n("hide ripple when we don't want items clickable");?></li>
<li><?php i18n("make handle follow arbitrary height of the drawer");?></li>
<li><?php i18n("[SwipeListItem] Take into account scrollbar visibility and form factor for handle and inline actions");?></li>
<li><?php i18n("Remove scaling of iconsize unit for isMobile");?></li>
<li><?php i18n("always show back button on layers&gt;1");?></li>
<li><?php i18n("hide actions with submenus from more menu");?></li>
<li><?php i18n("default ActionToolBar position to Header");?></li>
<li><?php i18n("big z to not appear under dialogs");?></li>
<li><?php i18n("use opacity to hide buttons that don't fit");?></li>
<li><?php i18n("add the spacer only when it fills the width");?></li>
<li><?php i18n("fully retrocompatible with showNavigationButtons as bool");?></li>
<li><?php i18n("more granularity to globalToolBar.showNavigationButtons");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("David Faure is now the maintainer for KItemModels");?></li>
<li><?php i18n("KConcatenateRowsProxyModel: add note that Qt 5.13 provides QConcatenateTablesProxyModel");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Offer metadata.json when requesting the package metadata");?></li>
<li><?php i18n("PackageLoader: Use the right scope for the KCompressionDevice");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("declarative: refresh actions list when person changes");?></li>
<li><?php i18n("declarative: don't crash when the API is misused");?></li>
<li><?php i18n("personsmodel: Add phoneNumber");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Expose X-KDE-Wayland-Interfaces");?></li>
<li><?php i18n("Fix KService build on Android");?></li>
<li><?php i18n("KService: remove broken concept of global sycoca database");?></li>
<li><?php i18n("Remove very dangerous deletion code with kbuildsycoca5 --global");?></li>
<li><?php i18n("Fix infinite recursion and asserts when the sycoca DB is unreadable by user (e.g. root owned)");?></li>
<li><?php i18n("Deprecate KDBusServiceStarter. All usage in kdepim is now gone, DBus activation is a better solution");?></li>
<li><?php i18n("Allow KAutostart to be constructed using an absolute path");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Save and load page margins");?></li>
<li><?php i18n("Don't persist authentication");?></li>
<li><?php i18n("Re-map default \"Switch input mode\" shortcut to not conflict with konsolepart (bug 409978)");?></li>
<li><?php i18n("Make keyword completion model return HideListIfAutomaticInvocation by default");?></li>
<li><?php i18n("Minimap: Do not grab the left-mouse-button-click on up/down buttons");?></li>
<li><?php i18n("allow up to 1024 hl ranges instead of not highlighting the line at all if that limit is reached");?></li>
<li><?php i18n("fix folding of lines with end position at column 0 of a line (bug 405197)");?></li>
<li><?php i18n("Add option to treat some chars also as \"auto bracket\" only when we have a selection");?></li>
<li><?php i18n("Add an action to insert a non-indented newline (bug 314395)");?></li>
<li><?php i18n("Add setting to enable/disable text drag-and-drop (on by default)");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Add Binary Data units (bits, kilobytes, kibibytes ... yottabytes)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Move kwalletd initialization earlier (bug 410020)");?></li>
<li><?php i18n("Remove kde4 migration agent completely (bug 400462)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Use wayland-protocols");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("introduce concept of header and footer for kpageview");?></li>
<li><?php i18n("[Busy Indicator] Match duration of QQC2-desktop-style version");?></li>
<li><?php i18n("Add a warning dialog with a collapsible details section");?></li>
<li><?php i18n("new class KBusyIndicatorWidget similar to QtQuick's BusyIndicator");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[platforms/xcb] Use XRES extension to get real window PID (bug 384837)");?></li>
<li><?php i18n("Port KXMessages away from QWidget");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Add expanding spacers as a customization option for toolbars");?></li>
<li><?php i18n("Use monochrome action icons for KAboutData buttons");?></li>
<li><?php i18n("Remove visibilityChanged connection in favor of existing eventFilter");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Allow updating default DBus timeout on every interface");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("device: include reapplyConnection() in the interface");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[ToolButtonStyle] Use same color group for hovered state");?></li>
<li><?php i18n("Handle colors file in fake plasma theme installer");?></li>
<li><?php i18n("Install plasma theme into local XDG_DATA_DIR for icon test");?></li>
<li><?php i18n("Apply busy indicator duration change of D22646 to the QQC2 style");?></li>
<li><?php i18n("Compile package structure plugins into expected subdirectory");?></li>
<li><?php i18n("Change Highlight to ButtonFocus");?></li>
<li><?php i18n("Fix running the dialognativetest without installing");?></li>
<li><?php i18n("Search for the plugin of the other plasmoid");?></li>
<li><?php i18n("[Busy Indicator] Match duration of QQC2-desktop-style version");?></li>
<li><?php i18n("Add missing components in org.kde.plasma.components 3.0");?></li>
<li><?php i18n("Update refresh and restart icons to reflect new breeze-icons versions (bug 409914)");?></li>
<li><?php i18n("itemMouse isn't defined in plasma.components 3.0");?></li>
<li><?php i18n("use clearItems when an applet gets deleted");?></li>
<li><?php i18n("Fix crash if switchSize is adjusted during initial setup");?></li>
<li><?php i18n("Improve plugin caching");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Phabricator: open a new diff automatically in the browser");?></li>
<li><?php i18n("Fix extraction. Patch by Victor Ryzhykh");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix broken guard that prevents styling sliders with negative values");?></li>
<li><?php i18n("Slow down the busy indicator's rotation speed");?></li>
<li><?php i18n("Fix \"Type error\" when creating a TextField with focus: true");?></li>
<li><?php i18n("[ComboBox] Set close policy to close on click outside instead of only outside parent (bug 408950)");?></li>
<li><?php i18n("[SpinBox] Set renderType (bug 409888)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Make sure solid backends are reentrant");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("TypeScript: fix keywords in conditional expressions");?></li>
<li><?php i18n("Fix generator and test paths of CMake");?></li>
<li><?php i18n("Add support for additional QML keywords not part of JavaScript");?></li>
<li><?php i18n("Update cmake highlighting");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.61");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
