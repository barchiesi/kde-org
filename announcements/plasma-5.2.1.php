<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Plasma 5.2.1, Bugfix Release for February");
  $site_root = "../";
  $release = 'plasma-5.2.1';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<div style="text-align: center; float: right">
<a href="plasma-5.2/full-screen.png">
<img src="plasma-5.2/full-screen-wee2.png" style="padding: 1ex; border: 0; background-image: none; " width="400" height="226" alt="<?php i18n("Plasma 5.2");?>" />
</a>
</div>

<p>
<?php i18n("Tuesday, 24 February 2015.
Today KDE releases a bugfix update to Plasma 5, versioned 5.2.1.  <a
href='https://www.kde.org/announcements/plasma-5.2.0.php'>Plasma 5.2</a>
was released in January with many feature refinements and new modules to complete the desktop experience.
");?>
</p>

<p>
<?php i18n("
This release adds a month's worth of new
translations and fixes from KDE's contributors.  The bugfixes are
typically small but important and include:
");?>
</p>

<ul>
<li><?php i18n("Don't turn off the screen or suspend the computer when watching videos in a web browser");?></li>
<li><?php i18n("Fix Powerdevil from using full CPU");?></li>
<li><?php i18n("Show the correct prompt for a fingerprint reader swipe");?></li>
<li><?php i18n("Show correct connection name in Plasma Network Manager");?></li>
<li><?php i18n("Remove kdelibs4support code in many modules");?></li>
<li><?php i18n("Fix crash when switching to/from Breeze widget style");?></li>
<li><?php i18n("In KScreen fix crash when multiple EDID requests for the same output are enqueued");?></li>
<li><?php i18n("In KScreen fix visual representation of output rotation");?></li>
<li><?php i18n("In Oxygen style improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight.");?></li>
<li><?php i18n("In Plasma Desktop improve rubber band feel and consistency with Dolphin.");?></li>
<li><?php i18n("In Plasma Desktop use smooth transformation for scaling down the user picture");?></li>
<li><?php i18n("When setting color scheme information for KDElibs 4, don't read from KF5 kdeglobals");?></li>
<li><?php i18n("Baloo KCM: Show proper icons (porting bug)");?></li>
</ul>

<a href="plasma-5.2.0-5.2.1-changelog.php"><?php i18n("Full Plasma 5.2.1 changelog");?></a>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images with Plasma 5.2 are available from <a
href='http://cdimage.ubuntu.com/kubuntu/daily-live/current/'>Kubuntu development daily builds</a>.
");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
