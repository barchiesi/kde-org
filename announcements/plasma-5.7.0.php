<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Plasma 5.7 Release");
  $site_root = "../";
  $release = 'plasma-5.7.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/A9MtFqkRFwQ?rel=0" frameborder="0" allowfullscreen></iframe>
</figure>
<br clear="all" />

<figure style="float: none">
<a href="plasma-5.7/plasma-5.7.png">
<img src="plasma-5.7/plasma-5.7-wee.png" style="border: 0px" width="600" height="375" alt="<?php i18n("KDE Plasma 5.7");?>" />
</a>
<figcaption><?php i18n("KDE Plasma 5.7");?></figcaption>
</figure>


<p>
<?php i18n("Tuesday, 5 July 2016. "); ?>
<?php i18n("Today KDE releases an update to its desktop software, Plasma 5.7.
");?>
</p>


<br clear="all" />
<h3>Improved Workflows</h3>

<figure style="float: right">
<a href="plasma-5.7/krunner-jump-actions-wee.png">
<img src="plasma-5.7/krunner-jump-actions.png" style="border: 0px" width="350" height="336" alt="<?php i18n("Jump List Actions in KRunner");?>" />
</a>
<figcaption><?php i18n("Jump List Actions in KRunner");?></figcaption>
</figure>

<p>In our previous release we added Jump List Actions for quicker access to certain tasks within an application. This feature has been extended and those actions are also found through KRunner now.</p>

<br clear="all" />
<figure style="float: right">
<a href="plasma-5.7/pim-events-wee.png">
<img src="plasma-5.7/pim-events-wee.png" style="border: 0px" width="350" height="207" alt="<?php i18n("Agenda Items in Calendar");?>" />
</a>
<figcaption><?php i18n("Agenda Items in Calendar");?></figcaption>
</figure>

<p>Plasma 5.7 marks the return of the agenda view in the calendar, which provides a quick and easily accessible overview of upcoming appointments and holidays.</p>

<br clear="all" />
<figure style="float: right">
<a href="plasma-5.7/plasma-pa-drag-wee.png">
<img src="plasma-5.7/plasma-pa-drag.png" style="border: 0px" width="350" height="330" alt="<?php i18n("Dragging Application to Audio Device");?>" />
</a>
<figcaption><?php i18n("Dragging Application to Audio Device");?></figcaption>
</figure>
<p>Many improvements have been added to the Volume Control applet: it gained the ability to control volume on a per-application basis and allows you to move application output between devices using drag and drop. Also implemented is the ability to raise the volume above 100%.</p>

<br clear="all" />
<figure style="float: right">
<a href="plasma-5.7/icons-invert.png">
<img src="plasma-5.7/icons-invert.png" style="border: 0px" width="167" height="102" alt="<?php i18n("Icons Tint to Match Highlight");?>" />
</a>
<figcaption><?php i18n("Icons Tint to Match Highlight");?></figcaption>
</figure>
<p>For improved accessibility, Breeze icons within applications are now tinted depending on the color scheme, similarly to how it's done within Plasma. This resolves situations where our default dark icons might show up on dark surfaces. </p>

<br clear="all" />
<h3>Better Kiosk Support</h3>

<p>The <a href="https://userbase.kde.org/KDE_System_Administration/Kiosk/Introduction">Kiosk Framework</a> provides means of restricting the customazibility of the workspace, in order to keep users in an enterprise or public environment from performing unwanted actions or modifications. Plasma 5.7 brings many corrections about enforcing such restrictions. Notably, the Application Launcher will become read-only if widgets are locked through Kiosk policies, i.e. favorites are locked in place and applications can no longer be edited. Also, the Run Command restriction will prevent KRunner from even starting in the first place.</p>

<h3>New System Tray and Task Manager</h3>

<p>The System Tray has been rewritten from scratch to allow for a simpler and more maintainable codebase. While its user interface has only seen some minor fixes and polishing, many issues caused by the complex nature of the applet housing applets and application icons within have been resolved.</p>

<p>Similarly, the task bar has gained a completely revamped backend, replacing the old one that has already been around in the early days of our workspace. While the old backend got many features added over the period of time it was used, the new one has a remarkably better performance and could be engineered more cleanly and straight-forward as the requirements were known beforehand. All of this will ensure a greatly increased reliability and it also adds <a href="https://blog.martin-graesslin.com/blog/2016/06/a-task-manager-for-the-plasma-wayland-session/">support for Wayland</a> which was one of the most visible omissions in our Wayland tech previews.</p>

<br clear="all" />
<h3>Huge Steps Towards Wayland</h3>

<figure style="float: right">
<a href="https://c2.staticflickr.com/8/7418/26639077964_445c1761e3_b.jpg">
<img src="https://c2.staticflickr.com/8/7418/26639077964_445c1761e3_b.jpg" style="border: 0px" width="350" alt="<?php i18n("Betty the Fuzzpig Tests Plasma Wayland");?>" />
</a>
<figcaption><?php i18n("Betty the Fuzzpig Tests Plasma Wayland");?></figcaption>
</figure>

<p>This release brings Plasma closer to the new windowing system Wayland. Wayland is the successor of the decades-old X11 windowing system and brings many improvements, especially when it comes to tear-free and flicker-free rendering as well as security. The development of Plasma 5.7 for Wayland focused on quality in the Wayland compositor KWin. Over 5,000 lines of auto tests were added to KWin and another 5,000 lines were added to KWayland which is now released as part of KDE Frameworks 5.</p>

<p>The already implemented workflows got stabilized and are ensured to work correctly, with basic workflows now fully functional. More complex workflows are not yet fully implemented and might not provide the same experience as on X11. To aid debugging a new debug console got added, which can be launched through KRunner using the keyword “KWin” and integrates functionality known from the xprop, xwininfo, xev and xinput tools.</p>

<p>Other improvements include:</p>
<ul>
<li>When no hardware keyboard is connected, a <a href="https://blog.martin-graesslin.com/blog/2016/05/virtual-keyboard-support-in-kwinwayland-5-7/">virtual keyboard</a> is shown instead, bringing a smooth converged experience to tablets and convertibles</li>
<li>The sub-surface protocol is now supported which means that System Settings works correctly and no longer errorneously opens multiple windows.</li>
<li>Mouse settings, such as pointer acceleration, are honored and the touchpad can be enabled/disabled through a global shortcut. Touchpad configuration is still missing.</li>
</ul>

<div style="text-align: center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/_29BYcm8xKk" frameborder="0" allowfullscreen></iframe><br />
Virtual Keyboard Support in Plasma Wayland
</div>
<p><a href="plasma-5.6.5-5.7.0-changelog.php">
<?php i18n("Full Plasma 5.7.0 changelog");?></a></p>


<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
