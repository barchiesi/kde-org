<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.15.3 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.15.3";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Don't crash if the fwupd error is null. <a href='https://commits.kde.org/discover/1d8ab1561d0b328b641789fbafe581d7ddfdea58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19561'>D19561</a></li>
<li>Flatpak: Fix look-up of updates on flatpak. <a href='https://commits.kde.org/discover/2b7e28d372c20d51913b4e82a1493caee3e5d998'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404434'>#404434</a></li>
<li>Flatpak: fix warning. <a href='https://commits.kde.org/discover/67ad21e9f8b0720d928aa5c4c63a03f79bd42e86'>Commit.</a> </li>
<li>Flatpak: Report errors when running a transaction. <a href='https://commits.kde.org/discover/13557f71e486b5711d1bd686526a88b5219adf9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404913'>#404913</a></li>
<li>Set the sources label to  textFormat: Text.StyledText. <a href='https://commits.kde.org/discover/1b569001aac76d1a733a7ed45cf249da18c10a7a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404755'>#404755</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Warning--. <a href='https://commits.kde.org/drkonqi/280dd67b3f7f9b5442ba3e6a0f3306bd4836da8e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19415'>D19415</a></li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Fix crash when installing gtk themes locally. <a href='https://commits.kde.org/kde-gtk-config/1bdaa7db43f2645ff1e4607545048f4eb395e26d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398112'>#398112</a></li>
<li>Explicitly render svgs into images that are the right size. <a href='https://commits.kde.org/kde-gtk-config/301497fb33177ac305a7621a98de867dbb649074'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396586'>#396586</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Consistent arrow key handling in the Informative Alt+Tab skin. <a href='https://commits.kde.org/kdeplasma-addons/d7237df172e5a9065e387c94336cc38fd5c1fc13'>Commit.</a> See bug <a href='https://bugs.kde.org/370185'>#370185</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16093'>D16093</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Style++. <a href='https://commits.kde.org/kinfocenter/515575b9495b2c487032a6c4cca6d8f2e882e5fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19704'>D19704</a></li>
<li>Single-clicks correctly activate modules again when the system is in double-click mode. <a href='https://commits.kde.org/kinfocenter/dbaf5ba7e8335f3a9c4577f137a7b1a23c6de33b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405373'>#405373</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19703'>D19703</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[platforms/x11] Properly unload effects on X11. <a href='https://commits.kde.org/kwin/38e22ce6d18ca3f71cc1b3213ce4fc7de9cc8ef4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399572'>#399572</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19178'>D19178</a></li>
<li>Properly restore current desktop from session. <a href='https://commits.kde.org/kwin/a9e469bf13f6d99c24eb2ae24c3a0f4ec0a79d61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390295'>#390295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19520'>D19520</a></li>
<li>[effects/screenshot] Set m_windowMode in screenshotWindowUnderCursor. <a href='https://commits.kde.org/kwin/2ea6fc2abefd810915c38daee95fe31205ca741b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374864'>#374864</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19552'>D19552</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Add break to fix tty information. <a href='https://commits.kde.org/libksysguard/dcb0901e543843cbeb9edec702aeadc72bde229b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19064'>D19064</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Make Ctrl+A work regardless of focus and visualize active selection in search heading. <a href='https://commits.kde.org/plasma-desktop/4d0f1a3e6ec76469b918b1c853004d2d6df4fd6d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19518'>D19518</a></li>
<li>[Task Manager] Fix virtual desktops subtext on task tooltip. <a href='https://commits.kde.org/plasma-desktop/d769b7aa8838e95e20aa3b52ae5a0db575111416'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19257'>D19257</a></li>
<li>Reset tooltip delegate label height to defaults. <a href='https://commits.kde.org/plasma-desktop/ef06af7c01a95339a8f44aafa82e0fd619a9efe9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401842'>#401842</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18640'>D18640</a></li>
<li>[kcolorschemeditor] Don't re-add existing tab. <a href='https://commits.kde.org/plasma-desktop/45f10322b8f33b7f7a38e3a1cafdbbc030a484c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393282'>#393282</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19572'>D19572</a></li>
<li>[Task Manager] Make sure "Alternatives..." context menu item is always available. <a href='https://commits.kde.org/plasma-desktop/fd7b359f84617cbab6a232007789b2ed618c4395'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404969'>#404969</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19472'>D19472</a></li>
<li>[ConfigCategoryDelegate] Add horizontal padding to the label. <a href='https://commits.kde.org/plasma-desktop/ff0a0e30a28a0e34bcf87b719a3e58255a66e421'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19275'>D19275</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Search our cmake dir first. <a href='https://commits.kde.org/plasma-pa/3f6a9d90123ba86e4e1799177c56342b78312746'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19381'>D19381</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Task Manager] Fix sorting of tasks on last desktop in sort-by-desktop mode. <a href='https://commits.kde.org/plasma-workspace/53290043796c90207c5b7b6aad4a70f030514a97'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19259'>D19259</a></li>
<li>[notifications] Lift up notification content if one line of body text droops. <a href='https://commits.kde.org/plasma-workspace/23f3345a22878eadc58a54eed27ad9a231877bca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404407'>#404407</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19036'>D19036</a></li>
<li>[OSD] Fix animation stutter. <a href='https://commits.kde.org/plasma-workspace/9a7de4e02399880a0e649bad32a40ad820fc87eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19566'>D19566</a></li>
<li>[shell] Show kactivies warning only on error. <a href='https://commits.kde.org/plasma-workspace/70ef0829982e395ca1243bf9ff84d9368726239b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18922'>D18922</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Fix crash when mutating a container while iterating it. <a href='https://commits.kde.org/powerdevil/980156cda79e573d99a5bdc07fe379aac07cc42e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403776'>#403776</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Set colorSet on categories header. <a href='https://commits.kde.org/systemsettings/441697ca9b81e78a27075ef2b40dde170bc81259'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19682'>D19682</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Use high dpi pixmaps. <a href='https://commits.kde.org/xdg-desktop-portal-kde/90f655a146d1fd19a4ac6c14cf3452f9f7034b0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405160'>#405160</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19580'>D19580</a></li>
<li>[File Chooser Portal] Confirm overwrite on saving. <a href='https://commits.kde.org/xdg-desktop-portal-kde/194fb4dee8cbfef9ae72968367f20182f517efa0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404719'>#404719</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19441'>D19441</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
