<?php
  $page_title = "KDE 3.0 Released to public";
  $site_root = "../";
  include "header.inc";
?>
<p>DATELINE APRIL 3, 2002</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Releases Leading Linux Desktop, Welcomes Enterprises to
   the Free World
</h3>

<p>
  <strong>
    The KDE Project Ships the Third-Generation of the Leading
    Desktop for Linux/UNIX, Offering Enterprises, Governments, Schools,
    and Businesses an Outstanding Free and Open Desktop Solution
  </strong>
</p>

<p>
  April 3, 2002 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.0,
  the third generation of KDE, a <em>free</em> and powerful desktop for Linux
  and other UNIXes.  KDE 3.0 is available in 50 languages and ships
  with the core KDE libraries, the base desktop environment,
  an <a href="http://www.kdevelop.org/">integrated development environment</a>, 
  and hundreds of applications and other desktop enhancements
  from the other KDE base packages
  (administration, artwork, development, edutainment, development,
  games, multimedia, PIM, utilities, and more). A KDE 3 port of the 
  <a href="http://www.koffice.org/">KDE office suite</a> is available.
  Consistent with KDE's rapid and disciplined
  development pace, the release of KDE 3.0 includes an
  <a href="/announcements/changelog2_2_2to3_0.html">impressive catalog</a> of bug fixes, performance
  enhancements and feature additions.
</p>
<p>
  KDE, including all its libraries and its applications, is
  available <strong>for free</strong> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from the KDE
  <a href="http://download.kde.org/stable/3.0/">http servers</a>
  or <a href="http://www.kde.org/ftpmirrors.html">ftp mirrors</a>,
  and can also be obtained on
  <a href="http://www.kde.org/cdrom.html">CD-ROM</a> or with any
  of the major Linux/UNIX systems shipping today.
</p>
<p>
  "More and more pieces have come together for creating a viable Linux
  desktop," stated Bernd Kosch, Vice President of Marketing Strategy
  and Alliances at
  <a href="http://www.fujitsu-siemens.com/linux/index.html">Fujitsu
  Siemens Computers</a>.  "KDE 3 is an important step forward towards
  this goal since it will make it easier for ISVs to port to the
  Linux/KDE platform. We see increasing interest in Linux and KDE and
  we appreciate the excellent achievements of the development team."
</p>
<p>
  "In response to customer demand, we have made KDE the default desktop
  environment in our Turbolinux Workstation product," said Dino Brusco, VP
  of Marketing at <a href="http://www.turbolinux.com/">Turbolinux Inc.</a>
  "Our customers really appreciate the features and stability that KDE
  provides and we will be offering this latest version of KDE in future
  releases of our Turbolinux Workstation product."
</p>
<p>
  "We are very impressed with the design and performance and productivity
  boosts KDE 3.0 provides for Linux desktop computing," said Carsten
  Fischer, <a href="http://www.suse.com/">SuSE Linux</a> Product Manager.
  "KDE 3 is perfectly aligned with our strategy to provide our users
  with simple but powerful tools to perform their daily tasks with Linux,
  and we are very pleased to ship KDE 3 in our new 8.0 release later
  this month."
</p>
<p>
  "KDE systems - combined with GNU/Linux or a UNIX system - offer a
  compelling solution for
  enterprises which desire to realize substantial savings in their
  IT budgets, and comes at an opportune time in light of current
  economic conditions and runaway licensing fee inflation," added
  Andreas Pour, Chairman of the <a href="http://www.kdeleague.org/">KDE
  League</a>.
  "KDE enables enterprises, governments, schools, retail outlets, charities
  and other businesses to shed themselves of a substantial portion of
  maddening licensing fees, restrictions and audits.  Essentially, I guess
  you could say that KDE is opening the gates to freedom for the
  enterprise."
</p>
<p>
  More information about using KDE systems in an enterprise environment
  is <a href="http://www.kdeleague.org/kgx.php">available</a> at the
  KDE League <a href="http://www.kdeleague.org/">website</a>.
</p>
<p>
  KDE 2 applications will work with KDE 3 if the KDE 2 libraries
  are present.  Many KDE 2 applications have already been
  ported to KDE 3, and <a href="#porting">porting to the new framework</a>
  is mostly straight-forward.
</p>

<h4>
  K Desktop Environment 3.0
</h4>
<p>
  <u>Printing</u>.
  <a name="kdeprint"></a>
  One of the most exciting additions to KDE 3 is the new printing
  framework, <a href="http://printing.kde.org/">KDEPrint</a>.
  Its modular design makes it easy to support different printing
  engines, such as CUPS, LPRng, LPR, LPD or other servers or
  programs.
</p>
<p>
  The framework consists of a print command/dialog; a printing
  manager; a job viewer for queue control and management; a
  Command Editor for cascading a series of external print job
  filters such as <a href="http://people.ssh.fi/mtr/genscript/">enscript</a>,
  <a href="http://www-inf.enst.fr/~demaille/a2ps/">a2ps</a> and
  pamphlet and providing the GUI elements to configure these filters;
  a wizard for auto-detecting and installing new printers; and a
  CUPS configuration tool.
</p>
<p>
  In conjunction with CUPS, KDEPrint can now manage an elaborate
  enterprise networked printing system.
  The new features include complete administrative control over
  multiple print queues and the queued jobs, drag'n'drop printing,
  print job scheduling, print job prioritization, billing and
  accounting support, print queue migration with possible adaptation
  of the print settings to the new printer's capabilities, per-printer
  print quotas, per-user print authorization and printer list filtering
  for restricting the printers which are listed in individual users'
  printer selection dialog.
</p>
<p>
  As KDEPrint provides a command-line interface, the framework,
  including the GUI configuration elements, are accessible
  by any non-KDE application which permits users to configure
  a printing command, such as StarOffice, OpenOffice, WordPerfect 2000,
  Netscape, Mozilla, Galeon, Acrobat Reader, gv and many other
  applications.
</p>
<p>
<u>Internationalization</u>.
  KDE 3.0 is shipping in 50 languages and additional languages are
  expected for the future KDE 3.1 release. All speakers of yet unsupported
  languages are encouraged to translate KDE into their native language 
  using KDE's
  advanced translation tools, without the need for any programming
  skills. Please visit the <a href="http://i18n.kde.org/">KDE i18n server</a>
  for details and coordination.
</p>
<p>
  KDE's impressive internationalization is largely possible due to
  its use of Unicode (including Unicode 3) throughout its libraries.
  In addition, KDE provides support for "right-to-left" languages
  such as Arabic and Hebrew.  And multiple font encodings and directions
  can be displayed in the same document, even to a certain extent when no 
  Unicode fonts are available.
</p>
<p>
  <u>Browser</u>.
  <a name="Konqueror"></a><a href="http://konqueror.kde.org/">Konqueror</a>
  is KDE's fifth-generation web browser, file manager and document viewer.
  The standards-compliant Konqueror has a component-based architecture
  which combines the features and functionality of Internet
  Explorer/Netscape Communicator and Windows Explorer.
</p>
<p>
  <a name="konq-internet">Konqueror uses KHTML</a> as its rendering engine.
  KHTML supports the full gamut of current Internet technologies.
  It supports the scripting language ECMAScript ("JavaScript") as well as Java; the XML 1.0 and
  HTML 4.0 markup languages; cascading style sheets (CSS-1 and -2); secure
  communications with SSL; Netscape Communicator plug-ins, including
  <a href="http://macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">Flash</a>
  and
  <a href="http://www.real.com/player/index.html?src=010709realhome_1">RealAudio
  and RealVideo</a>; and, in conjunction with some
  <a href="http://www.codeweavers.com/products/crossover/">commercial
  add-ons</a>, Windows Netscape plugins, including
  <a href="http://macromedia.com/software/shockwaveplayer/">Macromedia
  Shockwave Player</a>, the
  <a href="http://www.apple.com/quicktime/">QuickTime</a> and Windows
  Media Player 6.5 multimedia players, and various MS Office document
  viewers.
</p>
<p>
  "One of the major improvements brought by KDE 3.0 over KDE 2.2 is the
  JavaScript/DHTML support in Konqueror," stated
  <a href="http://www.kde.org/people/dirk.html">Dirk Mueller</a>,
  a KHTML developer and the KDE 3 release coordinator.
  "The implementation of the DOM model, used for rendering XML and 
  well designed HTML pages, has been much improved. The CSS 1 rendering
  module support is considered complete.  
  The JavaScript bindings and support is almost complete, faster
  and more stable than in KDE 2.
  The resulting improvements in the speed and rendering quality
  of state of the art web pages is something users will appreciate
  immediately."
</p>
<p>
  Konqueror's improved JavaScript support is nicely demontrated
  by the added support for <a href="http://www.webreference.com/dhtml/column65/">Heirmenus</a>,
  a sophisticated JavaScript pop-up and drop-down menu system.
  This release also adds comprehensive SSL certificate and CA management
  tools, as well as new configuration options for image management and
  a reimplemented zooming functionality.
</p>
<p>
  <u>File manager</u>.
  Konqueror is also KDE's file manager.
  In KDE 3.0, Konqueror can display additional file information -
  file size, permissions and mimetype-specific information such as
  ID3 tags for MP3s or comments for JPG images - in a tooltip, and,
  in appropriate cases, edit this information.
  Moreover, Konqueror provides audio file "previews" by hovering the
  mouse over the file's icon.  In addition, Konqueror's sidebar now
  features a media player, to which one can drag audio/video files
  for quick play.
</p>
<p>
  <u>Email</u>.
   <a href="http://kmail.kde.org/">KMail</a> is KDE's full-featured
   and user-friendly e-mail client.  KMail supports both the popular
   IMAP and POP3 mail standards.  Users can have multiple accounts and multiple
   identities.  Its address book is based on the vCard address book
   standard, and is shared with the rest of KDE.
</p>
<p>
   Its extensive message management capabilities include
   powerful filtering, background message sending/receiving
   (including new, fast POP3 message pipelining), selective message
   retrieval from POP3 servers using the powerful new filter mechanism
   on the message headers, fast IMAP header retrieval (<em>new</em>),
   nested mail folders, drag'n'drop organization of mails and smart
   mailing-list folders.  Its powerful email handling
   includes support for reading HTML emails, message sorting, inline attachments,
   auto-email-address completion, distribution lists and aliases
   (<em>new</em>), and the ability
   to import existing mail folders from various popular mail agents.
   Its user-friendly mail editor offers spell-checking, undo/redo,
   searching, and (<em>new</em>) improved auto-selection of suitable
   charsets and encodings for messages.
</p>
<p>
   For protecting the privacy of emails, KMail supports
   OpenPGP encryption via PGP and <a href="http://www.gnupg.org">GnuPG</a>,
   including automatic encryption of outgoing messages whenever possible
   ("opportunistic encryption") (<em>new</em>).
   In addition, KMail supports SLS/TLS for accessing POP3 or IMAP4
   mail servers and SMTP and DIGEST-MD5 authentication, most of which
   can be auto-configured by KMail (<em>new</em>).  The KDE Project
   is proud that the federal government of Germany has
   <a href="http://www.gnupg.org/aegypten/">sponsored KMail</a>
   to participate in
   <a href="http://www.bsi.de/aufgaben/projekte/sphinx/index.htm">Sphinx</a>,
   its project to provide secure email facilities.
</p>
<p>
  <u>Office suite</u>.
  <a href="http://www.koffice.org/">KOffice</a> 1.1.1, a free, integrated office suite
  which utilizes free and open standards for the component object model, 
  is available for KDE 3.0. All KOffice components
  are capable of embedding other KOffice components/documents as well
  as images, support scanning images, provide advanced printing features,
  use a DOM-compliant XML for their native document format, and provide a
  <a href="http://developer.kde.org/documentation/library/dcop.html">DCOP</a>
  interface for desktop scripting.
</p>
<p>
  <em>Word processing</em>.  
  KWord is a FrameMaker-like word-processing and desktop publishing
  application ideal for those who need not frequently interchange MS Word
  documents with others, though this limitation should disappear with the
  <a href="http://developer.kde.org/development-versions/koffice-1.2-release-plan.html">release
  of KOffice 1.2</a> in mid-August 2002.  It has a frame orientation,
  making it suitable for simple desktop publishing (DTP).
</p>
<p>
  Paragraph style settings include borders, alignment, spacing, indentation,
  bullet points, tab stops, page breaks, and font type, style, color and
  size.  KWord provides a stylist to edit, add, remove and update
  styles, and ships a number of predefined styles.
</p>
<p>
  Page-level features include multiple columns per page, headers and
  footers (including different first page headers/footers) and
  numerous preset as well as custom page sizes.
</p>
<p>
  Document features include tables, embedding of text frames, images and
  clip-art (.WMF files) as well as other KOffice components; templates;
  auto-generation of table of contents; auto-correction and spell checking;
  chapter numbering; and document variables, such as page number, company
  name, user name, document summary, date and time or a custom variable.
</p>
<p>
  <em>Spreadsheet</em>.
  KSpread is a scriptable spreadsheet program which provides both
  table-oriented sheets and support for complex mathematical formulas
  and statistics.  KSpread's document-level features include templates;
  multiple tables/sheets per document; headers and footers; comments; and
  hyperlinks.
</p>
<p>
  KSpread also provides powerful formula support, including over 100
  formulas (such as standard deviation, variance and present value of
  annuities); sorting; and series (days of week, months of year, numbers,
  etc.).
</p>
<p>
  Its table/cell capabilities include cell data validity checking with
  configurable warnings/actions; conditional coloring of cells;
  multiple chart formats for graphically displaying data; row and
  column customization (size, show/hide, font type, style and size, etc.);
  and cell customization (data/number format, precision, border, alignment,
  rotation, background color and pattern, font type, style and size, etc.).
</p>
<p>
  <em>Presenter</em>.
  KPresenter is a presentation application.  It provides basic operations
  such as inserting and editing rich text (with bullet points, indentation,
  spacing, colors, fonts, etc.); embedding images and clip-art (.WMF files);
  and auto-forms.  KPresenter can set many object properties, such as
  background, gradient, pen, shadow, rotation and object-specific settings;
  manipulate objects, such as resizing, moving, lowering and raising;
  group and ungroup objects; and assign effects for animating objects or
  changing
  slides.
</p>
<p>
  On the document level, KPresenter supports templates; headers and footers;
  advanced undo/redo; configuring the slide backgrounds (color, gradients,
  pictures, clip-art, etc.).  It also provides a Presentations Structure
  Viewer.
</p>
<p>
  Besides the capability to generate screen presentations with effects,
  KPresenter can also generate HTML slide shows or PDF documents with just
  a few mouse clicks.
</p>
<p>
  In addition to the forgoing components, KOffice 1.1.1 for KDE 3 ships with
  <a href="http://www.koffice.org/kontour/">Kontour</a>
  (a vector-drawing application);
  <a href="http://www.thekompany.com/projects/kivio/">Kivio</a>
  (a flowchart application);
  <a href="http://www.koffice.org/kchart/">KChart</a>
  (a chart drawing application);
  <a href="http://www.koffice.org/kformula/">KFormula</a>
  (a formula editor);
  <a href="http://www.thekompany.com/projects/kugar/">Kugar</a>
  (a tool for generating business quality reports); and
  <a href="http://www.koffice.org/filters/">filters</a>
  (for importing documents created by, or exporting documents for use with,
  other office suites or office programs).
</p>
<p>
  <u>PIM</u>.
  <em>Calendaring / Group Scheduling</em>.
  <a name="korganizer"></a>
  <a href="http://korganizer.kde.org/">KOrganizer</a> is KDE's
  calendaring and scheduling program for organizing appointments, todo
  lists, projects and more. It is an integral part of the
  <a href="http://pim.kde.org/">KDE PIM</a> suite,
  which aims to be a complete solution for organizing your personal
  data. KOrganizer supports the two dominant standards for storing and
  exchanging calendar data, vCalendar and iCalendar.
</p>
<p>
  The major new feature in this release is support for group
  scheduling according to the iTIP standard. This addition
  supports appointment sharing, meeting requests, responses to
  meeting requests, and more.  The group scheduling is based on
  a peer-to-peer architecture using email as the communication
  medium. It interoperates with other scheduling applications
  implementing the iTIP standard, such as Evolution or Outlook.
</p>
<p>
  KOrganizer 3.0 also has a new plugin interface for extending
  KOrganizer with additional date based information like holidays or
  new views on the calendar data like the project view, permits pinning
  contacts to appointments and tasks and provides an innovative way to
  indicate the current date and time in the agenda views ("Marcus Bains
  Line").
</p>
<p>
  <em>Address book</em>.
  <a name="kaddressbook"></a>This release introduces a new KDE address
  book library which provides a central address book to all KDE
  applications. The new library is based on the vCard standard and has
  provisions for being extended by additional backends like LDAP or
  database servers.
</p>
<p>
  <em>Reminders</em>.
  <a name="kalarm"></a>
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a> is a
  new application in the KDE 3.0 version of KDE PIM. It allows to
  quickly setup reminders when the full functionality of a scheduling
  program isn't needed. KAlarm shares the alarm daemon with KOrganizer.
</p>
<p>
  <u><a name="multimedia">Multimedia</a></u>.
  KDE 3 provides a rich set of multimedia tools, from a CD player to
  a themeable media player for .WAV, .MP3 and
  <a href="http://www.xiph.org/ogg/vorbis/">OggVorbis</a> audio files and
  MPEG-1 and <a href="http://mpeglib.sourceforge.net/">DivX</a>
  video files (<a href="http://noatun.kde.org/">Noatun</a>).  Noatun
  features audio effects, a six-band graphic equalizer,
  a full plugin architecture, network transparency and several skins.
  With KDE 3.0, Noatun for the first time offers support for
  <a href="http://www.icecast.org/">Icecast</a> and
  <a href="http://www.shoutcast.com/">SHOUTcast</a> digital
  audio streaming.
</p>
<p>
  KDE 3 also ships aKtion!, a video player for a large number of video
  formats.
</p>
<p>
  <u>File, document and data access</u>.
  KDE provides a network-transparent file, document and
  network protocol access architecture using the <a name="kioslave">KIOSlave I/O
  objects</a>.  When a new KIOSlave is "dropped in" a system its services
  are automatically available to all KDE-compliant applications.
  This modular, plug-in nature of KDE's data architecture makes it
  simple to add additional protocols (such as IPX) to all of KDE.
</p>
<p>
  A large number of protocols have already been implemented,
  from HTTP, SFTP/FTP, telnet/SSH, POP/IMAP,
  NFS/SMB/NetBIOS, LDAP, WebDAV (<em>new</em>) and local files to
  man and info pages, SQL queries, audio CDs, digital cameras,
  PDAs and even shell commands.  All requests can be bookmarked
  for simple and quick retrieval of often-accessed data.
</p>
<p>
  <a name="edutainment"><u>Edutainment</u></a>.
  A package with "Edutainment" applications has been added to KDE. 
  It is maintained
  by the <a href="http://edu.kde.org/">KDE Edutainment Project</a>,
  which aims to create educational software based around KDE.</p>
<p>
  The package currently includes:
<!--- NOT PART OF KDE 3.0 RELEASE
  <a href="http://edu.kde.org/kalzium/">Kalzium</a>, a tool for learning
  the periodic system of the elements;
  <a href="http://edu.kde.org/khangman/">KHangMan</a>, the well-known
  word-solving game;
  <a href="http://edu.kde.org/klatin/">KLatin</a>, a utility to help
  revise or learn Latin;
  <a href="http://edu.kde.org/kmessedwords/">KMessedWords</a>, a simple
  mind-training game in which you have to guess a scrambled word;
  <a href="http://kmplot.sourceforge.net/">kmplot</a>, which makes 2D
  plots of mathematical functions;
  <a href="http://edu.kde.org/kpercentage/">KPercentage</a>, a small
  math application for learning percentages;
  <a href="http://members.tripod.de/arnoldk67/computer/kverbos/kverbos.htm">KVerbos</a>,
  an interactive learning tool for the forms of Spanish verbs;
-->
  <a href="http://keduca.sourceforge.net/">KEduca</a>, an educational
  project to enable the creation and revision of form-based tests and
  exams;
  <a href="http://kgeo.sourceforge.net/">KGeo</a>, an interactive
  geometry learning program similar to Euklid(tm);
  <a href="http://edu.kde.org/klettres/">KLettres</a>, an alphabet and
  sound-recognition game (in French);
  <a href="http://kstars.sourceforge.net/">KStars</a>, a graphical
  desktop planetarium;
  <a href="http://ktouch.sourceforge.net/">KTouch</a>, a program for
  learning touch typing; and
  <a href="http://kvoctrain.sourceforge.net/">kvoctrain</a>, a foreign
  language vocabulary trainer.
</p>
<p>
  <u>KDE Kiosk</u>.
  Some environments, such as kiosks, Internet cafes and enterprise
  deployments, demand that the user not have full access to all of
  KDE's capabilities in order to preclude certain undesirable
  actions.  To address these needs, the KDE Kiosk project was
  launched to supplement standard UNIX permissions.
</p>
<p>
  KDE 3.0 gives birth to the new lockdown framework, which is essentially
  a permissions-based system for altering application configuration
  options.  The
  <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/*checkout*/kdelibs/kdecore/R
EADME.kiosk?rev=1.5">kiosk
  framework</a> supplements KDE's configuration framework
  with a simple application API which applications can query to test
  authorization for certain operations.
</p>
<p>
  Both the KDE panel and the desktop manager already employ this
  system, and by the KDE 3.1 release the other major desktop
  components, such as Konqueror and the Control Center, should also
  have this technology enabled.
</p>

<h4>
  K Development Environment 3.0
</h4>
<p>
  <u>Libraries</u>.
  KDE 3.0 offers both free and proprietary software developers a
  mature, powerful and consistent API for rapid application
  assembly.   Chief among these technologies are the
  <a href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>, the
  <a href="#KIO">I/O libraries (KIO)</a>, <a href="#KParts">the component
  object model (KParts)</a>, an <a href="#XMLGUI">XML-based GUI class</a>,
  a <a href="#KHTML">standards-compliant HTML rendering engine (KHTML)</a>,
  the <a href="#aRts">multimedia architecture (aRts)</a>, the
  new database classes, and the XML GUI framework.
</p>
<p>
  <em>I/O and Virtual Filesystems</em>.
  <a name="KIO">KIO</a> implements application I/O using KIOSlaves
  running as a separate process, thereby enabling a non-blocking
  GUI without the use of threads.
  The class is network and protocol transparent and hence can be
  seamlessly used to access data using the whole gamut of data
  formats and protocols provided by the installed
  <a href="#KIOSlaves">KIOSlaves</a>.
  KIO also implements a trader which can locate handlers
  for specified mimetypes; these handlers can then be embedded within
  the requesting application using the KParts technology.
</p>
<p>
  <em>IPC</em>.
  <a name="DCOP">DCOP</a> is a client-to-client communications
  protocol intermediated by a server over the X11 ICE library.
  The protocol supports both message passing and remote procedure
  calls using an XML-RPC to DCOP "gateway".  KDE 3 greatly expands on
  DCOP usage and the base KDE applications expose substantial functionality
  for other applications to exploit.  DCOP bindings for C, C++ and
  Python, as well as experimental Java bindings and
  a shell-script interface, are available.
</p>
<p>
  <em>Components</em>.
  <a name="KParts">KParts</a>, KDE 3's proven component object model,
  handles all aspects of application embedding, such as positioning
  toolbars and inserting the proper menus when the embedded component
  is activated or deactivated.  KParts can also interface with the
  KIO trader to locate available handlers for specific mimetypes or
  services/protocols.  This technology is used extensively by
  KOffice, KHTML and Konqueror.
</p>
<p>
  <em>Web technologies</em>.
  Konqueror's <a href="#konq-internet">Internet capabilities</a> are
  actually derived from <a name="KHTML">KHTML</a>.  KHTML is available
  to all KDE applications, both as a widget and as a KPart component,
  and to all applications using X11 window reparenting, possibly in
  conjunction with DCOP.
</p>
<p>
  <em>Multimedia</em>.
  KDE's multimedia system is based on
  <a name="aRts"></a><a href="http://www.arts-project.org/">aRts</a> and
  includes the players noted in the <a href="#multimedia">multimedia
  summary</a>.
  ARts is a modular media framework with full streaming support which
  can create sound and play audio and video.  Example modules
  include filters, mixers and faders, codecs, as well as modules for playing
  sounds on speakers or streaming sound over a network.
  With this release, aRts has improved its support for recording,
  MIDI and ALSA, and added support for IRIX.
</p>
<p>
  <em>Database access</em>.
  KDE 3 provides a new database-independent API for accessing SQL
  databases, which provides support for ODBC as well as direct support
  for Oracle, PostgreSQL and MySQL databases (custom drivers may be
  added as well). In addition, new database-aware controls provide
  automatic synchronization between the GUI and the database.
</p>
<p>
  <em>Dynamic GUI</em>.
  The XML GUI framework employs XML to create and position menus,
  toolbars and possibly other aspects of the GUI.  This technology
  offers developers and users the advantage of simplified configuration
  of these user interface elements across applications, as well as
  simple and automatic compliance with the KDE
  <a href="http://developer.kde.org/documentation/standards/kde/style/basics/index.html">Standards
  and Style Guide</a> irrespective of modifications to the standard.
</p>
<p>
  <em>Regular Expressions</em>.
  Finally, KDE 3 offers a powerful new regular expression class.
  While compatible with, and as powerful as,
  <a href="http://www.perl.com/">Perl</a> regular expressions,
  the Qt regular expression classes additionally provide full support
  for international (Unicode) character sets.
</p>
<p>
  <u>IDE</u>.
  <a name="kdevelop">KDevelop</a> is the leading free Linux IDE with numerous
  features for rapid application development, including a setup wizard,
  a GUI dialog builder (with
  <a href="http://www.trolltech.com/products/qt/tools.html">Qt Designer</a>),
  integrated debugging (optionally using <a href="#kdbg">KDbg</a>), project
  management, documentation and translation facilities (with
  <a href="#kbabel">KBabel</a>), built-in concurrent development
  support, a console, man page support, syntax highlighting,
  and a number of new project templates, including KControl modules,
  Kicker (panel) applets, KIOSlaves, Konqueror plugins and desktop styles.
</p>
<p>
  With KDE 3.0, KDevelop has benefitted from a greatly improved
  Qt Designer, which now supports interactive construction of the
  application main windows with menus and tool bars in addition to
  dialogs.  It supports KDE, Qt and custom widgets, including
  preview, and integrates smoothly into KDevelop.
</p>
<p>
  In addition, KDevelop has added auto-code-completion for class
  variables, class methods, function arguments and more; full
  cross-compiling support, with the ability to specify different
  compilers, compiler flags, target architecture, etc.; and
  support for Qt/Embedded projects (such as the
  <a href="http://developer.sharpsec.com/">Zaurus</a> and
  <a href="http://www.compaq.com/products/iPAQ/">iPAQ</a>);
</p>
<p>
  <u>Other development tools</u>.
  Other KDE development tools include:
</p>
<ul>
  <li>
    <a name="kate"></a><a href="http://kate.sourceforge.net/">Kate</a>,
    a multi-view programmer's editor with syntax highlighting and integrated
    console;
  </li>
  <li>
    <a href="http://members.nextra.at/johsixt/kdbg.html">KDbg</a>,
    a frontend to <a href="http://www.gnu.org/directory/gdb.html">gdb</a>
    (the <a href="http://www.gnu.org/">GNU</a> debugger) which integrates
    into KDevelop and provides an intuitive interface for setting
    breakpoints, inspecting variables and stepping through code;
  </li>
  <li>
    <a name="kbabel"></a>
    <a href="http://i18n.kde.org/tools/kbabel/">KBabel</a>, an advanced
    and easy-to-use editor for translating applications into other languages
    which integrates into KDevelop and
    <a href="http://i18n.kde.org/tools/kbabel/features.html">features</a>
    full navigation capabilities, full editing functionality,
    search functions, syntax checking and statistics functions; and
  </li>
  <li>
    Konsole, KDE's advanced terminal emulator, has been enhanced
    in a number of significant ways for this release, including shortcuts
    for activating menubars and renaming sessions, history management,
    text drag'n'drop, associating Konsole "sessions" with working
    directories, and a DCOP interface.
  </li>
</ul>
<p>
  <u>Language bindings</u>.
  A number of languages have bindings for KDE.  In particular,
  full C, Objective C, Java and Python bindings are available for KDE 3,
  which look and behave identically to a C++ version, including
  in most cases access to the signal/slot architecture.  In addition,
  C# bindings for Qt 3 are available and C# bindings for KDE are planned.
</p>
<p>
  <u>Qt integration</u>.
  KDE 3 improves the integration of pure Qt applications into KDE
  by applying the KDE widget style plugins to pure Qt applications.
  In addition, the Qt style engine has been extended to support a
  wider range of standard widgets, including progress bars,
  spin boxes, and table headers.  Consequently, developers can
  benefit from Qt's cross-platform support without sacrificing
  the KDE "look-and-feel" when running in KDE.
</p>
<p>
  <u>Porting to KDE 3</u>.<a name="porting"></a>
  Since KDE 3 is mostly source compatible with KDE 2, porting applications
  from KDE 2 to KDE 3 can usually be done with only minor adjustments.  Even large
and complicated applications have been ported in a matter of just hours.
  Instructions for porting KDE 2 applications to KDE 3 are available
  separately for the
  <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE3PORTING.html">KDE
  libraries</a> and the
  <a href="http://doc.trolltech.com/3.0/porting.html">Qt libraries</a>.
  The entire series of KDE 3.x releases will remain source and binary compatibility
  with KDE 3.0.
</p>

<h4>
<a name="binary">
  Installing KDE 3.0 Binary Packages
</a>
</h4>
<p>
  <u>Binary Packages</u>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.0 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/stable/kde-3.0/">http</a> or 
  <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>
<p>
  Please note that the KDE Project makes these packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which the KDE project has no control. If you
  cannot find a binary package for your OS, or you are displeased with
  the quality of binary packages available for your system, please read
  the <a href="http://www.kde.org/packagepolicy.html">KDE Binary Package
  Policy</a> and/or contact your OS vendor.
</p>
<p>
  <u>Library Requirements / Options</u>.
  The library requirements for a particular binary package vary with the
  system on which the package was compiled.  Please bear in mind that
  some binary packages may require a newer version of Qt and other libraries
  than was shipped with the system (e.g., LinuxDistro X.Y
  may have shipped with Qt-3.0.0 but the packages below may require
  Qt-3.0.3).  For general library requirements for KDE, please see the text at
  <a href="#source_code-library_requirements">Source Code - Library
  Requirements</a> below.
</p>
<p>
  <a name="package_locations"><em>Package Locations</em></a>.
  At the time of this release, pre-compiled packages are available for:
</p>
  <ul>
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>, 
    <a href="http://download.kde.org/stable/3.0/Conectiva/">7.0</a></li>
<li><a href="http://www.freebsd.org/">FreeBSD</a>,
    <a href="http://download.kde.org/stable/3.0/FreeBSD/">4.5-STABLE</a></li>
<li><a href="http://www.mandriva.com/">Mandrake</a>,
    <a href="http://download.kde.org/stable/3.0/Mandrake/">8.0, 8.1, 8.2</a></li>
<li><a href="http://www.redhat.com/">Red Hat</a>,
    <a href="http://download.kde.org/stable/3.0/Red%20Hat/README">README</a></li>
<li><a href="http://www.slackware.org/">Slackware</a>,
    <a href="http://download.kde.org/stable/3.0/Slackware/">8.0</a></li>
<li><a href="http://www.suse.com/">SuSE</a>,
    <a href="http://download.kde.org/stable/3.0/SuSE/">various for i386, ppc, s390 and Sparc</a></li>
<li><a href="http://www.tru64unix.compaq.com/">Tru64</a>,
    <a href="http://download.kde.org/stable/3.0/Tru64/">link</a></li>
<li><a href="http://www.yellowdoglinux.com/">YellowDog</a>,
    <a href="http://download.kde.org/stable/3.0/YellowDog">2.2</a></li>
  </ul>
<p>
  More binary packages will likely become available over the coming
  days and weeks, so please check back again from time to time. 
</p>
<h4>
  Compiling KDE 3.0
</h4>
<p>
  <a name="source_code-library_requirements"></a><u>Library
  Requirements / Options</u>.
  KDE 3.0 requires or benefits from the following libraries, most of
  which should be already installed on your system or available from
  your OS CD or your vendor's website:
</p>
<table border="0" cellpadding="6" cellspacing="0">
  <tr valign="top">
    <td width="50%">
      <u>Basic</u>
    </td>
    <td width="50%">
      <u>Help/Manuals</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt 3.0.3</a> (<strong>required</strong>)
        </li>
        <li>
          an X Server (<strong>required</strong>) with various
          optional extensions (<a href="http://www.xfree86.org/">XFree86</a>
          4.2.x, which includes all the enumerated extensions, is
          recommended)
          <ul>
            <li>
	        the RENDER extension for beautiful anti-aliased fonts
	      </li>
            <li>
	        the DPMS extension Energy Star display power management
	      </li>
            <li>
              the Xinerama extension for modern multi-head displays
	      </li>
            <li>
              the XVideo extension for enhanced video playback
	      </li>
          </ul>
        </li>
        <li>
          <a href="http://www.cs.wisc.edu/~ghost/index.html">GhostScript</a>
          for PostScript/PDF support (preferably 6.50 or later)
        </li>
        <li>
          <a href="http://www.mysql.com/">MySQL</a>,
          <a href="http://postgresql.readysetnet.com/">PostgreSQL</a>,
          or <a href="http://genix.net/unixODBC/">unixODBC</a> for
          database support
        </li>
        <li>
          <a href="http://www.sleepycat.com/">Berkely DB II</a> is highly
          recommended for
          <a href="http://i18n.kde.org/tools/kbabel/">KBabel</a>, the
          KDE translation tool
        </li>
        <li>
          <a href="http://www.python.org/">Python</a> for scripting in some
          KOffice components
        </li>
        <li>
          <a href="http://www.perl.com/">Perl</a> for scripting in KSirc
          and automating updates of configuration files for new releases
        </li>
        <li>
          <a href="ftp://ftp.gnu.org/pub/gnu/tar/">gzip</a> and
          <a href="ftp://sourceware.cygnus.com/pub/bzip2/v100/">bzip2</a>
          for data compression
        </li>
        <li>
          <a href="http://www.dante.de/">TeX</a> and for LaTeX document
          processing
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://www.xmlsoft.org/downloads.html">libxml2</a>
          &gt;= 2.3.13 and
          <a href="http://xmlsoft.org/XSLT/downloads.html">libxslt</a>
          &gt;= 1.0.7 for reading documentation
        </li>
        <li>
          <a href="http://www.htdig.org/">ht://dig</a> for indexing and
          searching documentation with KDevelop
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Hardware</u>
    </td>
    <td>
      <u>Networking</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.cups.org/">CUPS</a> &gt;= 1.1.9 for
	      enhanced printing administration, options and usability
        </li>
        <li>
          <a href="http://www.gphoto.org/download.html">gphoto2</a> &gt;=
          2.0.1 devel for accessing images on digital cameras
        </li>
        <li>
          <a href="http://www.mostang.com/sane/">SANE</a> for scanning
          support
        </li>
        <li>
          <a href="http://www.netroedge.com/~lm78">lm-sensors</a>
          for monitoring motherboards
        </li>
        <li>
          <a href="http://mtools.linux.lu/">mtools</a> for accessing a
          floppy disk as <code>floppy:/</code> from Konqueror
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="ftp://cs.anu.edu.au/pub/software/ppp/">pppd</a> for
          dialup networking
        </li>
<!--
        <li>
          ?? USED ?? <a href="http://radscan.com/nas.html">NAS/libaudio</a>
          for network audio support
        </li>
-->
        <li>
          <a href="http://nicolas.brodu.free.fr/libsmb/">libsmb</a> for
          browsing Windows/NetBIOS shares
        </li>
        <li>
          <a href="http://www.openldap.org/">libldap</a> for
          LDAP address book support
        </li>
        <li>
          FAM for efficient
          file/directory change notification
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Browsing/Internet</u>
    </td>
    <td>
      <u>Encryption/Security</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          Java Virtual Machine &gt;= 1.3 for
          <a href="http://java.sun.com/">Java</a> applet support
        </li>
        <li>
          a recent version of <a href="http://www.lesstif.org/">Lesstif</a>
          or Motif for Netscape Communicator plugin support
        </li>
        <li>
          <a href="http://www.winehq.org/">WINE</a> for executing certain
          MS Windows controls and applets
        </li>
        <li>
          <a href="http://www.codeweavers.com/products/crossover/">Crossover
          Plugin</a> for Quicktime, Shockwave Director, Windows Media
          Player 6.4 and MS Office viewer support
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x for
          HTTPS, SFTP, SSH, VPN and more (versions 0.9.5x are no longer
          supported)
        </li>
        <li>
          <a href="http://www.gnupg.org/">GnuPG</a> or PGP for email/document
          encryption/decryption
        </li>
        <li>
          <a href="http://java.sun.com/products/jsse/">JSSE</a> 1.0.2 for
          Java applets requiring SSL/HTTPS (common with online banking) -
          included with a JVM 1.4
        </li>
        <li>
          <a href="http://www.us.kernel.org/pub/linux/libs/pam/modules.html">PAM</a>
          for services (such as login) authentication
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Graphics</u>
    </td>
    <td>
      <u>Multimedia</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.mesa3d.org/">OpenGL</a> for some 3D
          screensavers and some 3D graphics programs
        </li>
        <li>
          <a href="http://libsdl.org/">SDL</a> &gt;= 1.2.0 for some
          <a href="http://noatun.kde.org/">Noatun</a> plugins
        </li>
        <li>
          <a href="http://www.libtiff.org/">libtiff</a> for viewing
          facsimiles
        </li>
        <li>
          <a href="http://www.libmng.com/">libmng</a>,
	      <a href="http://www.libpng.org/pub/png/libpng.html">libpng</a> and
	      <a href="ftp://ftp.uu.net/graphics/jpeg/">libjpeg</a> for
          viewing images
        </li>
        <li>
          imlib 1 for using
          <a href="http://kuickshow.sourceforge.net/">Kuickshow</a> for
          viewing a wide variety of image formats
        </li>
        <li>
          <a href="http://www.freetype.org/">freetype 2</a> for anti-aliased
          font handling and manipulation
        </li>
        <li>
          <a href="http://www.foolabs.com/xpdf/about.html">PDFinfo</a>
          for enhanced PDF file browsing
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://mpeglib.sourceforge.net/">mpeglib</a> for video
          playback (included with kdemultimedia)
        </li>
        <li>
          <a href="http://www.alsaplayer.org/">ALSA</a> for more
          advanced audio support
        </li>
        <li>
          <a href="http://www.fokus.gmd.de/research/cc/glone/employees/joerg.schilling/private/cdrecord.html">cdtools/cdparanoia</a>
          for ripping audio CDs
        </li>
        <li>
          <a href="http://lame.sourceforge.net/">LAME</a> for encoding
          MP3 files
        </li>
        <li>
          <a href="http://www.xiph.org/ogg/vorbis/">libogg/libvorbis</a> for
          Ogg Vorbis encoding/playback
        </li>
        <li>
          <a href="http://webs1152.im1.net/~dsweet/XAnim/">XAnim</a>
          for aKtion!'s video engine
        </li>
        <li>
          <a href="http://www.68k.org/~michael/audiofile/">libaudiofile</a>
          for playing .WAV audio files
        </li>
      </ul>
    </td>
  </tr>
</table>
<p>
  <u>Compiler Requirements</u>.
  KDE is designed to be cross-platform and therefore supposed to compile with a 
  variety of Linux/UNIX compilers.  However, both the set of available compilers
  as well as KDE is advancing very rapidly, so the ability to compile KDE
  on various UNIX systems depends on users reporting compile problems with
  possible fixes to the KDE project. 
</p>
<p>
  In addition, C++ support by <a href="http://gcc.gnu.org/">gcc</a>,
  the most popular compiler used on GNU/Linux and many traditional UNIX systems, 
  has undergone major improvements. There are many known compile and
  code generation problems with older gcc/egcs releases, therefore the support
  for those compilers has been dropped. 
</p>
<p>
  In particular, gcc versions earlier than gcc-2.95, such as egcs-1.1.2,
  gcc-2.8.x or gcc-2.7.x are no longer supported. 
  In addition, some components of KDE 3.0, such as
  <a href="#aRts">aRts</a>, generally will be incorrectly compiled by unpatched
  versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0.x</a>
  (this problem should be fixed with the release of gcc-3.1). The
  code generation bug does not lead to a compile failure, but rather just
  unexpected problems at runtime. 

  While there have been reports of successful KDE compilations with recent
  versions of the "Red Hat-gcc" 2.96 and gcc-3.1 CVS snapshots, the KDE 
  project at this time
  continues to recommend the use of gcc-2.95.* for compiling KDE. 
</p>
<p>
  <a name="source_code"></a><u>Source Code/SRPMs</u>.
  The complete source code for KDE 3.0 is available for
  <a href="http://download.kde.org/stable/kde-3.0/src/">download</a>.
</p>
<p>
  <u>Screenshots</u>.
  <a href="mailto:cap@capsi.com">Rob Kaper</a> has produced some 
  <a href="/screenshots/kde300shots.html">Screenshots of KDE 3.0</a>.
</p>
<p>
  <u>Further Information</u>.  For further
  instructions on compiling and installing KDE 3.0, please consult
  the <a href="http://developer.kde.org/build/index.html">installation
  instructions</a> and, if you should encounter compilation difficulties, the
  <a href="http://developer.kde.org/build/compilationfaq.html">KDE Compilation FAQ</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p>
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/gallery/index.html">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandriva.com/">MandrakeSoft</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a>, as well as <a href="http://www.kde.org/donations.html">individual
  sponsors</a> (<a href="http://www.kde.org/support.html">donate</a>),
  provide significant support for KDE.  A special thanks goes to
  the <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>,
  for so graciously hosting the KDE projects all these years.  Thanks!
</p>
<h4>
  About KDE
</h4>
<p>
  KDE is an independent project of hundreds of developers, translators,
  artists and professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>
<a name="links"></a>
<table border="0" cellpadding="16" cellspacing="0">
  <tr valign="top">
    <td>
      <em>General</em>
      <ul>
        <li>
          <a href="http://www.kde.org/whatiskde/">What is KDE</a>
        </li>
        <li>
          <a href="http://www.kde.org/faq.html">KDE FAQ</a>
        </li>
        <li>
          <a href="http://bugs.kde.org/">Reporting bugs and
          requesting features</a>
        </li>
        <li>
          <a href="http://www.kde.org/family.html">Family of websites</a>
        </li>
        <li>
          Download (<a href="http://download.kde.org/">http</a>,
          <a href="http://www.kde.org/ftpmirrors.html">ftp</a>)
        </li>
        <li>
          Third-party <a href="http://apps.kde.com/">applications</a>
        </li>
        <li>
          <a href="http://mail.kde.org/mailman/listinfo">Mailing lists</a>
        </li>
        <li>
          <a href="http://enterprise.kde.org/">KDE :: Enterprise</a>
        </li>
        <li>
          <a href="http://www.kdeleague.org/">KDE League</a>
        </li>
      </ul>
    </td>
    <td>
      <em>Contributing</em>
      <ul>
        <li>
          <a href="http://www.kde.org/support.html">Money</a>
        </li>
        <li>
          <a href="http://www.kde.org/jobs/jobs-open.html">Code /
          development</a>
        </li>
        <li>
          <a href="http://i18n.kde.org/">Translations / Documentation</a>
        </li>
        <li>
          <a href="http://artists.kde.org/">Art / Icons</a>
        </li>
        <li>
          <a href="http://promo.kde.org/">Promotion</a>
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <em>KDE 3</em>
      <ul>
        <li>
          a KDE 3 <a href="http://www.kde.org/info/3.0.html">info page</a>
        </li>
        <li>
          <a href="http://www.kde.org/screenshots/kde3shots.html">KDE 3
          screenshots</a>
        </li>
        <li>
          <a href="http://www.kde.org/announcements/changelog2_2_2to3_0.html">KDE
          3.0 change log</a>
        </li>
        <li>
          <a href="http://developer.kde.org/build/build2ver.html">instructions</a> for
          setting up<br />KDE 2 and 3 systems side-by-side
        </li>
        <li>
          a tentative
          <a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">KDE 3.1</a>
          release plan
        </li>
        <li>
          <a href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">developer
          resources</a>
        </li>
      </ul>
    </td>
    <td>
      <em>Major Projects</em>
      <ul>
        <li>
          <a href="http://www.kdevelop.org/">KDevelop</a>
        </li>
        <li>
          <a href="http://kmail.kde.org/">KMail</a>
        </li>
        <li>
          <a href="http://www.koffice.org/">KOffice</a>
        </li>
        <li>
          <a href="http://konqueror.kde.org/">Konqueror</a>
        </li>
        <li>
          <a href="http://multimedia.kde.org/">Multimedia</a>
        </li>
        <li>
          <a href="http://pim.kde.org/">PIM</a>
        </li>
        <li>
          <a href="http://edu.kde.org/">Edutainment</a>
        </li>
        <li>
          <a href="http://i18n.kde.org/">Translations</a>
        </li>
      </ul>
    </td>
  </tr>
</table>
<hr noshade="noshade" size="1" width="98%" align="center" />
<p></p>
<p>
  <font size="2">
  <em>Press Release</em>: Written by 
  <a href="ma&#105;lt&#111;&#x3a;&#x70;&#111;&#117;&#x72;&#064;k&#x64;&#101;.&#111;r&#x67;">Andreas Pour</a> of the 
  <a href="http://www.kdeleague.org/">KDE League</a> with the
  invaluable assistance of numerous volunteers from the KDE Project.
  </font>
</p>
<p>
  <font size="2">
  <em>Trademarks Notices.</em>
  KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

  Acrobat Reader, FrameMaker and PostScript are registered trademarks of Adobe
  Systems Incorporated.

  AOL is a registered trademark of America Online, Inc.

  Compaq, Alpha, iPAQ and Tru64 are either trademarks and/or service marks or
  registered trademarks and/or service marks of Compaq Computer Corporation.

  HP is a registered trademark of Hewlett-Packard Company.

  IBM and PowerPC are registered trademarks of IBM Corporation.

  Intel, i386 and i586 are trademarks or registered trademarks of Intel
  Corporation or its subsidiaries in the United States and other countries.

  Java and Sun are trademarks or registered trademarks of Sun Microsystems, Inc.

  JavaScript is a trademark of Netscape Communications Corporation
  in the United States and other countries.

  Linux is a registered trademark of Linus Torvalds.

  MS Windows, Internet Explorer, Windows Explorer, MS Office, Windows
  Media Player, ActiveX and MSN are trademarks or registered trademarks of
  Microsoft Corporation.

  Netscape and Netscape Communicator are trademarks or registered trademarks of
  Netscape Communications Corporation in the United States and other countries.

  QuickTime is a trademark of Apple Computer, Inc., registered in the U.S.
  and other countries.

  RealAudio and RealVideo are trademarks or registered trademarks of
  RealNetworks, Inc.

  Shockwave and Flash are trademarks or registered trademarks of Macromedia,
  Inc. in the United States and/or other countries.

  Trolltech and Qt are trademarks of Trolltech AS.

  UNIX and Motif are registered trademarks of The Open Group.

  Zaurus and Sharp are trademarks of Sharp Electronics Corporation in the

  Yahoo! is a registered trademark of Yahoo! Inc.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>
<hr noshade="noshade" size="1" width="98%" align="center" />
<table border="0" cellpadding="8" cellspacing="0">
  <tr>
    <th colspan="2" align="left">
      Press Contacts:
    </th>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      United&nbsp;States:
    </td>
    <td nowrap="nowrap">
      Andreas Pour<br />
      KDE League, Inc.<br />
      pour@kdeleague.org<br />
      (1) 917 312 3122
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (French and English):
    </td>
    <td nowrap="nowrap">
      David Faure<br />
      f&#00097;&#0117;r&#101;&#64;&#107;&#x64;e&#46;&#x6f;&#x72;&#x67;<br />
      (33) 4 3250 1445
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (German and English):
    </td>
    <td nowrap="nowrap">
      Ralf Nolden<br />
      &#00110;ol&#100;&#00101;n&#00064;kde&#x2e;&#111;r&#x67;<br />
      (49) 2421 502758
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (English):
    </td>
    <td nowrap="nowrap">
      Jono Bacon<br />
      j&#x6f;no&#x40;k&#x64;e.&#x6f;&#0114;g<br />
<!--      (XX) YYYY YYYY -->
    </td>
  </tr>
</table>
<!-- $Id: announce-3.0.php 1509804 2018-02-02 07:41:48Z bcooksley $ -->
<?php
  include "footer.inc"
?>
