<?php
  $page_title = "Announcing KDE 3.4";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE MARCH 16, 2005</p>

<p>
The <a href="http://www.kde.org">KDE Project</a> ships a new major release of their leading Open Source desktop environment.</p>

<img src="announce-3.4.jpeg" align="right" width="250" height="139" hspace="10" alt="Splash"/>

<p>
March 16, 2005 (The Internet) - After more than a half year of development 
the KDE Project is happy to be able to announce a new major release of the 
<a href="http://www.kde.org/awards">award-winning</a> K Desktop Environment. 
Among the many new features that have been incorporated, the improvements 
in accessibility are most remarkable.
</p>

<p>
One of the milestones in this new release will be the advanced KDE 
Text-to-speech framework. It integrates into KDE's PDF-viewer,
<a href="http://kate.kde.org">editor</a>, <a href="http://konqueror.org">webbrowser</a>
and into the new speaker-tool KSayIt. It also allows to read out notifications
from all KDE applications. Especially partially-sighted people and
speech-impaired users will benefit, but it should also prove a fun
desktop experience overall.
</p>

<p>
For people with low vision, several high contrast themes including a 
complete monochrome icon set have been added. Other accessibility 
applications have been improved. KMouseTool which can click the mouse for 
people with, for example, carpal tunnel syndrome or tendinitis; KMouth to 
allow the computer to speak for the speech impaired; and KMagnifier to 
magnify sections of screen for partially-sighted users. Standard 
accessibility features including "Sticky Keys", "Slow Keys" and "Bounce 
Keys" are also available and are now more easily accessed via keyboard 
gestures. All of these features combine to open the world of computing to 
a much wider audience and to a section of the population that is often 
overlooked. The KDE project will continue its close cooperation with the 
accessibility community to reach even more people in the future.
</p>

<p>Another milestone will be the improvements of KDE's personal information
management suite <a href="http://www.kontact.org/">Kontact</a> and of
KDE's instant messenger <a href="http://www.kopete.org/">Kopete</a>.
Kontact has improved usability including a new message composer and start 
screen, and its support for the free software groupware solution
<a href="http://www.kolab.org/">Kolab</a> has 
been updated to Kolab 2.0. This means that KDE has now a complete groupware 
solution including an open-source server interoperable with proprietary MS 
Windows Outlook clients. Other supported groupware servers include 
eGroupware, GroupWise, OpenGroupware.org and SLOX. Kopete features an improved
contact list showing contact photos, improved Kontact integration and supports
AIM, Gadu-Gadu, GroupWise, ICQ, IRC, Jabber, Lotus Sametime, MSN, Yahoo, and
the sending of SMS.</p>

<p>
With KDE being based on an international community there are more than 49 
translations available and even more to be expected for future service packs
of KDE 3.4. This is why KDE serves best the needs of today's world 
wide Linux community.</p>

<p>
KDE 3.4 is available for free under Open Source licenses and boasts 
eighteen packages of optional applications including accessibility, 
development, games, PIM, network, utilities, administration, 
edutainment, multimedia, graphics and more.</p>

<h2>Reactions from the accessibility community</h2>

<p>&quot;With each new release, KDE continues to enhance its support for people
with disabilities&quot;, Janina Sajka, chair of the
<a href="http://accessibility.freestandards.org/">Accessibility Workgroup</a> of
the <a href="http://www.freestandards.org/">Free Standards Group</a>, said.
&quot;This is making KDE more and more attractive to more persons with disabilities.
And, it's also helping KDE meet various social inclusion objectives worldwide,
such as the Sec. 508 requirements of the U.S. Government.&quot;
</p>

<p>
Lars Stetten from the Accessibility User Group
<a href="http://www.linaccess.org/">Linaccess</a> said about the release:
&quot;The new accessibility features in KDE 3.4 
are an important step for the future, to enable disabled 
people to get to know the KDE desktop and to join its community.&quot;
</p>

<h2>Highlights at a glance</h2>

<ul>
<li>Text-to-speech system with support built into Konqueror, Kate, KPDF and the 
	standalone application KSayIt</li>
<li>Support for text to speech synthesis is integrated with the desktop</li>
<li>Completely redesigned, more flexible trash system</li>
<li>Kicker with improved look and feel</li>
<li>KPDF now enables you to select, copy &amp; paste text and images from PDFs, along with many other improvements</li>
<li>Kontact supports now various groupware servers, including eGroupware, 
        GroupWise, Kolab, OpenGroupware.org and SLOX</li>
<li>Kopete supports Novell Groupwise and Lotus Sametime and gets integrated 
        into Kontact</li>
<li>DBUS/HAL support allows to keep dynamic device icons in media:/ and on the desktop in sync with the state of all devices</li>
<li>KHTML has improved standard support and now close to full support for CSS 2.1 and the CSS 3 Selectors module</li>
<li>Better synchronization between 2 PCs</li>
<li>A new high contrast style and a complete monochrome icon set</li>
<li>An icon effect to paint all icons in two chosen colors, converting third party application icons into high contrast monochrome icons</li>
<li>Akregator allows you to read news from your favourite RSS-enabled websites in one application</li>
<li>Juk has now an album cover management via Google Image Search</li>
<li>KMail now stores passwords securely with KWallet</li>
<li>SVG files can now be used as wallpapers</li>
<li>KHTML plug-ins are now configurable, so the user can selectively disable 
        ones that are not used. This does not include Netscape-style plug-ins.
        Netscape plug-in in CPU usage can be manually lowered, and plug-ins are more 
        stable.</li>
<li>more than 6,500 bugs have been fixed</li>
<li>more than 1,700 wishes have been fullfilled</li>
<li>more than 80,000 contributions with several million lines of code and documentation added or changed</li>
</ul>

<p>
For a more detailed list of improvements since the KDE 3.3 release, please refer to the
<a href="http://developer.kde.org/development-versions/kde-3.4-features.html">KDE 3.4 Feature Plan</a>.
</p>

<h2>Getting KDE 3.4</h2>

<p>
Full information on how to download and install KDE 3.4 is available on 
our official website at <a href="http://www.kde.org/info/3.4.php">http://www.kde.org/info</a>. Being free and 
open source software, it is available for download at no cost. If you 
use a major Linux distribution then precompiled packages may be 
available from your distributions website or from 
<a href="http://download.kde.org/">http://download.kde.org</a>. The source code can also be downloaded from 
there. Both <a href="http://www.arklinux.org/">ArkLinux</a> and <a href="http://www.ubuntulinux.org/wiki/Kubuntu/">Kubuntu</a> have targeted a release including KDE 3.4 right after the 3.4 release. If you prefer to build KDE from source you should consider using 
<a href="http://developer.kde.org/build/konstruct/">Konstruct</a>, a tool that 
automatically downloads, configures and builds KDE 3.4 for you.
</p>

<p>
Many more KDE applications are freely available from <a href="http://www.kde-apps.org/">KDE-Apps.org</a> and different look and feel improvents can be downloaded from <a href="http://www.kde-look.org/">KDE-Look.org</a>.
</p>

<h2>Supporting KDE</h2>

<p>
KDE is an open source project that exists and grows only because of the 
help of many volunteers that donate their time and effort. KDE 
is always looking for new volunteers and contributions, whether its 
help with coding, bug fixing or reporting, writing documentation, 
translations, promotion, money, etc. All contributions are gratefully 
appreciated and eagerly accepted. Please read through the <a href="http://www.kde.org/community/donations/">Supporting 
KDE page</a> for further information. <br />
We look forward to hearing from you soon!
</p>

<h2>About KDE</h2>

<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h2>Press Contacts</h2>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
