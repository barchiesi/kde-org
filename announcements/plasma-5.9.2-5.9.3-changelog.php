<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.9.3 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.9.3";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-5.9.3.php">Plasma 5.9.3</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Add .arcconfig. <a href='https://commits.kde.org/bluedevil/0a3d87673fa872306bc680844ffbd6c05be9659e'>Commit.</a> </li>
<li>ReceiveFileJob: Don't cancel the request right after accepting it. <a href='https://commits.kde.org/bluedevil/def41978a6330097ddb191328185fa79a1d63df4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376773'>#376773</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4744'>D4744</a></li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Use QPalette::Shadow instead of QPalette::Text to darken inactive tabs. <a href='https://commits.kde.org/breeze/13c049c6fba95cf58de9bcc3f61df56153b7c54f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373088'>#373088</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fix how we deal with margins in the ApplicationDelegate. <a href='https://commits.kde.org/discover/febf4fb968f3b138b5c34ee7d8cae04069961ec0'>Commit.</a> </li>
<li>Don't stretch icons. <a href='https://commits.kde.org/discover/863c1b0dee0ed674525b5c412fc1e30d5456db50'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Fix QCommandLineParser usage WRT windowclass parameter. <a href='https://commits.kde.org/kde-cli-tools/c3aa100b814197fa19b271db0dcfe7d9b962c510'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376662'>#376662</a></li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Properly retrieve all boolean properties. <a href='https://commits.kde.org/kde-gtk-config/557e20b961fe478ecf7c8a0192830eaf0894098b'>Commit.</a> </li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Fix build with -fno-operator-names. <a href='https://commits.kde.org/khotkeys/bf57c786f01feee3737545c9c08882a2057ffbb8'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Fix crash in Screen Locker KCM on teardown. <a href='https://commits.kde.org/kscreenlocker/2601fa10725284feb5bcc78006250c0aa92f92dd'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4535'>D4535</a></li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Fix git ssh password detection. <a href='https://commits.kde.org/ksshaskpass/408c284f6a35694f7e6be76c3416b80f3ef7fdba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376228'>#376228</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4540'>D4540</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Fix array allocation for non-continuous cpu numbers. <a href='https://commits.kde.org/ksysguard/e4c58c2f6ef74bad065f7327c19c02a3dac88883'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376537'>#376537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4640'>D4640</a></li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Fix off by one in the socket filename. <a href='https://commits.kde.org/kwallet-pam/fa1fbc2568fdfcf3f6ffa7370c3d26a3a47b9a5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129955'>#129955</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Move the view at the correct index at startup. <a href='https://commits.kde.org/kwin/6673f713bb97fcaebc8706022a483441dbcdb173'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4703'>D4703</a></li>
<li>We need Breeze 5.9. <a href='https://commits.kde.org/kwin/32ec309e183919df4fa7f6b7b14c48cd978e9bf4'>Commit.</a> </li>
<li>Find minimum required Breeze version instead of current version. <a href='https://commits.kde.org/kwin/cb481b492271860586c69298395e2ba118239406'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4644'>D4644</a></li>
<li>Avoid a crash on Kwin decoration KCM teardown. <a href='https://commits.kde.org/kwin/70d2fb2378d636ef6d052da08417b27c99182fb0'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4533'>D4533</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix switching categories via the filter listview on touchscreens. <a href='https://commits.kde.org/plasma-desktop/d85dd61627bfa2e2e906fddb16d7a9bee71375f8'>Commit.</a> </li>
<li>[tooltips] Limit tooltip title to one line. <a href='https://commits.kde.org/plasma-desktop/04c922dac118f0c58d361f547511b4869ab569cf'>Commit.</a> </li>
<li>Add an option to limit the number of text lines in task buttons. <a href='https://commits.kde.org/plasma-desktop/0a83f161140bc8b83f15f4c77baea66bd6233ae3'>Commit.</a> </li>
<li>Make the hover state optional. <a href='https://commits.kde.org/plasma-desktop/c88e4bff746c72d96d1bd69b49e0d83e5ceaa1bd'>Commit.</a> </li>
<li>Fix default fixed font in fonts kcm. <a href='https://commits.kde.org/plasma-desktop/d02de0db36a35cc2e66ff91b8f5796962fa4e04e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4794'>D4794</a></li>
<li>Avoid capturing MenuItem instead determine checked state from toggled signal. <a href='https://commits.kde.org/plasma-desktop/e5df3ded85c94f0a33afe12b18e6afad96f12639'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376826'>#376826</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4757'>D4757</a></li>
<li>Set root.dragging to false during reset. <a href='https://commits.kde.org/plasma-desktop/2695842c02bb2ca8e3af9bcdc0916dc1d6d108cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376633'>#376633</a></li>
<li>Use icon center for vertical hitscan on drop. <a href='https://commits.kde.org/plasma-desktop/c9a7741f2b8082f025b5eb2e53c3c489a95e1da8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4683'>D4683</a></li>
<li>Don't pin during reordering by DND. <a href='https://commits.kde.org/plasma-desktop/d1f2b1e5241bbb42f5d797699051a9875ad7200a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376572'>#376572</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Don't assume initialized if there are pending dbus replies. <a href='https://commits.kde.org/plasma-nm/2c70ac2a6eddc9f87eac7e4e3a8c68f558d7dd0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376739'>#376739</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4705'>D4705</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>StreamRestore: Cache values in writeChanges until receiving change from pa. <a href='https://commits.kde.org/plasma-pa/ba844b3e0e36c414c078c9aff9cebf046b02a6de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4674'>D4674</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Remove deprecated --shut-up start flag. <a href='https://commits.kde.org/plasma-workspace/dc21a1996edb98916224fb9dcf03ebe50d4b2990'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3344'>D3344</a></li>
<li>Fix another regression with rearranging launchers in an activities world. <a href='https://commits.kde.org/plasma-workspace/16680da7ab0e06c67753c2dbd5229180c5013ee8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4749'>D4749</a></li>
<li>[Media Controller] Round button size for play button. <a href='https://commits.kde.org/plasma-workspace/beb47e5e65a19a60df12f7ab98c03f5882d4c292'>Commit.</a> </li>
<li>Try harder to make the selected wallpaper visible. <a href='https://commits.kde.org/plasma-workspace/0eab04a3b549f0be1dfbab9e304d50bb247132a9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4648'>D4648</a></li>
<li>[Logout Screen] Show suspend button only if supported. <a href='https://commits.kde.org/plasma-workspace/8bc32846a5a41fa67c106045c43bb8c4af7e7e6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376601'>#376601</a></li>
<li>Map StartupWMClass for Chrome apps instead of RegExp'ing the name. <a href='https://commits.kde.org/plasma-workspace/513ad12b76a96d2919e1bbfae1c64ba9cfe72770'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376695'>#376695</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4680'>D4680</a></li>
<li>[kioslave/remote] Fix broken kded module. <a href='https://commits.kde.org/plasma-workspace/2a3b74df75123678577161e4ea6b9549246ffdf0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4573'>D4573</a></li>
<li>Port to QMultiHash. <a href='https://commits.kde.org/plasma-workspace/5632f4c38943b28829ce6013e3dd858b8d9e2a2f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4654'>D4654</a></li>
<li>Only remove + announce if leader change actually occured, avoid excessive loop. <a href='https://commits.kde.org/plasma-workspace/ee9f966dd55ce87f75376cae5b90549dace41ea8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4652'>D4652</a></li>
<li>Fix crash. <a href='https://commits.kde.org/plasma-workspace/b82d364536fab6bdeea0c9bc3f8eecfa988e7df3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4651'>D4651</a></li>
<li>[Notifications] Never manually hide() the NotificationPopup. <a href='https://commits.kde.org/plasma-workspace/6b833f1ca2bda69ed2c4984699f4e3ecdf2a5d4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4632'>D4632</a></li>
<li>Add comment laying out goals. <a href='https://commits.kde.org/plasma-workspace/62b803b45dbe14dc24f7593c1c2907d1e4db3d74'>Commit.</a> </li>
<li>Fix crash when switching activities. <a href='https://commits.kde.org/plasma-workspace/05826bd5ba25f6ed7be94ff8d760d6c8372f148c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376055'>#376055</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4631'>D4631</a></li>
<li>Tweak alphabetic (default) sort behavior. <a href='https://commits.kde.org/plasma-workspace/410086d06bf0b6195030fec5e9ac848cc50f50a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373698'>#373698</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4469'>D4469</a></li>
</ul>

	</main>

<?php
  	require('../aether/footer.php');
