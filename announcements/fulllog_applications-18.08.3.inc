<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix StorageJanitor SQL error in duplicate detection. <a href='http://commits.kde.org/akonadi/89727ad4da4da1173179f295152e9381273e2fbf'>Commit.</a> </li>
<li>Fix KOrg category color customization crash. <a href='http://commits.kde.org/akonadi/9d83f0752828942789535b1d0dc0ddc3c8b479bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398459'>#398459</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Disable Compress menu on remote URLs. <a href='http://commits.kde.org/ark/9ce568274b3e73b88ffece7611ee1beaca402ac1'>Commit.</a> </li>
<li>Show 'Extract' menu on archives without extension. <a href='http://commits.kde.org/ark/73cb4111124832891341436bd53ea1c35764f0cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399806'>#399806</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Disable --simple-prompt Sage parameter for now. <a href='http://commits.kde.org/cantor/ee1793fa468191196dd0a051d11c6732a2d6d6be'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Don't enable two-clicks renaming in single-click mode. <a href='http://commits.kde.org/dolphin/e26ee6455e408654de7e31a8ba84d44056d08bc2'>Commit.</a> </li>
<li>[KStandardItemListWidget] Pass icon state to overlay painter. <a href='http://commits.kde.org/dolphin/502a5c86feb0015c42f052d242c8115de320a38e'>Commit.</a> </li>
<li>Update hidden state correctly. <a href='http://commits.kde.org/dolphin/200e0d1f07428427913bd118467909d77c14a742'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399599'>#399599</a></li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Hide]</a></h3>
<ul id='uldragon' style='display: block'>
<li>Fix: user settings for gui not saved after program is closed. <a href='http://commits.kde.org/dragon/96c7f9314c4ef181c8a55f3e0aad0cfefb608e18'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376961'>#376961</a></li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Eventviews: properly fall back to color-generator if ColorAttribute is absent. <a href='http://commits.kde.org/eventviews/4dab2d3b545bb1d395f3e35653c5a48d49cd633c'>Commit.</a> </li>
<li>Eventviews: simplify/modernize resource color code. <a href='http://commits.kde.org/eventviews/46344f242854274e799a6dc19e2f0f2ed4e6b3da'>Commit.</a> </li>
<li>Fix color inconsistency for unset categories. <a href='http://commits.kde.org/eventviews/0b12580c8e848089a373dadae1c3e12b3ea0a531'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382185'>#382185</a></li>
<li>Factor out coloring from AgendaItem::paintEvent. <a href='http://commits.kde.org/eventviews/41a1d204064f787b07d40d425356420a74b81538'>Commit.</a> </li>
</ul>
<h3><a name='ffmpegthumbs' href='https://cgit.kde.org/ffmpegthumbs.git'>ffmpegthumbs</a> <a href='#ffmpegthumbs' onclick='toggle("ulffmpegthumbs", this)'>[Hide]</a></h3>
<ul id='ulffmpegthumbs' style='display: block'>
<li>Don't crash if initializeVideo fails. <a href='http://commits.kde.org/ffmpegthumbs/b8223de8e65de6de08f2e29a398f0922fa282bbb'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Fix keyboard editting of start/end times of VTODOs. <a href='http://commits.kde.org/incidenceeditor/10056772fd32c89dd5e881b495f597393b92f2ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391769'>#391769</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Systray: Make SHIFT+Mousewheel change the volume, not the song. <a href='http://commits.kde.org/juk/e2012e79359854f0ad73769bd02f092e44f40cf4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/250355'>#250355</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>MetaItemModel: Fix Qt assertion in adding rows. <a href='http://commits.kde.org/k3b/8709a3dcaef941db7467a94cfc77ca68542e2c45'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399753'>#399753</a></li>
</ul>
<h3><a name='kamera' href='https://cgit.kde.org/kamera.git'>kamera</a> <a href='#kamera' onclick='toggle("ulkamera", this)'>[Hide]</a></h3>
<ul id='ulkamera' style='display: block'>
<li>Set mime type for special text files. <a href='http://commits.kde.org/kamera/930e045e8bfdd0203923a4d451b97bb98123898a'>Commit.</a> </li>
<li>Fix opening special text files. <a href='http://commits.kde.org/kamera/8a057ef9b8651868eca27a21f13ae2c73ac40c66'>Commit.</a> </li>
<li>Add Messages.sh. <a href='http://commits.kde.org/kamera/467ad9f7e017922d9693101a661ac333c8dfba0b'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/kamera/e22151ed9e61693d4e1e6275745ed09804ffb687'>Commit.</a> </li>
<li>Add QCoreApplication. <a href='http://commits.kde.org/kamera/b17dd284090fd980504188dae748c4b3cec5be9c'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Open document before using it's checksum to load metainfos. <a href='http://commits.kde.org/kate/f0f301be4b29e1e1db429173d3c39f99d97d15c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384087'>#384087</a></li>
</ul>
<h3><a name='kdegraphics-thumbnailers' href='https://cgit.kde.org/kdegraphics-thumbnailers.git'>kdegraphics-thumbnailers</a> <a href='#kdegraphics-thumbnailers' onclick='toggle("ulkdegraphics-thumbnailers", this)'>[Hide]</a></h3>
<ul id='ulkdegraphics-thumbnailers' style='display: block'>
<li>Fixed crash when trying to generate a thumbnail for bogus .eps files. <a href='http://commits.kde.org/kdegraphics-thumbnailers/edbac9239ef8ad915b4b125ba4f22b45238be6f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399896'>#399896</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix wrong previous commit (missed changes, doesn't compile). <a href='http://commits.kde.org/kdenlive/d884e9477c11f45cb74057ee4c9d50bc5f3b93b8'>Commit.</a> </li>
<li>Fix finding MLT data in build-time specified path. <a href='http://commits.kde.org/kdenlive/021437c6730b0d820a0023e93df089db371ca918'>Commit.</a> </li>
<li>Fix play/pause on Windows. <a href='http://commits.kde.org/kdenlive/4587d29380fe0c7b5c3b0dd9611fba4e874bfb1e'>Commit.</a> </li>
<li>Try catching application initialization crashes. <a href='http://commits.kde.org/kdenlive/27c64c000d950904f11c7f0f073ffa16750832df'>Commit.</a> </li>
<li>Fix MinGW build script misses. <a href='http://commits.kde.org/kdenlive/579812c905dcc4c126d8450b461cd58000c59453'>Commit.</a> </li>
<li>Backport some Shotcut GLwidget updates. <a href='http://commits.kde.org/kdenlive/e44fbe218e1034d88626619fd102ed497a8cd44b'>Commit.</a> </li>
<li>Fix MinGW build. <a href='http://commits.kde.org/kdenlive/d8fae4d6419cb9548979ed04c6f4f3403553a93b'>Commit.</a> </li>
<li>Install doc files. <a href='http://commits.kde.org/kdenlive/a35bd53fef776adf72fe35195b169babf2679ba9'>Commit.</a> </li>
<li>Build scripts for Linux & Windows. <a href='http://commits.kde.org/kdenlive/23d6d018ee7527b7f959f22f763995551e50516c'>Commit.</a> </li>
<li>Backport packaging scripts. <a href='http://commits.kde.org/kdenlive/a23aca3b2d7a057b12995f22c1c8f78e94eabd0c'>Commit.</a> </li>
<li>Fix MinGW build. <a href='http://commits.kde.org/kdenlive/cde8ebe01aa07c3604c96afe93d608d7cc8a3031'>Commit.</a> </li>
<li>Backport fix for incorrect bin rename. <a href='http://commits.kde.org/kdenlive/3c6b2441cf56107dc0821de56bd087f1925f6ba7'>Commit.</a> See bug <a href='https://bugs.kde.org/368206'>#368206</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix Bug 399231 - Kmail uses a low resolution icon for the notification. <a href='http://commits.kde.org/kdepim-runtime/aaea8aaf718f0b05ad20a157f1a1b78226996a61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399231'>#399231</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Apply eexist workaround from listDir to stat as well. <a href='http://commits.kde.org/kio-extras/76402b7c61da193f32e3ff062338a2ddf6e23d4e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399699'>#399699</a></li>
<li>Avoid crash by not checking free space for smb://. <a href='http://commits.kde.org/kio-extras/767415da97a7fae3c5578b9a4fb17a7be041d8ae'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>GBool -> bool. <a href='http://commits.kde.org/kitinerary/22175dc433dad1b1411224d80d77f0f655219122'>Commit.</a> </li>
<li>Relax leading space offset detection for international tickets. <a href='http://commits.kde.org/kitinerary/747e0e6a2f6b51dbe76997d24b7f83f140b96cee'>Commit.</a> </li>
<li>Auto-detect leading space in compact/international DB tickets. <a href='http://commits.kde.org/kitinerary/0b3cd3edd7fa2f4c409a7931f4d638b364221979'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix show html format. <a href='http://commits.kde.org/kmail/df97f80eba9fa63e2053b89c03e5655d1046367f'>Commit.</a> </li>
<li>Fix save format. <a href='http://commits.kde.org/kmail/1dfe014556b8cc8370d397ec4744c86b324ac121'>Commit.</a> </li>
<li>Fix Bug 395711 - Since last Update kmail didn't load external images anymore. <a href='http://commits.kde.org/kmail/e17b0461c3c0bba4c317c26db6977dfab2195920'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395711'>#395711</a></li>
<li>Fix icon in notification. <a href='http://commits.kde.org/kmail/b5670f26239d0d67952ac7e9dd6ef63a471e834a'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Fix Bug 399999 - Notes tooltips do not use the correct encoding. <a href='http://commits.kde.org/knotes/04cde5088d99dfe146d2a5e6a527a28d3225a18d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399999'>#399999</a></li>
</ul>
<h3><a name='kolf' href='https://cgit.kde.org/kolf.git'>kolf</a> <a href='#kolf' onclick='toggle("ulkolf", this)'>[Hide]</a></h3>
<ul id='ulkolf' style='display: block'>
<li>Comment out phonon dependency. <a href='http://commits.kde.org/kolf/3feffd64a7b5a994e0a61b410c02b01535ae4076'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Korgac: repair the dumpAlarms() DBus method. <a href='http://commits.kde.org/korganizer/7d2d99a18cdf70df4607496aa2b5c4a58eda5111'>Commit.</a> </li>
<li>Korgac: hide "Dismiss all" button if there's only one reminder. <a href='http://commits.kde.org/korganizer/77fb04fb4310338ef0a6d09a1a09805bc08c0db6'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Make sure the card we're trying to automove is the top of the pile. <a href='http://commits.kde.org/kpat/925769b482e5c6c0b53d93ec68e007718db8e9d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399123'>#399123</a></li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Hide]</a></h3>
<ul id='ulktp-text-ui' style='display: block'>
<li>Fix automatic scrolling of chat window. <a href='http://commits.kde.org/ktp-text-ui/a5c4193fbd8bbea03f94b7983e2099c3e03fa8a6'>Commit.</a> </li>
</ul>
<h3><a name='kubrick' href='https://cgit.kde.org/kubrick.git'>kubrick</a> <a href='#kubrick' onclick='toggle("ulkubrick", this)'>[Hide]</a></h3>
<ul id='ulkubrick' style='display: block'>
<li>Add OARS to Kubrick. <a href='http://commits.kde.org/kubrick/b8e9c93836ec5be54e650555ad8bc9615698199a'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix show messagebox. <a href='http://commits.kde.org/libksieve/7fe092249b285d87df8999ac3e94ef0cbca06684'>Commit.</a> </li>
<li>Fix bug when we click on ok. When script has error we closed it => we. <a href='http://commits.kde.org/libksieve/cc68013c233576d2791f71a29e5d91e8a4f0c69a'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix Bug 377708 - KMail doesn't remember to render in HTML. <a href='http://commits.kde.org/messagelib/cd4a66e69b3ef1f25eea3d1bd19c847d8f452fbc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377708'>#377708</a></li>
<li>Use RichText here. <a href='http://commits.kde.org/messagelib/156288a9c5bf044c1204b7da696d250d868ccb20'>Commit.</a> </li>
<li>Fix Bug 396398 - KMail 5.8.2 displays HTML formatting on top of message. <a href='http://commits.kde.org/messagelib/33991513a4fb0e55feb89e81ab7e8f5306714191'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396398'>#396398</a></li>
<li>Check QtWebEngine version instead of Qt's. <a href='http://commits.kde.org/messagelib/569c3780a8f18830d7be24b7a7d40b755a2dbc60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397349'>#397349</a>. See bug <a href='https://bugs.kde.org/388440'>#388440</a></li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Add three autotests for part saving. <a href='http://commits.kde.org/okular/05462e2670988c2e83161f3fa1bd5e034cf50283'>Commit.</a> </li>
<li>Fix saving to files that don't exist. <a href='http://commits.kde.org/okular/8f1b0bda2244223d898138cf3c82d34cc0e12983'>Commit.</a> </li>
<li>Resolve symlinks before saving so we don't "break" them. <a href='http://commits.kde.org/okular/99fe8fa6cfa7741351844f70fb86ff297378e4c2'>Commit.</a> </li>
<li>Fix crash if processing a link closes the document. <a href='http://commits.kde.org/okular/27197b5f76be367acd2ae7ca030857842fb663d8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400104'>#400104</a></li>
</ul>
<h3><a name='palapeli' href='https://cgit.kde.org/palapeli.git'>palapeli</a> <a href='#palapeli' onclick='toggle("ulpalapeli", this)'>[Hide]</a></h3>
<ul id='ulpalapeli' style='display: block'>
<li>Fix crash on startup. <a href='http://commits.kde.org/palapeli/ff1e5bc8415167801c1b240fe7384d858e84aa5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398416'>#398416</a></li>
<li>Fix leak of the graphicsscene on the Puzzle preview. <a href='http://commits.kde.org/palapeli/483d3f8b9428878c5bfd3be3f76a4d41a367c6a5'>Commit.</a> </li>
<li>Fix small memory leak. <a href='http://commits.kde.org/palapeli/38f21198899c0fccc9246c7419e0b1777342ca97'>Commit.</a> </li>
</ul>
<h3><a name='pim-sieve-editor' href='https://cgit.kde.org/pim-sieve-editor.git'>pim-sieve-editor</a> <a href='#pim-sieve-editor' onclick='toggle("ulpim-sieve-editor", this)'>[Hide]</a></h3>
<ul id='ulpim-sieve-editor' style='display: block'>
<li>Get result when we close tab. <a href='http://commits.kde.org/pim-sieve-editor/01af70061e191cb4476d87c28a2fcfded19cc40e'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fixup of commit af72e05. <a href='http://commits.kde.org/umbrello/abb8b3721ba400ab423489295e8e6662832aa288'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400501'>#400501</a></li>
<li>Fixes broken docbook URLs when creating xhtml. <a href='http://commits.kde.org/umbrello/e418f62bf690b5e7d98c4e25a43f432d9d905062'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400657'>#400657</a></li>
<li>Make it possible to run docbook export from build dir. <a href='http://commits.kde.org/umbrello/552631f9c1902ca0ef3c4b6671eab3e985e7eef2'>Commit.</a> See bug <a href='https://bugs.kde.org/400657'>#400657</a></li>
<li>Fix 'Umbrello fails to start due to ASSERT failure in QVector<T>::at "index out of range"'. <a href='http://commits.kde.org/umbrello/af72e059f728d70e43ac3ea03653865585818e66'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400501'>#400501</a></li>
</ul>
