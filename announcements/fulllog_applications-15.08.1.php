<?php
  $version = "15.08.1";
  $page_title = "KDE Applications ".$version." Full Log Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
<?php print "This is the automated full changelog for <a href='announce-applications-".$version.".php'>KDE Applications ".$version."</a> from the git repositories." ?>
</p>
<p>
Click on [Show] to show the commits for a given repository
</p>

<?php
include "fulllog_applications-$version.inc"
?>

<?php
  include "footer.inc";
?>
