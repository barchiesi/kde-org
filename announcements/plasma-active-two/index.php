<?php
  $page_title = "Plasma Active Two Boosts Performance, New Features";
  $site_root = "../";
  include "header.inc";
?>


<p align="justify">Mobile devices that adapt to who you are, reflecting what you are doing when you are doing it. This concept is at the heart of the <a href="http://plasma-active.org">Plasma Active</a> user experience. Plasma Active One was released in October 2011, providing early adopters the first opportunity to experience Activities on a tablet. Since then, the design and development team behind this open source touch interface has been hard at work on an update. The fruits of their labor were released today, December 14, 2011 as Plasma Active Two.</p>

<p align="justify">A video introduction to Plasma Active Two, which also appears on both the live and installable device images, can be viewed below or <a href="http://share.basyskom.com/contour/UIDesign/RC2_plasma_active_two-with_Audio.ogv">downloaded</a>.</p>

<div align="center"><iframe style="margin-top: 4em" width="400" height="240" src="http://www.youtube.com/embed/UPkYyDiuGyc" frameborder="0" allowfullscreen></iframe></div>

<h3>Improved User Experience</h3>
<p align="justify">When Plasma Active One was released, people began installing and using Plasma Active on a variety of tablet devices, resulting in valuable user feedback. Tablets were also provided to testers in controlled environments where usage was observed by the Plasma Active design team. This information about real-world usage enabled the team to improve the end-user experience significantly over the past two months.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActivePeekNLaunch.png"><img src="images/thumbs/PlasmaActivePeekNLaunch.png" align="center" width="600" alt="Plasma Active's Peek &amp; Launch" title="Plasma Active's Peek &amp; Launch" /></a>
<br />
<em>Plasma Active's Peek &amp; Launch</em>
</div>

<p align="justify">Some of the changes, while small, make a big difference―for instance, the top bar is much easier to drag down to expose the Window Peek and App Launch areas. While a small thing, it allows this interaction pattern to fade into the background and feel more natural. The presentation of documents, applications, widgets, contacts, images and other resources were harmonized, giving a more pleasant and consistent experience to Plasma Active. The Activities view was improved with the addition of vertical resizing and refinements to the Activity Switcher. Now, whether you have just a few Activities or dozens of them with hundreds of items associated with each, interacting with your Activities is easier and smoother.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_Settings.png"><img src="images/thumbs/PlasmaActive_Settings.png" align="center" width="600" alt="Plasma Active's touch-friendly settings app" title="Plasma Active's touch-friendly settings app" /></a>
<br />
<em>Plasma Active's touch-friendly settings app</em>
</div>

<p align="justify">Many of the default settings were improved based on user feedback and testing. Font settings and power management defaults were tweaked to provide a better "out-of-the-box" experience. A new touch-friendly settings application was introduced to make adjusting date, time and web browsing preferences easy and intuitive. Many included applications, such as the Okular document and eBook reader and Kate text editor, join the web browser and image viewer in being enabled for <a href="http://www.notmart.org/index.php/Software/Share,_Like_and_Connect">Share-Like-Connect</a>. This makes it easy for you to bookmark, rate, attach to Activities and send by email whatever content you are viewing.</p>

<p align="justify">The <a href="http://qt.nokia.com/qtquick/">QtQuick</a>-based user interface elements were refined for Plasma Active Two to make everyday tasks even easier. Improvements were made to the visual styling, and there are functional improvements to enhanced text editing and drop-down menu interaction. The web browser received a new Quick Jump panel integrated with the location bar to make visiting recent and favorited sites more convenient. There were also improvements to basic browser navigation tasks such as scrolling and zooming. Interaction was improved for nearly all of the included apps, from the log-out interface to the image browser to the on-screen keyboard. Even the screen locker was refreshed, and got additional security enhancements in the process.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_Browser.png"><img src="images/thumbs/PlasmaActive_Browser.png" align="center" width="600" alt="Plasma Active's touch-friendly web browser" title="Plasma Active's touch-friendly web browser" /></a>
<br />
<em>Plasma Active's touch-friendly web browser</em>
</div>


<h3>Better Performance</h3>
<p align="justify">In addition to tweaking presentation, the team paid attention to performance. This is important because Plasma Active is intended for smaller devices. Listing documents, applications, widgets and contacts were optimized for a more fluid experience. In some cases, they are more than 10 times faster than Plasma Active One. Several elements of the QtQuick-based interface were optimized for a much-needed performance boost. The on-screen keyboard is now more responsive, and boot times have been significantly reduced.</p>

<p align="justify">The ultimate goal is to bring a fully functioning and smooth Plasma Active experience to devices with as little as 256MB of RAM and sub-1GHz processors. Plasma Active Two represents a significant step in that direction. Early feedback from testers during development of Plasma Active Two has indicated that the improvements are noticeable. Developers will continue to focus on this goal in upcoming releases.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_ActivitySwitcher.png"><img src="images/thumbs/PlasmaActive_ActivitySwitcher.png" align="center" width="600" alt="Plasma Active's Activities organize your device" title="Activities organize your device" /></a>
<br />
<em>Plasma Active's Activities organize your device</em>
</div>

<h3>Introducing Recommendations</h3>
<p align="justify">Plasma Active Two focused primarily on improvements to One. However, Plasma Active Two has one significant new feature ― Recommendations. Plasma Active is now able to learn as you use your device. It uses that information to make recommendations as to what content, web sites and applications are likely to be related to what you are doing right now. This technology uses the power of the "semantic desktop" efforts from KDE Nepomuk to make your device a more valuable adviser and helper. Future releases will build on predictive power as well as the breadth of recommendations. All of this happens right on your own device. Out of respect for your privacy, no data is sent through the network. And no active Internet connection is required for it to work. This brand new technology is not available on any other mobile device.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_Recommendations1.png"><img src="images/thumbs/PlasmaActive_Recommendations1.png" align="center" width="600" alt="Activity-aware recommendations in Plasma Active" title="Activity-aware recommendations in Plasma Active" /></a>
<br />
<em>Activity-aware recommendations in Plasma Active</em>
</div>

<h3>Plasma Across the Device Spectrum</h3>
<p align="justify">Plasma Active development is currently focused on tablets. Future development will expand this scope and might include set-top boxes and smartphones, both of which individuals within the Plasma Active community have begun targeting. With its flexibility and modern approach to the user interface, the types of devices that can use Plasma Active are essentially unbounded, save by the imagination ― ours and our partners.</p>

<p align="justify">As a member of the <a href="https://www.kde.org/workspaces/">Plasma Workspaces</a> family, Plasma Active development also benefits both the Desktop and Netbook offerings. Due to the shared engineering between all Plasma Workspaces, improved performance and feature enhancements in one interface often end up benefiting them all. The various Plasma Workspaces work well in combination, so Plasma Active is a perfect companion to your Plasma Desktop and Plasma Netbook systems.</p>

<h3>Getting Plasma Active</h3>
<p align="justify">Plasma Active Two greatly extends its device reach over Plasma Active One, which was only available for a small number of Intel-based devices. In the last two months, Plasma Active has been brought up on over a dozen different kinds of devices using both the <a href="http://merproject.org">Mer</a> and <a href="http://www.open-slx.de/">Open-SLX Balsam</a> operating systems. With the power of the <a href="http://openbuildservice.org/">Open Build Service</a>, the Plasma Active team has been able to power devices as diverse as the <a href="http://www.exopc.com/">ExoPC</a>, <a href="http://beagleboard.org/">BeagleBoard</a>, <a href="http://dot.kde.org/2011/11/30/plasma-active-archos-g9-tablet">Archos G9 tablet</a> and <a href="http://dot.kde.org/2011/10/24/plasma-active-arm">NVidia Tegra 2 systems</a>. Plasma Active is now available for and supported on both Intel and ARM architectures.</p>

<p align="justify">Information on <a href="http://community.kde.org/Plasma/Active/Installation">installing Plasma Active 2 on a compatible device is available on the Plasma Active wiki</a>, with images available from both the Mer Project and Open-SLX.</p>

<p align="justify">Given the effort required to install Plasma Active Two on a device yourself, many are looking forward to being able to purchase a device with Plasma Active pre-installed on it. With the maturation of Plasma Active and its growing device coverage, the wait is coming to an end. At least two announcements for the availability of Plasma Active Two tablets are set to be made within a month, which will increase the size of the Plasma Active audience and community.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_Introduction.png"><img src="images/thumbs/PlasmaActive_Introduction.png" align="center" width="600" alt="Plasma Active's Welcome Activity" title="Plasma Active's Welcome Activity" /></a>
<br />
<em>Plasma Active's Welcome Activity</em>
</div>

<h3>Getting Involved</h3>
<p align="justify">Plasma Active is unique in the device landscape as it is fully open and transparent in design, development and deployment. It is the result of a partnership between companies and community. Opportunities abound for people and organizations to participate. Plasma Active Three development begins in January and will focus on features. It will have a longer development cycle compared to Plasma Active Two, and is expected to be released in Summer 2012. Plasma Active Three will focus on areas such as enhanced privacy and security, mapping, app availability, document management work-flows and shared activities. Documentation, artwork, improvements to existing applications, and entertainment will be part of the project.

<p align="justify">We encourage you to join in these efforts. Help make the core Activities experience better, work on extending the device reach, or introduce new and exciting applications on top of Plasma Active. Please visit the <a href="http://plasma-active.org">Plasma Active</a> website, participate in our <a href="http://forum.kde.org/viewforum.php?f=211">online web forums</a>, join the <a href="https://mail.kde.org/mailman/listinfo/active">Active email list</a> and visit us in #active on irc.freenode.net. All efforts are welcome, both individual and corporate. The network of those involved with Plasma Active is growing significantly as Plasma Active progresses. Now is a terrific time to get involved and become part of that wave.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/PlasmaActive_ChristmasActivity.png"><img src="images/thumbs/PlasmaActive_ChristmasActivity.png" align="center" width="600" alt="Collect christmas ideas on your device" title="Collect christmas ideas on your device" /></a>
<br />
<em>Collect christmas ideas on your device</em>
</div>

<h3>Thanks and Acknowledgments</h3>
<p align="justify">Plasma Active would not be possible without the participation of all those involved, and there is a small mountain of personal achievement that is the engine behind Plasma Active. We rely on rock star community supporters such as Carl Symons; committed developers like Marco Martin, Ivan Čukić and Sebastian Kügler; designers with passion such as Fania Bremmer and Thomas Pfeiffer; release and packaging gurus such as Javier Llorente, Maurice de la Ferté and Karlheinz Hohm; hardware hackers such as Martin Brook; and many more. Behind and around so many of these individual efforts lie the critical input, involvement, stewardship and financial support of leaders such as Aaron Seigo, Eva Brucherseifer of <a href="http://basyskom.com/">basysKom</a> and Stefan Werden of <a href="http://www.open-slx.de/">open-slx</a>, founding members of Plasma Active and specialists in supporting Plasma Active on devices.</p>

<p align="justify">Of course, all of our efforts rest upon the 15 years of efforts from the KDE community as well as the brilliant Qt Frameworks libraries on top of the fantastic Linux stack. The Plasma Active team thanks all who have been involved up to this point, and looks forward to welcoming all those who will join these efforts in the future.</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
