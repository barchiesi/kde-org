<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.25.0");
  $site_root = "../";
  $release = '5.25.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
August 13, 2016. KDE today announces the release
of KDE Frameworks 5.25.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General");?></h3>

<ul>
<li><?php i18n("Qt &gt;= 5.5 is now required");?></li>
</ul>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Follow HTTP redirects");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("update mail- 16px icons to recognize the icons better");?></li>
<li><?php i18n("update mic and audio status icons to have the same layout and size");?></li>
<li><?php i18n("New System Settings app icon");?></li>
<li><?php i18n("add symbolic status gnome icons");?></li>
<li><?php i18n("add gnome 3 symbolic icon support");?></li>
<li><?php i18n("Added icons for Diaspora and Vector, see phabricator.kde.org/M59");?></li>
<li><?php i18n("New icons for Dolphin and Gwenview");?></li>
<li><?php i18n("weather icons are status icons no app icons");?></li>
<li><?php i18n("add some links to xliff thanks gnastyle");?></li>
<li><?php i18n("add kig icon");?></li>
<li><?php i18n("add mimetype icons, krdc icon, other app icons from gnastyle");?></li>
<li><?php i18n("add certificate mimetype icon (bug 365094)");?></li>
<li><?php i18n("update gimp icons thanks gnastyle (bug 354370)");?></li>
<li><?php i18n("globe action icon is now no linked file please use it in digikam");?></li>
<li><?php i18n("update labplot icons according to mail 13.07. from Alexander Semke");?></li>
<li><?php i18n("add app icons from gnastyle");?></li>
<li><?php i18n("add kruler icon from Yuri Fabirovsky");?></li>
<li><?php i18n("fix broken svg files thanks fuchs (bug 365035)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Fix inclusion when there's no Qt5");?></li>
<li><?php i18n("Add a fallback method for query_qmake() when there's no Qt5 installation");?></li>
<li><?php i18n("Make sure ECMGeneratePriFile.cmake behaves like the rest of ECM");?></li>
<li><?php i18n("Appstream data changed its preferred location");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("[KActivities-CLI] commands for starting and stopping an activity");?></li>
<li><?php i18n("[KActivities-CLI] setting and getting activity name, icon and description");?></li>
<li><?php i18n("Added a CLI application for controlling activities");?></li>
<li><?php i18n("Adding scripts to switch to previous and next activities");?></li>
<li><?php i18n("Method for inserting into QFlatSet now returns index along with the iterator (bug 365610)");?></li>
<li><?php i18n("Adding ZSH functions for stopping and deleting non-current activities");?></li>
<li><?php i18n("Added isCurrent property to KActivities::Info");?></li>
<li><?php i18n("Using constant iterators when searching for activity");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Many improvements to the output formatting");?></li>
<li><?php i18n("Mainpage.dox has now higher priority than README.md");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Handle multiple gzip streams (bug 232843)");?></li>
<li><?php i18n("Assume a directory is a directory, even if the permission bit is set wrong (bug 364071)");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("KBookmarkGroup::moveBookmark: fix return value when item is already at the right position");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add DeleteFile and RenameFile standard shortcut");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Add DeleteFile and RenameFile standard action");?></li>
<li><?php i18n("The config page has now scroll bars when needed (bug 362234)");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Install known licenses and find them at runtime (regression fix) (bug 353939)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Actually emit valueChanged");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Check for xattr during config step, otherwise the build might fail (if xattr.h is missing)");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Use klauncher dbus instead of KRun (bug 366415)");?></li>
<li><?php i18n("Launch jumplist actions via KGlobalAccel");?></li>
<li><?php i18n("KGlobalAccel: Fix deadlock on exit under Windows");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Support percentage unit in border radius");?></li>
<li><?php i18n("Removed prefixed version of background and border radius properties");?></li>
<li><?php i18n("Fixes in 4-values shorthand constructor function");?></li>
<li><?php i18n("Create string objects only if they will be used");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Greatly improve the performance of makeCacheKey, as it is a critical code path in icon lookup");?></li>
<li><?php i18n("KIconLoader: reduce number of lookups when doing fallbacks");?></li>
<li><?php i18n("KIconLoader: massive speed improvement for loading unavailable icons");?></li>
<li><?php i18n("Do not clear search line when switching category");?></li>
<li><?php i18n("KIconEngine: Fix QIcon::hasThemeIcon always returning true (bug 365130)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Adapt KInit to Mac OS X");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix KIO::linkAs() to work as advertised, i.e. fail if dest already exists");?></li>
<li><?php i18n("Fix KIO::put(\"file:///path\") to respect the umask (bug 359581)");?></li>
<li><?php i18n("Fix KIO::pasteActionText for null dest item and for empty URL");?></li>
<li><?php i18n("Add support for undoing symlink creation");?></li>
<li><?php i18n("GUI option to configure global MarkPartial for KIO slaves");?></li>
<li><?php i18n("Fix MaxCacheSize limited to 99 KiB");?></li>
<li><?php i18n("Add clipboard buttons to checksums tab");?></li>
<li><?php i18n("KNewFileMenu: fix copying template file from embedded resource (bug 359581)");?></li>
<li><?php i18n("KNewFileMenu: Fix creating link to application (bug 363673)");?></li>
<li><?php i18n("KNewFileMenu: Fix suggestion of new filename when file already exist in desktop");?></li>
<li><?php i18n("KNewFileMenu: ensure fileCreated() is emitted for app desktop files too");?></li>
<li><?php i18n("KNewFileMenu: fix creating symlinks with a relative target");?></li>
<li><?php i18n("KPropertiesDialog: simplify button box usage, fix behavior on Esc");?></li>
<li><?php i18n("KProtocolInfo: refill cache to find newly installed protocols");?></li>
<li><?php i18n("KIO::CopyJob: port to qCDebug (with its own area, since this can be quite verbose)");?></li>
<li><?php i18n("KPropertiesDialog: add Checksums tab");?></li>
<li><?php i18n("Clean url's path before initializing KUrlNavigator");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRearrangeColumnsProxyModel: fix assert in index(0, 0) on empty model");?></li>
<li><?php i18n("Fix KDescendantsProxyModel::setSourceModel() not clearing internal caches");?></li>
<li><?php i18n("KRecursiveFilterProxyModel: fix QSFPM corruption due to filtering out rowsRemoved signal (bug 349789)");?></li>
<li><?php i18n("KExtraColumnsProxyModel: implement hasChildren()");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't set parent of sublayout manually, silences warning");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Infer the ParentApp from the PackageStructure plugin");?></li>
<li><?php i18n("Let kpackagetool5 generate appstream information for kpackage components");?></li>
<li><?php i18n("Make it possible to load metadata.json file from kpackagetool5");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Remove unused KF5 dependencies");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("applications.menu: remove references to unused categories");?></li>
<li><?php i18n("Always update the Trader parser from yacc/lex sources");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Do not ask for overwriting a file twice with native dialogs");?></li>
<li><?php i18n("added FASTQ syntax");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[client] Use a QPointer for the enteredSurface in Pointer");?></li>
<li><?php i18n("Expose Geometry in PlasmaWindowModel");?></li>
<li><?php i18n("Add a geometry event to PlasmaWindow");?></li>
<li><?php i18n("[src/server] Verify that surface has a resource before sending pointer enter");?></li>
<li><?php i18n("Add support for xdg-shell");?></li>
<li><?php i18n("[server] Properly send a selection clear prior to keyboard focus enter");?></li>
<li><?php i18n("[server] Handle no XDG_RUNTIME_DIR situation more gracefully");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KCharSelect] Fix crash when searching with no present data file (bug 300521)");?></li>
<li><?php i18n("[KCharSelect] Handle characters outside BMP (bug 142625)");?></li>
<li><?php i18n("[KCharSelect] Update kcharselect-data to Unicode 9.0.0 (bug 336360)");?></li>
<li><?php i18n("KCollapsibleGroupBox: Stop animation in destructor if still running");?></li>
<li><?php i18n("Update to Breeze palette (sync from KColorScheme)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[xcb] Ensure the compositingChanged signal is emitted if NETEventFilter is recreated (bug 362531)");?></li>
<li><?php i18n("Add a convenience API to query the windowing system/platform used by Qt");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix minimum size hint (cut-off text) (bug 312667)");?></li>
<li><?php i18n("[KToggleToolBarAction] Honor action/options_show_toolbar restriction");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Default to WPA2-PSK and WPA2-EAP when getting security type from connection settings");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("add application-menu to oxygen (bug 365629)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Keep compatiable slot createApplet with Frameworks 5.24");?></li>
<li><?php i18n("Don't delete gl texture twice in thumbnail (bug 365946)");?></li>
<li><?php i18n("Add translation domain to wallpaper QML object");?></li>
<li><?php i18n("Don't manually delete applets");?></li>
<li><?php i18n("Add a kapptemplate for Plasma Wallpaper");?></li>
<li><?php i18n("Templates: register templates in own toplevel category \"Plasma/\"");?></li>
<li><?php i18n("Templates: Update techbase wiki links in READMEs");?></li>
<li><?php i18n("Define what Plasma packagestructures extend plasmashell");?></li>
<li><?php i18n("support a size for adding applets");?></li>
<li><?php i18n("Define Plasma PackageStructure as regular KPackage PackageStructure plugins");?></li>
<li><?php i18n("Fix: update wallpaper example Autumn's config.qml to QtQuick.Controls");?></li>
<li><?php i18n("Use KPackage to install Plasma Packages");?></li>
<li><?php i18n("If we pass a QIcon as an argument to IconItem::Source, use it");?></li>
<li><?php i18n("Add overlay support to Plasma IconItem");?></li>
<li><?php i18n("Add Qt::Dialog to default flags to make QXcbWindow::isTransient() happy (bug 366278)");?></li>
<li><?php i18n("[Breeze Plasma Theme] Add network-flightmode-on/off icons");?></li>
<li><?php i18n("Emit contextualActionsAboutToShow before showing the applet's contextualActions menu (bug 366294)");?></li>
<li><?php i18n("[TextField] Bind to TextField length instead of text");?></li>
<li><?php i18n("[Button Styles] Horizontally center in icon-only mode (bug 365947)");?></li>
<li><?php i18n("[Containment] Treat HiddenStatus as low status");?></li>
<li><?php i18n("Add kruler system tray icon from Yuri Fabirovsky");?></li>
<li><?php i18n("Fix the infamous 'dialogs show up on the Task Manager' bug once more");?></li>
<li><?php i18n("fix network wireless available icon with an ? emblem (bug 355490)");?></li>
<li><?php i18n("IconItem: Use better approach to disable animation when going from invisible to visible");?></li>
<li><?php i18n("Set Tooltip window type on ToolTipDialog through KWindowSystem API");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Always update the Predicate parser from yacc/lex sources");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("hunspell: Clean up code for searching for dictionaries, add XDG dirs (bug 361409)");?></li>
<li><?php i18n("Try to fix language filter usage of language detection a bit");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.25");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
