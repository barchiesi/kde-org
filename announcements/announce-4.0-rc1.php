<?php

  $page_title = "KDE 4.0 Release Candidate 1";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships First Release Candidate for Leading Free Software Desktop,
Codename "Calamity"
</h3>
<p align="justify">
  <strong>
    With the first release candidate, the KDE project would like to collect feedback to
ensure the quality of KDE 4.0.
  </strong>
</p>
<p align="justify">
November 20, 2007 (The INTERNET).
</p>

<p>
The KDE Community is happy to announce the immediate availability of <a
href="http://www.kde.org/announcements/announce-4.0-rc1.php">the
first release candidate for KDE 4.0</a>.  This release candidate marks that 
the majority of the components of KDE 4.0 are now approaching release quality.
</p>
<p>
While the final bits of <a href="http://plasma.kde.org">Plasma</a>, the brand 
new desktop shell and panel in KDE 4, are falling into place, the KDE
community decided to publish a first release candidate for the KDE 4.0 
Desktop.  Release Candidate 1 is the first preview of KDE 4.0 which is 
suitable for general use and discovering the improvements that have taken 
place all over the KDE codebase.
</p>
<p>
At the same time, the KDE team releases the final version of the <strong>KDE Development
Platform</strong>, which provides the needed libraries and applications the 
KDE Desktop is based on. The KDE Development Platform, comprising 
the basis for developing KDE applications, is frozen and is now of release 
quality. The sourcecode for the KDE Development Platform can be found in the 
"stable/" subdir on KDE's FTP server and mirrors.
<p />

Building on this, the majority of applications included in KDE 4.0 are now 
usable for day to day use. The KDE Release Team has recently underlined this by 
<a href="http://lists.kde.org/?l=kde-release-team&m=119521408516846&w=2">calling 
on the community</a> to participate in reporting bugs during the time 
remaining before the release of KDE 4.0 in December.<br />
Meanwhile, preparations for the KDE 4.0 release events are taking place, with 
the main event taking place in Mountain View, California in the USA in January 
2008. Make sure you don't miss it!

<p>
The first release candidate incorporates many improvements from previous Beta and alpha
releases: <a href="http://www.kde.org/announcements/announce-4.0-beta4.php">Beta
4</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta3.php">Beta
3</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta2.php">Beta
2</a>, <a href="http://www.kde.org/announcements/announce-4.0-beta1.php">Beta
1</a>, <a href="http://www.kde.org/announcements/announce-4.0-alpha2.php">Alpha
2</a> and <a
href="http://www.kde.org/announcements/visualguide-4.0-alpha1.php">Alpha 1</a>.
</p>
<h2>RC 1</h2>
<p>
With the first release candidate, the KDE developers hope to collect comments 
and bug reports from the wider KDE community. With their help, we hope to solve the most
pressing problems with the current KDE 4 codebase to ensure the final 4.0
release is usable. We would like to encourage anyone who is willing and able to
spend some time on testing to find and <a href="http://bugs.kde.org">report</a>
problems to the KDE developers. It's advisable to have a current snapshot of the
codebase handy. That makes trying things easier, it also helps the process by
not having  to hunt down bugs that have already been fixed.
</p>
<p>
You can find <a
href="http://techbase.kde.org/Contribute/Bugsquad/How_to_create_useful_crash_reports">some documentation</a> on the pages of our
<a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> and <a
href="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">in other
places</a>. There will be weekly <a
href="http://aseigo.blogspot.com/2007/10/kde4-krush-days-saturday.html">KDE 4
Krush Days on Saturdays</a> and everyone is invited to help out. Having a recent
SVN checkout (follow instructions on <a
href="http://techbase.kde.org">TechBase</a>) is great, but using the regularly
updated <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE KDE 4 Live CD</a> 
or the <a href="http://pkg-kde.alioth.debian.org/kde4livecd.html">Debian KDE4 Beta4 Live CD</a>, 
while very easy, is also a big help to us.
</p>


<h4>Get it, run it, test it...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0-rc1 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/3.96.php">KDE 4.0-rc1 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>

<br>
<i>Compiled by Sebastian K&uuml;gler with extensive help from the KDE community</i>

<h2>About KDE 4</h2>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks
to fulfill the need for a powerful yet easy to use desktop for UNIX and LINUX
workstations. The aim of the KDE project for the 4.0 release is to put the
foundations in place for future innovations on the Free Desktop. The many newly
introduced technologies incorporated in the KDE libraries will make it easier
for developers to add rich functionality to their applications, combining and
connecting different components in any way they want.
</p>

<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
