<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Plasma 5.19: A more Polished Plasma",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.19.0'; // for i18n
    $version = "5.19.0";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">
  <?php include "./announce-i18n-bar.inc"; ?>

  
  <h1 class="mt-2"><?php print i18n_var("Plasma %1", "5.19")?></h1>
  <div class="laptop-with-overlay d-block my-3 mx-auto" style="max-width: 800px">
    <img class="laptop img-fluid mt-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
    <div class="laptop-overlay">
      <video controls class="w-100 h-100 mt-3" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Thumbnail.png">
        <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.webm"
            type="video/webm">
        <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.mp4"
            type="video/mp4">
      </video>
    </div>
  </div>

  <p><?php print i18n("Tuesday, 9 June 2020.") ?></p>

  <p><?php i18n("Plasma 5.19 is out! If we gave alliterative names to Plasma releases, this one could be \"Polished Plasma\". The effort developers have put into squashing bugs and removing annoying papercuts has been immense.")?></p>

  <p><?php i18n("In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.")?></p>

  <p><?php i18n("Read on to discover all the new features and improvements of Plasma 5.19…"); ?></p>

  <h2 id="desktop"><?php i18n("Plasma Desktop and Widgets");?></h2>

  <p><?php i18n("The first change you will see is the new wallpaper. The eye-catching <I>Flow</I> background designed by Sandra Smukaste brings a dash of light and color to your desktop. Of course, you can choose something different by right-clicking on your desktop and clicking on \"Configure desktop...\" from the pop-up menu. Notice how you can now see the name of the creators of each desktop wallpaper when you go to pick one. Another personalization detail you will probably appreciate is that there is a completely new collection of photographic avatars to choose from when setting up your user."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/user-avatars.png" class="border-0 img-fluid" width="500"  alt="<?php i18n("Completely New User Avatars");?>" />
    <figcaption><?php i18n("Completely New User Avatars");?></figcaption>
  </figure>
  
  <p><?php echo(str_replace('. However,', ', however.', i18n_var("Some of the changes are more subtle. However, the panel spacer, for example, the invisible element that helps place components on the panel, can now automatically center widgets, and Plasma 5.19 also incorporates a consistent design and header area for system tray applets as well as notifications. These are things you may not immediately notice, but contribute to making the design of the desktop subliminally more visually attractive. In a similar vein, GTK3 apps immediately apply a newly selected color scheme, GTK2 apps no longer have broken colors and we have increased the default fixed-width font size from 9 to 10, making all the text easier to read."))); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/system-tray-applets.png" class="border-0 img-fluid" width="600"  alt="<?php i18n("Consistent System Tray Applets");?>" />
    <figcaption><?php i18n("Consistent System Tray Applets");?></figcaption>
  </figure>  
  
  <p><?php i18n("We have refreshed the look of the media playback applet in the System Tray and, related to that, you now have more control over the visibility of the volume controls. Task Manager tooltips have also been overhauled, the System monitor widgets have all been rewritten from scratch and Sticky notes get several usability improvements."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/sensors.png" class="border-0 img-fluid" width="700"  alt="<?php i18n("Rewritten System Monitor Widgets");?>" />
    <figcaption><?php i18n("Rewritten System Monitor Widgets");?></figcaption>
  </figure>

<!--
  <figure class="text-center">
    <img src="plasma-5.19/multi-device-switching-ui.png" class="border-0 img-fluid" width="400"  alt="<?php i18n("More consistent appearance for switching the current audio device");?>" />
  </figure>
-->

  <h2 id="systemsettings"><?php i18n("System Settings");?></h2>

  <p><?php i18n("Users that want more control over the file indexing process will appreciate the new configurable file indexing options for individual directories. You can also completely disable indexing for hidden files if you so wish. If you use Wayland, you will also appreciate the new option that lets you configure the mouse and touchpad scroll speed."); ?></p>

  <p><?php i18n("Intent on improving not only features, but also the look and feel of Plasma, the Default Applications, Online Accounts, Global Shortcuts, KWin Rules and Background Services settings pages have all been overhauled and we have added lots of small improvements to the font configuration."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/redesigned-kcms.png" class="border-0 img-fluid" width="900"  alt="<?php i18n("Redesigned Settings Pages");?>" />
    <figcaption><?php i18n("Redesigned Settings Pages");?></figcaption>
  </figure>

  <p><?php i18n("Increasing usability throughout is one of Plasma 5.19's main objectives, so to make reaching the section you need easier, when you launch System Settings modules from within KRunner or the application launcher, the complete System Settings app launches on the page you asked for."); ?></p>
  <figure class="text-center">
    <div class="embed-responsive embed-responsive-16by9">
    <video width="700" height="504" controls>
      <source src="plasma-5.19/krunner.webm" type="video/webm" />
    </video>
    </div>
    <figcaption><?php i18n("Full System Settings App Is Now Launching");?></figcaption>
  </figure>

  <p><?php i18n("Other small changes that make your life better are that the Display settings page now shows the aspect ratio for each available screen resolution and you now have a more granular control over the animation speed for your desktop effects."); ?></p>

  <h2 id="kinfocenter"><?php i18n("Info Center");?></h2>

  <p><?php i18n("Consistency is an important aspect of a well-designed desktop, and that is why the Info Center application has been redesigned with a look and feel that is consistent with the System Settings. The Info Center also gains a feature that lets you see information about your graphics hardware."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/kinfocenter.png" class="border-0 img-fluid" width="600"  alt="<?php i18n("Redesigned Info Center");?>" />
    <figcaption><?php i18n("Redesigned Info Center");?></figcaption>
  </figure>

  <h2 id="kwin"><?php i18n("KWin Window Manager");?></h2>

  <p><?php i18n("The KWin window manager includes subsurface clipping. This feature greatly reduces the flickering in many apps, making them less of a strain on the eyes. In similar news, the icons in titlebars have also been recolored to fit the color scheme making them much easier to see."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/recoloured-icons.png" class="border-0 img-fluid" width="350" alt="<?php i18n("Icon Recoloring in the Titlebar");?>" />
    <figcaption><?php i18n("Icon Recoloring in the Titlebar");?></figcaption>
  </figure>

  <p><?php i18n("A nice new feature in Wayland is that screen rotation for tablets and convertible laptops now works."); ?></p>

  <h2 id="discover"><?php i18n("Discover");?></h2>

  <p><?php i18n("Discover, the application that lets you add, remove and update software, has a new feature that makes removing Flatpak repositories much easier. Discover also now displays the app version. This is helpful for example when there is more than one version of the application you are looking for. You can choose the version that contains the features you need; or a version that is older, but more stable; or the one that works better with your set up."); ?></p>
  <figure class="text-center">
    <img src="plasma-5.19/discover.png" class="border-0 img-fluid" width="500"  alt="<?php i18n("Flatpak Repository Removal in Discover");?>" />
    <figcaption><?php i18n("Flatpak Repository Removal in Discover");?></figcaption>
  </figure>

  <h2 id="ksysguard"><?php i18n("KSysGuard");?></h2>
  <p><?php i18n("The version of KSysGuard shipped with Plasma 5.19 supports systems with more than 12 CPU cores."); ?></p>

  <a href="plasma-5.18.5-5.19.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.19"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
