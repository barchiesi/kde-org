<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.5.0 complete changelog");
$site_root = "../";
$release = 'plasma-5.5.0';
include "header.inc";
?>
<p><a href="plasma-5.5.0.php">Plasma 5.5.0</a> complete changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Wizard: Set minimum size of window to ensure entire PIN is visible. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=ea03996f34d9d22083bdbe5fcdc11910e50253b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355798'>#355798</a>. Code review <a href='https://git.reviewboard.kde.org/r/126160'>#126160</a></li>
<li>Cleanups in applet. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=e1dc0081161633bb7ffbb756066f11aadfdf05a7'>Commit.</a> </li>
<li>Applet: Adjust icon size in switch button. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=1b67ebce7d2433cb1d6aad3b49b6acdb02d8cfca'>Commit.</a> </li>
<li>Applet: Align device details labels to center. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=eb8951d5cc3ebdaccd361e3bd2da4d62ac3a9ef2'>Commit.</a> </li>
<li>KCM: Set version in KAboutData to BLUEDEVIL_VERSION. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=d7d672da982869ac1f1ce10e69576b1a2cd5c086'>Commit.</a> </li>
<li>KCM Devices: New UI with two columns (device list + device details). <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=bc0a93dfc53090e88f9170ba8fcad7039e102b5a'>Commit.</a> </li>
<li>KCM Adapters: Show adapter hci name in groupbox title. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=2ff8cb2f7b3cf7a33b48b470686691c2890568be'>Commit.</a> </li>
<li>KCM Adapters: Change no adapters message. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=fdc1dc8bcd7ac3d3179775fd411d88689c1c725e'>Commit.</a> </li>
<li>Move each kcmodule to its own directory. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=059fd502d4cb698bf7b0d6c6cb1f11c16500956c'>Commit.</a> </li>
<li>Use JSON files directly instead of kcoreaddons_desktop_to_json(). <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=8383ba5a54ab1b63ce4b56351f2484d136883d40'>Commit.</a> </li>
<li>Set title to incoming file notification. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3f13518832cde552a3a4e8757ae1f2dbee0d9b2e'>Commit.</a> </li>
<li>HELPER_INSTALL_PATH is no longer used. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=c866130a8fd0664210f0059bf712adcfa41e8681'>Commit.</a> </li>
<li>Move code from obexftp KDED module to bluedevil KDED module. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3ccaf76795af1e046c87c2776f8d165b11c63754'>Commit.</a> </li>
<li>Move helpers code to KDED module. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=76219969b35e9eeea65a5e74b81a1973c552ba5f'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Fixed close button animation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2c895b303d20977f7a4ef5bad8ecc385eba21fe8'>Commit.</a> </li>
<li>Increased radius for mask. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=41d01b47c67a46b247da6a7532f15624a34e6351'>Commit.</a> </li>
<li>Removed incorrect composition mode. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a80434f6777eb92d659245c7ea691988c2c8f489'>Commit.</a> </li>
<li>Reduce strength of the contrast pixel. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=509388e007f255fffcd1222c39956bf41f5d64e6'>Commit.</a> </li>
<li>Takeout inner part of the shadow to avoid artifacts for semi-transparent windows, and. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d9369b5e4276b60f35ef8c033ca247a6843d0f93'>Commit.</a> </li>
<li>Use similar code for rendering the decoration shadow and menu shadows. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2fdd1423bc29634e8920fb1479f5144ed2d3db31'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355647'>#355647</a></li>
<li>Properly mask out the inner part of the shadows (that overlaps with e.g. menus), to prevent artifacts when translucency is enabled. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=54c397d937c8a475052e54cbd6e4ebc835fb1c18'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355695'>#355695</a></li>
<li>Render unaltered background behind selected checkboxes (in menus and lists) rather than changing color to Highlight. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e59bb5fa129b5daa613c1349ab14c37ec6302157'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343428'>#343428</a></li>
<li>Remove icons and icons-dark from compiling.  remove obsolete message about orion theme. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d0a2cb816c020961aaef632b14c30e9a9d7eaef9'>Commit.</a> </li>
<li>Remove icons now in kde:breeze-icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=983b2879836b5cd6776dbe9e4a78342fbf8758d5'>Commit.</a> </li>
<li>Fix the nth case of broken symlinks in icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=278071edd386a023a3005a35cdece843428094f0'>Commit.</a> </li>
<li>Breeze Icons: add 16px action oxygen icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c775f84e34d570e0707119cbb774cda080e70934'>Commit.</a> </li>
<li>Breeze Icons: add 16px oxygen icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=786e569706c96b2a9e66e3fe5b0e59b90497c842'>Commit.</a> </li>
<li>Do not grey sunken comboboxes and raised menu-toolbuttons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=697f953683f94f0772bc4f1ec9ae68d8e0c77603'>Commit.</a> </li>
<li>Properly clip selected tab to prevent rendering artifacts. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4b7de62cec55609e599893f53931a9e5f8013dd9'>Commit.</a> </li>
<li>Fixed bounding conditions to consistently decide whether a combobox should be flat or not. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c226cb48ff8ac713ae5f4fcf6c4a55e221294d7b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354708'>#354708</a></li>
<li>Restoring icon applications-other 48px. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a25d40624f165f3440a118b301d843ac27a1eac7'>Commit.</a> </li>
<li>Fix YABS (Yet Another Broken Symlink). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0ac70055d9c2e46ea66cf6fcd6f62002c4afab70'>Commit.</a> </li>
<li>Breezestyle: set sidePanelView property to true already in polish. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=58fb398ad439e3342005a6113d6eccdd8b3f381b'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e4278a58665c142ee78eb2105be770fac1ee43ce'>Commit.</a> </li>
<li>Fix build on arm. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=568937801409093fe000d2205c0c6836e3e07c1f'>Commit.</a> </li>
<li>Remove non-existent symlink. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4ae30d0dbb67f02ccc3690a4329aede9690f94c0'>Commit.</a> </li>
<li>Standard Plasma 5.5 wallpaper from Kven. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=627408a97b0f79c55bdd7174292e03d21f983cd7'>Commit.</a> </li>
<li>Gtkbreeze now moved to breeze-gtk repository. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cc466876d2679f7824686aba6a39a7b2bd7fb24e'>Commit.</a> </li>
<li>Bookmarks.svg is an action folder-bookmark.svg is a folder, the folder should not go in actions and the action should not go in places. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3adf74dadd7a497a7808a12428bdb28b56769779'>Commit.</a> </li>
<li>Breeze Icons: add rating-unrated icon for file rating with stars. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=545b6ba112f4bee04012fc4e91abe86fa3d95bc7'>Commit.</a> </li>
<li>Centralized arrow color calculation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5c22c24818426eb99ef0d5b0310b54ce5021a2d6'>Commit.</a> </li>
<li>Removed unused parameter. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4c66289e7499259306891b93c95807ffe59dbbf9'>Commit.</a> </li>
<li>Use new volume icon in 24px size. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3ab6df4cbbca3de284e2e75b3f017336e31329bb'>Commit.</a> </li>
<li>Better gear icon for kdevelop actions. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1f94608c32a0466e130ecb93bc79ff7ebc254a7e'>Commit.</a> </li>
<li>Match 24px with 22px icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=83afd0185467bb4ffee2f09883465d4cb600f3a2'>Commit.</a> </li>
<li>Better order-object-x icons 16 and 22. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4c4a7f5bb9eb169a1ab63094a506b46fdfc36017'>Commit.</a> </li>
<li>Better repeat and  shuffle icons. Mtach object-flip-x 16px with 22px. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4a9c39963d2d2d6a80ef40dde5956a805dad0c4b'>Commit.</a> </li>
<li>Audio-card was unbalanced on the sides. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4254e402a99e0d8abcbf115e259a079447bb1f7c'>Commit.</a> </li>
<li>Removed duplicate icon applications-other from apps/. Added Apper. Changed a Kdenlive action icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=01d20b6c4ca32f84d7ed725fb1292e5946e89aab'>Commit.</a> </li>
<li>1 px outline around slider handles, for consistency. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=16056ddc14d6bc4c1cbcbe225742307dd4ada866'>Commit.</a> </li>
<li>BreezeHelper: Make sure radius is never negative even when changing metrics. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=656d34203c946dc40fb859de12f1375f1f9cac0e'>Commit.</a> </li>
<li>32px action icons can have more details than the smaller counterparts. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0403193172f873f26e8cb4685d199269a001a664'>Commit.</a> </li>
<li>Re-added some spacing around separators (sorry for the noise). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bd983a2dce6f1c73824dfb0db6ee321311001a77'>Commit.</a> </li>
<li>Use minimum size for menu separators. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=93c51fb63899192e180854f5b23b3f08a10b3da2'>Commit.</a> </li>
<li>Use the same "grey" background for sunken, un-hovered buttons as for sunken, un-hovered toolbuttons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=12c9d301f56c54d232bfa1b50fa00ebf35ca175d'>Commit.</a> </li>
<li>Re-added cursor moving hack for kde4, otherwise mouse-over effects are broken after window drag. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b2c06365a27fb89e61ce00831fdfa949627c1e24'>Commit.</a> </li>
<li>Changed draw-text icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7266fbc8a380d8d27d925105d5f4dbaf777244c9'>Commit.</a> </li>
<li>Revised folders: Different color (previous was inspired in Oxygen as a tribute). Removed gradient in upper tab and aligned edges of tabs. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2285b68391574cca78b64a8a1951f37b0155c3de'>Commit.</a> </li>
<li>Delay update of widget's palette to after polish is completed, because setPalette messes up with the. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6d852f30a1f2c1988359d4e0cdb21e2f1714a6bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344425'>#344425</a></li>
<li>Moved updatePalettes from public Q_SLOTS to protected. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=787708db541d5e200b6df969d085f3133461cd98'>Commit.</a> </li>
<li>Do not re-register widget if already in list. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7105f0a04697344c78fbf2ad21eaadd8051c8aba'>Commit.</a> </li>
<li>Breeze Icons: add quicklaunch widget icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a81e2531a3f0923c4c7a8bb9912124cc099b0769'>Commit.</a> </li>
<li>Breeze Icons: add Touchpad input icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3cd8598ff832e4fdd5aa80d5d3ab7b2c6e0e109b'>Commit.</a> </li>
<li>Breeze Icon: Change notes widget dark icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5fa5788f26de5cacb4d31eab6cd1a8676a5efde3'>Commit.</a> </li>
<li>Breeze Icons: add user switcher widget icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b41e1151a7df8b7967c305366cd53cca08c2d37a'>Commit.</a> </li>
<li>Breeze Icons: add applet colorpick icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3b2fd19e0abf683b8633718b6424bfed0a7e4eea'>Commit.</a> </li>
<li>Breeze Icons: Start 32px action icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=dadf0a31740894802817c7801e265e8cf047cdfd'>Commit.</a> </li>
<li>Use NETRootInfo to initiate wm move operation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=454483b51f86f0b15403ffdf6643735100080565'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125522'>#125522</a></li>
<li>Added option (true by default) to draw focus indicator in lists, similar to what's in oxygen. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3ddd8ab9c9a0a11b692676ba7409108cd5e22ffb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352025'>#352025</a></li>
<li>Moved KDEConnect 22px icon to apps (again), changed emblem-unmounted. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bac5a554494ee5b6890b69e80114e2e465009809'>Commit.</a> </li>
<li>Added proper margin around toolbox tabs. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5dc36a4eecd7be21bc2f2acd9330051069e5fe7c'>Commit.</a> </li>
<li>Make separators expand full width in menus. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cdc889019a125e2f62dd8ae6d41ad7cc9eae6dae'>Commit.</a> </li>
<li>Fixed checkbox animation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c6edea61276b84b02490e7e5c254978807183171'>Commit.</a> </li>
<li>No need to have the script inside the folder structure of the theme. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2fbe30ec21aca3ae219ae55cc0a27f114715b059'>Commit.</a> </li>
<li>Add Breeze Dark script. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c3a9fd38292b4fec3b6f8e89b854295fe318bb68'>Commit.</a> </li>
<li>Breeze dark applet icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=88238ae32a2592f7f8a5af0f068197a6782f07f2'>Commit.</a> </li>
<li>Breeze Icon: add some widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=be3394e6606ae00dc77de14a9fd54c805527e26a'>Commit.</a> </li>
<li>Breeze Icons: link spectacles icons to kscreenshot. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1dca1a3a7c433c12fa6c9a96f7c740407f6ffa9d'>Commit.</a> </li>
<li>Breeze Icons: icon names are lowercase. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d073cc0a5ea06e3ec30b57f320b0362032d75663'>Commit.</a> </li>
<li>Breeze Icons: add missing widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=781c9eed7f680e93abed075985ff7934a4969815'>Commit.</a> </li>
<li>Breeze Icons: add some widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0171a5f209bd3f7b2ff26a9ac5de78e61af949eb'>Commit.</a> </li>
<li>Breeze Icons: add widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=8adb003c01386284adaf43a161ce1830b99b6d94'>Commit.</a> </li>
<li>Breeze Icons: new widget Icons and use only oxygen font. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d4c0d17fdb0b2be90306743c7b7ee9b13ea27290'>Commit.</a> </li>
<li>Breeze Icons: add widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d432971f79bb16cb764e5a206d53ac67632b17d5'>Commit.</a> </li>
<li>Move KDEConnect 16px icon to apps, added missing links to Breeze Dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ce038503dbb2a3005f8af4aa4b0c28e5fae7d089'>Commit.</a> </li>
<li>Missing linnks in Breeze Dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6e7c41620fb78a461d27d29324cfc536600d60cf'>Commit.</a> </li>
<li>Removed PCSX2 icon, Steam icon, added Keyboard icon 22px and changed Keyboard icon for 32 and 64px. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4df0e5bf7751cf79c9d2d68353c5544de54a1aa4'>Commit.</a> </li>
<li>Breeze Icons: add some widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cfff30aadbb658cab21cadacce9168e989570c2e'>Commit.</a> </li>
<li>Added option to disable window background gradient. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2809f031bfa83205fad5b2037686924caf7ff53c'>Commit.</a> </li>
<li>Cosmetics. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c375cbcdcda06fc29032f095e535a6e47d6a1fd7'>Commit.</a> </li>
<li>Implemented SH_KCustomElement from KStyle, to have them working in KDE4. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=53d25d9558186f014fc4bdfbdbf30aafe5fb2ef7'>Commit.</a> </li>
<li>Breeze Icons: change security icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d284dd4ef6afa0fc5c4644d6e599defcaa4430c9'>Commit.</a> </li>
<li>Revised phone and tablet icons 22px. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3ad3b36c0045247e2f58f422ffc2c7c665a920ee'>Commit.</a> </li>
<li>Breeze Icons: add applet preview icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0d7e8e388f5f39e15a7c89a906913931046bbf37'>Commit.</a> </li>
<li>Breeze icons for Clementine. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b626faa53311d9620703dcc124ef55e60083ca87'>Commit.</a> </li>
<li>Add Marble Breeze Icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=33b1366ddc2a3033bc4b087873d5a4ee9eb927a1'>Commit.</a> </li>
<li>Breeze Icons: link im-user icons from actions to status. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0be81ef03e92fbca24ae9f46a9e37351f3cfed78'>Commit.</a> </li>
<li>Missing mimetypes in Breeze Dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9b10143d9cd6b21a6cdb0cc4ac2f0aa40cdc531b'>Commit.</a> </li>
<li>Match icon with Breeze. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a197bb58f45013f90ca2ba7768ddedd275a1da43'>Commit.</a> </li>
<li>Breeze icons for plasma applets. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=945c2503dcff5c938be9b3b6d1f3b26b8e7891b2'>Commit.</a> </li>
<li>New zoom icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5c75dda5070609e2be81c7cc42196d4ad912edb0'>Commit.</a> </li>
<li>Small fix in IM icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3dde7f8beb4c2fe9d35cb3a5de74ca88e803c68c'>Commit.</a> </li>
<li>Breeze Icons: add Marble app icons 1st part. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=93d3e2e13bb2cd5bb3f31e46a7f44526c3cf96c5'>Commit.</a> </li>
<li>Missing icon in Breeze Dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=fcbdb842d2622dbebaa3bf4c37c082a40a8abaa8'>Commit.</a> </li>
<li>Added user online action icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=78a0996f206fb857bdbed0a3bb81516baaecbb1b'>Commit.</a> </li>
<li>Breeze Icon for Choqok. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1a09b395e87d3195e68cbe78513994297fd98319'>Commit.</a> </li>
<li>Plasma applet icons for widget explorer. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cd87416e5746945134a89323d138cef4a9f85240'>Commit.</a> </li>
<li>Changed KDEConnect tray icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=17c4dae57ea985f277499ce25222fb13240bbfed'>Commit.</a> </li>
<li>Changed mobile devices icons and network icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=55e134a73da7ae60bb6be2179414847feedf4fe7'>Commit.</a> </li>
<li>Removed 16px IM status icons, revised IM action icons and used them for status in 22px size. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=49c84c88e898b69702cd365d0e204db60eb7e5fa'>Commit.</a> </li>
<li>Trimmed icon speakers. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f0e2fb4277fa4a52a43581b69b6534f287d088d2'>Commit.</a> </li>
<li>Breeze Icons: first widget icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b66f14c16238df7f004b3623e289a4aa92ddd771'>Commit.</a> </li>
<li>Breeze Icons: add applets icons for widget explorer. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f75b729531a75df1536b7340fcb016a5752ebf69'>Commit.</a> </li>
<li>Breeze-Icons: remove monochrome system icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3e2d112cbbaea565e28c0eed0615fc1cd7a29714'>Commit.</a> </li>
<li>Breeze Icons: remove monochrome terminal and file manager icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a05bb13fff2beab2385329136cba9dcd690104d1'>Commit.</a> </li>
<li>Breeze Icons: remove 22px app icons for terminal and file manager. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f6e0f32b2d9b818dbd6470247f4a08564331586c'>Commit.</a> </li>
<li>Breeze Icons: relink Inode-directory to 32px and add places for 32 and 64. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=58bc83a367195b26171330602e7a4ced923ae29d'>Commit.</a> </li>
<li>Update the GTK icon cache when installing. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=92cd2b667c04e18fd66986e8a42a53c052f5b5a0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125195'>#125195</a></li>
<li>Revert rendering of outline pixel, because buggy. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=feea03af73a9701d900828d2942adf4733483733'>Commit.</a> </li>
<li>Cleanup shadow contrast pixel rendering. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4449ab925b10bab4a5408ed0b757e7ed94571518'>Commit.</a> </li>
<li>Fixed rendering of "unmaximize" button. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a6693dfcef8cabe5e763bb0fb5f704ff475921e1'>Commit.</a> </li>
<li>Fixed rendering of partial checkboxes. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=279dd393461932a8326dd8556887052cd508e3f9'>Commit.</a> </li>
<li>Changed and adde link. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=640d672fc3a5a89769f6b64ed6baabe343252755'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5bfb0ab6cc2f6e65be081c125fb4b581483dfc08'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1bb44b062f6c2d726066e5f81582a0ae59bba9c2'>Commit.</a> </li>
<li>Added icon for markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6b69dde23e65bb5190ca637d283f571506b83e0a'>Commit.</a> </li>
<li>Only use fixed icon size for QtQuickControls. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=684e71ab0dcdbda62d508c2432890aec936a55f1'>Commit.</a> See bug <a href='https://bugs.kde.org/339106'>#339106</a></li>
<li>Fix rendering of disabled radio buttons in menus. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1598957e6de0951dee6b1c8b3e562cc303565c3e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352160'>#352160</a></li>
<li>Fix all icon symlinks to be relative rather than absolute. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0c947178529b65bb2d464da9d1e293353a885705'>Commit.</a> </li>
<li>Added an outline to focused buttons, similar to what is done in menus. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ab1467be9188a21f81e1f4efddcfed4eecc5033d'>Commit.</a> </li>
<li>Reduce size of checkbox and radiobuttons; reduced spacing between mark and text. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9572ab8a4dc2dd4b8fec3111fdfbd0eddc157362'>Commit.</a> </li>
<li>- Changed menu rendering to remove margin between selection rect and frame. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3230d9f7d77a48d0d0a5538ad2b8817d1d4e5ebc'>Commit.</a> </li>
<li>Added .gitignore to icons and icons-dark folders, synced GitHub repo with KDE Breeze repo. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3c398dd7005224329b0b946d4c3cb1b51579337f'>Commit.</a> </li>
<li>Edited out my mail. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d397e0253f92bf7f4ebeeaea7e876e5f636f14f2'>Commit.</a> </li>
<li>Readded proper gradient on active window titlebar. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cae73cbd9d1779c6016ee0d1e83fed618ae293f9'>Commit.</a> </li>
<li>Breeze Icons: KPasswordDialog icons #T571. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=dc71a5ccd03f238f033ea8d243a614832d35b3f2'>Commit.</a> </li>
<li>Breeze Icons: New Folder structure according to. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=eec55e90963a968b49bafa0a42f6cd64fb0ca6a6'>Commit.</a> </li>
<li>Breeze Icons: push test. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=57c497a2af07e8765d135cdbd68e3795aeea628e'>Commit.</a> </li>
<li>Add look and feel package for Breeze Dark theme. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3f4b199c41c9b20842448417b5df37e9be17c34d'>Commit.</a> </li>
<li>Fixed rendering of progressbars when progress is very small. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=43c6e6ff62c94cbc96dafcbbc2b1e743fb9e6e13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351237'>#351237</a></li>
<li>- deal with dpiRatio when sending WM move resize events. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0df39f19588e29e2d3969be2234db5fef81d3722'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351321'>#351321</a></li>
<li>Breeze Folders are updated with a new design from Uri should be in 5.4. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=75f6ee43db8f3092bcd5c81045763a7a983ecde0'>Commit.</a> </li>
<li>Breeze Icons: update authors mimetype according to T570. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=54862288377664afdb0f688497e22c87df2c656f'>Commit.</a> </li>
<li>Breeze icons: some needed links. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=db4d0f0792f1c5a3ea9bd1569e9746c1f580b342'>Commit.</a> </li>
<li>Breeze-dark backup icon set is breeze. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6c9319e52e7f85b43b960eb8223f68d5890d1896'>Commit.</a> </li>
<li>Breeze Icons: add Clementine icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=853c530bf3b7ad94e3474123e4d39a4aaa39bbe4'>Commit.</a> </li>
<li>Breeze Icons: Sync with github. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1f241a1a97ce8ccad64b78970b4ffe00ec9d1242'>Commit.</a> </li>
<li>Breeze Icon: fix an plasma 5.4 bug. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cd4328238fff1367497a607b023138177fd9e6d5'>Commit.</a> </li>
</ul>


<h3><a name='breeze-gtk' href='http://quickgit.kde.org/?p=breeze-gtk.git'>Breeze GTK</a></h3>
<ul id='ulbreeze-gtk' style='display: block'><li>New in this release</li></ul>
<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a></h3>
<ul id='uldiscover' style='display: block'><li>New in this release</li></ul>
<h3><a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Require Frameworks 5.16. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=c675fa1dad863567fa4563b13e619a5a5ae30453'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126025'>#126025</a></li>
<li>Use the original path if QStandardPaths::locate fails. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=03aba97a6f32aa405032369d93e0fb2d0c968f91'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125567'>#125567</a></li>
<li>Add DocTools dependency. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=a58642c5f1398b2852f5ce0252fff7d28cecad51'>Commit.</a> </li>
<li>Restore building of kstart. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=f7343d441cf8e78ee4b25b7c333b68961bf78d63'>Commit.</a> </li>
<li>Fix whitespace. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=e50c6d5d6072c734113a57d47db6c7308b2aed76'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125155'>#125155</a></li>
<li>Makse some modules optional. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=dbd0929cb9b227e41aaefb9db84e0f0e642a2ea5'>Commit.</a> </li>
<li>Update dependencies and fix build on Windows. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=dcb783c34754ae1cb2651b9e63d9dba603ee0bd7'>Commit.</a> </li>
<li>Deprecate kcmshell5 --lang. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=be794621a6f190834ac88f8b77fd616224cec5e5'>Commit.</a> </li>
</ul>


<h3><a name='kdecoration' href='http://quickgit.kde.org/?p=kdecoration.git'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Improve documentation. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=2bdcc06ce8d4d111c3eb8274351e0a4fc03883d5'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[User Switcher plasmoid] Elide text if neccessary. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e7b6cc1b838a497e1a590a3aad0f56452dcb542f'>Commit.</a> </li>
<li>[User Switcher plasmoid] Fix applet size calculation. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=324186af1b6eab5e2ce40bb3889a7b9749470aeb'>Commit.</a> </li>
<li>[Color Picker] Ensure sensible minimum size for the controls. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=4d314c43a856d0d23143c5cfb470ad84af0bf18c'>Commit.</a> </li>
<li>Remove notes.svgz from kdeplasma-addons. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d52e64f5e87d134326cc64fe861716d341a3abc3'>Commit.</a> </li>
<li>Comic engine: fix relative icons loading. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1613128573a25896034fbdf1b4a6f6a823559a42'>Commit.</a> </li>
<li>Comic engine: fix checking of main script extension. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=75eda480e0cbba47c862410673c599165921b65f'>Commit.</a> </li>
<li>Quicklaunch: Switch to vertical mode when height > width. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=69a6982e44f6b14e479b1881bc649d14f570aa3b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126052'>#126052</a></li>
<li>Fix quarterly fuzziness (level 2 fuzziness is now 15 minute accurate). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=74f7ffff15b20cdf589c8c6a5c9f731c3167223e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355189'>#355189</a></li>
<li>Align data source to minute. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8b0e8efa992c2cb823a999794ad2d198cdfc85aa'>Commit.</a> </li>
<li>Show actual time in tooltip. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e091a2bf266407414818fec62e1f601688a481a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355191'>#355191</a></li>
<li>Add Disk Quota Plasmoid. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7649200669efa8d01d8b1ec7c8c7fbeda633da87'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124589'>#124589</a></li>
<li>Introduce the Activity Pager plasmoid. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8d1b5b37a7a6717c98538e3e04dd5f3f02268128'>Commit.</a> </li>
<li>Add option to show individual monitors for each CPU to sytemloadviewer. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1c0cf92208b8a7589e1d6103f3fd5640840509c8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125006'>#125006</a></li>
<li>Add User Switcher plasmoid. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=01824730acc49aea50bc0d0da1eb17b270125458'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125678'>#125678</a></li>
<li>Fix SystemLoadViewer freezing plasmashell. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=351c2a03827040d148a1b73dbeab12eb3d3e69de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348385'>#348385</a>. Code review <a href='https://git.reviewboard.kde.org/r/125858'>#125858</a></li>
<li>Add some formfactor metadata. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ba0ab7a3f1a6ed1ba9bba97a2eca27fbc4381931'>Commit.</a> </li>
<li>Quicklaunch: Show default icon when launcher icon is empty. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=43d7a16e37146b033d244eb017155950f5079843'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125839'>#125839</a></li>
<li>Quicklaunch: Fix crash when KOpenWithDialog returns null service. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=2e928b7fc7e4e1779497378ba8600dd836ada81f'>Commit.</a> </li>
<li>New notes graphics. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8480431b07a9ec8ba59e706e7f64b6d45c211355'>Commit.</a> </li>
<li>Breeze Icons: add quicklaunch widget icon. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=07a2bde06bbe73d2239244df953c4b0541848e88'>Commit.</a> </li>
<li>Breeze Icon: add Widget icon for notes widget. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e5b76e54cbd83f0b021b7e1033a26e7bbc02ac94'>Commit.</a> </li>
<li>Breeze Icon: add widget icon for colorpicker. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e300316f6bef5a5c724455f9ef078f4fcb61badd'>Commit.</a> </li>
<li>Applets: Change applet icons to breeze/applet icons. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=fb59e9fa7b9845582f84e14cbb2b8058962d9ddd'>Commit.</a> </li>
<li>Quicklaunch: Add support for popup with launchers. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=2cf1c816c15be637929cab17c4719ec0e92162e2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125283'>#125283</a></li>
<li>Plasma 5 Quicklaunch applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8cad97ac615b9875a0b65f4b9c797074f65eaf70'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125222'>#125222</a></li>
<li>The color picker plasmoid has been ported to Plasma 5. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6e663a9e85cf8017fc20e0c749e5a2d939cef98c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124923'>#124923</a></li>
<li>Remove showdashboard plasmoid. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1c400d24b97366742a8ea57b364949add4361d74'>Commit.</a> </li>
</ul>


<h3><a name='kgamma5' href='http://quickgit.kde.org/?p=kgamma5.git'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Proofread kgamma kcm docbook to kf5. <a href='http://quickgit.kde.org/?p=kgamma5.git&amp;a=commit&amp;h=89ebd8bcaedf4dae39827a550c0390ff5c80510e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124548'>#124548</a></li>
</ul>


<h3><a name='khotkeys' href='http://quickgit.kde.org/?p=khotkeys.git'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Remove PrintScreen config. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=a8c177911dea1e099bfb499f199ee7039c756cde'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126091'>#126091</a></li>
<li>Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=2ed0d08b6ec1e19ab929b82a9f16458abf644e3d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125845'>#125845</a></li>
<li>Add guards for Xlib code. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=b84848594f765e171cc4d8fdc7c6094d864e8780'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125825'>#125825</a></li>
<li>Use a DBUS mutex to avoid writing dated settings. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=bf27c5088d435f4ecdbf7b75333661ed53cddbf0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125769'>#125769</a>. Fixes bug <a href='https://bugs.kde.org/354282'>#354282</a></li>
<li>Unselect current item on clicking into empty space. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=48681d45d5e5caf3c6e012fea3942637e66ee1fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/94949'>#94949</a>. Code review <a href='https://git.reviewboard.kde.org/r/125680'>#125680</a></li>
<li>Do not write back dated settings from daemon. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=1c28e9b9a8caf34643ae2a697415a0dfc8d1a8c3'>Commit.</a> See bug <a href='https://bugs.kde.org/352067'>#352067</a>. Code review <a href='https://git.reviewboard.kde.org/r/125630'>#125630</a></li>
<li>Schedule saving to the next event cycle. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=4747599badf67389530483ea62b6f54bc36ac9c3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343891'>#343891</a></li>
<li>Port kded plugin to json metadata... <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=9661807009fee7b1183f80e6ce90b8ede3237f27'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>KInfoCenter</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Ise QStringLiteral + new connect api. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=230597ca6d6663cde74752c635bbc40b85ec70c6'>Commit.</a> </li>
<li>Fix kinfocenter crash when run with --version. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=fe16e35904587266f37114ac682916e449677054'>Commit.</a> </li>
<li>[wayland] Add key repeat information. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=bc46778693d078e6b81efee5d1a2f86880efc02e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125234'>#125234</a></li>
<li>React to baloomonitor qml plugin changes. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=baec983d523e5893b5ffa234fac9fdbb6e59c595'>Commit.</a> </li>
<li>Add File Indexer Monitor KCM. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=b00d311a2bef990876e26169e3f6a47f793ed5ad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124720'>#124720</a></li>
</ul>


<h3><a name='kmenuedit' href='http://quickgit.kde.org/?p=kmenuedit.git'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Port to new connect api. Use QStringLiteral and co. <a href='http://quickgit.kde.org/?p=kmenuedit.git&amp;a=commit&amp;h=bde5a2895fffce09a3044b672ad845fe540e6fe0'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Use QStringLiteral + new connect api. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=ceef076effdf4d40450a763af74024f022b13564'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=818287e06de37b2a1abd4f0100f4cb3d27368133'>Commit.</a> </li>
<li>Add keywords to .desktop file. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=7c652e43a2c0c2bdb6749c6b229e88ccd8f0ce72'>Commit.</a> </li>
<li>Port kded plugin to json metadata... <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=6cc50d83e97e76d167bd8b8a42d2ca99055cd008'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='http://quickgit.kde.org/?p=kscreenlocker.git'>KScreenlocker</a></h3>
<ul id='ulkscreenlocker' style='display: block'><li>New in this release</li></ul>
<h3><a name='ksshaskpass' href='http://quickgit.kde.org/?p=ksshaskpass.git'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Initialize flag. Minor optimization. <a href='http://quickgit.kde.org/?p=ksshaskpass.git&amp;a=commit&amp;h=e66122a2274d94b252aa762d2327399938f53d9d'>Commit.</a> </li>
<li>USe QStringLiteral. <a href='http://quickgit.kde.org/?p=ksshaskpass.git&amp;a=commit&amp;h=78695bae76c0c81ec23bcea37609469f5064bd75'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Use QStringLiteral + new connect api. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=1e575241f6d4a94ea613287d06d6e23e8d6b0297'>Commit.</a> </li>
<li>Fix titles being cut at the first space. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=3b4ef4c032e247ffcd2e38e152cf2b079b634f38'>Commit.</a> See bug <a href='https://bugs.kde.org/306243'>#306243</a></li>
<li>Do not pass empty strings to k1i8n. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=45c04530ce0cac317be251deb7c036a5901e941f'>Commit.</a> </li>
<li>Disable gui if the webkitwidgets module is not found. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=88f0152e66ed9fe44df834b3532048221a22b856'>Commit.</a> </li>
</ul>


<h3><a name='kwallet-pam' href='http://quickgit.kde.org/?p=kwallet-pam.git'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Fix build on OS X. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=4c0886aad118944e743ee61e85855b75fd7f8403'>Commit.</a> </li>
<li>Added implementation of pam_syslog and pam_vsyslog for OS X. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=f2c1b8836ad9ccd32455e74af0b94c5004d1693e'>Commit.</a> </li>
<li>Add gcrypt include path setup. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=95eeaaaa1d9ed5942f257127b4205a7818b262fd'>Commit.</a> </li>
<li>Add reviewboardrc. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=8c3fa6f3b9f70882430b883e647bdd76353aa0d3'>Commit.</a> </li>
<li>Daemonize the forked kwalletd{,5} process. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=115ad562baf0cb0534876cbcb705fd9190e290b4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125190'>#125190</a></li>
<li>Fix pam_kwallet5.so not being installed in the specified install prefix. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=105cd18dbfe644e510e772a7e8226d8e965477f9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124962'>#124962</a></li>
<li>Add PROJECT_VERSION. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=d6ab85cf103be6217ae438460b2e8073ff52d3b8'>Commit.</a> </li>
<li>Add different log prefix for KWallet5 build. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=91ae142b2bafe6053defdb21280457c83a5929d7'>Commit.</a> </li>
</ul>


<h3><a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a> </h3>
<ul id='ulkwayland' style='display: block'>
<li>[autotests] Declare metatype for Qt::Edges. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3c2da0b427516b1afec232b65bd61bb9c0e9ff95'>Commit.</a> </li>
<li>[tests] Use QCoreApplication for ShadowTest. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=71f9abbb9e02e1ff319a2e3ff482adbfe2e70524'>Commit.</a> </li>
<li>[tests] Add a shadow test application. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=30fb755a18c896db7db0d862ae25a9b68ad8ace4'>Commit.</a> </li>
<li>[server] Fix possible crash after deleting an output(device)interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=24c9f4576c2b63387ce63f6ecc7bd4a69aed527e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126097'>#126097</a></li>
<li>[client] Enforce creating platform window in Surface::fromWindow. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=2464bbb18ef78f98d64adc6a7b4a069714d5033d'>Commit.</a> </li>
<li>[autotests] Delete connection in thread in TestWindowmanagement. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=80d99adb2eb15d1133327f2319e6b218dceb0b68'>Commit.</a> </li>
<li>[autotests] Use QStringLiteral for TestWindowManagement::testWindowTitle. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=fee0c62681f19c2760a5ff94f3a234a6e0df3b39'>Commit.</a> </li>
<li>[autotests] Switch all tests to GUILESS_MAIN. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cefddf44f29f976f0cb1377bb011995acd8fa1b9'>Commit.</a> </li>
<li>[autotests/client] Use GUILESS_MAIN for TestWindowManagement. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a8b9382c7cffae30e5de8d79f429abe22e28c67e'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=dc59ccc00e0d50602db793df8b677db320482a47'>Commit.</a> </li>
<li>[autotests] Destroy a created ClientConnection. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=20146bd5d34e02194849775994097c51af4fded6'>Commit.</a> </li>
<li>[server] Add a convenient ClientConnection::destroy. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a59a8fc894dca859ee82135823c157b11f5a54e8'>Commit.</a> </li>
<li>[autotests] Cleanup surface handling in TestWindowManagement. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8a7b81f47cd9ce28fbe7094d6e320c72b4b8419b'>Commit.</a> </li>
<li>Address last comments of review 125871. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=1464e17b62adae1c2027e6d8b6ed170573cb2563'>Commit.</a> </li>
<li>Add new interfaces to mapping file. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=493e3fe98e36d14e0764099206ce5db8c51ee7c8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125942'>#125942</a></li>
<li>Autotests for outputmanagement and outputdevices protocols. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a9fdfa626b0a0f1d694758979aa2cf41418db5e1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125942'>#125942</a></li>
<li>Server side of new outputmanagement protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ad58729b403ecf7e0292b44fe762aff394b3b537'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125942'>#125942</a></li>
<li>Client side of new outputmanagement protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=bcf03a1b79750507aae7a6fc44bb290ea59e0f77'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125942'>#125942</a></li>
<li>[server] Flush client after sending a frameRendered callback. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c5638bfb9d334e9721775e516c32e34a63183418'>Commit.</a> </li>
<li>Add a static PlasmaShellSurfaceInterface::get(wl_resource*). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=da8771323c62bf7bb4233515d25d62f124936de2'>Commit.</a> </li>
<li>Task geometries to wayland for minimize effect. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3d41b6fc726ed0fb9589e5fbefa1b07821cbc7c7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125871'>#125871</a></li>
<li>Support request resizing on ShellSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=41caa6badf0e3e24e2b0b758d0440c7db9a47755'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125836'>#125836</a></li>
<li>Support request moving on ShellSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=78781abdb5319f34625c5e3e5a5305f80e79cf2c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125828'>#125828</a></li>
<li>Toggle minimize api for plasma windowmanagement in kwayland. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=215695414e35cfcf9392fdff69f3b82b5c90253d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125841'>#125841</a></li>
<li>[server] Add support for flags on transient ShellSurfaceInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=1de10491f7a56951af86679ccdb24fcc2b4cb571'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125552'>#125552</a></li>
<li>[server] ShellSurface's transient is a dedicated mode. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=38bb1c0855effd9551362efe4482a89983a07cfc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125468'>#125468</a></li>
<li>Introduce SkipTaskbar. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=140c1860161aac4aa81fb9cec3c0b55d9bf0ad74'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125453'>#125453</a></li>
<li>Fake a movement before sending a click. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=408b656aafa8a4928fde695629f01d0689f5e1e6'>Commit.</a> </li>
<li>Add Mainpage.dox to triggeri apidocs generation. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9e3d930d48565925c4dcf26f7ade12aa91cae9d4'>Commit.</a> </li>
<li>Add a getter for window type. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=f160a85c59ae3be46c71685a1b79d5762d684ee4'>Commit.</a> </li>
<li>[tools] Simulate a panel tooltip. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=2b5165ccff14322c24b2c0e2b2e7ac7954f5211c'>Commit.</a> </li>
<li>Fix Typo in Surface::inputIsInfitine. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=29fdf023aafc9fb2af020cee73199c829dcd86c5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125260'>#125260</a></li>
<li>Add support for transient to ShellSurface(Interface). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ea74d0a895f649c8a70871b3418c4da6ea875263'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125223'>#125223</a></li>
<li>Increase version requirements for wl_data_device_release. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=aef4708353fa2b6b22f08358cb72def947da9863'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352663'>#352663</a></li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=473b06fa7fb9cee3e8138c42bb3e9d0675dd603f'>Commit.</a> </li>
<li>[server] Add more documentation. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9f661ce4b1b6d36eeaa4b80b7ccf7513c53f5fc3'>Commit.</a> </li>
<li>[server] Less warnings please. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=e03ae7e8b72aec14fa6259bb54355dbc17ff3c19'>Commit.</a> </li>
<li>[client] Move static functions into anonymous namespace. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ae2341d189ea4a86616237bdb0193f80ebf027c9'>Commit.</a> </li>
<li>[client] Hide macro from doxygen. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9d74f0dbe2d103ec5004caf1180807f4c657167b'>Commit.</a> </li>
<li>[server] Move static methods into anonymous namespace. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6336dd9c9bfec295c5cf28cfccac5f94e8ee4f82'>Commit.</a> </li>
<li>[server] Make s_version a static member of private class. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=4179b7b68f257723cfe8e37bd182f17e02dcc58c'>Commit.</a> </li>
<li>Add links to our classes in the README.md. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a3b8a36ab8b4d338bb024265a16a4d517a17b1bc'>Commit.</a> </li>
<li>[client] Improve documentation of Registry a little bit. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=dd4c884584c07d445dce02b16710831c9136292d'>Commit.</a> </li>
<li>[server] Install slide_interface.h. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=92c54ece70f90e5f0fd40421eb2bf0acbba3985a'>Commit.</a> </li>
<li>Fix autotest. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ec7169e0d46e6dca9a59077d88b656c08b498b1a'>Commit.</a> </li>
<li>Add documentation for the namespaces. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=634a51ec0238a8280358388b0d5ca3b4196bea66'>Commit.</a> </li>
<li>Small fixes for kwaylandScanner. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7d2f54a4088011061e9fc3816d036ef278d2422f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125110'>#125110</a></li>
<li>[autotests] Extend tests for Dpms. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=1a463cd779ecc35afabf2a149d30d31da615ef43'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125018'>#125018</a></li>
<li>[tests] Add an example for the Dpms interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=40d7d8033b57856d370741da522f808b3bc40ead'>Commit.</a> </li>
<li>[client] Dpms protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=08aff4256def4d18848699602a85081d79f3f766'>Commit.</a> </li>
<li>[server] Add a DpmsManagerInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8b620eeb97640591c2e0d4e9aae6269357a41434'>Commit.</a> </li>
<li>[server] Add a static OutputInterface::get(wl_resource*). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=f78fdbcccf8014e361ece22b77a529b9870a8593'>Commit.</a> </li>
<li>Interface for a Slide effect. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=27103c7116d0e2f63bf82fd1c014121935be14ab'>Commit.</a> </li>
<li>[server] Another round of docu improvements. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ca688ee8832c42a6e298072ba0572235dab235c7'>Commit.</a> </li>
<li>Fix doxygen generation. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=502e1c5d7081f155b97e023763a7bfcb21532136'>Commit.</a> </li>
<li>API doc improvements. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=bae5bb0e8be3788bd07d8df618b53ac31c6dda90'>Commit.</a> </li>
<li>Fix documentation of ConnectionThread. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=19a96c2be8b6e8e2bb30956ebbb2192e2c4ae1bd'>Commit.</a> </li>
<li>[client] Improve documentation of ConnectionThread. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7eec98544d98154f649562ed5e3f76c7c56d75a2'>Commit.</a> </li>
<li>[client] Improve documentation of Registry. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=e86d023b9b567fc8624ca044758d6a153ff122a9'>Commit.</a> </li>
<li>Add dedicated destructor requests to blur and contrast interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c0d7aeacbfdd636ba1af9fcb8bf2cb4a33030ab7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125114'>#125114</a></li>
<li>Implement release for wl_data_device. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=698b18eaa9470b28e0df55923aca56685c0f6b97'>Commit.</a> </li>
<li>[server] Implement raise/lower in QtExtendedSurfaceInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d985c8c62b563fe29a1b7ad978313fced38b588d'>Commit.</a> </li>
<li>Fix typos. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3c68987c57fef276a49452f7e89fe152b4d96823'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125100'>#125100</a></li>
<li>Add documentation about the library in a README.md. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ca059bf888e7c0bc3e4d369609ad335f70158a86'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125092'>#125092</a></li>
<li>[tools] Generation of server boilerplate code. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a51c1bb2cbca5429843e357946bb5142fc4be178'>Commit.</a> </li>
<li>[server] Clean up headers of BlurInterface and ContrastInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=b5c67b9f55e9331dde4c9006455261e593f5cfe5'>Commit.</a> </li>
<li>[tools] Generate CamelCase request names and arguments. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=85e3690a5d051d0b410156f732f646550219dc09'>Commit.</a> </li>
<li>[tools] Generate boiler plate of requests on client side. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=0d245792e1ffc9799f36e9e691ae3d4af39ea647'>Commit.</a> </li>
<li>[client] Track PlasmaShellSurface and don't create multiple times. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=b7407db13f8f9cd17c64f880d5fc6528df6ee612'>Commit.</a> </li>
<li>[client] Return existing Surface for fromWindow. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=05cfbe9e0ccba7089cfe7053f618010fca0ac8ae'>Commit.</a> </li>
<li>[tools] Generator can parse xml protocol description to generate code. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7bb9fdb7eea00948ea1f2266c96c48d9073346d1'>Commit.</a> </li>
<li>Compile. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=73624f96c44a0728d617f94013c8e6125c1eb565'>Commit.</a> </li>
<li>Support OnScreenDisplay window type. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=262e0610206152ec113be6084d01ea3257b65f55'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125036'>#125036</a></li>
<li>Add missing files for the contrast effect. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6128020c85c67026ea4cae733067acdc2dc55439'>Commit.</a> </li>
<li>Wayland protocol for background contrast. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d66adb0e5eb288d6607bdf50dd271435508b7a65'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125030'>#125030</a></li>
<li>Add a small tool to generate the boilerplate for Wayland interfaces. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=11c9fc0b598c7ab92085e11a34e00d5d646a724d'>Commit.</a> </li>
<li>Fix compiler warnings. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=dba8265d37cc4ab1b273458637273098d24e1506'>Commit.</a> </li>
<li>[server] Use WL_KEYBOARD_REPEAT_INFO_SINCE_VERSION. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=4db533b29f53b608f8f9f03e4166d9cd7dfe002d'>Commit.</a> </li>
<li>Implement repeat info on wl_keyboard protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=48326670528c7068648744c524c78dada40f1ce6'>Commit.</a> </li>
<li>Blur protocol in KWayland. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7207c4ed10bc3dd293d883fd93d02afed6f0628f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125015'>#125015</a></li>
<li>[client] Remove needless ;. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ef6af70082891dbd40765e97fdcefbd3703997cc'>Commit.</a> </li>
<li>[client] Add a removed() signal to each class representing a global. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=58d0266b36a93cab3a71e32c046f1bf5fa292fa7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124835'>#124835</a></li>
<li>Add documentation to Registry on how to add another interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=42b77db524175ea9ec4c67519f2ad6143c61bfb0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124813'>#124813</a></li>
<li>[client] Reduce code-duplication in Registry. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=151fdf6d4616126de4b0a3eb9a492f28200f5994'>Commit.</a> </li>
<li>[client] Add EventQueue to Output. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a6c6ebffdd9cf0f60ecf4caae8b043d410b41aca'>Commit.</a> </li>
<li>[client] Install EventQueue on FullscreenShell. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c33c88acf8f8ed901984c3ce1366c9d26291406f'>Commit.</a> </li>
<li>[autotests] Fix typos. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=e9499608a1d809b4c2d48679772c70fc3ff5696b'>Commit.</a> </li>
<li>[client] Extend Registry by providing name and version of announced interfaces. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=14a32321140c6983da6d4cde931ab52d21866e94'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124734'>#124734</a></li>
</ul>


<h3><a name='kwayland-integration' href='http://quickgit.kde.org/?p=kwayland-integration.git'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>Werap skipTaskbar in setState. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=c70458bcd6d80f0c7c41ad80d2f216f358d63730'>Commit.</a> </li>
<li>Add a basic KWindowInfo. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=e989409e1b310b1504994378219d96094c2c39fd'>Commit.</a> </li>
<li>Support for the slide protocol. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=746873eec182545ed3bb5a059dd16d0fee1b448e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125119'>#125119</a></li>
<li>Check for plasmashell existence. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=fd7f4c3322d8332f0b0976a46eae41c88479e2f3'>Commit.</a> </li>
<li>Share the wayland connection in the plugin. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=c7057625adcdccfddc86af6be168d783c0f03bf2'>Commit.</a> </li>
<li>Use Compositor::fromApplication. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=20107c681f7147916bda10e043bed5794824176b'>Commit.</a> </li>
<li>Support contrast protocol. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=9d998493129863407d13d7ba9526c96a93201ef7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125035'>#125035</a></li>
<li>Implement kwindoweffects for wayland. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=5def3e4f3c04aa9a361ad9889e6ca8d4feee25b7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125015'>#125015</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Make Wayland::EGL optional again. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d89777bcacb7b040076a783c7ef15558182407d0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126202'>#126202</a></li>
<li>Only compile VirtualTerminal if libinput is found. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a055e2de828fcd86dc7667c29a8fcc140d287b76'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126182'>#126182</a></li>
<li>Ensure Scene doesn't render non lock screen windows while screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=38cde38edf583093bd3e0f7f79c284531608507a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126144'>#126144</a></li>
<li>[effects] Disable more effects while screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3e007b51982182cb813fc1fc7f5eb22081568a0b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126122'>#126122</a></li>
<li>Explicitly update the stack track on unminimization. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3525d1ce022d151744bff6a3534e42522a78fe2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353745'>#353745</a>. Code review <a href='https://git.reviewboard.kde.org/r/126134'>#126134</a></li>
<li>Correctly scale and position logout window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e2632f3816430c9c0047a6f01652c81561cab483'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355539'>#355539</a>. Code review <a href='https://git.reviewboard.kde.org/r/126133'>#126133</a></li>
<li>[backends/hwcomposer] Announce support for DPMS. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3361373445d151bc08bc2d92ac4c33e779060933'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126095'>#126095</a></li>
<li>Don't query for GL_CONTEXT_FLAGS if not at least OpenGL 3.0. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=416d8060cf60ef231e64d1b99b0f40f666953aa2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355522'>#355522</a></li>
<li>[wayland] Introduce an additional --exit-with-session command line arg. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=23784d2038e4a5d5a31185bf5948b686069ea60d'>Commit.</a> </li>
<li>Add path to kwin_wayland binary to KWin's installed CMakeConfig. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eabddf939b01d84d11e1a794f8dfed648f54ce24'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126106'>#126106</a></li>
<li>Fix minor typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a57e8c20a4093e66bd571eb979b2ec70a33c7a4'>Commit.</a> </li>
<li>[wayland] Reset internalWindow if the QWindow gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=681113e4a621f0ccaa6956e7e75f0c7b7838a9fc'>Commit.</a> </li>
<li>Ensure pointer position is updated before screen locker enforcement. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d883a9cda497169e9eebf6e8e4bf5bfeaed8635'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126103'>#126103</a></li>
<li>[wayland] Destroy all ClientConnections on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3be016fba4936df9aa3a48069de5ce2af60c8790'>Commit.</a> </li>
<li>Remove deleted windows from motionmanagers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=12b63e2aa7ed3ecc9a20b1724b2368e9a8ab3d65'>Commit.</a> See bug <a href='https://bugs.kde.org/339970'>#339970</a>. Code review <a href='https://git.reviewboard.kde.org/r/126034'>#126034</a></li>
<li>Add rule to protect the focus on a window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a4791e629d6d0e6a988a222e70b82bb3555d3759'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/185060'>#185060</a>. See bug <a href='https://bugs.kde.org/337798'>#337798</a>. Code review <a href='https://git.reviewboard.kde.org/r/126059'>#126059</a></li>
<li>Catch lost FOCUS_POINTER_ROOT FOCUS_IN events. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=37a64dcf5f7cebbb44886561d309e59112ef6afd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348935'>#348935</a>. Code review <a href='https://git.reviewboard.kde.org/r/126033'>#126033</a></li>
<li>Revert "Handle conflicts between epoxy and manually resolved function pointers". <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=01451eea360781639e58f5cf03ca3a4729d679ef'>Commit.</a> </li>
<li>[hwcomposer] Add support for backlight through light_device_t. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e71a230213db70cd834565002ebda48853145146'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126083'>#126083</a></li>
<li>[hwcomposer] Don't waitVsync if we haven't activated vsync yet. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8d0035544844f71cbec3c5d21a3b34889262065e'>Commit.</a> </li>
<li>[backends/hwcomposer] Base HwcomposerScreens on BasicScreens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=57a3cea14214b14b2c8cd43c0a76ab1bb5f26b51'>Commit.</a> </li>
<li>[backends/fbdev] Use BaseScreens implementation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eb81ddfb861f1a72a806687122fd56bd1b3ee70e'>Commit.</a> </li>
<li>[backends/wayland] Use BasicScreens implementation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=898eefc6be9ffa1f1d7383db2e507f4ac121bdb0'>Commit.</a> </li>
<li>[backends/x11] Use BasicScreens implementation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7b12e55857de30fd30fdc8a22ad7c875a3ee0c01'>Commit.</a> </li>
<li>[wayland] Add a base implementation for Screens in a basic setup. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fb4d59abb749a45bf14b8e3b4b139bfea46b5469'>Commit.</a> </li>
<li>[backends/wayland] Drop warning messages concerning egl backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cab25fc13f6ce5630ac710434c42db710db0e318'>Commit.</a> </li>
<li>[backends/wayland] Only set socket name if it got sepcified. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=75c82fce19df6039e6f6d3260f30876e0ff5f0d4'>Commit.</a> </li>
<li>[backends/wayland] Do not query outputs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=930e56767e23996397552628680bc791a573dd7c'>Commit.</a> </li>
<li>[backends/wayland] Simplify WaylandScreens by basing on ShellSurface size. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df71c5b61b4da47bcbc5a300a35475863b43585b'>Commit.</a> </li>
<li>[backends/wayland] Drop XRandR updating. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9381d3f88c2d0869a06e11f931f6fd01b8217dcb'>Commit.</a> </li>
<li>[backends/wayland] ShellSurface opened as a toplevel instead of fullscreen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3f6cb8812b709989cb56d14a6da0de7ee07f8a24'>Commit.</a> </li>
<li>[backends/wayland] Drop subsurface for cursor. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eafded71273d090dc6b4d82b656fcdb7afab8199'>Commit.</a> </li>
<li>[backends/wayland] Drop support for fullscreen shell. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1ca690d4eeb3aa22928049c981fb92de97141ba3'>Commit.</a> </li>
<li>[InputRedirection] Don't crash on invalid Toplevel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=42ddf685c1b31838acc00fc52358f4d7c6cfbe32'>Commit.</a> </li>
<li>Add safety check to Deleted::~Deleted for tear-down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=38f19dafb26fe85b3930ab472a3aa4c1fc4fb7e7'>Commit.</a> </li>
<li>Cancel loading in EffectsLoader on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=deb63fa3444b51e6c204bb1199cff964168523a4'>Commit.</a> </li>
<li>[InputRedirection] Check if workspace is valid before accessing it. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e5a0af1589535f40c1b927c754afc4aa123b9c69'>Commit.</a> </li>
<li>[wayland] Add some restrictions for lockscreen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e5518dffe22ae5ffeb04717a01c01bebdc17f023'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126015'>#126015</a></li>
<li>[wayland] Introduce property to identify lockscreen and inputmethods. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=450bbaafdc6eb70d1934c2c54c0e38daf8271add'>Commit.</a> </li>
<li>Introduce additional safety checks for init debug output in SceneOpenGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d416a0f699c68bb15f16c2474d3ba8b615c4d44'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126053'>#126053</a></li>
<li>Verify that context is robust before resolving robust functions. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d75cd26fb32cca7f88d797f45beee8a6f3ef196'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126051'>#126051</a></li>
<li>Create robust egl context if possible. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b383db2e2e6747d3beb881c4b948e2fe2403efde'>Commit.</a> </li>
<li>Move egl context creation into AbstractEglContext. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d96b8c4af00c37f954d2d70d295c679d74647fb'>Commit.</a> </li>
<li>[backends/hwcomposer] Do not manually cleanup our HwcomposerWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a610ec9f6a8f6d27f9e2f9ffa0f07e6c575a4bf'>Commit.</a> </li>
<li>[backends/hwcomposer] Blank output on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eb5ff917f657c2683624ec87dc473120694d1752'>Commit.</a> </li>
<li>Disallow XRender or NoCompositing on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d4dbc8c8020acd064f0f78c29172ce7abc21ec79'>Commit.</a> </li>
<li>[libkwinglutils] Fix regression in OpenGL version parsing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bcf76c0d2f057a65867df0abba7eece8457ec2ff'>Commit.</a> </li>
<li>[wayland] Don't use waitForFinished on the Xwayland QProcess. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f6ef9e8d8ce926ecf01db6fae03d0f041de129a6'>Commit.</a> </li>
<li>[wayland] Emit signal before x11 connection gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=240e7ba33744320cde8c050a2e75bc5e5eb0d8ce'>Commit.</a> </li>
<li>[wayland] Disable Compositor during tear-down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f2bdb4818a37400be071aa57d27cabea47037566'>Commit.</a> </li>
<li>[wayland] Don't exit due to Xwayland exiting on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=238ff89edb05bcdc39fb14aebd9527b3b92ecf37'>Commit.</a> </li>
<li>[wayland] Unload all effects prior to destroying Xwayland connection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c1e9280efa56d8c9199801f259717a28dbb9c530'>Commit.</a> </li>
<li>Reset generic shader after zoom. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7b459214d9a7b78c1243318baed47b830579fde3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/108252'>#108252</a>. Fixes bug <a href='https://bugs.kde.org/355028'>#355028</a>. Code review <a href='https://git.reviewboard.kde.org/r/125994'>#125994</a></li>
<li>Abort max animation on unrelated geometry changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c04f7919c1f751d00cf7ae3f52bfa6dfabdcb6e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/336467'>#336467</a>. Code review <a href='https://git.reviewboard.kde.org/r/125989'>#125989</a></li>
<li>Wait for GL after swapping. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8bea96d7018d02dff9462326ca9456f48e9fe9fb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125659'>#125659</a>. Fixes bug <a href='https://bugs.kde.org/346275'>#346275</a>. See bug <a href='https://bugs.kde.org/351700'>#351700</a></li>
<li>Re-detect triple-buffering after compositor resume. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0788890233850ca598af55abe52641d7b4254f14'>Commit.</a> See bug <a href='https://bugs.kde.org/343184'>#343184</a>. Code review <a href='https://git.reviewboard.kde.org/r/125659'>#125659</a></li>
<li>[wayland] Don't pass keyboard events to Unmanaged windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4ebba6e13443f1056fa1bb57c07d5220e2abc31f'>Commit.</a> </li>
<li>Handle conflicts between epoxy and manually resolved function pointers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ac1dbac8b11f66a10e7871ae878e30a3e5d42100'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125883'>#125883</a></li>
<li>[wayland] Destroy input method connection after process finished. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fa6fbbdfc1f5b26ccc9e8168f5593f338c7fefbc'>Commit.</a> </li>
<li>[wayland] Destroy our internal wayland connection from server side. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0bf51d99f6183f82b93eec5997060fdcf6e5bc4d'>Commit.</a> </li>
<li>[wayland] Ensure we can tear down ShellClient after Workspace is destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3f4e73346845092a4fc555f03888c2398c299e0f'>Commit.</a> </li>
<li>[wayland] Destroy Xwayland's ClientConnection on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=affcbac7e7c3edeb457d5ea3d800d37ab1b6263f'>Commit.</a> </li>
<li>[wayland] Ensure to not call into x11 on Compositor tear-down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f75e53af60e715030f942c70db8b04cf5cf300c5'>Commit.</a> </li>
<li>[wayland] Don't leak our internal client connection thread object. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=acb0dfd8935182b9ae8f8554092eb8be077a6dd9'>Commit.</a> </li>
<li>[wayland] Fix cleanup handling on tear down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8175562a7afb6abffaad2550f792a4396951d3f2'>Commit.</a> </li>
<li>[autotests/wayland] Fix with Qt 5.6. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=51b44f4a879e4dcb53c1a685d58cfef20fa18543'>Commit.</a> </li>
<li>[wayland] Improve tear-down to not crash if X11 applications are still around. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1998d5ac1ad92011505f0e00761ccc618099fa19'>Commit.</a> </li>
<li>Only emit EffectsHandler::windowClosed if Deleted got created. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c09f4039d1a88b2d175647e642ffa3eeb4cd04e7'>Commit.</a> </li>
<li>Check whether Cursor is valid before using from EffectsHandler mousePolling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=936c52e2631d1236f3f5dcaad05b66a7ebc41186'>Commit.</a> </li>
<li>[wayland] Use Qt::AA_DisableHighDpiScaling in Qt 5.6. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7b9c21d63ca00597812d31d365d50c38af4dfe07'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126008'>#126008</a></li>
<li>[effects] Support GLES 3 in Blur and BackgroundContrast. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7983acea88ebbc419ab28d2bbfe16a3a1f6f04a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/324478'>#324478</a>. Code review <a href='https://git.reviewboard.kde.org/r/126003'>#126003</a></li>
<li>Use popup instead of exec on useractions menu on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4805316c0aefd38e4c60c25cf17c1034c847f944'>Commit.</a> </li>
<li>[wayland] connect to greeterClientConnectionChanged instead of locked signal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6e70dd0ba28f90c1eaff3dbb6964179203cbcfee'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4323102ec7dbd39647bd80df0275bf0bc9f72ac9'>Commit.</a> </li>
<li>[wayland] Introduce ShellClient::isLockScreen method. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a1371989e290d3df08cf0abd4d5eb298d329fa4'>Commit.</a> </li>
<li>[drm] Support configuring absolute output position. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cbbd684430cf3da7afeff1149a65fcd867766b5f'>Commit.</a> </li>
<li>Add a plugin for KIdleTime. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a8ff9d39a77292239be0f6b3c52d6ea5041187f4'>Commit.</a> </li>
<li>[wayland] Adapt to changes in the kscreenlocker. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c957b145a4127678b65490dfb9a41df61c6f4cf4'>Commit.</a> </li>
<li>Optimize string operations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a8d7d866ada5f24fc7313bfd094a076943ec8ce'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125933'>#125933</a></li>
<li>[wayland] Start ksldapp from the WaylandServer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bacfd876fecf2df72ba38d651f637a720e108581'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125954'>#125954</a></li>
<li>Preliminar support for task geometries in wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1f9fa64a4936ab85ccb2816c7aa60a01433a69b7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125873'>#125873</a></li>
<li>[wayland] Reset QT_QPA_PLATFORM to wayland after starting Application. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=19602a921e5cde85f4f989985dd52688ca91bcab'>Commit.</a> </li>
<li>[libinput] Event compression for PointerAxis. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5904349c69d5ecec43265c87097e1513476a067f'>Commit.</a> </li>
<li>[libinput] Pointer motion event compression. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=123e361f5505254bcf202d25cf3acd7e0e207d1e'>Commit.</a> </li>
<li>[libinput] Add an event queue. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cebd723c2c14beb02500cad9ed4bea6c13c88136'>Commit.</a> </li>
<li>[libinput] Use a dedicated thread for libinput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1cfd72f49fff8dca953616ef6467f564df13afb9'>Commit.</a> </li>
<li>[libinput] Make signals queueable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ea77d32259c1b5c9b4c035a03849015ddb5ad26b'>Commit.</a> </li>
<li>Fix compile with libepoxy < 1.3. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c50da5bdeea30400c7cede270b65a3156f3eac8e'>Commit.</a> </li>
<li>Add O2ES as possible value for KWIN_COMPOSE. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f2ad98aa3a59ecab25633ab161248b0e50aaec03'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125919'>#125919</a></li>
<li>Remove build checks for gles or gl. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aa4385230112f258bdfbfa976c758d8ebe536bc8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125865'>#125865</a></li>
<li>Remove remaining compile time checks for OpenGLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d5a5725db193317769d8eb075f7de4768986c5b3'>Commit.</a> </li>
<li>Core uses runtime checks for whether we are on OpenGLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=004b928c8dada6350500df3f5677c144aa2892e3'>Commit.</a> </li>
<li>Runtime depend on GLES in all egl backends. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=14d943caea71465f10cc198e6de320ec7292a7ec'>Commit.</a> </li>
<li>[plugins/qpa] Runtime depend on OpenGLES instead of compile time. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2f065b9c6ba6e699e2dfa65cef2b84189e940a54'>Commit.</a> </li>
<li>Runtime depend on GLES in SceneOpenGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4f64b32abaa77901bea8390b2c2f7bcc92b7b330'>Commit.</a> </li>
<li>Bind building of glx support on whether epoxy has a glx header. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=03231942bb65e56e0e04727b11dbda75f47993d9'>Commit.</a> </li>
<li>[kwineffects] Runtime checks for GLES in ColorCorrection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9919627106c47d07ea3c974a8b3076b5edca8820'>Commit.</a> </li>
<li>[kwineffects] Runtime depend on gles in GLPlatform. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=49d85025676a75ba8b4f31b4dccdc7c7f02e7ca5'>Commit.</a> </li>
<li>[effects] Runtime checks for GLES instead of compile time checks. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=78ac6aaf137dc400a373aa199881f978de104060'>Commit.</a> </li>
<li>Turn compile time checks to runtime check in kwinglutils.cpp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fc2805d2182d55dd7513088b6f4564ca5e225ad6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125865'>#125865</a></li>
<li>[glplatform] Ensure glsl is supported with OpenGLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=99ddcfbac02ea17b65e10b8070173eb99524bdd7'>Commit.</a> </li>
<li>[kwineffects] Runtime detect whether we are on GLES in GLPlatform. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1170303fc6ee6878826340ac6a74001db6c87825'>Commit.</a> </li>
<li>Don't activate clients (autohide) if we're dragging a window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cd9f43af7d95030e29483c1dbb3ceb80e2bbf20a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352267'>#352267</a>. Code review <a href='https://git.reviewboard.kde.org/r/125867'>#125867</a></li>
<li>Re-evaluate rules on title change. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b2755bcaa70027e9f9d14e2ddf221e3fd9df139f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/220227'>#220227</a>. Code review <a href='https://git.reviewboard.kde.org/r/125427'>#125427</a></li>
<li>Desktop grid: allow desktop to cross screens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3b71411112252f17475c882f78ea929647520756'>Commit.</a> </li>
<li>Desktop grid: general cleanup. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c2acb5ad6379ee27024361123417986c0bfc26c4'>Commit.</a> </li>
<li>Desktop grid: set desktop when moving stickies. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=454f704647798ad9d83df66af0f8c70157774caa'>Commit.</a> </li>
<li>Desktop grid: less branches for isOnAllDesktops. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=66ed2d39a0cd3461f822246e4076d14cfce9f67b'>Commit.</a> </li>
<li>Desktop grid: brightness doesn't flicker here. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4a439bd3cfb89f7e2ab2fc3846d0fa3f134cef02'>Commit.</a> </li>
<li>Desktop grid: zoom hovered window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=14d1fdf63fc27d908f062695e85fa55dde725b55'>Commit.</a> </li>
<li>Desktop grid: vector instead of qhash for buttons. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d1cf29f6b27ddfb7a1f5f1f36fb6b12ebdb26146'>Commit.</a> </li>
<li>Desktop grid: offset buttons from screen edge. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c0e61c3e1a9a2e8d3f8bd61ee9ffad43928aa3f5'>Commit.</a> </li>
<li>Desktop grid: add option to hide buttons. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fbfc573a8ab421c33b46a6a638747e0517e93c57'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352588'>#352588</a>. Code review <a href='https://git.reviewboard.kde.org/r/125228'>#125228</a></li>
<li>Desktop grid: use -/+ text buttons. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4fc9f44d2afd4e6a1e4290bb119485a58875799f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354131'>#354131</a></li>
<li>[hwcomposer] Adjust present strategy for block on retrace. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e379d06f34feb175ac7fadf373e938e4369297c3'>Commit.</a> </li>
<li>[wayland] Add support for move/resize triggered on ShellSurface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c35c464b1d96093d5f93159fb0f7022d81ea0d78'>Commit.</a> </li>
<li>Make most windows minimizable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8fe8bf59a5c8c3cadcbb60617d80bf2aff8c7d79'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125842'>#125842</a></li>
<li>[wayland] Drop check for QtWayland 5.4.2. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7cc566681dbcdfe24ba1f51a5561eda605d152cd'>Commit.</a> </li>
<li>[wayland] Force Qt::AA_NoHighDpiScaling on Qt 5.6. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=28992c3d3106d9c104f145548ce18a31af44449a'>Commit.</a> </li>
<li>Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2c4ed0aca102be6f9a90535ec42b7a102ca7e24b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125843'>#125843</a></li>
<li>[wayland] Sync resizes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=89a4df651d22faa3c5de67999340c1037418975b'>Commit.</a> </li>
<li>Make it possible to end move/resize through mouse button release. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a23a9d38f98fd34efce7507a2f1906b97c6f826f'>Commit.</a> </li>
<li>Fix mouseChanged signal arguments in InputRedirectionCursor. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7dcd69fdccbfb7dd9d778cb8cadd4aa4d8adf31f'>Commit.</a> </li>
<li>Move implementation of (shrow|grow)(Horizontal|Vertical) to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f1215e44d448a78224b80f386f0581cb5593d9e4'>Commit.</a> </li>
<li>[wayland] Fix quick tiling auto test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8f2b01b5490070781d5f01bbc1df26c9b017cbbe'>Commit.</a> </li>
<li>Consider all client in Workspace::packPositionFoo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=45fb1680fc7a8e14e6d10ef52e011a073e23ee83'>Commit.</a> </li>
<li>Move implementation of Client::packTo to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b19da3cb14b86e7e93d2abc7c81f227001904430'>Commit.</a> </li>
<li>Merge setting up client and shell client connections in EffectsHandlerImpl. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1a664724943325cc5e38d57560ae82a2acd6129e'>Commit.</a> </li>
<li>Move geometry related connects from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d4cd0b26c188776b61d13314377f665e091114e'>Commit.</a> </li>
<li>Allow moving of Wayland windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4e7521fe64932bbb8f18883f363d62a067a01dad'>Commit.</a> </li>
<li>Use AbstractClient for keyboard moving of clients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ac1f01fd3fdff3a099034d3c1f1f724dfffaeb21'>Commit.</a> </li>
<li>Move keyPressEvent(uint) to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d2251875f6c95a092ba7546001e5fb545f6c19a'>Commit.</a> </li>
<li>Move move/resize related mouse command handling to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=717a48a2a1855e307a852dc36747e36e28923244'>Commit.</a> </li>
<li>Move implementation of updateMoveResize to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f5848d940581c2cdebb21476676c011017edc4c0'>Commit.</a> </li>
<li>Move handleMoveResize to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f4b02d5a8c7fdec8861ee91e93d158c1c44828a3'>Commit.</a> </li>
<li>Move checkQuickTilingMaximizationZones to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dcff41ab403ebf12c8702a7b75ada5dffb1b8925'>Commit.</a> </li>
<li>Move performMoveResize to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7e23860957f494ee8408a7fa4ac2e6106811f7ea'>Commit.</a> </li>
<li>Provide positionGeometryTip() as virtual method in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=43e40719750a9f5a6ed749055e0d0f5eef83290b'>Commit.</a> </li>
<li>Move startMoveResize() to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c83f041005470b83ef5f3ca20618fbc5bf777060'>Commit.</a> </li>
<li>Move (start|stop)DelayedMoveResize to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9e323227a1b5e42f200e57853606fd4c940209dd'>Commit.</a> </li>
<li>Move s_haveResizeEffect from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=95a99d337a07577a5bc294858743ff749276590e'>Commit.</a> </li>
<li>Move finishMoveResize(bool) to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ba53407b52ed8bc1df72b1affd5a49b16e72bfd5'>Commit.</a> </li>
<li>Provide leaveMoveResize() as virtual method in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1c40e809ea7935bc22dc3bb706492dbf174b2422'>Commit.</a> </li>
<li>Move signals clientFooUserMovedResized to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=88e097ec16ae84292ca625ce43067b510423b289'>Commit.</a> </li>
<li>Move moveResizeStartScreen from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dc04cdef64e9adb4d8f34de5cecf14d2abd715f8'>Commit.</a> </li>
<li>Move updateCursor() functionality to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dcb5e293163484f9c27704984268007b0016d2bf'>Commit.</a> </li>
<li>Move buttonDown from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b8e68307bb4064c673f6034b7ab0df155202fbf8'>Commit.</a> </li>
<li>Move checkUnrestrictedMoveResize() from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=605acaf60eecf83ec0bdcb58d62947cc56ae2cb7'>Commit.</a> </li>
<li>Move properties move and resize from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c9dbd94812c46fb9783e7ef78448b9498ebd0f20'>Commit.</a> </li>
<li>Move the Position mode from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8a3be2bacd08ca0ea2219e92bd87cafe7703a2fc'>Commit.</a> </li>
<li>Move moveResizeGeom to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c3f14521c101ce237b8520d03b5be5ab5c35b1e7'>Commit.</a> </li>
<li>Move initialMoveResizeGeometry to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b4fc4d72357e786e73609a542b12487da31f1320'>Commit.</a> </li>
<li>Move (inverted)MoveOffset to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=84c7575cbf5dac4ff5df9ae55d4d29e2f498f8cd'>Commit.</a> </li>
<li>Track whehter moveResize is unrestricted in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3514b4d691a21b6f3deb6d920d47e02d68d9590c'>Commit.</a> </li>
<li>Track whether Client is in moveResizeMode in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b5e8e3511e1df0592b3b1831325d8dcf3407d6ee'>Commit.</a> </li>
<li>[wayland] Don't call into Workspace from ShellClient during teardown. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3fa9013691e7e5386b450f7ee2fec797ca225133'>Commit.</a> </li>
<li>[autotests] Test case for 10ad9262a184e1afc088bee35b7fa4c188d9d63f. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=58bcf66ecfd95dcb7f161baba096e37655d99212'>Commit.</a> </li>
<li>[wayland] Dispatch the WaylandServer once more before killing internal client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7253235a288a403fc2a14178fe38f2d99d5ea855'>Commit.</a> </li>
<li>Declare metatype for Deleted and export Deleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3619d58044f9412a20a4051696bab024f342e27b'>Commit.</a> </li>
<li>Drop PreventScreenLocking electric border. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d37ccfce0907f4e8a393ae20c87d71fa65d9f9f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125701'>#125701</a>. See bug <a href='https://bugs.kde.org/331841'>#331841</a></li>
<li>Drop remaning code for ShowDashboard. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c408e9ccec2579e34f287c911f2d883b7bf9cc6f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125700'>#125700</a>. Fixes bug <a href='https://bugs.kde.org/353928'>#353928</a></li>
<li>[hwcomposer] Rework the vsync code. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=93b5e13308bfc8e5b546fff63d926f26ec3c4a54'>Commit.</a> </li>
<li>Don't perform Compositor::setCompositeTimer during startup. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d24411b9ac0439c3b75dcc5849e75267b77a1d1e'>Commit.</a> </li>
<li>[hwcomposer] Use three rendering buffers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b38999e0a93e4dc43ccd265a798a510a77f51515'>Commit.</a> </li>
<li>[hwcomposer] Use newer setOutputsEnabled API instead of blocking buffer swap. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=98a669d70f0ce3c0f8372345aa35a2e2731e42a4'>Commit.</a> </li>
<li>[autotests] Dashboard is no longer available. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f8e8985df84200800dedb29b728e55e828321d4'>Commit.</a> </li>
<li>Drop the Dashboard Effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=126263b6dbffa8dc0cc86cc8dc2e89ce66841408'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125686'>#125686</a></li>
<li>Delay enabling vsync till first frame is rendered. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d2cb445f4c3b677a004b3be1c931ae60cf1049e7'>Commit.</a> </li>
<li>[backends/hwcomposer] Add a failsafe timer for vsync events. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=70e744fe26d60770740d72f2518743c3fd345467'>Commit.</a> </li>
<li>Force grab on useractions menu. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=801e60b2907690bf6de659a313131b85d3a1e1ac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351112'>#351112</a></li>
<li>Call checkWorkspacePosition also for Wayland clients in ::updateClientArea. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=226c9a0367d9114f78aa50c85366e8c1b65fc4cb'>Commit.</a> </li>
<li>Merge back implementation of Client::move and ShellClient::move. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8cdfb0e88ff118b56f8ca780806f45ebc30fa66f'>Commit.</a> </li>
<li>Add virtual AbstractClient::updateTabGroupStates(TabGroup::States). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c5787206155228816d16a373b8b07d67b622f5b3'>Commit.</a> </li>
<li>Move functionality for geom_before_block to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d7b4d8fe8250bdb734a63d0694f1c1153909692a'>Commit.</a> </li>
<li>Move handling around deco_rect_before_block to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a186e407bf002acb901863618fac2167f24aa7bc'>Commit.</a> </li>
<li>[hwcomposer] Add support for vsync. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f36af69a0edb5a3b6bec691599d7450fd566f844'>Commit.</a> </li>
<li>Use AbstractClient where possible in EffectsHandlerImpl. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=03074c925ce97e3144c7e08695beffdf982214de'>Commit.</a> </li>
<li>[wayland] Update geometry in ShellClient::setGeometry directly if size didn't change. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a2b83bffc5aeb764da02ab1e740265a5cb3a1ec6'>Commit.</a> </li>
<li>[backends/virtual] Add possibility to have multiple virtual screens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5558d622200fb12bb8a5ef709114a3bf6d866271'>Commit.</a> </li>
<li>Move implementation of sendToScreen from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6049b9ff3ceaf1603cde3700ed62367d843eb0eb'>Commit.</a> </li>
<li>Fix unit test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0e1e85d81cab2938a49f26360f2698126885dbdc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125635'>#125635</a></li>
<li>[backends/hwcomposer] Drop dependency on hybrissync library. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=66e27b69bb5a66e4dceddaf605b33c62183051b2'>Commit.</a> </li>
<li>[wayland] Use first valid geometry of ShellClient as restore geometry. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=86448c66ddf8351c38f5511ba4ec2d3a4b14c3b9'>Commit.</a> </li>
<li>[autotest/wayland] Add a test for quick tiling Wayland clients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=996c828da3f32fe3c6f4c36191c778b0a3b19822'>Commit.</a> </li>
<li>[wayland] Allow resizing on ShellClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=41af9c400c5d528052c305d36dc38fc7679e6312'>Commit.</a> </li>
<li>Move quick tiling from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9f91431e8c46b057bca18df4d63ebbba47c32069'>Commit.</a> </li>
<li>Introduce a virtual AbstractClient::updateQuickTileMode(QuickTileMode). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9f2b4c7d1454ea0d5918d254f2000885a45c3bc5'>Commit.</a> </li>
<li>Use quickTileMode() instead of quick_tile_mode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fa18d60e9e8ffd5031e4152b2a2a1deba9f27f2b'>Commit.</a> </li>
<li>Use (set)GeometryRestore() instead of geom_restore in Client::setQuickTileMode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=61caf788e38d0ca4fb9aca3c876992c90ae1a933'>Commit.</a> </li>
<li>Add a pure virtual AbstractClient::setGeometryRestore(const QRect &). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=15f588642680c511240db3f0f0ae08060f3dee7b'>Commit.</a> </li>
<li>TabSynchronizer operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=94b5ca0d2ba0dc3ea73617c10810865e7f63dd19'>Commit.</a> </li>
<li>Move blocking geometry updates functionality to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9ef42ae3c7d33ff01c49f6f9322f7848a519ce08'>Commit.</a> </li>
<li>Provide isDecorated() as virtual method on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=646eeb8bbbabd18f1f3ca66d0c56540dbe177cd0'>Commit.</a> </li>
<li>Use isDecorated() instead of m_decoration in Client::setQuickTileMode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=727929db8a8f35dc462df854f97fa084bb5f6117'>Commit.</a> </li>
<li>Move implementation of checkWorkspacePosition to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ec6c778df7f9ab86bfa5c04a6f72eff577b06359'>Commit.</a> </li>
<li>Add setGeometry to AbstractClient as pure virtual method. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=696cdb9e3913afa379e2bfc287d434f2aa1ffc10'>Commit.</a> </li>
<li>Improve sharing of maximize between Client and ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eacaf25acfa5a285a51413bc35a4011e74b22b34'>Commit.</a> </li>
<li>Move adjustedSize from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=60342d44e3fd91c3e3b381439e8b67106a91fd7a'>Commit.</a> </li>
<li>Add sizeForClientSize as virtual method to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7c49b1ca42224b7f1f659b6d55e09f00a4ae1a34'>Commit.</a> </li>
<li>Move enum Sizemode from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=37fc20d3c0b70a4a4641bccfab428220377d9982'>Commit.</a> </li>
<li>Add border(Left|Top|Right|Bottom)() const as virtual methods in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=acb3a142000afba41cc3ca740f5d22d27a484638'>Commit.</a> </li>
<li>Use quickTileMode() instead of quick_tile_mode in Client::checkWorkspacePosition. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e121d4a10730e362690c21b3529cbe62de03f87f'>Commit.</a> </li>
<li>Provide quickTileMode() as virtual method in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ee47ce613d2ac988f60ee9f5dd43a3796ed63b08'>Commit.</a> </li>
<li>Use geometryRestore() instead of geom_restory in Client::checkWorkspacePosition. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d1baa4d2837a5353b2c53b10d473dc8ee46def1c'>Commit.</a> </li>
<li>Adjust to newer upstream libhybris version. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=28fe78a94f7d833eb2624300c409aca0d08cc3c8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125606'>#125606</a></li>
<li>Drop Android input handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=425e703dffe2f51380c64cf2db52f656d2ac39f8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125608'>#125608</a></li>
<li>Add hint similar to autohide to raise/lower a window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=04ab8554aacb5d39093aeb5fab0849e584b4fe82'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124272'>#124272</a></li>
<li>Move checkOffscreenPosition from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=78e9a56cd1513de8ecf864354c2d9f82348009a2'>Commit.</a> </li>
<li>Move (is|set)ElectricBorderMaximizing from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=53b87dbbd9e6bc34ac01bc785c1cc6e274cbc158'>Commit.</a> </li>
<li>Move electricBorderMaximizeGeometry from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=226a099366de5100b509bd3225c58699c3847000'>Commit.</a> </li>
<li>Make geometryRestore() pure virtual in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=32fbbc90bcd19f2ea1b0e5eef201a6983d6798d0'>Commit.</a> </li>
<li>Move (set)electricBorderMode() from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9b8836b455108d30b1f085d0a1761ba0fb3bb663'>Commit.</a> </li>
<li>Move signal quickTileModeChanged from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b489ebabdce355d9d59b93ac6a83085db93f2df3'>Commit.</a> </li>
<li>Initialize AbstractEglTexture::m_image. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=68b7274c5da2c555c7afdaabcec2d84c5e6073e8'>Commit.</a> </li>
<li>[autotests] Enforce QPainter compositing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=80b6a26a09b97844ebe11233a8da6e4bbf984024'>Commit.</a> </li>
<li>[autotests] Enforce OpenGL compositing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4a35a63d2df2ee1e08e3b12549f0f6613ccf0f0f'>Commit.</a> </li>
<li>[backends/virtual] Add a virtual rendering OpenGLBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a025791d7bdb3b9ddde4d238bf911319ec0a7009'>Commit.</a> </li>
<li>Allow OpenGLBackend to hold a surfaceless context. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0fc1142ca229dcc40d7fd29ee3eccc633fe3955b'>Commit.</a> </li>
<li>[backends/virtual] Move save screenshot functionality to the backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2e888da70ecfbc38fecebed689175c1737337c06'>Commit.</a> </li>
<li>[wayland] Honor ShellSurfaceInterface::acceptsKeyboardFocus in ::wantsInput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=29b2082daaaaff07a379a2c66d0c180fbfe356c3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125553'>#125553</a></li>
<li>[wayland] Proper metatype for ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0bb4498e1de8bdd43468a911e830d7f958e65eee'>Commit.</a> </li>
<li>[wayland] Keep transient state directly in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3cb84383995664ca37d5e56fe70a05f098391f44'>Commit.</a> </li>
<li>[wayland] Keep fullscreen state directly in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b35c3e8501da5f837727a0e670605db275568b16'>Commit.</a> </li>
<li>Do not grab server during manage. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f3cfbacb1e4c6cb68c0685431d0d1abedf3555ce'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125474'>#125474</a></li>
<li>Do not reinterpret_cast insufficient data. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a38b21f49961ffc4c2b6a6327a7a933a5278636'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341010'>#341010</a>. Code review <a href='https://git.reviewboard.kde.org/r/125513'>#125513</a></li>
<li>Return sane screen geometry on missing randr. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7e72aee474917a3b7f70d0666fbfdc90e060f70d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125456'>#125456</a></li>
<li>Overhaul of deco kcm. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ed2314dfbef88cd59ed5094d75be6e0be60b6aab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125393'>#125393</a></li>
<li>[globalaccel] Ensure we don't call into deleted InputRedirection on shutdown. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=be7e9249cf870d2eacb3881cc5c4d95611afe050'>Commit.</a> </li>
<li>[wayland] Rework command line checks for platforms. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eed5531b4f7fe4d68ebf0725448eb84fcb837839'>Commit.</a> </li>
<li>[autotests] And remove the problematic code. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b74cae4dbe0887b6d0a97cf4c87a4f9d70d2eb9c'>Commit.</a> </li>
<li>[autotests] Only use QTest::setMainSourcePath if we have Qt 5.5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4b917da861ce1c61da5fa6eceee0214246d0513c'>Commit.</a> </li>
<li>[autotests] Don't use QTEST_SET_MAIN_SOURCE_PATH. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f5fbdaa6ca42bcaab195c9d4c2756ae253cbf8f4'>Commit.</a> </li>
<li>[autotest] Welcome to integration testing KWin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7fed20f13687035d6aa38e9ffa850259f757343d'>Commit.</a> </li>
<li>[wayland] Add a virtual framebuffer backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bfa9646d1dc695698dc948f73a08d82c924022d8'>Commit.</a> </li>
<li>[wayland] Add options depending on available backend plugins. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=007a31709950f2251fef083f27b12829e6554f20'>Commit.</a> </li>
<li>[wayland] Add a --list-backends command line arg for listing available backends. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fc0272f3c44ffa16364a2f1505b0858ed1c3816b'>Commit.</a> </li>
<li>[wayland] Trigger an update of client layer when managing a ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ffca66d94bdf793cdcc83b40a4f761949d5da91'>Commit.</a> </li>
<li>Move layer updating for setActive back to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b5255de4b6075c34d66b14b79976587148c05877'>Commit.</a> </li>
<li>[wayland] Introduce better placement checks for ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b587926803049b08a3b70e52a566f5cc597d321d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125469'>#125469</a></li>
<li>Remove unwanted debug and change in 9912d84. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0bf64446cdd6a340d28c1a147c9244d7c0285d3d'>Commit.</a> </li>
<li>[wayland] Properly support add/remove transient on ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=242e2806ca0e3295f9fa46833a6e8142503b3122'>Commit.</a> </li>
<li>[tests] Support a trasient window in the waylandclienttest. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=48a62729166f7f07f700ada6771541afb07356e5'>Commit.</a> </li>
<li>[effects] Properly announce/remove support in blur/contrast if shader fails. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=044e2a05b266b9cba8ab05f90a47832c5ee9d8d8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125444'>#125444</a></li>
<li>Supports kwayland skipsTaskbar. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9912d84f746c1dd3c33befc0fb469025a99fad10'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125454'>#125454</a></li>
<li>[tests] Add fullscreen mode to waylandclienttest. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=748754bce10ee543f97331532b781a68a8357ef6'>Commit.</a> </li>
<li>[autotest] Add workaround for broken no-XRandr in screen edge test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eda4f6103707bc425dec884c3fe4dac1077b21a7'>Commit.</a> </li>
<li>[screenedges] Add a missing context to connect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8073d851c4099418d2349c8c1935ed573815e835'>Commit.</a> </li>
<li>[backends/x11] Allow grab keyboard/pointer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e5fbb20e8b7d36219bd6dd20c56b40388939f878'>Commit.</a> </li>
<li>[backends/x11] Use NETWinInfo for our window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4fa01583cb6a46548944f060c9fe6246145a424e'>Commit.</a> </li>
<li>Shuffle mnemonics in "more actions". <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b0dc3b3009d29b56fe886bcf48ed21cc4a002ec8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125434'>#125434</a>. Fixes bug <a href='https://bugs.kde.org/319695'>#319695</a></li>
<li>Initialize pseudo_max with desired maximization. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ace742d143a0156691ec79a747acced1a37e56c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352185'>#352185</a>. Code review <a href='https://git.reviewboard.kde.org/r/125037'>#125037</a></li>
<li>Prevent calling xrandr w/o extension available. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0302b97aea6a686b9d62c2d8c63b736cb9d93128'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343844'>#343844</a>. Code review <a href='https://git.reviewboard.kde.org/r/125074'>#125074</a></li>
<li>Enforce update on re-redirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d04645266c13afe5fba570a4acdf1d3fa67ff7e4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125039'>#125039</a>. Fixes bug <a href='https://bugs.kde.org/342881'>#342881</a></li>
<li>Support changing tabbox mode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cb1005120a6aaef4d3e894cb66315112354cf9cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/316544'>#316544</a>. Code review <a href='https://git.reviewboard.kde.org/r/125392'>#125392</a></li>
<li>Add black/whitelist config to videowall. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=121d59ee28709ca0229930f86d77d8e71fb9024d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125410'>#125410</a>. Fixes bug <a href='https://bugs.kde.org/353153'>#353153</a></li>
<li>Improve restricted moveresize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d61e6cd4458609c30826a2b14682dad2908133dd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125428'>#125428</a></li>
<li>Fix connects with QPointer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9f054bd8ad236b331f7230b9bca71205152a89de'>Commit.</a> </li>
<li>Fix up delayed quick tiling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=af4809134c4c9f91a098f4da958a53f347efba01'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352744'>#352744</a>. Code review <a href='https://git.reviewboard.kde.org/r/125250'>#125250</a></li>
<li>Workspace::adjustClientSize operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6dc211dfaf0cd9b4b51b1466508af3a9d65773f0'>Commit.</a> </li>
<li>Workspace::adjustClientPosition operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=47d2b4cb49aaf98378c3ad780464c93edc3e5c84'>Commit.</a> </li>
<li>Introduce an allClientList in Workspace. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=51888e8abdac9cff5744cd5e487ab73aa46a851c'>Commit.</a> </li>
<li>[backends/x11] Set a black background pixel on our rendering window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6326d96c02beba482527b842f8d9d4a5c6657664'>Commit.</a> </li>
<li>Fix regression in Workspace::activateNextClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=23f1b04a71afddb0adc4cb5e9f8f496df1a814d3'>Commit.</a> </li>
<li>Allow transients to go over dock windows under certain conditions. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=000954c50857009282044b3f415ada7643a4bc24'>Commit.</a> </li>
<li>Reintroduce nullptr check in Client::removeFromMainClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a6118016ecdb097a4d99a5dc1500dc7813fce729'>Commit.</a> </li>
<li>Add safety check for recursive transient for. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9988fa9d74bb77a20f0b63b267c6600fb2c55437'>Commit.</a> </li>
<li>Move layer functionality to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d25c46521171a84df967131aa4348ee1c9d6d2d3'>Commit.</a> </li>
<li>Support raise and lower windows on wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=49e5e81970ea780b116c37c0a98428a9e58f8e59'>Commit.</a> </li>
<li>Workspace::raiseClientWithinApplication opertes on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fc5b303c5f6d97990a87b4c81d26d847211f1a9f'>Commit.</a> </li>
<li>Workspace::lowerClientWithinApplication operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e46bf873111b64170d2019bfc41e79eb856178dd'>Commit.</a> </li>
<li>Pass through mainClients for AbstractClient in EffectWindowImpl. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c4f02a66776ef63dcd22634ef414ce1eee13ba81'>Commit.</a> </li>
<li>Fix incorrect static_casts to Client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f84b4758f2e75b1f8627cd692402569713cadb7f'>Commit.</a> </li>
<li>Workspace::topClientOnDesktop returns AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f429625f09c03da46450b0ff26b0c03a33e6315'>Commit.</a> </li>
<li>Workspace::findDesktop returns AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9d8926d3c142de10cb315b35382f10ef125a6e4'>Commit.</a> </li>
<li>Move functionality of Client::updateLayer to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a421f546cd44505fe27d64d449d63f1222e10053'>Commit.</a> </li>
<li>Move functionality from Client::doSetDesktop to AbstractClient::setDesktop. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b19935677402a9fd14f58a2ad0d495fd57d7cb87'>Commit.</a> </li>
<li>Workspace::updateOnAllDesktopsOfTransients operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4a3d0fdac6812af0bd7e99d5ccde5f7fe5b4f945'>Commit.</a> </li>
<li>Remove no longer needed casts to Client regarding transients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=69be73d382eb490fb60c9c39dc66112a75cf5191'>Commit.</a> </li>
<li>Fixup with removeTransietn. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=000300866834a32b2075ad6e4aaab4a653650c4d'>Commit.</a> </li>
<li>Workspace::constrainedStackingOrder supports transients on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4e6ea0808a54bb9820ff29d82a5903d9a43170cd'>Commit.</a> </li>
<li>Move transients from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2f7597e522f9fd156e4801fcd51e92bba3e8b53e'>Commit.</a> </li>
<li>Use auto where we call ensureStackingOrder(client->transients()). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2da04aa26b46e945d61f4f7457fcb56d10c8e621'>Commit.</a> </li>
<li>Workspace::ensureStackingOrder can operate on QList<Client*> or QList<AbstractClient*>. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4ad749e5604b8fe0aeac00adc2d6775db0c37b64'>Commit.</a> </li>
<li>Better support AbstractClient in Deleted::copyToDeleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ba8d11b3057fde8d74f798f8fb30fdc738c1ff41'>Commit.</a> </li>
<li>Move modal from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=305699be8e1539854f82705459e0529e44af3a12'>Commit.</a> </li>
<li>Move mainClients() and allMainClients from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d242d9daf1c030fbf5c8a10220bccb2a70fd2f0'>Commit.</a> </li>
<li>Use auto for iterator over transients(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a5d331764560af5199a367f3fe8bbf64548c36a8'>Commit.</a> </li>
<li>Move hasTransient from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df98e3883f6e6450a888cbd14ac120eb309d4824'>Commit.</a> </li>
<li>Workspace::keepTransientAbove operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1e00e1597a56a2671d429dfd49bbd6474a45c513'>Commit.</a> </li>
<li>[wayland] A popup shell surface doesn't want input. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fda5a5feeaecfd36f5321c341c8d28e360d20283'>Commit.</a> </li>
<li>Add a placement strategy for transient ShellClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a86a7e7b3cae90893de1280b3c886c80cb56c116'>Commit.</a> </li>
<li>Implement transientFor in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0935a60b38d7cfddec55e35443e43895bc0e2fb8'>Commit.</a> </li>
<li>Move transientFor from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c4c3dfc73f28020537cc83e58dc675c92f615599'>Commit.</a> </li>
<li>Move transient property from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=07dd743412ad32255b726b0b7005501670b189f7'>Commit.</a> </li>
<li>[wayland] Don't delete Surface we got fromWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=16e9c8375c59b27e2df9cca927604b123910d8f1'>Commit.</a> </li>
<li>[wayland] Configure key repeat. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cd254c8f47bfd4c9227bebd953fcf1098d9089f0'>Commit.</a> </li>
<li>Delay QuickTiling indication on inner screenborder. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a1f11d21347032f7d1b8fd43d30ee7d3ce5ede0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352050'>#352050</a>. Code review <a href='https://git.reviewboard.kde.org/r/125024'>#125024</a></li>
<li>Support the slide protocol. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5a557270565e22284c3e5fd284f0ba7ec27fb22c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125120'>#125120</a></li>
<li>Fix build with some compilers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=873b4ed717e430131bb56f374cc7d6db33a2d827'>Commit.</a> </li>
<li>[wayland] Add support for DpmsInterface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=43f6c1e04132f27acfe9098d5e9a1a4cbc854fdc'>Commit.</a> </li>
<li>[wayland] Don't run Compositor loop if all outputs are disabled. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=679da47235b4c4d523d7f3e3b8ccac1f8a9a6891'>Commit.</a> </li>
<li>[drm] Mark outputs as disabled in backend depending on DPMS state. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ffa075490ef7fc216108ab44c6c88bc27ad10a49'>Commit.</a> </li>
<li>[wayland] AbstractBackend indicates whether outputs are enabled. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4f1033f17bb062b6053a3fdfc1f04b5df0df3ddd'>Commit.</a> </li>
<li>[drm] Re-enable Output on input event. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e3ff85d4d9dc9ff0817ee1803ce692debf1b3b6b'>Commit.</a> </li>
<li>[drm] Make DrmOutput a QObject. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6281cf84b47cd113d70c421037b29055f1dfd44e'>Commit.</a> </li>
<li>[drm] Don't present on an output which is in dpms standby. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a1bcc8e84898ae700b68df1b2fcfda75dced7731'>Commit.</a> </li>
<li>Add signal InputRedirection::keyStateChanged. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=66ad99787c688473b9ed1b46d92c92659d262587'>Commit.</a> </li>
<li>[drm] Add initial support for DPMS. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=902bdb6cf0b76a0983422355b022947f172fa05c'>Commit.</a> </li>
<li>Add support for modifier only shortcuts on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=07414e88a5dca8abe00e555b9ea94189b429a5c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124954'>#124954</a></li>
<li>Support OnScreenDisplay from wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=658a28a25785fa94e6d7cf65903287fee07b7c1d'>Commit.</a> </li>
<li>Support the kwayland contrast protocol. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d3053fab58bc20c7a239683badcd8b369a3a9f51'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125035'>#125035</a></li>
<li>Use the kwayland blur protocol in the blur effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3f5bf65a9ec8fa413ca59dd07df267c8b765329c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125017'>#125017</a></li>
<li>[cmake] Let's try PKG_Qt5PlatformSupport_INCLUDEDIR instead of PKG_Qt5PlatformSupport_INCLUDE_DIRS. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f280d36f8d5e96e495d9684a3d0061d51dfe8b90'>Commit.</a> See bug <a href='https://bugs.kde.org/351776'>#351776</a></li>
<li>[qpa] Use QPlatformIntegrationFactoryInterface_iid for Q_PLUGIN_METADATA. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f0f8421cef956c8794915f0dcd99067edb36c99e'>Commit.</a> </li>
<li>[wayland] Create event dispatcher in QPA plugin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=abb9bf13556f461d3f9a1b52e875d3a0fe91a739'>Commit.</a> </li>
<li>[qpa] Dispatch Wayland server before trying to find a ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a10be8a7d72b1efb3272f23fc436b5e8af372c1f'>Commit.</a> </li>
<li>[wayland] Create waylandServer after creating QApplication. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ef20f82b4d4793b4f94199ca2d1775af1479f745'>Commit.</a> </li>
<li>[wayland] Drop workaround on Qt window expose event. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f034537ad76484a3297cd7a21f68ae65ea0aad9c'>Commit.</a> </li>
<li>[wayland] Drop workaround for faking frame rendered for Qt windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9afbecf4a6385bf7e3db4eb1999e755aad6e98f5'>Commit.</a> </li>
<li>[wayland] Drop hack for faking input for Qt popups. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8add14fe891f9e59f20bf1a1d9c69604c8e83f1f'>Commit.</a> </li>
<li>[wayland] Add repaints on geometry changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=28b48e69fb52ae00759dc0a88a10291a6e379873'>Commit.</a> </li>
<li>Keep Qt::Popup windows as internal pointer window once they leave window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=296313b2fc80a7539e53a5483e34850b57c51620'>Commit.</a> </li>
<li>[wayland] Send QKeyEvent to internal windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=afd76b80ee1c05c6dc643e4736b69f0868367d57'>Commit.</a> </li>
<li>[wayland] Drop QT_WAYLAND_DISABLE_WINDOWDECORATION env variable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cd0e9052486c7678afab9f866593cdc3174448d2'>Commit.</a> </li>
<li>[wayland] Drop threaded eglInitialize hack. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc27042a7bbb11ed0462ee41f352626bc556e19d'>Commit.</a> </li>
<li>[wayland] Drop QtWayland specific BypassWindowManagerHint workaround. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e90fc1f8e1113f8092b9afa1097e3dc1445a6b20'>Commit.</a> </li>
<li>[wayland] Remove the specific socket pair for QtWayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e0103b15b752adff7617f0e9a4850eafa87f84fa'>Commit.</a> </li>
<li>[wayland] Enforce our internal QPA plugin as QT_QPA_PLATFORM. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4851470ff9a62626d2eda364ebcbf3b3f83889b9'>Commit.</a> </li>
<li>[wayland] Add a QPA plugin for kwin_wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26b3569a0b4f5b298141d2b3bb3d9f8a3176eedd'>Commit.</a> </li>
<li>[wayland] Export ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=79317717c8beb64d024637df3c6d101372a92f6c'>Commit.</a> </li>
<li>Composite windows from a QOpenGLFramebufferObject. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c56bbc0ddfd6613ac8b357a0e284801bb7532c96'>Commit.</a> </li>
<li>[wayland] AbstractBackend announces whether a surface less context is possible. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d8378306613c816ba7de60cf1706dd9f183542b5'>Commit.</a> </li>
<li>[wayland] Forward EGLDisplay and EGLContext from Scene to Backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b9d7726f7a35e0c47aef12082c61b26d0e26714d'>Commit.</a> </li>
<li>Add a WaylandServer::findClient which takes a QWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9272d01a2dcff73832d3d432d59395a3ad4bfec5'>Commit.</a> </li>
<li>Add SceneOpenGL::backend() const -> OpenGLBackend*. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f76c18bfb8eea86ecddd703a6e1ee577b1ba2876'>Commit.</a> </li>
<li>[wayland] Use an event thread for the internal Wayland connection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9e5343eb6e9a69801ef1e19a90c004a0b76c894'>Commit.</a> </li>
<li>[wayland] Keep Registry for internal connection around. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6294961ba4e4cecf762ae28af012eaa5b4bf7229'>Commit.</a> </li>
<li>[tabbox] Support wayland in establish/remove TabBoxGrab. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f7925c586ef3426bc0f236aa4209d3bd94532e92'>Commit.</a> </li>
<li>[tabbox] Split areModKeysDepressed into X11 and Wayland variant. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=304ac3ac8415d0a28733b08829ea25a9463e8bc0'>Commit.</a> </li>
<li>[libinput] Don't change default log level. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6b95705f246be25d0426c9463adfb7e6b1f77d99'>Commit.</a> </li>
<li>Fix moving windows in InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=73b8b3ec7beb76d0869408d83617c116598ab178'>Commit.</a> </li>
<li>Do not try to open VirtualTerminal through logind. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cfddaed6df06998f44a140457ff72ff271e14e72'>Commit.</a> </li>
<li>Make X11_XCB a build dependency of X11 windowed backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d946e37629b56be5b57931c1a3f0463dc8bd913'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124697'>#124697</a></li>
<li>Drop cmakedefine HAVE_WAYLAND_EGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=06aacf4f65fe06b074dc2444f857dbcb02ad207f'>Commit.</a> </li>
<li>Drop cmakedefine HAVE_WAYLAND. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3139dcd3b9b1fe022bb4c82ec7667b07021a5eb3'>Commit.</a> </li>
<li>Drop cmakedefine HAVE_WAYLAND_CURSOR. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a6c6408f543626b10925f9db6471b814d634a463'>Commit.</a> </li>
<li>Drop cmakedefine HAVE_XKB. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=da1e063a37941bd2bf14e019a1920c0618de2c90'>Commit.</a> </li>
<li>Make Wayland a hard build time dependency. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b8f2d4a34a57ddcb47a41685a395db7a5d0b76e'>Commit.</a> </li>
<li>[decorations] Bring back option NoPlugin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a002442224633f49249c869a45a040c595a6d504'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124708'>#124708</a></li>
<li>Drop build option KWIN_PLASMA_ACTIVE. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2635243650f88322e5bfd124859cdc622c619fc0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124694'>#124694</a></li>
<li>Drop build option KWIN_BUILD_EGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c24e315a9ba7eb6b1c975bfdb5b54f83c7023a62'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124695'>#124695</a></li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix one more XRandR::screenResources()-related leak. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8676ecddfe710fca60d849650567a2a5b71d7439'>Commit.</a> </li>
<li>Fix memory leak. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c0141c4b22cc2c336fefa54f331fd1036fdaf3e9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126096'>#126096</a></li>
<li>QDebug -> qCDebug. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=98ceb485036ec4d7f028fcbeb99e62de0a4ce8c1'>Commit.</a> </li>
<li>GetConfigOperation: store backend in member variable. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1566a136085a80256147f73085e222ed82bbf862'>Commit.</a> </li>
<li>Invalidate interface before creating a new one, just to be sure. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b9254fd9f9a78a84e5742af77747087a9dcdaa44'>Commit.</a> </li>
<li>Fix leaking QDBusPendingWatcher in BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7cd2bb2bd53b0fb9e0c5e297f40272c5533242a6'>Commit.</a> </li>
<li>Use DBus activation to start the backend launcher. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5766d087c1f4de4fb3b55f15a009f7b159904648'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353720'>#353720</a>. Fixes bug <a href='https://bugs.kde.org/353685'>#353685</a></li>
<li>Fix test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=effd39e2ebe767868660b52a8758873e45e4c656'>Commit.</a> </li>
<li>Cmake_min_req goes to the very top. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0c8df55ee8914d31a726ef3fc0589facc2f04d0e'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='http://quickgit.kde.org/?p=libksysguard.git'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Allow to build ProcessUI without QtWebKitWidgets. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=0d2cbb1390c2efcbc68b7d640d98f12cf5399cd1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125887'>#125887</a></li>
<li>Use new connect api + QStringLiteral. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=d4e4940f42d1ba285476d7a8eefb25de26ca8fff'>Commit.</a> </li>
<li>Add i18n for log files. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=5386346a502a05f0ce3bbdcdf2dc3ebfb303cb85'>Commit.</a> See bug <a href='https://bugs.kde.org/306243'>#306243</a></li>
<li>Add translations for wifi stuff. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=db6f6b3c9a4a415b391dbcfd3d25ef5bd65c7f18'>Commit.</a> See bug <a href='https://bugs.kde.org/306243'>#306243</a></li>
<li>Call the correct QString::arg() overload. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=cc7102940f86d87ff702f957909b9b23fdc81872'>Commit.</a> </li>
<li>Implement systemUptimeSeconds for OS X. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=00c25c0a537b9ad6b2051638805dc10c546f026b'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=accf75fefe71657412f0ad0c2d8766f30a810c94'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124728'>#124728</a></li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Fix coverity issue #1335195. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=949b6ca908b9fcf14ed4e929ea482623c26620b5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126048'>#126048</a></li>
<li>Added checkboxes in to list. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=227ad6094ee3ac701c4261221058f752213b92e6'>Commit.</a> </li>
<li>Re-added code to update config's minimumsize based on the animation tab. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=a04514d9a68f5f51221818172b8c2640a52da3ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354547'>#354547</a></li>
<li>Re-added cursor moving hack for kde4, otherwise mouse-over effects are broken after window drag. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=7dd58d7d864b7b28542911ddac9e949f9fa5be3f'>Commit.</a> </li>
<li>Use NETRootInfo to initiate wm move operation. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=7eef68ce9f16deeaf08be75366688f3db35dc081'>Commit.</a> </li>
<li>Using new style typedefs. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d9f528de0f6f0779794e84299743d375c9b34cd4'>Commit.</a> </li>
<li>Implemented SH_KCustomElement from KStyle, to have them working in KDE4. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=363cd18e845c6990d9cc5e147604484e72be4fe9'>Commit.</a> </li>
<li>Fix benchmark layout. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=70a326d6cde4b2858a7ec857158214ad55b9468e'>Commit.</a> </li>
<li>Only use fixed icon size for QtQuickControls. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=b3c754678cac6e87c2645543ad069364bcb54982'>Commit.</a> See bug <a href='https://bugs.kde.org/339106'>#339106</a></li>
<li>Added missing painter->restore() after rendering menu button. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=200a9e554b41c10c3545af1db4364bac838606b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346955'>#346955</a></li>
<li>Removed legacy kwin decoration. It is not required anymore even when building in KDE4 compatible mode, since only kwin@kf5 is nouw. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=34bb62a3b41e351674aa49db92137a5f5516d8b6'>Commit.</a> </li>
<li>- deal with dpiRatio when sending WM move resize events. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=cf1c7c3b74602d8a34501c847d94ad8b4d4b304b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351321'>#351321</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Arrows on parent item delegates consistent with Kicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b4ffebd524ab6464db8d754509a3811330196bf3'>Commit.</a> </li>
<li>Always show arrows in delegates with children. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f08c6923c2c2bfd4b92d9f259b97537ea370470c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126126'>#126126</a></li>
<li>Kickoff thought that its source model is KAStats::ResultModel. It was mistaken. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d6083260831ab21591dea751cb41f0390ccbf251'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355672'>#355672</a></li>
<li>[Compact Applet] Don't break the dialog size binding. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b0ebdbb4c63fd84ae95b3e35bdf40f25e1f10c06'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126207'>#126207</a></li>
<li>[Kickoff] Extract i18n also from .js files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a1f32c55fa454eb7d7553bb43e16c57a1c19e8a1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126187'>#126187</a></li>
<li>Fix drop-onto for right-aligned views. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4e84f24e1dc7204d6a5a1685f7e220bfedbde5a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354037'>#354037</a></li>
<li>Don't build kcms/input and kcms/touchpad if XInput is not present. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0bd4f5d4777d1aa5978a67ee90bd8a963a245687'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126014'>#126014</a></li>
<li>Taskmanager: Fix sometimes not showing labels in tooltip. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0ed62041f1715d4e732576bde3e98b7c6d1f3190'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346348'>#346348</a>. Code review <a href='https://git.reviewboard.kde.org/r/126162'>#126162</a></li>
<li>Kacess should not be built if xkb is not present. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5667cb503e513bc5197b2ad008d1a921d450ffb3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126013'>#126013</a></li>
<li>Widget Explorer opens quicker now. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7eb5de9feaf43d1b76c3f2d4e0b8ad9643135cf0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126163'>#126163</a></li>
<li>[Widget Explorer] Disallow double click to add when applet is scheduled for uninstallation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=81a545e5802ef89c272fe47ccf1d73a453fba610'>Commit.</a> </li>
<li>Containment is a Plasma::Containment. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=26194bc58c87964a2cb0aac9ee3c52a1050fe664'>Commit.</a> </li>
<li>Default press-to-move to on. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fe096c88da1027674a51bcbc20b2566fe8f231af'>Commit.</a> </li>
<li>Fix data roles for directory entries in RecentUsageModel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eed84960dd9a1b3d6f4559046235d7cef07b047c'>Commit.</a> </li>
<li>[Application Dashboard] Force active focus on the FullScreenWindow. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ad775885e2354b255c6f1a22630c335b86931086'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125210'>#125210</a>. See bug <a href='https://bugs.kde.org/352647'>#352647</a></li>
<li>[Widget Explorer] Allow uninstalling user-installed applets. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cc1a9b52119f4fa441dbc08d0188c03688d9768b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125991'>#125991</a></li>
<li>Do not dim taskbutton when a window is minimized. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0b50e08a38865fac7912aa4c99c4472586b6f05c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124675'>#124675</a>. Fixes bug <a href='https://bugs.kde.org/311991'>#311991</a></li>
<li>Kcm_keyboard: Use udev device notifier when xcb-xinput is not available. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=daa54f5f9ec969edca3943fd7efe662c6e25bb38'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346336'>#346336</a>. Code review <a href='https://git.reviewboard.kde.org/r/125465'>#125465</a></li>
<li>Resolve .desktop URLs in drop-on-item, too. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1b37262d13b40193eb755af9c3a9bff2d2bcec1a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354484'>#354484</a></li>
<li>[Kicker] Call new user switcher instead of unconditionally starting a new session. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=896aafc1e277b93d841a23fe4a26e4487b03e299'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126099'>#126099</a></li>
<li>Kcm_fonts: Don't touch any Xft options when anti-aliasing is set to system settings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6c78b092786e6c4315144e81982edd50294d2749'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126093'>#126093</a></li>
<li>Drag delegate pixmap support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=08da2799cfeb9ec59a58115f1a7ed55e440fa044'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355361'>#355361</a></li>
<li>Keep drag and drop in range on both sides. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=69c8e809da579a3a5318511a6fb4fbb8d7d5b523'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355365'>#355365</a>. Code review <a href='https://git.reviewboard.kde.org/r/126079'>#126079</a></li>
<li>KAStats: Syncing to the latest version from KActivities repository. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fd1a016a1aaaa2a11942beacc6ebd2977fa13597'>Commit.</a> </li>
<li>A FolderView created by drag and drop now stores the URL properly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=55a21c17f2730f4a86b03317c205fbe8cd52c6c7'>Commit.</a> </li>
<li>Offer to create a trash widget when dragging the trash onto a containment. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1eb8b7545c41b5f97c6c9cad7d5f227a56ff4f5d'>Commit.</a> </li>
<li>Restore semantic icon for use in tooltips and notifications. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=042771d92b2a1b462c85bae9a3087850b83f9b78'>Commit.</a> </li>
<li>Drop unnecessary maps. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d1191d784060e359cc4e06b44e4ce47463798c05'>Commit.</a> </li>
<li>Avoid crash by making emit synchronous. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3dbbddcc75e6e99c2967b3be460511c833021c71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354660'>#354660</a></li>
<li>Don't allow horizontal flicks in multi-grid view. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bb8f3b3d488392a432bf67f99e4bac2cb50443eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355193'>#355193</a></li>
<li>Port Kickoff to the Kicker backend. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5108149f81cdea9d085e9cea5fd3f24a699e7494'>Commit.</a> </li>
<li>Add a migration script for Kickoff's config. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=77d01cdbca0eae729fbf59cd02e4d7283075c716'>Commit.</a> </li>
<li>Reimplement Kickoff's Computer model the Kicker way. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9515d15d61e63712c3c72e5eb6b2e6ea2c010324'>Commit.</a> </li>
<li>Fix support for directories and nicer metadata extraction. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=159e17f55aef8611ee89ec967463f2336325a457'>Commit.</a> </li>
<li>Make AppsModel usable standalone and expose it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8c4bb3fd72b7508e8a84b66cb1a293121380d9ca'>Commit.</a> </li>
<li>Description logic to match old Kickoff behavior. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6c4d7972e26a39a6d70812de19c0601c035c5e38'>Commit.</a> </li>
<li>Support a merged results mode. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d93585803f9abfd1000a848a6739af5a88fab6e1'>Commit.</a> </li>
<li>Allow overriding the favorites model. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3d237473bd5a656808352cded60af661f4762cf0'>Commit.</a> </li>
<li>Merge RecentApps and RecentDocs models into RecentUsageModel and expose it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0bc2b01bfddaf96c55fc17775d188fb283dd46e6'>Commit.</a> </li>
<li>Add getter for row index from model. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=82869d14bf4a586e79e1844ab04c63cb326d6eb3'>Commit.</a> </li>
<li>Expose SystemModel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=86d038af72512b2927ccfa3f5a792e52a6ad57de'>Commit.</a> </li>
<li>Support for grouping. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cb1cf9f821936a8d3fd4beef524ad8df243cb265'>Commit.</a> </li>
<li>Support for descriptions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d88834bd9cd0e29dc810c28aa7bc589b53186d44'>Commit.</a> </li>
<li>Fix window dismiss by context menu actions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=da590c0abaf01e8eb0ffa30b37f562711465beb1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354176'>#354176</a></li>
<li>Enable the Desktop Sessions runner in Appdash search by default. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=188529e21221f9bdc787307dff1ad6a0a1c3e786'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354722'>#354722</a></li>
<li>Make the up button ignore the double-click setting based on user feedback. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=312de942bdb53036dffda1e30a2175a1b10c345c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354657'>#354657</a></li>
<li>Allow opening file manager on current path by clicking the title. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f0a6cef5ebcfadfbfce1466cf31b6d005045396f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354792'>#354792</a></li>
<li>Fix 2px alignment problem \o/. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5c3031fdb32e8f551d96b9d39ef45ef1957184bc'>Commit.</a> </li>
<li>Do not write out an unusable LC_* value for the "C" locale. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=78f02ac2ed40e88d6f069f35794d991b40a33139'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354984'>#354984</a>. Code review <a href='https://git.reviewboard.kde.org/r/125984'>#125984</a></li>
<li>Kcm_formats: Fix sorting order and formatting in combo boxes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4e033cdabfb876d75d5a8e08c74259559f9e38be'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125957'>#125957</a>. Fixes bug <a href='https://bugs.kde.org/354895'>#354895</a></li>
<li>Applet content no longer leaks outside its boundaries under certain circumstances while resizing it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=07d0819795e6dd271e68e73cf544b380c1b4338f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125896'>#125896</a></li>
<li>Warning--. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=99979d5107fd5ffd2f317b5a9c370e52b347602a'>Commit.</a> </li>
<li>Use QStringLiteral + use new connect api. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=77946ab8d96f1fde1ea43f531955b88fd26bc82f'>Commit.</a> </li>
<li>Ensure there are two columns in the widget explorer. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1eaf2867a32a0bf3765a1f96b9d53beba95d2d95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353760'>#353760</a></li>
<li>Kcm_keyboard: Set default debug level to QtWarningMsg. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1bb6fb4ebe7458f67603deb3c17b1fbbd33a9e9d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125826'>#125826</a></li>
<li>Fix build with QT5.6. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9edf2270a4a4cc6a1dc243f305de506986815e9b'>Commit.</a> </li>
<li>Switch to Noto Sans font. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d3ed6525c9f323712768eca047dd706ca272715d'>Commit.</a> </li>
<li>Sync to Kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6b8f582258307d5620922f3d43c7460042416622'>Commit.</a> </li>
<li>New activity switcher UI. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=df34451e7482442734c6d1e17c35b828013ef784'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125626'>#125626</a></li>
<li>Use PlasmaCore.IconItem in desktop toolbox. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=06084ffea50612d16c7b48666dd9356a153026e0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125660'>#125660</a></li>
<li>Context menus in the task manager applet are now aligned to the respective entry. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4a690053b1dff685f7f3c1a60ac16a925f2f54a3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125589'>#125589</a></li>
<li>[Panelcontroller] Let checked property handle the menu's appearance. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3df21c1502488f752fa95da6b12fca9145d8e4fa'>Commit.</a> </li>
<li>KAStats: syncing to master (added activity descriptions). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f41fa66fed69a088f4e6fd9bec387952540fd4c7'>Commit.</a> </li>
<li>Ungrab recursively. Fixes regression from c0e14ae54. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1a975e2d957a8f39e9cd1a25cf2f2cb780bc980b'>Commit.</a> </li>
<li>[kickerdash] Show ToolTip when mouse hovered on app item. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=148a5a6f3ce8974f1ba5b57d37c7e14116848796'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125402'>#125402</a></li>
<li>Reduce maximum preferred size for task delegates a little. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f34cf61260ab02dfda4da0cb1368d853df306246'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351425'>#351425</a>. See bug <a href='https://bugs.kde.org/353337'>#353337</a></li>
<li>Align applet icons to top. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=af1e6fde78dc1a38924e06dfab95e1b00e0e1170'>Commit.</a> </li>
<li>Applet: ad breeze widget icon for folderview widget. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cb69bb9de95ca4d82ad9f1fa388e4d4d6ed9f36e'>Commit.</a> </li>
<li>Applets: Use Breeze applet icons for the applets (e.g. widget manager). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=df80d1750f6322c2f73c740da9a2e2d2e7851908'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125437'>#125437</a></li>
<li>Switch item delegate to Complementary color group and use non-inverted roles in containment mode. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b4a189b67652af7a587eada821775668f570e8df'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125323'>#125323</a></li>
<li>Set geometry instead of resizing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a8cffa6a30a2fd27f537cab9f11c94a982a2e5f2'>Commit.</a> See bug <a href='https://bugs.kde.org/351569'>#351569</a></li>
<li>Kcm_keyboard: Make the default values for key repeat constants. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=86c9f2a234028e0c798336fb9e043e095e33ea06'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125415'>#125415</a></li>
<li>Kcm_keyboard: Remove kdemacros.h include. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8e6da687f785f0c1ff9dc56ae0acd4b0ce3b6d04'>Commit.</a> </li>
<li>Kcm_keyboard: Port away from KDELibs4Support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5bc30d0eba9e34e019373005223a993304922e83'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125408'>#125408</a></li>
<li>Kcm_keyboard: Remove LayoutWidget plugin. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=de49dce9c34b534d86d74ef39f6f409f7de8c4ef'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125407'>#125407</a></li>
<li>Moved check all mount points excluded out of widget class. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d8ccacd346a4a02cf3c7402a816c06fdddd9a0d1'>Commit.</a> </li>
<li>Revert currentIndex change to start at first tab again. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6d53ffcf8751ccd51a7e8f8dac2a453b1259b5f0'>Commit.</a> </li>
<li>Build on older Qt. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=18add5cd8267b6524ad0d921d4c191bab2080ed7'>Commit.</a> </li>
<li>Don't use parent anymore. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=58d99cdf1b34899fda4473eac0378fceec57c4ad'>Commit.</a> </li>
<li>Fix Appdash opening on the wrong screen. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4ed7145a5d7a0dbc773714b9c273bfe0d8039186'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352416'>#352416</a></li>
<li>Handle "Offer shutdown options" being or getting disabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=73386a2d10fa6a26019840d6ba9522c3009c7967'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352324'>#352324</a></li>
<li>Show screenshots in the widget explorer. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2712b370de3ca7453774c3517d3beebf1d8a67a6'>Commit.</a> </li>
<li>Merge the favorite-by-DND feature written at the Seoul hackathon. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9a07a082ef040d16e8d0f819db1c9e6cec55311c'>Commit.</a> </li>
<li>Drop debug for empty context menus, instead refuse to open. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d23876c3c094e7f3f2fb499a87e8bb7164b017de'>Commit.</a> </li>
<li>Add a FavoritesModel.maxFavorites prop to limit the model in size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8cf43a3062c58d30a8137f42808aa05213fdb05b'>Commit.</a> </li>
<li>Add a FavoritesModel.enabled prop to toggle addFavorite/removeFavorite. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=28e41b7e461a54df0c049fb63200bc94f97ff726'>Commit.</a> </li>
<li>Port desktop containment to EventGenerator. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c0e14ae546de745593db809e321c5e0c0be0750c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125288'>#125288</a></li>
<li>Drop option; see plasma-workspace.git c5539a6 for more. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8bfb3d596d2bb87e58476bd8a366f890e3cb1d9c'>Commit.</a> </li>
<li>KAStats: Using a custom matching function instead of sqlite's glob. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=801883268b21248d17cfb051292ef2758c6cac23'>Commit.</a> </li>
<li>Accept proposed drop action in panel and desktop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9f46501027037781284fa5fd7675685786b654d6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125159'>#125159</a></li>
<li>Add keynavigation wrap to kicker dashboard filterlist. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fcff88497f464b3e4ddeec166f008f1c0c6e9c1f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125012'>#125012</a></li>
<li>Add discover to favorties. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6580689bbc3995d9b6d6eab5c2b1ba65ff07f01d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125127'>#125127</a></li>
<li>Renamed some things for better description. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a80d1931d878ddf024975bbeeb869c2f731db194'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125107'>#125107</a></li>
<li>Move kaccess into separate source directory. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3631b2482a1bd279350becee081bcdd506ce8660'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125041'>#125041</a></li>
<li>Renamed some things for better description. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b4791d8fa749f31817d4a550f42fbf272a855190'>Commit.</a> </li>
<li>Add "Lock Screen" to desktop toolbox. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d83473858b8a338c764009fa5d6311d9f852e659'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124114'>#124114</a></li>
<li>[applets/pager] Remove show dashboard option. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=93b6c2e6266a34b99a6ca14cef419b23c7c7811a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125085'>#125085</a></li>
<li>KAStats: Fixed problem with adding a new resource to the model. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eb73be4d54c096d689fb3743f812f79678c817c3'>Commit.</a> </li>
<li>KAStats: Model which shows both the used and the linked resources combined. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0dc895cef9956be279ebe4f64dfe325beee0a20f'>Commit.</a> </li>
<li>Fixed paths in CMakeLists.txt for cursor theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dfd77a5cbe3856fe349afe0b55d820f02dab4718'>Commit.</a> </li>
<li>Don't leak when closing without action trigger. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f32172c17390ae639d4efbc8afc5b6f7ccad4469'>Commit.</a> </li>
<li>Open context menu asynchronously. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=19d6267711fb9c769f94166c657370b308083e9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350794'>#350794</a></li>
<li>Revert "[Appletconfiguration] Prevent binding loop in main stackview". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2eb6d5142c3160175f34d4991e6fe894410ddf65'>Commit.</a> </li>
<li>Shorten labels of QCheckBox so the accessibility kcm won't need a scrollbar. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=78644357249af839cecc421f25b557e32ea2345b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339407'>#339407</a>. Code review <a href='https://git.reviewboard.kde.org/r/124959'>#124959</a></li>
<li>[Appletconfiguration] Prevent binding loop in main stackview. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b90a8f63d25c2f277dfb7f7114900d4eaa7fecf4'>Commit.</a> </li>
<li>Add a comment to the keyboard kded module desktop file. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b5d7f757c86668fb8184c2b4bc38b90adcc59d74'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124937'>#124937</a></li>
<li>[Panelconfig] Add more / fix constraints in the position/size handles. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d058c1d44f0d9276f3a6287513023041065afe1c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124786'>#124786</a></li>
<li>[Panelconfig] Reset the panel offset on alignment change. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bba9e301cde90e7442ed1b5f5268061ddaeaad7f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124785'>#124785</a></li>
<li>[Panelconfig] Add some comments and rename the max/min value variable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c4617a65b2bfdad2b0bb3274c08f3a45d2c3c4eb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124782'>#124782</a></li>
<li>Don't enable middle click paste in the panel by default. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f596c2cb6fa79ad311149ac20d142380d739c15f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351631'>#351631</a>. Code review <a href='https://git.reviewboard.kde.org/r/124891'>#124891</a></li>
<li>Port solid-automounter and keyboard kded plugins to json metadata... <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a4f7192406a5e6357e91e398b7fca2aee1f3c986'>Commit.</a> </li>
<li>Use kcmshell5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5ef71dc37b6ae86dfca6552184b0478bf6d4cee7'>Commit.</a> </li>
<li>Add copyright header for DeviceModel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ef6152ac330b3d7afe295614dc54eb46af301266'>Commit.</a> </li>
<li>Automatic mounting of external storage is now possible again. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e8a571f09557e9cb597b466c1f61bdb4b68a6913'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124730'>#124730</a>. Fixes bug <a href='https://bugs.kde.org/351003'>#351003</a></li>
<li>Unify UI Sleep -> Suspend. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=806ce138b94478d8771f85df574bd84c175bc91c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351412'>#351412</a></li>
<li>Don't eat the left click event. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d1bca10d2154f6efa57075a510476c41428968ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351277'>#351277</a></li>
<li>Make trash settings open the trash KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=37ffbe10ea39d68db1b81e2360689ee72ea837eb'>Commit.</a> </li>
<li>Fix typo (patch by victorr2007@ya.ru). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=07e14fa3d4799e8092d4e72d90fb8a4f8f20fbee'>Commit.</a> </li>
<li>Set component display name for all actions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f22611c6304221d0244fe0114720accbfc0ec2c9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124702'>#124702</a></li>
<li>Filter out non-desktop formfactors in Kickoff's application model. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e10923384b21fe0976338d0b784beeeac84bc63f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124625'>#124625</a></li>
<li>No point in bottom margin since we have no bottom anchor. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5cf4bfaa9b36e042b1d53462b495d357238332d6'>Commit.</a> </li>
<li>Remove unused import. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d47538c452eba9e35035d0794707ed4cf33fb16f'>Commit.</a> </li>
<li>Cosmetic surgery of the activity switcher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=41573e6e2cc0f50aa23a3552032bbd766d950f47'>Commit.</a> </li>
<li>Correct label in autostart KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d772c718ae7598f859cce0565f50b32680655241'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351036'>#351036</a></li>
<li>Synchronizing the libKActivitiesStats with the source repository. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=92ccf7f61ef096c272ab9860a9ee0a119204f817'>Commit.</a> </li>
</ul>


<h3><a name='plasma-mediacenter' href='http://quickgit.kde.org/?p=plasma-mediacenter.git'>Plasma Media Center</a> </h3>
<ul id='ulplasma-mediacenter' style='display: block'>
<li>Changed MediaSources to DataSources. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=0f8e6bec7d8a805e94798a441491426505e97140'>Commit.</a> </li>
<li>Kill mockcpp dependency from plasma-mediacenter. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=65a93045e0722b3f5951b0de146dc71ab13ec05c'>Commit.</a> </li>
<li>Fix the testcase for mediaRemoved. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=9a0fe084470deb97ea957f0b20a96c33149b21a4'>Commit.</a> </li>
<li>Temp fix for PMC crash because of baloo crash. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=aab2008326b364bcc1d814ff22238bf02d486aca'>Commit.</a> </li>
<li>Updated Qt connect signal slot syntax. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=a8dbf0c3c8f4d2e78129cf121c9216a6a01f1430'>Commit.</a> </li>
<li>Added Fake mediasource for testing. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=716fe28938de6eafee7cb4c4b038b7c91eae86a6'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Make all secrets for GSM connections as not required by default. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=9da46b8a7dc83d87bf15a8b38b4c990e5ddf4fd5'>Commit.</a> </li>
<li>Allow to load "uninitialized" setting as it was before. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=031f5d71573b31da2b87537677ff6fc27603bb1e'>Commit.</a> </li>
<li>Revert: avoid using dialog->exec() in openconnect VPN plugin. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=77282d889333d558e805ef744627d567cf9816e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348318'>#348318</a></li>
<li>Do not consider virtual devices for systray icon. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=da09936c127e9f2a90a5dca431d349f3616f7cfc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126118'>#126118</a></li>
<li>VPN connection settings can now be imported by calling the connection editor with the --import-vpn <filename> argument. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=8515b1b1809c12e56e89a5fcffd63cb834c13dcd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126035'>#126035</a></li>
<li>Optimize string operations. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ab3de9ae341b506bf3e5bfc491a6246725ca3cb3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126049'>#126049</a></li>
<li>Add option to select password storage to all password fields. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=5b8db6cf2363906e25b4e033100b8da7ac3a9aa4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340707'>#340707</a>. Code review <a href='https://git.reviewboard.kde.org/r/125723'>#125723</a></li>
<li>Fix order of tab stops. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=806d276d3f5b9677644a3cf1235bf17238801492'>Commit.</a> See bug <a href='https://bugs.kde.org/340721'>#340721</a></li>
<li>Do not load secrets for 802-1x setting when LEAP authentication algorithm. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ffaa675cfd087651046753c0cc58227da99377d6'>Commit.</a> </li>
<li>Make all bluez calls asynchronous. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f3e940d49574b3d464b8d7118cdced136283579b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125785'>#125785</a>. Fixes bug <a href='https://bugs.kde.org/354230'>#354230</a></li>
<li>Make sure that the rescan button is visible all the time. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=69062f202cb01b9c28cd0950106b08b7deb9f1e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347135'>#347135</a></li>
<li>Fix minor typos. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=db7f8f12850602d3a4e60ae46f20b2e92b4b1c19'>Commit.</a> </li>
<li>OpenVPN improvements. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=1a348c01d421f6ce5479e66ba7f72a58646bc2a3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125543'>#125543</a></li>
<li>Use more descriptive names and comments for VPN plugins. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=41a1e711d0299f261b9c770e6788d76a4aaeba28'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125540'>#125540</a></li>
<li>Editor: Add tooltips for most common settings/connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=3e3e98679256555a198a22a7a9a150aeab1946a6'>Commit.</a> </li>
<li>Move the view to the top while a connection is being activated. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f7757a1c5e8269a17e41be86bcb2cbfb8d4cfaa7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/342801'>#342801</a></li>
<li>Allow only certain file types for certificates. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=3ba7485940fd8a82132efb3982e9071bfe6d9028'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340706'>#340706</a></li>
<li>Applet: add breeze widget icon for nm applet. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=bab4a143e9e5b258814e342593a77a87181cc00a'>Commit.</a> </li>
<li>Set proper maximum MTU size for infiniband, wireless and wired connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=afb99db0bab15526391ba3c42bf93e11fe4e3fcb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353241'>#353241</a></li>
<li>Add some advanced validation for WPA/WPA2 Enterprise. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=edc29fb7ca7c9610156bfe571c4bcec2cef44be4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125356'>#125356</a>. Fixes bug <a href='https://bugs.kde.org/351191'>#351191</a></li>
<li>Add WPA/WPA2 Enterprise (partial) validation + options to add subjectMatch and altSubjectMatch properties. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=92faca4156698df2aee896dbd3a1a6734be58056'>Commit.</a> </li>
<li>Coding style. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=20b7b3d009a771fac91743a206472f9fc2f6b023'>Commit.</a> </li>
<li>The icon for showing/hiding the password should represent the oposite state. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=23e83061eb3ca72813bd6470ad22b36e37ec52b4'>Commit.</a> </li>
<li>Add notifications with new state change reasons when device gets disconnected. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=80f7fd75bb6da2a83781ac0f5154b8df19ef924f'>Commit.</a> </li>
<li>Use new breeze icons. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=34a890654d5d221652c9315503f2bf39a22b5168'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346133'>#346133</a></li>
<li>The icon name for password field was changed to 'visibility'. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=7fa933e92432a4f15bba6381d14933cded9eed38'>Commit.</a> </li>
<li>Replace 'show password' checkboxes with a custom password field. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=5dad876a2a329271287952c67d2eaba6ba01ab67'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124922'>#124922</a></li>
<li>Port kded plugin to json metadata... <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=dc86e490c52a87b715f44d57e940815d25f5c2db'>Commit.</a> </li>
<li>Fix random MAC generation on Wired/WiFi. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d66c6bd1ca0954ac7c4268f4f0538078ea6b6a29'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124747'>#124747</a></li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Fix crash if context gets unrefd. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=5d46f6c2a767c25868c143ee64ef71e071a7a55b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354510'>#354510</a>. Code review <a href='https://git.reviewboard.kde.org/r/126012'>#126012</a></li>
<li>Install kconf_update script in kdelibs4 directory. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=2351fbb7c61b807550d627915ac0bbfc1ec0dfa0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125706'>#125706</a></li>
<li>Change applet icon to the new applet icons from breeze. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9b67bb00ea28003f2c75040c110dda52cba16d2e'>Commit.</a> </li>
<li>Speedup finding the property change signals. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=6c97ddabc3917717e09c74365b19d76cd2f43386'>Commit.</a> </li>
<li>Make SourceModel writeable. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9e686fefef660b36d5e94343840fb37c2c5b8b57'>Commit.</a> </li>
<li>[kcm] Remove the anchors in the stream and device list. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=4fd9f020a928b440ef10808d5b467e108747cecc'>Commit.</a> </li>
<li>Add a .reviewboardrc file. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=b367285fa89dc25d2885ad4c67a36c4c007ffceb'>Commit.</a> </li>
<li>[kcm] Elide the label of the streams, parts and devices in the lists. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=042c9feac95aad41cadc74e0a29ea224192b3876'>Commit.</a> </li>
<li>Remove unused dependency. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=80d3b3e7052636982dbbc0d74324c283e0684b30'>Commit.</a> </li>
<li>Revert "delay loading of popup". <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=b45a2601fce21f9bda5f7a451e68953faa96b417'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351345'>#351345</a></li>
<li>Fix typos (patch by victorr2007@ya.ru). <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=6a7a3ce23b6fdbc762f59a2154e3d868140867b8'>Commit.</a> </li>
<li>Fix XML. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=ad5e559df56fb47dfebc4cda0f8d167af171d82e'>Commit.</a> </li>
<li>Fix minor typos. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=f66ec63cbdc6a4ae80adb9dd9f713dd45a4fc380'>Commit.</a> </li>
<li>Disable build of docs until new entities are released. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=136a20743a24df5e96e4ad140656757a8557e587'>Commit.</a> </li>
<li>Documentation for plasma-pa. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=472c7e5b7f22599d8a66964a0bc93164b197a59c'>Commit.</a> </li>
<li>Fix interactivity in KCM. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=94380f818357e1d5d2041a9e198553f016f5a1f9'>Commit.</a> </li>
<li>High dpi and sizing fixes. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=f3ce9a1352c18a4c379cbff7502ccdfde651ca32'>Commit.</a> </li>
<li>Fix i18n in kcm module. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=15a9f6dadced4527f5dc9abec75cff5ff9abac29'>Commit.</a> </li>
<li>Fix i18n in applet. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=28ca29f8fb7df023e6dfb9f54f5edf936448a345'>Commit.</a> </li>
<li>Delay creation of the popup dialog. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=d1da7e65a2c813aa132cafd033613cc39a0f872f'>Commit.</a> </li>
</ul>


<h3><a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Plasmoidviewer: Fix filename pattern. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=f47662cdd65dedf656881ac47b4c261762653f32'>Commit.</a> </li>
<li>Plasmoidviewer: Add @title:window context. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=7f6912b8b3a6e6d2cffdbedbd9bba76a2c962940'>Commit.</a> </li>
<li>Plasmoidviewer: Fix typography. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=a8847f3084c6163c3d6b8325178531f2ac8368ac'>Commit.</a> </li>
<li>Plasmoidviewer: Update location options in documentation. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=57d6e5a9269ba2fba90e6dcf5eb48fcf0afd79f2'>Commit.</a> </li>
<li>Add trailing semicolon in Categories. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=38373cf77e11e1aefc7b7caba7b2f4122edb7b11'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[startkde] Fix font prefixDir. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0ef5a3c367194d8df220ce12abaf714fbcd01913'>Commit.</a> </li>
<li>[notifications] Rework the notifications positioning a bit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4a6f7db0018a2a7366ac7f2ad61fc33f31566c03'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126221'>#126221</a>. Fixes bug <a href='https://bugs.kde.org/355069'>#355069</a></li>
<li>Forward mouse enter and leave events on feathered edges. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=76cb47f1279ea012a9eda11c4ead7f28db4c1399'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126175'>#126175</a>. Fixes bug <a href='https://bugs.kde.org/354651'>#354651</a></li>
<li>Fix quit if we fail to claim. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=968b14962a454f19fc99d7728e14fc9e6e448525'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126080'>#126080</a></li>
<li>[Clipboard Plasmoid] Use flat ToolButtons. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7dba0fa31a094a5ab40f4451e61343c79cbfa587'>Commit.</a> </li>
<li>[Clipboard plasmoid] Make file preview ListView non-interactive. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cd15843e9794b926bdca41b253d023943a3b114e'>Commit.</a> See bug <a href='https://bugs.kde.org/354592'>#354592</a></li>
<li>Ensure the spinboxes in image slideshow config are wide enough. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4d0d55e4a2910ab8b50d98c2f2fd377b6f2af83c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355553'>#355553</a>. Code review <a href='https://git.reviewboard.kde.org/r/126150'>#126150</a></li>
<li>[Look and Feel] Add rest of the missing user switcher files. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=614adba7cff0a8a01ccdb87f9314ff39b2a35243'>Commit.</a> See bug <a href='https://bugs.kde.org/355708'>#355708</a></li>
<li>[Look and Feel] Add UserSwitcher.qml. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e2a6103ca2ee94fb5534a7e70ed2d6d7deef5a71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355708'>#355708</a></li>
<li>[lookandfeel] The splash should have been PreserveAspectCrop. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=29b515789c9dc02b730922fcec733ae2aaee9ef3'>Commit.</a> </li>
<li>Set dialogs location according to containments. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e83a07415975950ca0f657f05e5f9c3a04137c5f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349887'>#349887</a></li>
<li>[Widget Explorer] Remove uninstalled applets from containments. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f7279f4463a8dc94f7acbaa58006424290026f4b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126120'>#126120</a></li>
<li>Use --exit-with-session in kwin_wayland start command. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f1f2a44e930f65796f1b019d9f6a1829b28f77c4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126108'>#126108</a></li>
<li>Use absolute path to kwin_wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fdd56d1a71d7f3e990de7d458cc8e6930f62c633'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126107'>#126107</a></li>
<li>[Clipboard plasmoid] Fade out text where the buttons are. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8f8aa215e8314916816ac465b1fc503ed4ac41c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354602'>#354602</a>. Code review <a href='https://git.reviewboard.kde.org/r/125898'>#125898</a></li>
<li>[KRunner] Forward KRunner switchUser to new KSMServer user switcher. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2dcd1e0bd89a63feca1dcac1f0ee30c5844485ff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126100'>#126100</a></li>
<li>Revert all of the Calendar-agenda changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=697aaf8067ebac4c649895e57eff73a5f071b430'>Commit.</a> </li>
<li>[lookandfeel] Set the splash bg image to PreserveAspectFit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a8e6d751fdcc6b7a700d20064b022300142c1075'>Commit.</a> </li>
<li>[lookandfeel] Update the blurry background to 5.5 wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=43a66a3dc0f9d5b878f2d34b6904418144e362b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355502'>#355502</a></li>
<li>Launch dbus-session in wayland session. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9318cfa4bb48de3db2a111f82882c6bd6c4ebbb0'>Commit.</a> </li>
<li>Install session file for Plasma on Wayland session. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=49194b62294350751aaa1361d6c868c47a371cec'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126098'>#126098</a></li>
<li>Add pretty user switcher. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fe97f4272432de0ad563fe32aeaaaaff6a8dee84'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124585'>#124585</a></li>
<li>[digital-clock] Add missing copyright. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9bf9ee664e92237b896bd7dba5b8d35682c51ae6'>Commit.</a> </li>
<li>Update digital-clock and notification applets info. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=662c4f44f334e6a43fd54eff6d0ecccc8503bbfd'>Commit.</a> </li>
<li>[digital-clock] Pass the enabled plugins to the manager explicitly. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a179d512b4fcba909a7c03da995dd963c220491b'>Commit.</a> </li>
<li>[digital-clock] Add the calendar plugins configs to the applet config. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=69e13f67379e3f45e216a8f2423e56693674bf43'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126010'>#126010</a></li>
<li>[digital-clock] Split the Calendar config stuff to it's own category. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4b631f8c5c99957313ce2a1953b7975836fcef46'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126010'>#126010</a></li>
<li>[digital-clock] Adapt the Agenda part to latest applet updates. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7232b798d557fb8d785827143aa576b24e790acc'>Commit.</a> </li>
<li>[digital-clock] Bring back the agenda part of calendar. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ea6691a307f22490030a4fbfcc8d85535cc32284'>Commit.</a> </li>
<li>[Widget Explorer) Fix applet uninstall. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fe5ee9c72360c648c85b3ade3821c629b7978ef7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125990'>#125990</a></li>
<li>Widget Explorer can now filter for widgets the user installed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b3f42870443667735f1a18bb869f545356b1ae4d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126074'>#126074</a></li>
<li>Remove accidentally pushed files. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cbd2d53cba47a904786dc389cd23b94456718709'>Commit.</a> </li>
<li>Look in the correct path for shell update scripts. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=132ffbab0898cca47997af05b653f3eb195317b3'>Commit.</a> </li>
<li>[ksmserver] Add a dedicated --no-lockscreen command line option. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1d7af8c97164d3416bfa0d103c9f3c883cea3083'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126022'>#126022</a></li>
<li>Fixed dropping files onto an icon widget linking to an application with restrictions on supported file types. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=02bbfcf8ac4c357c0926bcd635185250d53b3376'>Commit.</a> </li>
<li>Dropping elements onto an icon widget pointing to Trash now works. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ab2775a45a8a0c316dc57d9c1ec2ea59c495de49'>Commit.</a> </li>
<li>Fix font path to not have /usr//usr/. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=073b669fdce7279e6658a5c4c3f80f31197cb921'>Commit.</a> </li>
<li>Fix krunner crash when open "/". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0ab0823d8cd192f67a431174fa02b8975adc8987'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126060'>#126060</a>. Fixes bug <a href='https://bugs.kde.org/355290'>#355290</a></li>
<li>Restore semantic icon for use in tooltips and notifications. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ae16b20c02fdb5280c5463bbafacf423d7a5de41'>Commit.</a> </li>
<li>Load applet icons from the plugin name first. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1b77bd6c4798095b7498f9473b275d4b1a8ee7ad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126046'>#126046</a></li>
<li>Avoid QMenu::exec in plasmoid context menu. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4ea74276f1a58697c6441ca2cc3b14c09c7e7fdb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354903'>#354903</a>. Code review <a href='https://git.reviewboard.kde.org/r/126042'>#126042</a></li>
<li>Don't crash with invalid plugins. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e081403b3e5a68d3cfe35c229f80d2d675f6c6de'>Commit.</a> </li>
<li>Fix ksmserver path. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6df81702473b616d0102c250cc38f3cb03e88056'>Commit.</a> </li>
<li>Fix ksmserver path in startkde. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5717020e1d2f337ac95e1769a631080f843f22b1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126007'>#126007</a></li>
<li>Catch other openGL error gracefully. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e20c4c6b7a2910bef752a19a414488e257a49419'>Commit.</a> See bug <a href='https://bugs.kde.org/354966'>#354966</a>. See bug <a href='https://bugs.kde.org/355054'>#355054</a>. Code review <a href='https://git.reviewboard.kde.org/r/125997'>#125997</a></li>
<li>KRunner no longer stops showing results under certain circumstances. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a336fbc013a668c011d4fa71343c885dc2c8e8af'>Commit.</a> </li>
<li>[ksmserver] adapt to changes in kscreenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1217de97d398dc6e351645e4055e3e520761e9d0'>Commit.</a> </li>
<li>Clean up dependencies after removal of the screenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ad906e3c4ec6d5ad2ca4d1fdf88dc66a1cf169a7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125964'>#125964</a></li>
<li>Remove kscreenlocker code from the plasma-workspace. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4414b99dcab460ffebdc40e6d1acfcc212908fab'>Commit.</a> </li>
<li>Merge xembed SNI proxy updates. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c4d3d42ec4a9ec6fe08f2077072056dfb6b9994c'>Commit.</a> </li>
<li>Fix build. ksld was renamed to KScreenLocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=092869da5742dbd49ff06a86c2e38932492eb31d'>Commit.</a> </li>
<li>Fix crash on multiscreen setups by not relying on sender(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3fcccfb3d7ebe7d982c3fdf53de462a9c71f04d'>Commit.</a> </li>
<li>[screenlocker] Install the cmake configuration and header files. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f3aa06cc99750bc576dfecd28775c5c25d95ef16'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125930'>#125930</a></li>
<li>Proxy Xembed icons to SNI. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2fc947526df5d7f62db79e7f224cc13b1aa15493'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125655'>#125655</a></li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2b511741798eb28eeda0ea1c22a0c97efa1dd3e8'>Commit.</a> </li>
<li>[screenlocker] Make it possible to build kscreenlocker without ksmserver. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c336055efd3f6648a6e4b9a6c064103f68ad3fa6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125892'>#125892</a></li>
<li>[screenlocker] Remove kDebug default debug area. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fa9f24c52f5c3b2a97eeaf180f99fde1f24637c'>Commit.</a> </li>
<li>[screenlocker] Remove unused ksmserver dbus interface from screenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=651449c80856cbf8fcc9dc1505b8cf9bc397279e'>Commit.</a> </li>
<li>Drkonqi: Drop KF5::WebKit dependency. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6aedc9534f76f9fb0d5d9560f81a704bda1e8df7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125886'>#125886</a></li>
<li>[kscreenlocker_greet] Fix QML fallback. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5ce2e2b707db361a5e2377341167221dab3be190'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125848'>#125848</a></li>
<li>Set maximum width of notification popup to minimum width. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0826db43c1d80b41b3e1c09680c5bca56fa297f4'>Commit.</a> </li>
<li>Use new connect api + QStringLiteral. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a007f294b4991c6f5730e0ec8d9d20f36e31acab'>Commit.</a> </li>
<li>Systemmonitor: Don't link to unused libraries. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=87029dd53d20edb066fbdbf9e7f8e435d87f17a4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125888'>#125888</a></li>
<li>[kscreenlocker_greet] Port fallback theme to SessionsModel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0ff4006610f960b21cd81d6361ffc27ac0c5708d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125849'>#125849</a></li>
<li>Add some translation context. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=64c8e7d975362e3c0a45f9fbc67f64c9e45643fc'>Commit.</a> </li>
<li>[lookandfeel/lockscreen] Remove non-existent kscreenlocker import. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bcd62c7257bcb29dc4ef6a68527a65c116055b60'>Commit.</a> </li>
<li>Simplify code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9d8a216f2aa408ff3423aeac5e8975de63924ac5'>Commit.</a> </li>
<li>Don't duplicate code for setting the screen geometry. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=19e4d883f301a541dcfe97971fc6795734f2849c'>Commit.</a> </li>
<li>Remove redundant code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6b0c7c63302e0ba5291d9a2880cb059556914f95'>Commit.</a> </li>
<li>[kscreenlocker] Remove KWorkspace dependency. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ab258330725cc03af3a90c721e3dc684864d8cf1'>Commit.</a> </li>
<li>[Lock Screen] Use org.kde.plasma.private.sessions for User Switcher. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=987f4f76030d1dce5729285df3eb595d50951b04'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125774'>#125774</a></li>
<li>Add org.kde.plasma.private.sessions with a SessionModel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=667ed80cbe9866c87f238cd1a54516820fb8da83'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124980'>#124980</a></li>
<li>[screenlocker] Set soversion for libksld. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f33bd8ab57cb496ba193748920e71a1375071a40'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=00e33656c2676a5447b940e327dc3bc8472c11ab'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1b312493995b4ae488786730308c752471ceea0b'>Commit.</a> </li>
<li>Update drkonqui hints for relevant information. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9cce76bc4b12c18ef7796e8a62311275bd2ba419'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125870'>#125870</a></li>
<li>Emit correct signal for notifications expiring. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d1a9729d1f3b857642ff415153cdca73cb341596'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354293'>#354293</a>. Code review <a href='https://git.reviewboard.kde.org/r/125770'>#125770</a></li>
<li>[ksmserver] Remove compile switch to disable building screenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=45b516a442e87d336bbbe12486867e067738ddc6'>Commit.</a> </li>
<li>[ksmserver] Remove workaround to cleanup lockscreen before shutdown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cc8c4e7b137ee38d208f572372ab0040182da267'>Commit.</a> </li>
<li>KRunner: Cleanup CMakeLists.txt. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=eb5a68b20d603cc40d328adf19689e750b1ef81c'>Commit.</a> </li>
<li>KRunner: Set the correct version number. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d11efced2fc01e60c314733e279136646761203f'>Commit.</a> </li>
<li>KRunner: Use KDBusService after parsing the command line params. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=981737000b29d8ab42e73d8643dd64ade7ea06ec'>Commit.</a> </li>
<li>[screenlocker] turn screenlocker into shared library from static lib. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=30f6bce621daa4d558e70e5c27d71748e1e6f32e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125838'>#125838</a></li>
<li>Use PATH_VARS feature of ecm_configure_package_config_file. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=050c66bfb25b088711f237562abca8f6adbd39fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125844'>#125844</a></li>
<li>[libkworkspace] remove the setLock function. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5b18a659a8a8fde3a1c4bab7e03927679506bc53'>Commit.</a> </li>
<li>[screenlocker] Get rid of libkworkspace dependency from the ksld. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=dcdb990e3841d406113ea73997cad67868ac0402'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125837'>#125837</a></li>
<li>Revert "workaround patch for fixing ksplashqml lookandfeel wrong position.". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=45a6db4a198101a7ad219d06c2f24643a462cbf8'>Commit.</a> </li>
<li>Workaround patch for fixing ksplashqml lookandfeel wrong position. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5674a757c942f329901d6e3865dc377aadf953e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/354480'>#354480</a></li>
<li>Use semantically correct icons to rate backtraces. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a0f52e6921d43485e300615ea0bc6c02d4e16ccd'>Commit.</a> </li>
<li>[screenlocker] Introduce WaylandLocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e97bb623469b1c65c66c61561680217d47c7da12'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125827'>#125827</a></li>
<li>Skip applets not in formfactor. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=dc34aa70b45f5b59ffebb62aa6916bbbb0864708'>Commit.</a> </li>
<li>Add some formfactor info to plasma-workspace applets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6e3c17e5c846eccc4fa6c744c5ab6bd4885df196'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125810'>#125810</a></li>
<li>[screenlocker] More XFlush and XSync before server grab. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=30ccef90de694b37443c2b3341d016387d06ff08'>Commit.</a> </li>
<li>[screenlocker/autotest] Helper grab input applications disconnect xcb. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3645dbfcae444e79163a1a8fdfee8e7fce8a19cd'>Commit.</a> </li>
<li>[screenlocker/autotests] Enforce Raster Widgets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6bc8b1bedc9cf744138ba97f980a25d6a999efb7'>Commit.</a> </li>
<li>[screenlocker] Add unittest for emergency window. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e1be6dd5203ad96c00880de0d365889a47bfd2c2'>Commit.</a> </li>
<li>[screenlocker] Add back changes from 9bf61a and d9328e. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=75a7d363bbc84fec6390d0beb75fe9b27f608fed'>Commit.</a> </li>
<li>[screenlocker] Rename the lockwindow.* files to x11locker.*. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=34589cfd79819f8ede9a041ed80f4918980ccede'>Commit.</a> </li>
<li>System Tray: Add ScrollArea to hidden items view. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a2a9d41b334db36238232be8e093deeecab03514'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341165'>#341165</a>. Code review <a href='https://git.reviewboard.kde.org/r/125775'>#125775</a></li>
<li>Fix icon-based status notifier icons. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=867132ffc6799c19bc27ff48a14106f43ac4539d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125812'>#125812</a></li>
<li>Don't try to set the overlay if it's empty. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ff6f26ab7f823ed5d218080107413f0b096a33b1'>Commit.</a> </li>
<li>[screenlocker] Add missing files from commit 5a0ab38613763a. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8da1255654e844a93e387f3f80bf341bbe522870'>Commit.</a> </li>
<li>[screenlocker] Split generic parts of X11Locker into AbstractLocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5a0ab38613763ab50a0e1315615163a48fdb753b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125802'>#125802</a></li>
<li>[screenlocker] Tidy up the code a bit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d9328ef3c473f3301e7ae42f913aa1f490fdd77c'>Commit.</a> </li>
<li>[lockscreen] Improve autotests for lockwindow. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9bf61a335b06b94aae642518bbe1d7f671880061'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125805'>#125805</a></li>
<li>Update the KSplash background to that 5.4 wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8c25336b638d8a941c1097dd18de6c9c52c788f3'>Commit.</a> </li>
<li>Remove broken ifdef'd code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f83b9cdf9d34de3cbbc066a895b3f464072c74c0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125761'>#125761</a></li>
<li>[ksplash] refrect windowState correctly. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c5970ae34c81d5a4d8e868afc145352c8a3647ba'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125727'>#125727</a></li>
<li>Component Baloo|Files no longer exists. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=70de75efffdf5b8df5b72ce24fea7495aa8dec54'>Commit.</a> </li>
<li>Don't connect to signals which don't exist. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2441d350ef571329b67848f79668f3956534806e'>Commit.</a> </li>
<li>Resize lock screen windows if screen size changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b4310f627ac3de060433c9b8dd0571ea8a2adda8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125605'>#125605</a></li>
<li>Fix calculation of available screen region. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=95d56f617e027a88d25fe37a2bc27ca1383998ac'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125696'>#125696</a></li>
<li>Use the correct old screen to shift panels to new screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=20778e83026d5390230bb3b2726f9c722878ba78'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125694'>#125694</a></li>
<li>Prevent changing the primary screen if it doesn't have a view yet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=61828df89f3e7319bdc7024409804b51c0ba5b68'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125693'>#125693</a></li>
<li>Display which process blocks unmount/eject in a plasmoid notification. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d64719de4bf824b40cfe43965f09f8ac74605072'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125248'>#125248</a>. Fixes bug <a href='https://bugs.kde.org/96107'>#96107</a></li>
<li>Rename show dashboard to show desktop. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8b9af710630d3624cf2810c36fd9b63be6565d61'>Commit.</a> See bug <a href='https://bugs.kde.org/353928'>#353928</a>. Code review <a href='https://git.reviewboard.kde.org/r/125670'>#125670</a></li>
<li>Remove redundant code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bca23eaa323d68b13a954138af0ea1a6aaac71eb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125669'>#125669</a></li>
<li>Don't include struts in initial ksplash geometry. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d6ea77ca6a7574d2159235714e105ea7ba2e1a7a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125654'>#125654</a></li>
<li>Use shared engine in ksplashqml. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=44fb5c45abc05a8bbc19d6fe65b6c5037e5d8a5b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125654'>#125654</a></li>
<li>Ignore shells without a "loader.qml" file. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=43493e7bc9837adbf4e6e50acc3ea92435296aea'>Commit.</a> </li>
<li>Disconnect lambdas when "this" is destroyed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=030033bd9ca74e7770408b90aae06f577db28737'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353870'>#353870</a>. Code review <a href='https://git.reviewboard.kde.org/r/125639'>#125639</a></li>
<li>Don't assume dbus-launch autolaunch is still broken. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f949f161a77114203d0489b2020011be019b47d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352251'>#352251</a>. Code review <a href='https://git.reviewboard.kde.org/r/125637'>#125637</a></li>
<li>Rotate wallpaper based on image EXIF. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c082cee7715cb5141cc41ccb62e22e5530c66f23'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352022'>#352022</a>. Code review <a href='https://git.reviewboard.kde.org/r/125014'>#125014</a></li>
<li>Scripted default config for applets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d89be8e25be711919233cc1e873436df32a8276c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125562'>#125562</a></li>
<li>Use KWin to lower/raiser panel in windows can cover mode with edge activation. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=80bdf99d7b9cc90afc2b32f086ca151cc7829fe6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343448'>#343448</a></li>
<li>Breeze Icons: remove screenshot to use systemtray widget icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=88af97288c08cc71de99f986e62c5dfe36c73d07'>Commit.</a> </li>
<li>Dropping files onto a folder icon now supports copying/moving/linking the file there. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7e3942abe3569fa08c0263c6164649029a9a068c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125118'>#125118</a></li>
<li>When connecting a device always open the plasmoid. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=06e9766fea883890d23ea7a35241c3ff4301dfb8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349616'>#349616</a></li>
<li>Include env vars from sourced files on pre-startup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f869daca8244131f6b452e2c15b4dee5903ff768'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125323'>#125323</a></li>
<li>Revert "Delay desktop view geometry update". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5e1bce0ab5ad9b3a4f335ff7a00fdf12fd79c8be'>Commit.</a> </li>
<li>Skip the taskbar on wayland for panels and desktop. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df78c5e49c72370054c967ce92605e22b52e8b19'>Commit.</a> </li>
<li>Update bundled exception rule to Gimp 2.8. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=25ebfe24f988b31b7031ba296d924e6404b98b96'>Commit.</a> </li>
<li>Fix utility windows not being removed from task tree after demands-attention state is cleared. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c34550cf4a7e93a5e14e882b01f7ab0cb24936ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352477'>#352477</a>. See bug <a href='https://bugs.kde.org/178509'>#178509</a></li>
<li>Allow explicitly set on-screen input method such as maliit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5dc6cf4de33fb61cf725c06a92b99a8184c0f5f0'>Commit.</a> </li>
<li>Applets: link to new breeze icons for applets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fd04b714d4e68a7e1e16ee209eca042347933c69'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125438'>#125438</a></li>
<li>Simplify logic. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=50d2dfd597c5ebdf90dfa956190c42851fdf0f5f'>Commit.</a> </li>
<li>Delay desktop view geometry update. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=35003951e4b6d18b3761ee266255526f0370f2f8'>Commit.</a> </li>
<li>[libtaskmanager] Protect against X11 usage on Wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3927e62e19a86dddc7e1c1d2a469a3d5ebbabe9c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125445'>#125445</a></li>
<li>Fix broken testChromeBookmarks. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a3d252574d9414c378e400d343595a48675ff596'>Commit.</a> </li>
<li>[screenlocker] Grab XServer while establishing the grab. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cfef8bf23d361624bab39dde1cf4a3ccc56fcd81'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122528'>#122528</a></li>
<li>Use colorScope for colors. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4135ddab24bfe99df3b882eff1a92debca950eb8'>Commit.</a> </li>
<li>Bring backpossibility for plasmoids to contain their own icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0528f909ae0fa6697b69c8ed9a4a4b83f7f6a1c3'>Commit.</a> </li>
<li>Add a role for screenshots in the widgetexplorer model. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9518d59d99ee28605e48aa176b0c0640c946c209'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125134'>#125134</a></li>
<li>Bump frameworks requirement. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0b8c4afe3522fafafe2c4c58094d74a3ec8e0dd9'>Commit.</a> </li>
<li>Remove outdated Quicklaunch applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cd0b31d9907ae3387faa2551c74c12264352a400'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125282'>#125282</a></li>
<li>Further enhance the icon lookup behavior. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c5539a6e59602591d6131ad18de2d744a905a677'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351624'>#351624</a></li>
<li>Support window move with the shell interface. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7763a9c956ef7ca9146aaf7f99bb5b669770e76b'>Commit.</a> </li>
<li>Fix corruption of startupconfig(files) in Qt5.6. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e92628aa42251cad211733d88affbf6a56277379'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351609'>#351609</a>. Code review <a href='https://git.reviewboard.kde.org/r/124877'>#124877</a></li>
<li>Soliddevice: Use KDiskFreeSpaceInfo instead of solid to get disk size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9b474a4ab01c3268048e29c336cf7b3fe9a83a47'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123818'>#123818</a></li>
<li>Monitor for the clock changes from the kernel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bbfffc2d65ab53acd41ee056fdd2a6ea9c2f6a08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344870'>#344870</a>. Code review <a href='https://git.reviewboard.kde.org/r/125028'>#125028</a></li>
<li>Widget screenshot for widget explorer. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=33ec9fd62c13c29aac752d4037397c0e34c1f330'>Commit.</a> </li>
<li>Don't show genericName if it's the same as applicationName. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8a24c958819729574279d706ee40f98755cbb34e'>Commit.</a> </li>
<li>[startkde] Generate absolute path to ksmserver. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=13e8e8b85de5216bc87c1a274ae4e0dc69e7767e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125113'>#125113</a></li>
<li>Right-align KeySequenceItem. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=acb812deb0a8dd19dbbdc5fb614a6ad4957469d7'>Commit.</a> </li>
<li>[System Tray] Bring back shortcuts configuration. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a1bf6d82d7b0342db796d6535eb1c6bf89875fe0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/Shortcuts for applets in system tray can now be configured from system tray configuration'>#Shortcuts for applets in system tray can now be configured from system tray configuration</a>. Code review <a href='https://git.reviewboard.kde.org/r/124986'>#124986</a></li>
<li>Bump Frameworks requirement to 5.14. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4a68d6286634490cd0eee72a275c21df85ade6fd'>Commit.</a> </li>
<li>Dropping file(s) onto an icon widget will now open them in the given application. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5f8051324985d9fb14bccfa0a4ecda9830aeb143'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124472'>#124472</a></li>
<li>Don't crash on wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0524b4d45ad8d001fce7ec067e180a92fa2da45e'>Commit.</a> </li>
<li>Use the correct enabled borders. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c289c5db188760f9fa7afaac2b911c78ba737885'>Commit.</a> </li>
<li>Use a PanelShadow subclass to do the dialog shadow instead. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=61be9dcae2e7c1e42c2df4b4bcd9d5467a9e095c'>Commit.</a> </li>
<li>[lock logout] Respect whether the user can lock the screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cc8ecb137d168cbd4fe80718c87d856162d7d50f'>Commit.</a> </li>
<li>Simplify. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bc82b81065d5a8a98d9fb18b4248f7237bd75da6'>Commit.</a> </li>
<li>Expose whether we can lock the screen in the powermanagement data engine. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=76d4a9b4b91a2efbf036447660a6d9dc0d22da04'>Commit.</a> </li>
<li>CMakeLists.txt: Install sddm theme to KDE_INSTALL_FULL_DATADIR. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7ba52e1fd6ead676cd31cc5bb8b274d760cdadf6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123190'>#123190</a></li>
<li>Make gridUnit available in the shell scripting. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4cdfdf52fb59e1ac029e8f7a23e52e09de766e3e'>Commit.</a> </li>
<li>RTL support in login manager and lock screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f5c61c43323075daa684084d95c4abf23fd62da9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351534'>#351534</a>. Code review <a href='https://git.reviewboard.kde.org/r/124867'>#124867</a></li>
<li>[screenlocker] Filter out XCB_FOCUS_OUT events in the screenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d143252ae9a719ca1f33fe97099bdbdb42c77830'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124966'>#124966</a></li>
<li>[screenlocker] Delay the async loading till first frame is rendered. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0da420f6309cdfa8dc95566a1afbf0f29a6b7ebe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124948'>#124948</a></li>
<li>[screenlocker] Render greeter backgrounds as black. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6ee0d63af943526e2453631c6c709861061f08ac'>Commit.</a> </li>
<li>[screenlocker] Share QQmlEngine between all views in the greeter. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7e1a4df817bdd156352ac9b37eac01837ed25204'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124947'>#124947</a></li>
<li>[screenlocker] Try to load faster by using more loaders. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e2df9af6e2df4ba1c07dc40d43ecd591584db498'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124933'>#124933</a></li>
<li>[lookandfeel] Fix errors on startup of lockscreen greeter. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=71c495d5b530e19d4e996b7309cc7ee392cb34be'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124931'>#124931</a></li>
<li>[screenlocker] Rename LockWindow to X11Locker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a47d95473a452e35ac891cd64c92f857e3023a5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124929'>#124929</a></li>
<li>[screenlocker] Drop setting event mask on the background window. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=79379d668b0dc97463c21df8deead29facdd95c8'>Commit.</a> </li>
<li>[screenlocker] Drop call to setCursor on the background window. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ddaf1e13a678d79cc1100a299f66bc7e928acd2b'>Commit.</a> </li>
<li>[screenlocker] Start grace time when the greeter is shown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=46750b8c958916f2d865bac29f329959b3d6c7e8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124917'>#124917</a></li>
<li>Use qplatformevents only on Qt 5.5 onwards. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d6916d9b09a832c33eb7125d6cb887d27e685736'>Commit.</a> </li>
<li>Use kwayland to move Plasma::Dialog. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=129af59910a58ab4d8e5d79bbf67525c5c68adea'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124555'>#124555</a></li>
<li>[screenlocker] Add emergency mode for greeter crashing. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5e2f4016dced0af61f5d2817d8eac0cfef3ad52b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351696'>#351696</a>. Code review <a href='https://git.reviewboard.kde.org/r/124915'>#124915</a></li>
<li>[screenlocker] Emit locked once the lock window is shown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1be3ba05370b4f9e9f7515ca8035883ac2da341b'>Commit.</a> See bug <a href='https://bugs.kde.org/348850'>#348850</a>. Code review <a href='https://git.reviewboard.kde.org/r/124912'>#124912</a></li>
<li>[screenlocker] Split black window rendering out of LockWindow. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c3c53d2767c08bb80785aa91bfec6b9729277307'>Commit.</a> </li>
<li>Port desktopnotifier kded plugin to json metadata... <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=81c80964d63f647188a7d555174f551f7cdd1333'>Commit.</a> </li>
<li>Port KDED plugins to json metadata. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=498cd08a1956d557478b119aeed17fbfcfbd9b56'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124845'>#124845</a></li>
<li>Unify UI sleep -> Suspend. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cae44016f4d3b895141c56406c43606f719958c8'>Commit.</a> See bug <a href='https://bugs.kde.org/351412'>#351412</a></li>
<li>ConsoleKit2 support for screenlocker. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=72578284a1fda5f012cafcaccad6069fadbf9a25'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124469'>#124469</a></li>
</ul>


<h3><a name='plasma-workspace-wallpapers' href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git'>Plasma Workspace-wallpapers</a> </h3>
<ul id='ulplasma-workspace-wallpapers' style='display: block'>
<li>Last wallpaper from plasma 5.5 contribution summer 1am. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=7f7259792d3bfa2361d0cf13a405a3bcc8b2b3e9'>Commit.</a> </li>
<li>Add Plasma 5.5 wallpaper contest winners. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=87b528f20766f04c61eead38f4efaa299ad2137a'>Commit.</a> </li>
<li>New wallpapers from the Plasms 5.5 wallpaper contest set2. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=30568e2caa78cc253b89bb3efa62819d6c66ecd0'>Commit.</a> </li>
<li>New wallpapers from the Plasms 5.5 wallpaper contest set1. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=438728c20045898270dd8e1bcd91197f177833f0'>Commit.</a> </li>
<li>Set version to 5.4.90. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=5d1b45dba707a1edb1fb0bd93b13b2471d442a8f'>Commit.</a> </li>
<li>Add version to follow plasma convention. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=56bc73bf902f6eb8985d98ee506282151bb90b72'>Commit.</a> </li>
<li>Two new wallpapers from ken and lionel. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=c21c05ac2a46e32357077af9e093345c4f35ad3d'>Commit.</a> </li>
<li>Resize gray wallpaper (export to jpg with 75% compression). <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=82cf0ca05776f504980301b531b7b0c848fa6441'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Simulate user activity when session becomes active. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=3c41d4c3eca34c54e0d6a2b8196f9d8e3dd8dc03'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354250'>#354250</a></li>
<li>Use absolute brightness value in conservative check when loading profile. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=6b4ffe32367a6413d8c270480445a4955d63e71f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355687'>#355687</a>. Code review <a href='https://git.reviewboard.kde.org/r/126138'>#126138</a></li>
<li>Fix brightness key being ignored during brightness animation. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=56e8b755c63a85568472f563fc65642e1de9f6da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125182'>#125182</a></li>
<li>Revert "Revert "[dpms] Add a Wayland implementation"". <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=5d3536b1544a7e976dd223151fbb94c02198239c'>Commit.</a> </li>
<li>Show fully charged message when battery state becomes "Fully charged" rather than "Not charging". <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=4c66646761834236f6b14789162fe71f6179f472'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354623'>#354623</a></li>
<li>Revert "[dpms] Add a Wayland implementation". <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=10b37d634ffe53f060f441263ff02eb05776eec4'>Commit.</a> </li>
<li>[dpms] Add a Wayland implementation. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=3321b9d2c78c64e6e7c9be4f7f14cb2776bc2e71'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125026'>#125026</a></li>
<li>[dpms] Split the Xcb code out of PowerDevilDpmsAction. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=a6eed9bb67bb0036bf7cd5e826ad9eabc3d62d2a'>Commit.</a> </li>
<li>Add lidClosedChanged signal to org.kde.Solid.PowerManagement. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=b5a1e1bec3052460ff8c87764a065cc61cdeba47'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/121915'>#121915</a></li>
<li>Port kded plugin to json metadata... <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=019ec4784a8504767669ededd17c6b0a13363328'>Commit.</a> </li>
<li>Unify terms Sleep -> Suspend. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=a33e0f60f85eee42f07a1ac73896a8bb01153995'>Commit.</a> See bug <a href='https://bugs.kde.org/351412'>#351412</a></li>
<li>Add support for toggling screen on/off with a button. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=4c4d7ba5eee83133948ddab398518533691cc9a8'>Commit.</a> </li>
<li>Make UDev mandatory. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=73e13daef13d38948b1194d2d1103404fdd808b7'>Commit.</a> </li>
<li>Remove HAL backend. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=b7c5bbd96757ea7367028e7855f0c7bc94d036ad'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='http://quickgit.kde.org/?p=sddm-kcm.git'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Add a .reviewboardrc file. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=3ebd47dded5a55f931137f6756451a7a524d936d'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='http://quickgit.kde.org/?p=systemsettings.git'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Use the plural form for "Notification" category. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=571985f5fd8b2ab1b0d8c91aeb96135ba04a789b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126018'>#126018</a></li>
<li>Use new connect api. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=fd20251e5296a1a5cadbabac396afc038ded6561'>Commit.</a> </li>
<li>Change System Settings toolbars. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=12f009f76371cabb9cab323f062703089781209a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125111'>#125111</a></li>
<li>Make the classic module optional and thus the dependencie to khtml. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=b78ceaf804d5d1137bf3fe3f429886879d4b5954'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125161'>#125161</a></li>
<li>Icons: Use action Icons in Toolbars according our HIGs. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=e8dac66ea54476ca3f302aa2e970bea0e063d377'>Commit.</a> </li>
<li>Adjust icon names for Breeze theme in Systemsetting overview. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=8386d8a43b9f7ebf3c1977877a3b4b15a290bb9a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348429'>#348429</a></li>
</ul>


<h3><a name='user-manager' href='http://quickgit.kde.org/?p=user-manager.git'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Refactored PWQuality detection and fix linking for OS X. <a href='http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=f657a7841c24ba1191a2a6168e24e4adee643e6a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125055'>#125055</a></li>
</ul>


<?php
  include("footer.inc");
?>
