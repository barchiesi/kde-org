<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships July Updates to Applications, Platform and Plasma Workspaces");
  $site_root = "../";
  $release = '4.10.5';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php i18n("July 2, 2013. Today KDE released updates for its Workspaces, Applications and Development Platform. These updates continue the series of monthly stabilization updates to the 4.10 series. 4.10.5 updates bring many bugfixes and translation updates on top of the 4.10 release and are recommended updates for everyone running the 4.10 release series. As this release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone.");?>
<br /><br />
	<?php print i18n_var("The over 30 recorded bugfixes include improvements to the Personal Information Management suite Kontact, the File Manager Dolphin, and others. The changes are listed on <a href='%1'>KDE's issue tracker</a>. For a detailed list of changes that went into 4.10.5, you can browse the Subversion and Git logs.", "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.10.5&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=");?>
<br /><br />
<?php i18n("To download source code or packages to install go to the <a href='http://www.kde.org/info/4.10.5.php'>4.10.5 Info Page</a>. If you want to find out more about the 4.10 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.10/'>4.10 release notes</a>.");?>
</p>


<div align="center" class="screenshot">
<a href="4.10/screenshots/jpg/plasma-tasks.jpg"><img alt="<?php i18n("KDE's Dolphin File Manager");?>" src="4.10/screenshots/thumbs/plasma-tasks.png" />
<br />
<em><?php i18n("KDE's Dolphin File Manager");?></em>
</a>
</div>

<p align="justify">
<?php i18n("KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a
href='http://download.kde.org/stable/4.10.5/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.");?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing 4.10.5 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of 4.10.5 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/4.10.5.php#binary'>4.10.5 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Compiling 4.10.5");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for 4.10.5 may be <a href='http://download.kde.org/stable/4.10.5/src/'>freely downloaded</a>. Instructions on compiling and installing 4.10.5 are available from the <a href='/info/4.10.5.php'>4.10.5 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
