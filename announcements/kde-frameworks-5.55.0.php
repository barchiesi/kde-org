<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.55.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.55.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
February 09, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.55.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[tags_kio] Disable access with a double slashed url, i.e. \"tags://\" (bug 400594)");?></li>
<li><?php i18n("Instantiate QApplication before KCrash/KCatalog");?></li>
<li><?php i18n("Ignore all non-storage deviceAdded signals from Solid");?></li>
<li><?php i18n("Use the nicer K_PLUGIN_CLASS_WITH_JSON");?></li>
<li><?php i18n("Remove Qt 5.10 checks now that we require it as min version");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add Cursors KCM icon");?></li>
<li><?php i18n("Add and rename some YaST icons and symlinks");?></li>
<li><?php i18n("Improve the Notification Bell Icon by using the KAlarm design (bug 400570)");?></li>
<li><?php i18n("Add yast-upgrade");?></li>
<li><?php i18n("Improve weather-storm-* icons (bug 403830)");?></li>
<li><?php i18n("Add font-otf symlinks, just like the font-ttf symlinks");?></li>
<li><?php i18n("Add proper edit-delete-shred icons (bug 109241)");?></li>
<li><?php i18n("Add trim margins and trim to selection icons (bug 401489)");?></li>
<li><?php i18n("Delete edit-delete-shred symlinks in preparation for replacing them with real icons");?></li>
<li><?php i18n("Rename Activities KCM icon");?></li>
<li><?php i18n("Add Activities KCM icon");?></li>
<li><?php i18n("Add Virtual Desktops KCM icon");?></li>
<li><?php i18n("Add icons for Touch Screen and Screen Edge KCMs");?></li>
<li><?php i18n("Fix file sharing preference related icon names");?></li>
<li><?php i18n("Add a preferences-desktop-effects icon");?></li>
<li><?php i18n("Add a Plasma Theme Preferences Icon");?></li>
<li><?php i18n("Improve contrast of preferences-system-time (bug 390800)");?></li>
<li><?php i18n("Add a new preferences-desktop-theme-global icon");?></li>
<li><?php i18n("Add tools icon");?></li>
<li><?php i18n("document-new icon follow ColorScheme-Text");?></li>
<li><?php i18n("Add a preferences-system-splash icon for the Splash Screen KCM");?></li>
<li><?php i18n("Include applets/22");?></li>
<li><?php i18n("Add Kotlin (.kt) mimetype icons");?></li>
<li><?php i18n("Improve the preferences-desktop-cryptography icon");?></li>
<li><?php i18n("Fill lock in preferences-desktop-user-password");?></li>
<li><?php i18n("Consistently fill the lock in the encrypted icon");?></li>
<li><?php i18n("Use a Kile Icon that is similar to the original");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("FindGperf: in ecm_gperf_generate set SKIP_AUTOMOC for generated file");?></li>
<li><?php i18n("Move -Wsuggest-override -Wlogical-op to regular compiler settings");?></li>
<li><?php i18n("Fix python binding generation for classes with deleted copy constructors");?></li>
<li><?php i18n("Fix qmake module generation for Qt 5.12.1");?></li>
<li><?php i18n("Use more https in links");?></li>
<li><?php i18n("API dox: add missing entries for some find-modules &amp; modules");?></li>
<li><?php i18n("FindGperf: improve api dox: mark-up usage example");?></li>
<li><?php i18n("ECMGenerateQmlTypes: fix api dox: title needs more --- markup");?></li>
<li><?php i18n("ECMQMLModules: fix api dox: title match module name, add missing \"Since\"");?></li>
<li><?php i18n("FindInotify: fix api dox .rst tag, add missing \"Since\"");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("fix for macOS");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Save two KFilterDev::compressionTypeForMimeType calls");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Remove support for passing gui QVariants to KAuth helpers");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Build without D-Bus on Android");?></li>
<li><?php i18n("Const'ify");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("[kcmutils] Add ellipsis to search labels in KPluginSelector");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("nsSJISProber::HandleData: Don't crash if aLen is 0");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconfig_compiler: delete the assignment operator and copy constructor");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Build without KAuth and D-Bus on Android");?></li>
<li><?php i18n("Add KLanguageName");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Comment why changing the ptracer is required");?></li>
<li><?php i18n("[KCrash] Establish socket to allow change of ptracer");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[KCM Controls GridView] Add remove animation");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix some country flags to use all the pixmap");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("handle wrong password when using sudo which asks for another password (bug 389049)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("exiv2extractor: add support for bmp, gif, webp, tga");?></li>
<li><?php i18n("Fix failing test of exiv gps data");?></li>
<li><?php i18n("Test empty and zero gps data");?></li>
<li><?php i18n("add support for more mimetypes to taglibwriter");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holidays/plan2/holiday_ua_uk - updated for 2019");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Add JSON metadata to khtmlpart plugin binary");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Build without D-Bus on Android");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("xcf: Fix fix for opacity being out of bounds");?></li>
<li><?php i18n("Uncomment the qdebug includes");?></li>
<li><?php i18n("tga: Fix Use-of-uninitialized-value on broken files");?></li>
<li><?php i18n("max opacity is 255");?></li>
<li><?php i18n("xcf: Fix assert in files with two PROP_COLORMAP");?></li>
<li><?php i18n("ras: Fix assert because of ColorMapLength being too big");?></li>
<li><?php i18n("pcx: Fix crash on fuzzed file");?></li>
<li><?php i18n("xcf: Implement robustness for when PROP_APPLY_MASK is not on the file");?></li>
<li><?php i18n("xcf: loadHierarchy: Obey the layer.type and not the bpp");?></li>
<li><?php i18n("tga: Don't support more than 8 alpha bits");?></li>
<li><?php i18n("ras: Return false if allocating the image failed");?></li>
<li><?php i18n("rgb: Fix integer overflow in fuzzed file");?></li>
<li><?php i18n("rgb: Fix Heap-buffer-overflow in fuzzed file");?></li>
<li><?php i18n("psd: Fix crash on fuzzed file");?></li>
<li><?php i18n("xcf: Initialize x/y_offset");?></li>
<li><?php i18n("rgb: Fix crash in fuzzed image");?></li>
<li><?php i18n("pcx: Fix crash on fuzzed image");?></li>
<li><?php i18n("rgb: fix crash in fuzzed file");?></li>
<li><?php i18n("xcf: initialize layer mode");?></li>
<li><?php i18n("xcf: initialize layer opacity");?></li>
<li><?php i18n("xcf: set buffer to 0 if read less data that expected");?></li>
<li><?php i18n("bzero -&gt; memset");?></li>
<li><?php i18n("Fix various OOB reads and writes in kimg_tga and kimg_xcf");?></li>
<li><?php i18n("pic: resize header id back if didn't read 4 bytes as expected");?></li>
<li><?php i18n("xcf: bzero buffer if read less data than expected");?></li>
<li><?php i18n("xcf: Only call setDotsPerMeterX/Y if PROP_RESOLUTION is found");?></li>
<li><?php i18n("xcf: initialize num_colors");?></li>
<li><?php i18n("xcf: Initialize layer visible property");?></li>
<li><?php i18n("xcf: Don't cast int to enum that can't hold that int value");?></li>
<li><?php i18n("xcf: Do not overflow int on the setDotsPerMeterX/Y call");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("KLauncher: handle processes exiting without error (bug 389678)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Improve keyboard controls of the checksum widget");?></li>
<li><?php i18n("Add helper function to disable redirections (useful for kde-open)");?></li>
<li><?php i18n("Revert \"Refactor SlaveInterface::calcSpeed\" (bug 402665)");?></li>
<li><?php i18n("Don't set CMake policy CMP0028 to old. We don't have targets with :: unless they are imported.");?></li>
<li><?php i18n("[kio] Add ellipsis to search label in Cookies section");?></li>
<li><?php i18n("[KNewFileMenu] Don't emit fileCreated when creating a directory (bug 403100)");?></li>
<li><?php i18n("Use (and suggest using) the nicer K_PLUGIN_CLASS_WITH_JSON");?></li>
<li><?php i18n("avoid blocking kio_http_cache_cleaner and ensure exit with session (bug 367575)");?></li>
<li><?php i18n("Fix failing knewfilemenu test and underlying reason for its failure");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("raise only colored buttons (bug 403807)");?></li>
<li><?php i18n("fix height");?></li>
<li><?php i18n("same margins sizing policy as the other list items");?></li>
<li><?php i18n("=== operators");?></li>
<li><?php i18n("support for the concept of expandible items");?></li>
<li><?php i18n("don't clear when replacing all the pages");?></li>
<li><?php i18n("horizontal padding is double than vertical");?></li>
<li><?php i18n("take padding into account to compute size hints");?></li>
<li><?php i18n("[kirigami] Do not use light font styles for headings (2/3) (bug 402730)");?></li>
<li><?php i18n("Unbreak the AboutPage layout on smaller devices");?></li>
<li><?php i18n("stopatBounds for breadcrumb flickable");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("[kitemviews] Change the search in Desktop Behavior/Activities to more in line with other search labels");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Set SKIP_AUTOMOC for some generated files, to deal with CMP0071");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix semantics for ghns_exclude (bug 402888)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix memory leak when passing icon data to Java");?></li>
<li><?php i18n("Remove the AndroidX support library dependency");?></li>
<li><?php i18n("Add Android notification channel support");?></li>
<li><?php i18n("Make notifications work on Android with API level &lt; 23");?></li>
<li><?php i18n("Move the Android API level checks to runtime");?></li>
<li><?php i18n("Remove unused forward declaration");?></li>
<li><?php i18n("Rebuild AAR when Java sources change");?></li>
<li><?php i18n("Build the Java side with Gradle, as AAR instead of JAR");?></li>
<li><?php i18n("Don't rely on the Plasma workspace integration on Android");?></li>
<li><?php i18n("Search for notification event configuration in qrc resources");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Make translations work");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Fix struct/class mismatch");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Build without D-Bus on Android");?></li>
<li><?php i18n("Suggest people to use K_PLUGIN_CLASS_WITH_JSON");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Qt 5.12.0 has issues in the regex implementation in QJSEngine, indenters might behave incorrectly, Qt 5.12.1 will have a fix");?></li>
<li><?php i18n("KateSpellCheckDialog: Remove action \"Spellcheck Selection\"");?></li>
<li><?php i18n("Update JavaScript library underscore.js to version 1.9.1");?></li>
<li><?php i18n("Fix bug 403422: Allow changing the marker size again (bug 403422)");?></li>
<li><?php i18n("SearchBar: Add Cancel button to stop long running tasks (bug 244424)");?></li>
<li><?php i18n("Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH");?></li>
<li><?php i18n("Review KateGotoBar");?></li>
<li><?php i18n("ViewInternal: Fix 'Go to matching bracket' in override mode (bug 402594)");?></li>
<li><?php i18n("Use HTTPS, if available, in links visible to users");?></li>
<li><?php i18n("Review KateStatusBar");?></li>
<li><?php i18n("ViewConfig: Add option to paste at cursor position by mouse (bug 363492)");?></li>
<li><?php i18n("Use the nicer K_PLUGIN_CLASS_WITH_JSON");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[server] Generate correct touch ids");?></li>
<li><?php i18n("Make XdgTest spec compliant");?></li>
<li><?php i18n("Add option to use wl_display_add_socket_auto");?></li>
<li><?php i18n("[server] Send initial org_kde_plasma_virtual_desktop_management.rows");?></li>
<li><?php i18n("Add rows info to the plasma virtual desktop protocol");?></li>
<li><?php i18n("[client] Wrap wl_shell_surface_set_{class,title}");?></li>
<li><?php i18n("Guard resource deletion in OuptutConfiguration::sendApplied");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KWidgetsAddons] Do not use light font styles for headings (3/3) (bug 402730)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Build without D-Bus on Android");?></li>
<li><?php i18n("Make KCheckAccelerators less invasive for apps that don't directly link to KXmlGui");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Fix QVariantMapList operator &gt;&gt; implementation");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Wallpaper templates] Add missing Comment= entry to desktop file");?></li>
<li><?php i18n("Share Plasma::Theme instances between multiple ColorScope");?></li>
<li><?php i18n("Make the clock svg's shadows more logically correct and visually appropriate (bug 396612)");?></li>
<li><?php i18n("[frameworks] Do not use light font styles for headings (1/3) (bug 402730)");?></li>
<li><?php i18n("[Dialog] Don't alter mainItem's visibility");?></li>
<li><?php i18n("Reset parentItem when mainItem changes");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Use the nicer K_PLUGIN_CLASS_WITH_JSON");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix combobox initial sizing (bug 403736)");?></li>
<li><?php i18n("Set combobox popups to be modal (bug 403403)");?></li>
<li><?php i18n("partly revert 4f00b0cabc1230fdf");?></li>
<li><?php i18n("Word-wrap long tooltips (bug 396385)");?></li>
<li><?php i18n("ComboBox: fix default delegate");?></li>
<li><?php i18n("Fake mousehover whilst combobox is open");?></li>
<li><?php i18n("Set CombooBox QStyleOptionState == On rather than Sunken to match qwidgets (bug 403153)");?></li>
<li><?php i18n("Fix ComboBox");?></li>
<li><?php i18n("Always draw the tooltip on top of everything else");?></li>
<li><?php i18n("Support a tooltip on a MouseArea");?></li>
<li><?php i18n("do not force text display for ToolButton");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Build without D-Bus on Android");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't call this code if we have only space");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix end of folding region in rules with lookAhead=true");?></li>
<li><?php i18n("AsciiDoc: Fix highlighting of include directive");?></li>
<li><?php i18n("Add AsciiDoc support");?></li>
<li><?php i18n("Fixed Bug Which Caused Infinite Loop While Highlighting Kconfig Files");?></li>
<li><?php i18n("check for endless context switches");?></li>
<li><?php i18n("Ruby: fix RegExp after \": \" and fix/improve detection of HEREDOC (bug 358273)");?></li>
<li><?php i18n("Haskell: Make = a special symbol");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.55");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
