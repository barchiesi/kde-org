<?php
  $page_title = "KDE 1.0-beta4 Information";
  $site_root = "../";
  include "header.inc";
?>

<CENTER><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
<TR VALIGN=TOP>
<TD>
<BR>
<h2>Where to get the base packages</h2>

You can download the KDE base  packages  from
ftp.kde.org or one of its many mirrors. The  precise
locations of the beta 4 packages on ftp.kde.org are:<p>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/tgz/binary">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/tgz/binary</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/tgz/source">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/tgz/source</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/rpm/binary">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/rpm/binary</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/rpm/source">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/rpm/source</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/deb">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/deb/binary</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/deb">
ftp://ftp.kde.org/pub/kde/stable/Beta4/distribution/deb/source</a><br>

<p>
There are a great number of applications available for KDE.<br> 
<ul>
<li><a href="ftp://ftp.kde.org/pub/kde/unstable/apps/">
ftp://ftp.kde.org/pub/kde/unstable/apps</a><br>
<li> <a href="ftp://ftp.kde.org/pub/kde/stable/Beta4/apps/">
ftp://ftp.kde.org/pub/kde/stable/Beta4/apps</a><br>
</ul>
Those in the 'stable' directory tree should work fine with KDE Beta 4,
while those in the unstable directory tree are not guaranteed to compile
and install without problems. Nevertheless, most work fine.
<p>
Please consider downloading from one of our many 
<a href="mirrors.html">mirror</a> sites.
<p>
You may also want to browse the
<a href="applications.html">KDE applications</a>
and the
<a href="cvs-applications.html">KDE applications in the CVS</a>
pages, for a complete overview of all available KDE applications,
Only a small percentage of all available KDE applications are part of the base distributions
made available above.
<p>
In case you feel adventurous you can follow KDE development through the daily
snapshots of our work made available  
<a href="ftp://ftp.kde.org/pub/kde/unstable/CVS/snapshots/">here</a>. Do not
forget to subscribe to our <a href="../contact.html">mailing lists</a>. This will
keep you up to date with KDE development and it is here that you can find
installation assistance. A mailing list archive is provided <a href="http://www.itm.mu-luebeck.de/~coolo/kde/">here</a>. We suggest that you voice general comments 
and problems on the mailings lists first, before contacting individual developers.

<p>
If you enjoy KDE please consider <a href="support.html">supporting</a> the KDE project.

<H2>Installation instructions</H2>
<P>
These are the installation instructions for the Beta 4. Please read them
carefully and try to help yourself out if anything goes wrong. If you
need further assistance, consider joining the 
<A HREF="../contact.html">kde mailing lists</A>.
</P>

<H3>Available package formats</H3>
<P>
The KDE team provides five different kinds of packages: source and binary
RPMs, source and binary .tgz files and binary Debian files. The
installation process depends on which package format you choose. After
installation, there are post-installation procedures that apply for
all package 
formats. Note that if you are an inexperienced Unix user and have a
RPM-based system such as Red Hat Linux or SuSE Linux 5.0 or higher, you
are probably best off choosing the binary RPM package. If you need
special configuration options (e.g. because you have shadow passwords
and want to use the screensavers), your best bet is to use the
source-tgz packages and compile the KDE Desktop Environment
yourself. The binary RPM packages are made available in two versions:
compiled for libc5 and libc6 (also known as glibc2). The second
version is mainly for Red Hat 5.0 users.
</P>

<H3>Prequisites</H3>
<P>
You need the Qt library (and header files if you want to compile KDE
yourself), version 1.33 or higher, available at no cost from
<A HREF="http://www.troll.no/dl">http://www.troll.no/dl</A>. 
You also need the libgr which should be included in most distributions. 
Please make also shure that your local loopback device is setup correctly.
</P>

<H3>Available packages</H3>
<P>
The base distribution currently consists of nine packages. Some are
required, some are optional. Each package is available in each of the
aforementioned package formats.
</P>

<UL>
<LI><b>kdesupport</b>	<BR><tt>RECOMMENDED</tt><BR>
		This package contains support libraries that have
		not been written as part of the KDE project, but
		are needed nevertheless. If you are already have
		the libraries (libgif, libjpeg, libmime, libuu,
		libgdbm) in this package in the required versions,
		you do not need to install this package. When in
		doubt, install it. Note that for the Debian packages, this
		package is required.

<LI><b>kdelibs</b>	<BR><tt>REQUIRED</tt><BR>
		This package contains shared libraries that are
		needed by all KDE applications.

<LI><b>kdebase</b>	<BR><TT>REQUIRED<BR></tt>
		This package contains the base applications that
		form the core of the KDE Desktop Environment like
		the window manager, the terminal emulator, the
		control center, the file manager and the panel.

<LI><b>kdegames</b>	<BR><TT>OPTIONAL<BR></tt>
		Various games like kmahjongg, ksnake, kasteroids
		and ktetris.

<LI><b>kdegraphics</b>	<BR><TT>OPTIONAL<BR></tt>
		Various graphics related programs like kghostview,
		kfax, kdvi and kpaint.

<LI><b>kdeutils</b>	<BR><TT>OPTIONAL<BR></tt>
		Various desktop tools like a calculator, an editor
		and other utilities.

<LI><b>kdemultimedia</b> <BR><TT>OPTIONAL<BR></tt>
		Multimedia applications like a CD player, midi player,
                mod player, mixer and more.
	
<LI><b>kdenetwork</b>	<BR><TT>OPTIONAL<BR></tt>
		Internet applications. Currently contains the mail
		program kmail, the news reader knews and several other
		network-related programs.

<LI><b>kdeadmin</b> <BR><TT>OPTIONAL<BR></tt>
		System administration programs. Currently contains the user
		manager kuser and the runlevel editor ksysv.
</UL>
<P>
kdesupport (if needed) should be installed before everything else. The
next (or first) package should be kdelibs. The other packages can be
installed in an arbitrary order.
</P>
<P>
In addition to these packages, there is a package called
<em>kdeinstruments</em> which contains MIDI instruments for use with
<em>kmidi</em> from the package <em>kdemultimedia</em>.
</P>

<H3>Installation instructions for the different package formats</H3>

<H4>Installation of the Debian packages</H4>
<P>
The Debian packages install into /opt/kde. This is compliant with the
upcoming FHS (file hierarchy standard).
</P> 
<P>
<B>To install the Debian package:</B>
</P>
<UL>
<LI>Become superuser
<LI>dpkg -i &lt;packagename&gt;.deb
</UL>
<P>
and accordingly for the other packages
</P>

<H4>Installation of the RPM packages</H4>
<P>
The RPM packages install into /opt/kde.
</P>
<P>
<B>To install the binary RPM:</B>
</P>
<UL>
<LI>Become superuser
<LI>Execute: rpm -i &lt;packagename&gt;.rpm
</UL>
<P>
<B>To create a binary RPM from the source rpm and install it do the
following:</B>
</P>
<UL>
<LI>Become superuser
<LI>rpm -i kdesupport-Beta4.src.rpm
<LI>cd /usr/src/redhat/SPECS
<LI>rpm -bb kdesupport-Beta4-1.spec
<LI>cd ../RPMS/i386 (or whatever architecture you use)
<LI>rpm -i kdesupport-Beta4-1.i386.rpm
</UL>

<H4>Installation of the source .tgz files</H4>
<P>
The source .tgz package installs into /usr/local/kde per default. You
can override this setting by using the --prefix option of the
configure script.
</P>
<UL>
<LI>Unpack the packages with: tar xvfz &lt;packagename&gt;.tgz
<LI>Change directory in to the package directory: cd &lt;packagename&gt;
<LI>Configure the package: ./configure<BR>
Some packages (notably kdebase) have special configuration options
that might be applicable to your installation. Call ./configure
--help to see the available options.
<LI>Build the package: make
<LI>Install the package: su -c &quot;make install&quot;
</UL>

<H4>Installation of the binary .tgz files</H4>
<P>
<B>The binary .tgz package installs into /opt/kde.</B>
</P>
<UL>
<LI>Become superuser
<LI>cd /
<LI>tar xvfz &lt;packagename&gt;.tgz
</UL>

<H3>Post-installation procedures</H3>
<P>
First of all, make sure that you have added KDE's binary installation
directory (e.g. /opt/kde/bin) to your PATH and KDE's library
installation directory to your LD_LIBRARY_PATH (only necessary on
systems that do not support rpath; on Linux ELF, it should work
without) this environment variable may be called differently on some
systems, e.g. it is called SHLIB_PATH on Irix). Then set the
environment variable KDEDIR to the base of your KDE tree, e.g. /opt/kde.
</P>
<P>
Even though you can use most of the KDE applications simply by calling
them, you can only benefit fully from KDE's advanced features if you
use the KDE window manager kwm and its helper programs.
</P>
<P>
In order to make it easy for you, we have provided a simple script
called startkde which gets installed in $KDEDIR/bin and is therefore
in your path. Edit the file .xinitrc in your home directory (make a
backup copy first!), remove everything that looks like calling a
window manager, and insert startkde instead. Restart X. This should
present you with your shining new KDE desktop. You can now start to
explore the wonderful world of KDE. In case you want to read some
documentation first, there is a quickstart guide at
http://www.kde.org/documentation/desktop.html. Also, every application
has an online help that is available via the help menu.
</P>

<H3>NO WARRANTY</H3>
<P>
[This text taken from the GPL.]<BR>
BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM &quot;AS IS&quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.
</P>
<P>
IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.
</P>

<H3>Reporting bugs</H3>
<P>
Please report bug reports at our <A HREF="http://buglist.kde.org">bug
report site</A>. If you do not have access to the WWW but can send
mail to the Internet, please send your bug report to
<A HREF="&#109;&#097;&#x69;lt&#00111;&#58;&#107;&#x64;e&#x2d;&#098;&#117;g&#x73;&#x40;&#x6b;de.&#111;rg">kde-b&#00117;g&#115;&#64;k&#100;&#0101;.o&#x72;&#x67;</A> where they will be
distributed to the developer in charge.
</P>
<P>
Have fun with KDE!
</P>

</P>
</BODY>
</HTML>

</TD>
</TR>
</TABLE></CENTER>

<?php include "footer.inc" ?>
