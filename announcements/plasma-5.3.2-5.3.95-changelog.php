<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.3.95 complete changelog");
$site_root = "../";
$release = 'plasma-5.3.95';
include "header.inc";
?>
<p><a href="plasma-5.3.95.php">Plasma 5.3.95</a> complete changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>KCM Adapters: Implement "Defaults" action. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=e84b6e37c314ef28ba7fa42c1d6df312c1c97712'>Commit.</a> </li>
<li>KCM Advanced Settings: Immediately apply receive files settings. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=bd306e3909c4e9613c1b78d41c5142ca2c3df43d'>Commit.</a> </li>
<li>KCM: Rename Transfers module to Advanced Settings. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=c505bae2de23eadfd8310658318b0b8004a6cba3'>Commit.</a> </li>
<li>KCM Adapters: Fix reseting settings after clicking on "Reset" button. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=e959248e9961f69f4bc531c8f27fefc22cd3e0ac'>Commit.</a> </li>
<li>Make BlueDevil plasmoid belong to Hardware category. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3d6fa77e703dd859a14fd21057c9862d054e5ed9'>Commit.</a> </li>
<li>Applet: Use property QtObject for QObject*. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=fa47635308560470c8564691d0e6defc789d21da'>Commit.</a> </li>
<li>Applet: BluezQt.Manager is now singleton. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=71cce44519539398c37e633a3939520899b52063'>Commit.</a> </li>
<li>Prepare for single QQmlengine mode. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=7813ea168a3a2be88081e88e25f63bb73e11579c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123859'>#123859</a></li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=ae0a60a7b040b966b5a2e0cea459793c2a4ff2e1'>Commit.</a> </li>
<li>Adapt to changes in BluezQt. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=bf153dcaeec080278bcd9440dda9ee5f1e9595d4'>Commit.</a> </li>
<li>Wizard: Merge all pairing pages to single page. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=63d1c901ab9047c0c637cbfbc3ad352727b2d9e5'>Commit.</a> </li>
<li>Remove now unused org.bluez.obex interface files. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=38c146ba0cc2ad2f8be6c010a4877fdfc7e679d9'>Commit.</a> </li>
<li>Sendfile: Add FailPage when connecting to the device fails. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3833024a47693ae922d2c25e1d700b52e30befef'>Commit.</a> </li>
<li>Sendfile: Show names of selected files next to select files button. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=67758db8a3118b189a19b6995c2591de96219b69'>Commit.</a> </li>
<li>Sendfile: Improve text in window title. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=4eb6e40cb85155bed1191936e68237c98f8c0d0f'>Commit.</a> </li>
<li>Wizard: Allow to restart wizard without restarting the process. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=51492f9e6795505435be1aef5ed718afa8986cc9'>Commit.</a> </li>
<li>Wizard: Don't show paired devices in device list. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=7d014876dd1efa2267169e8493935f539213ca8a'>Commit.</a> </li>
<li>Use KAboutData to setup and process command line options. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=0d40e2f451b286bf1714c99ae2eb2c96690ded5a'>Commit.</a> </li>
<li>Make it build with QT_NO_CAST_FROM_ASCII. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=7f6649fcfcee34e506768130bec4bbc80cdf2428'>Commit.</a> </li>
<li>Use QRegularExpression instead of QRegExp. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=ffc33a70cd1e4b61a590baed607ef68025a78d8b'>Commit.</a> </li>
<li>Use KWindowSystem to force PIN dialog above other windows. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=d89831b835bcce309c6463c62b0cef3aab42fb7d'>Commit.</a> </li>
<li>Use QLineEdit instead of KLineEdit in requestpin helper. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=b3af4e7c2f0579af8f98d8ac5e2009b49d568a3c'>Commit.</a> </li>
<li>Applet: Rewrite device details component to use QtQuick Layouts. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=cc81c7d1a74253aa9583ece0abb4f2400b06c167'>Commit.</a> </li>
<li>Applet: Show busy animation also when disconnecting from device. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=1d94069707a5c88f544cb81f4308dc4ba2cf31a8'>Commit.</a> </li>
<li>Applet: Hide device details when device changes position in model. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=986a87abf533e211af55547805ebd78d2c82108b'>Commit.</a> </li>
<li>Adapt to changes in BluezQt. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=914ca7a22ee81137654cf5343c0fa37f1debe881'>Commit.</a> </li>
<li>Applet: Add media player section to device details. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=b5a305353f6c99b998963efba286fc9bf85e5ba1'>Commit.</a> </li>
<li>Applet: Simplify checks for empty/non-empty arrays. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=ae4e3b39ef938e66bbbbaab8598f70a4ff81a7d9'>Commit.</a> </li>
<li>Use KCMShell.open to launch the Bluetooth kcm. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=b2be12474941f2ae00c51ba77d856cf0f59f4a6d'>Commit.</a> </li>
<li>Fix krazy issues. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=8726391f9f50bbeb8cfd0f9d2cf8fe4262c0dcd0'>Commit.</a> </li>
<li>Sendfile: Remove unused QFileDialog pointer. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=9ebdd38c83be422809708a225b6593394005033b'>Commit.</a> </li>
<li>Applet: Use KIO import instead of manually calling KRun::runUrl. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=6870e00998d32d031e726032c28836a24febe3fb'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>New icons from andreas <kainz.a@gmail.com>. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f9261099047053894daa781f376f13955df2bb47'>Commit.</a> </li>
<li>Install Breeze High Contrast color scheme. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5320f4fd33424e719b83997b70552e34218de274'>Commit.</a> </li>
<li>Add Breeze High Contrast theme by Aaron Honeycutt. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3ebb6ed33fb6522b0f5ca855a9fbd2b79c165e65'>Commit.</a> </li>
<li>Breeze-icons: make links to calligra named app icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5a74fa7ce7a5c5f82120bd68bf585d80b751e702'>Commit.</a> </li>
<li>Breeze-icons: add folder-add icon and document-multiple. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=05ef30d868bab2fc6536504f3c7c9eac5cda7b6a'>Commit.</a> </li>
<li>Breeze-icons: small-places are from not fixed. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=61e89d4c00f4f04152f24b88ba38340edb750f98'>Commit.</a> </li>
<li>Breeze-icons: some missing app specific toolbar-icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7434f4e31fbc97e3ffcbe05d2c8f55e8ca0b22f9'>Commit.</a> </li>
<li>Update Breeze-Icons from the github working dictionary. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=dd4c5c5f217c911917bb7fb4030f4c79f29d7afb'>Commit.</a> </li>
<li>Moving kde specific toolbar icons into the right folder. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=27563396c6765a31a974003143a21762694734a0'>Commit.</a> </li>
<li>Update Breeze icon set from github working dic. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bd16dd1551bb2c1d7fb65fba3c61d288bd66eb2d'>Commit.</a> </li>
<li>Plasma 5.4 standard wallpaper from ken. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=97af5bcc98edfefd0897692e3c65f35d441fa6b9'>Commit.</a> </li>
<li>Breeze Icon: add nepomuk for Dolphin. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=865008662d0c0d4ff36a8e6d51a63ce8059c6d5d'>Commit.</a> </li>
<li>Breeze Icons: Add new icons and sync breeze with breeze-dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c517ddc31b4defd752b1e20850e9cc3283fe4fc8'>Commit.</a> </li>
<li>Also use shadow color in Breeze style. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d0ddfe88ff9e756c9fbb4fd52643bf443c99f6d9'>Commit.</a> </li>
<li>Added option to configure shadow color. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5a1f5c22c014875da0554f24dccbb8601b75f929'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349360'>#349360</a></li>
<li>Removed unused sideExtent variable. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6946054e7442be950ce96a53e6a30ab710250bc0'>Commit.</a> </li>
<li>Improved rendering of menu and mdi window shadows. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=73112d92e3541400edd4ab3f7eb62798b958ff92'>Commit.</a> </li>
<li>Next Wallpaper iteration. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a19b3c45986fc0a4c635df58d98dd7912ccc90a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346018'>#346018</a></li>
<li>[kdecoration] Add support for new KPlugin metadata. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5303e0ea6e7ae28ec5b39079f73ed6ef6378d567'>Commit.</a> </li>
<li>Breeze: update icon set Breeze and Breeez-dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a829b896bbf053dc441a94cae5b67f232c2423c1'>Commit.</a> </li>
<li>Added the (compile time) possibility to render CommandLinkButton flat;. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=787aad717b772d75be5c74cc9b7340ff1d46ac9d'>Commit.</a> </li>
<li>Fixed rendering of QCommandLinkButton. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2a24ebce25bae976b06711ae6f023dfad6694745'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348257'>#348257</a></li>
<li>BreezeIcons add geany support (with gtk2 icons). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7ecffa42ad61bacdd3edf653906664b104317c71'>Commit.</a> </li>
<li>Breeze: geany gtk app icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b6d1c39e150efca68279c90722a0bfc6bc26fd1d'>Commit.</a> </li>
<li>BreezeIcons: update folder-download symbolic link and filelight. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=721eb8f048d850d5ba85403a9574d9e831513e4b'>Commit.</a> </li>
<li>Fix install. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=fee7ac38c6e87d83ab8e506df206938bca8ac04b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124127'>#124127</a></li>
<li>BreezeIcons: app icon for kfind. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=728d2e378639d66f63ddca4bb3bd162e49cd1067'>Commit.</a> </li>
<li>BreezeIcons: Dolphin image icon for sidebar. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bda7d87f6cc58ea206b818b1c3428a3815c8c0f3'>Commit.</a> </li>
<li>BreezeIcon: remove wrong icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b5fc8be36f8590cf8870ddd4d1e3d0339acabcc1'>Commit.</a> </li>
<li>BreezeIcons: move app specific icons to toolbar-kde folder. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4d73a23e935d7f22974f764dbc7fcd8fb9bde7d6'>Commit.</a> </li>
<li>BreezeIcons: Update the icon set see working repositor. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=28e733212e0202846306dd181952a25f91c799c8'>Commit.</a> </li>
<li>Breeze: Icon set update. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0453985c5fb88f565d2237383204a8c8daa66a3e'>Commit.</a> </li>
<li>Properly account for MenuItem text size when computing sizeFromContents. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=48a5fd346bf8e59484e3d647ed26a88bc928a82d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349258'>#349258</a></li>
<li>Remove min border size requirement on the sides, for tiny border size and above. Bottom side is kept at min 4 pixels,. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6a1fcfe9db40df5603d04d21401eb30db2555ca8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349039'>#349039</a></li>
<li>Properly calculate caption rect when button lists are empty on one of the titlebar side. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6fad4358e2123186d1b35cf73cd1a928d225c5d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349107'>#349107</a></li>
<li>Render menu frame via PE_FrameMenu only for extended toolbars and QtQuick controls. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=28af75ac324e8c61e95396ac13cfd3ec14905fdf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348908'>#348908</a></li>
<li>Added tiny (=minimal) button size, for high-dpi screens. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=56bc52fe3c190358176ed201f1ab1905a59a2e8e'>Commit.</a> See bug <a href='https://bugs.kde.org/348399'>#348399</a></li>
<li>Added feature summary for xcb and x11-xcb. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b14b203467bb8248e8e5cc68b9d4d152fb09784f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347420'>#347420</a></li>
<li>- Cleanup tests in scrollarea event filter. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=46ca7603843b0215d13560f4b68b6139b4d61c6d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347971'>#347971</a></li>
<li>Removed color settings. They were redundant with DecoratedClient::color(...). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b3bb047ebdf9605a98f89ad0c114c398c0deeafb'>Commit.</a> </li>
<li>Default to Qt::AlignVCenter (instead of Qt::AlignTop) when vertical alignment flag is not set. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0fd84447e5393891b620a53b82cf391f3e787032'>Commit.</a> See bug <a href='https://bugs.kde.org/346679'>#346679</a></li>
<li>Removed extra character after #endif. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2e38f4e01af42b5476345731246f1eec4875aa59'>Commit.</a> </li>
<li>Make sure iconSize is valid before rendering. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4fa7ab51d66f9501691a083eadc4be018069c7cf'>Commit.</a> </li>
<li>Sanitize button positioning. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c2b890899c30f55a1eec48e7bf11a5293ca247aa'>Commit.</a> </li>
<li>Pass iconSize as button's parameter rather than trying to calculate it from geometry and offset. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9a63b05a247206f40877be2c800965332d71c028'>Commit.</a> </li>
<li>Build on ARM. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=176a3b892f8fac7b196a4c3a11e8103ebc40d573'>Commit.</a> </li>
<li>Fix icons inheritance. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d7c9dc7d812b1e40e7060fd078ae4916f568f3d3'>Commit.</a> </li>
<li>Rephrased shadow creation code, for readibility. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9ac25e775f290063656fceb7bcd751745bf76009'>Commit.</a> </li>
<li>Uncomment assignement to global shadow. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0f2f82dbaab3578c2fb948856ab7acf0aa385a16'>Commit.</a> </li>
<li>Use TabWidget for settings. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6234ca550214270fb6e12264c87765675fbb3cc2'>Commit.</a> </li>
<li>Properly update button's animation duration on reconfiguration. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=331f9b6cebca4207392fb24476622f1bf40627b2'>Commit.</a> </li>
<li>Uncomment deco count increment (should never have been commented in the first place). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c29e760a5e4c2b183bf2a2a807efe1da22c74ce0'>Commit.</a> </li>
<li>Trigger update directly, on active state changed, when animations are disabled. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=40f6fc4666057a52e1c44b336d7c4ccf89de74c2'>Commit.</a> </li>
<li>Removed debug output. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2a31156cca6d306c2ab8cb3db60ef5e881c511a7'>Commit.</a> </li>
<li>Overall cleanup to align with oxygen code. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d2ccf2df45e54efbe795ba956462e044d83aee25'>Commit.</a> </li>
<li>Use proper, animated, font color for caption rendering. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e9b31918c22230835094f91ffa7f5d70db0a11c5'>Commit.</a> </li>
<li>Do not add margins around items for qtquick controls. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=eecf425c76fa170db92031480e3148a595b42ad9'>Commit.</a> </li>
<li>Simplified rendering of menu titles. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=db245de81635f5656c69d5e15404c80a01b2f397'>Commit.</a> </li>
<li>Detect toolbutton in menus and mark as menu titles. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=661756757cf450bd8f403a35c6f54d2b3bcb1af8'>Commit.</a> </li>
<li>Changed parameter orders to better match parent class methods. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=51f123a029c709544daa8535ac891a5b8f230337'>Commit.</a> </li>
<li>Fix the icons size combobox for Breeze. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=809761512851f3a612e8fa48f45a930a541b0701'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346254'>#346254</a></li>
<li>Increase contrast (lightness) up to 15% separated (mimetypes). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6681b7b3dd7545bb1313be820b27000202f69b54'>Commit.</a> </li>
<li>Cleanup transition data, to align with oxygen. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4b080f8a2a8e70d010251fa8372ba280e1883084'>Commit.</a> </li>
<li>- re-added stacked widget transitions, based on oxygen code. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bd7466be3acd2de09d2d6ff43b4cf1ad571a1b59'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>GIT_COMMIT Update version number for 5.3.95. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=3695bc45d7c968b999fd1ac32e483265cc3a312a'>Commit.</a> </li>
<li>Remove calls to QTranslator. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=313bb5cda9f44af9962e848e945eaacfaf6b2bc6'>Commit.</a> </li>
<li>[minor] formatting and const fixes. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=2519800c1b02d096c8f9ab2088a35960bbb5aa29'>Commit.</a> </li>
<li>Replace deprecated use of KRun::run with KRun::runService as advices by the docs. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=1893d903fa2c4bc2ae04d2f8c5ceceb7f452d4ae'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=2bfe5775e57d8373144bf5948ab6233b45df74b3'>Commit.</a> </li>
<li>The kioclient commands ls, cat and openProperties were using direct user input as QUrl. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=787bdf628fdf9f74c978363e07ddad0ae306d8bf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123544'>#123544</a>CHANGELOGCHANGELOG: Regression fix. The kioclient commands ls, cat and openProperties should handle paths with no protocol as local paths (file protocol).</li>
</ul>


<h3><a name='kde-gtk-config' href='http://quickgit.kde.org/?p=kde-gtk-config.git'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Disable Help button in KCM kde-gtk-config. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=41380a9ac3d257d138453c31d8aad551516bdb5c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124462'>#124462</a></li>
<li>Include FindPackageHandleStandardArgs rather than find it. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=3d751fa7b762d20c2650ada5648c2a7bb1b4f007'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123303'>#123303</a></li>
</ul>


<h3><a name='kdecoration' href='http://quickgit.kde.org/?p=kdecoration.git'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Added explicit header coverage for a few types. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=4befe3b5324e7be8230d22c7c58417dc7fafdde0'>Commit.</a> </li>
<li>Update links to the project's home and repo. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=ad019acb93739f87bc31777bca0bae7db970e224'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>When dropping URLs onto the applet make links from them. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6077ac14683e06394827e887aed2cc25af1b19d3'>Commit.</a> </li>
<li>Also replace line breaks by &gt;br> for dropped data. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=984f81d94ecf7fd176f9c7867071d5167118e0c3'>Commit.</a> </li>
<li>Fix metadata. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=3b0739a76302e454ec55eda1e4c11f50b70339d9'>Commit.</a> </li>
<li>Add stub for the Kicker-based fullscreen launcher. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=4b4f5a4aa14919241bc6512f9f00a7bbd1083eea'>Commit.</a> </li>
<li>Improve sticky notes. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=01fcf475e8fb877ff932da3b7437c08b796df99d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124246'>#124246</a></li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e5b99a85ae2cbfe348f7b8f47bf39429ed87f237'>Commit.</a> </li>
<li>Remove unused imports. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d844b30047eed41aefabb39ecf1c0bba2a9938ef'>Commit.</a> </li>
<li>Add config UI for sticky notes. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c6277abcef9cb5c8f789928176ae775c95c66718'>Commit.</a> </li>
<li>Use preventStealing for drag and drop on the applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=29ba0c3758c02ab3330c8ed276c898e1367334fc'>Commit.</a> </li>
<li>Rename to org.kde.plasma.konsoleprofiles. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=db6129e7e54bdd0efcaf51dfe884ac171e213b20'>Commit.</a> </li>
<li>Fix konsoleprofile sizing. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e9d252086ccc9e78d9e1099add0b9bf762d9ef05'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350214'>#350214</a></li>
<li>Don't specify a library on pure QML applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=96182653740d493a506f99e2871468f20b35234f'>Commit.</a> </li>
<li>Install the plugin where it's expected. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=46fa9ede85febbf77e28c46145e2690f5193a12d'>Commit.</a> </li>
<li>Kimpanel: workaround window content not update. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=027d0ce9aab815dc75d0d0a198aac0ff2df90145'>Commit.</a> </li>
<li>The comic package is a KPackage. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7c5a61e967144e824f70fb6ab541fa6c62d76925'>Commit.</a> </li>
<li>Ported Konsoleprofiles to plasma5. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=fe29d5c9ae667f13d61bb86b56cce507b562601e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346783'>#346783</a></li>
<li>Kimpanel: workaround sometimes window content is not updated. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d3aa5855adf6635c64cbde8cd14fb5124fe97984'>Commit.</a> </li>
<li>Add a copy automatically option. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=084f05615934c59b7a3af622f87d715aaaf753ca'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7021e83efa7cf1cd9a9b7c6fd0549576b083c016'>Commit.</a> </li>
<li>Timer: added tooltip for compact mode (e.g. in panel) and full mode. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8f09567eae260a1241e65e2ec2a9e9ba28bbf0ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/259630'>#259630</a>. Code review <a href='https://git.reviewboard.kde.org/r/123420'>#123420</a></li>
<li>Timer: Added config ui for notification and title text. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c4ce08e5a384c3dfbf38c25abf6120f94fd6cdce'>Commit.</a> </li>
<li>Simplify. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=0ed980552dee8edf0ea504dc57163169969e73e8'>Commit.</a> </li>
<li>Use always share icon. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f68e7e38d56085aa01c501d085538aa37955620d'>Commit.</a> </li>
<li>Prefer contenttracker mime if available. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7c91368e6b235031596b8dfc505c5c62c60ddd80'>Commit.</a> </li>
<li>Don't leak the dbus connection. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=36bc6f424855cfd9c92b619ded8bd67ee146fcc7'>Commit.</a> </li>
<li>Dialog stays on top, looks like all other popups. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=698e78886110c2a3f5a686d0a16ebd72c5474e43'>Commit.</a> </li>
<li>Add a slc-like behavior on icon click. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=efb34252df863f44bd390bbbaef59fc321704d9b'>Commit.</a> </li>
<li>Remove optional X11 dependency. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=bdbc693e4657fb93d914aaf0542df21f46f43dc0'>Commit.</a> </li>
<li>Use KWindowSystem::showingDesktop instead of X11 hackery. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=168c600f8ddc54c675032d65378a3efb3494c014'>Commit.</a> </li>
<li>Ifdef HAVE_X11 has to be if HAVE_X11. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ec2992a97b905c8bda6edf743d14b5b5db7d268a'>Commit.</a> </li>
<li>Adapt to changes in Purpose. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=00ecdcefd7caf76f4950eca84a459e03b6c59992'>Commit.</a> </li>
<li>Work as standalone app. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1316e583d13d12f5d576ace717bbaebae7184f53'>Commit.</a> </li>
</ul>


<h3><a name='kgamma' href='http://quickgit.kde.org/?p=kgamma.git'>Gamma Monitor Calibration Tool</a></h3>
<ul id='ulkgamma' style='display: block'><li>New in this release</li></ul>
<h3><a name='khelpcenter' href='http://quickgit.kde.org/?p=khelpcenter.git'>KHelpCenter</a> </h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=39d8627d4cc4051ca5912bb3617bfe1274fecc61'>Commit.</a> </li>
<li>Update fundmentals docbook to kf5. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=5e6b2651596401c4fe1f4517ed873aceabf498f1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124535'>#124535</a></li>
<li>Proofread + update khelpcenter docbook to kf5. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=bf8d64f4ca32f3b8a45f7bf1b7442290334ad404'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124458'>#124458</a></li>
<li>Add missing find_package arguments for target_link_libraries calls. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=aeaee391b7ed96df3c98214dd77b3c9420c837f0'>Commit.</a> </li>
</ul>


<h3><a name='khotkeys' href='http://quickgit.kde.org/?p=khotkeys.git'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Ensure khotkeys doesn't crash without platform xcb. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=78dd1885b4d2d1d082056f4e591b6c930d1c9aad'>Commit.</a> </li>
<li>Disable debug messages about the gesture grab/ungrab. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=d085e9f99df7ed513b07941905adb8b162276b17'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>KInfoCenter</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Rename import to reflect it's a private thing used by the energy info module only. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=8bfa62a9474cd70d8526201ad5d62ef74b6c5d62'>Commit.</a> </li>
<li>Adaptions for smaller screens. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=19f21cfcea293b2fed599ebcf4b5f374136861cd'>Commit.</a> </li>
<li>Remove querying the indirect rendering context. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=9f9814219d899a64dd5b68711534ec7206858147'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344971'>#344971</a>. Code review <a href='https://git.reviewboard.kde.org/r/123995'>#123995</a></li>
<li>Set minimum versions of kf5. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=fd7f444b1e08d43dac0d0ec26e7b3c46290bbe2d'>Commit.</a> </li>
</ul>


<h3><a name='kmenuedit' href='http://quickgit.kde.org/?p=kmenuedit.git'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Revert "Remove separator functionality". <a href='http://quickgit.kde.org/?p=kmenuedit.git&amp;a=commit&amp;h=6f5e2d2783452ecba53875be1d66cb73d49d04c6'>Commit.</a> See bug <a href='https://bugs.kde.org/347412'>#347412</a></li>
<li>Update kmenuedit docbook and screenshots to plasma 5.3. <a href='http://quickgit.kde.org/?p=kmenuedit.git&amp;a=commit&amp;h=34defe724aedf6307f2b2cbe2491881a77424b1c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123529'>#123529</a></li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>KMessageWidget needs an explicit show(). <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=831a8f248230fb6527efccf30e7085c86760d182'>Commit.</a> </li>
<li>Add setting to adjust screen scaling. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=95afecda704d3782ece026c4abd52c29e96a6f98'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123468'>#123468</a></li>
<li>Print output::size instead of currentMode's. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=75465c74f26194ec69ae560e53cc7fe465b830e0'>Commit.</a> </li>
<li>Translate "Hz". <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=3e9c110e5c3b596f0423e176d594491731bc3720'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346236'>#346236</a></li>
</ul>


<h3><a name='ksshaskpass' href='http://quickgit.kde.org/?p=ksshaskpass.git'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Add docbook information to the manpage. <a href='http://quickgit.kde.org/?p=ksshaskpass.git&amp;a=commit&amp;h=b606f3ea0942a02c3b929096ac10bdcbc53bf956'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124006'>#124006</a></li>
<li>Fix missing category to silence `desktop-file-validate`. <a href='http://quickgit.kde.org/?p=ksshaskpass.git&amp;a=commit&amp;h=aa3c4e7797940e53433dfbd38a1bdc4b0a5727a6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123743'>#123743</a></li>
</ul>


<h3><a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Fix ECM warning. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=2c54c31b8c90d4d0ce0b2dbf9f9e6aee41732532'>Commit.</a> </li>
</ul>


<h3><a name='kwallet-pam' href='http://quickgit.kde.org/?p=kwallet-pam.git'>kwallet-pam</a></h3>
<ul id='ulkwallet-pam' style='display: block'><li>New in this release</li></ul>
<h3><a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a> </h3>
<ul id='ulkwayland' style='display: block'>
<li>Set default logging category to Critical. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=80513c0101fdff1644a831fd40946b0426933619'>Commit.</a> </li>
<li>Install categories file for KWayland. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6c074fe66d0c012360668204aa059558788a56bf'>Commit.</a> </li>
<li>Add a Shadow protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6922d5184037ea6f85d173b027c858c5e9d6b3be'>Commit.</a> </li>
<li>[client] Add ConnectionThread::roundtrip. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a6e1ad0da9fff9d2f6f54966b519df0199fe3fea'>Commit.</a> </li>
<li>[server] Add missing include. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c284dcd26c37891e3610838a08ab5b7a854190ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350058'>#350058</a></li>
<li>Adding support for a fake input interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c7298db15b5afd94674f9212997ce294fbf56adc'>Commit.</a> </li>
<li>Add support for an idle time interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=4701879039209a75630d835ed64ba9b14186ec57'>Commit.</a> </li>
<li>Expose internal window id in PlasmaWindow Management. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=85a7022fc761f418762d0a6ca8fadefe3012bc3f'>Commit.</a> </li>
<li>Better handle the creation of PlasmaWindow in PlasmaWindowManagementInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=83050743aaaa86f7214bff1ec84cda53b3a3952f'>Commit.</a> </li>
<li>Unbreak. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ee1f7a0f268b766e78b70fce4e6635432a9b9ce7'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9d3807b3e4ef659de25ad3c85b04b09aae1d93af'>Commit.</a> </li>
<li>[client] Ensure PlasmaWindowManagement::windowCreated is handled in correct thread. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=460c18bc3a8340e92ccf666d74529836ce3c89c5'>Commit.</a> </li>
<li>Add PlasmaWindowModel. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3f8463d57012118fbe9a108e459d166f0dd7ecff'>Commit.</a> </li>
<li>Add PlasmaWindow::requestActivate(). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d6bd50236a83274f4b405e8cef2ed03f70245a61'>Commit.</a> </li>
<li>Generate fake mouseevents on touch for xwayland. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cde26bc71c7148381e82799c17509966b03a93fb'>Commit.</a> </li>
<li>Add a themed icon name to PlasmaWindow interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8d1cd30c8f50afee4fc12fa39cc475c4c29e3fd4'>Commit.</a> </li>
<li>Add support for panel behavior in PlasmaShellSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=f25638848663b2cb6e4e53c8058cce88b65e1ad9'>Commit.</a> </li>
<li>Request state changes in PlasmaWindow interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8d1ef336a3c167e3603f33dd40c6e519aafec270'>Commit.</a> </li>
<li>Add close request to the PlasmaWindow interfaces. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=254b46da661e1c1677c7e3d4b0fc7325372b3b45'>Commit.</a> </li>
<li>[client] Add a convenient activeWindow to PlasmaWindowManagement. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d19370ef041bf5e7907c50a26ddce69dec0b63ff'>Commit.</a> </li>
<li>Add state handling to PlasmaWindow interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=0a753fd3bc4511e6c0ed778ee8b52b2a9ea36565'>Commit.</a> </li>
<li>[client] Create Compositor from QtWayland integration. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=e04c29847aed469b4e99c49b9f662e62ee99b03c'>Commit.</a> </li>
<li>[server] Destroy created Surface/Region when ClientConnection goes away. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=78677f933327fb3d4c0af519ced7dc51a7655370'>Commit.</a> </li>
<li>Make KWayland usable from qmake. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=14cefee19508db26348d5fd3f2dec3eede6c1bd3'>Commit.</a> </li>
<li>[server] Expose PlasmaWindowInterface::unmap. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=eb5f56904f30f59dbc83994d0271fbd721f6195b'>Commit.</a> </li>
<li>[client] Delete PlasmaWindow after an unmap. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=32e5912b99130b6058e8548ff2cf7b3e0c78a2e4'>Commit.</a> </li>
<li>[tests] Extend paneltest on how to use PlasmaWindow. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c4314300d631170e7877e9b9501466ecf7abd2f9'>Commit.</a> </li>
<li>[server] Add PlasmaWindowInterface to PlasmaWindowManagementInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=80ae111059ebc6699b9e2664c3827626cdea616b'>Commit.</a> </li>
<li>[client] Add PlasmaWindow to PlasmaWindowManagement interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=f1de1f8f703aa7985a2564aee4ca4ffac00b67d6'>Commit.</a> </li>
<li>[server] Add resource safety checks to TouchInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=1cd7e817032b03165b7598819b9c8f42baf6e7f2'>Commit.</a> </li>
<li>Fix cde95cf07176d0347cc863d2d4ae459abcd55148. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=44b697923ce01b907a601ba26c4a2acda80d1827'>Commit.</a> </li>
<li>[server] Add safety checks in SeatInterface::setFocusedFooSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cde95cf07176d0347cc863d2d4ae459abcd55148'>Commit.</a> </li>
<li>[tests] Extend paneltest to enable/disable showDesktop mode. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=94017bbf5bf823b2bf9aef888cc8a8f007789a2b'>Commit.</a> </li>
<li>[server] Add PlasmaWindowManagementInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=b0e76e9d3433032609d144cd7a98d96c708f33d8'>Commit.</a> </li>
<li>[client] Add PlasmaWindowManagement. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=761a0c3cebdb89d72fa52f5597bc955451cc79ca'>Commit.</a> </li>
<li>[protocols] Disable org_kde_plasma_window interface for the time being. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=466af869c5db8b6f05228850bda7711ba17b3f59'>Commit.</a> </li>
<li>[Protocols] Add an event for show_desktop changed to plasma-window-management. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=2da34cc7751698341feef55f035426d9cf6829b5'>Commit.</a> </li>
<li>[server] Add support for QtSurfaceExtension protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=69f9ac69bb11e89898f892e158b57bb2cfc81935'>Commit.</a> </li>
<li>[tests] Add a test application for PlasmaShellInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7684c4e9d0afd377400b20058e46150b5905e07f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124053'>#124053</a></li>
<li>[server] Add bindings for PlasmaShell interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cbd321c26938248e32065c62ed60ca6fe8140d4d'>Commit.</a> </li>
<li>[client] Add binding for PlasmaShell and PlasmaShellSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a80ef7a8f12656e63804f49d6ae1ef69dd9a7577'>Commit.</a> </li>
<li>[protocols] Reduce roles in plasma-shell to bare minimum plasma needs. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6327676fe40e69c9418ba5ab0dea91c10e1ff476'>Commit.</a> </li>
<li>[protocols] Reduce plasma-shell to the bare minimum what we need right now. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cc7d4029e71f8eedaa1be297f7719bc9c86fc58e'>Commit.</a> </li>
<li>[server] Drop incorrect assert from PointerInterface::setFocusedSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9f0ddf83843fd6ef4ffbd056031e1bf72a1e25f4'>Commit.</a> </li>
<li>[server] Don't discard callbacks on surface commit. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=be41f2aa7a945dd2b85cdb17b5707b4a9b788fa4'>Commit.</a> </li>
<li>[server] Query whether BufferInterface's format has an alpha channel. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6a44f3c40aa50ad9aeb1233a7ffc399cd62c2ee0'>Commit.</a> </li>
<li>[sever] Check whether it's a shmBuffer in BufferInterface::Private::format. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=01bb1db7458a4f02c73ad18dd9bdf811a1651f0f'>Commit.</a> </li>
<li>[server] Send keymap if we have one when creating a keyboard. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ffbfb32d35173ff4cfde79fe6e6817cc9a503767'>Commit.</a> </li>
<li>[server] Restrict damage to the surface geometry. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d78919d7312f3281bde2d0486a9d50bcb80ad96f'>Commit.</a> </li>
<li>[server] Only send surface leave if resources are still valid. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ae018903eaa361636e39bd6e552f711f6623f6e5'>Commit.</a> </li>
<li>[server] Make Display::dispatchEvents also work when event loop is started. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8a25edbf12e06fac947b377b7c7e1833c6d0438d'>Commit.</a> </li>
<li>[client] Create a ConnectionThread for QtWayland's display. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=70e334f9f946e965bc1df7be9c59f1a50c1755b0'>Commit.</a> </li>
<li>[client] Create a Surface from a QWindow. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=09f50c81685b937ba6c8089148521e52f3f4310d'>Commit.</a> </li>
<li>[client] Add Surface::id method. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=e81aa475c8d78917825d76304a269d82edc8f171'>Commit.</a> </li>
<li>[autotest] Drop custom flush code in testWaylandSeat. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ccaf55c02888b52afb35147a9b50262c9f1febd8'>Commit.</a> </li>
<li>[autotests] Improve stability of testWaylandSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=d636436c8e55aee2a433bfd6decddc3cf15a40f3'>Commit.</a> </li>
<li>[autotests] Improve stability of TestWaylandFullscreenShell. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8500e452b630b33686d72ba8f1236a7f6f83a9d6'>Commit.</a> </li>
<li>[autotest] Make TestWaylandOutput more robust. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6dbb529b238f58776a9d80d4fff5373e6962fe34'>Commit.</a> </li>
<li>[Server] Delete BufferInterface directly from destroyListenerCallback. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3999cb9a48ef9a0a3abe285cbaffe21dffe8fa90'>Commit.</a> </li>
<li>[server] Default init link.prev/next of the destroyListener in BufferInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=5b2ae3d624b4b09e577e72a440d1b8d596d558ce'>Commit.</a> </li>
<li>[server] Better split flushing events and dispatch events. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9c65ef7658769d8606aa87cd641dd807cd74dd9f'>Commit.</a> </li>
<li>[server] Call wl_resourc_destroy from destroyCallbacks. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6b2f11bd2815ba74e77a03f9015ef0197691bf44'>Commit.</a> </li>
<li>[server] Guard sending events to PointerInterface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c362a576ac98ed35d1734e71741ab729f561fb27'>Commit.</a> </li>
<li>[server] Nullptr check in Resource::Private::get. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=3e33d3307fa643e9d50bc07d2098c4f10c22f0f6'>Commit.</a> </li>
</ul>


<h3><a name='kwayland-integration' href='http://quickgit.kde.org/?p=kwayland-integration.git'>KWayland-integration</a></h3>
<ul id='ulkwayland-integration' style='display: block'><li>New in this release</li></ul>
<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Change default logging category to QtCriticalMsg. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=125cc5082fd6ba68376850d55ff3226321dca5ab'>Commit.</a> </li>
<li>Port left over qDebug to qCDebug. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=53004b95ffd54330323209a867de115115ecb6e5'>Commit.</a> </li>
<li>[scripting] Add dedicated logging category. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bffbbce17291c12dfa1b56eb35d6a0ab08578b3c'>Commit.</a> </li>
<li>[decorations] Introduce logging category for decorations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=09880d126799d4a7b9d88d48310f7461991412a5'>Commit.</a> </li>
<li>[tabbox] Add dedicated logging category for TabBox. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=910c49959a324ed61eb7d5fe41ec1c310cf48f25'>Commit.</a> </li>
<li>[libinput] Add dedicated logging category for libinput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe28ca1d8a74e44f42fce2c630cc81fd0394741f'>Commit.</a> </li>
<li>[libkwineffects] Introduce logging categories for libkwineffects. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=57c521c21487ad8dd08005ee7cc8367e82305d04'>Commit.</a> </li>
<li>[effects/cube] Drop warnings if not with shader support. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e476cbc0d26150d850983b758c1ef10596f05650'>Commit.</a> </li>
<li>Update kwintabbox kcm docbook to plasma 5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=720d10cee8bacc9b472414b785801f0f510ddb20'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124475'>#124475</a></li>
<li>Remove obsolete kwincompositing kcm docbook. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=25ca9df6479e94aad1834362d2f6b93a4dd4f470'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124436'>#124436</a></li>
<li>Move Q_PROPERTY wantsInput from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a0adf3507aa5e91d1f87371261d824747966b164'>Commit.</a> </li>
<li>Fix naming. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a5185f924dd227e900ae60ca537bb939bd6caaab'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=999b01ec274954b06a79503e02d34da94ab289a0'>Commit.</a> </li>
<li>Fix typos. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bff41058c598655bb54acecbdf4e17499c493cfa'>Commit.</a> </li>
<li>Review Request 124352: Update kwinsceenedges docbook to Plasma 5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4730812df89312f123f8506c480418ab883bec2c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124352'>#124352</a></li>
<li>Update kwindecoration kcm docbook to Plama 5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7aebca746130a77b1594eb1d8f9e299936b6ba36'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124351'>#124351</a></li>
<li>Update windowbehaviour kcm docbook to Plasma 5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9aa436f4ddecaea972d518e05947db75a8dd8cc3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124348'>#124348</a></li>
<li>Clean debug output. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6f345f4915e3bc55cc4da7e62507bccaffd7542e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124320'>#124320</a></li>
<li>[wayland] Improve pointer movements at screen edges. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e56520dc812e922e2bd2d1710dc971cd0086cb69'>Commit.</a> </li>
<li>[wayland] Don't break if we try to vt-switch to vt we're on. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e98d5d6a15c6d4b7345bba1f528a3620c8f7dcc2'>Commit.</a> </li>
<li>[wayland] Pass appId to PlasmaWindow interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=01d805f193047736a7e2e02f23d5edc39afb2637'>Commit.</a> </li>
<li>Fix build without wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=105cda7769311de1d0ae51de9378e6b6c591637c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124390'>#124390</a></li>
<li>[wayland] Not visible windows should not trigger a repaint. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bc45c61eeb26bd0a6f0357989f2f81ca2a08fdf1'>Commit.</a> </li>
<li>[backends/drm] Drm backend support syncsToVBlank. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=339d9de3518f146a0b0ad6a77f30c2cff70624e1'>Commit.</a> </li>
<li>[wayland] Determine whether libinput is needed from plugin metadata. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8797f826f94d5433ec5236dcf80a82162ba347e2'>Commit.</a> </li>
<li>[wayland] Improve mapping of windowClass to desktop file. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a3db04f66eae75474512ce61d4dd768ea9a5cf58'>Commit.</a> </li>
<li>Install a categories file for kdebugsettings. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f7d317601e68663728d882f152b7c9bc4007d34c'>Commit.</a> </li>
<li>[wayland] Shadow gains support for a Wayland protocol. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df1146bfa63de6bebd044161fffbff20e31ba677'>Commit.</a> </li>
<li>[wayland] Announce modifier changes to clients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3beff66acad7b37082f678f1c30b3e9a88692970'>Commit.</a> </li>
<li>[wayland] Enforce basic render loop. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3fe270b804d25d276d1021092ec53c1d79069873'>Commit.</a> </li>
<li>[wayland] Only activate ShellClient in takeFocus if it wantsInput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f304a49a58ef29ae0918dd9ac1a61785cc7ed5b2'>Commit.</a> </li>
<li>[tabbox] Fix loading of default layout. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=63bdc54b329bb38e0624b6126894ff0ae248a559'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350029'>#350029</a></li>
<li>[wayland] Perform mouse command on AbstractClients on touch down. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a687a58e82a73955212f1fbc01498c3c9b341e98'>Commit.</a> </li>
<li>[wayland] Activate AbstractClient from InputRedirection on mouse button press. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0c0ad87caf3967d3f980ee229e02c02a015287d4'>Commit.</a> </li>
<li>Move logic to handle mouse buttons on windows to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c3b7ed907bc7076bedb193f98be74440ee574628'>Commit.</a> </li>
<li>Provide a base implementation for performMouseCommand in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8d407157c4f2aac8b0d570cbf379f429beb8c0df'>Commit.</a> </li>
<li>Workspace::gotFocusIn and ::shouldGetFocusIn changed to operate on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9a8f94f8fcc19a7849754875c291cfe1e3ddecc4'>Commit.</a> </li>
<li>[wayland] Add support for FakeInput interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=810ed5d396104fe8d213ab9cdf6b586f41b8530b'>Commit.</a> </li>
<li>[wayland] Announce support for idle interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=282321bfb5d71ebc4a3634d32fdd601a2dd0b7b2'>Commit.</a> </li>
<li>[wayland] Improve creation of KWayland::Server::PlasmaWindowInterface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=72635101f02742c05d03b9af26d6fe03d41a0b89'>Commit.</a> </li>
<li>Port away from KToolInvocation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f6458fa1e8e92fdf16a1acc961703d229894454c'>Commit.</a> </li>
<li>[wayland] Focus to null X11 window when activating a Wayland client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=da4305c2fa308cb7e41184cef5b63990e7289588'>Commit.</a> </li>
<li>EffectsHandlerImpl::activateWindow operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=377988eae9f8360dc36ace7de6f93074be7b67d6'>Commit.</a> </li>
<li>Change supported platforms of kglobalaccel plugin to "org.kde.kwin". <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=316914a38b315ef479aa413b41514ddcd9738278'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349911'>#349911</a></li>
<li>Disable Activities support on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a1a89d3d1efcf863c33a827a5a5d917f839dbb02'>Commit.</a> See bug <a href='https://bugs.kde.org/349992'>#349992</a></li>
<li>Drop KF5::Service dependency where it's no longer needed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c31bb6d46ff88027560cc48db8071676ce174abb'>Commit.</a> </li>
<li>[aurorae] Port loading of QML decos to KPackage. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=06469f66a831342caf7504469339f872287ef324'>Commit.</a> </li>
<li>PluginEffectLoader doesn't use KPluginTrader any more. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=93ef1843565142531b2f89eb8319e1458ccd8e24'>Commit.</a> </li>
<li>[scripting] Find scripts through KPackage. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b01b03aa84dc62e48b04657bb90476228c0fd8f0'>Commit.</a> </li>
<li>[tabbox] Locate layouts through KPackage instead of KService. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5b111cc46712e1894e1eae51ed2e359d39e4040f'>Commit.</a> </li>
<li>Switch decoration loading to KPluginLoader::findPluginsById. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2cd6efa409edbd8377773d9160562a85cbe8cbf1'>Commit.</a> </li>
<li>Port scripted effect loading from KService to KPackage. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ca14073b543105e4ca7e0a5811578b18d966befe'>Commit.</a> </li>
<li>[wayland] kwin_wayland is no longer a kdeinit_executable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a4ca6f196eabdededdb251f3d31068d8e7591066'>Commit.</a> </li>
<li>[wayland] Add a plugin for kglobalaccel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3041a7c32d6b95a032272e9ff1c791506bdc9114'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124187'>#124187</a></li>
<li>[wayland] Create a KGlobalAccelD during startup. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8461344ce1d3c777fa2064560650f3fea8621bc6'>Commit.</a> </li>
<li>Fix screenedgeshow manual test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=37c7e4c2566968236d7b5ed43a635d71e5eb0d38'>Commit.</a> </li>
<li>Check whether it's platform xcb before calling QX11Info::display(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=924b66c4d5e30c7141890bdb64e30c47761eb5eb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124203'>#124203</a></li>
<li>[backends/drm] Fix crash in cleanup of DrmOutput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4c03115882db450a9d1d89e335e3cc562f9aa168'>Commit.</a> </li>
<li>[wayland] Introduce automatic backend selection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=94827c7405a09c3de950c73cdab63ee7e45118df'>Commit.</a> </li>
<li>[wayland] Improve passing env variables to applications started by KWin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fa7b2fd055e61cab1209ad238440befcd0121c70'>Commit.</a> </li>
<li>[wayland] Create PlasmaWindow as child of AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=085c77810b4ef47c4720f39480dc849c49d7154e'>Commit.</a> </li>
<li>Return unless glDebugMessageCallback resolves on GLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c22fdcf146ff1c59209ba523822ea29df1548116'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124135'>#124135</a></li>
<li>[hwcomposer] Get refresh rate. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=67734050e65cb487fe8a12b6afb7ccdb39628c9c'>Commit.</a> </li>
<li>[hwcomposer] Switch to format HAL_PIXEL_FORMAT_RGB_888. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=53dcd98b8b4bbcdcea22163ca807dac045b03dab'>Commit.</a> </li>
<li>[wayland] Trigger update of client area when the panel behavior changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=48bf38e8302aaf9a815390951ba65b2649067d73'>Commit.</a> </li>
<li>[effects] Support blitScreenshot on OpenGLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1f6c0c07f715d432f457a19a85b5d8817e89aaa0'>Commit.</a> </li>
<li>[libkwineffects] OpenGL ES 3.0 supports glBlitFramebuffer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=59a6c9ddf9f07060b63a7141e2772e9a8ec0e78d'>Commit.</a> </li>
<li>[hwcomposer] Announce support for pointer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b8ae823b50a84e8c2355ed8dfb75cf18bd60a056'>Commit.</a> </li>
<li>[wayland] Break showing desktop from ShellClient::takeFocus. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7e1e044948ba70bd6521dbc5b782d8bbbe67cd68'>Commit.</a> </li>
<li>[wayland] Export themed icon name to PlasmaWindow interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8b6fef457b4fc8895c485106d4c6c134252936e5'>Commit.</a> </li>
<li>[wayland] Provide icon from ShellSurface's windowClass. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=31e599a63dc1f48d895a1d08abf6c1893ffdbe0f'>Commit.</a> </li>
<li>[wayland] Add support for PanelBehavior in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2192bd69e0294e1638514bfb0905ea3d9dafed7b'>Commit.</a> </li>
<li>Move hasStrut from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1ccd6081214d01ca5d26f82159d47cde0486c945'>Commit.</a> </li>
<li>Fix unused variable warning. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=69d78fe8dbd1c3f633cbae02381a4093612b3a05'>Commit.</a> </li>
<li>Fix sendToScreen/checkWorkspaceGeometry. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=39c35f8f48fcb47e823af1b7147f91d2d445b306'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/330968'>#330968</a>. Code review <a href='https://git.reviewboard.kde.org/r/122517'>#122517</a>. Fixes bug <a href='https://bugs.kde.org/330968'>#330968</a>. Code review <a href='https://git.reviewboard.kde.org/r/116029'>#116029</a></li>
<li>Don't offer to set window shortcut if rule forced. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d053c315718618d4c60521b2f69ef911db140bc3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332184'>#332184</a>. Code review <a href='https://git.reviewboard.kde.org/r/124125'>#124125</a></li>
<li>[wayland] Connect state change requests from PlasmaWindowInterface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ccd8f50b63de4e9be6ab2a43de85c39c42f65097'>Commit.</a> </li>
<li>[wayland] Add support for closeWindow request in PlasmaWindowInterface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3c2c1d31c93ab1ff0f26e2cd9f78930c88aafd8e'>Commit.</a> </li>
<li>[wayland] Add support for input shape. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=97588faea2d79ed1208824cee3a25ca9ef41eeb8'>Commit.</a> </li>
<li>CheckOffscreenPosition on restored session geoms. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7b842ccc8151d8dff1557877b3ce9304997902c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/336193'>#336193</a>. Code review <a href='https://git.reviewboard.kde.org/r/124122'>#124122</a></li>
<li>Make checkOffscreenPosition multiscreen aware. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=acb8016e65bee17bd26be47595b73d99013e681b'>Commit.</a> See bug <a href='https://bugs.kde.org/336193'>#336193</a>. Code review <a href='https://git.reviewboard.kde.org/r/124122'>#124122</a></li>
<li>[wayland] Export more states to PlasmaWindowInterface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=171bbd662ab3b294893689fef40f2d5383fa6d38'>Commit.</a> </li>
<li>[wayland] Unmap PlasmaWindowInterface instead of destroying it. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f5724b9a559fae2ce7a085dad9acdaa37a85addd'>Commit.</a> </li>
<li>[wayland] Only create PlasmaWindowInterface if AbstractClient has a surface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=183479fe67876911bf2703d591781729ab00934e'>Commit.</a> </li>
<li>Port session management to KF5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3442664609284ea201d50f6da27222c706a75c8e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123580'>#123580</a>. Fixes bug <a href='https://bugs.kde.org/341930'>#341930</a></li>
<li>[hwcomposer] Only update the attached screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=268eb700165c498596e3199b3d4f7b3573f0ed22'>Commit.</a> </li>
<li>[wayland] Announce AbstractClient to PlasmaWindowManagement interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b00af25dc57a181703c86507538b2f35c823754'>Commit.</a> </li>
<li>[wayland] Drop EGL_PLATFORM env variable from QProcessEnvironment passed to launched processes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6e58d50efa758ae2a6d3f349f2eb82351280448a'>Commit.</a> </li>
<li>This line wasn't intended to go in. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d457a8d92b2a0b95fef46ba541fe4a29ee5a871b'>Commit.</a> </li>
<li>[wayland] Add support for PlasmaWindowManagement interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=859a3bb5986091c4c30ca2040f09d15567032e1b'>Commit.</a> </li>
<li>[wayland] Change keyboard focus window when active window changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=01ac0abfd7c4cc7491fd5fdf5e9b71bb9e3a7f9d'>Commit.</a> </li>
<li>[wayland] Add a command-line option to start an input method server. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=98bcdbe70a9106bdcbe2bdf93746c4375b2b3346'>Commit.</a> </li>
<li>A shell client's layer is OSD if it's OSD. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d78430acce48835fdabf517c1a506fd048990aa'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=63e877a0e315e9f74c6d9095ba86f645316b3fa0'>Commit.</a> </li>
<li>Warnings--: unused parameter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e8f7ab14b5e6fae78b16df6ecb7fc31008d22fd4'>Commit.</a> </li>
<li>Warnings--: helper needs to be created, but appears to be unused. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e946e5e0dbd34c4e5079dde27a235a8c7cb92103'>Commit.</a> </li>
<li>Warnings--: unused parameter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=955b0bb8e81130d98392ddfc6e93715c130ffb97'>Commit.</a> </li>
<li>Warnings--: remove set-but-unused variable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9c2b53dd047d607793d090d38df137c0829b5a83'>Commit.</a> </li>
<li>Remove icon size calculation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1a18eb97ad0a68d018c3cb62707f48d693a6ab83'>Commit.</a> </li>
<li>Warnings--: unused parameters. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6ddf294b55d6ef53c785c55168198875d3615a99'>Commit.</a> </li>
<li>[wayland] Unreference the buffer when destroying the WindowPixmap. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=02cebe93a67bb6781e4642344a9a566ab94a1326'>Commit.</a> </li>
<li>Don't close desktop windows and docks. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9744625253cb56070ae8dff46d82e5a6ef5e1fb6'>Commit.</a> </li>
<li>[wayland] Let's try to activate a ShellClient after it's created. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=65665b052b4bc28ba5c76b92b1ac15a73bc77ea9'>Commit.</a> </li>
<li>[wayland] Reset ShellClient::m_unmapped before calling setReadyForPainting. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ad80e31e6af821988a3780a2dd1b42290b598426'>Commit.</a> </li>
<li>[wayland] While a ShellClient is not shown, it won't take input. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=93c88aa4467cbf6287f18935348c3c9930dd6d7d'>Commit.</a> </li>
<li>Add a signal Toplevel::windowHidden. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=11e0e75fb37104b4775c3a462796cc2422aa2807'>Commit.</a> </li>
<li>Fix compilation on older gcc (at least 4.8.3). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ff2590ac52d246bab051623a2b5a49e6b72e663f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123936'>#123936</a></li>
<li>Evenly distribute windows into all corners. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc9a95df5d5fc73becbaeee69bf0d59257cc0749'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348709'>#348709</a></li>
<li>Update geom_restore when silently breaking QT mode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e830f08defc3c38ec79542faeacbc5aea2cb2327'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348640'>#348640</a>. Code review <a href='https://git.reviewboard.kde.org/r/123882'>#123882</a></li>
<li>Update geom_restore on unshading. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=42f53eccbdb380144e291a003213375f3922cee1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123882'>#123882</a>. Fixes bug <a href='https://bugs.kde.org/348064'>#348064</a></li>
<li>Improve geometry handling on bordersChanged. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1eae9f4baf63f83f76235cf39ba242057500ec8d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123882'>#123882</a>. Fixes bug <a href='https://bugs.kde.org/348064'>#348064</a></li>
<li>CheckWorkspacePosition: Don't adjust to shaded sz. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b9cfffe1ae6d62366098f1f1ca19ce7eb224bd1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123882'>#123882</a>. Fixes bug <a href='https://bugs.kde.org/348064'>#348064</a></li>
<li>Don't nuke both dims if undecorated frame isEmpty. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7f71c891118aa3d22820bd998399bd046fb65126'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123882'>#123882</a></li>
<li>Robust modal activation; ensure showing activated. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=41be18e317155c282fa6778620903b906ada69c5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123875'>#123875</a>. Fixes bug <a href='https://bugs.kde.org/348083'>#348083</a></li>
<li>[wayland] Add support for QtSurfaceExtension for closing ShellClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f696b578ccf5761cacfcb7b8c76b702aa5fa1f03'>Commit.</a> </li>
<li>Workspace::setShowingDesktop operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6bdf120b3f901233b8d690e1899aaae02da06193'>Commit.</a> </li>
<li>[wayland] Add support for the PlasmaShell interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=124bd8aaed19aa1347ed236424ea3a7fd8920573'>Commit.</a> </li>
<li>[wayland] Check for docks in ShellClient in Workspace::updateClientArea. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2bdabe62aa8340f56d6aaceae773256d40555b87'>Commit.</a> </li>
<li>[wayland] Support windowType for internal windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=280075b7cdb3a9a43ea090947d3da78cf16e1528'>Commit.</a> </li>
<li>[wayland] Internal ShellClient is on all desktops. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e46ad2091b7b467a08067af8bdc63258678abc78'>Commit.</a> </li>
<li>[tests] WaylandClientTest can maximize/restore on middle click. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=199cd70416bb91a9c4298c0a99c47b79b58c95c4'>Commit.</a> </li>
<li>[wayland] Implement maximizing of ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7cded491457fd74e0c7ec734cf0e8de8d1cdfa14'>Commit.</a> </li>
<li>Move clientMaximizedStateChanged signal from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=034276af36e5e1b785d57f6af9941b1de2aa4429'>Commit.</a> </li>
<li>[wayland] Trigger layerRepaint on old geometry when changing geometry. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=239104b4f48c900173cb0687c4a0a72000ed552e'>Commit.</a> </li>
<li>[scripting] Add support for ShellClients in ScriptingClientModel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b196ad7052a1d3e686f1aa9043af3d02c197eb80'>Commit.</a> </li>
<li>Move skipTaskbar from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d9769af1fe19fef2eff4233627e2504a1c11741'>Commit.</a> </li>
<li>Move skipPager from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=11a3eac070ed3198d7d24e62814a3363dc2542f8'>Commit.</a> </li>
<li>[scripting] ScriptingClientModel operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c4c637db924e117a86415791cadb666121c21241'>Commit.</a> </li>
<li>[wayland] Ensure internal clients can trigger compositor repaints. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=86e0fc4dded4dac41efdad653d7bb631ad5d1acf'>Commit.</a> </li>
<li>[wayland] Handle pointer events for internal windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7f0758b85fceca8e20d282253047bb9783c3a793'>Commit.</a> </li>
<li>Trigger updateMoveResize after keyboard move. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a64426085551f14f0bdce69a320e5afc137b4c3'>Commit.</a> </li>
<li>[wayland] Support pointer warping in the backends. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=06fc00b4a9ad79f3ae2fc6c3ed23ff6e8afe25a4'>Commit.</a> </li>
<li>[wayland] Add support for pointer warping in InputRedirectionCursor. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=74c111ef886157cede02432dc7be3c63a11da72d'>Commit.</a> </li>
<li>[wayland] Support move/resize mode in pointer move handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4ca3734d12c97d4d0a878804a403710607b9d04a'>Commit.</a> </li>
<li>Send HoverMove event to selected window decoration if buttons are pressed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=545c8b190e7acde26ae6705d92bf237b4c369694'>Commit.</a> </li>
<li>[wayland] Improve getting cursor image from theme. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=735ffc8ab20e457f777380ccaf1f42f6391e42e7'>Commit.</a> </li>
<li>[wayland] Update cursor image from current decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4a1288b0c96035cb56b439b5c9a7b2c3b1e47e0a'>Commit.</a> </li>
<li>[wayland/x11] Implement installCursorImage in X11WindowedBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=59ca83d67ab1f76d0082920710022dacac250135'>Commit.</a> </li>
<li>Handle interaction with window decoration directly in InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1edd6892931745f15e5c8531f2dae8b13e3131d1'>Commit.</a> </li>
<li>Don't update focused pointer window on pointer movement while button is pressed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8b3be23032385777a9981c9d408c3c9c822c267b'>Commit.</a> </li>
<li>Clenup: Drop QPoint argument from Client::mousePosition. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eac99d5df67d44156050c7ec06269829c92573b6'>Commit.</a> </li>
<li>Require XCB 1.10. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6c0c513874a8dd8070bdffed0d5d3dc660eced9e'>Commit.</a> </li>
<li>XCB::CURSOR becomes a required dependency. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8a9bbf7ca389d57df002e21af9ae0c4c432aa9eb'>Commit.</a> </li>
<li>WORKAROUND for nvidia VBO failures. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9f13e9b26035984906896a56d64e85e11522c581'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123936'>#123936</a>. See bug <a href='https://bugs.kde.org/344326'>#344326</a></li>
<li>[wayland] Handle unmap in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=35abacabc41dabb4d5680c18c5c59c8ca9179cc5'>Commit.</a> </li>
<li>Fixing activities list in the rulewidget. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e44bffa0970d399358b043f90d3c0bc272700f05'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123869'>#123869</a>. Fixes bug <a href='https://bugs.kde.org/347732'>#347732</a></li>
<li>Set Toplevel::m_surface to nullptr if SurfaceInterface gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1e16ab2567bd9b0d20b12a7bc3af9126f9b03a30'>Commit.</a> </li>
<li>[wayland] Set depth in ShellClient depending on whether the Buffer has alpha. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5a98d8bbbdedb7e5db10a37dfbd556b4a4fc4628'>Commit.</a> </li>
<li>Enable wayland clients to go fullscreen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9791978d535e3592ee00df9c16e1e0b05ce4869d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123853'>#123853</a></li>
<li>[wayland] Drop Qt version check from WaylandServer::fakeDummyQtWindowInput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1420bb38a98ab118164882bfe91d8c9be28b3ecd'>Commit.</a> </li>
<li>[wayland] Pass xkb keymap to Wayland server. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3b4c508ee36ac74c37e77fcaa14d106397ad2994'>Commit.</a> </li>
<li>[wayland] Ensure size is updated before marking window as ready for painting. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=988ce28943dbdbda2d418ee777a0c2259cb50343'>Commit.</a> </li>
<li>[wayland] Place ShellClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=90a6814513f79952df1addac0552fdfc338a6de9'>Commit.</a> </li>
<li>Placement fully operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1a89fc55b529c2cfb6ec9976cda9218122a38620'>Commit.</a> </li>
<li>Move maxSize and minSize to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f16644ac2e25191882ee363c4680b464c7efd4ad'>Commit.</a> </li>
<li>Placement::placeUnderMouse operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ffd6f9ceba831e31d01a1b7a6d1b8ce49d606941'>Commit.</a> </li>
<li>Move keepInArea to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=737ad0b6649c8e08d089568e16a7d0c661bb5a5d'>Commit.</a> </li>
<li>Add resizeWithChecks to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=007e1253c6b37c2cd1a07513aebbdc368183d12e'>Commit.</a> </li>
<li>Placement::placeOnScreenDisplay operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=670787086b22bbeb91f4757edc002eb48a5e42be'>Commit.</a> </li>
<li>Placement::placeAtRandom operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aaca12290205168dd557ac65ac9f8751848c010a'>Commit.</a> </li>
<li>Placement::placeSmart operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d077c42b67d26edd13280e430ac1c850f78c6ab'>Commit.</a> </li>
<li>Placement::placeCentered operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a261b1c2530f9da971b66dde713bbcee26bd13e8'>Commit.</a> </li>
<li>Placement::placeZeroCornered operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b971749f1d39e0d605aded5e8bfb898c3efc0f88'>Commit.</a> </li>
<li>Placement::checkArea operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=78700e23fd6f850bef20ca1313e41afd3284e7f0'>Commit.</a> </li>
<li>Move Client::move to AbstractClient and add implementation in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=da2731be511b380033997b71b9b26172f85ab817'>Commit.</a> </li>
<li>Ensure Compositor can tear down without a Workspace. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=48272a0de83fba3bddb1b38a91aee2fb926804d6'>Commit.</a> </li>
<li>[wayland] Ensure Compositor is destroyed early enough. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=604b6d05f533c0cb08c0181559936e2ea5a9a302'>Commit.</a> </li>
<li>[wayland] Ensure KWin::atoms are destroyed before doing xcb_disconnect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c798977e558bd142670b624d4341228f783f6828'>Commit.</a> </li>
<li>[wayland] Ensure our own windows don't have flag Qt::X11BypassWindowManagerHint. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f81655b7f5d9bf00571f6e6d9d96c85b4f28fa4d'>Commit.</a> </li>
<li>Move isFullScreenable to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df1499784f4e28303762a39b922461d4f70e643e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123871'>#123871</a></li>
<li>[wayland] Properly handle case that EGL_WAYLAND_Y_INVERTED_WL is not supported. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9f1f3d85eb151d57bef213e59c6d166b847317e'>Commit.</a> </li>
<li>[wayland] Use QProcess to start XWayland server. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=64e01ac2ca63de04fcf35d0f7beb8e95066688af'>Commit.</a> </li>
<li>[wayland] Call pixmapDiscarded when ShellClient's size changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9954f1167f39d991b70f2c49d76d60421be39fe7'>Commit.</a> </li>
<li>Reset not-valid WindowPixmap in pixmapDiscarded. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=20286fa671ed73a18fb427fefa3558b590f72cce'>Commit.</a> </li>
<li>[wayland] Delay WaylandServer::shellClientAdded till the Surface is ready for painting. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=783afd17b759230ffa388ce7f7aacff500829226'>Commit.</a> </li>
<li>[wayland] ThumbnailItem supports ShellClients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aff93b1216284a187b8f8014f4b822280584fa0d'>Commit.</a> </li>
<li>Make Toplevel::window() virtual and implement in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=45c1eddf4a6e8f547b6c41dab309af3c2f2708be'>Commit.</a> </li>
<li>[wayland] Internal ShellClients don't take input. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eec356494a6e5133e1ce6130728ba50805eb171d'>Commit.</a> </li>
<li>[wayland] Track the internal ShellClients in WaylandServer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=36fa88893ecba0d6f17e5a95c563f27b3e39fdcd'>Commit.</a> </li>
<li>[wayland] Add a bool ShellClient::isInternal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=23b19b4efea03624aa1cccdaa429686fac298bb6'>Commit.</a> </li>
<li>[wayland] Create a windowId for ShellSurface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4ed4d4dab4bbbc7d441b6b83dce8240a2b7c7fcb'>Commit.</a> </li>
<li>ThumbnailItem operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c63605da06b23fe9c779f45ef4f2a565c6ce2d00'>Commit.</a> </li>
<li>[wayland] Implement caption in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e97576aab7c70e3f7d16e536a1fb53ccdee69e1f'>Commit.</a> </li>
<li>Move caption property and captionChanged signal to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=846fd58d143ce9275c0d1f4e32d6fed618095fd6'>Commit.</a> </li>
<li>[wayland] Implement notify in ApplicationWayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f885ce3796559fcb67eff61f77af1392bd0ac354'>Commit.</a> </li>
<li>[wayland] Fix typo in 2ff903e. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f2bcccedf497e960d3935310bc40faa5cbcbb1bb'>Commit.</a> </li>
<li>[wayland] Sync ShellClient's geometry with internal QWindow's geometry. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2ff903e8b34b6f27fc3696a9ab969f91ce19e57e'>Commit.</a> </li>
<li>[drm] Fix KConfigGroup resolution for edid overwrite. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6581bc62a3d0bd89d4ca3cc84a19652bb018b24d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123848'>#123848</a></li>
<li>Use screen name as additional information in UseractionsMenu. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9d580a628feb8bc2e2a7104ffb81bc799bdb2e07'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123849'>#123849</a></li>
<li>[drm] Implement Screens::refreshRate. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f30eb5075615303656c7778f3d6c2887062c19f7'>Commit.</a> </li>
<li>[drm] Implement Screens::name. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f5d123d61e3569f1945e5a6790550924ada55ce6'>Commit.</a> </li>
<li>Add more information about Screens to supportInformation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3129f7d698856fee664e935b704d2d1137bf12ac'>Commit.</a> </li>
<li>Silence unused variable warnings in Screens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e904e4fd325291fe50f869cb344754975aa974e7'>Commit.</a> </li>
<li>[wayland] ShellClient can reference an internal QWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7d152991c7e21eb054b2bb020353c66b0d297e12'>Commit.</a> </li>
<li>[wayland] Reuse position when size of a ShellClient changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=31cc2628ccb235e4aadca1a7adbec920c6b26a61'>Commit.</a> </li>
<li>[wayland] Flush QtWaylands wl_display and KWin's wayland server when processing events. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=74ae2f503ef20b9ef387b7e0a7697cd35c7196dc'>Commit.</a> </li>
<li>[wayland] Use a dummy window for QtWayland's popup window handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=29c2ae57e011bb38f967182fa52516fc245494bb'>Commit.</a> </li>
<li>[wayland] Ensure QWindowSystemInterface::sendWindowSystemEvents gets called in EventDispatcher. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3f94a2afc75c04c7352deeeda7f44328415281e2'>Commit.</a> </li>
<li>[wayland] Don't handle ShellSurface created signals till Workspace is ready. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4a671fbf98f3796e69bc9d3a3280893555c3f850'>Commit.</a> </li>
<li>[wayland] Disable Qt's client side decoration for KWin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9649885ed35a527e208dd323ad4e2d7c08c0adf'>Commit.</a> </li>
<li>Don't pass pointer focus to a Toplevel which is not yet ready for painting. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0853445dceba3e5fabccb47a2e48754aee1cc5b3'>Commit.</a> </li>
<li>Drop Client::m_frameWrapper workaround for reparenting deco. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0dda7b3f9463d3e0fc098677b2e0c1aceda80b71'>Commit.</a> </li>
<li>Delay desktopPresenceChanged in EffectsHandlerImpl instead of Workspace. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=30e6ae34d7ed24500d68e2ce00f28b3b2966f2af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347490'>#347490</a>. Code review <a href='https://git.reviewboard.kde.org/r/123729'>#123729</a></li>
<li>Use mode based freq calc on all drivers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0529d0b787776ea41ab0abbd2675272aa95bc293'>Commit.</a> </li>
<li>Fix building w/o EGL better. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f79cdc6b33888aa2cfbdd69999e54435d461f296'>Commit.</a> </li>
<li>Lazy setting of xbc properties on qApp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8855f47f34df6226d302a08a248e553ec9e97cc2'>Commit.</a> See bug <a href='https://bugs.kde.org/346748'>#346748</a>. Code review <a href='https://git.reviewboard.kde.org/r/123777'>#123777</a></li>
<li>Stall wobbling while screen is transformed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ba16371e781013cebbc4e9c33d9b00878c051b9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123733'>#123733</a>. Fixes bug <a href='https://bugs.kde.org/338972'>#338972</a></li>
<li>Ignore elevation list while screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=538ff0c8aa0de93fba67af94789477c1325e4e24'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347466'>#347466</a></li>
<li>Prefer query Screens::refreshRate(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3158e987fa241f95a984be1b6a0dba344d2c0c95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347360'>#347360</a>. Code review <a href='https://git.reviewboard.kde.org/r/123693'>#123693</a></li>
<li>Add Screens::name(int screen); STUB but for XRandr. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3597959c0ef597725750dd2af49ce506ab34b8a5'>Commit.</a> </li>
<li>Add OutputInfo class to obtain output names. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2d6df416b69071877402c234ca370a1a5991c342'>Commit.</a> </li>
<li>Add refreshRate to Screens. STUB but for XRandr!. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c7f13c7266c95aa0ff62e3a49b9c1c694e45ccc4'>Commit.</a> </li>
<li>Forward resource modes next to crtcs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0a3155972b0aef5f3e03dd777a7fa155158f2e41'>Commit.</a> </li>
<li>Desk grid: do not recreate DesktopButtonsViews. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c56d64b83743fe062ed9b406e238c44496f4b09a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347339'>#347339</a>. Code review <a href='https://git.reviewboard.kde.org/r/123668'>#123668</a></li>
<li>Make switchWindow FROM stickies act on current VD. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b3bd9ea14484fddc7257d09b7754850576d3cb7a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/331512'>#331512</a>. Code review <a href='https://git.reviewboard.kde.org/r/123640'>#123640</a></li>
<li>UpdateXTime before sending a takeFocus message. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=77047aa555c8ed58e8fc21c3f573b42d0c05a59b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347153'>#347153</a>. Code review <a href='https://git.reviewboard.kde.org/r/123639'>#123639</a></li>
<li>Window aperture always needs to cancel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0da58680fc805175d041104f47a800dde85586fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347174'>#347174</a>. Code review <a href='https://git.reviewboard.kde.org/r/123636'>#123636</a></li>
<li># This is a combination of 2 commits. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=050852846108188ef973e81da2a3d03fa3003cbd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123599'>#123599</a></li>
<li>Break desktopshowing state from cover & flipswitch. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a6ee1c7bb00c70a61f314ae98ad08b75969851d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123550'>#123550</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a></li>
<li>Prefer singleton transient leader on activateNext. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cfa1d612fea363ef1b5e73c9c49f6499f4320083'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123691'>#123691</a>. See bug <a href='https://bugs.kde.org/347437'>#347437</a></li>
<li>Multitouch on hwcomposer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5115d549264c39babec070a3a6eddaae3dd81206'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123796'>#123796</a></li>
<li>Add missing include. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=42768d515a4eb54d68c484599e28568fccb89037'>Commit.</a> </li>
<li>[scripting] Register KWin::AbstractClient in declarative scripts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=51bb4cf2a119425690fdad8ad125367a04a3008a'>Commit.</a> </li>
<li>Disable GBM if we build without EGL support. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a0c45a68b8165c586f161b55167481e1a87be0c8'>Commit.</a> </li>
<li>[libkwineffects] GL_NONE instead of None in GLTexture. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4bf479aca2389aed67e6d95a5e05e5d5ec404d2b'>Commit.</a> </li>
<li>[backends/hwcomposer] Forward input events to main thread. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d6700ff9f988f96c8546ea89e4a93f30c007f091'>Commit.</a> </li>
<li>[backends/hwcompser] Toggle blank screen on power button press. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d5d304c2f6f1d535b4e634bfc313f056255bd8db'>Commit.</a> </li>
<li>[backends/hwcomposer] Perform mapping of key codes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=533a39bc3eb60ec4416408e995cc2a33b829b15e'>Commit.</a> </li>
<li>[wayland] Support shm buffers on GLES. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=79c94d42f32e55f5119536b4a42dbe0a717cd2b8'>Commit.</a> </li>
<li>[backends/hwcomper] Initial support for input handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6a66d84f5ca72293614f47f41ebd3e17e26a2fc4'>Commit.</a> </li>
<li>[backends/hwcomposer] Handle OutputInterfaces in Backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1f62c0acb0f6ab2adf60cb4aa048b6ee24c738c2'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=af134dbbda350496fb881dda9e2f10b2ccbba77f'>Commit.</a> </li>
<li>Call Workspace::clientHidden when ShellClient gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e9e055dcca60c014ae4ede329670f27941ae4dfe'>Commit.</a> </li>
<li>Workspace::activateNextClient and clientHidden operate on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=597a9da77fc490ecc49971cde903edfec968c5ef'>Commit.</a> </li>
<li>Change Workspace::clientRemoved to carry an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f55c08600733280179a87f0ff3781da9e3984952'>Commit.</a> </li>
<li>[wayland] Don't set surface on Toplevel for non Xwayland-Clients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8d3ee65269863ec64a74a6226f7f9ce709adaa97'>Commit.</a> </li>
<li>[wayland] Create a DataDeviceManager in wayland server. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=28d314a4877321b6df2069227e3dc2298d428603'>Commit.</a> </li>
<li>[autotests] Adjust to changes regarding AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6826b9eb946c75b5c56122bee96f97199efc8f6c'>Commit.</a> </li>
<li>ShellClient inherits AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=912ab71abdc292036e331c69946097d0e24b2902'>Commit.</a> </li>
<li>Toplevel::setOpacity becomes virtual. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b6f793090e6329f9001264c2b16fea5719ac1e09'>Commit.</a> </li>
<li>Move implementation of ::palette to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=23862e512d1f208dfaff498110db69e1886d41d7'>Commit.</a> </li>
<li>Move minimize implementation to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=38b418887aea2f84360d3d1d6004ab77e59e8f5a'>Commit.</a> </li>
<li>Move ::titlebarPosition to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d2884484bc0a9a679eb0e3ba06b14167b655999e'>Commit.</a> </li>
<li>Partly move shade implementation to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a4d16debfc22f4e877119fe62171de69bdfd6f16'>Commit.</a> </li>
<li>Implement virtual desktop handling in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=254887155c820284b7d9dc68f171a2c7c0ec45c6'>Commit.</a> </li>
<li>Signal desktopPresenceChanged carries AbstractClient as argument. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1bfba1765cbaf27e451e921e7e55f39931df0137'>Commit.</a> </li>
<li>Move implementation of demanding attention to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e672a5372159877fbf822d4e147cba446b225f41'>Commit.</a> </li>
<li>Demands Attention handling in Workspace operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=16568804be32b1b7c73a1ca65278df8c7a024461'>Commit.</a> </li>
<li>Move implementation of isSpecialWindow to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=986308f938f16f8fad612aec947b285c0ecd2a73'>Commit.</a> </li>
<li>Move implementation of wantsTabFocus to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4b27abd76b975ba3cdbba6bfdcbf9f95b1a2cb61'>Commit.</a> </li>
<li>Move auto-raise handling to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1fcd1120d9070fbcd11f963ab7cbd49b781e4c5f'>Commit.</a> </li>
<li>Implement keepAbove and keepBelow in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=25e3af5988347f1499131fbe8e6247b23a055f63'>Commit.</a> </li>
<li>Workspace::updateClientLayer operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b84118a51bba5846136202ed7b57d548954b6805'>Commit.</a> </li>
<li>Implement AbstractClient::isActive and ::setActive. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7d51838efce867ea7e2820de3e5a0e88230289be'>Commit.</a> </li>
<li>Implement AbstractClient::icon. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3527f1d4b8123b71cfc14511ab18a920f5f8ea07'>Commit.</a> </li>
<li>Move property closeable to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2f59f3e8b4a023afd9f44fc3163236d480ed4a02'>Commit.</a> </li>
<li>Implement AbstractClient::skipSwitcher. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d466f9ab3f251a1625bb0c11a5b7fb781ef1cd9'>Commit.</a> </li>
<li>Implement AbstractClient::isFirstInTabBox. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3182c8f3e51d12ae885d005383b955a7b0b23223'>Commit.</a> </li>
<li>Implement AbstractClient::tabBoxClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4ac4a97c200ee55c8d284815ac1f302879fba09e'>Commit.</a> </li>
<li>Remove no longer needed casts from TabBox::establish/removeTabBoxGrab. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ab4e79883721a0e897fd452989301916f3b804e8'>Commit.</a> </li>
<li>Drop no longer needed dynamic_cast in Workspace::performWindowOperation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1e7a28033b9b46d83a8f394fdf855df4aead0d1b'>Commit.</a> </li>
<li>Drop dynamic_cast from UserActionsMenu::slotSendToDesktop. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a74413712678d9cb07e55c3ede52faa81a037e8'>Commit.</a> </li>
<li>Remove remaining Client usage from FocusChain. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d4f74457493f1698103bfdcbfbda60d5a7e9952d'>Commit.</a> </li>
<li>Drop no longer needed dynamic_casts from TabBox. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=704b2552340cd15a204ea71a1a8403649709e1dd'>Commit.</a> </li>
<li>TabBox::shadeActivate operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7d5236cac879f3f065ae215d7a4cefc5e5ea6ad5'>Commit.</a> </li>
<li>Drop not needed dynamic cast in WorkspaceWrapper::setActiveClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9457b362c180ee72d248d68069c5ec34348af124'>Commit.</a> </li>
<li>Remove no longer cast in Workspace::allowClientActivation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe8946740c63276937d1e31a8b9b598b6d50c721'>Commit.</a> </li>
<li>Move Workspace::activeClient and ::mostRecentlyActivatedClient back to header. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=41cd927bce2f907e83cdaa36dda44d0d38f30f66'>Commit.</a> </li>
<li>Make Workspace::active_client an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fdcaf2b86f344c909f65a9d676b88ff348f8fb52'>Commit.</a> </li>
<li>Move ::demandAttention to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=69f198e92320128f351354ba6b1752f4f9529f4f'>Commit.</a> </li>
<li>Workspace::should_get_focus becomes QList<AbstractClient*>. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=af48d38d22016b8755a705a835f94e9edfba8b21'>Commit.</a> </li>
<li>Workspace::mostRecentlyActivatedClient returns an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=94ce47ef7b118bc57e7ecda94902688843207ce5'>Commit.</a> </li>
<li>Workspace::allowFullClientRaising operates on AbstractCliet. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b1026aa1623296bd39724d45f24f244a370977ef'>Commit.</a> </li>
<li>Workspace::last_active_client becomes an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d7d1787a81c15d039556166f54cae246e1744f8'>Commit.</a> </li>
<li>Workspace::allowClientActivation operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f28d759352a6d8a6eb7855fc2097086fe95c0101'>Commit.</a> </li>
<li>Move ::userTime to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dde194746103b03ee0583371fffe23cde7a561a8'>Commit.</a> </li>
<li>Move enum QuickTileFlag and ::setQuickTileMode to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1a7e8aeaf25a647712ebc14edf753b5213dfeda1'>Commit.</a> </li>
<li>Move ::packTo to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5b53d2401ed60fab33d753161d74f5943be1ac3f'>Commit.</a> </li>
<li>Workspace::packPosition* operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=683fc0613f5ffc224994a5fa5226348bfd53a7d3'>Commit.</a> </li>
<li>Make helper method isIrrelevant operate on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a85ee5309850171b9a7c0b10b157898c7bb1368'>Commit.</a> </li>
<li>Move ::isCurrentTab to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7ffc742a69c430c2debf04303580c00fc33b77e3'>Commit.</a> </li>
<li>Move ::titlebarPosition and enum Position to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6feb1c6604dd1781d134633badf13d01533e758f'>Commit.</a> </li>
<li>Move (grow|shrink)(Horizontal|Vertical) to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e5734f9e6cc07d24034a80b5b5624dcfab591cc1'>Commit.</a> </li>
<li>Workspace::sendClientToDesktop operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=138c1b2ff076f0bf810820709c26dce5ec56e445'>Commit.</a> </li>
<li>Workspace::showWindowMenu operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4f1c98773e1eccdcc7a23aede04571d576749e50'>Commit.</a> </li>
<li>Workspace::restackClientUnderActive operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c357ac01fe5d95907812d872a18247d1b79dba55'>Commit.</a> </li>
<li>Workspace::slotWindowTo(Previous|Next)Desktop operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ddcd4dab59f324ec6a30f6488fc54619f99ed219'>Commit.</a> </li>
<li>Workspace::takeActivity operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d4701d67eb10d78a3095040fce61baceaaa3bc2'>Commit.</a> </li>
<li>Workspace::activateClient and ::requestFocus changed to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=484f9b6e65214f20445aae1140a7171f89a2443d'>Commit.</a> </li>
<li>Workspace::clientActivated signal changed to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8cfe0a93160641520f736ca61893c57ef56708bf'>Commit.</a> </li>
<li>Workspace::activeClient returns an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3ad117ac2874d5d516a3a64bfdf135c180a68807'>Commit.</a> </li>
<li>Workspace::movingClient changed to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4b41c33268a1c155c606293d1d39ba2f61a478ea'>Commit.</a> </li>
<li>UserActionsMenu operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d2cdd3de1d8eb46245d7e1adcc669b12c88ca8ab'>Commit.</a> </li>
<li>[scripting] actionsForUserActionMenu operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4eb9a98f1a6da0d2186624055eb9fc6ce43384da'>Commit.</a> </li>
<li>Workspace::performWindowOperation operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=519006ffa4a756b08078b3340c924014a4983ecd'>Commit.</a> </li>
<li>RuleBook::edit operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bfb52b20c9c7edfee7f3c2a74ad6ce41ee599f88'>Commit.</a> </li>
<li>Workspace::cascadeOffset operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bfa4e1cec147ea8236fbbd16c3d394d6e1295c17'>Commit.</a> </li>
<li>Workspace::setupWindowShortcut operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=942b49e2c4cff97085b7cd3cc2697d69deadbb7b'>Commit.</a> </li>
<li>Workspace::clientArea takes AbstractClient instead of Client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aa1e9161d5c5a1b7e4e59e7508dd5e35af76dae8'>Commit.</a> </li>
<li>Workspace::sendClientToScreen operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7c187359a7853f1926f91604b8888ea2dbcf3a7f'>Commit.</a> </li>
<li>Screens changed to use AbstractClient in ::setCurrent. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c46c92e20460f8450aa291b35710da8c7010899b'>Commit.</a> </li>
<li>TabBoxClientImpl changed to using an AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6e45901844eb4283f84a8519e72cd6cc6cdd6e91'>Commit.</a> </li>
<li>Use AbstractClient for raise/lowerClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f800477be5102ffe78a2cacfc4a4faa3dba9b5a9'>Commit.</a> </li>
<li>Add overload Workspace::hasClient(const AbstractClient*). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1b02837d0b579238eff2159827dfdec0a15888a6'>Commit.</a> </li>
<li>Change Workspace::restack to operate on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=792745c9633be725143e16571910a9f6394b8985'>Commit.</a> </li>
<li>FocusChain operates on AbstractClient instead of Client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe7a6834b200d2192ae1b50779e9017c6463e5c3'>Commit.</a> </li>
<li>Introduce an AbstractClient base class for Client (and ShellClient). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0f54da9dde2896527bdcdbd5f8ba0730a00cc7c5'>Commit.</a> </li>
<li>[wayland] Introduce a ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d60c3778908763397afea09524d8f4b3595feb0d'>Commit.</a> </li>
<li>Toplevel::windowRole becomes virtual and is copied to Deleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8fad5830f125d3c00e8aaaae97f5825967af37b8'>Commit.</a> </li>
<li>Copy NET::WindowType to Deleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fec2b6310282d9ca11d21b5ea66e2f3243ef1825'>Commit.</a> </li>
<li>[input] Add safety checks before accessing Cursor of focused pointer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e8b14cd5877131f416887f2e1862330c69a8e5e7'>Commit.</a> </li>
<li>Make Toplevel::opacity virtual and copy it to Deleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=92d06a8c4b870568537df9cea7f51af903d79ab3'>Commit.</a> </li>
<li>[wayland] Reference BufferInterface in WindowPixmap. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cfba4bacdf894708f4ae53bb67305129b1ae7b06'>Commit.</a> </li>
<li>[wayland] Pass EGLDisplay to KWayland::Server::Display. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=209b62d522271dcaa8ae29132404618a3b00bb9d'>Commit.</a> </li>
<li>Add a hwcomposer backend based on libhybris. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e12400a67574f2f7415427f091572e0c22b9d5b3'>Commit.</a> </li>
<li>[kwinglutils] Init glVersion on gles. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=713c6b25dce2fc60184ddc7b618b0b1195ab9870'>Commit.</a> </li>
<li>Properly add define for gles. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2a8d7664b3f605bab3416660d0805ce6227fba01'>Commit.</a> </li>
<li>Create a plugin for each of the wayland backends. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2220ae44c4c7f4868e8cf74639b2c0e5772dcbb8'>Commit.</a> </li>
<li>[wayland] Introduce a deviceIdentifier in AbstractBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=33fb763e3701db1a986f6003f01bc042a1aa08e4'>Commit.</a> </li>
<li>[wayland] Add an initialWindowSize to AbstractBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d45cf6ee8d83a7898f31309e46725dbb64968262'>Commit.</a> </li>
<li>[wayland] AbstractBackend can emit an initFailed signal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=17d5839580a0c88f22cea4ea28b16dbc62f14308'>Commit.</a> </li>
<li>[wayland] AbstractBackend provides signal screensQueried. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0782252f72f07926f0fcd4a7188b6d4c6f5efd75'>Commit.</a> </li>
<li>[wayland] X11WindowedBackend anounces input support by itself. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b2f5873a282de5f16120a3ae6e9b458d8c1f3543'>Commit.</a> </li>
<li>[wayland] Use a pure virtual AbstractBackend::init method. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c4f8bed815df8a0707eea769a9442ad418971408'>Commit.</a> </li>
<li>[wayland] A backend can mark whether it is ready. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2afaa60dc55684838eec12b6e80f8d726b54dfce'>Commit.</a> </li>
<li>[wayland] Dedicate logging category for each of the backends. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ff394a020dece8690591046c26fcffa0def4fcb'>Commit.</a> </li>
<li>[wayland] Don't call into X11WindowedBackend from EglOnXBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=767ca8d357369542ed63897f226677b555733258'>Commit.</a> </li>
<li>[wayland] Move X11WindowedQPainterBackend into backends/x11. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d80984beab2aee95ca66e656763cf059065e90d2'>Commit.</a> </li>
<li>[wayland] Move WaylandQPainterBackend into backends/wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f7bf7cb7baa077bd92af26031ea68151900de7a2'>Commit.</a> </li>
<li>[wayland] Move FramebufferQPainterBackend into backends/fbdev. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df6221ae4a598c8a23f21143d746d9d2bb348c50'>Commit.</a> </li>
<li>[wayland] Move DrmQPainterBackend into backends/drm. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=06d820619260850d69bc5403f6802c9bbcea27ee'>Commit.</a> </li>
<li>[wayland] Move backend implementations into a backends/<name> directory. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=72db1e63a342227aef07fa6a7682558fccacc0bb'>Commit.</a> </li>
<li>[wayland] Split out WaylandCursorTheme into own header/implementation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=300a576d32f682d3c2f978fb7699410df70f9f70'>Commit.</a> </li>
<li>[wayland] Add protected AbstractBackend::repaint(const QRect&). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=79b5685b7a562b5ec7175773a7cfad5a3e50217e'>Commit.</a> </li>
<li>[wayland] Don't call into InputRedirection from AbstractBackend implementations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8be38dc4f62282ff9c0ec2c099f44761873ff81b'>Commit.</a> </li>
<li>Fix unintentional no-op, unite() doesn't change the object. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e283bda8e5e5c83dcbd0f31adb3007182e72960f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123618'>#123618</a></li>
<li>Add modelines to OutputInterfaces in DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b50d6d487befe0e1980d3b0ee41966ced8883127'>Commit.</a> </li>
<li>Read EDID in DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d3fa98e09b120da4da6f0d5a7b44feb4d83acda'>Commit.</a> </li>
<li>Fix build of DrmBackend with gbm dependency. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c52f54226135fd1fa9341855311662eff4908355'>Commit.</a> </li>
<li>DrmOutput creates OutputInterface on init. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=389d81731b72ad3393c3334090d4fcd49740ab02'>Commit.</a> </li>
<li>[wayland] Allow the AbstractBackend subclasses to manage OutputInterfaces. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=603844cf98b939e8c478997448c147a4fc815f0e'>Commit.</a> </li>
<li>Improve tracking of DrmBuffers in DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f775e13c6ee9a84028473c401b15ec5703e2fbca'>Commit.</a> </li>
<li>React on DrmOutput removed/added in EglGbmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=843bfb8052995245bacd806d638f036bdcb55eb0'>Commit.</a> </li>
<li>React on DrmOutput added/removed in DrmQPainterBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e298c2d5842ddf405a01db6b9024a58ae5fa6525'>Commit.</a> </li>
<li>Update ScreensDrm whenever the screens got queried in the DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a05e4b5353ce128c97a95e665caadb5588bfd3b'>Commit.</a> </li>
<li>Support hotplug events for outputs in DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7aee69a6b2915fd04da7e1b1828c94ca2acbe535'>Commit.</a> </li>
<li>[udev] Add wrapper for udev_monitor. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d9a29f3a49ac2bf2f5721835a7c97a9d97293ec0'>Commit.</a> </li>
<li>[udev] Read property on UdevDevice. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cf27a056b8ebf62ceb4710ba9cbe1ee61ce9d8f8'>Commit.</a> </li>
<li>Improve the rendering per output in SceneOpenGL/EglGbmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2a64755b764e2239f249f347eb4bc545356a9a3d'>Commit.</a> </li>
<li>[drm] Drop DrmBackend::present without DrmOutput*. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c33a74ae58800543d36187406366b244ca22c3ce'>Commit.</a> </li>
<li>[drm] Find correct crtc/encoder/connector combination. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e650eef323ef0dc13cc197f2c91c772277c102ec'>Commit.</a> </li>
<li>[SceneOpenGL] Render per DrmOutput in EglGbmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7c8c1dac0a702e28b3b94c1f01de6c844222c663'>Commit.</a> </li>
<li>[SceneQPainter] Render per DrmOutput in DrmQPainterBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5c79f777a4bf280eea85d4252f94d8a61aeb8ff4'>Commit.</a> </li>
<li>[SceneQPainter] Per-Screen rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9133c0f9d5bacfd472c7e6f8884ce9d7b1ee32c1'>Commit.</a> </li>
<li>Allow presenting a DrmBuffer on a dedicated DrmOutput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=600b3cd2c146959a87a2c28138d8ca0f6bcc435f'>Commit.</a> </li>
<li>[drm] Better support for multiple outputs in DrmScreens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fcc895180ba10885ae5c9b72c1ef802aa31cff80'>Commit.</a> </li>
<li>Set the drm mode again when stride changed compared to previous buffer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d6c9514029e693a88c6fd0df810a944a9d74695'>Commit.</a> </li>
<li>Handle session deactivate and restore in DRM backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8278ae84874517254f61504701ca072d564479a8'>Commit.</a> </li>
<li>Connect to Logind's PauseDevice signal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a9adcac9139ec46d930fc97cf2fd8eb9afac8a8'>Commit.</a> </li>
<li>First deactivate VirtualTerminal, then notify kernel that we are ready to switch. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2c5337074f7f1845dc3867b388cc9006db5ad13f'>Commit.</a> </li>
<li>Set VirtualTerminal into KD_GRAPHICS mode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9efada5bb62aebf56086740d95f16ec704ac443b'>Commit.</a> </li>
<li>[libinput] Bind libinput support to VirtualTerminal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=efa0500313aee37334e760355ed84b208f1781cf'>Commit.</a> </li>
<li>Add a DrmOutput class to be used by the DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2f312f35c9de18b0e97221d0e7e894708a2bc407'>Commit.</a> </li>
<li>Add support for hardware cursors in DrmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=38b676d8096d3a4e9ee8fb4e74c6a3fe86e8762a'>Commit.</a> </li>
<li>Initial implementation of EglGbmBackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=877c33fe7d2b1bc72f5fb103cf0c59d68938e318'>Commit.</a> </li>
<li>Page flip event support in drm backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3a7d0c395e881000d6f47c09c35c5782e051ad74'>Commit.</a> </li>
<li>[wayland] Add a basic drm/kms backend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c759551340ea52b60f26cf7d0a8e51638c0bd9ea'>Commit.</a> </li>
<li>[framebuffer_backend] Delay VirtualTerminal::init till we have logind control. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=da3f7914f7a5ab073dec1747636e2cec633ad938'>Commit.</a> </li>
<li>[udev] Find the primary gpu by iterating drm devices. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f135d92c006655bc2bd3e131a48b655ceace57cd'>Commit.</a> </li>
<li>Move Udev into an own header and implementation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e6b076df9cef48a94dfd5e8486243c20259670e5'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d31775ee80b4afee09473c134cc68aabe48f57c'>Commit.</a> </li>
<li>[wayland] Pass X11 events through the native event filters. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c8ebcf4ca29dcb6568585df995fa20c28c9edae1'>Commit.</a> </li>
<li>[wayland] Add positional arguments to be programs to starts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=db34ddda5335ab3bc90bfd202ba1fe53c8c6bd35'>Commit.</a> </li>
<li>[wayland] Abort if Xwayland connection breaks. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ec3f55ff4cfc17dab5751df9cadabc9a4dee1b9'>Commit.</a> </li>
<li>Don't follow QT_DEVICE_PIXEL_RATIO. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=10f1dc3fcb63f95ae5f2f08a0183e188353647ad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123115'>#123115</a></li>
<li>Add disambiguation context. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f9a6278d0cde10758ea21eaeeb6c7f892d5301d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123396'>#123396</a></li>
<li>[autotests] Fix TestScriptedEffectLoader::testLoadAllEffects. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cfe84aa9deaade35dd4edf437b1a1d1e6c3fc6bc'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix regression in Output::geometry() behaviour introduced in f57dc. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=20855223b99ad58b041cb8954816b1f979abae56'>Commit.</a> </li>
<li>Add Output::size and make it report correct size. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f57dc936a41d681fc250026a95e284d15ac2042a'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='http://quickgit.kde.org/?p=libksysguard.git'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Properly hook the close button for the Detailed Memory Information Dialog. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=6682a4b8ab96d1d2f940d5a4d9b66d1945149e7f'>Commit.</a> </li>
<li>Make Renice dialog work again. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=fd35292ef539b413449cc05ab12737b9b6a8363f'>Commit.</a> </li>
<li>Fix assert by not calling onto an empty path for setOfflineWebApplicationCachePath(). <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=18e151f5c34cb7dabdc6c777cb79a8694744cab4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123222'>#123222</a></li>
</ul>


<h3><a name='milou' href='http://quickgit.kde.org/?p=milou.git'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Remove debug output. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=7859d85410b6b8cde214578cfa48a67c8adc5f89'>Commit.</a> </li>
<li>Allow overriding the displayed type text and work with plain String array. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=011b266bc90fb55098efac743f76936447c36e7f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124516'>#124516</a></li>
</ul>


<h3><a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a> </h3>
<ul id='ulmuon' style='display: block'>
<li>Improve InstallApplicationButton. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=9ef5dc04b5840d3e9b86bfe559dc74f8a21e8fd0'>Commit.</a> </li>
<li>Fix ProgressView display. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=fc659b04288453f293891fd0cf2583ad13658b41'>Commit.</a> </li>
<li>Don't call stuff "theme". <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=40cc347f878642fae816462fdfc6357e939aae8c'>Commit.</a> </li>
<li>Reduce allocations. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ea7810364f0a250ee48c11e319bd917a480a5b53'>Commit.</a> </li>
<li>Remove null action. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=18f32d51f9111bb6b8551a17a51a9e835ada9460'>Commit.</a> </li>
<li>Use the correct icon for settings. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=6da8736c0266bfacf4cac93b97943a082a7e75b2'>Commit.</a> </li>
<li>Simplify ApplicationsTop delegate's code. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=74d7a66c8e6c9803f91673b8e2d3e541997e86c4'>Commit.</a> </li>
<li>Fix alignment issue. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=5996b6d59f0ebad1ab3bd634ca104797f45e9e13'>Commit.</a> </li>
<li>Enable high dpi pixmaps. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=2a07a0f90ed4108dafc3e55e6d8893776f6ea1ef'>Commit.</a> </li>
<li>Stop messing with the enabled property of actions in the action collection. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a3dd573823f0d97dc144b2b6f773eb5621816f9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349787'>#349787</a></li>
<li>Fix tests. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=572317ed5e2d2bad7bf4136c3446090154afdd07'>Commit.</a> </li>
<li>Fix ResourcesModel initialization for tests. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=48dfda95a5e113b4f1ce8300eceeb8dba1d76836'>Commit.</a> </li>
<li>Reuse previously set variable instead of doing a comparision again. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=1becd4cd6214b611e416d9cdc0a41444b6c723d8'>Commit.</a> </li>
<li>Make code more readable. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=692b6d07a0d40aa8b9c8a79f77a277378b0a4550'>Commit.</a> </li>
<li>Properly export the BackendNotifierModule class. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=e0e0453829b28418c2bfbfbc03c205c095dc3a84'>Commit.</a> </li>
<li>Ensure display of buttons without text. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=5a22811f512cc670faf46362ee07165e0aefb28d'>Commit.</a> </li>
<li>No need to add the text to the back button. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4ba930b9c36e3b8deb69539a9d9d1b56e2444332'>Commit.</a> </li>
<li>Fix startup with Plasma components. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=9352fbd124c0f1e082b1d39620d95d73d239215e'>Commit.</a> </li>
<li>Offer progress for the standard updater since it's started. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=abaa6f10ef70444371b735b00284e5a3daeb33fb'>Commit.</a> </li>
<li>Modernize!. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=d5fbffbb1e89a5d130adfecf85f58fd60c4da72b'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=faf0d8ac7e416e45554150482feb3a65b3c29237'>Commit.</a> </li>
<li>Just cache roleNames. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=24610b2a780054670a3113e5a120ce65428d49de'>Commit.</a> </li>
<li>Optimize on tests. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4d04c4ee8e3375e2a6e13ae7c20b45df80addd32'>Commit.</a> </li>
<li>Improve roleNames performance. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ebe2112e16dec97934ddf5f8280370317cb361a1'>Commit.</a> </li>
<li>Sources page fixes. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=61550947672c42ca04b88513dde291b79c86b244'>Commit.</a> </li>
<li>Sources page fixes. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=bbeb7fdb1ed07994ad2caab071fd3dace02b771d'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=1221200ec0f4dd3b2cbb09568928ac2ab1d3891f'>Commit.</a> </li>
<li>Add screenshots on the smaller application page. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=2ba4097d83ff10db6cb122a7b1549c4f8cd8c5ea'>Commit.</a> </li>
<li>Fix category switch. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=7b2c3a577f5c645458c8b5045819e63b799f04c3'>Commit.</a> </li>
<li>Improve Application* re-usability. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ef1fcaf72e6f2f17657c8b0f7ee3916f9e5272bf'>Commit.</a> </li>
<li>Don't try to run the releasechecker if it wasn't found. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=6469bd5b47b5959fde53cee0c3212e13570fc2ba'>Commit.</a> See bug <a href='https://bugs.kde.org/349787'>#349787</a></li>
<li>Request updater to raise if another instance was created. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=cb3b11cac7d3ca5358d6b5814f2a02b30503bf9f'>Commit.</a> </li>
<li>Add shortcut to go up again. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=d27118ab0677cc5a86bbea5ad97bc65da21a0096'>Commit.</a> </li>
<li>++warning. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=e9b76dce80dba94e7aa10ec847e0cdcfdacfdae6'>Commit.</a> </li>
<li>Make it possible for the ApplicationPage to adapt to the screen size. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=379c1853d21dba0935bffef88633fcf7abc502e7'>Commit.</a> </li>
<li>Improve the ApplicationList on a compact display. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a1fd1cb115b558410b8e4df5ee3e2bd4887d0ec7'>Commit.</a> </li>
<li>Somewhat adapt the toolbar for smaller form factors. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=8a036f7cc6d73d377a745b7ef87097be4b18bffb'>Commit.</a> </li>
<li>Remove unused stuff from the easter egg we removed. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4ab44ab4314d15c9fa4f91a5299dec15bfb29fba'>Commit.</a> </li>
<li>There's no problem with shutting down while fetching. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a681d0556b5d515185708989f10edbce365d5c96'>Commit.</a> </li>
<li>Improve size of the Grid delegate when on compact mode. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=0905d18f47f2f559d6b4f1f2204d10bdcb89080f'>Commit.</a> </li>
<li>Remove duplicate toolbar. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=0e83f21849d525e0dd123d254e9f0317802bb697'>Commit.</a> </li>
<li>--debug. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ebcfe63295aa9922069d79d8dfc61f68ed8c0908'>Commit.</a> </li>
<li>Properly specify property. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=66a785c72e458f4e2600b1729719acd7c60bc154'>Commit.</a> </li>
<li>Improve integration on different sizes. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ec0439f70a84bc988bc56bfaf2c863083434718e'>Commit.</a> </li>
<li>Porting error. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4ff41caf8663b408406e4369d9aa95884ee19661'>Commit.</a> </li>
<li>Flexibilize UI to adapt to smaller screen sizes. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=755d96f2b22e817721f428ef781b8558a9313259'>Commit.</a> </li>
<li>Move Discover toolbar into the QtQuick scene. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ef60e9b02f40b6b79aeea029784ce58c918758b9'>Commit.</a> </li>
<li>Improve DiscoverAction integration in QML. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=d5bedf28c95c50bb9feb92dc12efc8fd66920295'>Commit.</a> </li>
<li>--warning. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=3597f7c6362fbd896770bde8153641bb73309251'>Commit.</a> </li>
<li>Fix warnings. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ea5ae87def2447f38cf91ba51494e7c01820e828'>Commit.</a> </li>
<li>Drop MuonMainWindow as a base class for all windows. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=423adea990a53da5c23338a33e1077ed4a3d5708'>Commit.</a> </li>
<li>Remove the only uses of MuonMainWindow::setActionsEnabled. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=6424cb6c98e0c8fe47ea335a7bfd3be12a3ebd5c'>Commit.</a> </li>
<li>Remove MuonMainWindow::setupActions usage. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=63eea0cf4d7eb2647cd7f476a49eb051c859466e'>Commit.</a> </li>
<li>Remove usage of MuonMainWindow in the backends. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=c2741a6f26977ee3446feec1764c12bea972fae5'>Commit.</a> </li>
<li>Stop using MuonCommon from MuonApt. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=3bf106700a260a67615bd918c4c774a4dc311723'>Commit.</a> </li>
<li>Reorganize *SettingsPage. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4d4bd01cf79508e60bdfabc713768e461d949a27'>Commit.</a> </li>
<li>Move ::queryClose back to each application's main window. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=85327ca8bcf7efbc109f9c687aab84b437534657'>Commit.</a> </li>
<li>Reduce usage of MuonMainWindow. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4afb1ee5ddbf1848c3bf115a34016428f380c01e'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4efb455ef9c44c4cd1d08f10afc9e14460812bf5'>Commit.</a> </li>
<li>Use a fancier location for QML install destination. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=cbd8fb1694a29255638f7956b2097edc5956e2d0'>Commit.</a> </li>
<li>Don't show both columns if the application is to narrow. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4c38a9b22d17dd74c3d61bd47f298658831cf53a'>Commit.</a> </li>
<li>Reduce usage of MuonMainWindow. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4b5e829ab0bb4962d23502fa70bfe6ff48f8f162'>Commit.</a> </li>
<li>Use pkexec instead of kdesudo. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=36b97c0e2b23707687c66e4b39ccf7b97f878639'>Commit.</a> </li>
<li>Prefer use of layouts than anchoring. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=5e76a1b94fffe8ecf14c9a5dab9fd053676de9c0'>Commit.</a> </li>
<li>Cleanup code. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=6a02dbfb0be6fd3a3a80622a69df8c27705b5c10'>Commit.</a> </li>
<li>KDELibs4Support is only used by muon package manager. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=0aade21aa8d97d916e9bb85728aec296d9faccd0'>Commit.</a> </li>
<li>Properly feed the output file to muon-exporter. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ce0ceac7431a1aa0b7846db2b5e15b1a75ac5170'>Commit.</a> </li>
<li>Ensure we don't crash muon-exporter. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=158c93d4d8451b9a37f065ab3bdc40d81980378d'>Commit.</a> </li>
<li>Make it possible to just open the updater, even though there isn't updates. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=194ea4ecdc91ac9acc497c01b98d1b780b4e6d58'>Commit.</a> </li>
<li>Fix network connection awareness. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=10b743aa6f183227c0e7e1f34ba1618c0717e112'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=4140ef0064619c6c8aab34087314893d2726c29a'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>[kdecoration] Add support for KPlugin meta data. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=ddc453d52db858346916f2567d09de05d5b0e285'>Commit.</a> </li>
<li>Added the (compile time) possibility to render link buttons are flat. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=3651e06e8345af88b4f65c7330dde6b7399a2b3e'>Commit.</a> </li>
<li>Fixed rendering of QCommandLinkButton. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=800fff44e6291fdf72868033116c87a25b6966c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348257'>#348257</a></li>
<li>Properly account for MenuItem text size when computing sizeFromContents. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=cbc7d9c710f28e68f9dc2a46c073932631391845'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349258'>#349258</a></li>
<li>Remove min border size requirement on the sides, for tiny border size and above. Bottom side is kept at min 4 pixels,. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d599d0467e5eda466a07aee7c29e9969dbd115e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349039'>#349039</a></li>
<li>Properly calculate caption rect when button lists are empty on one of the titlebar side. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=630d303a212ed7c56d0a894e0fb6a93fb379b3c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349107'>#349107</a></li>
<li>Added feature summary for xcb and x11-xcb. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=25e1fbd0f9b35955c837dbe659d4ed661fef51dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347420'>#347420</a></li>
<li>Default to Qt::AlignVCenter (instead of Qt::AlignTop) when vertical alignment flag is not set CCBUG: 346679. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=5eb9eda992bfdbd5238ab78f0d7b2d728f77d7dc'>Commit.</a> </li>
<li>Make sure iconSize is valid before rendering. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f99a3fde2b6f92ccc2d41140517b4e3e0bc1ebb1'>Commit.</a> </li>
<li>Sanitize button positioning. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=444366b87232920c926a1447273d1a30be42080e'>Commit.</a> </li>
<li>Pass iconSize as button's parameter rather than using qobject_cast. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=561b1c8460d3b6d0f9a9ea74fefda0ec3e8c6e02'>Commit.</a> </li>
<li>Get icon size directly from decoration's buttonHeight, rather than trying to calculate from geometry and offset. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=6a3c6951604abfe21c689ebf08dff98474ee0434'>Commit.</a> See bug <a href='https://bugs.kde.org/346955'>#346955</a></li>
<li>Slight adjustment on shadow/glow positioning. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=cf9dd89234cd113bf1b04843912999fc5d7d7cc3'>Commit.</a> </li>
<li>Try sanitize button rendering, and fix centering with respect to button center. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=b6bedee62041fbe780673699eef16a2e4b105f74'>Commit.</a> See bug <a href='https://bugs.kde.org/346955'>#346955</a></li>
<li>Properly hide buttons when corresponding feature is made unavailable on client. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=2ba1699c010dd069eeb53993ae7bbc8e8a18dcbe'>Commit.</a> </li>
<li>Renamed icon to oxygen-settings, as was originally intended and to prevent conflicts with oxygen. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=43d1fee6b1bc79cf6bfd298e2812ee76d2b26f22'>Commit.</a> </li>
<li>Fixed button positioning for varying sizes. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=53923cbfb2cb781972005a91944de9e55e66accd'>Commit.</a> See bug <a href='https://bugs.kde.org/346955'>#346955</a></li>
<li>Explicitly delete size grip in destructor. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d32447dd7cab292d13ab455786330dcdadbf93bb'>Commit.</a> </li>
<li>Restore proper default value for hasBorder flag in shadowcache's key. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=bf4ad3a3457de6fd723aa1878c6ea72c092648ae'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=24404c435f0ac69cc386bbb0db2fb99c33447b8b'>Commit.</a> </li>
<li>Remove superfluous call to ::setShadow. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=921d2e0f38ac2347161b0aeec4e607ffd2d2a7ed'>Commit.</a> </li>
<li>Move animated flag earlier. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=3824ad6182c3cea35a37dcd7383ab0e895e09bb3'>Commit.</a> </li>
<li>Improved layout. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=adfb56aa5516e8d9f6c8784721dbc0b6c07de038'>Commit.</a> </li>
<li>Simplified ui. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=65bc692072fc595cc4a257b9b37f5c6c8670af91'>Commit.</a> </li>
<li>Re-added the possibility to hide the titlebar on some windows. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=70de6f2ac2787fa06be80182e75e953ecddb0f58'>Commit.</a> </li>
<li>Removed extra pixel needed for separator, since there is no separator. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=dda0b46f239ee46df5fcb7db39b979abfd7d4f45'>Commit.</a> </li>
<li>Re-added shadow configuration. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=59328eb7a152f083cd2f3fc7cf6793d17fd2519f'>Commit.</a> </li>
<li>Unused variable. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=80a19c2e2e3b374195e2239f481ef77e49bd103f'>Commit.</a> </li>
<li>Ported KDE4 configuration ui to KF5. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=764dc22a03001c5796cc9fc9d398a67a2cb99520'>Commit.</a> </li>
<li>Removed 'expert mode' button. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=15f82b0aaed1cdc604b0c75d9d9f773de5ae8055'>Commit.</a> </li>
<li>Properly update button's animation duration on reconfiguration. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=dbffd4941033a2904f85b94e526627823b5fafca'>Commit.</a> </li>
<li>Removed config dialog class, which is not used any more. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=8aeb3990d8a57dd8bc5c441bfe6b147e952a0f9d'>Commit.</a> </li>
<li>- set proper animation duration value. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=5052832a9c6db6f90c112ba3f89cadcd92c81739'>Commit.</a> </li>
<li>Trigger update directly, on active state changed, when animations are disabled. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=363b9d20eaff9e0da7ed6c01c1bd5dc39d960e00'>Commit.</a> </li>
<li>Implemented cache for decoration shadows. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=e024c7dd0351fe55d6e56b40f12cc0a7ff3688a5'>Commit.</a> </li>
<li>Create active and inactive shadow and properly update. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=567e36cccc5a51a596f982cd6b0b2f5de53ab22e'>Commit.</a> </li>
<li>Properly load helper config. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=09d01acde1c0dec6757fb916ced10e69dc1b2a31'>Commit.</a> </li>
<li>Use shadowcache to setup decoration. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=129c013d90d88a0bed665c6c054c7433592fa444'>Commit.</a> </li>
<li>Removed decohelper singleton. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d9560aa2ced36beda3bb67da3ea6f68ef3ce33f2'>Commit.</a> </li>
<li>Fixed button positioning and geometry so that their position remains unchanged when maximizing, while still satisfying fitts' law. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=b36866edf794c843c001b7301247ab93a58df560'>Commit.</a> </li>
<li>Use KCModule directly, to handle configuration ui. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=971fd1feee04bca20546b27f3ac0cd3e7613d7ab'>Commit.</a> </li>
<li>Use KCModules for oxygen-settings. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f67aeca697d6f7b8941572292feb0724fe69f1c1'>Commit.</a> </li>
<li>Removed incorrect comment. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=ff0b3334b3a5488ac34262fdd4c38ba37d5b8374'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=a35487b77e30f94f1c4fefcb1876f666ba652c10'>Commit.</a> </li>
<li>Fixed 'corner' rendering in maximized mode. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f930c56fa2c0349edee8c5060e90d4103aa2bcc1'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=03a9eb316341f6c4dbb8eb49cbdb58c63d45c54e'>Commit.</a> </li>
<li>Fix caption vertical positioning in maximized mode. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=88fdc448a6b78ad77b30927b2c7ba05ac6ac3196'>Commit.</a> </li>
<li>Fixed pressed state of decoration toggle buttons (on all desktop; keepAbove; keepBelow). <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=858e126268ef83fe76e63c9b8bac6848402d8fb0'>Commit.</a> </li>
<li>Removed unused methods from helper. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=745c2a598753e813fc02fc049f76d872d15d1cbb'>Commit.</a> </li>
<li>Removed titleoutline option. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=54914d2d2429e7572ef39d29c41fb67443770fae'>Commit.</a> </li>
<li>Simplified and cleanup title rendering; removed titleOutline. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=edde7a6f667bb2686f961cab9db181c48c5d57cc'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=8a228542b4fa009d23dbb57648cbe798cb90cff4'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=c8488463189377ae83bbe58bd9bea5249b8fab62'>Commit.</a> </li>
<li>Added David as copyright. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=3a883d8fd803d29e44e55eb4dd87a6586e47c697'>Commit.</a> </li>
<li>Fixed bottom corner rounding for shaded clients. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=8aa000121fc21dce4c1fe7b3df23edf9c1532c07'>Commit.</a> </li>
<li>Use font from settings to draw caption. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=029f6c7ffd5015642ca80fe140ed0b17aad36dba'>Commit.</a> </li>
<li>Fixed column stretch. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=51d2edcab493587c89f70e1b6852a6b8d9612b93'>Commit.</a> </li>
<li>Cleanup transition data, to align with breeze. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=78961c9360751e7a74e4168ffcd9cb47c26d8300'>Commit.</a> </li>
<li>Implemented default event handler to transition widget to make sure it gets hidden on mousePress/release and KeyPress/Release. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=af3db05fb26e66c323111deec3e2d05fc0a7a977'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8689559fb4bbaaf87468edad73e726dba0c08227'>Commit.</a> </li>
<li>Use the better implementation of IconDialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a514de428ef1da5fa14dc004775c21b3b710e7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350864'>#350864</a>. Code review <a href='https://git.reviewboard.kde.org/r/124580'>#124580</a></li>
<li>Update autostart kcm docbook to kf5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b7d61145b57e5f749f5c5627d044a47254408ba4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124296'>#124296</a></li>
<li>Disable useraccount, replaced by user-manager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=92f2760d4f923e054e3ffc188e85e8c73ed290a6'>Commit.</a> </li>
<li>Remove unused completely incorrect import. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=38e65aa3af12117d900e872f975624dc351bda24'>Commit.</a> </li>
<li>Fix white text on white background. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5f807ffcac9ed193c3efd1595aaf061e18e2ae4e'>Commit.</a> </li>
<li>Add note that activating the screen reader may require restarting the session. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d779522d7f3d6e6c347901e8955c935932ba4747'>Commit.</a> </li>
<li>Try harder not to have any duplicate system apps in there. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e1b3ed8aa110015974bdc56dff8f47dfe90c4b90'>Commit.</a> </li>
<li>Fix one more typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ce10efa01d0ec654186ca2a0b590c6b1754766ad'>Commit.</a> </li>
<li>Fix one more typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=49bcb6a68d501e7ec7492bae4ab4129cf3e09ff1'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=90565472bd138d35843e792b957f00f215b976e3'>Commit.</a> </li>
<li>Read screen reader setting on startup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7041819edf5e4a94249cc26feca2a79ef31c1410'>Commit.</a> </li>
<li>Update Plasma Handbook to kf5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=49c8c10c920103008cf8025d2b82c327e49d70d7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124447'>#124447</a></li>
<li>Down a bit. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=97c7d7b6cf3228500bbe93c52888a0077085bdba'>Commit.</a> </li>
<li>More small screen tweaks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=beedfecc6da3f8e1134da9df2c999a3579bbbcea'>Commit.</a> </li>
<li>Switch down from units.iconSizes.huge to .large on smaller screens. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eff1d78c14e6f0869f93ae318ac28ab30ee1e10f'>Commit.</a> </li>
<li>Handle All Apps page delegate pre-instanciation better. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=86fe7cc4b0f8b720c23e23979ae62aff62a9dd43'>Commit.</a> </li>
<li>Fix item hover activation in Qt 5.4. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b13d42a4d1da59ecbfba893036adfbf13ec1e47d'>Commit.</a> </li>
<li>Better check for internal drops. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=13c2f8ce769ca54043848d73c1b1012597a48986'>Commit.</a> </li>
<li>Make behavior consistent with menu version. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=adfd25b21253ebedd2b1056cdaff32189c2a07d5'>Commit.</a> </li>
<li>Temp workaround for broken mouse wheel support in multi-grids. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3d2f9fb6262b65ae89905bc0efb0d252dfdc7e59'>Commit.</a> </li>
<li>Update todo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e8440b2f547554b8476f2b88a5eef3f61ff17594'>Commit.</a> </li>
<li>Work around DND anomaly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fc4877f079ba97df6c3198bc72567f0cb976228a'>Commit.</a> </li>
<li>Add button to clear KRunner history. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=703e9b113c07c447d7d4e3d65c7216627af4865a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123889'>#123889</a></li>
<li>Don't add footer below the last subgrid. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e596bb1d4c8b2bded60f1cabf7c354b7ba417c0d'>Commit.</a> </li>
<li>Skip lazy menu delay in reset state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bb3cfa55284053e1889eb65236c17cbf2d47a23d'>Commit.</a> </li>
<li>Sanity check. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=46b3d8dd311c6851b8684aa5f95caf19462b33d7'>Commit.</a> </li>
<li>Fix fallthrough to view context menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b26bb25060f63675a66dc42dc479576c8d4b59a6'>Commit.</a> </li>
<li>Check string bounds. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=57ba45c76b3b60ee18ecd4b803a399272e62a7a7'>Commit.</a> </li>
<li>Clean up gunk. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=083cfb086d5e2ea9ff0ca5a0fdec1fafc36aaa64'>Commit.</a> </li>
<li>Less jarring reveal for overflowing filter list. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=388d47e3da18d611afa817644a167e0fd3e43b45'>Commit.</a> </li>
<li>Fix filter list content-to-scrollbar margin. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=199a70c4f5b8bd8f63b505bf94b903a032434db9'>Commit.</a> </li>
<li>Add code for Kicker-based fullscreen launcher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f5f59b922589938ebbea02991a223c51a8b770e2'>Commit.</a> </li>
<li>Keyboard: do not reset shortcut for changing layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fcdd29ac411a6806200c51887d64620f15dee998'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124255'>#124255</a></li>
<li>[Panel] Fix min/max size sliderhandles. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=30d283edb5d928f48ac05d340142df25b40bd425'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124481'>#124481</a></li>
<li>[Panel] Delay disappearance of widget deletion dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6834ad378a33dea592f837c2b59ed2b3c34bc62e'>Commit.</a> </li>
<li>Rename kcm docbbok according X-DocPath and generate correct CMakeLists.txt for translation docbooks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4cae43a8fe9ad4cd0d24dc37ab35b742300f6b7a'>Commit.</a> </li>
<li>[kcms/input] Prevent crash on platform wayland. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a45c51e9928c700696a6f2ca6ca90df48f96df82'>Commit.</a> </li>
<li>Add toggle to use launcher vs. window icons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9e6dd77ecfc2286d3d4278e700a382cc94cc458d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348050'>#348050</a></li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a1351fce072c6af0ae5fb83ef12c39cdff17b3d0'>Commit.</a> </li>
<li>Add useraccount avatars from andreas kainz (konqui style). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=42a70b30379877698e07e54cd24c23ef861fd3a4'>Commit.</a> </li>
<li>Add useraccount avatars from jens reuterberg. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2123580ae10a9f7ae5e5783cb43268d0beeff5b9'>Commit.</a> </li>
<li>Update desktopthemedetails kcm docbook to Plasma 5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e14a0e6a80b4c9b8f0f129d411a2aefd8dcce73b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124347'>#124347</a></li>
<li>Look up items by storage ID as well. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9b9957f343a3fc0e2cd019cfb9c9cbca4b5789b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348879'>#348879</a></li>
<li>Fix turning off reverse scroll direction. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=462f7ecd8aff63749fde1bc9c29da0004aff717f'>Commit.</a> </li>
<li>Fix Kickoff applications. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b9ee0b33a9aeebc8ae5e58aaf2cdf34f98fc9c42'>Commit.</a> </li>
<li>Require KDE Frameworks 5.12.0. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1f1ecf94b69ed6c2408c00560653a14f31a9096b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124391'>#124391</a></li>
<li>Remove/Disable useless debug message. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=57803d9120b3baa6f65f155cc3b95f409365c4ce'>Commit.</a> </li>
<li>Scroll to the top when switching between config pages. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=61fe1bc891fb0c20a4cabd7bcba6639a23c0d01b'>Commit.</a> </li>
<li>Only have one instance of the LayoutManager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4d726c4c48c30199a1508f2137f332e638fd8b1e'>Commit.</a> See bug <a href='https://bugs.kde.org/349052'>#349052</a></li>
<li>Only have one instance of the LayoutManager per containment. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1217b9060f5490605832649116b29e94b448a3ec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350157'>#350157</a></li>
<li>Update kcmstyle docbook to Plasma 5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=87d53433b511a0bab0f9ad2161e42546f356f70f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124283'>#124283</a></li>
<li>Remove pragma library. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=febaa59d1ba0399a28ff3033130464ef1397d379'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349052'>#349052</a></li>
<li>Load correct catalog for mouse cursor kcm. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9f7570d69dc38d6f4ae68848f3006fa8a858265c'>Commit.</a> </li>
<li>Remove logitech-mouse-related code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a029fb591ae6ddba5e23c5d5874afa7f2935fbd9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124298'>#124298</a></li>
<li>[kcms/keyboard] Another non-X11 protection. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=11c21637ae36afe45e4fd387a601445ad60aa7e9'>Commit.</a> </li>
<li>[kcms/touchpad] Only create X11 backend if we are on X11 platform. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=22c075477f8677f7bfc2d89729cc7f6d9281230c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124294'>#124294</a></li>
<li>Add COPYING license text for LGPL 2 and 2.1. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dc41d66cb4422ce97c97c9ac71a0c1cd894e0bcd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124290'>#124290</a></li>
<li>Allow building kcminput without libusb-compat. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b30ea5a153bd0cc1c8381eda19ecb09abb5d1b5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124275'>#124275</a></li>
<li>Close single-level dialog subtree again when exiting through the root list. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e2314d94f8e2677ee10a47d6a0e11bbe4745397b'>Commit.</a> </li>
<li>Fix text label enabled state of leaf nodes and system model entry icons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2b382b3cf19b3fe1234a4629da6a4d9ca887549e'>Commit.</a> </li>
<li>Unfocus applet when clicking empty desktop area. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fc123ce19c8619124eac7df410e5411f5c45afd4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124273'>#124273</a></li>
<li>Explicitly look for XKB instead of juggling paths in base of a prefix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=543652303260945ba0ca0190184d3a72aa4097b7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124215'>#124215</a>. Fixes bug <a href='https://bugs.kde.org/349658'>#349658</a></li>
<li>[kcms/kcmstyle] Drop setting _QT_DESKTOP_PROPERTIES code path. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e63fb9914a5395a1e3c7125d303b4a0b4935a9ad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124268'>#124268</a></li>
<li>[kcms/access] Do nothing on non X11 platforms. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=da9836e4dc6a59c720af878066f793a0ff0fd881'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124269'>#124269</a></li>
<li>[kcms/input] Ensure X11 specific code doesn't crash on other platforms. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=889f640dc6ceaf5e6bf00d116da5a6101cd74043'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124270'>#124270</a></li>
<li>More sensible sizing for containment configuration dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3d87a19d282c51271b17a60ea17c4f9f3f5a4462'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124236'>#124236</a></li>
<li>[kcms] Delete bell.desktop again. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d1bf953821ca1782ebabebfae0f9ca1be3ce0bf7'>Commit.</a> </li>
<li>Large refactoring of the Kicker backend with new features. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=593d6556d8df43172e8cd0b102a437d8c2bd4d20'>Commit.</a> </li>
<li>Make sure the icon is always horizontally centered. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=343ed7b615c1ecd84805d2de04b7b6c2f5092240'>Commit.</a> </li>
<li>Fix a bunch of racy ReferenceErrors. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=95aff46ac65fc205beedfa7236354ac77a17fe9d'>Commit.</a> </li>
<li>Support dropping a folder to the desktop creates a folder view. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=934209ea4632927a0b84980adfe423a63b9c891c'>Commit.</a> CHANGELOGCHANGELOG: Dropping a folder to the desktop will now offer to create a full-fledged folder view. Code review <a href='https://git.reviewboard.kde.org/r/124251'>#124251</a></li>
<li>Take into account title label height. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5144bfeb3e86f63d740dcf17d7f1d119ee52ba52'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124252'>#124252</a></li>
<li>Fix Phonon Pulseaudio speaker test button sound. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=53bd3b8fc529b314b37614277834a9f407a7f251'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349825'>#349825</a></li>
<li>Update phonon test sound to use correct oxygen 5.x name. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6678237e1b901c62c5498c3770c385983292c2f1'>Commit.</a> See bug <a href='https://bugs.kde.org/349825'>#349825</a></li>
<li>Baloo KCM: Update dbus path. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=94b6aa3de9596f5cd07bd55ecc32a7096436ce0a'>Commit.</a> </li>
<li>Fix touchpad backend initialization. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3f2e6bd31e0cd7355f95450cdc185e4d37ed2c16'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124172'>#124172</a></li>
<li>Properly construct strings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e4a68b4e86ac749767142dfe733543146d79f32f'>Commit.</a> </li>
<li>Ensure we're actually finding the paths we're looking for. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f70d8407cd3e7858e37660afd404b7b89db2ec1b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124191'>#124191</a></li>
<li>Remove redundant context menu title. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=84fc89b016019a5b1812914767a3b5cc675df512'>Commit.</a> </li>
<li>Implement drag and drop for Trash applet. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dd3a50102e985fe6414953c258ca6e2659ed1de6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124112'>#124112</a>. Fixes bug <a href='https://bugs.kde.org/348378'>#348378</a></li>
<li>Fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3360a21dc2b1b3d34a7756ca2af40881b8eccdae'>Commit.</a> </li>
<li>Remove X-KDE-Library from the trash library. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cb899bd4fac645a847497df971c07a1c804970bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349207'>#349207</a></li>
<li>Unbreak for non-dir items. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=18a31434bda01c23e3f8bc24ab0d7a4a3caa8e7b'>Commit.</a> </li>
<li>Reset currentIndex and selection when popup closes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5392b77452734a8515ea1e8d71538cb26a8460d2'>Commit.</a> </li>
<li>Don't skip selecting single-click activated dir items. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8dbde6818b1e5ad1055893182ece1c3bd3a2d6dc'>Commit.</a> </li>
<li>Fix hideOnWindowDeactivate state when hiding label. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=38ffdaeb1ff00e130486e79e9d3aab4bc9948869'>Commit.</a> </li>
<li>Default labels to on. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b18ec96e9971b2387702da6dec1d8447f5513aab'>Commit.</a> </li>
<li>Add window pin tool button to the title area in popup mode. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=697e79df462766a460b90879f46b359731e107a1'>Commit.</a> </li>
<li>Account for possible title label when computing popup size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0eea6c208e0796771a18e3c9a1c648b10254a196'>Commit.</a> </li>
<li>Compute cell height in list view mode à la Kicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c191fedd4cd6f8bea55b08813e510a706d70d4ed'>Commit.</a> </li>
<li>Fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=94f8823adeda5466afc7dcb8943759d1cd533937'>Commit.</a> </li>
<li>Make panel button icon configurable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=668e2d1fd65c3ea201684575d1e9857c00a1fa50'>Commit.</a> </li>
<li>Run instead of cd on middle-click in popup mode. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c581cf93ffb25e7a9351e95a789e7b9d5c941119'>Commit.</a> </li>
<li>Elide title. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ed80ccea569b7d509be5a141c31e6e3afe9da8b1'>Commit.</a> </li>
<li>Fix checks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8ff79df7a2135ba074eb519dbacc6f4bf9f28913'>Commit.</a> </li>
<li>[kcms/keybpard] Ensure Keyboard kded doesn't crash on platform wayland. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ae08e9923d5e03ad1d766d818f3679502d588d20'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123998'>#123998</a></li>
<li>Style as list view when constrained in a popup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d37928b252e69b3def16de0576e76173f5fd9e77'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344178'>#344178</a></li>
<li>KAStats: Forgetting the deleted files happens when the model wants to return them. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5a0b9469b1ff06856893fcd0497c4708069d5730'>Commit.</a> </li>
<li>Fixing the withinBounds test for Recent*Model. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e22725c39e296ff32a0249ef87e2f5f8d913ce21'>Commit.</a> </li>
<li>KAStats: Reimplementing the result model cache to use smarter updating mechanism. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=94aeb97dcf94788d57902e8b08ee627391e98409'>Commit.</a> </li>
<li>Use variables, they're your friend. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a9863b21fff599d99ef52c0db56457ea8fea0db5'>Commit.</a> </li>
<li>Fix typos in docs. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1bf1551c91d7a880fd8451d8b3f4910e4180e7ea'>Commit.</a> </li>
<li>Baloo KCM: Explicitly tell the baloo_file process to reload its config. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dff2f72196afe5fbf314f71ffac0676d782a9ceb'>Commit.</a> </li>
<li>Split obsolete language docbook into translations + format docbook. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c86ff5303d51eff49caa2187ef5e188bc0426422'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123855'>#123855</a></li>
<li>AppletAlternatives.qml: Rename caption "Alternatives" to "Alternative Widgets". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=85cb73f50fecc29bc1b809ea972de829251f263d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123912'>#123912</a></li>
<li>Make the Forget All actions available in the root menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ca4958c3cb13feb8ae6050a7650dbde381c273bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348138'>#348138</a></li>
<li>Adapt to orientation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=214d8df746ceb9348466b3b8d440f424973e12a8'>Commit.</a> </li>
<li>Use column major in the taskbar when "Force row settings" is set. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c685b9ce1f0d567b2c2c2e27a546506d29b41114'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123738'>#123738</a></li>
<li>Always show window icons when window icons are enabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2942aa6836e3bf3a1281e69ea86b938a6bf4a58b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/323885'>#323885</a></li>
<li>All former legacy applets at least 3.0. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a20f761ac655c0dd000de515364b7dab0cafd5e7'>Commit.</a> </li>
<li>Remove sharedengine key from desktop files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1279a66bde2775e9fdddf024e1b044d7aad33456'>Commit.</a> </li>
<li>Fix wrong title on kded kcm docbook. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5e891c5a0bc196d38fa78469e0ee3eb3ede09b1f'>Commit.</a> </li>
<li>Add FindEvdev cmake module. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ec5bb558cabc3726ccd082b6675fc997af9c9f4e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347749'>#347749</a>. Code review <a href='https://git.reviewboard.kde.org/r/123808'>#123808</a></li>
<li>Fix the panel configuration width to avoid truncated buttons for long strings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ad81cae37b112b67de2a515255cf73ce6df2330d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123815'>#123815</a></li>
<li>Compile. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=38469b0f061c48c6788dead9a0aabecfe992a337'>Commit.</a> </li>
<li>Reset proxy when source model is destroyed. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e74a2e0d66645f845c691497b25742cb48141ae6'>Commit.</a> </li>
<li>KAStats: Properly resetting model when clear is called. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cdb5413c3899c5c20228307cf6da7ba8fee6cf55'>Commit.</a> </li>
<li>Fix reverse scroll in Mouse KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6f36a7afa5b83748045edba33452993034461fe4'>Commit.</a> </li>
<li>Fix vertical scaling having gotten out of sync with horizontal scaling. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=861ef07eb12a285b3a89e4bb8583ed6def0cb47f'>Commit.</a> See bug <a href='https://bugs.kde.org/347591'>#347591</a></li>
<li>Fix removable devices mounting in Kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=49a1a2827639b3099a2f16fc3c8eef6130e4d95d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123426'>#123426</a>. Fixes bug <a href='https://bugs.kde.org/346002'>#346002</a></li>
<li>Capability to load QML kcms in plasmoid configs. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5c8c965cfb12e46ca045dff4ababd3a91fda8d73'>Commit.</a> </li>
<li>Simplify. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2fb863e47cd991f597021143bc1b7f2319ee6170'>Commit.</a> </li>
<li>Support custom menu layouts and menu separator items. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8304f377ad865bdd9329c462e8734ea6c2a2ab4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347412'>#347412</a></li>
<li>Support X-Plasma-RequiredExtensions=SharedEngine. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3549a0c5e888e1f4c7971f88b8b5f35418ae3366'>Commit.</a> </li>
<li>Capability to load QML kcms in plasmoid configs. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d4e54b1b1972a09b098b426affb54ab4efa5a0c9'>Commit.</a> </li>
<li>Use new section menu item support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f3e8439d1c6bf9c7c3f69d97e63f4e7f47fa5dda'>Commit.</a> </li>
<li>Task Manager : Make the middle click action on tasks configurable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ebc796a61db738a1f955d2d50a8921c7c312f1bf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123547'>#123547</a></li>
<li>Revert "Merge branch 'mart/kcm_cursorthemeQML'". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89a5d8138098ac1989a4c820b8db2363b7442f36'>Commit.</a> </li>
<li>Capability to load QML kcms in plasmoid config windows. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f98326ac32746d6e3c56410aa405362f3af30f16'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123673'>#123673</a></li>
<li>Fix the warning. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4c508b61c434ea8c4311316e486be675df66d30c'>Commit.</a> </li>
<li>SQLite supports offset only of limit is specified. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=078e364a91de048ca5fc81e2bff1c3d01b158b30'>Commit.</a> </li>
<li>Keeping ResultSet open as little as possible and fixing the count limit. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=85de5a08c770de2a553a71704f1850c34a8e6696'>Commit.</a> </li>
<li>KActivitiesStats: Obey the item count limit for the ResultModel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=42fd69694a96b1467c0f6277bdd32012ff7ce381'>Commit.</a> </li>
<li>The KActivitiesStats library will not assert when the database is not present. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7fc71d7f4ece6d77fbae619eef32d8b6e8473783'>Commit.</a> </li>
<li>Fix displaying the symbolic (no-face) icon. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d5b7786bbd5138365b56e1f2f869c8c3c65bc15f'>Commit.</a> </li>
<li>Improve interactivity for the user icon and search field in kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bae60a355524ca6b88d87aed28d7ecb002369777'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123530'>#123530</a>CHANGELOGCHANGELOG: User icon and search field are now clickable in the menu</li>
<li>Iunset the cursor on leave. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6a96e5651d12690ca24aa94b4081980a5c34211d'>Commit.</a> </li>
<li>RemoveClicked->removeTheme(int). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=58d6b6aea6b48595345185b256962b513c453296'>Commit.</a> </li>
<li>Inline preview widget. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=843e63e90136b0d73f151bc26e60f7eec96fb8ba'>Commit.</a> </li>
<li>KActivitiesStats: Filtering results on mime types. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3e6e34c4b19cbfa452f76938fb476a01533ac243'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9eb2dee3312b66eff36b4ddb362a8fdca6ec6474'>Commit.</a> </li>
<li>Ome keyboard navigation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=29550e46f233c6054c4081bf223cfcc66d568a8d'>Commit.</a> </li>
<li>Remove references to libusb where not needed. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6cedda2a00accdf1d74ce84d4ac83916a6edd3c1'>Commit.</a> </li>
<li>Initialize with the correct theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=314aaddbe545671672f1f48a6b96cf09a5c85ea7'>Commit.</a> </li>
<li>Make the RowLayout the root item. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fd1a8db9515dd7b39ca2cbd83ce508d5685ca4c9'>Commit.</a> </li>
<li>Catch qml messages as well. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5cbbe8299cbc256df11156e9cdbd1f2c4407581f'>Commit.</a> </li>
<li>Use isCurrentItem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=86bd110dc0a3f1a6780c24db5c4fc0cc0807e1e4'>Commit.</a> </li>
<li>Remove dead code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=df81f2e073195e682c553d85d3f3e40a5310fa53'>Commit.</a> </li>
<li>Refine and separe the delegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1281df733b1a58a71c5bf9da522daf17e20a771b'>Commit.</a> </li>
<li>Restore HAVE_XFIXES and immediate apply of the theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2dd6be87552225fbfe710a665513ab39478dbdbb'>Commit.</a> </li>
<li>Tweaks in the list layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5c433a028e7bf6ab8f75123013de9d838f794acd'>Commit.</a> </li>
<li>#Please enter the commit message for your changes. Lines starting. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a96bc110ed8e9f26ec1a3ff48645d97f7c53e970'>Commit.</a> </li>
<li>Apply the cursor size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6da5de1ad85b251caaa712535e7db1b98eaaed00'>Commit.</a> </li>
<li>Get rid of the old delegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=be6fde09fe950b5f08675215bc73daa4fbe6f7bb'>Commit.</a> </li>
<li>Size hints. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bee6c1af2475cb72beb60f847a74a72dfebd89b1'>Commit.</a> </li>
<li>Make the preview widget a QQuickPaintedItem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=669ae86970fdcc74f44944dc7511f04b1540ddee'>Commit.</a> </li>
<li>Correctly update canRemove. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ddb38bbda2bd5d21cbc334a25a51063887f33a86'>Commit.</a> </li>
<li>Add size combobox and install/remove buttons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6e81f178f5deb36d991f2660a0c3eaa4b8447276'>Commit.</a> </li>
<li>Move themepage logic in main kcm class. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=082582b96b29b7349d074e5d72cf6ecbe2d6a3dd'>Commit.</a> </li>
<li>First non working QML port. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7eb51e136174ce4dbe2484987f8a16bdaba6905f'>Commit.</a> </li>
<li>Split the cursortheme kcm out. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d9f5cdc9c9308a2e711a08a6b6ce9981353044ea'>Commit.</a> </li>
<li>Protect X11 calls in the Pager plasmoid to prevent plasmashell from crashing in Wayland. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dd635224de346c1e87a68139301128834a6f0677'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123411'>#123411</a>. Fixes bug <a href='https://bugs.kde.org/346258'>#346258</a></li>
<li>Apply button should become disabled after the pending changes have been applied. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2d1ffb563abe97c30aacdc456d60b6a571874a28'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123455'>#123455</a></li>
<li>Message dialog should always show if we have any pending changes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8d6a08416f95ce8b346d205fd4bdab3692ce24de'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123448'>#123448</a>. Fixes bug <a href='https://bugs.kde.org/346431'>#346431</a></li>
<li>Provide a preview of both long and short datetime formats. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d27d79d1271a0ed52994cdeb5bc72bd4d7715417'>Commit.</a> </li>
<li>Unbreak "Revert" function. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d4a619a177f915c12feb5c7cf79d56aa1c765d61'>Commit.</a> </li>
<li>Revert d7578e55915b95c7849d3c348b75eaf4612f7694. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d51418f5938476f3067a28d044bc480a209526c5'>Commit.</a> </li>
<li>Fix non-functional Q_WS_MAC. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=184cf399f70051be10acd2c643a226cb5074b4b6'>Commit.</a> </li>
<li>Size cotainment size after drop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9c414ca53f47c7b0197b963ecdb4fc3f577f863f'>Commit.</a> </li>
<li>Update spellcheck docbook to 5.3. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5a38597cb3a943524b5307a81eb4a1559021b7ab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123318'>#123318</a></li>
<li>Remove references to the dashboard. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d03739fcaf369ea14fd0997e04c67ddecd580f0b'>Commit.</a> </li>
<li>Modules can be normal items now. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=205ccf55ba5bbceb184acabe70f7d7a091b9378d'>Commit.</a> </li>
<li>Fix typos. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3fa3c76f02a7355ad18d01e6a8384b7bc7d679ba'>Commit.</a> </li>
<li>Remove obsolete guilabel markup in title. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ec3e3c9382d47a9f68d275143a280355475ab95e'>Commit.</a> </li>
<li>Don't add launchers on internal drops. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3fe222ba2761276d329414589ddc112b8436dc48'>Commit.</a> </li>
<li>Add launcher when DND'ing an app from launcher menus to the Task Manager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6adfb466d1322feaec8352a477313982de496097'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346055'>#346055</a></li>
<li>Disable Help button for Search kcm. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=82fe2aa26745d1ba1741383f261a060b5b8f4358'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123319'>#123319</a></li>
<li>Disable Help button for workspaceoptions kcm. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=95037a699d13200388d0c03a4b0f42a0d852ae5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123317'>#123317</a></li>
</ul>


<h3><a name='plasma-mediacenter' href='http://quickgit.kde.org/?p=plasma-mediacenter.git'>Plasma Media Center</a> </h3>
<ul id='ulplasma-mediacenter' style='display: block'>
<li>Fixed artist removal; fixed careless mistakes. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=0b460e473dde1f4cb7366a0c632d898749b3f930'>Commit.</a> </li>
<li>Fetch album/artist image only once per album/artist. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=99f6e1843e0282e645f4ecd48fbcad36400f3a1c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124379'>#124379</a></li>
<li>Added removeMedia to  medialibrary. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=b9111a4c917d595454c6a8e71b134bfb5335d174'>Commit.</a> </li>
<li>Fix build error in online services backends. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=392c2a67c0a569422b3e9536f52354608f205de0'>Commit.</a> </li>
<li>Don't report played media files to KAMD when running autotests. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=7e972198cf2b55f9b14befdd8a9c0c84a32f4b39'>Commit.</a> </li>
<li>Notify kactivitymanagerd about playing media. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=c0d6d24d7d18fbdc24aa16087d7443c9cd734a3f'>Commit.</a> </li>
<li>Disable logging message for the last.fm image fetcher. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=8214220e423775550951168f4986d6b90f3705a7'>Commit.</a> </li>
<li>Add CoverArt uri to the metadata exposed with MPRIS interfaces. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=8c416587adc8780639d3bef02c16a198f58b16f7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123467'>#123467</a></li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=6eb54f7a33b0e07b031f9ec94ea4196972d57d05'>Commit.</a> </li>
<li>Remove more unused code. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=976c6baeac68ab569c2d5b61e88a43c8a3ee7110'>Commit.</a> </li>
<li>Remove unused code. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=57d255529d6bf27e91bb2c47afdf86caee221df3'>Commit.</a> </li>
<li>Write settings to plasma-mediacenterrc. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=b37e33ba0017637a09190b7bab390502f1f7b70f'>Commit.</a> </li>
<li>Warnings--. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=c73b2c38fd174837a3dfac28cebed10249f9ac48'>Commit.</a> </li>
<li>Fix crash on startup of plasma mediacenter. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=f812ffd525875a9c09b15e962adc7326bf727204'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346080'>#346080</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Allow opening Connection Editor from plasmoid context menu. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f261487a28ac4a2050ad2954e43277615eccba76'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124510'>#124510</a></li>
<li>Allow removal of multiple network connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d4afbc7a56e59ec285b94b483bd7ae7c01135bcf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124494'>#124494</a></li>
<li>Make sure that expanded details or password dialog become fully visible. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ac8cf3cc6f6db84204f8aa440f1629104fc82088'>Commit.</a> </li>
<li>Remove the old C++ based traffic monitor. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=a5276cdcc280fec91fcf599da72a9b42d80e46b8'>Commit.</a> </li>
<li>We can ignore generic connections for setting systray icon. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=06244d4aa8ddfc4c0ee30f33e1d36a25537d3c6e'>Commit.</a> </li>
<li>Add some info about how we steal code from other applets. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4ad74a7bad19a6c4b1e568dcf182d79bf0804d74'>Commit.</a> </li>
<li>Add traffic monitor. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=e20839806450cd018de2e7a24fb23a54a70fe6da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124485'>#124485</a>. Fixes bug <a href='https://bugs.kde.org/333070'>#333070</a></li>
<li>Workaround for notifications stealing focus of the connect/disconnect button. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f1ae6f60769876e5f7ddc0c710f5dec9f862a6e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124314'>#124314</a></li>
<li>Do not display "unavailable" icon in systray when there is an active virtual connection without default route. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=382e7ac4d15722318ebfc3db657fc18bed07d345'>Commit.</a> </li>
<li>Make loading of secrets in the editor asynchronous. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=2d7a689b79d6ca9752ada7ccdc1e635149d2ba36'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124279'>#124279</a>. Fixes bug <a href='https://bugs.kde.org/349002'>#349002</a></li>
<li>Add support to import .ovpn files with syntax described in. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=599afdacd744a2d7785274687438a23f21617c34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349282'>#349282</a></li>
<li>Make Toolbar highdpi-fit. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=7d5baa004871e581ca45fcfcc422d2322449a33d'>Commit.</a> </li>
<li>OpenVPN: Do not overwrite modes already configured in .ui files. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4b3d6bb063f7289185fef3ddcc41d2d6e67c0c15'>Commit.</a> </li>
<li>PPTP: Fix storing of secret flags. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=7607930c892021c6bd407bd8d63706ba1dce8e1a'>Commit.</a> </li>
<li>OpenVPN: Do not insert translated value for remote-cert-tls. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=0e0fa8fb37d05550db3e54877910daba876e980d'>Commit.</a> </li>
<li>Adjust SSID/BSSID combobox to minimum contents length. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=cee8daaedf35fce024964f8c70853c33933e8367'>Commit.</a> </li>
<li>Looks having same name for signals and slots doesn't work well when using Qt5 syntax. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=b969a89ae0516c8aaa6f994f5646736cf57a2af6'>Commit.</a> </li>
<li>Add SSTP VPN plugin. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f39cd2eac71ee630f542dc890c5a26ef287ac423'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338521'>#338521</a>. Code review <a href='https://git.reviewboard.kde.org/r/124051'>#124051</a></li>
<li>Use the colorgroup. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ffa16e163632019ee83c903c5a93013c58d9c82a'>Commit.</a> </li>
<li>Fix typo in END_TLS_AUTH_TAG. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=1bd9f0fd4a777f5be7d020c25dc5e3781fc09706'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347416'>#347416</a></li>
<li>Drop WiMAX support for NM 1.2.0+. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=c3004a42dea04efed319e18da6e89711afae86ba'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123657'>#123657</a></li>
<li>Simplify "show menubar" handling. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=40e635e6b18aecba4e94e763bd46d092ae842ea2'>Commit.</a> </li>
<li>Enable IPv6 setting for gsm/cdma connections since NM 1.0.0+. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=48d97a1b97c1098cd3d62412a7f29e858ed6d7f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347164'>#347164</a></li>
<li>Editor: request secrets when "secretkey-flags" is not present in setting. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d195517e492c3b37a09cef590a4cc9814d3fc008'>Commit.</a> </li>
<li>Add option to show/hide menu bar. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=6201160d81236177fe1634270bfc9dc562288d1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347141'>#347141</a></li>
<li>Add SSH VPN plugin. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d76fb3dee7ed20545ae1bb3b2a2d2e78517f6698'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341070'>#341070</a>. Code review <a href='https://git.reviewboard.kde.org/r/123465'>#123465</a></li>
<li>Include the required QObject header even when modemmanager is off. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=cfbee564aaf0e9a8087dd53bca8cb397e71bf260'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=8bc787d1c9b493c163f45ba6ca535cbeda6ff5e5'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=64542fa43453a5b22062a3e5710aad80e842e050'>Commit.</a> </li>
<li>Use Qt5 syntax for all signals/slots. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d418067e5a21b8596df79e251e66abd82d93990f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123474'>#123474</a></li>
<li>Searching for ModemManager shouldn't be necessary. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=649345e7868c36ca0e78d85a347336d25a65a413'>Commit.</a> </li>
<li>Coding style. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=48dc7903f1825fcde8edfacb589c63fe322679f9'>Commit.</a> </li>
<li>Editor: sort available VPN plugins. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=7e0535fe2bdfa5a6e3f6070ef9e142a85df43ac9'>Commit.</a> </li>
<li>Include camel case header to avoid an error due to a nm-qt change. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=433779ebc6c4cc09511d4a221b94bd04cc7eae36'>Commit.</a> </li>
<li>BluetoothMonitor: Simplify addBluetoothConnection + add bluetoothConnectionExists. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=b7ddf942153c1d6757361aa90c16decc19cc6345'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123388'>#123388</a></li>
<li>Do not display busy indicator for temporary active connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=28d786928dc9b223440815df47360963116c419f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346247'>#346247</a></li>
<li>Fix regex to allow also accented characters in password field. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=46800e0c1a868a675591af5cdac4ced6e4a95ad6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346026'>#346026</a></li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a></h3>
<ul id='ulplasma-pa' style='display: block'><li>New in this release</li></ul>
<h3><a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Fix plasmoidviewer screenshot function. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=2e510e221b8d44e2c9de63a6d28f526613a630b8'>Commit.</a> </li>
<li>Install .desktop file for cuttlefish. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=75579fb9ef7a49fe351356d103b6df32632b0011'>Commit.</a> </li>
<li>Add .desktop file launcher for Cuttlefish. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=b9a857eed3a9e43eae3a21e41b3461e0af2498b6'>Commit.</a> </li>
<li>Fix translation. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=032c098d8b756a5214f22227b339172afaa81286'>Commit.</a> </li>
<li>Fix alignment of filename in certain cases. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=89ef07b7e25e75768a242ac264fd30a7bd6ef8f8'>Commit.</a> </li>
<li>Remove unused files. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=f4331f3e45c744e68ac27027f3d15a5ee5b98a61'>Commit.</a> </li>
<li>Fix typo in Messages.sh. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=5c1edc0bf51ebaee9ddf4af12515227e14303ad2'>Commit.</a> </li>
<li>Install the desktop file of the app. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=cf1a0a64e275085d99e499a0cbc57daf6c026e71'>Commit.</a> </li>
<li>Add KDBusService. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=56fa068ad9a84ea175b6b83867b76da873df8d00'>Commit.</a> </li>
<li>Use i18n properly. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=cb0fe0139f006dfe6a28c388d6655f6feaaa8649'>Commit.</a> </li>
<li>Remove c-format from messages. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=ea4a61b9499421d229b3034baa9a2b739d5b127f'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Filter applets by formFactor. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=07c46ebe858d471a5660cb5d39cfb12342214100'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124634'>#124634</a></li>
<li>[digital-clock] Bring back font family option. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3e58a5210425dee72df80817c10133551f591acc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123877'>#123877</a>. Fixes bug <a href='https://bugs.kde.org/348072'>#348072</a></li>
<li>A very basic ui to add custom wallpapers. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=67bec945677730d02a50e9ff6ed903b86e2eac2f'>Commit.</a> </li>
<li>[digital-clock] Move unrelated code to its own slot. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fa01d9d8a6d10df7c40f2f48bb85516c93a7ed1c'>Commit.</a> </li>
<li>[digital-clock] Add a simple option to use 24h-clock format. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fc66aabb2e597fb4b2432a80d4acaa41f305197'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124453'>#124453</a>. Fixes bug <a href='https://bugs.kde.org/345378'>#345378</a></li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5d37c4ff95659d260b4a657087fe8c098ddfcec4'>Commit.</a> </li>
<li>Don't trigger finished() at process deletion. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=285a863cd94a57a1593aa9a9f1d8b42a59e7bd52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350832'>#350832</a></li>
<li>[digital-clock] Fix double slot. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1c7932de1d276bbb8b741d09b244c1bd0f13cfa7'>Commit.</a> </li>
<li>[digital-clock] Add support for ISO date format (YYYY-MM-DD). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3d241158ff42fb484eac1d5162d71331e9527886'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124451'>#124451</a>. Implements feature <a href='https://bugs.kde.org/348080'>#348080</a></li>
<li>[digital-clock] Introduce new layouting state for small panels. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=33395e8e4200a5d61e834cdd44a4088ef1b28e4b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124449'>#124449</a>. See bug <a href='https://bugs.kde.org/348072'>#348072</a></li>
<li>Clean up error message text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=031f74cbb69a1b6c57842b2510448348b442d9b7'>Commit.</a> </li>
<li>Show a dialog if Plasma failed to create an OpenGL Context. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=727852897203fb750d9a06f04b78b07527573948'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124576'>#124576</a></li>
<li>Ksmserver: restore support for autostart scripts; migrate them from the KDE4 dir. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e33b582fda5588762aa50e3ad619283b97e7d758'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338242'>#338242</a>. Code review <a href='https://git.reviewboard.kde.org/r/124534'>#124534</a></li>
<li>Port to QStandardPaths and use setTestModeEnabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=03d7f8f01553af1c35f8f02d3b70e821f68d7727'>Commit.</a> </li>
<li>Fix porting to KF5. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d349476f3978d117368fcaebaac8421eb16821c8'>Commit.</a> </li>
<li>Go back to the user/action selection dialog when returning from switch session. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1c5c25a8d7301caae5152ae9c8d83dd1610e7317'>Commit.</a> </li>
<li>Fix typos. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=175bb86fba5b1fae53fcb047c283df18d443a629'>Commit.</a> </li>
<li>We only need the Sleep States source. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=86ef7f0ff68843c0297612bdf5893ca381d98361'>Commit.</a> </li>
<li>Only offer and show hibernate/suspend options if we can actually do that. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1894aa98a9f9eec9c1ed32b933752e67e91afd04'>Commit.</a> </li>
<li>No pixelated icons for DrKonqi on HighDPI, the crash pixmap still is pixelated, thought. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=43e502da462ce7eaf6f21579168e9df837f4ee25'>Commit.</a> </li>
<li>Update klipper docbook to kf5. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bf22a10eafa59f9671e89b99c35113da2aa7b955'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124452'>#124452</a></li>
<li>Fix race conditions in WindowsRunner, as found by helgrind (and code review). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=25cee4386cfd17e0b786f3ce48532e873a0f663a'>Commit.</a> </li>
<li>Simplify. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5b98f75a900476e76b7d34671eafeb79a138ea42'>Commit.</a> </li>
<li>Show history only when pressing the arrow keys once. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=733fec5650a4ccd8de2a179e7f90bed3eca08f6f'>Commit.</a> </li>
<li>Store up to 50 history entries. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a9378b6e086a46dab6cca39ef88db6bec27367d7'>Commit.</a> </li>
<li>Enable auto completion of history items. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1945601c763225aa4cc4a327fb746f36dc755d53'>Commit.</a> </li>
<li>Restore FreeFloating options. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1ce88d0ff2ffad34c4458e296dcae1f4e296a00b'>Commit.</a> </li>
<li>Bring back KRunner history. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2d2a79390d8bafeda8dc1ce30fd4bc40eafaea57'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123888'>#123888</a></li>
<li>[startkde] Add KWallet5-PAM handling. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cf9c02629ee6968e5cb57f34ca301a77722ba66e'>Commit.</a> </li>
<li>[osd] Change the height based on feedback. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=43d7e0c6b1e6aaa9037bd8ff5b0fa4e8538ba397'>Commit.</a> </li>
<li>[osd] Make the inner margins smaller. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3b68c74d756790842e9627f6d3b2bd455b3bcede'>Commit.</a> </li>
<li>Add label for broken battery. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8271192eca35061aca6f75970e7d10457655a978'>Commit.</a> </li>
<li>Make icon widget initial size dpi aware. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b4d0b42f597726b33c936367feef0118f8e382d9'>Commit.</a> </li>
<li>[osd] Make the osd 1/6th of a screen at most. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3956cac47d2a15184b0ecb4ca84049b2ab6b1870'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347123'>#347123</a></li>
<li>Disable devicePixelRatio in PlasmaShell. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=558e123d66f839ceb1ed32e25d04000f6cb38f5a'>Commit.</a> See bug <a href='https://bugs.kde.org/347951'>#347951</a>. Code review <a href='https://git.reviewboard.kde.org/r/124463'>#124463</a></li>
<li>Don't crash when OSD QML file is invalid. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fb3a6b5a2391326b7a5a1181e89ad988d7c00a02'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124415'>#124415</a></li>
<li>[digital-clock] Simplify the sizing logic. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d6edf888ea432a068d8c67d1dce8bbffe41798d0'>Commit.</a> </li>
<li>Bump required frameworks version. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=19da19c9ae6a00fdf60117ec1a2f9bdac30ef5b3'>Commit.</a> </li>
<li>Add sanity check. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4990f293a757878a6966f3fdf1d087506daaa136'>Commit.</a> </li>
<li>Revert accidental change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=61cf54a445961d606f330b606f82c9838edb6264'>Commit.</a> </li>
<li>Trigger icon update. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6224ed1fb28aee9dafeb16944216f899d976e506'>Commit.</a> </li>
<li>Add an option to use the matching launcher icon for task items.. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fbd4a876f32f402ae240450c4c7d8575391637b9'>Commit.</a> </li>
<li>Begin fading the OSD immediately. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ca33e8ee69eab68b024014509675d33f490b91a8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124409'>#124409</a></li>
<li>No need to move to another index if we're not deleting the current item. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=218b8d35174e2eeb2779343a06742d334b5a3096'>Commit.</a> </li>
<li>Select a different wallpaper when the current one is deleted. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c3ea4099c2c8e3703a375bad121e9ed53dbff805'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350414'>#350414</a></li>
<li>Add X-Plasma-NotificationAreaCategory for applets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c0d20b52d8c4a187b67c59ca10e70432eab31f01'>Commit.</a> </li>
<li>Allow plasmoid to specify X-Plasma-NotificationAreaCategory. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=85450ade844e1d1657028c6db9ea96f2a2e7af1e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124382'>#124382</a></li>
<li>[dataengines/notifications] Honor the timeout setting for notifications. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4c2d576b0715a16d4ea8e457ead079f204b466fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350164'>#350164</a></li>
<li>Make System Tray use a ProxyModel rather than tree independent models. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e64d41cc2fe549e4efcb60c7561dd83648294903'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347554'>#347554</a>. Code review <a href='https://git.reviewboard.kde.org/r/124380'>#124380</a></li>
<li>FindQalculate: Use FindPkgConfig instead of UsePkgConfig. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8b3f6ac7a959300803334f6959184a6df34360f0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123557'>#123557</a></li>
<li>Test if signals are defined before using them. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5e88bb70f18571f4e4a429ee282056f49892a237'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124395'>#124395</a></li>
<li>Fix transfer speed label. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a9c690c9530e3094d8efe90042444a483f92d236'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350378'>#350378</a></li>
<li>Prevent possible endless recursion in PanelView event handling. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=31b0bf7f199113c73873c451ad0d740072cbbda3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124375'>#124375</a></li>
<li>Require KDE Frameworks 5.12.0. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=36886a4ed89ddce163b442e25fd78596fa8e3166'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124389'>#124389</a></li>
<li>[dataengines/notifications] Fix undo notifications being grouped. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4bdc997f149369a28328f652b5ee24ecc18d4092'>Commit.</a> </li>
<li>Remove useless debug. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a905b9eb1f63edb9ee77e04cba85a95914c2fc85'>Commit.</a> </li>
<li>Add a startplasmacompositor and startplasma file. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d058ac79b561ece0a0854894b0ef14eb041794e5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124293'>#124293</a></li>
<li>[ksmserver] Force QPA platform xcb. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5581b0d872d0fbcf7f7e979512f817f8898f848a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124292'>#124292</a>. Fixes bug <a href='https://bugs.kde.org/346427'>#346427</a></li>
<li>[shell] Add Wayland support to PanelShadow. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=152ceeb553b3a2663b5320b350327dba18a50090'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124360'>#124360</a></li>
<li>Show plasmoid / application icon in system tray entries config. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fd68001fda114d65edb5bc7c307857802cb79206'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124342'>#124342</a></li>
<li>Clear view lists in destructor. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4942878b67c6a5ae82df11264eb751944cea4a69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348511'>#348511</a>. Code review <a href='https://git.reviewboard.kde.org/r/124323'>#124323</a></li>
<li>Improve wallpaper rendering. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=29e39fe58aed5ef3b9155fbf96227eea3c7dddb2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124311'>#124311</a></li>
<li>[ksplashqml] Delay closing stdin, stdout till app is constructed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=56d2c15b9acb9c4b57398b281685807c3191f622'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124317'>#124317</a></li>
<li>[ksplashqml] Only use X11BypassWindowManagerHint on X11. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6fffe4963d14a27668c9346d61b1217bccc93e24'>Commit.</a> </li>
<li>Add option to always show all system tray icons. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1b7e63e80b9528c91ac82afeeef9e99979f620d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349812'>#349812</a>. Code review <a href='https://git.reviewboard.kde.org/r/124234'>#124234</a></li>
<li>Round notification icon to icon size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a06269e0b178dbbd14bed160dfd63216f8f7d828'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350040'>#350040</a></li>
<li>[screenlocker/greet] Make greeter work on wayland platform. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2c52923178749de715e8a808d7d1a7b54c8514cb'>Commit.</a> </li>
<li>[screenlocker/greet] Don't crash on platform != xcb. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fe63e2dc39ddf3a4fdb6a47d060aab0ec42c422a'>Commit.</a> </li>
<li>Prevent two wallpaper images showing overlayed when "Scaled, Keep Proportions" is chosen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6326f09e9b6111a6048f2fc8191f04e8486aaa00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345405'>#345405</a>. Code review <a href='https://git.reviewboard.kde.org/r/124093'>#124093</a></li>
<li>Move desktop kio slave from kio-extras. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6f94dcc98ffa31936dfe4777f9b2d3f3e77f1d65'>Commit.</a> </li>
<li>Fix regression of plasmashell executable not showing version. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=24052f0b268e308ca96dfb95342c5778cdaf7799'>Commit.</a> </li>
<li>Add screenlocker docbook. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d9adb0b3fccf4e2c367619a13923b0cea08625e8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124250'>#124250</a></li>
<li>Use upcoming version of libdbusmenu-qt. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3d0a9cd462a7ab256b65dec7ed2035e0d7658818'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343971'>#343971</a>. Fixes bug <a href='https://bugs.kde.org/345838'>#345838</a>. Fixes bug <a href='https://bugs.kde.org/345933'>#345933</a>. Code review <a href='https://git.reviewboard.kde.org/r/123992'>#123992</a></li>
<li>Cleanup lock logout settings. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fdff218d1c21644a1f225ed09f35538ce59d06c1'>Commit.</a> </li>
<li>Set visualParent of the QueryDialog. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0e6c41f83e6bad3ebfd3832eb5b8bed7fc6d38ca'>Commit.</a> See bug <a href='https://bugs.kde.org/349887'>#349887</a></li>
<li>Don't export QT_PLUGIN_PATH. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=be1c5fbfbcb436ef701799e6f1ebbabf8eca6f7c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349371'>#349371</a></li>
<li>Simplify. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d4a8ac5920af412c23277990bade049d469c2fc1'>Commit.</a> </li>
<li>Update plasmoid status when data in the pmSource changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=33730d9094d2c170745a697fd24bf8fe0fd632d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349370'>#349370</a></li>
<li>The powermanagement engine reports "Ups" not "UPS". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7338a98cb0365a7857538d16d96a754c64d65bac'>Commit.</a> </li>
<li>Fall back to averaging percentage when there are no energy values reported. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a2a5b9bd82dcb738594b8e6cf7d663e841325c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348588'>#348588</a></li>
<li>[shell] A desktop window should be frameless. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=de81f492ade74091abca352dee35538cac2088f0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124210'>#124210</a></li>
<li>Allow title and artist to wrap into multiple lines. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e662a4b823faed5c94600dc4e281800f046d4f6f'>Commit.</a> </li>
<li>Support platformcontents from the package. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=595366b0ce24df6bff43044b92fdb9c7c2c19310'>Commit.</a> </li>
<li>[lockscreen] Select wrong lockscreen password later. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=044f1bcce8bb769a41d3aa4f38aee9b64e608d30'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123991'>#123991</a></li>
<li>[calendar] Disable the Agenda config. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2687497938cbebe83c64919c72bc6b4848c09f7e'>Commit.</a> </li>
<li>[calendar] Add week numbers to Calendar applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b665752aa1045c2702b7b5b2983fe21b90b6b565'>Commit.</a> </li>
<li>[digital-clock] Add week numbers to calendar. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a500d06431fbe795374eb746a7e64d9b4addbe2c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124005'>#124005</a>. Fixes bug <a href='https://bugs.kde.org/338195'>#338195</a></li>
<li>Set window type on Wayland for DesktopView. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=548686efff8c1057298b5c0767df43757d8adf39'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124057'>#124057</a></li>
<li>Set absolute positions on PanelView on Wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aab431e7fe010060373ee1744b4f60792038b005'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124056'>#124056</a></li>
<li>Add support to mark the PanelView as a Panel on Wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e7deda80ed7decb913cbbec6124521c56cf6b58f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124054'>#124054</a></li>
<li>Fix conditional build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b7628a8d72bb04050f1e2dfdf5d9e0ed4fd5bbf0'>Commit.</a> </li>
<li>Conditionally link to the int console. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=dcf1db72a2a09a193c67ec7404195ffdcae4021d'>Commit.</a> </li>
<li>Add "Adjust Date and Time" entry to digital-clock context menu. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f6e680825affb1e623dac433d260207271c30ff8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124021'>#124021</a>. Fixes bug <a href='https://bugs.kde.org/348738'>#348738</a></li>
<li>Wait for kamd to pop up before actually loading. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c1d2e21cfee703595f3d30e47057a60ccb0c50cd'>Commit.</a> </li>
<li>Fix createActivity not respecting shell package defaults. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3e42d6ae989e036ccdb19186aa09e06abcf7c9cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348617'>#348617</a>. Code review <a href='https://git.reviewboard.kde.org/r/123990'>#123990</a></li>
<li>Fix and simplify i18n. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5dcd9bb7ee1559c9fbda0397796c5ac455b8aed0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/337682'>#337682</a></li>
<li>KRunner: hide the window on losing focus. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c81b745e5be30d426df22f2ec71d0c58057bdb9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348431'>#348431</a></li>
<li>[ksmserver] Check the 'Action' for 'Sound' before playing. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2e60c27a85bd8c785db47be176f6b78a2d3c4ba8'>Commit.</a> </li>
<li>No horizontal lines for systemmonitor widgets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6f4a13e547d6d238f5c05f779ecf90578097759c'>Commit.</a> </li>
<li>SystemMonitor: Always center the dialog on the screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2b56c3b02e8f41417ec85ecf3683159bae0bcebe'>Commit.</a> </li>
<li>Bugzilla Integration: Look for the mappings file in the correct location. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b67f430134262287b25058548783b985bdd0ac5f'>Commit.</a> </li>
<li>Limit the scope of what we're capturing on lambdas. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e64e42d51e73989c02eb0ac1d9e3bfd8313aeef3'>Commit.</a> </li>
<li>Switch the login sound to Phonon directly...for now. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c445f99304bd0190a7a0b83e4700222132bc34d1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123834'>#123834</a></li>
<li>Improve SDDM Experience. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0eb746cc8c357a626f8d3a80e336a8a48d1654fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123688'>#123688</a></li>
<li>Fix compile warning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d635b18fd7824e1eea089b5722aa3cf6942ade2e'>Commit.</a> </li>
<li>Add menu to statusnotifiertest. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=dd325ad3ab1a5cc19d0de0017ac66e85d7f295ef'>Commit.</a> </li>
<li>Check all shell directories. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f59164914c46eca3686a3ade63f46bf6489f9193'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348056'>#348056</a>. Code review <a href='https://git.reviewboard.kde.org/r/123870'>#123870</a></li>
<li>Watch for extraItems config changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=39280d242ce57aa9313b8582619506bd3e115f19'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347858'>#347858</a></li>
<li>Process updates scripts even after we load the default layout. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=67d0aba0a17f659ab89d61fcc9a8a6ef8de47382'>Commit.</a> </li>
<li>Remove the sharedengine extension from desktop files. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=77d18c9877e3e163a82ddef5f83a6f75fccbb8c7'>Commit.</a> </li>
<li>Revert "don't call init() two times". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=09ae19f0a6a9d2ac2f8001f35b9991ea5cfea1db'>Commit.</a> </li>
<li>Default to desktop sorting. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a79935a065748b79bcf29c01f095c37a3f03f8df'>Commit.</a> </li>
<li>++paranoia;. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7e2a5d0d7e142f887f12dcfefb0885e241fe3e32'>Commit.</a> </li>
<li>Don't call init() two times. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d0801854aaf2f39fdba911d14b4222abe7e9e9fb'>Commit.</a> </li>
<li>Use ContainmentView. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bfee72c1b8519d7f122b97d743a2ee6b7dd16b99'>Commit.</a> </li>
<li>[klipper] Highlight leading and trailing whitespace in the plasmoid. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4fdfcded48f222a2aa1f8032142d8191f0624f5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123821'>#123821</a>. Fixes bug <a href='https://bugs.kde.org/159267'>#159267</a></li>
<li>Device notifier: Refresh the free space indicator every 5 seconds. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=44f957af3ef3d786012a70167e5cd170df0d5440'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123817'>#123817</a></li>
<li>Make writing of panelview settings work during startup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=11483d2985c9b3c8314f2c6d8c8d7ed30a4ad0be'>Commit.</a> </li>
<li>Make sure we have the proper default containment. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=49e3e04dca9bfd4ecb247677046c537df8154e13'>Commit.</a> </li>
<li>Remove references to a no longer existing DPMSconfigUpdated signal. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1e22c761babc2028b6961fe2b51508dab74a5f6a'>Commit.</a> </li>
<li>[ksmserver] Bring back the login sound. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b0fe26bfb314698693fcf011e59a28769bd9a437'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123770'>#123770</a>. Fixes bug <a href='https://bugs.kde.org/335948'>#335948</a></li>
<li>Automatic add new installed plasmoids in systray. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b494fe13770f25058179f93ab7cf355517078166'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123789'>#123789</a>. Fixes bug <a href='https://bugs.kde.org/345872'>#345872</a></li>
<li>Check plugininfo validity to guard crashes at startup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8a6c0b00979d235f68c46de761ba030848591247'>Commit.</a> </li>
<li>BalooRunner: Baloo/Result header is no longer installed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=321cdc7278c29bcf4cf15078a71c8ceea47e7e95'>Commit.</a> </li>
<li>Add all X-Plasma-RequiredExtensions=SharedEngine. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=619aaa9fdd2e83ab3e6756ebfe70631b20bc1c84'>Commit.</a> </li>
<li>Remove dead code of plasma1 systemmonitor applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cb65139eee1c67af1dc8d9b22036dbf4d054085d'>Commit.</a> </li>
<li>Move solid/bluetooth to Bluedevil. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a1e50a2308e0b6936f05657eebc26f7fbb269147'>Commit.</a> </li>
<li>Remove support for dbusmenuqt <4.0. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=32b230bbec98eb96badf081cd528ee71e6b107dc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123633'>#123633</a></li>
<li>Find DBusAddons component explicitly. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e215432a039c3681d9246ce621f8f50382acc39a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123664'>#123664</a></li>
<li>Baloo Runner: Use the filepath as the unique id of the match. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=34c11a481baafd9824ef681b6d359ca34ab04ad8'>Commit.</a> </li>
<li>Don't hardcode oxygen fonts here. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=842fb93d07c87b9ad4c2ad735f5601f0bde03017'>Commit.</a> </li>
<li>Use qCDebug. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c3c752d71f9d676c2c4360a789feab8fc8cdc2e3'>Commit.</a> </li>
<li>Port systemtray away from sycoca. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=071a7884ab8ef5764e107fea4f3bc78a4a176b2b'>Commit.</a> </li>
<li>When switching activities, use only the running ones. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ddb7b332e53e8eddaf8d9e639ca45f233702a86b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347029'>#347029</a>. Code review <a href='https://git.reviewboard.kde.org/r/123597'>#123597</a></li>
<li>[notifications] Clear notification from show queue if it's closed before it's shown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e1388789ffc5239587f88ec29bc0fa83bbeadfa3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/342605'>#342605</a></li>
<li>[notifications] Always check first if the dispatch timer isn't running already. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=08415c47d9c3c6a322b4fcd8403fc275b8f0ade2'>Commit.</a> See bug <a href='https://bugs.kde.org/342605'>#342605</a></li>
<li>Port away from sycoca everything possible. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9846f4b0afa9601ee2d56a504581b718536ce1b7'>Commit.</a> </li>
<li>Global locale and language variables. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3ecffbfcb012a9dac1cd6eda7d454e6e4e24d3f7'>Commit.</a> </li>
<li>Visible error messagebox on fatal loading errors. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9e5f357a89273b39c9897fa6bcbb211a746f4aca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346792'>#346792</a>. Code review <a href='https://git.reviewboard.kde.org/r/123535'>#123535</a></li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=eacc660109458e345fae3a5143a7744aaaf6103c'>Commit.</a> </li>
<li>Set KAboutData in krunner. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0b7a607bebc58acd448a4437545663d5b5587a23'>Commit.</a> </li>
<li>Append "(Paused)" to paused jobs since they're not really obvious. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2b503ba920e44f629fe83ec6a1382239dcb7c6a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123501'>#123501</a></li>
<li>Fix crash when currentActivityChanged is emitted before creating view. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=317421fd23eabd17d84320a31552c40ccd45a778'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123494'>#123494</a></li>
<li>We don't need QtWebkit as a dependency anymore. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9c1d4f889d32a1b54938709c8c5dd9ca975532a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123457'>#123457</a></li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6882f14e7b3175a3db66436a9a31c0c7470af9dd'>Commit.</a> </li>
<li>[containmentactions] Verify that list returned from kglobalaccel is not empty. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=45ab13f297681930cede92cc3ce3299ae5d8fcaf'>Commit.</a> </li>
<li>[libtaskmanager] Use windowClass from KWindowInfo instead fetching from X each time. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9e4aa8c4a86d74d5a4ab08a3a1fa051a5479ed42'>Commit.</a> See bug <a href='https://bugs.kde.org/340583'>#340583</a></li>
<li>[ksmserver] Do not try to start X11 window manager if Wayland env variables are set. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3a7e17c997a8419ade1520768d28a4740387b4a1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123444'>#123444</a></li>
<li>Fix regression of plasmashell executable not showing version. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e42a0f067a60697b2cac458c130ec2aa101632db'>Commit.</a> </li>
<li>Be more educative and assign the local shortcuts from the global ones. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2734b8e809a1adb5db9dc74fc66c810cc873668f'>Commit.</a> </li>
<li>Simplify the map construction. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9f8a6c29892c0056c1ec83b0082bbe4984b6e2fe'>Commit.</a> </li>
<li>Check for the correct action in KAuthorized. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1634dd268916dacd93317f1e056429c23020dc1e'>Commit.</a> </li>
<li>Remove the local copy of kglobalaccel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2a5d9713989f101c45008e78599ff600f508ac97'>Commit.</a> </li>
<li>Image Wallpaper: Don't show duplicates after removing some of the wallpapers. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8d7da7a61c6bbd7f003d28c2eb14ead20e550c07'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123273'>#123273</a></li>
<li>Use the new effect for the dashboard. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3745569781a0edf6e36996e0dd5de5ab00e7dc62'>Commit.</a> CHANGELOGCHANGELOG: The desktop dashboard is now realized with a new KWin effect.. Code review <a href='https://git.reviewboard.kde.org/r/123362'>#123362</a></li>
<li>Fix taborder in lockscreen KCM. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=da37832e851de87a0177ff9342c622449804935b'>Commit.</a> </li>
<li>Start blinking battery icon at 5% and below rather than 10%. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=77626c1e3b540efadf1fc6bc23efa3b54bfaeb60'>Commit.</a> </li>
<li>Add keywords to screenlock KCM. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4ed07fe34679ed2a85ecc72bbc9c6a3192df1ad3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/331947'>#331947</a></li>
<li>Don't try to import no longer existing plugin. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f7a396768a1b8818ba33a5b1b0927ff881510bac'>Commit.</a> </li>
<li>Bump the minimum required KF5 version to 5.9, like plasma-desktop. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=dd19b6636e519c06df3e1ee725a03ec9cfd6741f'>Commit.</a> </li>
<li>Fix memleak. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8c82e5dcc1f075134546017470c86dc3b39781d6'>Commit.</a> </li>
<li>Port to KCMShell.open(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f5a177b6fadfd64a5c2246edb7329b8a9a07929d'>Commit.</a> </li>
<li>Restore index after removing a wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ba61ce61b4e1ebb28df0cc10aefa7a1824282394'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123275'>#123275</a>. Fixes bug <a href='https://bugs.kde.org/344384'>#344384</a></li>
<li>Use X-Plasma-StandAloneApp in some applets. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8be939c3033311cf3e84763ba2355a1e60f7babf'>Commit.</a> See bug <a href='https://bugs.kde.org/345851'>#345851</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Remove broken notification on auth prompt. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=595b96ed99d44f8a718e8e3f8c9834852bc7f6d7'>Commit.</a> </li>
<li>Revert "Fix warning". <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=8a2137643299f9647d5b683c0b564cf73defc36b'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=33a17014baf4f7f96e9fdeb67bd995412de02f59'>Commit.</a> </li>
<li>Improve debug messages. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=c9398689b32c02f1dbc2aa247ea0bb6fde59e239'>Commit.</a> </li>
<li>Use KMessageWidget for authentication error. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=9edb5e8041db3080cbebee363a29f699b0a20e3d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123814'>#123814</a></li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Stop messing around with the user's brightness by default. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=9c89b86e06090ae53883bc12ce987c74155dd7f6'>Commit.</a> </li>
<li>Get rid of broken battery notification. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=a2fd637db2e551dc3aff6fe39c848f557e140ca2'>Commit.</a> </li>
<li>Add ConsoleKit2 as a power backend. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=10f2be4f0a2b482008937b1e378e914a637d83be'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124388'>#124388</a></li>
<li>Execute KAuth jobs for brightness control in an async manner. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=3b56856d66f392bf18343f3b45f35da40813509f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123475'>#123475</a></li>
<li>Load DPMSAction only on xcb platform. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=cc89ab71b572473f84d94868fc82993073992a12'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124318'>#124318</a></li>
<li>Resurrect dead code and fix two memory leaks. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=0bda8dd05d52e22a989ca4e2fb948652cbdec4e7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124253'>#124253</a></li>
<li>Allow to cancel critical battery timer. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=9bb399ef24a3323788ef3084f2e404c8831f251b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124037'>#124037</a></li>
<li>Restore keyboard brightness when waking up from suspend. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=c5bba72d18f5af6dc2ad2b0ade86325630739f84'>Commit.</a> </li>
<li>Remove "Suspend inhibited" notification. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=928b497de4f563d5b9f223d675de94c6d226cc9d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/319447'>#319447</a></li>
<li>Simulate user activity when inhibitions change. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=bf9bf79c92a3d77df863d15c543f7f5329a564dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/315438'>#315438</a></li>
<li>Don't crash in XRandrBrightness if not on xcb. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=bb67165aa5d99e9eeea3c7f8b328b3b53a5e70a5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123999'>#123999</a></li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=63239199fc62f294ee82660105a4b33e76d3846b'>Commit.</a> </li>
<li>Set a meaningful description of the "Power Off" global action. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=9600a7b1f28ba012a4084c38d6464631a30f5a9a'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='http://quickgit.kde.org/?p=systemsettings.git'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Adjust icon names for Breeze theme in Systemsetting overview. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=b0030f95a78afc0bb436951caff9bfa9ee8265c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124530'>#124530</a></li>
</ul>


<h3><a name='user-manager' href='http://quickgit.kde.org/?p=user-manager.git'>User Manager</a></h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Now sets e-mail defaults and replaces useraccounts kcm module</li>
</ul>

<?php
  include("footer.inc");
?>
