<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 14.12.1");
  $site_root = "../";
  $version = "14.12.1";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("January 13, 2015. Today KDE released the first stability update for <a href='%1'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.", "announce-applications-14.12.0.php");?>
</p>

<p>
<?php i18n("More than 50 recorded bugfixes include improvements to the archiving tool Ark, Umbrello UML Modeller, the document viewer Okular, the pronunciation learning application Artikulate and remote desktop client KRDC.");?>
</p>

<p>
<?php print i18n_var("This release also includes Long Term Support versions of Plasma Workspaces %1, KDE Development Platform %2 and the Kontact Suite %2.", "4.11.15", "4.14.4"); ?>
</p>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_".$release.".php");?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications %1 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%2'>support the nonprofit organization behind the KDE community</a>.", $version, "https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5"); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php print i18n_var("Installing KDE Applications %1 Binary Packages", $version);?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php print i18n_var("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications %1 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.", $version);?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php print i18n_var("Compiling KDE Applications %1", $version);?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='%2'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='%3'>KDE Applications %1 Info Page</a>.", $version, "http://download.kde.org/stable/applications/".$version."/src/", "/info/applications-".$version.".php");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
