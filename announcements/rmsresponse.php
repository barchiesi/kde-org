<?php
  $page_title = "KDE \"Official\" Response to Stallman Editorial";
  $site_root = "../";
  include "header.inc";
?>

In a recent <A HREF="http://linuxtoday.com/news_story.php3?ltsn=2000-09-05-001-21-OP-LF-KE">editorial</A> on Linux Today, Richard Stallman claimed that
KDE is still in violation of the GPL even though Qt is now covered
under the GPL and all KDE code is compatible with the GPL.  His rather
absurd reasoning is that since KDE once violated the GPL, it will
always be in violation unless the individual copyright holders "grant
forgiveness."
<P>
We maintain, as always, that there are no licensing problems with KDE.
Here's why:
<P>
All code in KDE is copyrighted and covered under a free software (Open
Source) license.  The vast majority of the code was written
explicitely for KDE.  A few bits were written elsewhere and
incorporated into KDE.
<P>
In the former case, Stallman himself has said that there is and never
was any problem with license compatibilities.  In an email to the
debian-legal mailing list<A HREF="#1">[1]</A>, he writes "if the
authors of the program clearly intended it to be linked against Qt, I
would say they have given some kind of implicit permission for people
to do that."
<P>
In the latter case, his reasoning just doesn't make sense.  There are
only two parts of KDE that have GPLed code not written explicitely for
KDE -- a small bit in kmidi and a few lines in kghostview.  According
to Stallman, because we once linked that code to a library not
compatible with the GPL, we now have to beg forgiveness from the
copyright holders of that code or we will forever be in violation.
<P>
The "solution" to this is simple: we remove the "tainted" code from
kmidi and kghostview, release a "pure" version of each, then re-add
those files.  Since adding non-tainted code is fine, we would be
cleared.
<P>
This entire thing is just too absurd and we refuse to play this game.
<P>
That said, if you or somebody you know has code in KDE whose copyright
we really are violating, please speak up and send an email to
&#107;d&#x65;&#x40;&#107;&#100;&#101;.&#0111;r&#103; so that we may fix the issue.  In case of doubt, we have
compiled three documents: one listing all modules in KDE and the
license it follows<A HREF="#1">[2]</A>, one listing all copyright
holders in all code inside of the KDE CVS<A HREF="#3">[3]</A>, and one
with all copyright holders email addresses<A HREF="#4">[4]</A>
<P>
<B><A NAME="1">[1]</A></B> <A HREF="http://lists.debian.org/debian-legal-0006/msg00062.html">http://lists.debian.org/debian-legal-0006/msg00062.html</A><BR>
<B><A NAME="2">[2]</A></B> <A HREF="http://developer.kde.org/documentation/licensing/licensing.html">http://developer.kde.org/documentation/licensing/licensing.html</A><BR>
<B><A NAME="3">[3]</A></B> <A HREF="http://developer.kde.org/documentation/licensing/copyright-lines.html">http://developer.kde.org/documentation/licensing/copyright-lines.html</A><BR>
<B><A NAME="4">[4]</A></B> <A HREF="http://developer.kde.org/documentation/licensing/email-addresses.html">http://developer.kde.org/documentation/licensing/email-addresses.html</A><BR>

<?php include "footer.inc" ?>
