<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.48.0");
  $site_root = "../";
  $release = '5.48.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
July 14, 2018. KDE today announces the release
of KDE Frameworks 5.48.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Port remaining uses of qDebug() to qcDebug(ATTICA)");?></li>
<li><?php i18n("Fix checking invalid provider url");?></li>
<li><?php i18n("Fix broken url to API specification");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata");?></li>
<li><?php i18n("[tags_kio] The url query should be a key-value pair");?></li>
<li><?php i18n("The power state signal should only be emitted when the power state changes");?></li>
<li><?php i18n("baloodb: Make changes to cmdline arg description after rename prune -&gt; clean");?></li>
<li><?php i18n("Clearly show duplicate filenames in tag folders");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Update D-Bus xml files to use \"Out*\" for signal type Qt annotations");?></li>
<li><?php i18n("Add signal for devices's address changing");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Use the broom-style icon for edit-clear-all too");?></li>
<li><?php i18n("Use a broom-style icon for edit-clear-history");?></li>
<li><?php i18n("change 24px view-media-artist icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Android: Make it possible to override a target's APK directory");?></li>
<li><?php i18n("Drop outdated QT_USE_FAST_OPERATOR_PLUS");?></li>
<li><?php i18n("Add -Wlogical-op -Wzero-as-null-pointer-constant to KF5 warnings");?></li>
<li><?php i18n("[ECMGenerateHeaders] Add option for other header file extension than .h");?></li>
<li><?php i18n("Don't include a 64 when building 64bit architectures on flatpak");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Fix off by one error in Cache::clear (bug 396175)");?></li>
<li><?php i18n("Fix ResultModel item moving (bug 396102)");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Remove unneeded moc include");?></li>
<li><?php i18n("Make sure KLineEdit::clearButtonClicked is emitted");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Remove QT definitions duplicated from KDEFrameworkCompilerSettings");?></li>
<li><?php i18n("Make sure that it compiles with strict compile flags");?></li>
<li><?php i18n("Remove unused key X-KDE-DBus-ModuleName from test servicetype metadata");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Reduce QT_DISABLE_DEPRECATED_BEFORE to minimum dep of Qt");?></li>
<li><?php i18n("Remove QT definitions duplicated from KDEFrameworkCompilerSettings");?></li>
<li><?php i18n("Make sure to build with strict compile flags");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("check the node actually has a valid texture (bug 395554)");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("KDEDModule servicetype definition: remove unused key X-KDE-DBus-ModuleName");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Set QT_USE_FAST_OPERATOR_PLUS ourselves");?></li>
<li><?php i18n("Don't export kf5-config to the CMake config file");?></li>
<li><?php i18n("Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata");?></li>
</ul>

<h3><?php i18n("KDE WebKit");?></h3>

<ul>
<li><?php i18n("Port KRun::runUrl() &amp; KRun::run() to undeprecated API");?></li>
<li><?php i18n("Port KIO::Job::ui() -&gt; KIO::Job::uiDelegate()");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Avoid compiler warnings for taglib headers");?></li>
<li><?php i18n("PopplerExtractor: use directly QByteArray() args instead of 0 pointers");?></li>
<li><?php i18n("taglibextractor: Restore extracting audio props without tags existing");?></li>
<li><?php i18n("OdfExtractor: deal with non-common prefix names");?></li>
<li><?php i18n("Don't add -ltag to the public link interface");?></li>
<li><?php i18n("implement the lyrics tag for taglibextractor");?></li>
<li><?php i18n("automatic tests: do not embed EmbeddedImageData already in the library");?></li>
<li><?php i18n("add ability to read embedded cover files");?></li>
<li><?php i18n("implement reading of rating tag");?></li>
<li><?php i18n("check for needed version of libavcode, libavformat and libavutil");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Update D-Bus xml file to use \"Out*\" for signal type Qt annotations");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holidays/plan2/holiday_jp_* - add missing metainfo");?></li>
<li><?php i18n("updated Japanese holidays (in Japanese and English) (bug 365241)");?></li>
<li><?php i18n("holidays/plan2/holiday_th_en-gb - updated Thailand (UK English) for 2018 (bug 393770)");?></li>
<li><?php i18n("add Venezula holidays (Spanish) (bug 334305)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("In cmake macro file use CMAKE_CURRENT_LIST_DIR consequently instead of mixed use with KF5I18n_DIR");?></li>
<li><?php i18n("KF5I18NMacros: Don't install an empty dir when no po files exist");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Support choosing .ico files in custom icon file chooser (bug 233201)");?></li>
<li><?php i18n("Support Icon Scale from Icon naming specification 0.13 (bug 365941)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Use new method fastInsert everywhere where applicable");?></li>
<li><?php i18n("Restore compatibility of UDSEntry::insert, by adding a fastInsert method");?></li>
<li><?php i18n("Port KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled");?></li>
<li><?php i18n("Update D-Bus xml file to use \"Out*\" for signal type Qt annotations");?></li>
<li><?php i18n("Remove QT definition duplicated from KDEFrameworkCompilerSettings");?></li>
<li><?php i18n("Use a correct emblem icon for readonly files and folders (bug 360980)");?></li>
<li><?php i18n("Make it possible to go up to root again, in the file widget");?></li>
<li><?php i18n("[Properties dialog] Improve some permissions-related strings (bug 96714)");?></li>
<li><?php i18n("[KIO] Add support for XDG_TEMPLATES_DIR in KNewFileMenu (bug 191632)");?></li>
<li><?php i18n("update trash docbook to 5.48");?></li>
<li><?php i18n("[Properties dialog] Make all field values on general tab selectable (bug 105692)");?></li>
<li><?php i18n("Remove unused entry X-KDE-DBus-ModuleName from kded plugins' metadata");?></li>
<li><?php i18n("Enable comparing KFileItems by url");?></li>
<li><?php i18n("[KFileItem] Check most local URL for whether it's shared");?></li>
<li><?php i18n("Fix regression when pasting binary data from clipboard");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("more consistent mouse over color");?></li>
<li><?php i18n("don't open submenu for actions without children");?></li>
<li><?php i18n("Refactor the Global ToolBar concept (bug 395455)");?></li>
<li><?php i18n("Handle enabled property of simple models");?></li>
<li><?php i18n("Introduce ActionToolbar");?></li>
<li><?php i18n("fix pull to refresh");?></li>
<li><?php i18n("Remove doxygen docs of internal class");?></li>
<li><?php i18n("Don't link against Qt5::DBus when DISABLE_DBUS is set");?></li>
<li><?php i18n("no extra margin for overlaysheets in overlay");?></li>
<li><?php i18n("fix the menu for Qt 5.9");?></li>
<li><?php i18n("Check the visible property of the action as well");?></li>
<li><?php i18n("better look/alignment in compact mode");?></li>
<li><?php i18n("don't scan for plugins for each platformTheme creation");?></li>
<li><?php i18n("get rid of the \"custom\" set");?></li>
<li><?php i18n("add resetters for all custom colors");?></li>
<li><?php i18n("port toolbutton coloring to custom colorSet");?></li>
<li><?php i18n("introduce Custom color set");?></li>
<li><?php i18n("writable buddyFor to position labels regarding to subitems");?></li>
<li><?php i18n("When using a different background color, use highlightedText as text color");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("[KMoreTools] Enable installing tools via appstream url");?></li>
<li><?php i18n("Remove KNS::Engine d-pointer hack");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Remove unused key X-KDE-DBus-ModuleName from test service metadata");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("guard updateConfig for disabled status bars");?></li>
<li><?php i18n("add context menu to statusbar to toggle show total lines/word count");?></li>
<li><?php i18n("Implemented displaying of total lines in kate (bug 387362)");?></li>
<li><?php i18n("Make menu-bearing toolbar buttons show their menus with normal click rather than click-and-hold (bug 353747)");?></li>
<li><?php i18n("CVE-2018-10361: privilege escalation");?></li>
<li><?php i18n("Fix caret width (bug 391518)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[server] Send frame event instead of flush on relative pointer motion (bug 395815)");?></li>
<li><?php i18n("Fix XDGV6 popup test");?></li>
<li><?php i18n("Fix stupid copy paste bug in XDGShellV6 Client");?></li>
<li><?php i18n("Do not cancel old clipboard selection if it is same as the new one (bug 395366)");?></li>
<li><?php i18n("Honor BUILD_TESTING");?></li>
<li><?php i18n("Fix some spelling issues suggested by new linter tool");?></li>
<li><?php i18n("Add the arclint file in kwayland");?></li>
<li><?php i18n("Fixup @since for skip switcher API");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KMessageWidget] Update stylesheet when palette changes");?></li>
<li><?php i18n("Update kcharselect-data to Unicode 11.0");?></li>
<li><?php i18n("[KCharSelect] Port generate-datafile.py to Python 3");?></li>
<li><?php i18n("[KCharSelect] Prepare translations for Unicode 11.0 update");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Implement support for the Voice and Call interfaces");?></li>
<li><?php i18n("Don't set custom domain filter rules");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Show an icon for hidden files in Dolphin (bug 395963)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("FrameSvg: Update mask frame if image path has been changed");?></li>
<li><?php i18n("FrameSvg: Do not wreck shared mask frames");?></li>
<li><?php i18n("FrameSvg: Simplify updateSizes");?></li>
<li><?php i18n("Icons for Keyboard Indicator T9050");?></li>
<li><?php i18n("fix color for media icon");?></li>
<li><?php i18n("FrameSvg: Recache maskFrame if enabledBorders has been changed (bug 391659)");?></li>
<li><?php i18n("FrameSvg: Draw corners only if both borders in both directions are enabled");?></li>
<li><?php i18n("Teach ContainmentInterface::processMimeData how to handle Task Manager drops");?></li>
<li><?php i18n("FrameSVG: Delete redundant checks");?></li>
<li><?php i18n("FrameSVG: Fix QObject include");?></li>
<li><?php i18n("Use QDateTime for interfacing with QML (bug 394423)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Add Share action to Dolphin context menu");?></li>
<li><?php i18n("Properly reset plugins");?></li>
<li><?php i18n("Filter out duplicate plugins");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("no pixel values in checkindicator");?></li>
<li><?php i18n("use RadioIndicator for everybody");?></li>
<li><?php i18n("Don't overflow around popups");?></li>
<li><?php i18n("on Qt&lt;5.11 the control palette is completely ignored");?></li>
<li><?php i18n("anchor button background");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Fix device label with unknown size");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fixes for Java comments");?></li>
<li><?php i18n("Highlight Gradle files with Groovy syntax too");?></li>
<li><?php i18n("CMake: Fix highlighting after strings with a single <code>@</code> symbol");?></li>
<li><?php i18n("CoffeeScript: add extension .cson");?></li>
<li><?php i18n("Rust: Add keywords &amp; bytes, fix identifiers, and other improvements/fixes");?></li>
<li><?php i18n("Awk: fix regex in a function and update syntax for gawk");?></li>
<li><?php i18n("Pony: fix single quote escape and a possible infinite loop with #");?></li>
<li><?php i18n("Update CMake syntax for the upcoming release 3.12");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.48");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
