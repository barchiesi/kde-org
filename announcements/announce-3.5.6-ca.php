<?php

  $page_title = "Anunci de KDE 3.5.6";
  $site_root = "../";
  include "header.inc";
?>

<!-- Other languages translations -->
També disponible en:
<a href="announce-3.5.6.php">Anglès</a>
<a href="announce-3.5.6-es.php">Espanyol</a>
<a href="http://fr.kde.org/announcements/announce-3.5.6-fr.php">Francès</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.6.php">Islandès</a>
<a href="announce-3.5.6-it.php">Italià</a>
<a href="announce-3.5.6-nl.php">Holandès</a>
<a href="announce-3.5.6-pt.php">Portuguès</a>
<a href="announce-3.5.6-pt-br.php">Portuguès del Brasil</a>
<a href="announce-3.5.6-sl.php">Eslovè</a>
<a href="announce-3.5.6-sv.php">Suec</a>

<h3 align="center">
   El Projecte KDE publica la sisena versió que millora les traduccions i serveis
   de l'escriptori líder de programari lliure.
</h3>

<p align="justify">
  <strong>
    El KDE 3.5.6 incorpora traduccions a 65 idiomes, millores en al motor d'HTML
    (KHTML) i a altres aplicacions.
  </strong>
</p>

<p align="justify">
  25 de gener de 2007 (INTERNET). El <a href="http://www.kde.org/">Projecte
  KDE</a> anuncia avui la disponibilitat immediata del KDE 3.5.6, una nova
  versió de manteniment per l'última generació de l'escriptori lliure més
  avançat per GNU/Linux i altres UNIX. El KDE té ara traducció a 65 idiomes,
  fent-lo disponible a més persones que la majoria de programari no lliure, i
  pot ser fàcilment estès a altres comunitats que vulguin contribuir al
  projecte.
</p>

<p align="justify">
  Aquesta versió inclou un bon nombre de correccions d'errors al KHTML,
  <a href="http://kate.kde.org">Kate</a>, el kicker, el ksysguard i moltes 
  altres aplicacions. Les millores significatives inclouen millor compatibilitat 
  per usar compiz com a gestor de finestres amb el kicker,
  gestió de sessions per les de les pestanyes de
  l'<a href="http://akregator.kde.org">Akregator</a>,
  plantilles pels missatges al <a href="kmail.kde.org">KMail</a>, i nous
  menús de resum per <a href="http://kontact.kde.org">Kontact</a>
  fent més fàcil treballar amb les cites i tasques pendents. Les traduccions 
  també continuen, amb les traduccions del
  <a href="http://l10n.kde.org/team-infos.php?teamcode=gl">Gallec</a> quasi
  doblant-se al 78%.
</p>

<p align="justify">
  Per una llista més detallada de les millores des de la versió
  <a href="http://www.kde.org/announcements/announce-3.5.5.php">KDE 3.5.5</a>
  consulteu la pàgina de canvis del
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php">KDE 3.5.6</a>.
</p>

<p align="justify">
  El KDE 3.5.6 està format per un escriptori bàsic i quinze paquets extra 
  (gestió d'informació personal, administració, xarxes, educació, utilitats 
</p>

<h4>
  Distribucions amb KDE
</h4>
<p align="justify">
  La majoria de distribucions de Linux i sistemes operatius UNIX no incorporen
  immediatament les noves versions de KDE, sinó que integren els paquets de KDE
  3.5.6 en les seves properes versions. Consulteu
  <a href="http://www.kde.org/download/distributions.php">aquesta llista</a>
  per veure quines distribucions porten KDE.
</p>

<h4>
  Instal·lació de paquets binaris del KDE 3.5.6
</h4>
<p align="justify">
  <em>Creadors de paquets</em>.
  Alguns proveïdors de sistems operatius ens han obsequiat amablement amb
  paquets binaris del KDE 3.5.6 per algunes versions de les seves distribucions,
  en altres casos han estat voluntaris de la comunitat. Alguns d'aquests paquets
  es poden descarregar lliurement al servidor de descàrregues del KDE a
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">http://download.kde.org</a>.
  En properes setmanes hi poden haver nous paquets així com millores als paquets
  disponibles. 
</p>

<p align="justify">
  <a name="package_locations"><em>Ubicació dels paquets</em></a>.
 Per una llista dels paquets binaris disponibles actualment dels quals el
 Projecte KDE ha estat informat consulteu la pàgina d'informació del 
  <a href="/info/3.5.6.php">KDE 3.5.6</a>.
</p>

<h4>
  Com compilar el KDE 3.5.6
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codi font</em>.
  El codi complet del KDE 3.5.6 es pot
  <a href="http://download.kde.org/stable/3.5.6/src/">descarregar
  lliurement</a>. Les instruccions de com compilar i instal·lar el KDE 3.5.6
  estan disponibles a a pàgina d'informació del <a href="/info/3.5.6.php">KDE
  3.5.6</a>.
</p>

<h4>
  Com col·laborar amb el KDE
</h4>
<p align="justify">
  El KDE és un projecte de
  <a href="http://www.gnu.org/philosophy/free-sw.html">Programari Lliure</a>
  que existeix i creix gràcies a molts voluntaris que donen el seu temps i
  esforç. El KDE sempre busca nous voluntaris i contribucions, ja sigui per
  programar, corregir o informar
  d'errors, escriure documentació, traduir, ajudar en la promoció, aportar
  diners, etc. Totes les contribucions s'accepten amb gratitud. Si us plau
  llegiu <a href="/community/donations/">Com col·laborar amb el KDE</a> per més informació.
</p>

<h4>
  Quant al KDE
</h4>
<p align="justify">
  KDE és un projecte <a href="/awards/">premiat</a> i independent format per
  <a href="/people/">centenars</a> de desenvolupadors, traductors, artistes i
  altres professionals d'arreu del món, col·laborant gràcies a Internet per
  crear un entorn d'escriptori i oficina de lliure distribució, sofisticat,
  personalitzable i estable que utilitza una arquitectura basada en components,
  transparent a la xarxa i ofereix una magnífica plataforma de desenvolupament.
</p>

<p align="justify">
  KDE proporciona un escriptori madur i estable que inclou un navegador
  (<a href="http://konqueror.kde.org/">Konqueror</a>), un conjunt d'utilitats de
  gestió d'informació personal (<a href="http://kontact.org/">Kontact</a>), un
  conjunt d'aplicacions d'oficina complet
  (<a href="http://www.koffice.org/">KOffice</a>), una gran quantitat
  d'aplicacions i utilitats de xarxa i un eficient i intuïtiu entorn integrat de
  desenvolupament (<a href="http://www.kdevelop.org/">KDevelop</a>).
</p>

<p align="justify">
  KDE és la prova real que el model de desenvolupament de programari lliure
  "estil Basar" pot crear tecnologies de primer nivell a la par o superiors que
  el més complex del programari comercial.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Notes en quant a marques registrades.</em>
  KDE<sup>&#174;</sup> i el logo K Desktop Environment<sup>&#174;</sup> són
  marques registrades de KDE e.V.

  Linux és una marca registrada de Linus Torvalds.

  UNIX és una marca registrada de The Open Group als Estats Units i d'altres
  països.

  Totes les altres marques registrades i copyrights esmentats en aquest anuncis
  són propietat dels respectius propietaris.
  </font>
</p>

<hr />

<h4>Contactes de premsa</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Àfrica</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Telèfon: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Àsia</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Telèfon : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Telèfon: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Nordamèrica</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Telèfon: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Telèfon: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Sudamèrica</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Telèfon: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
