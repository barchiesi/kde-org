<?php
  $page_title = "KDE 1.94 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<P>DATELINE SEPTEMBER 15, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">Final Beta of KDE Desktop Available for Linux<SUP>&reg;</SUP></H3>
<P><STRONG>Final Beta Preview of Leading Desktop for Linux<SUP>&reg;</SUP> and Other UNIXes<SUP>&reg;</SUP></STRONG></P>
<P>September 14, 2000 (The INTERNET).  The <A href="/">KDE
Team</A> today announced the release of KDE 1.94, the fifth and final beta
preview of Kopernicus, KDE's next-generation, powerful, modular desktop. 
Following the release of KDE 1.93 on August 23, 2000, the release,
code-named "Kandidat", is based on
<A HREF="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>
Qt<SUP>&reg;</SUP> 2.2.0 and will include the core KDE libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other core KDE packages: 
Administration, Games, Graphics, Multimedia, Network, Personal
Information Management (PIM), Toys and Utilities. 
The release is targeted at users who would like to help the
KDE team make usability, speed and feature enhancements and fix the remaining
set of bugs before the release of KDE 2.0 ("Kopernicus"), scheduled
for early-fourth quarter 2000.
</P>
<P>
The major changes since the Beta 3 release last month include:
<UL>
<LI>General stability improvements throughout the packages.</LI>
<LI>Major performance enhancements to the HTML widget. It also now fully supports "mailto:" URLs and caches pages for fast subsequent retrieval.</LI>
<LI>The latest Qt release, qt-2.2.0, which for the first time is licensed under
the <A HREF="http://www.kde.com/legal/gpl_lic.php">GNU General Public
License</A>, as well as the
<A HREF="http://www.kde.com/legal/qpl1.0_lic.php">Q Public License</A>.</LI>
<LI>Fixes to FTP and HTTP proxy support and many improvements to FTP
support, including symlink support.</LI>
<LI>The Secure Socket Layer (SSL) code in <A HREF="http://konqueror.kde.org/">Konqueror</A> has been improved, although it is still undergoing heavy development.</LI>
<LI>Konqueror now features an "Open With" and "Save As" option for non-HTML links.</LI>
<LI>Useability enhancements, such as background colors for desktop icon text.</LI>
</UL>
</P>
<H4>Highlights of Kandidat</H4>
<P><A HREF="http://konqueror.kde.org/">Konqueror</A> reigns as the
next-generation web browser, file manager and
document viewer for KDE 2.0.  Widely acclaimed as a technological
break-through for the Unix desktop, Konqueror has a component-based
architecture which combines the features and functionality of Internet
Explorer<SUP>&reg;</SUP>/Netscape Communicator<SUP>&reg;</SUP> and
Windows Explorer<SUP>&reg;</SUP>.  Konqueror will support
the full gamut of current Internet technologies, including
JavaScript, Java<SUP>&reg;</SUP>, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<SUP>&reg;</SUP> plug-ins (for
playing Flash<SUP>TM</SUP>,
RealAudio<SUP>TM</SUP>, RealVideo<SUP>TM</SUP> and similar technologies).
In addition,
Konqueror's network transparency offers seamless support for browsing
Linux<SUP>&reg;</SUP> NFS shares, Windows<SUP>&reg;</SUP> SMB shares,
HTTP pages, FTP directories as well as any other protocol for which
a KIO plug-in is available.
</P>
<P>
The <A HREF="http://koffice.kde.org/">KOffice suite</A> is one of the
most-anticipated
Open Source projects.  The suite consists
of a spreadsheet application (KSpread), a vector drawing application
(KIllustrator), a frame-based word-processing application (KWord),
a presentation program (KPresenter), and a chart and diagram application
(KChart).  Native file formats are XML-based, and work on
filters for proprietary binary file formats is progressing.
Combined with a powerful scripting language and the
ability to embed individuals components within each other using KDE's
KParts technology, the KOffice
suite will provide all the necessary functionality to all but the most
demanding power users, at an unbeatable price -- free.
</P>
<P>
KDE's customizability touches every aspect of this next-generation
desktop.  <A NAME="Style engine">Kandidat benefits from Qt's
style engine, which permits developers and artists to create their
own widget designs down to the precise appearance of a scrollbar,
a button, a menu and more, combined with development tools which will
largely automate the creation of these widget sets.  In addition, KDE
offers excellent support for GTK themes.  Just to
mention a few of the legion configuration options,
users can choose among: numerous types of menu effects; a menu
bar atop the display (Macintosh<SUP>&reg;</SUP>-style) or atop each individual
window (Windows-style); icon styles; system sounds; key bindings;
languages; toolbar and menu composition; and much much more.
</P>
<H4>Downloading and Compiling the Final Beta</H4>
<P>
The source packages for Kandidat are available for free download at
<A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/">http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>.  Kandidat requires
qt-2.2.0, which is available from the above locations under the name
<A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/qt-x11-2.2.0.tar.gz">qt-x11-2.2.0.tar.gz</A>.
Please be advised that Kandidat will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt 2.2 is a stable product and not part of KDE's beta testing.
</P>
<P>
For further instructions on compiling and installing Kandidat, please consult
the <A HREF="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://developer.kde.org/build/index.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages of the Final Beta</H4>
<P>
The binary packages for Kandidat will be available for free download under
<A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/">http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>. Kandidat requires
qt2.2.0, which is available from the above locations.
Please be advised that Kandidat will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt is not part of KDE's beta testing.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/Mandrake/RPMS/">Linux-Mandrake</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/COL-2.4/RPMS/">Caldera Open Linux 2.4</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/axp/6.4/">SuSE Linux AXP  6.4</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/i386/6.4/">SuSE Linux i386 6.4</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/i386/7.0/">SuSE Linux i386 7.0</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/ppc/6.4/">SuSE Linux PPC  6.4</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/localized/">SuSE Linux noarch</A></LI>
<LI><A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/FreeBSD/">FreeBSD</A></LI>
</UL>
<P>
Check the ftp servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days.
</P>

<H4>About KDE</H4>
<P>KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
Currently development is focused on KDE 2, which will for the first time
offer a free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create first-rate technologies on par with
and superior to even the most complex commercial software.</P>

<P>For more information about KDE, please visit KDE's <A HREF="../whatiskde/">web site</A>.</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#x67;r&#x61;&#110;&#0114;&#x6f;th&#x40;&#107;&#x64;&#101;&#x2e;&#111;&#x72;g<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
pour&#64;k&#00100;&#x65;.o&#00114;g<BR>
(1) 718-456-1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#00111;n&#0111;&#00108;&#100;&#x40;&#x6b;de&#46;&#x6f;&#114;g<BR>
(49) 179 2252249
</TD></TR>
</TABLE>

<?php include "footer.inc" ?>
