<?php
  $page_title = "KDE 4.1 Alpha1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<a href="http://fr.kde.org/announcements/announce-4.1-alpha1.php">French</a>
<a href="announce-4.1-alpha1-pt_BR.php">Portuguese (Brazilian)</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">German</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Romanian</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
  KDE Project Ships First Alpha of KDE 4.1
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Alpha Release of KDE 4.1, The First End-User Release
of the Next-Generation Free Desktop</strong>
</p>

<p align="justify">
 April 29, 2008 (The INTERNET). The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.1 Alpha1.
With the soft feature freeze in effect, KDE 4.1 Alpha1 provides a first preview of
what can be expected from KDE 4.1, due in late July this year.
</p>


<h4>
  <a name="changes">What will KDE 4.1 bring?</a>
</h4>

<p align="justify">
<ul>
    <li>
        <strong>Akonadi</strong> is among the new big things in KDE 4.1. Akonadi is the desktop-independent
        storage engine for Personal Information Management (PIM) data. While not yet based on
        Akonadi, KDE 4.1 also brings a KDE 4 port of PIM applications such as KMail and
        KOrganizer.
    </li>
    <li>
        With KDE 4.0 being available for X11 platforms, KDE 4.1 will also be available on
        <strong>Windows, Mac OS X and OpenSolaris</strong>. The ports are not yet completely
        finished, but good for a first preview nevertheless.
    </li>
    <li>
        KDE 4.1 Alpha1 is based on <strong>Qt 4.4</strong>. Qt 4.4 brings improvements in
        performance and functionality to KDE 4.1. SVG rendering speed has
        vastly improved, and widgets and layouts can now be used on canvases such as the
        Plasma desktop and panel. Migrating the Plasma codebase to these new
        features is still work in progress, so some instability here is to be
        expected.
    </li>
</ul>

Please note that KDE 4.1 Alpha1 is not suitable for daily usage. It is meant as a preview for upcoming
technology more than anything else.
</p>

<h4>
  Compiling KDE 4.1 Alpha1 (4.0.71)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.71 may be <a
  href="http://www.kde.org/info/4.0.71.php">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.71
  are available from the <a href="/info/4.0.71.php">KDE 4.0.71 Info
  Page</a>, or on <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
