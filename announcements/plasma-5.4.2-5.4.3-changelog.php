<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.4.3 complete changelog");
$site_root = "../";
$release = 'plasma-5.4.3';
include "header.inc";
?>
<p><a href="plasma-5.4.3.php">Plasma 5.4.3</a> complete changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Re-added cursor moving hack for kde4, otherwise mouse-over effects are broken after window drag. <a href='https://quickgit.kde.org/?p=breeze.git&a=commit&h=983b2879836b5cd6776dbe9e4a78342fbf8758d5http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b5c4b2b19e79ba8450dcf25dbe37f06d29411986'>Commit.</a> </li>
<li>Properly clip selected tab to prevent rendering artifacts. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=97352b2338d3bfaa719c0fff8f1a9eb21a96d878'>Commit.</a> </li>
<li>Fixed bounding conditions to consistently decide whether a combobox should be flat or not. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f74a58ce7ccbc78b5d77a340d870a94299e3bbe5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354708'>#354708</a></li>
<li>Use NETRootInfo to initiate wm move operation. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ca72ca904f7fb8872667322bf5143e2b81eaf2f3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125522'>#125522</a>. Fixes bug <a href='https://bugs.kde.org/353749'>#353749</a></li>
<li>Install emotes and mimtetypes directories of the dark icon theme. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=12d8b9a9907b28ba1cbaffc380f4eeae4de9774d'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Fix SystemLoadViewer freezing plasmashell. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e32a705bcca843b71fa5a43defd2d0630cb2f810'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348385'>#348385</a>. Code review <a href='https://git.reviewboard.kde.org/r/125858'>#125858</a></li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>KInfoCenter</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Energy: Limit org.freedesktop.UPower.Wakeups.GetData calls to one per second. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=5e8760df9ce119df92a386f42af3555dd43ec2cf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125745'>#125745</a></li>
<li>Fix connecting to QAction::triggered signal. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=98db51d242b4657937c9a2a17d365e34ec699e21'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125744'>#125744</a></li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Fix crash when exiting kscreen kcm in systemsettings. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=4653c287f844f2cb19379ff001ca76d7d9e3a2a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344651'>#344651</a>. Code review <a href='https://git.reviewboard.kde.org/r/125734'>#125734</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[aurorae] Use property binding for toggled state. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e6542f01d39e49b0d5aca61b8cb4a06cf2825fc7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354702'>#354702</a>. Code review <a href='https://git.reviewboard.kde.org/r/125917'>#125917</a></li>
<li>[aurorae] Fix state changes for maximize/restore button in plastik theme. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7c3e4afe54f4289dc0a781f10f189a41efa003b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354702'>#354702</a></li>
<li>Improve virtual desktop selection for transients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7339e9639ff53955862da74e48d5440ff465dbca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354090'>#354090</a>. Code review <a href='https://git.reviewboard.kde.org/r/125758'>#125758</a></li>
<li>[kcmeffects] Do not use root context properties. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=76cd1fdc3411142991096adf8da9d7e74536f671'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354164'>#354164</a>. Fixes bug <a href='https://bugs.kde.org/351763'>#351763</a>. Code review <a href='https://git.reviewboard.kde.org/r/125737'>#125737</a></li>
<li>[kcmkwin/deco] Delay deleting of PreviewBridge. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=484e4be7f66b6f6d4021f6adb4f43531df9649c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344278'>#344278</a>. Code review <a href='https://git.reviewboard.kde.org/r/125724'>#125724</a></li>
<li>[decorations] Delay closeWindow to next event cycle. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=10ad9262a184e1afc088bee35b7fa4c188d9d63f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346857'>#346857</a>. Code review <a href='https://git.reviewboard.kde.org/r/125704'>#125704</a></li>
</ul>


<h3><a name='libksysguard' href='http://quickgit.kde.org/?p=libksysguard.git'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Load all ksgrd translations correctly. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=eabbbab15d64b5919dd2c58cc7d5f6bc183620db'>Commit.</a> </li>
</ul>


<h3><a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a> </h3>
<ul id='ulmuon' style='display: block'>
<li>Batch resource package id's. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a24e495c2eea95e76b1389bdffaf27e30d7e273a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354562'>#354562</a></li>
<li>Fix PackageKit details display. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=f110bb31d0599fda5478d035bdaf5ce325419ca6'>Commit.</a> </li>
<li>Org.kde.discover.desktop: validation fixes. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=89208725ec4c18129076c661873bf303ab7a4d38'>Commit.</a> </li>
<li>Fix package removal on PackageKit. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=3c1cccdb7f9b0d53da3e2e22bfafe2cefd5fc577'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354415'>#354415</a>. See bug <a href='https://bugs.kde.org/315063'>#315063</a></li>
<li>ParseUpdateInfo() does not correctly account for securtity updates. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=dbf64fc4e07a6ed5eccf16cf116eb8590e5007ea'>Commit.</a> See bug <a href='https://bugs.kde.org/347284'>#347284</a>. Code review <a href='https://git.reviewboard.kde.org/r/125749'>#125749</a></li>
<li>Fix kuit markup calls. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=46df03e247fede140d95098f78af300eb3acb3d5'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Re-added code to update config's minimumsize based on the animation tab. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=c292466b64dcf9fe6a09f7c2b3775eefcc7245d9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354547'>#354547</a></li>
<li>Use NETRootInfo to initiate wm move operation. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=ad49d24748f3bbe9524528d3e36416514cc93114'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[desktopcontainment] Clip applet container while resizing it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ec990f55ea6f00fc9772591f4365bc27765ad809'>Commit.</a>  Applet content no longer leaks outside its boundaries under certain circumstances while resizing it. Code review <a href='https://git.reviewboard.kde.org/r/125896'>#125896</a></li>
<li>[Kickerdash] Close when opening applet settings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1d1c1ae5d98bf645492144ca37bbcf8148ab27c0'>Commit.</a>  Application Dashboard now closes when opening its settings not to conceal them. Code review <a href='https://git.reviewboard.kde.org/r/125817'>#125817</a></li>
<li>[Kickoff] Support mouse back button in ApplicationsView. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=23919d4f0552a4c7b361cbcf768c6d65d2174e8d'>Commit.</a>  Pressing the back button on a mouse now goes up a level in Kickoff's application view. Fixes bug <a href='https://bugs.kde.org/353971'>#353971</a>. Code review <a href='https://git.reviewboard.kde.org/r/125822'>#125822</a></li>
<li>Port KAuth return code error checking in fontinst. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7ebe57aa74cd99ff81c4d0be9f1423a47b94d64b'>Commit.</a> See bug <a href='https://bugs.kde.org/344473'>#344473</a>. See bug <a href='https://bugs.kde.org/300951'>#300951</a>. See bug <a href='https://bugs.kde.org/345234'>#345234</a>. Code review <a href='https://git.reviewboard.kde.org/r/125555'>#125555</a></li>
<li>[Desktop Toolbox] Keep hovered state while menu is open. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=45481bb57184fcd4b28adf9f741093ecbc281786'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125820'>#125820</a></li>
<li>Fix stash pop messup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5f45ad7a6cc0f747bfb1508a4d0f7922ea7d2027'>Commit.</a> </li>
<li>Fix Task Manager mouse handling state corruption. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6c82a2da365d83a01e21d138a64b8e7864df6c72'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353752'>#353752</a></li>
<li>Overhaul filter handling. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=31808d8b8bbf0c995acf20686d3dea63f84f4523'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353409'>#353409</a></li>
<li>Re-activate window on focus loss. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=52d04f89e7070aad6a8c1daf92de67a2f513f01f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352297'>#352297</a></li>
<li>Don't start an eventLoop from QML in menu entry editing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4bda99e759fc99efb6b98c932f3682b307434a4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347997'>#347997</a>. Code review <a href='https://git.reviewboard.kde.org/r/125545'>#125545</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Do not load secrets for 802-1x setting when LEAP authentication algorithm. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=39d8d9ac179bea1b38b470fabf6ef14bd174df4b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354265'>#354265</a></li>
<li>Make all bluez calls asynchronous. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=48a45ebad0e1d5ecc023a9e8866a95d47c76790f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125785'>#125785</a>. Fixes bug <a href='https://bugs.kde.org/354230'>#354230</a></li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Install kconf_update script in kdelibs4 directory. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=cbc51943750047d722d25c725461dae4b0037e78'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125706'>#125706</a></li>
<li>Use better guard against recursion when changing volume with sliders. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=3d77132a1ca918ca4082c5078833ec1b4817bc20'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125622'>#125622</a></li>
<li>Allow to change volume with mouse wheel in kcm. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=a81d5e847969f7c640c81bf710af99c1bf1a2669'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125088'>#125088</a></li>
<li>Fix changing volume with scrolling on high resolution touchpad. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=019e33a0c4c62228a21c6347f707fa955ad6d407'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125089'>#125089</a></li>
</ul>


<h3><a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>[plasmaengineexplorer] Don't crash on invalid plugininfo. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=e3ed62cf9f44fb16adb7bfe8e347472757e2ae18'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125895'>#125895</a></li>
<li>I18n: Fix translation of Cuttlefish UI and the KTextEditor plugin. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=b319ef0cbd70959adaa3bd58acf064faffb785d7'>Commit.</a> </li>
<li>Properly extract messages into cuttlefish.pot and cuttlefish_editorplugin.pot. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=d9b157b08fd10f4c6f17d282ed67564ff704feee'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Set alignment in PanelView scriptengine if PanelView is not created yet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=331a0e79036a44581eab78140593e18a6de6bba1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125920'>#125920</a></li>
<li>[systemtray] Use activated param of activated signal rather than currentIndex. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c67bdb1250a2b6bc9f7d8baa83f1803dca484224'>Commit.</a>  Fixed changing applet visibility in system tray settings when clicking the options</li>
<li>Remove duplicated QIcon icon property. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6728fd935da97093c69e414309e420353f6b5da1'>Commit.</a> </li>
<li>Ensure the DesktopView has the correct size since the beginning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4c4beb7a1097de831c3ec6b5f5155bb65446e155'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353975'>#353975</a></li>
<li>Window position is fundamental to determining what screen it's in. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7e8aad767a250845a182166876550fb4e9701de4'>Commit.</a> </li>
<li>ThemeChanged needs to be called on every window the panel has. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3d8edf5a1166e87dc2c12903f621a3c24ac9913'>Commit.</a> </li>
<li>Use dbus-update-activation-environment. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=90081cb0483ffc3ad46af3b99de171ed38716ac0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125768'>#125768</a></li>
<li>Update the KSplash background to that 5.4 wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=467d997d8ad534d42b779719cec03a8cbfb66162'>Commit.</a> </li>
<li>Ensure correct timezone is displayed when the selected ones change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=11473e62c4c91f1302ff2303f732827422b88eb8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125709'>#125709</a>. Fixes bug <a href='https://bugs.kde.org/353996'>#353996</a>. Fixes bug <a href='https://bugs.kde.org/348612'>#348612</a></li>
<li>Digitalclock: Use Text.HorizontalFit for date label in vertical panel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4f265eda41df4abc6aa7fb2a5e2a1e73ae2989e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351768'>#351768</a>. Code review <a href='https://git.reviewboard.kde.org/r/125625'>#125625</a></li>
<li>When connecting a device always open the plasmoid. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=46c43fe917f3075c3c0b0a71f381e022ac09045f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349616'>#349616</a></li>
<li>Include env vars from sourced files on pre-startup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=509bafddf5bd350e4f41719728bd670cd2fac359'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125323'>#125323</a></li>
</ul>


<h3><a name='plasma-workspace-wallpapers' href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git'>Plasma Workspace-wallpapers</a> </h3>
<ul id='ulplasma-workspace-wallpapers' style='display: block'>
<li>Add version to follow plasma convention. <a href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git&amp;a=commit&amp;h=5055e8ad7e5f8c2ca39f7389ec35b49d57f77aee'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
