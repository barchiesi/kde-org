<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi-mime' href='https://cgit.kde.org/akonadi-mime.git'>akonadi-mime</a> <a href='#akonadi-mime' onclick='toggle("ulakonadi-mime", this)'>[Hide]</a></h3>
<ul id='ulakonadi-mime' style='display: block'>
<li>Port from QTime to QElapsedTimer to fix build with Qt 5.14. <a href='http://commits.kde.org/akonadi-mime/1aed49a1e371ea4a9035f2eabc9fe72d2f15da18'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix bug in Worksheet::load(QByteArray* data). <a href='http://commits.kde.org/cantor/4da31bde2f8c872c40305f2b99456ebe558b0532'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix crash when lauching dolphin with a search scheme. <a href='http://commits.kde.org/dolphin/31fd775f368f61f6dde54c0c3a2e83d65599c3c2'>Commit.</a> </li>
<li>Update searchbox on URL changes (Fix D24369). <a href='http://commits.kde.org/dolphin/8599a42ac13e2af6f085d899380aee7714ae28c2'>Commit.</a> </li>
<li>Update searchbar parameters on URL change. <a href='http://commits.kde.org/dolphin/b3120cb90e3d1dd5f4eef13e93378ccb1d01d098'>Commit.</a> </li>
<li>Bring back placesitemmodeltest. <a href='http://commits.kde.org/dolphin/32a4d1dd40f8f1ab28ba3d57fba2a3dd661a8104'>Commit.</a> </li>
<li>Fix ascending/descending choosers getting unchecked when re-selecting the same sort order. <a href='http://commits.kde.org/dolphin/60d6a3bdbcd44360e1c6ae1c82239ecf6d60ded7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411223'>#411223</a></li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Hide]</a></h3>
<ul id='uldolphin-plugins' style='display: block'>
<li>Use clang-tidy to add missing override. <a href='http://commits.kde.org/dolphin-plugins/6db543e746aa16a13b8b8db133e1bc03e4191b27'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Port away from QWheelEvent deprecated API to fix compilation with Qt 5.14. <a href='http://commits.kde.org/eventviews/ab8b2113c73d9e3c07c0692e8bcfdd6eef4a6e09'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>Fix encoding of size tooltip in radial map. <a href='http://commits.kde.org/filelight/69a707b455739d6acac56a52e1953b9e8b283031'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411243'>#411243</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Save the Mimetype of opened resources. <a href='http://commits.kde.org/gwenview/7831c0af7c64912e31f13d39c177c1491a18c6be'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/juk/ee3bd7033dbe5442779d18b20806828cf60c634e'>Commit.</a> </li>
<li>Install notifier file in correct directory. <a href='http://commits.kde.org/juk/bffb7e7704059f2d2946b6df1b557a531222d14c'>Commit.</a> </li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix crash when adding big files and mkisofs is not installed. <a href='http://commits.kde.org/k3b/787aaa1f4a28bbf6d76f7c35c39f4c7dfa874ac0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411969'>#411969</a></li>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/k3b/3800a64b3b326af543313d1a28ae5ec40358398b'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Don't trigger repeat-at-login alarms when editing or importing. <a href='http://commits.kde.org/kalarm/483591a76fe41f0b549778cb3eb3dbf5b8c25751'>Commit.</a> </li>
<li>Fix error on undo of an active alarm deletion. <a href='http://commits.kde.org/kalarm/7cc6b0cdc04cd982847307ca7c8646ea3676ba87'>Commit.</a> </li>
<li>Fix errors in determining a collection's validity and content mime types. <a href='http://commits.kde.org/kalarm/c3b5feeef7356eff4a8284b1add1460c23cb0787'>Commit.</a> </li>
<li>Show correct read-only status of an alarm in its context menu. <a href='http://commits.kde.org/kalarm/ba4e896f4f7d1e6a897542f4d46701e7564ff78c'>Commit.</a> </li>
<li>Fix determination of writable collections. <a href='http://commits.kde.org/kalarm/e65cdb4abe05e620b14a4708ca2b52c38abb86b8'>Commit.</a> </li>
<li>Fix determination of writable collections. <a href='http://commits.kde.org/kalarm/57469efa692eda9088524c8afd202004f523f280'>Commit.</a> </li>
<li>When asked for writable collections, only return fully writable ones. <a href='http://commits.kde.org/kalarm/ded31e4a351ac425e086000c3ef3777f14a35586'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Hide]</a></h3>
<ul id='ulkalarmcal' style='display: block'>
<li>Fix UID conversion to active category. <a href='http://commits.kde.org/kalarmcal/f4e4ac636ba325dd8d81eca51b96d85390da5cf2'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Reasonable sub-toolbar sizes, like normal QPushButton sizes. <a href='http://commits.kde.org/kate/90f1b2bdca5a361bfb7b7aed020c3e5be41f136a'>Commit.</a> </li>
<li>Load terminal on demand to have some parent widget for WINDOWID computation. <a href='http://commits.kde.org/kate/f295734722808827e3cb0743f6588d01a2ae2a0b'>Commit.</a> See bug <a href='https://bugs.kde.org/411965'>#411965</a></li>
<li>Tab bar: use tab-close icon again. <a href='http://commits.kde.org/kate/b27dc2aa859eeac9fdd18c7b1d48b9488ebcbb54'>Commit.</a> </li>
<li>Fix hi-dpi rendering of tab buttons. <a href='http://commits.kde.org/kate/a853e687ac4f56f1c996e6edefd46f26192c3127'>Commit.</a> </li>
<li>Fix hi-dpi rendering of toolview buttons. <a href='http://commits.kde.org/kate/90a8ecb8cc24aaa3afe58653c885fd0051d81702'>Commit.</a> </li>
<li>Tab bar: Fix close button in right-to-left layout. <a href='http://commits.kde.org/kate/1b48d07e2148a4e55346ff64b01aae0884291b33'>Commit.</a> </li>
<li>Fix HiDPI support for KWrite + Kate. <a href='http://commits.kde.org/kate/5daa2ff7b7ce4e980fa96cad72190cb92c4e13a1'>Commit.</a> </li>
<li>Fix fortran symbol viewer for new highlighting names. <a href='http://commits.kde.org/kate/a7b98dd9d9be75deba1afe4a459363c0f7589c2e'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalendarcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Hide]</a></h3>
<ul id='ulkcalcore' style='display: block'>
<li>Fix compilation with Qt 5.14, QLatin1Literal seems gone. <a href='http://commits.kde.org/kcalendarcore/ebf83c832e13643c64174d9ac551628011e179ac'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Hide]</a></h3>
<ul id='ulkde-dev-scripts' style='display: block'>
<li>Fix unwanted removal of /* before emacs mode line, which broke compilation. <a href='http://commits.kde.org/kde-dev-scripts/0048b754fe44d346715e0055de0196c41d9f332f'>Commit.</a> </li>
<li>Fix script for the case of multiple .rc files in the same line. <a href='http://commits.kde.org/kde-dev-scripts/3d4f2a4f378f9ff9722a0bbcd31365ab0be13161'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix crash on composition resize. <a href='http://commits.kde.org/kdenlive/6d8c7cfbedcd9feae08ad2575da42157a618560f'>Commit.</a> </li>
<li>Update MSYS2 build script. <a href='http://commits.kde.org/kdenlive/728844d8da885208ea2ff14051b6f0fe05865529'>Commit.</a> </li>
<li>Fix Windows audio screen grab (#344). <a href='http://commits.kde.org/kdenlive/01322357e608e984b9e51986be5926704f21bcdd'>Commit.</a> </li>
<li>Remove local reference to current project. <a href='http://commits.kde.org/kdenlive/c1c39a936f3abcb336f707b0b04f9ebae6fd1d7e'>Commit.</a> </li>
<li>Disable multitrack view on render. <a href='http://commits.kde.org/kdenlive/1c804123d60948bf5e65272453467d16fd8dd620'>Commit.</a> </li>
<li>Fix clip duration incorrectly reset on profile change. Fixes #360. <a href='http://commits.kde.org/kdenlive/d02863d2b4b647dd3485f39dcca3ffe0c2718307'>Commit.</a> </li>
<li>Fix compile warnings. <a href='http://commits.kde.org/kdenlive/56536237259f0ef154430db8a46b4e82a5a6d24a'>Commit.</a> </li>
<li>Make affine filter bg color configurable. Fixes #343. <a href='http://commits.kde.org/kdenlive/1553e308c793fbd3de1456dd1ee8e11bb27d060c'>Commit.</a> </li>
<li>Fix speed job in some locales. Fixes #346. <a href='http://commits.kde.org/kdenlive/dd928e87f6e77946609cf3097dd7ee53c4bfd255'>Commit.</a> </li>
<li>Fix some remaining effectstack layout issues. <a href='http://commits.kde.org/kdenlive/4fe87ef07851e3c86493ed65f9b11c8ebf0b5964'>Commit.</a> </li>
<li>Fix keyframes not deleted when clip start is resized/cut. <a href='http://commits.kde.org/kdenlive/83a4ab9a760ee492dcfa392120274fa111babfa8'>Commit.</a> </li>
<li>Fix track effects not working when a clip is added at end of track or if last clip is resized. <a href='http://commits.kde.org/kdenlive/4cd6df703cd6258636030b251e91ebb16223a7aa'>Commit.</a> </li>
<li>Add clickable field to copy automask keyframes. Fixes #23. <a href='http://commits.kde.org/kdenlive/21382f010f2f57eadb6a1caf3b7550c9017645ef'>Commit.</a> </li>
<li>Show track effect stack when clicking on it's name. <a href='http://commits.kde.org/kdenlive/1bfca966cb6096ee5977fdb58297559c84ef3b3d'>Commit.</a> </li>
<li>Fix crash trying to access clip properties when unavailable. <a href='http://commits.kde.org/kdenlive/61d2b8508d849ff91ff952a5ca768dceaf6c6764'>Commit.</a> </li>
<li>Fix effectstack layout spacing issue introduced in recent commit. <a href='http://commits.kde.org/kdenlive/dfe4b56c3c372603f63131bfb799f917aea2c1f9'>Commit.</a> </li>
<li>Fix proxy clips lost on opening project file with relative path. <a href='http://commits.kde.org/kdenlive/dd9cfe332784fac10e826f07bb4f8b58eedcd6c2'>Commit.</a> </li>
<li>Update AppData version. <a href='http://commits.kde.org/kdenlive/c57414c40a6378625ae870343540ffaff6bc7238'>Commit.</a> </li>
<li>Cleanup effectstack layout. Fixes !58 #294. <a href='http://commits.kde.org/kdenlive/9cc83e804bac6ee803dddb45361224a2a9e96151'>Commit.</a> </li>
<li>Fix mixed audio track sorting. <a href='http://commits.kde.org/kdenlive/15ddd416647f4727ce6bb3bad39cc6500c78b272'>Commit.</a> See bug <a href='https://bugs.kde.org/411256'>#411256</a></li>
<li>Another fix for speed effect. <a href='http://commits.kde.org/kdenlive/0e5b1d7e2f969d880e4618ca1af60823895db6b6'>Commit.</a> </li>
<li>Speed effect: fix negative speed incorrectly moving in/out and wrong thumbnails. <a href='http://commits.kde.org/kdenlive/c03ccad4960bd0ea6d3c03f1ad41f1ff76c21838'>Commit.</a> </li>
<li>Fix incorrect stabilize description. <a href='http://commits.kde.org/kdenlive/e716e00e40ca941123324056a034c7398e083dcc'>Commit.</a> </li>
<li>Cleanup stabilize presets and job cancelation. <a href='http://commits.kde.org/kdenlive/2e5007fd6546b17fc547b61e787f71c951e547c8'>Commit.</a> </li>
<li>Deprecate videostab and videostab2, only keep vidstab filter. <a href='http://commits.kde.org/kdenlive/3ed27aacfc1e97ff5df8fe1214880681e8401fe0'>Commit.</a> </li>
<li>Fix cancel jobs not working. <a href='http://commits.kde.org/kdenlive/17a7e6f4088d71c3d7989210175aa8c440bbf5d3'>Commit.</a> </li>
<li>Fix some incorrect i18n calls. <a href='http://commits.kde.org/kdenlive/8aa5a69b2188cf991102dd427bfeed0e66d19656'>Commit.</a> </li>
<li>Don't hardcode vidstab effect settings. <a href='http://commits.kde.org/kdenlive/5c22efbbbebd686ba194d503100f4e629c202c89'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix Bug 317177 - HTML emails CSS should not be applied to the header in th UI. <a href='http://commits.kde.org/kdepim-addons/7fc59ab359d08c828e1e067c7abfae1af1409db2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/317177'>#317177</a></li>
<li>Port to QComboBox::textActivated to fix compilation with Qt 5.14. <a href='http://commits.kde.org/kdepim-addons/4b5671766012587b4db09d87e6c1d639d3f2ebca'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix the dependencies on generated files differently. <a href='http://commits.kde.org/kdepim-runtime/8ec4db907c403b24685a961b48be5f8a288d9bd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410474'>#410474</a></li>
<li>Ews: fix unnecessary reauthentication prompts. <a href='http://commits.kde.org/kdepim-runtime/532bfebb5380cb92b8403e83deb0ad833c7b5f28'>Commit.</a> </li>
</ul>
<h3><a name='kfind' href='https://cgit.kde.org/kfind.git'>kfind</a> <a href='#kfind' onclick='toggle("ulkfind", this)'>[Hide]</a></h3>
<ul id='ulkfind' style='display: block'>
<li>Really use KDE_APPLICATIONS_VERSION. <a href='http://commits.kde.org/kfind/c0dd1ffb683a62a0f02410486f3823301442f373'>Commit.</a> </li>
</ul>
<h3><a name='kgoldrunner' href='https://cgit.kde.org/kgoldrunner.git'>kgoldrunner</a> <a href='#kgoldrunner' onclick='toggle("ulkgoldrunner", this)'>[Hide]</a></h3>
<ul id='ulkgoldrunner' style='display: block'>
<li>Fix compile error on Windows. <a href='http://commits.kde.org/kgoldrunner/be77f2fe56bb7006a131de1cd8838ff6c8169cf5'>Commit.</a> </li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Hide]</a></h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Define the application version in a more consistent way. <a href='http://commits.kde.org/khelpcenter/c612099afa3edf20817609dbd2fa8f7a97182f92'>Commit.</a> </li>
</ul>
<h3><a name='kimagemapeditor' href='https://cgit.kde.org/kimagemapeditor.git'>kimagemapeditor</a> <a href='#kimagemapeditor' onclick='toggle("ulkimagemapeditor", this)'>[Hide]</a></h3>
<ul id='ulkimagemapeditor' style='display: block'>
<li>Fix build with cmake < 3.12. <a href='http://commits.kde.org/kimagemapeditor/5a1fe15780838b5b83070ae4e9d012dab336fe9d'>Commit.</a> </li>
<li>Enable QURL_NO_CAST_FROM_STRING and fix compilation. <a href='http://commits.kde.org/kimagemapeditor/497c907ad963430b8619a34418a3118c1b74f4f9'>Commit.</a> </li>
</ul>
<h3><a name='kiriki' href='https://cgit.kde.org/kiriki.git'>kiriki</a> <a href='#kiriki' onclick='toggle("ulkiriki", this)'>[Hide]</a></h3>
<ul id='ulkiriki' style='display: block'>
<li>Fix links. <a href='http://commits.kde.org/kiriki/350d203094d8802e74c2164487d50007cea44778'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Fix UTC conversion during date/time parsing. <a href='http://commits.kde.org/kitinerary/6bc0898e77f7e25bf0451c65ecd0270890babee7'>Commit.</a> </li>
<li>Add another date format fallback for a new Lufthansa date format. <a href='http://commits.kde.org/kitinerary/17815e5da6daba6189c4874cd1af6b64fcbcf0f4'>Commit.</a> </li>
<li>Add unit test for JSON-LD date/time parsing. <a href='http://commits.kde.org/kitinerary/8676d60eb4abe94403cb4614a0d01efd90fb9d8f'>Commit.</a> </li>
<li>Fix date parsing in SNCF confirmation emails. <a href='http://commits.kde.org/kitinerary/1bec3fd915adfc63c55d9b9e478718b6aaaa23fd'>Commit.</a> See bug <a href='https://bugs.kde.org/404451'>#404451</a></li>
<li>QLatin1Literal => QLatin1String to fix compilation with Qt 5.14. <a href='http://commits.kde.org/kitinerary/5c545b87eba574f5dde93b2ac4fdb69cf8bf7461'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 412122 - Crash when closing the composer. <a href='http://commits.kde.org/kmail/6198b6426615a58f8a667010baf5de43669a1fda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412122'>#412122</a></li>
<li>Fix hidpi support. <a href='http://commits.kde.org/kmail/dcbff7ad2a2b2882dd34b8460952c8e0031605ac'>Commit.</a> </li>
<li>FIX Bug 411836 - Folder settings for expired message stores bad values (maildir). <a href='http://commits.kde.org/kmail/875a00a4ad3d6bd8a83ff665328987f487cdcafb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411836'>#411836</a></li>
<li>Fix compilation with Qt 5.14. <a href='http://commits.kde.org/kmail/671da7dec6b360cf0c53a02bb53dd327cac81367'>Commit.</a> </li>
</ul>
<h3><a name='kmix' href='https://cgit.kde.org/kmix.git'>kmix</a> <a href='#kmix' onclick='toggle("ulkmix", this)'>[Hide]</a></h3>
<ul id='ulkmix' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kmix/1a33a1867c0e0241ce47e5e78dbed9110242fea1'>Commit.</a> </li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Hide]</a></h3>
<ul id='ulkmplot' style='display: block'>
<li>Implement third derivative and loosen the mesh a bit to calculate the fourth. This should allow to calculate extrema for second derivative right. <a href='http://commits.kde.org/kmplot/6e3c471fbdbad5cc249d623255519742adc2e26e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/304574'>#304574</a></li>
</ul>
<h3><a name='knights' href='https://cgit.kde.org/knights.git'>knights</a> <a href='#knights' onclick='toggle("ulknights", this)'>[Hide]</a></h3>
<ul id='ulknights' style='display: block'>
<li>Fix problem starting games when second player is computer engine. <a href='http://commits.kde.org/knights/ca88165a2e0383afa13e784e480db2953a3d1d24'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411825'>#411825</a></li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Fix icon. <a href='http://commits.kde.org/knotes/f46fc509827cb7323f06d933cf971b3b0325a467'>Commit.</a> </li>
</ul>
<h3><a name='kolf' href='https://cgit.kde.org/kolf.git'>kolf</a> <a href='#kolf' onclick='toggle("ulkolf", this)'>[Hide]</a></h3>
<ul id='ulkolf' style='display: block'>
<li>Add homepage. <a href='http://commits.kde.org/kolf/2236bf91e9482c55cd2ebdfb2ad0457694dfaa82'>Commit.</a> </li>
</ul>
<h3><a name='kollision' href='https://cgit.kde.org/kollision.git'>kollision</a> <a href='#kollision' onclick='toggle("ulkollision", this)'>[Hide]</a></h3>
<ul id='ulkollision' style='display: block'>
<li>For sure TRANSLATION_DOMAIN can't be kdebugsettings :). <a href='http://commits.kde.org/kollision/d1c515b3634c402849540170eded1d22faa1e38c'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Allow loading of pre 19.08 session files. <a href='http://commits.kde.org/konsole/1561f6203bbf5e97cc23e36200e68486f3decac0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412173'>#412173</a></li>
<li>Keep empty lines when "trim leading spaces" is enabled. <a href='http://commits.kde.org/konsole/67a29e4134ff80cb64291b84ebeea27a1b247fd5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412479'>#412479</a></li>
<li>Enable hidpi auto scaling. <a href='http://commits.kde.org/konsole/54820e0ff2745add8b8353f538ad67d66b657d49'>Commit.</a> See bug <a href='https://bugs.kde.org/373232'>#373232</a></li>
<li>Anti-alias line character painting, too, with the text aliasing config options. <a href='http://commits.kde.org/konsole/36cd1aa267cf616d8135e38f4da7e4142f64059a'>Commit.</a> See bug <a href='https://bugs.kde.org/373232'>#373232</a></li>
<li>Fix many of the rendering artifacts for hi-dpi setups. <a href='http://commits.kde.org/konsole/a3bc2f9667696a37b291852a2df851433cc4377b'>Commit.</a> See bug <a href='https://bugs.kde.org/373232'>#373232</a></li>
<li>Correct split view and keyboard navigation. <a href='http://commits.kde.org/konsole/b9d2cd668bc0e5bb65316419da0dbcf33dcc0799'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411387'>#411387</a></li>
<li>Fix split view crashes when using keyboard navigation with some themes. <a href='http://commits.kde.org/konsole/ba84640a7bd872831f8330540081f7ddd0ff308b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411387'>#411387</a></li>
<li>Make sure font weight value is within allowed range. <a href='http://commits.kde.org/konsole/64cc52754baa45f817fea73bca6cdae3f1ec0e8d'>Commit.</a> </li>
<li>Fix rendering artifacts introduced by calling winId. <a href='http://commits.kde.org/konsole/4056588da2421144ee84deac791400a0f3c0f9b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411965'>#411965</a></li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Port away from deprecated methods in Qt 5.14. <a href='http://commits.kde.org/korganizer/594b043af098d5028f0680f539dc2d81e3dd947a'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Port away from deprecated QWheelEvent API to fix compilation with Qt 5.14. <a href='http://commits.kde.org/kpimtextedit/e2bf4627c4b568d32048fb005ec4fe063fd3f41e'>Commit.</a> </li>
</ul>
<h3><a name='kreversi' href='https://cgit.kde.org/kreversi.git'>kreversi</a> <a href='#kreversi' onclick='toggle("ulkreversi", this)'>[Hide]</a></h3>
<ul id='ulkreversi' style='display: block'>
<li>Fix compile on Windows (more). <a href='http://commits.kde.org/kreversi/865a87c53146f69547a2516a92fe79d7c54af302'>Commit.</a> </li>
<li>Fix compile on windows. <a href='http://commits.kde.org/kreversi/6048355a014ad7ddd0c591bdd810ab76aa565430'>Commit.</a> </li>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kreversi/dba6f3fdb4be4cd059237ab51a4c70f212a24b62'>Commit.</a> See bug <a href='https://bugs.kde.org/407701'>#407701</a></li>
</ul>
<h3><a name='kruler' href='https://cgit.kde.org/kruler.git'>kruler</a> <a href='#kruler' onclick='toggle("ulkruler", this)'>[Hide]</a></h3>
<ul id='ulkruler' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kruler/ee30dae6b743df4548e6b23d78869b2d3582fdb9'>Commit.</a> </li>
</ul>
<h3><a name='ktnef' href='https://cgit.kde.org/ktnef.git'>ktnef</a> <a href='#ktnef' onclick='toggle("ulktnef", this)'>[Hide]</a></h3>
<ul id='ulktnef' style='display: block'>
<li>Port to QString::asprintf to fix compilation with Qt 5.14. <a href='http://commits.kde.org/ktnef/cb0264976fc6079eee9d7c68dbddf77caf05a7ea'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Hide]</a></h3>
<ul id='ulkwordquiz' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/kwordquiz/94eb7217e4bd9fce6dc6f4ba57b30278a15fa783'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Fix null pointer dereference, keep FileModifyJob metadata and serialize with options. <a href='http://commits.kde.org/libkgapi/2cbbdda752270cb505465eb789938fbd0fa3ae51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411634'>#411634</a></li>
<li>Fix compilation with Qt 5.14. <a href='http://commits.kde.org/libkgapi/e4ef79c3849010003aa9dc0f7356d04b7e2bb409'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>[KSieve] Fix messagebox about connection to server lost. <a href='http://commits.kde.org/libksieve/60cf49cc24f48f8e519139f72a2a1f8850b2b8f5'>Commit.</a> </li>
<li>Use new signal QComboBox::textActivated to fix compilation with Qt 5.14. <a href='http://commits.kde.org/libksieve/8c1b7b8e3fd8c3abdae5874c52e6c85155f55aac'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Fix crash when a modified document and not saving changes. <a href='http://commits.kde.org/lokalize/cd05ad7feb33b8337d397758408228e9cb6dcfb8'>Commit.</a> </li>
<li>Don’t disable the context menu for the source pane. <a href='http://commits.kde.org/lokalize/7758e999b76a1533a76648d0178eaaa72649f585'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/315462'>#315462</a></li>
<li>Take msgctxt into account when comparing old and new strings. <a href='http://commits.kde.org/lokalize/d6f6eeb0b83312d85fc753128b4aedb2017d67ae'>Commit.</a> </li>
<li>Remove duplicate newlines in diff view. <a href='http://commits.kde.org/lokalize/499f25167e29b642666c5de68a1e2a0ef16674e8'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix typo. <a href='http://commits.kde.org/mailcommon/e1a608ea3f71f29db51c8e43f5174efacaaa1637'>Commit.</a> </li>
<li>Fix Bug 411836 - Folder settings for expired message stores bad values (maildir). <a href='http://commits.kde.org/mailcommon/32245181b742729af8f2027704eb70dddadf0302'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411836'>#411836</a></li>
<li>Connect to new signal QComboBox::textActivated, to fix compilation with Qt 5.14. <a href='http://commits.kde.org/mailcommon/e93fe44f8e2e571fc1f7579d8e4335ec44c027b9'>Commit.</a> </li>
<li>Port from QString::sprintf to arg(), to fix compilation with Qt 5.14. <a href='http://commits.kde.org/mailcommon/2494c693d1d9d777ff574e08e2920875610ee74f'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix Bug 411822 - "Save As" option on email message does not show KIO Remote Folders. <a href='http://commits.kde.org/messagelib/d5e666ea94b45b894d7b685d896045e9e4e171d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411822'>#411822</a></li>
<li>Fix Bug 411769 - Enter more recipients than permitted - error message for each recipient. <a href='http://commits.kde.org/messagelib/7605f3b754c523a6e473dbedc6fb3dc2745f09f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411769'>#411769</a></li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix hi-dpi scaling. <a href='http://commits.kde.org/okular/96fec4acb9531470f1c8d787d31aab42caafcfbb'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Port away from QString::sprintf() to fix compilation with Qt 5.14. <a href='http://commits.kde.org/pim-data-exporter/ee0ab11b939741b99d278828e208a723bbf8eec6'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>FIX: Properly close spectacle after Save As. <a href='http://commits.kde.org/spectacle/e98249c30917ef84f3f982b29c33dc90714fdb45'>Commit.</a> </li>
<li>Make QuickEditor fullscreen on Wayland. <a href='http://commits.kde.org/spectacle/5679f7f5c9bc56e59744e9e4045dd01d7142fbfb'>Commit.</a> </li>
</ul>
