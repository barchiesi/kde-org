<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Plasma 5.7.5, bugfix Release for September");
  $site_root = "../";
  $release = 'plasma-5.7.5';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/A9MtFqkRFwQ?rel=0" frameborder="0" allowfullscreen></iframe>
<figcaption><?php i18n("KDE Plasma 5.7 Video");?></figcaption>
</figure>
<br clear="all" />

<figure style="float: none">
<a href="plasma-5.7/plasma-5.7.png">
<img src="plasma-5.7/plasma-5.7-wee.png" style="border: 0px" width="600" height="338" alt="<?php i18n('Plasma 5.7');?>" />
</a>
<figcaption><?php i18n("KDE Plasma 5.7");?></figcaption>
</figure>


<p>
<?php i18n("Tuesday, 13 September 2016. "); ?>
<?php i18n("Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.5.
<a
href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a>
was released in July with many feature refinements and new modules to complete the desktop experience.
");?>
</p>

<p>
<?php i18n("
This release adds month' worth of new
translations and fixes from KDE's contributors.  The bugfixes are
typically small but important and include:
");?>
</p>

<ul>
<?php i18n("
<li>Plasma Workspace: Fix some status notifier items not appearing. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df4387a21f6eb5ede255ea148143122ae4d5ae9c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366283'>#366283</a>. Fixes bug <a href='https://bugs.kde.org/367756'>#367756</a></li>
<li>SDDM Config - Fix themes list with SDDM 0.14. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=d4ca70001222c5b0a9f699c4639c891a6a5c0c05'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128815'>#128815</a></li>
<li>Make sure people are not trying to sneak invisible characters on the kdesu label. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=5eda179a099ba68a20dc21dc0da63e85a565a171'>Commit.</a> </li>
");?>
</ul>

<a href="plasma-5.7.4-5.7.5-changelog.php">
<?php i18n("Full Plasma 5.7.5 changelog");?></a>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='https://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
