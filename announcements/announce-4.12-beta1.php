<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships First Beta of Applications and Platform 4.12");
  $site_root = "../";
  $release = "4.11.80";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php i18n("November 7, 2013. Today KDE released the beta of the new versions of Applications and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.");?>
</p>

<p align="justify">
<?php i18n("This release does not include Plasma Workspaces since it was frozen for new features in 4.11.x. The Development Platform has been virtually frozen for a number of releases, so this release is mainly about improving and polishing Applications.");?>
</p>

<p align="justify">
<?php i18n("A non complete list of improvements can be found in <a href='http://techbase.kde.org/Schedules/KDE4/4.12_Feature_Plan'>the 4.12 Feature Plan</a>.");?>
</p>

<p align="justify">
<?php i18n("With the large number of changes, the 4.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.12 team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.");?>
</p>


<h3><?php i18n("KDE Software Compilation 4.12 Beta1");?></h3>
<p align="justify">
<?php i18n("The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.11.80/'>http://download.kde.org</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.");?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing 4.12 Beta1 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12 Beta1 (internally 4.11.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12_Beta_1_.284.11.80.29'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling 4.12 Beta1");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for 4.12 Beta1 may be <a href='http://download.kde.org/unstable/4.11.80/src/'>freely downloaded</a>. Instructions on compiling and installing 4.11.80 are available from the <a href='/info/4.11.80.php'>4.11.80 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
