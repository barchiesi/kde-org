<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.6.0 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.6.0';
include "header.inc";
?>
<p><a href="plasma-5.6.0.php">Plasma 5.6.0</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Sendfile.desktop: Remove MimeType. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=7f973794ab7ef35b37601a1e8ca541027f11dc5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360292'>#360292</a></li>
<li>Applet: Highlight items on hover. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=666a7146766a83d84e800677be0843d7d3dcdf93'>Commit.</a> </li>
<li>Fix CMP0063 warning. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=9bb8a50c1cf07949496f96f2c728f88a67e2dbd7'>Commit.</a> </li>
<li>Add link to "Downloaded Files" directory in kio bluetooth. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=1f0dc2e3dcd2f098089c0561322e9b5ebf052551'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/108503'>#108503</a>. Fixes bug <a href='https://bugs.kde.org/302964'>#302964</a></li>
<li>Fix some Crazy warnings. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=b9073443792b97f03a89d61d1223a605d5acdc3d'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>[kstyle] Disable window move on Wayland. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f052173ddbd35e3b34c6714736f31751fd230fa1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127301'>#127301</a></li>
<li>[kstyle] Add static bool Helper::isWayland(). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=331a7d7f1e601716988c4ed0151e490f2ff0a623'>Commit.</a> </li>
<li>[kstyle] Cache whether we are on X11. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ff04d0418a266a6a80a27b751781712401ceb1b5'>Commit.</a> </li>
<li>Fix installation of Breeze Dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=42b02dd7214432a9bb7c451360684d417c26869d'>Commit.</a> </li>
<li>Only build the decoration if the KDecoration2 is present. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=fa748e869ddcd4a75f314bfb2ba85b879a3b7e1d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127174'>#127174</a></li>
<li>Use proper background color for tabbar arrow buttons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9920983f869b3e3654c97ea4b623c0f4e8c8fae7'>Commit.</a> </li>
<li>Properly enable mouse-ove in tabbar arrows. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ac1a56fb5b737086967cba9931434d1ce1425b37'>Commit.</a> </li>
<li>Check whether parent has altered background to decide of tabbar's background in document mode. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ceeae5f513b9efe3c9fd215c96b22d91a60cda6a'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d616fcb060707a877f935c53ba1e9eece993716d'>Commit.</a> </li>
<li>Add a complementary group. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=018860bcfbf3f00b44cc5b813fa0757879198e65'>Commit.</a> </li>
<li>Honor showIconsInMenuItems. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1ea115411f2492145db0531ee5792c9bf563df13'>Commit.</a> </li>
<li>- calculate pushbutton size from scratch rather than relying on the contentsSize calculated from Qt. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5367cb88f00251449caff8905f93564311f3eb58'>Commit.</a> </li>
<li>SH_DialogButtonBox_ButtonsHaveIcons: return true. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=22c1ded25f81d59ced32f9be1f2198df00b4187f'>Commit.</a> </li>
<li>REVIEW:126319 Plasma 5.6 Graphite Wallpaper (minor pixel fix). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9e61463161c5bd7ccc8a6dc6add150e1b8a394d9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126319 Plasma 5.6 Graphite Wallpaper (minor pixel fix)'>#126319 Plasma 5.6 Graphite Wallpaper (minor pixel fix)</a></li>
<li>REVIEW:126319 Plasma 5.6 Graphite Wallpaper. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=68c839ab937e96680b45df5b8f9e1cc5e547bc50'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126319 Plasma 5.6 Graphite Wallpaper'>#126319 Plasma 5.6 Graphite Wallpaper</a></li>
<li>Nothing. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d50deac70c53bcebcdf3267fd8a08c7d3919cbe2'>Commit.</a> </li>
<li>For standard configuration, rely on KCoreConfigSkeleton::load and ::save. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7c8c117cea387d3e3047702ae410055faabceeb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357135'>#357135</a></li>
<li>Call updateButtonGeometry after decorationButtonsLeftChanged and decorationButtonsRightChanged. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ed08414b124e85b895ad3f553d590377c27af131'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356869'>#356869</a></li>
<li>Improved rendering of tabwidgets in qtquick. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1e98e96a015a73eb4320d9df63adface18858082'>Commit.</a> </li>
<li>Sanitize rect adjustment for rendering tabwidgets. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f30f2fdd406f5ef120de14fa9e66dca5896148a0'>Commit.</a> </li>
<li>Moved detection of qtquickcontrol to separate method. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0cfd1cd14b5c30291e5a729852199142aa96dbac'>Commit.</a> </li>
<li>- better handling of custom property for isMenuTitle. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=716d738cc0a060a76d0dbe0b496f3adc89be3132'>Commit.</a> </li>
<li>Moved all abstract scrollarea polishing to polishScrollArea. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c50b113d374664c44eb5e9fe0fb1243eb3b50f25'>Commit.</a> </li>
<li>- Removed palette helper. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c5e48d765ab7ea71b0a4e30fe6482ed7e7be026f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356561'>#356561</a>. Fixes bug <a href='https://bugs.kde.org/356343'>#356343</a></li>
<li>Cleanup shadowhelper's widget registration logic. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a513f4e0c41e402d69af821f9923c9cb665f804f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356625'>#356625</a></li>
<li>Do not set antialiasing when rendering square menu frames. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6282cbb242016b37451a65f6b66055524a0831f9'>Commit.</a> </li>
<li>Added ViewInvertSortIndicator hidden option (to be added to $HOME/.config5/breezerc) in order to changed sort. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c327915a8d864501ff0c7f701693225d457a8369'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356305'>#356305</a></li>
<li>Draw outline in anti-aliasing mode, to prevent darker pixel in the corner. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=27b476d319a5869d1052baae097eda4c17ad0b50'>Commit.</a> </li>
<li>When alpha channel is not supported,. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=10145c7bb2f7ac791d8c3bc072cb9e644b8afa8d'>Commit.</a> See bug <a href='https://bugs.kde.org/356165'>#356165</a></li>
<li>Make separator below menu title item as wide as regular separator. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=251c2003ddf0d151398198df4d67250015d7b1b5'>Commit.</a> </li>
<li>- use "metrics" to set corner radius. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=21c233de4c79586b410aa2de6d8b5b480f63c9e2'>Commit.</a> </li>
<li>Increased radius for mask. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b23f3598b610aa00045d188e2010a65608c61a67'>Commit.</a> </li>
<li>Removed incorrect composition mode. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=a9b7b5c27d7aae0605e5556154416cc3861c6e11'>Commit.</a> </li>
<li>Reduce strength of the contrast pixel. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=10a66f22ea718114b67182b10326b8f4b28a46de'>Commit.</a> </li>
<li>Takeout inner part of the shadow to avoid artifacts for semi-transparent windows, and. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e40195693c3423101ca91f2f2e4e5c7a6f4d7724'>Commit.</a> </li>
<li>Use similar code for rendering the decoration shadow and menu shadows. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=54fb9fe29c18c740e623f9bae258bbf4ebbc41ba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355647'>#355647</a></li>
<li>Properly mask out the inner part of the shadows (that overlaps with e.g. menus), to prevent artifacts when translucency is enabled. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=48321e81f74f10cb3b7edd882a243fbde5c2cb3d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355695'>#355695</a></li>
</ul>


<h3><a name='breeze-grub' href='http://quickgit.kde.org/?p=breeze-grub.git'>Breeze-grub</a></h3>
<ul id='ulbreeze-grub' style='display: block'><li>New in this release</li></ul>
<h3><a name='breeze-gtk' href='http://quickgit.kde.org/?p=breeze-gtk.git'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Gtk3: Fix wrong filename of scrollbar. <a href='http://quickgit.kde.org/?p=breeze-gtk.git&amp;a=commit&amp;h=0a3dda7c1717f672eee4e8bc6655a7d26561b465'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127323'>#127323</a>. Code review <a href='https://git.reviewboard.kde.org/r/126970'>#126970</a>. Fixes bug <a href='https://bugs.kde.org/356395'>#356395</a></li>
<li>Do not install development helper files via cmake. <a href='http://quickgit.kde.org/?p=breeze-gtk.git&amp;a=commit&amp;h=673f3e18af95ff2c4af0a999fd1ed00176e05516'>Commit.</a> </li>
</ul>


<h3><a name='breeze-plymouth' href='http://quickgit.kde.org/?p=breeze-plymouth.git'>Breeze-plymouth</a></h3>
<ul id='ulbreeze-plymouth' style='display: block'><li>New in this release</li></ul>
<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Change background gradient color. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e9a529144a685987548a81bcdac4a7f86082d0e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360299'>#360299</a>. Code review <a href='https://git.reviewboard.kde.org/r/127394'>#127394</a></li>
<li>Improve how we react to failed parses. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d0728b9a774c9128ac44f9b9dfac9d2ac3205c6b'>Commit.</a> </li>
<li>Fix refresh on the notifier plasmoid for QApt backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=749f5be5ebc7b56143e9952d085f396f3bd65905'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347602'>#347602</a></li>
<li>Don't count security updates twice. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ec814134261c282f2513f65f8aa511f10c825d31'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347284'>#347284</a>. Code review <a href='https://git.reviewboard.kde.org/r/126284'>#126284</a></li>
<li>Fix QApt's HistoryView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=125caa4c923a622cf5e621a4a92078334c7f9e44'>Commit.</a> </li>
<li>Mark methods as override. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=67e67b09abf5e7b39150174d97ddc496536503fd'>Commit.</a> </li>
<li>Slightly improve the test framework. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c41eb29f126557a8bc8383dae4f467ef4129e227'>Commit.</a> </li>
<li>Make sure the ResUpdatesModel adopts backends as they become ready. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c9ac24c26402d8eca2a1b02dae91a7c999b604e5'>Commit.</a> </li>
<li>Verify that signals have been emitted and we're not timing out. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=430cebc26e496cc5dab166e999085b503c4fc846'>Commit.</a> </li>
<li>Provide a transaction for the global update process. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0bca89a8ec846fd322045a9e339b93f7b08fd271'>Commit.</a> </li>
<li>Simplify initialization. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=40ae44e5739e53579c1a55708339bed282a0492c'>Commit.</a> </li>
<li>Remove updates kded interface. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3fb903087741de2b120c4df22b297f2c82b5a0f6'>Commit.</a> </li>
<li>Code cleanup. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=63001377170eabba7084b22e415d2afda6be1afb'>Commit.</a> </li>
<li>Assume that it's the full view before view is shown. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=999a8456de2fce180aafb49185d5876f12eb711b'>Commit.</a> </li>
<li>Make libDiscoverNotifiers private too. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6e92f5638ddd0a0ff257d2f425d1bbcbb35eba32'>Commit.</a> </li>
<li>Make libdiscover a private library. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7e099790f3603fd0bf343fb85deab3a1425b8972'>Commit.</a> </li>
<li>Fixuifiles. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=28ca3909896e4fdec53efa9cf5711ff850903076'>Commit.</a> </li>
<li>Actually test that the window can't be closed while updating. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=b3b08540dbf52df6325e0e08ebea89279a51aab8'>Commit.</a> </li>
<li>Also don't hide the window when busy. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dc647ab0d49012d20e5d69c1b3b359de59ea0746'>Commit.</a> </li>
<li>Fix some warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5e9d2788088b0337a64386b57a2fbf99e2cc3581'>Commit.</a> </li>
<li>Don't check for window.stack.currentItem twice in the same property. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dbeb1785ac9e06a4517da0db1b1a50189f42049b'>Commit.</a> </li>
<li>Don't crash if we check the Version for non-versioned update items. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9f5cff0e5c4967aa84bb2355a5dd0041fcb79b76'>Commit.</a> </li>
<li>Don't show the ApplicationsTop role delegate if it's too big. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ab260380d6926599064db352cd5ec0a35d92d94d'>Commit.</a> </li>
<li>Update bugzilla component names to reflect product renaming. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8f95f1b711ce286f1846c993e7330cdcad88e503'>Commit.</a> </li>
<li>Define the PackageKit backend as the main one. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c7a5f42b274255c40260d24ca2dcf3e0623866cf'>Commit.</a> </li>
<li>Fix build of QApt backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c256d0e30a0c189d6106f439041db1b4ddf870e3'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6ec76bbb4562058dc0c474bab9cc16d8f8374304'>Commit.</a> </li>
<li>No need to instantiate the values list. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=48d5a73f05ff799be531e217c27122ffbca5ffae'>Commit.</a> </li>
<li>Prefer smart pointers to managing memory. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fae239456a70454bb33365d5eae421436fa22ff2'>Commit.</a> </li>
<li>Improve UpdateModel code. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ee576df36b372c2171846502134775220a2346fd'>Commit.</a> </li>
<li>Only make the cursor change on the apt backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=48ac5e9a785d324f94fa6aa8f74a0af762cba4b6'>Commit.</a> </li>
<li>Make sure that UpdatesModel is consistent. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=57e7e097db49cac6776d6d7605213aa159a3d795'>Commit.</a> </li>
<li>Extend coverage for KNS backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4e9dbc68e313e66c13b0acdccbd802d457c82958'>Commit.</a> </li>
<li>Addons support makes us require AppStream 0.9.2. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=678809e969dfebf5834e97fb291c07ffefb8142e'>Commit.</a> </li>
<li>Support addons for PackageKit resources. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=86aa5f01328a4f8c2990fd74249db34c9f081b40'>Commit.</a> See bug <a href='https://bugs.kde.org/359042'>#359042</a></li>
<li>Let the test go through the updates process. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0a456438e06ace42e9f4c28476d31f9193dfa1b6'>Commit.</a> </li>
<li>Make sure the tests wait for the window to render. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=cd94e20fdada498fb39d551e89da4766021e8c10'>Commit.</a> </li>
<li>Test ApplicationPage install button. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c0f00b08cb1422fd6cf179651a99be7bbc6a46af'>Commit.</a> </li>
<li>Move code exclusive to updater into updater. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=10dd14e80561d530c281d69a1c1b67a873676f37'>Commit.</a> </li>
<li>Make sure the UI object is destroyed before the rest of the infrastructure. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d6582724c0d4eed40a885692034786b628aac30a'>Commit.</a> </li>
<li>Remove unused function. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ae45793430d0c6823286a4cc97560c97d6a19b0c'>Commit.</a> </li>
<li>Fix CLI arguments response. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c9b9e72e2eb7beb61f7a4069605ec4809062e5f0'>Commit.</a> </li>
<li>Set up an initial testing infrastructure for the Discover front-end. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dd43ebc2e7054d138ee5ad979dbdde87c7992a60'>Commit.</a> </li>
<li>Fix close prevention. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6d4ea9b45a0178612f2eca95facede9b2ec83a31'>Commit.</a> </li>
<li>Don't show the description title if there's no description. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0d34e2c78998d4d15a3bba8909903c756f5b96a9'>Commit.</a> </li>
<li>Don't load the install button if the mouse isn't on the delegate. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=92f44dc183c2ebc1cd744ec80bf15b4a0a85b9fd'>Commit.</a> </li>
<li>Polish Grid delegate. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3653976f02c303aeb8bb81544d7e61061edff78a'>Commit.</a> </li>
<li>Make a SystemPalette singleton. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e04c4c76ef9a2901fcbbcdc5738748d036e75a48'>Commit.</a> </li>
<li>Make sure we destroy the pages we pop(). <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=53d113d39b8bcdfaebcb9dfc86833e6bd34d3654'>Commit.</a> </li>
<li>Add a drop shadow to the icon. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=462e2b35f934c52aafeaaa02480a6dda0502f049'>Commit.</a> </li>
<li>Polish the grid delegate. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=aa5caf7233f1f1722589173c53cc16121b2ac5f2'>Commit.</a> </li>
<li>Remove the screenshot frame as suggested by the VDG. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c47bff2748f10f03abf7af8f0a1cc9dfc2a674de'>Commit.</a> </li>
<li>Cleanup the category display. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1560f62ab920e59552977bf4bccd325f595598a1'>Commit.</a> </li>
<li>Don't do weird things when clicking the background of the CategoriesView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=166fdb0c5ace9300db0e03b208eb4e5509ef0c0e'>Commit.</a> </li>
<li>Increase margins in the ApplicationPage and the BrowsingPage. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9402cfad18bfaa5929876fd2e6cd5642765ace51'>Commit.</a> </li>
<li>Make sure the UI starts to get torn apart when the application closes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=494d10f7cf233d4a18fe7c5152603e0a7b959746'>Commit.</a> </li>
<li>Refactor the details component into Andrew's new proposal. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=19128cb0a4b72a96536fe6f004730c4c21e66191'>Commit.</a> </li>
<li>Let ratings be smaller on the main page. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=af20b17e7229c6de800126d556a7d3696fbbffd1'>Commit.</a> </li>
<li>Improve banner display. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=276693ca98956225e657065da2f939aefc1bff43'>Commit.</a> </li>
<li>Use the golden ratio, people love golden ratios. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e9c48dd225b4f9b2fd1908a3a820d6eb9792bcc0'>Commit.</a> </li>
<li>Show an install button while we don't have the FAB. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7bda56332ff5954f1729bf493c8cf4fc3798d48e'>Commit.</a> </li>
<li>Remove hardcoded values. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a9d5b18935119a7be5a8f30dba40a43ab138d463'>Commit.</a> </li>
<li>Make sure headers will fit its contents. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fba4ad045306b31f31aa1bbbfb989b2830c9c14a'>Commit.</a> </li>
<li>Remove hardcoded sizes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=16a63fb4268a108caecdbf8ba9afef8471177845'>Commit.</a> </li>
<li>Remove unused code. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0e8e83bd6f50036ee50990f1abfd6f616a90b603'>Commit.</a> </li>
<li>Remove unneeded logic. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=db2de7cf15d109004e47e8e936befba4ce1dccb2'>Commit.</a> </li>
<li>Implement compact Launch Button. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ace7148ffb866c438d5b0dd80a74cc44f08d9a76'>Commit.</a> </li>
<li>Polishing ApplicationPage. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d1460e4f6c9bf0fefc5c8f8f0c8cf08531242637'>Commit.</a> </li>
<li>Position compact reviews page. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=93913f0e5b227a206ddae2137b4888a887b22c22'>Commit.</a> </li>
<li>Improve headers. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5b4a0b5bdc6833a2ac2bbe03d64d1c27149bb9dd'>Commit.</a> </li>
<li>Refactor the ApplicationPage. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4f45b67f663f221c3a08c469b69121206a958152'>Commit.</a> </li>
<li>Fix error, don't try to figure out a value before it's been set. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0b3e547f07ed151f610be7124f0d23ba620bd038'>Commit.</a> </li>
<li>Give a default URL within KDE to fetch the featured data from. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7cc892373547fd78f802ecc5a08d33f6b1607001'>Commit.</a> </li>
<li>Make sense of internal margins. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=00b3aa5a20c61e78ff993f0fad76ba59015a9946'>Commit.</a> </li>
<li>Don't let the user flick horizontally if it's a vertical view. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=25d92e74bd7b36cdcb51746796217980fd055b68'>Commit.</a> </li>
<li>Fix ApplicationScreenshots in a ScrollView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=bf27b73d8be8ed877117d2c2914feb7ab3f717e4'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9b7b0b2792bd8db499b773570a029818cf40446c'>Commit.</a> </li>
<li>Take into account in the page header the title font size. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c9bf3b32e3c1a42beb1e7deda29f7fc50752fbfa'>Commit.</a> </li>
<li>Improve compact representation of the ApplicationsList. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1ee19fe518dccd9df3ae8153ea328e3a0b18d546'>Commit.</a> </li>
<li>Improve Review dialog title. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=bad11bc7bd98d78775a4712fb488b4dbec79f6b2'>Commit.</a> </li>
<li>Unify the Heading component. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=86318fa34013fefd5f5898c27f585c5fa0aa44d8'>Commit.</a> </li>
<li>Make property readonly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d14952030b9fe4f2be627ce0e74d963fad4af957'>Commit.</a> </li>
<li>Swap the order between the button and the additionalItem on the app button. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fedb38860797ed9444854ca3448ae3efcbe52041'>Commit.</a> </li>
<li>Again, make sure the ApplicationHeader is different size proof. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5b0c19ca896c6af4830dd7e2779274f42137dd6a'>Commit.</a> </li>
<li>Use the font size to specify the height of the PageHeader. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8457a4f14c35747b4ebb3335368515f568ea4ad4'>Commit.</a> </li>
<li>Include missing file. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=28654ad752d5d1043f532bef970a217d0414981c'>Commit.</a> </li>
<li>Better match the VDG proposal for the application page. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d8ee58aaee0facf117f05e9e1020b4d8b71d298d'>Commit.</a> </li>
<li>Specify the search context in the search text field placeholder. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6038d0961677155f596c5230a54368f117502481'>Commit.</a> </li>
<li>Make search bar slightly bigger. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=562c09cfdf2238bada6187d866250c7e2866c9cc'>Commit.</a> </li>
<li>Let the Rating star size depend on the font size. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7675c13681bcc780ba2ea261be15975adc5c0752'>Commit.</a> </li>
<li>Let the grid size depend on the font size. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a2e5fe5af618f2c5c0a8e64f59e6742362c6d6bd'>Commit.</a> </li>
<li>Only show the Categories title if we're listing subcategories. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dae1381f668136d5621a49d5baa7801a4ea38704'>Commit.</a> </li>
<li>Solve binding loop. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=2a3ed33e85e75b7f1347f10ea3381ef2b96b399d'>Commit.</a> </li>
<li>Don't add an ellipsis to the comment. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d18f6ba742cd30d845cdc7b9d417fbfb957c102c'>Commit.</a> </li>
<li>Resolve binding loop. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fc0991e7fc0cecda1bfd520e8c30bbbf5b3231ce'>Commit.</a> </li>
<li>Don't use fixed size for Breadcrumbs separators. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a2faa67f69fdbc61a0faa71c9d97cc09696f208d'>Commit.</a> </li>
<li>Remove hardcoded sizes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=bbbaf13afc352c1c8bf05c162bb2f72331dc3dcb'>Commit.</a> </li>
<li>Remove Layout attached properties forwarding. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=72ef12da4320e54c23f3a282e00b393340139bb9'>Commit.</a> </li>
<li>Revert "Adapt Discover desktop files to the different form factors". <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0eda314555a538d587ce024cce7d24f49eb298cb'>Commit.</a> </li>
<li>Don't load the compact mode if it's not available. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=33160a19dca6a245550514f992d2ad4ad724427a'>Commit.</a> </li>
<li>Fix comment. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=37d6df9a5602dc459460f75fec31e2dff84ebd62'>Commit.</a> </li>
<li>Let the layout specify the size of the component. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=760bc63f93e904d9e81a8ad335263d714b3794dd'>Commit.</a> </li>
<li>Reduce stress on icon height for the applications top. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d822cdc444364f13b0811acbee3fd6f22fe62d5b'>Commit.</a> </li>
<li>Fix naming. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f543304cd9203f1423b23ade7812830616199b46'>Commit.</a> </li>
<li>Use the same Drawer delegate as PlasmaMobile. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1e5c0778294049d561e60a638607f1caa6d48ada'>Commit.</a> </li>
<li>Adapt Discover desktop files to the different form factors. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=87a5baac1fe2ccd74a59a6ac35be7ceaa30f02eb'>Commit.</a> </li>
<li>Make const if it's not going to change. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1c1b7da5ed9772db2847453511ffdc0ddc15dd7f'>Commit.</a> </li>
<li>Remove the fancy margins on the Main page and the Category Grid. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=78df95687229c2beeda1c41247f5dc09f21c1388'>Commit.</a> </li>
<li>Make property readonly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=232b74cf4d5d6e19878e5b6f853feb20a566e428'>Commit.</a> </li>
<li>Make sure ApplicationsTop borders overlap. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=23cd6e12facb87baf9a889c874380750158ef5c7'>Commit.</a> </li>
<li>Simplify KNS backend loader. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=770ff6ee111f4a088c85a6dfa4d690bf908e187c'>Commit.</a> </li>
<li>Remove duplicated code. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=b00ebbcc620c101a3daf7cd73dc4f35d5c7f99c9'>Commit.</a> </li>
<li>Make sure we don't forget to disable fetching when marking invalid. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=155b5a90eb9dbb7e42bc73c5e34be9bb960c10aa'>Commit.</a> </li>
<li>Clean up AddonsView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ca356fa71804bae15352c3411d6b5e531eaad893'>Commit.</a> </li>
<li>Fix opening with application as an argument. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9647e1f3f1859457094fabaafea2e64116f76c5c'>Commit.</a> </li>
<li>--qml warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5c1687c97600e31c243e07523b5568bfadbdef8d'>Commit.</a> </li>
<li>Make it possible to have a non-frontend-proof packageName for addons. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a752fd205ab1ef8684c5b90763cb9d62354b70fb'>Commit.</a> </li>
<li>Crash guard. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9a59984eb4498281a4fc2f977de6aa834f6ba5a4'>Commit.</a> </li>
<li>--warning. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=68401dcbaccc92d1fbecd9aefee0367b51fd1c5c'>Commit.</a> </li>
<li>Make attribute const. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=aebd1eee869b1710b7d5cb1a5619c9ed90712d22'>Commit.</a> </li>
<li>Pass AddonList by reference. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=81b7aeb7ed2f67de67d09c6dae3fa982123bb895'>Commit.</a> </li>
<li>Get the PackageKitBackend state into a Packages tuple. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4f1cc125074c857834435460e6ac0611ef3d33aa'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=62fc09a0635d344f2344a021c28a8a8840164143'>Commit.</a> </li>
<li>No need to discard the plugin right away. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=976e09f6a14e94484534faf2a028fe10cb4265d2'>Commit.</a> </li>
<li>Make it compile with GCC < 5. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e651466574bc08a8afe5b8bff40d23070f94d434'>Commit.</a> </li>
<li>Mark KNS backend as invalid if no providers were found. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4c16aa4746afb332a127ceb13e99968b611ac9be'>Commit.</a> </li>
<li>Prevent potential crash. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ac187743a5b5269151c3f724ad54bdc3b3a06326'>Commit.</a> </li>
<li>Extend test. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=413d10b51c2406424f45b21e7fdc4bc0558a50c0'>Commit.</a> </li>
<li>We aren't using the appname from the review instance that wasn't necessary. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f6710b64b938d128e0de4eae114ad97a26317f27'>Commit.</a> </li>
<li>Disable the delegate if it's an empty item. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9448f801819fee0ec4a24900e141d232abd8623a'>Commit.</a> </li>
<li>Let the ApplicationsTop component initialize with the correct row count. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f438e6f778c74b94a0771bcb32b52dd5061d46a1'>Commit.</a> </li>
<li>Simplify code. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7940c8751bd94c9f21430dc411e4fc558c259d53'>Commit.</a> </li>
<li>Bring KDeclarative back. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=14a0791a7205ec60063f110032712c7cecee2a66'>Commit.</a> </li>
<li>Revert "Remove unneeded compile-time dependency to KDeclarative". <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=71d3c0f007f98b08b37eeb38eb915ddb5ab73cbe'>Commit.</a> </li>
<li>Remove unused dependencies. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ddf785135b40551cd2853e9db5bb6057eb38efa4'>Commit.</a> </li>
<li>Remove unused includes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3e0700cbe0c864a48bdcc3b4da761e61b1969aeb'>Commit.</a> </li>
<li>Remove unneeded compile-time dependency to KDeclarative. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a9ffac90983df860a1d15d91437043affb81ae45'>Commit.</a> </li>
<li>Fix warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9f2d6ca425bdafc24807f9ac4e2bc445bd7d9abc'>Commit.</a> </li>
<li>Restrict KIconThemes dependency within the APT backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5a8d55f1fc9c92d75b33fff7073c2c6868eb3bef'>Commit.</a> </li>
<li>Reduce dependencies. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c58fcfe00a02a8012f19888920e426c602936c40'>Commit.</a> </li>
<li>Drop KCategorizedSortFilterProxyModel dependency in CategoryModel. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f0786c96411bd4c09d6bd7f848fc4269c00f48ef'>Commit.</a> </li>
<li>Make sure the tool button gets disabled with the action. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=71b1cbacc1bc0651493a949ffd81b4fa287efe9f'>Commit.</a> </li>
<li>Implement per-package update progress for PackageKit. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f70a86140ae415201d69a3240bf814a91efca1ea'>Commit.</a> </li>
<li>Show progress for upgrades as well. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=537ffab10c1ec7383f9ba1206909a8ad2cc91062'>Commit.</a> </li>
<li>Be more respectful to the palette. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=81d2637f76e929d022495e27a6d2f8fdb3ab88e8'>Commit.</a> </li>
<li>Show un/installation progress on the installed page. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=95a0fc1927eaf2f1ea5c12271072cb0018603583'>Commit.</a> </li>
<li>Make it possible to have progress in the LabelBackground. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a16cb0762d3af85158f1993aae42dbb6c142d257'>Commit.</a> </li>
<li>Delay current page destruction. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=29c3ecb9455178f76c51a3e15d9fcaa330423464'>Commit.</a> </li>
<li>If there was an error, print it before acting on it. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4cadf634442c241ba25bf8a796bf047b5f908cf3'>Commit.</a> </li>
<li>Fix how we forward with a string property. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=56a1622504463b0b113554633be5b178304d8b43'>Commit.</a> </li>
<li>Dont show a "Last checked 0 milliseconds ago" message. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=93b8a8c6549c689cdec6ba7d35a3fcd3e0d679c5'>Commit.</a> </li>
<li>Prevent changing section as update is stateless for now. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3b937573ac9202b062506f8204324097e713b24d'>Commit.</a> </li>
<li>Trivial: Small .desktop file style fixes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=36a53af9d3ed70d1c53a3947e773b0384cbcb944'>Commit.</a> </li>
<li>Rename muon-discover to plasma-discover. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5599c1dd7b992d7ecf0279fce335168a683b8b10'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126680'>#126680</a></li>
<li>Use AppStream instead of appinstall-data in the QApt backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=97f1426d5806beae0456566e1cf6653341d64afa'>Commit.</a> </li>
<li>Fix display of the drawer. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=db37a00ad26988f84138743f481474324cb23014'>Commit.</a> </li>
<li>Mark stack property as readonly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f1c417b97fd9033f1653d76049b8568308f27aa2'>Commit.</a> </li>
<li>Reduce needed imports. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c9e27c8afcebecf6e65b23daddf0847ddf5909a8'>Commit.</a> </li>
<li>Remove unneeded import. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fbd73aa520848c18e7c72e2044843df8a4221b34'>Commit.</a> </li>
<li>Update reviewboardrc. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c31c89a4e76ce1472866d70108ab186cd2f0b3ca'>Commit.</a> </li>
<li>Fix warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e47d03bc851c5bc6f071f717bc420cf54944666c'>Commit.</a> </li>
<li>Make it possible to enforce the compact mode from the command line. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=83cb7b59e5a15136124c84cec99da97e17d70657'>Commit.</a> </li>
<li>Separe the main code between the compact and non-compact states. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=81e77fc3f8bcf94ca7fec8ed257405ad4c9c4b08'>Commit.</a> </li>
<li>Move the toolbar definitions to the main view. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a1b44fc35edd8bc816aedeed7f7504fe8bd25d8f'>Commit.</a> </li>
<li>--warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=12b2cec2c62734e82f910847a4aaefa03b77312a'>Commit.</a> </li>
<li>Cleanup includes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ac232072e109a1b322984182a88bb3fbf4ce4863'>Commit.</a> </li>
<li>Don't use QMenu magically on a QQC. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d39242cc540c1d5e686a9295629a7f1a76e21484'>Commit.</a> </li>
<li>Make properties as readonly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=31b87ac183399fbcd0459e10cb41904d04ee8943'>Commit.</a> </li>
<li>Drop kxmlgui requirement. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d07c5ace40bd695d9ad2253f68de3d55555e51d5'>Commit.</a> </li>
<li>Fix SystemFonts includes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6dfef4cd944cbaae4b5bb5f0679f3f101f3e023c'>Commit.</a> </li>
<li>Reduce kxmlgui dependencies. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0d230bb04b4d4c9ac6e8b23fccadeb4d4169a53f'>Commit.</a> </li>
<li>Notify when the window was requested to close, but wasn't closed. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fc99ab48eda3436134e1dc582db47d037573a2c8'>Commit.</a> </li>
<li>Don't let the scrollbar policy up to the ScrollView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8dca7a656839242900b08e58e03cd74c75597dea'>Commit.</a> </li>
<li>Use the distance to figure out the size of the element. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0f8105981239db9aeb3d14b6c0edc7a71519dda9'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='http://quickgit.kde.org/?p=kactivitymanagerd.git'>kactivitymanagerd</a></h3>
<ul id='ulkactivitymanagerd' style='display: block'><li>New in this release</li></ul>
<h3><a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Keditfiletype: clean out any kde-mimeapps.list which would take precedence any cancel our changes. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=84e697f877e7cab2767b75448106b7e5abf27b10'>Commit.</a> See bug <a href='https://bugs.kde.org/359850'>#359850</a></li>
<li>Ktraderclient: add --properties flag, so the default output is much shorter. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=d8de33cb9552d390494405e6922041122ae2e968'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=b82fc09b9462b6497c8f99329a5d91a51ec7fe85'>Commit.</a> </li>
<li>Save settings into ~/.config rather than deprecated ~/.local/share/apps/. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=c4ebd9b537d749e5533ecaff5631debab4827760'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354179'>#354179</a></li>
<li>Disable ptrace on kdesu. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=034b5c39e49ce946ca97f0d003a4b4f144b10d39'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126276'>#126276</a></li>
</ul>


<h3><a name='kde-gtk-config' href='http://quickgit.kde.org/?p=kde-gtk-config.git'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=230a6dd7f4e9f7a96fa0090f001f4f02c14862f3'>Commit.</a> </li>
<li>Implement changing cursor theme for GTK applications. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=9440c6ff51f57e914e46fb9d8940c987f1eb3306'>Commit.</a> </li>
</ul>


<h3><a name='kdecoration' href='http://quickgit.kde.org/?p=kdecoration.git'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Fix build with MSVC 2013. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=d511942081b2d2ce69f910c28e54440cf7ee3d12'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=5fd7a06a2494bf023babfac1852adf3e7a461ff6'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Highlighted tabs visablity, update and scale image, toggle fullview widget, icon, prevent busy indicator from infinite loop, fix empty widget on logon. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=a79c2be9b4db0755ad6ff6b26a513d71a94ef790'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127356'>#127356</a>. See bug <a href='https://bugs.kde.org/357222'>#357222</a>. See bug <a href='https://bugs.kde.org/357333'>#357333</a>. See bug <a href='https://bugs.kde.org/360381'>#360381</a>. See bug <a href='https://bugs.kde.org/360382'>#360382</a>. See bug <a href='https://bugs.kde.org/353586'>#353586</a>. See bug <a href='https://bugs.kde.org/356640'>#356640</a></li>
<li>[Weather] fix: use translated variant of "N/A" to check validness of data. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=76986f0dd22a64205c3868d9b95e3997ccea6024'>Commit.</a> </li>
<li>[Weather] Fix connection to timeoutNotification closed signal. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=70ee995d84bb21e25dc19830efa5c33a0c42b0f1'>Commit.</a> </li>
<li>[Weather] disable search button while there is no searchstring. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=cc5de3b74777cb09910c87ad12ba09f783ff38e2'>Commit.</a> </li>
<li>[Weather] ensure the weather data engine is never deleted behind our back. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d2f031589258d63f136d20acbbcff296f3e31fbc'>Commit.</a> </li>
<li>[Weather] fix display of wind direction icons. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=4c709e42a1144b9a91a9454e18ba028ffa3c8169'>Commit.</a> </li>
<li>[Weather] units on own config page; station search result in own listview. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=39f6a9204d29a6f542efc58ae8a7a35c3c2f0998'>Commit.</a> </li>
<li>[Weather] Fix occasional empty widget after configuring a new weather station. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d5b8faa0e07be47f6bc2177d6e7ae40df873c014'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127319'>#127319</a></li>
<li>[Color Picker] Allow adding colors to the history by drag and drop. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=31436d56feed6f28d48d4cc7e4d8ec9782508dc5'>Commit.</a> </li>
<li>[Weather] Disable for now defunct take at autoconfig based on geolocation. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8d11ede95977aa5e8152ec8b908deb1b7140cd1a'>Commit.</a> </li>
<li>[Weather] Remove unneeded call to Plasma::setHasConfigurationInterface(). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b16099270b113dec810888d0e75ece376601034e'>Commit.</a> </li>
<li>[Activity Pager] Remove "Show Dashboard" functionality. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b3c26eb25d5f39ab4f609c905ce2b663dfc1cb6a'>Commit.</a> See bug <a href='https://bugs.kde.org/360274'>#360274</a></li>
<li>[User Switcher] Ensure saner layouts and sizing, especially in smaller size on desktop. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=494bff14045d55fa05de301b5efaeeac8baaf25f'>Commit.</a> </li>
<li>[Color Picker] Allow dragging the color. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=46aec88f3c090ad27b4cbf2efa22d70de1007c44'>Commit.</a> </li>
<li>Quicklaunch: Fix icon size in popup. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8ab5c13b5307baed76b0fb20aaba62f7ac539ac1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360232'>#360232</a></li>
<li>Do not install so-symlink. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=06b790c37c7db8c2dad2ea367871283280ceb398'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127256'>#127256</a></li>
<li>Quicklaunch: Correctly set margins between launchers. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=79d56da1aa032c75f143352d05dd251aa05ece82'>Commit.</a> </li>
<li>Fix weather applet Messages.sh. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=dcdb47a612cef323efb771b407f1df5a4da29a8e'>Commit.</a> </li>
<li>Restore semantic icon for use in tooltips and notifications. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b8ed086005bf64f100d8ddc8ba8043b731bce249'>Commit.</a> </li>
<li>More more fine tuning of location search UI for weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f537404846d145318314b3b84b15db072a1c584c'>Commit.</a> </li>
<li>Use ===/!== in weather applet JavaScript. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ad983dbcce844fa6c8b78eb75abaca8f3202beb7'>Commit.</a> </li>
<li>More fine tuning of location search UI for weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c80d110a87b4c5e37e18eb4985812de816a073fe'>Commit.</a> </li>
<li>Port feedback about no weather stations found to QML config UI. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7a0811b2ac0b34bc2dd3849e7a9e2b3327e888f8'>Commit.</a> </li>
<li>Proper icon on change and tooltip on startup when weather applet needs config. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=abb071fdecea81b8caf66c5934684788762ec4d6'>Commit.</a> </li>
<li>Remove unused signal WeatherPopupApplet::newWeatherSource. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f8abfb1f3ef17e5b4ef076de2f51dd9f409756fe'>Commit.</a> </li>
<li>No need for WeatherPopupApplet::connectToEngine() to be virtual. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b9beb32e42d1b29a68cb7b71930fe5d087d190ab'>Commit.</a> </li>
<li>Remove unneeded layout declarations. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=90ed00bb01990c5c0b7398b5129451f347ed7e37'>Commit.</a> </li>
<li>Translate location Search button in weather applet config. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d78195186c49c52f7b5bddf7b687de8339d39ec9'>Commit.</a> </li>
<li>Have locationComboBox in weather applet config use all width. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=14691f6624b1e901025f3822c9e7beca6a271a39'>Commit.</a> </li>
<li>Properly right align all labels in weather applet config page. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6924e7763beadea7616803e05e8db2153c69e483'>Commit.</a> </li>
<li>No need to port AspectRatioMode setting here. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=5cc587ec2978e37cbddec2d91f0a627935b21630'>Commit.</a> </li>
<li>Set minimum & preferred size for fullRepresentation of weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1210221ebc1e856ec2a736c35524e921b8103858'>Commit.</a> </li>
<li>Remove unused properties implicitWidth/implicitHeight from weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=390bbdd01e55db29af6b1694dfcdf9d54cda8cb5'>Commit.</a> </li>
<li>Align right side of weather applet courtesyLabel with weather applet row ends. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f556893afb2de5cca726371e6cf39f6e140e2831'>Commit.</a> </li>
<li>Do not hardcode height of rectangles in weather applet rows. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=31fde00822986a88833e9d47d43a19842f3b2aac'>Commit.</a> </li>
<li>Remove strange spacing on right side of weather applet main content. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9057035cad00d7063de240e9a707d216f374f43a'>Commit.</a> </li>
<li>Workaround for bad vertical position of first text items in weather applet rows. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=94ab262902de0974277c5367dede58836cbf50b3'>Commit.</a> </li>
<li>Rely on padding below weather applet by embedding container. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e23b3033bcc5e1060c00edb30f252a55610b9123'>Commit.</a> </li>
<li>Use Qt.rgba instead of homebrewn code. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c195ca905063929583e4f581136c55c46a6c2015'>Commit.</a> </li>
<li>Avoid whitespaces-only toolTipSubText with weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=056f68c4c11b8374f2d8af94e8cc8691288cd7ea'>Commit.</a> </li>
<li>Use explicit fullRepresentation/compactRepresentation for weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=069b71950afac79b22fe87d48618285463477b2c'>Commit.</a> </li>
<li>Use modern signal-slot connects in weather applet's LocationListModel. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=332a4ae9fc085d15a8d57e6896f049c62fab62e8'>Commit.</a> </li>
<li>Remove left-over WeatherPopupApplet::weatherConfig(). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=58da31564e6735c6516cb357fcea6f85bac83e90'>Commit.</a> </li>
<li>Use nullptr, not 0. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6fb38f7ab681723704f5020c035682d7d889ee48'>Commit.</a> </li>
<li>Coding style: no space after !. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=784d09a674aaf46ba513c98658b5e198ad5d790b'>Commit.</a> </li>
<li>Align name of tooltip properties to plasmoid lingo for weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6aa75f4818260dcabccf13fa38e4e191a634e0c1'>Commit.</a> </li>
<li>Use initializer list instead of manually adding items. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=0e67b1c0f082cc9f9f0f7a9efa5e31782b3034cf'>Commit.</a> </li>
<li>Coding style: use wrapping braces also for single lines. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=dd55344d74f722d9d390aebca809248d3f13ab65'>Commit.</a> </li>
<li>Property variant -> property var. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9a82c76355c380e3a11ab36861ecf50100f5422b'>Commit.</a> </li>
<li>Use Qt.openUrlExternally in weather applet instead of home-brew stuff. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f177066e593b7f86b9547c796fba693b2bdc4c38'>Commit.</a> </li>
<li>Don't enable weather applet by default. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=a2d3ef8df7ba088c1f6e94379d83bf7e01062058'>Commit.</a> </li>
<li>Use PlasmaComponents.BusyIndicator for weather applet location search. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=985c00e7f0d42af0982cc57f2168391af3dd9c8f'>Commit.</a> </li>
<li>Fix svg reference in weather applet detailsview. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=94ba0799f721827da9ca7abaab3988377b332b60'>Commit.</a> </li>
<li>Use more PlasmaComponents and units in weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=94d71c7951cf167eec2fef33fb6753dc3c7cce1e'>Commit.</a> </li>
<li>Shorten JavaScript "text.length > 0" to "!!text" in Weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=079f76a2b0e008eaaa8ba1e49681c218de1f6a25'>Commit.</a> </li>
<li>Remove no longer used plasma-applet-weather.desktop. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8088532e77952b817b56e343204710f717f48947'>Commit.</a> </li>
<li>Port weather applet from Applet::showMessage to KNotification. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7c6593b8f59089914e50dfdd02c073bf62253493'>Commit.</a> </li>
<li>Port tooltip of weather applet (and use built-in compactRepresentation). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=2e9d34576c49397389dad2f8264fff1a48b2f6b2'>Commit.</a> </li>
<li>Show current weather as icon in compact for of weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=247a7b208e198744948295a3031e33fb09bcd997'>Commit.</a> </li>
<li>Another TODO. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=43557424d4498e5e0db7dcd800cc4256789fd168'>Commit.</a> </li>
<li>Remove no longer used plasmaweather.knsrc, GHNS support only coming one day. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=09101aa12747037817f35207586452972d04e7a7'>Commit.</a> </li>
<li>No need to export WeatherLocation class symbols. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=a84ae2259e85f878f55f222d675304007e4f68b9'>Commit.</a> </li>
<li>Remove class WeatherConfig, no longer used. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c160c4bde40c2360e59db30b57c75838e2339353'>Commit.</a> </li>
<li>Avoid unset values in comicdata. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f314ccaf9ab2ed039e9f19430fb24c17eed5dc36'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127173'>#127173</a></li>
<li>Remove strange "(QML)" notion from Weather applet name. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=583c3e78a41a6b47ddcdd01858303a7c910e0806'>Commit.</a> </li>
<li>Initial port of weatherconfig location combobox & search button to QML. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9a60e8b78c6867f8dbebdba9e190b6399be36bce'>Commit.</a> </li>
<li>Port location search from WeatherConfig into LocationListModel QML class. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=14316d1fd7756c1326791a4f28f8df7a6b747e0e'>Commit.</a> </li>
<li>Port WeatherConfig dataengine->query -> dataengine->containerForSource. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=bbafc316863b904a5895d1e5e4ffbc62b700d85e'>Commit.</a> </li>
<li>QObject instead of QWidget as parent for WeatherValidator is enough. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8c82b5cf870d29052cd6af33b72e67a0cb5f5973'>Commit.</a> </li>
<li>Fixed last Diff revision 4 issue. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=685748ebabefa7bd17eb84ec443dfb6b305144e7'>Commit.</a> </li>
<li>Fixed most review comments for Diff Revision 4. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9d17ba02f9801ca6bd2988b75146ba6545ea91f3'>Commit.</a> </li>
<li>Removed unused include of QGSHoverEvent. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=af37079935067d0ae35cc8150fdf0ffb66c3fbc0'>Commit.</a> </li>
<li>No need to have Consumer and Controller as separate instances. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1b7e4980ed83d2186013fc55e918832dae9e3584'>Commit.</a> </li>
<li>Fixed minimum width for the notes applet so that buttons never leak. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d81226c7728d1ee0c2894d86b09ff11767363bac'>Commit.</a> </li>
<li>Disk Quota Applet: display "No quota restrictions found." instead of an empty label. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=71eb4801d1f2771915ae49b0423eef1f6aecf6e1'>Commit.</a> See bug <a href='https://bugs.kde.org/357699'>#357699</a></li>
<li>Remove dead method declarations in WeatherApplet API. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=528c8815915f1badcdd89e3d359642522b589d21'>Commit.</a> </li>
<li>First steps for QML-based UI for config of weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=91a37812fb09d8bd079c7109dcac9acf7eb8c4d7'>Commit.</a> </li>
<li>Refactor timer plasmoid. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=10c489b6b90bed1253451ae4fb4df241c3657336'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353090'>#353090</a></li>
<li>Restore state of ComboBox. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=26ef068f94b7a94887f811f418ac1955e7067840'>Commit.</a> </li>
<li>Stop plasmashell from CPU hawking after a while by removing the "countdown" visualization in mediaframe. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=68c429e17e303686d426ef672d83b2576ef0484c'>Commit.</a> </li>
<li>Initial "builds & starts!" port of the weather applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ca1f10747a6c3ac1d9d7136f7927d08cc44c26b2'>Commit.</a> </li>
<li>Readd libs/plasmaweather to build. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=a80a07b64995252f660a86ee991bd1f5b4027c03'>Commit.</a> </li>
<li>No such libs/rtm anymore. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=53624758f4e44e10caf45981f839f2d3dc506d44'>Commit.</a> </li>
<li>Have some version set on the libplasmaweather file. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e7dc1553c5fcf6753c8115184d1e0d4ed20bd85c'>Commit.</a> </li>
<li>Generate plasmaweather_export.h. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=14d1594afd4ca6437a7f79a522ee0a21c9743e94'>Commit.</a> </li>
<li>Initial "builds!" port of libplasmaweather. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7bf70921353ad8ee3801135071acb05f1cbb652b'>Commit.</a> </li>
<li>Remove no longer needed Weatheri18nCatalog::loadCatalog(). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6a0d78dcfbe1064e8f97c1a5d8a9c345ab78c294'>Commit.</a> </li>
<li>Moved history and future functionality to native code. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=cd9c3d5cbf00418e51853cf322b8718ef014b9c9'>Commit.</a> </li>
<li>Small fixes. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=50560336bae3f3c4aaf57232384bd3220b5a1cf3'>Commit.</a> </li>
<li>Issue fixes from review 126793. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=1b9b16fba5d06bf5707c174a41372372794c87d8'>Commit.</a> </li>
<li>Most QML issue fixes from review 126793 plus some C++ issue fixes. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b902b3a16f5ac0075ac04a31e769acb5136dc126'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=4da3154e4c1309e5b47c0dfb0b2315fa2796e167'>Commit.</a> </li>
<li>Quicklaunch: Use icon item directly as delegate image for drag. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=cc0eb521b75ede92a04a0c8ccd9d3601e44699e4'>Commit.</a> </li>
<li>Quicklaunch: Ignore drags without urls. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e227672fd53cf8d29efbab11252e96f297adedc5'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f8af0b524c48a6bfc90752a43d6a744429c453f1'>Commit.</a> </li>
<li>Quicklaunch: Add option to show title. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9b0e06e0d97d03cf8670dcddb27bb8aba1c7ca66'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126786'>#126786</a></li>
<li>First humble atempt at contributing a KF5 plasmoid version of the old "frame" plasma widget. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8ee7291d9e07f18ce26eda97d84f75fe98a8e2e6'>Commit.</a> </li>
<li>Remove kimpanel. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=c24ceb9eaf11795b750118c68a479da3303de2bd'>Commit.</a> </li>
<li>[WebBrowser plasmoid] Remember last visited url and restore it on startup. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=819dbb8b960a545d12f9b72ddf68d8b51924f8e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357387'>#357387</a>. Code review <a href='https://git.reviewboard.kde.org/r/126590'>#126590</a></li>
<li>[Activity Pager] Implement drag and drop for Task Manager entries. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=08cc10167b0bb0162006bf587b38d3097ee493b0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126374'>#126374</a></li>
<li>Dropping a text file onto the desktop will offer to create a sticky notes from that file's contents. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=5e565c6a2a664b064633481571070f650532cedd'>Commit.</a> </li>
<li>Fixing color picker ordering for Qt and LaTeX (was RBG instead of RGB). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=d0d4e1d80f5289f33fa95a6452cceb3152fa4bf1'>Commit.</a> </li>
<li>Use QtQuick DropArea instead of KDeclarative's. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=21893fb93a79b93842d46a54dbfa75bcf74ac339'>Commit.</a> </li>
<li>Quickshare: Trigger paste from clipboard action on click. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ca9b6310ceba285a67098b705b7015ae6202f21c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126165'>#126165</a></li>
</ul>


<h3><a name='kgamma5' href='http://quickgit.kde.org/?p=kgamma5.git'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Do not use Xf86vm but ${X11_Xxf86vm_LIB} when linking kcmgamma. <a href='http://quickgit.kde.org/?p=kgamma5.git&amp;a=commit&amp;h=1ddb19068cd97da8ed36886f5610c79e8cffc657'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126603'>#126603</a></li>
<li>Remove unused dependency. <a href='http://quickgit.kde.org/?p=kgamma5.git&amp;a=commit&amp;h=b9173ba3c4d610b42bc174a1621cb9eb3f14dd78'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126848'>#126848</a></li>
</ul>


<h3><a name='khelpcenter' href='http://quickgit.kde.org/?p=khelpcenter.git'>KHelpCenter</a> </h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Remove references to removed documentation. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=1cb32af51f220f2f37102131b3f4e8c98060198c'>Commit.</a> </li>
<li>I18n: stop extracting messages for htmlsearch. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=1f231c623f98a7d36b290327f8bfeef8ff39c0bc'>Commit.</a> </li>
<li>Refresh HTML templates for start and glossary entries pages. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=2721febfb6f76d06d5df733fc7beb1e01afa6a37'>Commit.</a> </li>
<li>Fix HTML of the start page. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=40719aa58bbbd8adbc19a53a92daa12585125726'>Commit.</a> </li>
<li>Remove unused version.h. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=f4481992ce2ef9c2110a6e3ec13c6df1891c5589'>Commit.</a> </li>
<li>Remove declaration of unimplemented function. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=b4e79bac831afe9fd026710ca0d7df67db51e2a8'>Commit.</a> </li>
<li>Infotree: improve character handling when building. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=d86999badf8905e97447bb2e425bedf29524185b'>Commit.</a> </li>
<li>Glossary/infotree: improve building of "by-alpha" subtrees. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=66c24b0c09ce88acbf7e73a4747b38a151f8fd86'>Commit.</a> </li>
<li>Cmake: remove unused vars. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=56ef650c89bbb45f3f55541feccd961f7b5e1b42'>Commit.</a> </li>
<li>Khc_mansearch.pl: use $section in HTML results. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=35161b6d59099b253aca6d61a2ffefd994506336'>Commit.</a> </li>
<li>Khc_mansearch.pl: force --maxcount to be integer. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=954bf7cef565ddb229b1b17b4f7d69e9cbd6b1ad'>Commit.</a> </li>
<li>Delete the search handlers on shutdown. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=9ff79cb86dbaabe354534cd82ea234980e4e1776'>Commit.</a> </li>
<li>Cmake: fix absolute path to the libexec dir. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=0a9edbd3b77ee981619be765a9e23434fae2b5ca'>Commit.</a> </li>
<li>Khc_indexbuilder: simplify operations on list. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=7bf67a897968440b17a3e5aa99cabd7134cfb7c3'>Commit.</a> </li>
<li>Fontdialog: switch away from KVBox. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=5fa6857ef9c58e73989a93b53fc3a24897d45212'>Commit.</a> </li>
<li>Cmake: explicitly use some frameworks. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=206823da6141f1e0786e2eb0e5cc183902781140'>Commit.</a> </li>
<li>Remove few unused includes. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=58e5901336253613221f82ac1041a5584a65a753'>Commit.</a> </li>
<li>Kapp -> qapp. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=3a40c6547e81d70d702af901832c08d132ed84a6'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=b384a5743e3dd4b8a46f13297814b306da91576a'>Commit.</a> </li>
</ul>


<h3><a name='khotkeys' href='http://quickgit.kde.org/?p=khotkeys.git'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=e93bda6a8007e7c1917b28c7898d28a5bf42a5b0'>Commit.</a> </li>
<li>Port mouse gestures to QAbstractNativeEventFilter and reenable them. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=57a0c4ecf98f973f2b7a7eeeb4e55213cbd22ec4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343383'>#343383</a></li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Fix more icons. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=626aff656a8467e3e5e20f7c545a79d3e9ab9d0a'>Commit.</a> </li>
<li>Fix icons of categories. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=143545d73f42a2bf3462e6a7c0e9c8f0008e1c43'>Commit.</a> </li>
<li>Remove K_EXPORT_PLUGIN macro. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=d22d97b9807f83a460ed44e8d7d8de0c8f6277f9'>Commit.</a> </li>
<li>Fix incorrect usage of KDE brand. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=b0def1d85a0d9f50a295a4c217c4aac6dbfd427d'>Commit.</a> </li>
<li>Fix incorrect usage of KDE brand. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=b4cbff4f4c257d42e701f966816bd031a43d47c0'>Commit.</a> </li>
<li>Use non-deprecated API for the window icon. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=ae7ba653ca8ad2e66052986051eaebd96c3198c3'>Commit.</a> </li>
<li>Two small layout improvements in energy module. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=7104e4ef5649ed4ca83ab12e5187b2297ca3df90'>Commit.</a> </li>
<li>Rename to Info Center. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=b6e33a92f377591dce1fc3ba9685decb1d4fdfc8'>Commit.</a> </li>
<li>Include header before uses of it. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=ecd86f10fa3b6d6d5d013c329f1ba6b29c7a5d06'>Commit.</a> </li>
<li>About-distro: Fix build with kcoreaddons < 5.20. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=33c455641a3aed14b31023978e898bfdfab85019'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127257'>#127257</a></li>
<li>Show Frameworks version next to Qt Version. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=839900875b266ec85c10167b48fe3988fde8b8da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127035'>#127035</a></li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=eca6187e068f1f6935b38b32bc9c80d5818c1315'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=fa1fff6e5555b6366692000ee2c2def7026154e3'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='http://quickgit.kde.org/?p=kscreenlocker.git'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>[kcm] Remove QQuickView for themes. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=47d3969ef306d30ecb4e377c283d9f7df7e2d8a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352650'>#352650</a></li>
<li>[autotest] Fix LogindTest with Qt 5.6. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=18c35d3e146b2324d65340e082c329977abe3acb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360025'>#360025</a></li>
<li>Only require Qt 5.4. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a4548741979d853f4bb380561c8f802789bb9b19'>Commit.</a> </li>
<li>Select theme from command line in testing mode. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a99b0eb703cc375849fe3e2fed9135121419ae82'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127168'>#127168</a></li>
<li>Remove KCrash from KSldApp. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a67560573f4f77fcf86a920fca858a0e831280ac'>Commit.</a> </li>
<li>Pass a parent QObject to the AbstractLocker and it's subclasses. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=344aa8a194f933902cb7e9dd97e2546556ae10ec'>Commit.</a> </li>
<li>Fix ordering warning. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=aa84fb8828f316197b7c9f7ba7910bcf336b7cb0'>Commit.</a> </li>
<li>GlobalAccel can operate on QKeyEvent. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=2c7782d8cbc39f50a101027699a25471dca09f3b'>Commit.</a> </li>
<li>Don't handle lockScreenShown if already in Locked state. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=0e5cb71a22dc0fe7ad7c1fb3d4cd6919116dd826'>Commit.</a> </li>
<li>Only use xcb_key_symbols_alloc on platform x11. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=7103bc2495298d386d639aa6f14c4ca1b17a3c2e'>Commit.</a> </li>
<li>Cancel locking when logind sends an unlock request. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=32821eba6d2fb5cdac441981fbbffa9f71050e10'>Commit.</a> </li>
<li>Introduce a lockStateChanged signal. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a1c17b3696d00f196128b25182fa039398a479f8'>Commit.</a> </li>
<li>Forward error channel from greeter process to KSld. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a68dda3dd2a8a60e7f03beb5f0e652ef10fedbaa'>Commit.</a> </li>
<li>Init m_waylandDisplay with null. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=9eb1a64b085c52f9e1746b3b4b27e91c3b23479c'>Commit.</a> </li>
<li>Async variant to check whether power management is inhibited. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=4e9dce5f0e073c39982ee90b1d32f7a6012f350b'>Commit.</a> </li>
<li>Port to CMake automoc. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=dc984381ee13e9b923869bcb0256b416a8bd14ef'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=d4d90172777a2cfd26dbbc7a4ac7d4db76ec4d12'>Commit.</a> </li>
<li>Add dbus protocol for org.kde.Solid.PowerManagement.PolicyAgent.xml. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=0c60df9378445cce9709444da49af448ad1fa29e'>Commit.</a> </li>
<li>Port away from kdelibs4support. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=080f65762ea7cf2784d7edfdf64159f13494bc38'>Commit.</a> </li>
<li>Use *_SINCE_VERSION instead of hard coded numeric values. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=89214807b0ec02e2ef2ee9e4a8c59a0fb82ad10c'>Commit.</a> </li>
<li>Fix for-each loops. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a3853d41b788c62cb3cecbaf46f211500077d91c'>Commit.</a> </li>
<li>[greeter] Drop unused variable canlogout. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=29f31515a1f2f139e2741db813d7c3a86baeb3b2'>Commit.</a> </li>
<li>Suspend/Hibernate through daemon instead of greeter. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a04768901ac13985dd673bbb45f2bf98d9a52903'>Commit.</a> </li>
<li>Bump interface version. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=a2ff353dcd3aac5957e70e15fc449f506b806a99'>Commit.</a> </li>
<li>Enforce wayland QPA for greeter if running on Wayland. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=939fe88413fe56a0a2a252545caec915bbd5e345'>Commit.</a> </li>
<li>Fix installation of KPackage. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=49a530fe0d743829f097641b737d17ea872d8064'>Commit.</a> </li>
<li>Drop not needed KF5::Plasma dependency. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=c07ef9389342db12d6475c51513e27a9afaa030b'>Commit.</a> </li>
<li>Improve setting global lock shortcut. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=56133098976e68591fb31b5cb50b537ee75cd074'>Commit.</a> </li>
<li>Add arcconfig. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=327d0ff8c9e0e7236e9dd63be3131c6bb870a47e'>Commit.</a> </li>
<li>Support userActivity in WaylandLocker. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=b396581b75bb9533d908f85ca4d3238d3a3a9cf9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126367'>#126367</a></li>
<li>Use SERVICE_TYPES parameter to kcoreaddons_desktop_to_json(). <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=687272c11e4d51ea014779eb6b7e767ef11f0996'>Commit.</a> </li>
<li>Allow setting the process environment for the greeter process. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=2e6ce72d88be62957afd28e8f6dafc41052635a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126243'>#126243</a></li>
<li>Disable ptrace for kcheckpass and the greeter. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=88a497e4c6b7599cf859703e25d65ba8bb2873ce'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126203'>#126203</a></li>
</ul>


<h3><a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=3b96b9634cc5890b21a1cbdc103b4ad60b01a732'>Commit.</a> </li>
<li>Don't truncate return value from fgetc. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=0b9c121f96cbb4e5f002a4a534c4bb9fe11dda66'>Commit.</a> </li>
<li>Port from KColorDialog to QColorDialog. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=977f8b6e8ad304d7a4e2c0b7c03a481df50c772e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126265'>#126265</a></li>
<li>Port from KIntNumInput to QSpinBox. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=55b045842eacd76788f5d8043a5e9ebec7cc874f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126264'>#126264</a></li>
<li>Port from KTabWidget to QTabWidget. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=d34dfab7c02c97ccb86e5eee93329c4bd8f6774e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126260'>#126260</a></li>
<li>Use QString() instead of QString::null. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=5a3da6f1fbcae547ada482e1ae1cf4b8d917a528'>Commit.</a> </li>
<li>Port from KInputDialog to QInputDialog. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=352b587f9b0b481791809cf754372675a7751a3f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126236'>#126236</a></li>
</ul>


<h3><a name='kwallet-pam' href='http://quickgit.kde.org/?p=kwallet-pam.git'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Socat is runtime dependency, no need to abort build if not found. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=008da60dea22aa283e3058b80312dc3c34ecaaf7'>Commit.</a> </li>
<li>Make kwallet-pam usable outside of Plasma. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=c974341fe3502c46d275ff56ae36477ed7cc573d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126843'>#126843</a></li>
<li>Check sockaddr_un buffer size before strcpy()ing into it. <a href='http://quickgit.kde.org/?p=kwallet-pam.git&amp;a=commit&amp;h=9543cc4058b24e4e5bfe8d324de309ca7050058b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126539'>#126539</a></li>
</ul>


<h3><a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a> </h3>
<ul id='ulkwayland' style='display: block'>
<li>[client] Perform ConnectionThread::roundtrip through QPA interface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=36c5291b6ac59129b53c4f4dbb6c69b8c9a50c2e'>Commit.</a> </li>
<li>[server] Add more Q_DECLARE_METATYPE. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=1766126abf6fe88baf02709a4b62007eeed7e622'>Commit.</a> </li>
<li>Add a default mode event to ServerSideDecorationManager. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ba479e3ee250eda29aad39bad38c22edba552f53'>Commit.</a> </li>
<li>[server] Default initialize the role of a PlasmaShellSurface. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8df30e1bcecc701c71caeff82b7e9095fc2447dc'>Commit.</a> </li>
<li>[autotests] Add test case for PlasmaShellSurface setting role. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=5282bd8339349c97a8650483a7de2e4d0879b680'>Commit.</a> </li>
<li>[server] Declare metatype for KWayland::Server::PlasmaShellSurfaceInterface::Role. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=843205984a4986134065aad4dadbaa63d0b822cd'>Commit.</a> </li>
<li>[client] Declare meta type for KWayland::Client::PlasmaShellSurface::Role. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a29967347309a4b3ef9e84d0e8a38bb8552f57b8'>Commit.</a> </li>
<li>[server] Implement support for drag'n'drop through pointer device. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=413025c293b10c54ce35e43e6bb275c8e423b835'>Commit.</a> </li>
<li>[autotest] Fix crash in TestWindowManagement::cleanup. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=66c139297f8893c5d0d32f8851f73029f6dd3357'>Commit.</a> </li>
<li>[server] Properly initialize Cursor. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=ba276f41d17c4c8ebb053930bc5b76b96adc1328'>Commit.</a> </li>
<li>[server] Add a signal SeatInterface::focusedPointerChanged. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=84d3fb4b4aa95cbdfaa6ce2fd2e734be08fc1688'>Commit.</a> </li>
<li>[server] Only send modifiers to client if they actually changed. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=616f33efb2df4eb69f00d342ee8bf68d62a61a50'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=355f68efd0f9bf86d78fa3cc63bed961404559da'>Commit.</a> </li>
<li>[server] Expose executable path in ClientConnection. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=aefcc51d0727483dfd9aa2e6bb877e5643be519a'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=a087da4a9ce0c3ce7bc2d774e280ddc725432f7b'>Commit.</a> </li>
<li>[autotest] Add tests for ServerSideDecoration protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=66e2bb65942a6d4af74bb95a293c22b504ae8352'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126301'>#126301</a></li>
<li>[server] Add implementation for server side decoration protocol. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=fb6c831ab38dc13cbefa11c46b544143fa2d3966'>Commit.</a> </li>
<li>[client] Add implementation for ServerSideDecoration. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=cf3c0b336df33db2774eddcf585f1dc9223297fe'>Commit.</a> </li>
<li>Add protocol for server side decoration. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=c787ea9bc22a8328c4e2fc5347cb8bf9aaeda34d'>Commit.</a> </li>
<li>Fix OutputDevice::edid(). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=9c548043d291c379d7d43fa1db04a3d3e3b96e8f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126380'>#126380</a></li>
<li>[server] Add Display::seats() -> QVector<SeatInterface*>. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=6d141c52a9b04b032df0b3d5ea9376ab7e01d000'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126364'>#126364</a></li>
<li>[server] Add support for pointer input transformation. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=2fabeeac6a7a7d78a084958787d71270eb48e03b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126271'>#126271</a></li>
<li>[server] Minimum supported idle timeout is 5 sec. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=7a34e3bece2796bd7873c7ec954f48f1e09a0d46'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126220'>#126220</a></li>
<li>Improve language in idle protocol docs. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8896ad798dc9ba92f2fcff49448a38a7c59a0054'>Commit.</a> </li>
</ul>


<h3><a name='kwayland-integration' href='http://quickgit.kde.org/?p=kwayland-integration.git'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>[windowsystem] Make plugin also available for KWin's own QPA plugin. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=35cd801fba524925d8fb843c6956da1f18dac88e'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=1e10e1c64eb4d4c9cfb00b7508b7ff14f0315942'>Commit.</a> </li>
<li>Properly announce whether slide effect is available. <a href='http://quickgit.kde.org/?p=kwayland-integration.git&amp;a=commit&amp;h=1069308023b32905e8cb4f68559104080faa69e9'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[plugins/qpa] Add a roundtrip platform function. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe745177e0f524abb72e1025fedb3d1031aa9c9e'>Commit.</a> </li>
<li>Fix crash when accessing ShellClient::iconGeometry for a not mapped window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bf932c4e9719ec9498f56a70099cbd62c0b955e8'>Commit.</a> </li>
<li>Fix start move through drag distance on window decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b9e6a4aa269a4b2ebf3980b70462b15037e9b1e'>Commit.</a> </li>
<li>Invalidate double click timer when start move resize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7777f0c507fe8b1f0d1d7dc04c594c41be137d5f'>Commit.</a> </li>
<li>[plugins/qpa] Fix build with Qt 5.4. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=68683873e38f5471b47b49deb587d5223309eef6'>Commit.</a> </li>
<li>[plugins/qpa] Add a dummy screen on startup. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=51ee514227d3eef16bbb38f2c6ca494ab4bb3e09'>Commit.</a> </li>
<li>Don't pass keyboard events to internal windows outside the screen geometry. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=abca474d443dd55ec8495d4075f179752bc8ccd1'>Commit.</a> </li>
<li>[plugins/qpa] Adjust to changes in Qt 5.7 QPA interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7d08b0847031fecb35047b98da61ffb852c2b1ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360269'>#360269</a></li>
<li>Fix repaints area on Wayland damage. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=34951b3bee88fd099768c8fe390a5e74371f8135'>Commit.</a> </li>
<li>Only pass key press events to TabBox. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2c0df531b7736b5c0f9540b99b27d4f01191e9fd'>Commit.</a> </li>
<li>Pass pointer and wheel events to TabBox from special event filter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d2716c834b6eb1b39bfe56b67d55482473bd10ef'>Commit.</a> </li>
<li>Add bool checkInputWindowEvent(QWheelEvent *e) to EffectsHandlerImpl. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6c0ed26c657cfb6101c60e4dfd764239ddea6f5f'>Commit.</a> </li>
<li>[effects] Don't assume windowInputMouseEvents only carries QMouseEvents. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7718b6dce3bebfd9fdff2da07ffc006a584c6357'>Commit.</a> </li>
<li>[tabbox] Pass the tabbox window to elevate as a QWindow instead of winId. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=aea4221575205c7262cc8872fcd6839ef3fed28a'>Commit.</a> </li>
<li>Add a Toplevel *Workspace::findInternal(QWindow *w) const. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=59e3b96812dc7b9cce969b5779caea36ee4cfa86'>Commit.</a> </li>
<li>Fix Workspace::hasClient(const AbstractClient *c). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9c78d2832784f8ed8716b97cf4f89c979cefe9ae'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c8b3f71cc6d31fd43f6f7638f3cf9778e61967f8'>Commit.</a> </li>
<li>[autotest] Fix build on build.kde.org (Try 3). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2787fdb8737e6b996fa14c57cee75e0fa3ffb1dc'>Commit.</a> </li>
<li>Fix build of stable branch on build.kde.org (Try 2). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=db76d4279a7016194d0b8e0396c5f5e052abc638'>Commit.</a> </li>
<li>Implement sanity checks when placing transients. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=04fdecdd59bf0d7a950d040c2530522bec59441d'>Commit.</a> </li>
<li>Fix transient placement for decorated parents. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fa774230f3f786e79e4b11ddf90778ccd890c07a'>Commit.</a> </li>
<li>[autotest] Add test for transient placement positioning. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f2873dcd36b863d4654aac0605aeab28417a3e28'>Commit.</a> </li>
<li>Fix build of stable branch on build.kde.org. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc64bb25aed5a5bf88b3e0f15c3850c4b6e2c481'>Commit.</a> </li>
<li>Add support for Drag'n'Drop on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8a1f19b1450fc244977ad6e7dff48d4fb6c28a13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/36065'>#36065</a></li>
<li>Respect motif and rules on unmaximizing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9d09cf6dc9cb36ebf94663106a87141642599430'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359797'>#359797</a>. Code review <a href='https://git.reviewboard.kde.org/r/127198'>#127198</a></li>
<li>Restrict cross-vd activation to in-client distrib. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0e80a16db57cb194431725aa204e1fa871ea87c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359683'>#359683</a>. Code review <a href='https://git.reviewboard.kde.org/r/127153'>#127153</a></li>
<li>Try more aggressively to retarget. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a104a4af7f7cd8d522bd8c6a6ee213ba6eff9a2a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127101'>#127101</a></li>
<li>Fix fallback resolution in WaylandCursorTheme. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6ae4775651dbf8b1a608edd8e3a227a98b20ae74'>Commit.</a> </li>
<li>[autotests] Specify XCUROSR_THEME and XCURSOR_SIZE in pointer input test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=166d282a9f468d317cd0c66f72bcc060a9d13afd'>Commit.</a> </li>
<li>Respect WindowForceBlurRole or forcecontrast. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f7feca678d082899a623b04d75f9525e57dc09e8'>Commit.</a> </li>
<li>Send leave/enter pointer event when starting/stoping effect mouse interception. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ed7bf6e0910d05e0141ab282209ed66b91b951b1'>Commit.</a> </li>
<li>[autotests] New test case to verify cursor changes correctly in effects. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d495f1dc95c279059530d1535b2718acdee8a93'>Commit.</a> </li>
<li>Reset stored window before setting it as focused pointer surface on seat. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b6677ca04e1c2685261661f614832996f25cff6a'>Commit.</a> </li>
<li>[autotest] Add test case for cursor image. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d15e94f326027ec0cbc1e15bef3e1e5de4b25020'>Commit.</a> </li>
<li>Add support for Move/Resize cursor mode. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=873e2b032054b36c3912acd98d4c4dceff46602b'>Commit.</a> </li>
<li>Clear cursor cache on theme changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2292e87b35c62e1a58d4c7727697a62e1884c454'>Commit.</a> </li>
<li>Rework cursor image handling for Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a029300ce597a72933298d528a83488934756cd6'>Commit.</a> </li>
<li>Morphingpopups: Don't skip small steps. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a982367a7509a1d86af36bc59660deeef0f411f8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127137'>#127137</a></li>
<li>Try alternative cursor names in WaylandCursorTheme::get. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5b945d2b1c548e81c15cd765406ea979e54a1a34'>Commit.</a> </li>
<li>Add Cursor::cursorAlternativeNames. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=72a25aa9ffaf651f17084fa73368e38e08c40ff8'>Commit.</a> </li>
<li>Set proper size when loading with wl_cursor_theme_load. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ecfe247598af007ae71eae150419ae6331c974ef'>Commit.</a> </li>
<li>Fix mouse action on (in)active window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9eeef2d9cac9070a0924a001e11e7a544e4d1ea0'>Commit.</a> </li>
<li>Connect to dbus signal reloadConfig from org.kde.keyboar /Layouts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d7c8ac372267bfaef132b11c2895a7aad584c41'>Commit.</a> </li>
<li>Notify org.kde.osdService about keyboard layout changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bc7f33db57c4eeb205e2eda5382c551aefd789a8'>Commit.</a> </li>
<li>Support switching keyboard layout shortcut. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c79511b2d509c28e8159fddb4c5089286d27a23b'>Commit.</a> </li>
<li>Update keyboard modifier state on seat after each key event. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=83a4fe54084278be80b485134d1d3c1ddf166bae'>Commit.</a> </li>
<li>Load xkb keymap information from kxkbrc config file. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2e36c4b7ca98ba5242dc6f707191ca0d2e8f7af3'>Commit.</a> </li>
<li>Ask Xkb before starting to repeat a key. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5caf33161069d528e8c26bc58e458d7e0d51720a'>Commit.</a> </li>
<li>Install custom debug handler on xkbcommon context. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=44205fbdb43f2180a8bba8d2a863c56bb75758ee'>Commit.</a> </li>
<li>Introduce dedicated debug category for everything xkbcommon related. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2205c98ec24d52861543cd7e86ca37f76f4e19f8'>Commit.</a> </li>
<li>Implement internal keyboard repeat. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cb3c6a47807b1c94e6bf3ba5b45b453b172dddad'>Commit.</a> </li>
<li>Invoke AbstractClient::enterEvent and ::leaveEvent on updating pointer window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b59593bd9d478ea73d261560117a037fc90e7000'>Commit.</a> </li>
<li>Move leaveNotify event handling to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c6e6d9a8723ab3f5da3c63d88ee3f2d3770b7967'>Commit.</a> </li>
<li>Move enterNotify handling to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ee550292f5cdfe56930bba8dce726dbeb325c5e'>Commit.</a> </li>
<li>Port Workspace::requestDelayFocus to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6b06779a64e0b03d18862c685227ef08ac2371b7'>Commit.</a> </li>
<li>Merge Options::MouseLower for Client and AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f0ec89f38b1dd86d8b79f1057706b222bce80bba'>Commit.</a> </li>
<li>Port Workspace::clientUnderMouse to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df4bb8296a675ba1645b6269357dd67e2df86556'>Commit.</a> </li>
<li>Fix scroll direction on window wheel command. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d47839e95a80dd85b461adf9730e8eb79709deb'>Commit.</a> </li>
<li>Move window action handling logic into a dedicated InputEventFilter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e6e11f7853a17a46280f3cab68eaf0e6e78d3b52'>Commit.</a> </li>
<li>[autotests] Add test case for touch down triggers a mouse action. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f1e3c9f1913d6903287e63545bf7916b305615fd'>Commit.</a> </li>
<li>Implement whell command in input handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2a98c681d07ee96858f9784dd9f0a04f6ca44c4c'>Commit.</a> </li>
<li>Add support for modifier+wheel action. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1f1a4ac6e8eeb1338276f0c5574dca9fc2ffab46'>Commit.</a> </li>
<li>Add support for opacity in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ba7c5bbd77dd2c7850fb8621014f1130a842584d'>Commit.</a> </li>
<li>Implement modifier+click in InputRedirection for Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f8f13a7fbaf6025656641f89e5908f0df1487898'>Commit.</a> </li>
<li>[autotest] Adjust TestScriptedEffectLoader::testLoadAllEffects for morphingpopups. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fd692ecf076de3763f035e833d96798d7d8d9ab0'>Commit.</a> </li>
<li>Morphingpopups effect, to animate tooltips. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=82a1b3ee137e4a4dfad528367dd454ee98e505dd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126968'>#126968</a></li>
<li>Move decoration double click logic to button press. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=093e56ff05c212e92fd8de2b41b839f3762e6d50'>Commit.</a> </li>
<li>[autotest] Don't crash when cancel animation from animationEnded in scripted effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc8b3a5a96efa6f6156d216d2bea56275cd4ecad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126975'>#126975</a></li>
<li>Improve keyboard handling for internal windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=521470b04a7502d804805dee99a9159a3d34b73c'>Commit.</a> </li>
<li>Reintroduce the useage of FBConfigInfo* hash in glxbackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=36facda1107688a265b6f8edb9851c80a6b091cc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127096'>#127096</a></li>
<li>[plugins/qpa] Implement a custom QPlatformCursor. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7d81e9cf2296b62218920f5ea006a85c815e9424'>Commit.</a> </li>
<li>Map all pointer buttons to Qt::MouseButton. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=44bb78c193dd50d3e7838353aac4bd6a06a063ed'>Commit.</a> </li>
<li>[autotests] Test move ends when all pointer buttons are released. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=61428b164ede92d1ea19e725472ce4280d31e632'>Commit.</a> </li>
<li>[autotest] Adjust LockScreenTest to fix that KSldApp goes to Locked state. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5200a484ce7e0baf72d7cc8c1eef5911e7935ba1'>Commit.</a> </li>
<li>Move global event filter from Application to ApplicationX11. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9e54cb5a1f7353b89194587323c9b387b463bc70'>Commit.</a> </li>
<li>Set timestamp on WaylandSeat for key event in LockScreenFilter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=46e3da297ceb5bcaaed9aa8042092a6168f26e02'>Commit.</a> </li>
<li>Fix validation cnp bugs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5ee90dce3de6b9f3915d4b63df2a4578ef391cde'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126981'>#126981</a></li>
<li>Fix retargetting script export. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c9c86f29fbf31e2b70bb465f518d62ed3e684ddf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126981'>#126981</a></li>
<li>Catch nullptr effects when deleting shadows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cae90bb035e6170a9beb38545cf60e31af612804'>Commit.</a> See bug <a href='https://bugs.kde.org/356938'>#356938</a>. Code review <a href='https://git.reviewboard.kde.org/r/126441'>#126441</a></li>
<li>Export retarget to scripts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=070b85bbcf813097222de099ca4689ec4a305187'>Commit.</a> </li>
<li>Allow to retarget animations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1faa8aa039d6f5681950f5dbab9d4a4f9b8b387c'>Commit.</a> </li>
<li>Protect against cancel of just ended animations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=07cc30d1367f2fd5af28255ca9aaf49da27e7002'>Commit.</a> </li>
<li>LockScreenEventFilter passes key events to KSldApp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4f66f324d7a885e678919561d7593b103a46c318'>Commit.</a> </li>
<li>Always notify lock screen when a lock surface got created. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2b34f97344035e2e981f400e012500ab2c79ae7d'>Commit.</a> </li>
<li>[backends/hwcomposer] Use input event filter for turning screen on/off. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1c4c5caf65b0531e2d89f8281524d3d86ea04989'>Commit.</a> </li>
<li>AnimationEffect: Fix memory leak. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a33c2730babf65b95ef6a063bb4be366be63866c'>Commit.</a> </li>
<li>Scene: Fix memory leak. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=43bd3e44e1c1f3b16ce75900a5a1e689e5d6a6dc'>Commit.</a> </li>
<li>[backends/drm] Double Tap to enable output. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a66eb1a5b91d9171aef1937c69343b94a5d84950'>Commit.</a> </li>
<li>[backends/drm] Fix typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5b1eb584d6199602004a8a344d883138b1f9be78'>Commit.</a> </li>
<li>[backends/drm] Use an InputEventFilter to reenable outputs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=57b11f84296b69944b907a89d403dc50d36a75dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341201'>#341201</a></li>
<li>Cleanup includes of input.(h|cpp) a little bit. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4e32dcfbfea911532a7393cbe2c891d8ca54d251'>Commit.</a> </li>
<li>Split keyboard related functionality from InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=849d17519c1200b1aa378f2195078579e514b2db'>Commit.</a> </li>
<li>Reset TouchInputRedirection::m_inited when Workspace or WaylandServer get destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=71a8879e95378124f137de75800deee9e8ba2168'>Commit.</a> </li>
<li>Add missing override to LockScreenFilter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=59dc3816b47f647e55cda69329e4f103847839d2'>Commit.</a> </li>
<li>Fix grammar in KCM MESA warning. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=41e16f56ec6a710b7b608b7502f655eee81a7621'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127066'>#127066</a></li>
<li>Split out touch related functionality from InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b8f8b2d8a0d0f4b2b61f16d47ed098ce34a0bd38'>Commit.</a> </li>
<li>Revert "[autotest] Let's dare enabling the OpenGL compositor in the tests". <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fed7a48112398a9998706cd35bf81d9eeac6f1f7'>Commit.</a> </li>
<li>[autotest] Let's dare enabling the OpenGL compositor in the tests. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5e21963a3b620a167d9319e992c79a56f19d5bfd'>Commit.</a> </li>
<li>Check for Workspace when a ServerSideDecorationInterface gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8f28e900ec85cafebfecfd2687fa301b5a995638'>Commit.</a> </li>
<li>Split out pointer related handling from InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c044ad98bed1ee9bab8aa388cd2de5652103a9d8'>Commit.</a> </li>
<li>Drop InputRedirection::pointerButtonState(uint32_t button) const. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=768f1be9391fb6d816620ed7891721e4c83bbfe8'>Commit.</a> </li>
<li>Drop bool InputRedirection::areButtonsPressed() const. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b8fcfbb8fca173a28a7329f87b57ca486ee5ab32'>Commit.</a> </li>
<li>ScreenEdgeInputFilter for checking whether a screenedge gets activated. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9b917a20feb2ed328f807330f9dae55bdf00aa69'>Commit.</a> </li>
<li>Improve updating the pointer position after screen changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a51171720e59a47c850a34629e99cba8ac4694d9'>Commit.</a> </li>
<li>[autotest] Add test case for update pointer focus after screen change. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=79c274b942992d08078a6055f8c0eac93f90c98a'>Commit.</a> </li>
<li>Process pointer warped positions like normal updates. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5b5a966e48a2548045fd5a284c34dfaed2dd1326'>Commit.</a> </li>
<li>[autotest] Add test for internal pointer warping. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3aeec88449d1e7f3aca16b4b9e3b9ec2caa0fee4'>Commit.</a> </li>
<li>[autotest] Test case for stacking order changes should change focus. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8e6f387a6ef7f39a1ece12969f789688983fdbc5'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f4e28e2c9cf8c0337b86e819aa04da9caafbbed1'>Commit.</a> </li>
<li>Implement lock screen security for touch events. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c8c33ae3987083d347015ebc006ff997f1a5f229'>Commit.</a> </li>
<li>Add check for lock screen in InputRedirection::updateKeyboardWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a311f9bfda412b74bdf243b1988c0550250351ca'>Commit.</a> </li>
<li>Improve lock screen interaction for pointer in InputRedirection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=16a33f662bf749303964d284ce4302c3b143d37a'>Commit.</a> </li>
<li>Clear touch ids when canceling a touch sequence. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=142e826191e668d2c5c1f4033c3ddcb16fc491bc'>Commit.</a> </li>
<li>Refactor input event handling to be based on filters. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d02c325a61a9561f4fd27833f8fadeb105fa3142'>Commit.</a> </li>
<li>Make animate() return something JS understands. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5e6f0b8b4fc3b896fe0350dc8a94d65c68053875'>Commit.</a> </li>
<li>Remove crash handling in kwin_wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7ccca92c3e4ae0001a143e256956c97dfb3d50a7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126655'>#126655</a></li>
<li>Maintenance - fixing naming typos. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=61e1a98fb7d64272e1de8682076d7675e2fcb528'>Commit.</a> </li>
<li>Drop InputRedirection::toXPointerButton. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=357e082d7a5feb4bf2c88d2eaed6a4699546c413'>Commit.</a> </li>
<li>Scale blurbehind and contrast besides translating. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e97eaffc6b1d5b65fbdcd25fa56c88b47fa5ad3c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126980'>#126980</a></li>
<li>[autotest] Add test for touch input. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4e907d6773ae57d02e294b268c18192811299a74'>Commit.</a> </li>
<li>Unset focused keyboard surface when screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a47b6f94352e746f65d4a9bd90f0d25139ecf025'>Commit.</a> </li>
<li>[autotests] Test case for lock screen intercepts key events. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=09cc134bfcf33235f216898da305036fc4ca9c19'>Commit.</a> </li>
<li>[autotest] Add test case for global shortcuts while screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=500bc9c18c51ed2b414d7a8c54402f1912e8a3a3'>Commit.</a> </li>
<li>[autotests] Test case for key events on internal windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=75261ab9f65dad452a00d7ddf1334c5ec68417cf'>Commit.</a> </li>
<li>[autotests] Add test case for effects key handling during screen lock. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=037b3ba66e3a2007b6b29c291ab9df72e37ec821'>Commit.</a> </li>
<li>[autotest] Add a test for a KWin internal QWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=dfd0664fd6702d35876a58c6afd8dc17bae98680'>Commit.</a> </li>
<li>[libinput] Install a log handler. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eca06ecb02a6ecae3fd15209a2f3b70bed211bcd'>Commit.</a> </li>
<li>[autotest] Move window through decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=66c77bcd31fc2a36d9cbdc65f1bba8f58f3db630'>Commit.</a> </li>
<li>[autotest] Test case for hover move on decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9f48ec62ce5ce79643a65309a32ba83fe1eb677'>Commit.</a> </li>
<li>[autotests] Add test case for double click on window decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=749ec28e237274579487fd0d5cae25837592c7d5'>Commit.</a> </li>
<li>[autotests] New test for input on window decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=938a0ff7fc7f354b7f3b2c9688ac8ac4ce3d1d43'>Commit.</a> </li>
<li>Add nullptr check in AbstractClient::checkWorkspacePosition for workspace(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4c7450a1f6c314fe71768b95067ae5d48cc73811'>Commit.</a> </li>
<li>[autotest] Extend LockScreen test for global pointer and axis shortcuts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7558fcd4b478c249de9fd3fa868e257b8cba73ae'>Commit.</a> </li>
<li>Fix axis direction for processing global axis shortcuts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d83780fea30902d8a3520cab25c62d0a18fceff1'>Commit.</a> </li>
<li>Check for Xwayland as RUNTIME dependency. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=48ec13a88182956eb7ee166b0c55d12d398c63a9'>Commit.</a> </li>
<li>[autotests] Extend LockScreenTest for locking while moving window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=25d07c22da55e35b3e6861b356c41f63698ecbd5'>Commit.</a> </li>
<li>[wayland] Delete cursor theme when internal connection terminates. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3ea4f161cffb4d1374aa15839bf18b7b80502713'>Commit.</a> </li>
<li>[autotest] Reduce code duplication in LockScreenTest. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a0cc3aac63d67ac87e619c350e9b7f1c4abd8484'>Commit.</a> </li>
<li>[autotests] Verify that Effects don't get pointer events on locked screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1baba74611050b4cb7f4e05741968dd8d46f431d'>Commit.</a> </li>
<li>[autotest] Extend lockscreen test. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=87d1f87cc087966008a4356519322d009ea64078'>Commit.</a> </li>
<li>[effects] Check for LimitedNPOT. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bd5f6420978a882d2b532faaee49512d98c300a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126966'>#126966</a></li>
<li>[wayland] Fix heap-use-after-free in idle time plugin. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ed3f55b9536264af277a429899b34e6e4be06eb2'>Commit.</a> </li>
<li>Depend glsl on TextureNPOT, not LimitedNPOT. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=db5a798952cab75f02e29bdb1126b321a537be56'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126959'>#126959</a></li>
<li>Export EGL_PLATFORM as x11 in eglonxbackend. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=176f492085f4a7f159cd78e525e1d52701beb51b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358750'>#358750</a>. Code review <a href='https://git.reviewboard.kde.org/r/126958'>#126958</a></li>
<li>[autotests] Adjust lockscreen test to improvements in KSld. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b905fa0f68ddefba999a3945674c75848a8ef2cb'>Commit.</a> </li>
<li>Set focusedPointerSurface to null when screen is locked and no greeter window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5e782ac93ea921d621e4547dd3631647a224213d'>Commit.</a> </li>
<li>Also consider the AcquiringLock state as a locked screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b5cbca36179b6f996eb72e0be6a889a6368550fa'>Commit.</a> </li>
<li>Port to CMake AUTOMOC. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8ea4f4dae7fbdf7037ac9a42e25eea6592aef446'>Commit.</a> </li>
<li>[autotest] Add test case for pointer buttons on locked screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=803d499287b38fee85817998e336f217aa1442e0'>Commit.</a> </li>
<li>Fix incorrect feature_info description for prctl-dumpable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=19e25a73c0231097f3dab5ad042444e852c081ad'>Commit.</a> </li>
<li>[autotest] Add a test for locking the screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7de9a402dffb07a1b072341e4a2c002cebc19521'>Commit.</a> </li>
<li>[autotests] Test quick tiling when moving a window with the Pointer. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1790168fb3da1cc77552a4d0fe4773c888d669dd'>Commit.</a> </li>
<li>[effects] Add a simplified fullscreen blur. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=22bd8badbf72b19a9c3849e4df3e873c1e2c9c0b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126906'>#126906</a></li>
<li>[kwineffects] Expose fullScreen property in EffectWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1fb0c31bb467e86e6d58e18a7b96460197de6f36'>Commit.</a> </li>
<li>[effects] Remove clip plane from Cube effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f155284281ac93cff9b9931a01a8a6817f00b13'>Commit.</a> </li>
<li>[effects] Combine all shaders in resources. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=613d76f2df35ff559036a1fd9d922268bd616105'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126905'>#126905</a></li>
<li>Set Workspace::m_compositor to null when Compositor gets destroyed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fbf14306d7677ca9c860e3fcbce31f535a0801ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358722'>#358722</a>. Code review <a href='https://git.reviewboard.kde.org/r/126925'>#126925</a></li>
<li>Use kwinApp()->config() instead of KSharedConfig::openConfig(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=757523a324d272a7479d8fbb2ac2345207091a1d'>Commit.</a> </li>
<li>[autotest] Extend QuickTilingTest for keyboard window move. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a12fb5ed030b65012674f13d7d12d85d505ce923'>Commit.</a> </li>
<li>Only send key press events to the moving client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bdb423da3d44f2de84679505096d9a198a6f6301'>Commit.</a> </li>
<li>Make QmlPath in Outline configurable. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2a935c523fe0700a8b3328a242d0fb0588ca2012'>Commit.</a> </li>
<li>KWin::Application holdes a KSharedConfigPtr with the config. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1f7daa934d8827bd4f061b96a9c8cf457ee60650'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2636aad5c32b31ba92e6e85b626362237cbec279'>Commit.</a> </li>
<li>[libkwinglutils] Cleanup Shader API: removal of ShaderType. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=54870d5e1420e2563944b243f0af036d2d7d7cd9'>Commit.</a> </li>
<li>[effects] Use passed through matrix in Blur Effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1752302203bd966c6caeb555b9358446def4d0fd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126215'>#126215</a></li>
<li>[kwineffects] Pass screen projection matrix to EffectFrame. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2e7bc0df87845e2a5e22f2280f7047033fa83af3'>Commit.</a> </li>
<li>[kwinglutils] Don't setup old shader API for rendering a texture. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ef7f7b01796121a0e4cae2b4a1c2f1603c7552b2'>Commit.</a> </li>
<li>Drop remaining old shader API usage from SceneOpenGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=84b73ab2b30b4ff2acac2ecd8c5314b7dd2b5bef'>Commit.</a> </li>
<li>[effects] Drop resetting GenericShader from cube effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=67d79385cf6682b190f41f41cc08f07757b154d3'>Commit.</a> </li>
<li>[effects] Change sphere to use shader traits variant. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b258dc53b3c7e9adc29e9439a3e0c89a988ed4f1'>Commit.</a> </li>
<li>[effects] Change cylinder to shader traits variant. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=318fb6989b1137a59a2b0b8ed80b46c2649396bc'>Commit.</a> </li>
<li>[effects] Adjust Cube to use shader traits shader for window rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6e35aded0b2368bf2ee1db3f30ad62af9fb79899'>Commit.</a> </li>
<li>Use shader traits API for lanczos filter's shader. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b366f0ac01b71be82a7a42bd6160cdec6fd6baa0'>Commit.</a> </li>
<li>Use shader traits API to render cached texture in lanczos filter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=99739106d16bd68764a06b99591f9eace85dde88'>Commit.</a> </li>
<li>[effects] Use shader traits API in logout blur. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2f5de22ac8488421bc86b614d5447e05485ecd87'>Commit.</a> </li>
<li>[effects] Use shader traits API for vignetting in Logout. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=af67391710b8e3c3084ae32c71aede91a8230e0f'>Commit.</a> </li>
<li>[effects] Use shader traits API for lookingglass. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9aeb2f7907ba6c82be545263ef464f7c79d541d0'>Commit.</a> </li>
<li>[effects] Fix reflections in coverswitch. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=18a15a59d511d31fdf6bbe20d2f89864edcaa373'>Commit.</a> </li>
<li>[effects] Use shader traits for CoverSwitch reflection shader. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a5e86c66bfa60eb26f7d38b62c135f5740e4ac29'>Commit.</a> </li>
<li>[effects] Use shader traits in Resize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9fcedcc2f8096bc27d16c8429de398f273fc3f1d'>Commit.</a> </li>
<li>[effects] Use shader traits API for reflection shader. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5c59f4261bded3f9f809376338d892b0d7ecbdf4'>Commit.</a> </li>
<li>[effects] Use shader traits API for CubeCap shader. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6bdef479a99f5244d65ff9d5b1581efcba085eb5'>Commit.</a> </li>
<li>[effects] Port startupfeedback to shader trait api. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=712e46e468aad6a0cd373bb99352b50d80b2556a'>Commit.</a> </li>
<li>[effects] Use shader traits generated shader for invert effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f13622a9144d4ed478b6431f9519be36aba48432'>Commit.</a> </li>
<li>[effects] Simplify setting mvp matrix for cursor in zoom effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7facdb67ee5b71ef1edef3777c31620bb486f863'>Commit.</a> </li>
<li>[effects] Drop resetting Generic Shader from zoom effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4e8e0f817f0a5cca93da3c9a14416280ebd2aeae'>Commit.</a> </li>
<li>[effects] Use shader traits API in trackmouse. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4b96370e67cd96486a6d81f09a9c73c676b457b2'>Commit.</a> </li>
<li>[effects] Use shader traits API to render wallpaper in cube effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a09ede8cc9a59c63407a206aaaaa80c9cb804d9e'>Commit.</a> </li>
<li>[effects] Use shader traits api in screenedge effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8d9321ca4c82d1113fb83009a52b2de8bb36590a'>Commit.</a> </li>
<li>[effects] Use shader traits API for MouseMark effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=700adad9b191d6d3ab0e9cd9f4456608208e1e50'>Commit.</a> </li>
<li>[effects] Use shader trait api in MouseClick. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2add89e2740efcbeead57ea1bd7840581d8eb57e'>Commit.</a> </li>
<li>[effects] Use color shader traits in magnifier effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ff9ffe77ba2a14b9ea55e81c39d27e2eed05885a'>Commit.</a> </li>
<li>[effects] Use shader traits in SnapHelper effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6aa19b15877ad53c8243868ed342dad07b09055e'>Commit.</a> </li>
<li>[effects] Use shader traits in ShowPaint effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2de0b527d00146e6083ef2ccc8a14792e6f36b6f'>Commit.</a> </li>
<li>[effects] Switch ShowFPS to ShaderTraits based rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b757b7e17d8bddd94a3dfd081f8dfc74279370c1'>Commit.</a> </li>
<li>Render effect frame with shader trait API. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d76f08e359a006aee46e5d859c13b05220b49dd2'>Commit.</a> </li>
<li>Always set mvp on shader in SceneOpenGL2Window::performPaint. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0d1da1c583acdab02d646b56488284a232fb1d99'>Commit.</a> </li>
<li>[kwineffects] Add a generateCustomShader to ShaderManager. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4b11b7b6b8a9bdcdc2e2104fc06ba554d1c50074'>Commit.</a> </li>
<li>[kwineffects] Pass screen projection matrix to WindowPaintData. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3be6ff9f1250e2b7c235d2114c56726667bf1abd'>Commit.</a> </li>
<li>Pass projection matrix to ScreenPaintData. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f284ef814cc880abe99f71ebb7c84ef396512d51'>Commit.</a> </li>
<li>Use ShaderTraits based shader for paint background. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5657405d3215937a549fe1bced79fdb50f4e4cfd'>Commit.</a> </li>
<li>Create the new projection matrix at start of frame rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ad7246e01055b0636fe0bb9bfb0b02737b4da720'>Commit.</a> </li>
<li>[kwinglutils] Add a ShaderTraits variant to ShaderBinder. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=778a7fd1a0f7ce89df9a8e1b35e8beb5461ab7bd'>Commit.</a> </li>
<li>Fix the build. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fbd1122a15d06223af7f0a2ca156e31344c56c82'>Commit.</a> </li>
<li>Make use of Xinput lib only if we found it. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bb7674f2b63de618bb9f92b04b852923be4a6f71'>Commit.</a> </li>
<li>Emit shadeChanged before calculating unshaded size. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1c344c16d9161a646eec076e79f87ae7397a4648'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357669'>#357669</a>. Code review <a href='https://git.reviewboard.kde.org/r/126671'>#126671</a></li>
<li>Move doubleclick logic into press event. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ed1d32288b50647469fb0e000f21b849e286ca36'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357450'>#357450</a>. Code review <a href='https://git.reviewboard.kde.org/r/126631'>#126631</a></li>
<li>Prevent nullptr dereferentiation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=95cbd7c1b3cdbe81fdee1682049bd08ab7fe99fb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357032'>#357032</a>. Code review <a href='https://git.reviewboard.kde.org/r/126787'>#126787</a></li>
<li>Skip SWAP_BEHAVIOR_PRESERVED for supportsBufferAge. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c7aefc6b6b62c704929fc74338da2ca0d07b547a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356992'>#356992</a>. See bug <a href='https://bugs.kde.org/356882'>#356882</a>. Code review <a href='https://git.reviewboard.kde.org/r/126783'>#126783</a></li>
<li>Use XCB_CURRENT_TIME for NET::TakeFocusProtocol. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e73e331f35b9da4bbb99c80c5c1eedd2ae99422b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347153'>#347153</a>. Code review <a href='https://git.reviewboard.kde.org/r/126753'>#126753</a></li>
<li>Actually keep the expandedGeometry alive. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=87795eef2a71d85680f797fe75404ca1a9a63a10'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/318322'>#318322</a>. Fixes bug <a href='https://bugs.kde.org/320892'>#320892</a>. Fixes bug <a href='https://bugs.kde.org/344359'>#344359</a>. Code review <a href='https://git.reviewboard.kde.org/r/126323'>#126323</a></li>
<li>Fix build of rules kcm. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f918bc3367c512219b64ef061a8fd75f8e0b30ed'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126733'>#126733</a></li>
<li>Force restart on crash. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=69aa80750f8d61a5db6311c33751461041a260d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348834'>#348834</a>. Fixes bug <a href='https://bugs.kde.org/353030'>#353030</a>. Fixes bug <a href='https://bugs.kde.org/353428'>#353428</a>. Code review <a href='https://git.reviewboard.kde.org/r/126741'>#126741</a></li>
<li>Update expanded geometry when slide is done. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=57c9aa9fc03d8af1afd63b43c136894cdef621d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/318322'>#318322</a>. Fixes bug <a href='https://bugs.kde.org/320892'>#320892</a>. Fixes bug <a href='https://bugs.kde.org/344359'>#344359</a>. Code review <a href='https://git.reviewboard.kde.org/r/126323'>#126323</a></li>
<li>Use XInput for "polling" the mouse positing. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5a3161846177797d4a8d7c28c6ad4f00e2177206'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357692'>#357692</a>. Code review <a href='https://git.reviewboard.kde.org/r/126733'>#126733</a></li>
<li>Remove saving and loading (and client matching by) WM_COMMAND. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2eac7634cc524bf5e425cf081a639a6b6407e380'>Commit.</a> </li>
<li>Remove KWIN_BUILD_COVERAGE. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3e255e6969fc3bc03ffc0651b68b5d891abb674e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126628'>#126628</a></li>
<li>Avoid undefined behavior in nearestPowerOfTwo. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=bcc36b87fb0e8608b153dbf24f03403fe759ffb0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126540'>#126540</a></li>
<li>Widen int to qint64 before multiplication, not after. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4957c18a445b2655c91ad1d36325b88c3d714172'>Commit.</a> </li>
<li>[wayland] Introduce a memeber variable for checking whether ShellClient is internal. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ecce0369796ca70cd112e7ba4da6c93dbd6e4784'>Commit.</a> </li>
<li>[wayland] Properly implement userCanSetNoBorder in ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8604e0310691c782c160ba8465d4c71d8ba1348f'>Commit.</a> </li>
<li>Include abstract_client.h instead of client.h in decorationbridge.cpp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7ed4b5ed1abd0f0d31284644996ff0c216602669'>Commit.</a> </li>
<li>DecorationBridge::recreateDecorations operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2d7477359ee82ba0e3c43297bab97fda50aca4d3'>Commit.</a> </li>
<li>Add a Workspace::forEachAbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=89be6cf4ed8e5d9598b2df63b8f0fabbba45fec3'>Commit.</a> </li>
<li>Add window decoration to ShellClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5e96f65224803ef5deec82e58b3bf83f3e9453ee'>Commit.</a> </li>
<li>[backends/wayland] Use server side decorations if available. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5cfe9428aa6c8b7828f1506eba82daa8427e2fae'>Commit.</a> </li>
<li>[wayland] Add support for ServerSideDecorationManager. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5cef26d27566a045200ae002a05af98b13b84b66'>Commit.</a> </li>
<li>Move implementation of clientPos() to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3de3a959c68003bb5836eb76d2265e4418b7e612'>Commit.</a> </li>
<li>Introduce a Toplevel::clientContentPos() -> QPoint. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=827486ff36857ecec21631e98be7a763bb0c2f20'>Commit.</a> </li>
<li>Introduce pure virtual, protectd AbstractClient::acceptsFocus -> bool. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f90fa5cfd4d91deee2378ca749c7f04cb3a8106'>Commit.</a> </li>
<li>[wayland] Translate inputTransformation matrix by borderLeft/Top. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a4c347fc7a7832f74b03581d93b44db08a5ed4e7'>Commit.</a> </li>
<li>DecorationShadow operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=d28fba883977a14ba11a96e430460e596912d171'>Commit.</a> </li>
<li>[decorations] Schedule repaint for renderer on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=60b09586ef235d5f7363b4daee626e27ed5547cc'>Commit.</a> </li>
<li>Add a Workspace::findAbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0bf2b1de0f5036cd3e929543bd29cf52db9ffc9a'>Commit.</a> </li>
<li>DecorationBridge creates Decoration for AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cb400d71285049affe9efe00ceebe294560b376b'>Commit.</a> </li>
<li>Group decoration related variables in a struct in AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9ae2b505fa7b31412e6fb4c068cc91b92f50170c'>Commit.</a> </li>
<li>SceneQPainter::Window::renderWindowDecorations operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f4246ba7e230bf79151711e81e381dba77490e16'>Commit.</a> </li>
<li>SceneOpenGL::Window::getDecorationTexture() operates on AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=401efc28a87c8fb97a064f46a017a64dbd27d28c'>Commit.</a> </li>
<li>InputRedirection::updatePointerDecoration operates on AbstractClient for deco. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e61ad65f104ff0d1a64bacfc74a92944dd83df8c'>Commit.</a> </li>
<li>Drop the Client specific code from Deleted::copyToDeleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7fc15b48fba288546727cebd0db15acba9006a52'>Commit.</a> </li>
<li>DecoratedClientImpl switches from operating on Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6270ea35a2e4f469ae0f331908a60cb568967085'>Commit.</a> </li>
<li>Move decoratedClient from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4873b2933f48ef994977c314e472ff253f57eb2d'>Commit.</a> </li>
<li>Move some decoration related signals from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ce84ae4b654e586674d27033378d8a4f5bf4875e'>Commit.</a> </li>
<li>DecoratedClientImpl::client returns AbstractClient* instead of Client*. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1758db337fc410fa92a8bac0d62f5827408458c0'>Commit.</a> </li>
<li>Add virtual AbstractClient::showContextHelp and ::providesContextHelp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ae28c2499df5b43828959ddda5d25842ee194234'>Commit.</a> </li>
<li>Move processDecorationButton(Press|Release) to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5de55b61e7d8a75a01cafef2d5b4754f69ab68e0'>Commit.</a> </li>
<li>Mode dontMoveResize from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=38d3346faa3fd7b8d38930b6322e3f78f67430f6'>Commit.</a> </li>
<li>Move processDecorationMove from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a15ccc2215b82634da7d0d08ce663d95c4c0024c'>Commit.</a> </li>
<li>Move implementation of mousePosition to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d618359882db958898dfb04387e75dc5ece82ee'>Commit.</a> </li>
<li>Move property noBorder from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ad6ead19283c9de4b3ae7e914b9975e82207073e'>Commit.</a> </li>
<li>Move implementation for borderFoo() to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d36bab6b2358bfa609345bf8f5378dc22c39159'>Commit.</a> </li>
<li>Scene::buildQuads can operate on AbstractClient for decoration handling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a82daabfe932517198543351bb58d73364a7ff93'>Commit.</a> </li>
<li>Move handling for decoration rects in Deleted::copyToDeleted. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f112b0b052a7237caba1074bcd78659f9524a770'>Commit.</a> </li>
<li>Add a base implementation for layoutDecorationRects to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c1053ce1a54f7786a129a2516290cd265aaa3b6f'>Commit.</a> </li>
<li>Move triggerDecorationRepaint from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1659c0b6412a7de2163f06b02c7c3752c3b945c4'>Commit.</a> </li>
<li>Move decorationHasAlpha from Client to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0a82746f915b32e8cfddae6ecdb84a94465b85fb'>Commit.</a> </li>
<li>Move the KDecoration2::Decoration to AbstractClient. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ebe29896490177089436a6537ad48c20b9d39859'>Commit.</a> </li>
<li>[autotests/wayland] Disable KActivities in the tests. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5f502c141ac1a016f72841055c1045435b088feb'>Commit.</a> </li>
<li>Specify inputTransformation in Toplevel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=55bae74aaef489776c2ca4986f8fd0fe915206e3'>Commit.</a> </li>
<li>[plugins/qpa] Safety checks in native interface. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6c746764b42701bb992ed069f82a69fcf666ef45'>Commit.</a> </li>
<li>Introduce a --no-kactivities command line option. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=20a9a2a247807e5e6c34e96ddbc33d7faf6f6105'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126153'>#126153</a></li>
<li>[backends/virtual] Fix include guards. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2e191bb75110039f6f187b50db4787d15f2b8e35'>Commit.</a> </li>
<li>Use SERVICE_TYPES parameter to kcoreaddons_desktop_to_json(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc29d0d2968d14693d5613b803ed355eb428675f'>Commit.</a> </li>
<li>Emphasize minimization in m. all script. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8885a6798a348cc709a3423f37593cc3f6ce6eee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356161'>#356161</a>. Code review <a href='https://git.reviewboard.kde.org/r/126225'>#126225</a></li>
<li>[wayland] Disallow ptrace on kwin_wayland process. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e5a27cffb1650d85f0b04b8095f8ff7e15a7346b'>Commit.</a> </li>
<li>Allow rendering of input method windows also if screen is locked and OpenGL is used. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=03909b9d5874d5c73914348dff2ec5eea5a51b80'>Commit.</a> </li>
<li>Allow rendering of input method windows also if screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8e66832ba3a8e8174f2c583cf3abe53f596015ba'>Commit.</a> </li>
<li>[wayland] Pass processStartupEnvironment to ScreenLocker. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e2dc6380d1a57382cbc720394a5dbac5d18da48c'>Commit.</a> </li>
<li>Fix build with Qt 5.6. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7c8adeff9ec9e16bf0ad2971d5edeb484a302e6b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126234'>#126234</a></li>
<li>Fix regression in EglOnXBackend caused by b1914b4b2cc4a97d2522e8b83cee54b30f8a13c5. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c5959a5ffa54bb87cf654f54363637acb34becc1'>Commit.</a> </li>
<li>Add a check for waylandServer in the dtor before using it. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8680586dbd0888087767be8e2a80a844aa024a30'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126190'>#126190</a></li>
<li>[backends/x11] Add a dedicated EGL backend for multi-surface rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=04ea8ec22f3528e5802112761382a05544695c06'>Commit.</a> </li>
<li>Refactoring of EglOnXBackend to allow implementing subclasses. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f8e7d58dea03ea996e4dafc46d4bfe2c94125164'>Commit.</a> </li>
<li>Add pure virtual OpenGLBackend::init() method. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b1914b4b2cc4a97d2522e8b83cee54b30f8a13c5'>Commit.</a> </li>
<li>[wayland] Properly set position of OutputInterface on creation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8b4d1a2f3f6449ace178b2becf9f947a56142370'>Commit.</a> </li>
<li>[wayland] Add support for initial output count for nested compositors. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=56bd1e7194de9b9dc45cb24b67166dcedeb3cb73'>Commit.</a> </li>
<li>[backends/x11] Prepare for multi-output rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1e3013b58c6380c27b87154c562f46e702ed5b9f'>Commit.</a> </li>
<li>[backends/x11] Prepare QPainter backend for multi screen rendering. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f5905e5e48cc0990b369f25fa7e577b82578b181'>Commit.</a> </li>
<li>[wayland] BasicScreens can serve multiple screens. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ee7f70afc8880e42136db75d998679f7870f5b8c'>Commit.</a> </li>
</ul>


<h3><a name='kwrited' href='http://quickgit.kde.org/?p=kwrited.git'>kwrited</a> </h3>
<ul id='ulkwrited' style='display: block'>
<li>Disable fallback session management (requires Qt >= 5.6.0). <a href='http://quickgit.kde.org/?p=kwrited.git&amp;a=commit&amp;h=e2bb0d7595c0898e8351bbcb721ad7173423f8f5'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=kwrited.git&amp;a=commit&amp;h=c5ae7ee04fb94d2abee5d4802cfccaa165a6e09e'>Commit.</a> </li>
<li>Port kded plugin to json metadata. <a href='http://quickgit.kde.org/?p=kwrited.git&amp;a=commit&amp;h=5fd327369bc664b99734136af3911b9bf0e87593'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8c4e72e928e4788467dac184cd2571bc3696d9a7'>Commit.</a> </li>
<li>Mark kwayland as required dependency. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=48a304ebab371e8db5c3809db5cc7d8e614bce72'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=92cff99ba40c5a73f0b2669c7cb371b295c4fec7'>Commit.</a> </li>
<li>Merge sebas/wayland branch. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cc4de29cf24f06e51dbc7e221ccb89162be525d4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126381'>#126381</a></li>
<li>Stylistic improvements from review. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fc3664e7727c2acb023ff2b56b5c1356183fd7e3'>Commit.</a> </li>
<li>Stylistic improvements from review. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=32240bdbef9f4958be6db6fa89fb52cae7d0a3aa'>Commit.</a> </li>
<li>Stylistic improvements from review. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=30f812eaab9267fdc695f1fb0c2768280d828974'>Commit.</a> </li>
<li>Stylistic improvements from review. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3357f50f779da0648bab184b645e1ed6167ad68b'>Commit.</a> </li>
<li>Tighten lambda. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=feef12ed06f2e6769d93de482ce46494bd18ace1'>Commit.</a> </li>
<li>Tighten lambdas. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3d023a0f02bdc9e8300a43e4fa6570a58f0c530c'>Commit.</a> </li>
<li>Setters and asserts for blocking signals. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=01fa128e024d18093b05f196a4693437ee046bdc'>Commit.</a> </li>
<li>Enable all tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=094ff1434a2169c140639784eb48128c4aad3c71'>Commit.</a> </li>
<li>Pass the notified config in signal. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=31230b6c6aeb671719fcb16002a46407aacabef0'>Commit.</a> </li>
<li>Compress events during config changes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=79acd5b59810713ee7462194844a836659f5afd9'>Commit.</a> </li>
<li>Check if the output is really disabled before. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=838ff410f703a3b64d16dfee651c45bd4631630f'>Commit.</a> </li>
<li>Kill the static. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=12c6c35a1cae3afe52eca829df3ad70ccc6a3dfc'>Commit.</a> </li>
<li>Tighten the lambda and make it a bit clearer what it does. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a1968aeb132b991cec7245f513fc21003c704986'>Commit.</a> </li>
<li>Use initializer list in configreader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=71dbbf19d71b4f65c6b610ef38f8002fb3a60c56'>Commit.</a> </li>
<li>Const& in foreach loops. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3a16abd3549a2c834688b6af4d83e1b4bbd0a06a'>Commit.</a> </li>
<li>More graceful thread termination on disconnects. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3a00c919e535b3903492b62ff76a8376eba43b89'>Commit.</a> </li>
<li>No custom timeout. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=60ae5cc2506e4823052f683f28ce385999b03ee0'>Commit.</a> </li>
<li>Const& in autotest loops. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=edc06aa402a9a7a39cc466e44ebb7f7485ef151b'>Commit.</a> </li>
<li>Use QStringLiterals. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0dc08142b6593221b50f81216f7c54fad074afa1'>Commit.</a> </li>
<li>Round refresh rate, instead of truncating it. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9e6982d5e57801289b85f9e89b32ddb9ae1ebca0'>Commit.</a> </li>
<li>Improve mode id lookup. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b701bb204dd85eea1e258d8cedc1ff46dea0cee0'>Commit.</a> </li>
<li>Use constFind instead of [] lookup for rotationMap. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d2d71cb7ad1b9772bc9408320f18feefe9d361eb'>Commit.</a> </li>
<li>Use initializer list for rotation map. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3e3df38e1d8331503d4abe38ddd43952c38289b2'>Commit.</a> </li>
<li>No wayland client needed anymore. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=acaa693441a754c5ff312a251878100a0daca893'>Commit.</a> </li>
<li>Update copyright year. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c93f5157c404b81c6b54420ca4443b27c2fdb6ed'>Commit.</a> </li>
<li>Remove edid from wloutput. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0138af6240cbfe5457684522488247c62ba0aef6'>Commit.</a> </li>
<li>Tighten lambda. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b7f2fcab67a8bb1e395001fc1226829ae2590ed8'>Commit.</a> </li>
<li>Tighten lambda. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8f7a8e37dd22f71d0954ce6a71535f879b898659'>Commit.</a> </li>
<li>Default connection. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=03fdbb61c42e38319c30f9b313e0d26d7e9cbd40'>Commit.</a> </li>
<li>Fix cleanup ordering. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f00b22ec25dd8daab5f1327839a061ed148dd4a6'>Commit.</a> </li>
<li>Connect before initConnection. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9af52be2d8cc3789a50cdb15b3a827c2e67f3768'>Commit.</a> </li>
<li>Stylistic improvements. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cbf317ba20b667ffc97051d30eb6db357ad70669'>Commit.</a> </li>
<li>Override instead of virtual in backend implementation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6478368e8279a165856ec9347f3646e496b49035'>Commit.</a> </li>
<li>Kwayland instead of wayland in plugin id. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=357f3a2f8feb03f05f0ca534a82c3a8c4a4b94f3'>Commit.</a> </li>
<li>Enlist -> list. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7b8659fadddf97697730a1e05035c36f40019835'>Commit.</a> </li>
<li>Use markdown for README. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c4a93b32acafb3c832daf9368e158c9f3ba818d9'>Commit.</a> </li>
<li>Use XCB_RANDR_FOUND to enable/disable the xrandr backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1e2b2b5aadf86de3c5694a639b413668f7bac313'>Commit.</a> </li>
<li>Default timeout in tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=17e2950dbc26a7e6eb4d56ceb661e8a5cd95fffb'>Commit.</a> </li>
<li>Improve rotation change test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=12d84627c38bb9178159caac1a6c9e24ec9f1d50'>Commit.</a> </li>
<li>Cleanups in tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=864d7bbf982ece99d85804550946564dbe27a0eb'>Commit.</a> </li>
<li>Remove TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=aeb4609cf2c99a6a8ac2e2fa57022c1b26f68f27'>Commit.</a> </li>
<li>Update todo -- everything is done. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=744fa449da9409bc92dc064c9efa26acd71d8027'>Commit.</a> </li>
<li>Less chatty, but do show edid. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=de41edb1fa84e9ce4d076d013f0bf949d2a1364d'>Commit.</a> </li>
<li>Unit test for edid data of kwayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=606e1f9d1b0538f96f035107c024d4bf3248b1ff'>Commit.</a> </li>
<li>Fix edid of kwayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=455fd44834757726dba741d465eaa30155de9379'>Commit.</a> </li>
<li>Noise--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9cb7c5844db6b5527e9946512f7f51a16186c056'>Commit.</a> </li>
<li>Clean up WaylandScreen, looks fine now. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5531eaeac986fcf863942e254238b8a4bea452ee'>Commit.</a> </li>
<li>Count already before adding. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fb991610aa24a6a60af41c8f4082154e8a483c56'>Commit.</a> </li>
<li>Don't emit configChanged too early. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6ba76a7c62e559af246b4fe172c466cde9c422b1'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=68fa78f164f487e5ae85bdfbff8cac737dfa1df0'>Commit.</a> </li>
<li>Always connect new backend objects. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c1670099215a5fea060b4023510d6fbb557d5a52'>Commit.</a> </li>
<li>Map between kwayland and kscreen ids. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6bf29a1885bc65ae765445dc8d8d03601c2a7036'>Commit.</a> </li>
<li>Add tests for hotplugging outputdevices. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=891a0398baaccd299cdb2eb6722492413d9e1ecd'>Commit.</a> </li>
<li>Cmake file for test server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=921846df31ee53c45ae7fdabfdd8240e4bac2acf'>Commit.</a> </li>
<li>Small cleanup in testserver. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b2c1becb01ab4d03a04a3fd3576291d13adf2bcf'>Commit.</a> </li>
<li>Enable all tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=eefab822e30bdb3fe0b51e16cb50939f059ec8f7'>Commit.</a> </li>
<li>Wayland backend is built unconditionally. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f9d91cdce87c86b88e533902b00c894e6d40f180'>Commit.</a> </li>
<li>Factor out server a bit clearer. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=20b785aea061ebb448ad5952c065e966d25062f0'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0b8c66e05c1c7fa15f80984739fc8363b6e452ab'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=298e9e2fa1e7b76b9721dc434b901a78f2a5725d'>Commit.</a> </li>
<li>Clean up waylandoutput. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bee0aa230fda447da00580f6ba22744f0479b6cb'>Commit.</a> </li>
<li>Clean up debug in tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=500489bb74e129b761c2463a6eb3aaec75525914'>Commit.</a> </li>
<li>Clean up configserializer. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0e9147a084c9c05d65864ff64f2b877c184bbea8'>Commit.</a> </li>
<li>Clean up debug in backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b7182faa6d998f952cd0b0fdc5ba07fc1bb50749'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2a7237c9416e4871cec1244bec57f7c8665c3d09'>Commit.</a> </li>
<li>Kill waylandconfigwriter. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dca1fadcb307a339a1d3e6ee9ac2db42743fb478'>Commit.</a> </li>
<li>Move waylandconfigreader/writer into tests, they're only needed there. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=12067f92d5fc7d4bbfa8e3a0464d3389c6a57e4e'>Commit.</a> </li>
<li>Remove redundant tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0f901c6971848f7c131016b1e415ec9bf0eeb163'>Commit.</a> </li>
<li>Remove testwlwrite autotest. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ad4ee73b4a6352bf09848046c5c7b7cb596934c4'>Commit.</a> </li>
<li>Server can show overview of outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9aa4a2cb5a327ec9dea0dd1d623c9fe8bceb6dfb'>Commit.</a> </li>
<li>Shush. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1d20a5eac0543ca55ad0eafdc1d3b3a9654c05ae'>Commit.</a> </li>
<li>Cooler UUID for our outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c59368ee7edaebdbe0309002369e00fe67544039'>Commit.</a> </li>
<li>Clean up and improve test a bit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=44278c96e670c0b5ab1765530c9566e031e9acb2'>Commit.</a> </li>
<li>Make sure we're not handing mode ids out twice. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=49dc90c89ec6951fb81a5b7f1f2cce1060629660'>Commit.</a> </li>
<li>Test setting the mode. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=968fe87c7a850b0085bd6bbc249e4c368cee7f62'>Commit.</a> </li>
<li>Implementation for mode-setting. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6c15063dc8cca227daaa4bba58a17bae61625017'>Commit.</a> </li>
<li>Add logic to show info about the current mode. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d69a969e70ba9a125f721bc814fba8dae20f3eed'>Commit.</a> </li>
<li>Add mode change test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a91b55989ea3bc5fc27021dda5cc1d3bab7bc08'>Commit.</a> </li>
<li>Rejig mode id logic. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a9191a93a08857a56d38d334eae55eebb44b5cdd'>Commit.</a> </li>
<li>Cleanup dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f5347562e0e1bdf9fd71b989fbeeef9e8182abe7'>Commit.</a> </li>
<li>Create new ConfigPtr in Backend::config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7cc972419fde080097b4b0a2dce580e8722a9244'>Commit.</a> </li>
<li>Add note. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6029cc63dd99ee564cc4b1a8a35f999ef0a63f3f'>Commit.</a> </li>
<li>Add debug. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=97b6b82867c434f1500c3ad3d00631a40c6d9229'>Commit.</a> </li>
<li>Add separate tests for rotation and position change. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2ce29d9e764088e613d87d120de577690c647c51'>Commit.</a> </li>
<li>Add debug when something changes in the server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=719e0780476bb8c70d1faf2fc0540c5b8d40019e'>Commit.</a> </li>
<li>Implement rotation/transform and position change. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5c22d81193f0c83f5ba4c1fa9dd3a210a3230c0c'>Commit.</a> </li>
<li>Cache the kscreen config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f7f9a24a3863e8377a479dc1c1ef4a5dbcc28984'>Commit.</a> </li>
<li>Add debug. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bc5da79fda226a36c2eb0bc54b322c4204eefd77'>Commit.</a> </li>
<li>Wait a little longer before bailing out. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6c69a43e272038e1213a2aee02c3007d02361b62'>Commit.</a> </li>
<li>Test server applies the changes to the outputdevices per request. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=348c06f34aa94eb00e3fca5ed3a0dbb729be383c'>Commit.</a> </li>
<li>Try wiring up output changes to backend changes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=72077dabea26a754c9505bb0c44153d9da1c0dea'>Commit.</a> </li>
<li>Rename to outputdevice. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=13b47951a5bf89d10c8f147990e4878e0904d7ac'>Commit.</a> </li>
<li>Monitor client config for changes (fails). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0e347b7ade8adc40925ca717f69e9a79be89b0ec'>Commit.</a> </li>
<li>Apply new config in wayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4701eb75fb9e3c9d0ed94e8fe56827974f25dace'>Commit.</a> </li>
<li>Add debug. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cb8cf0a6a4effd1a581a49b3c194c8010a2b9d4e'>Commit.</a> </li>
<li>Try enabling a display. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a50f7b5fb340d194b35788dce7749580d14357b2'>Commit.</a> </li>
<li>Our test uuids also include the id field. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=45932df1f984e260b23abb1e866c7135dbbd3cd2'>Commit.</a> </li>
<li>Clean up and implement slot for configChangeRequest. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1f5e3ea87100dc9ff0c01dabfbb4954cae6607d3'>Commit.</a> </li>
<li>Also run this test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=202970035950cd45ef1126c7213e78025aabd3e4'>Commit.</a> </li>
<li>Add new test to build. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=94dcbbb3a636add4d6d2377eb1070db40cf1d76b'>Commit.</a> </li>
<li>New test specifically for setconfigoperations in kwayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d0c3d0419b3234e47a31f7134078531e2dd625d9'>Commit.</a> </li>
<li>Clean up config handling from server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=38e3b7e5c0845c23507e4b61ff1aa97236c8e30c'>Commit.</a> </li>
<li>Less debug. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e0db9a1a9b195d622b4b8d9836fdb16c7aa7ad91'>Commit.</a> </li>
<li>Set a made-up uuid to the output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f7d60a36a509397c4cb99b1e4f183f4c345e6271'>Commit.</a> </li>
<li>Don't spam for every primary change. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dcc520fe371551daed6269c7a370ce926ea983a7'>Commit.</a> </li>
<li>Simplify and adjust to name change. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0f8adbf27b06ef91fa0fb1e77986ba89f7422f53'>Commit.</a> </li>
<li>In-process mode merge. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3382bba2b403f82f3e21e4a89a6631781e5950fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126101'>#126101</a></li>
<li>Delete the pluginloader right away. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6be1c38317478fc32c5b329a62f0df740ed10370'>Commit.</a> </li>
<li>Be more careful about shutdownBackend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3473289d39fc1e2da5205c38c88dea9bd3adce04'>Commit.</a> </li>
<li>Improve warning. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1cc8349cf08566d97c9451fcc0667588d1a33ffe'>Commit.</a> </li>
<li>Use categorized logging. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=14a3affd54f4757b332ea6d472b0f87bf95a4368'>Commit.</a> </li>
<li>Simplify backend loading a bit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c02dac308987d546ee923ec6433d2b3a314b33cb'>Commit.</a> </li>
<li>Clean headers. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b4378190c5b4be6539367969b41f38494cbdd3db'>Commit.</a> </li>
<li>Check results of exec(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4aaa989362c80af1fd6bd99f57b3c3bf6dae3109'>Commit.</a> </li>
<li>Return the backend* right away. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=da943272c010a581ebbb3d4e3f3d8342366a6539'>Commit.</a> </li>
<li>Don't store backend* in the job. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dbc920a169e0c7087537395fa15d5e720a1041d6'>Commit.</a> </li>
<li>Save a bit of memory. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a915a0f18f3fea905992c7ca4a4d232a4cd1bb8c'>Commit.</a> </li>
<li>Warnings--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=88a8592b4408039edeaf8df425a12cb38e1b6861'>Commit.</a> </li>
<li>Cleanup unused arg. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=445d5e003cc675cba04318213c2c69ad3ddf300f'>Commit.</a> </li>
<li>Also rename internals for mode -> method. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=14c9d8748dd8e1c085592f7c5b49fc55d7fe3989'>Commit.</a> </li>
<li>BackendManager::mode becomes method. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=82e056d63f68231ecc0c7ea1d725ab475cf97c8b'>Commit.</a> </li>
<li>More asserts in configmonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6d6bad49eea07f34c876e714b88f4ab9f1435c3d'>Commit.</a> </li>
<li>Asserts in ConfigMonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9f17d200dc5fe033c36a53c5994e4069206398da'>Commit.</a> </li>
<li>Use asserts in BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c516001cfadaf4c5dec53af7bcb7b1efd58f0dc3'>Commit.</a> </li>
<li>Debug and coding style improvements. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e5d75f074b5d6e52eca3d38c403fa5368b05ce7e'>Commit.</a> </li>
<li>Remove factory methods now the operations are merged. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=aff27f74545e3a8ff74f4828a1261628c8d3b8a5'>Commit.</a> </li>
<li>Add asserts for modal code paths. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9d60394a54cf848498baec11bc5512c1aac7d79f'>Commit.</a> </li>
<li>Clean up dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=86d4ef5d38d50d1991eb24f225aac3dc57d1646f'>Commit.</a> </li>
<li>GetConfigOperation becomes modal. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b997ac9e35fbe8fd848a4f186298ef4b24af517e'>Commit.</a> </li>
<li>Kill setinprocessoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ef8250584e3a44becccf14b0c10927268849be25'>Commit.</a> </li>
<li>Clean up Operation loadBackend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3bba0c50e2c544d095397b88fd12e8552ae1632a'>Commit.</a> </li>
<li>Various small cleanups addressing review comments. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b96b803c8ab42ed109bb2c42634c33e9bcb2f417'>Commit.</a> </li>
<li>Use qputenv rather than setenv. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4ae4e5865ce84c24b2c59723d052dc56cc77ba83'>Commit.</a> </li>
<li>Only a single backend at a time is loaded. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6b6f00998fdd58034e8409e46b1bf156de3b33b4'>Commit.</a> </li>
<li>Rename loadBackend methods to loadBackendInProcess and loadBackendPlugin. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f7dab18a3858cc683ea24de3f80ace9df4f17f81'>Commit.</a> </li>
<li>Bump so version. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2f109feaf0ad2e2b445b48a3084fda3e58911079'>Commit.</a> </li>
<li>Cleanup dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d297e3363bff3555a1b1c9214269cffdc82c89db'>Commit.</a> </li>
<li>Avoid crash. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9f72e5dbc32145f72cfaa7aee0658fff7cfdc5de'>Commit.</a> </li>
<li>Autotest for in-process-configmonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c6e9690e4a897cd9942ee2eb0e5bc1ecd46117e8'>Commit.</a> </li>
<li>Make configmonitor work in-progress. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=793a9a802ee71ddfb563e0360901107995368100'>Commit.</a> </li>
<li>Whitespace fix. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3d21ae2c52901519d61d6e4c8e09b519483fb94c'>Commit.</a> </li>
<li>Cleanups. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a6cc4ab6acca9daefe649e9a041bc272d22b2df'>Commit.</a> </li>
<li>Test setting the config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fde56b72e4cb2478239bc75c6235157ab2530584'>Commit.</a> </li>
<li>Force out-of-process for this test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0ad0084ad8f23c0e4503ad88c14bcde35171bc18'>Commit.</a> </li>
<li>Fake backends accepts config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7c705fb597789ce63e5f2a9dfd0c1143557c1d45'>Commit.</a> </li>
<li>New SetInProcessOperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9918e92f896e11b209165a571663582501cc6674'>Commit.</a> </li>
<li>More tests for in-process op. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d865b49de94d07bf4d44e6c7260a2245c9c3faaf'>Commit.</a> </li>
<li>Test both jobs and clean up properly. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=032938d63734b60050bd5d4e016d2600412a6eaa'>Commit.</a> </li>
<li>More tests for in-process config loading. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4572b894fdd082e83a0be4610bd18d9d2a7f4471'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c15c38fd9e0e7840998b6691f8c3c1cd673a64f4'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=37bbfcdebd3b102b2e98a4958472d58bd2c21e09'>Commit.</a> </li>
<li>Cleanup debugging output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4ecfb917adfe104c85e9f8e05b013fa358d16916'>Commit.</a> </li>
<li>Tests for cached backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cf1ee0fe74d2f724f42c9b4f9928f2dce9a21910'>Commit.</a> </li>
<li>Fix autotests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4adcb21a7f9a4b8a28136f8918bbdfb90a83933f'>Commit.</a> </li>
<li>Proper handling of multiple backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ead0581b6d6fb1fc63edec208902a485b917818f'>Commit.</a> </li>
<li>Switching between in-process and out-of-process works at runtime. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=709b1b29bc54035e3298432332cd89bd137eb696'>Commit.</a> </li>
<li>Handle KSCREEN_BACKEND_INPROCESS exclusively in BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=50418643f2fcf085cfe1e6e51a345febd20d350c'>Commit.</a> </li>
<li>Factor pluginloader into requesting class. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4fb1146113182ebcfa3b6163b0404436054bcfc3'>Commit.</a> </li>
<li>Refine inprocess test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=240b75fbd16334fbf67a2e356aaa6535d9558c7b'>Commit.</a> </li>
<li>Move loadBackend logic into BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=524de32a5a957b84a29b9fd03fd4f935e492e9d9'>Commit.</a> </li>
<li>Clean unused var. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a9a96b1f5fa2ca90a35d4ea9740cb1d6663c651'>Commit.</a> </li>
<li>Clean up dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0ab34d9b922a8c28a55ff6115685d891f64495d7'>Commit.</a> </li>
<li>Clean up when the operation is done, we basically only want to leave a ConfigPtr alive after this. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=285c1e387c3a78fa2cd502687baf892fce92f4d1'>Commit.</a> </li>
<li>Noise--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=26832e305a621ddbb83acb3ffb8bc314752db47d'>Commit.</a> </li>
<li>Test switching back to in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1da9b51e2d68bc318b41e019e1023abb8bfd7eeb'>Commit.</a> </li>
<li>Test switching to out-of-process operation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b369d47878e58ed94082ae593a0d391d3684d9ab'>Commit.</a> </li>
<li>Be less chatty. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7201589bc5977439ee8bd533e609b07244cb9908'>Commit.</a> </li>
<li>Make sure to return after setting config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e3bbffda8c5aefdb15910943bec93bbf766b4e61'>Commit.</a> </li>
<li>Hackish way to set config in process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fd5d4a79b976593cbab5d8800b03a31d35cc5170'>Commit.</a> </li>
<li>Reenable ASSERT in output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d22e981c26aae6a867b1278d66ce1043c0d31455'>Commit.</a> </li>
<li>Clean header files. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ce2dcbb2805716304ba8c6c810794b9e193f84b8'>Commit.</a> </li>
<li>Remove wrong test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a4dc83f59b0e73708d2a498473e5324445a414a4'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=985c4e03821983b7ad3180d19b1834fc5c31c3bd'>Commit.</a> </li>
<li>Fix BackendManager::config() for in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5b07026c67c1ad5cc4152373a16df5162d49338f'>Commit.</a> </li>
<li>Configmonitor test supports in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f5367747785176a07f3d23156bac59f3b391fb4b'>Commit.</a> </li>
<li>Don't register on dbus when we're running in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d8735ea50270156026e4f42561b9a01f063d408a'>Commit.</a> </li>
<li>Wayland test uses in-process backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=99c1723f67efecd226f8ba0a31b6a8e667e5fb17'>Commit.</a> </li>
<li>Error handling when no plugin is found. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=70fd0bb23012db9e21bee9e66fec98f62004ee7d'>Commit.</a> </li>
<li>Clean up in-progress test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f3ededdb5c2e5db4af9690497310baf3447fcf41'>Commit.</a> </li>
<li>Make sure we set d->config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3049df897e6506fd1c6c01c5b071ef4ae660b9ee'>Commit.</a> </li>
<li>Support options for both kinds of jobs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9d8c049f505b522ac508092ec7ed59a54a6ae031'>Commit.</a> </li>
<li>Consistent usage of configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e16fdd19199f724238e6dd0408aa738f6b915ae7'>Commit.</a> </li>
<li>Clear pointer before init. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c33a4ac56cbb843ef0c6e57af4790862e2058999'>Commit.</a> </li>
<li>Try using the inprocess thing for the fake backend tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a49b1101bcbf72caf64fe4b825d4e98428d8de74'>Commit.</a> </li>
<li>Fix typo in comment. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=57a227830f6b078580b4592ac52d10180b949b1f'>Commit.</a> </li>
<li>Some benchmarking code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d7d8acb073e41fbf85c0801911814adca898b7c8'>Commit.</a> </li>
<li>Disable the assert here. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c527be3bc7ad2fe3adcf0baa75a539be32d0daf5'>Commit.</a> </li>
<li>Allow to use in-process loader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1505ca2db95a052bb9bd9c7224dfc26fe845d9cf'>Commit.</a> </li>
<li>Use in-process loader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=54a566363acc995f8c34f146156b93bf724d06b0'>Commit.</a> </li>
<li>Pass TEST_DATA argument to the backend pluginloader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fd85f266f7dbd190133bb753e5ade7ed9113fb4e'>Commit.</a> </li>
<li>Override config() methods. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f1f6f3769b85d24eb5d7d010b8d3233e03df47fd'>Commit.</a> </li>
<li>Add a static getter for the preferred configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=64fd4ac48ddd6be50e8f12f04a32df3b8fd02155'>Commit.</a> </li>
<li>Reading, including edid now works. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=691ba1aa95b427dcc19cfb2f5d3fb0b48bf2d173'>Commit.</a> </li>
<li>Load backend in InProcessConfigOperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c7ff702312c81c200dbcdec097efe8e157891303'>Commit.</a> </li>
<li>Bare in-process configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ce3733dc88114c88699d4711c69b40b31c56828a'>Commit.</a> </li>
<li>Bare-bones test for in-process backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d562d38a82afad79d9576c06565f4b2e3b1b3c08'>Commit.</a> </li>
<li>Rename wayland backend to kwayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=950cd0ec97e396e8d734a3def6d40b3f338ad208'>Commit.</a> </li>
<li>Simple test for wayland config writes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=27d6829aced865c0acdfcc9eabdf92a5f7f8ccb7'>Commit.</a> </li>
<li>Simplify backend loading a bit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6f15f75593318bb4bc8920b2774bbe37bd957e70'>Commit.</a> </li>
<li>Clean headers. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e395e5ac221477ba9f39b5dd93eef76417790f77'>Commit.</a> </li>
<li>Check results of exec(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=94bca1ca08c5ffe548f0ee238d6937de455365f7'>Commit.</a> </li>
<li>Return the backend* right away. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f739277456f62436f3c2c33216dfa9b1acffb063'>Commit.</a> </li>
<li>Don't store backend* in the job. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=513efe4ee779d9f39dbb7d35e619a658100346d1'>Commit.</a> </li>
<li>Save a bit of memory. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7dc9ffd96fb2e578ef41c4f6aca184ccdbbcb376'>Commit.</a> </li>
<li>Warnings--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=61a42707bc74398d83c99c741120a89c7bdcb580'>Commit.</a> </li>
<li>Cleanup unused arg. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=03623c4aed781cc751eb213de35f1e26db80262c'>Commit.</a> </li>
<li>Also rename internals for mode -> method. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b4370ad0acc2160fc705438924edbbb0d35682c9'>Commit.</a> </li>
<li>BackendManager::mode becomes method. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=519af038a02393134b97235c1d583f007b7158fb'>Commit.</a> </li>
<li>More asserts in configmonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=aad46e2d8666019fdaa4a58e5a68ac96bb5aad07'>Commit.</a> </li>
<li>Asserts in ConfigMonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5dda81a02ae969e9e8f4871c44a9cf91008cd75a'>Commit.</a> </li>
<li>Use asserts in BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c8e311dbb32512fec5dd1e9a0b8360a70dcb43e7'>Commit.</a> </li>
<li>Debug and coding style improvements. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d487592b65c271024dc30fec9df2300cd79f9c86'>Commit.</a> </li>
<li>Remove factory methods now the operations are merged. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6393c1d763d2cd4d3a0e8bbe539214ddac22a12b'>Commit.</a> </li>
<li>Add asserts for modal code paths. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7fcf6aa99e57aea1abfd55df89264d0daaeab6c7'>Commit.</a> </li>
<li>Clean up dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9315f663010ca151ac69da20f7dd1c91fe369504'>Commit.</a> </li>
<li>GetConfigOperation becomes modal. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7579195f2e9fa33ddac321276b404d5c0bff9c56'>Commit.</a> </li>
<li>Kill setinprocessoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=266ed3a0f1b009ac1d235935716485b039f45d0e'>Commit.</a> </li>
<li>Clean up Operation loadBackend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=961f13d61b0ee2991f2eaf6a676768fc58f3299b'>Commit.</a> </li>
<li>Various small cleanups addressing review comments. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1724903ff25a741a993f9d1ec02895dcdebb22d5'>Commit.</a> </li>
<li>Use qputenv rather than setenv. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=235e85b828ab143f1fe5bc5dcbb1f14fca7dd5a9'>Commit.</a> </li>
<li>Only a single backend at a time is loaded. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=72042860e0f03228aca4bd49affa16e7f05c67d2'>Commit.</a> </li>
<li>Rename loadBackend methods to loadBackendInProcess and loadBackendPlugin. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a2c4543d69baac1e5a5f844a06d6d8ca74d6df5'>Commit.</a> </li>
<li>Bump so version. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b9510eb887390b2db0eb4e258cc56eaa9dcf2c45'>Commit.</a> </li>
<li>Cleanup dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=11063edcb0f27c26c3360baac845b56e05320112'>Commit.</a> </li>
<li>Avoid crash. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c28f6d5f066c4e0a7bd77e5bc056b7bcc3e289ba'>Commit.</a> </li>
<li>Autotest for in-process-configmonitor. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=813cadbeedab32f8fde4cdc9fae1367681e31669'>Commit.</a> </li>
<li>Make configmonitor work in-progress. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0fd46644e755972403959f9c4362ed4a9780f092'>Commit.</a> </li>
<li>Whitespace fix. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=23ab125127bd7dec19b650ac55c1ada09b8552dd'>Commit.</a> </li>
<li>Cleanups. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cb84af260ebbb4b3cbd35b293c1a1c5da2f00276'>Commit.</a> </li>
<li>Test setting the config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2f27fdbe72936dcaac600dbc4500923997c0c126'>Commit.</a> </li>
<li>Force out-of-process for this test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ed65f16739b7b6918f096740f3542aaf3bc70986'>Commit.</a> </li>
<li>Fake backends accepts config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=35e80135d61a6d2ad1b4093b8fa50ec23fe07790'>Commit.</a> </li>
<li>New SetInProcessOperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8962cc57c9b8059baae7e0b42d825db2c4cfd8ce'>Commit.</a> </li>
<li>More tests for in-process op. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2503ba1b9e3de7caf2fd1dcdd972fd61f109d6af'>Commit.</a> </li>
<li>Test both jobs and clean up properly. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=48ebdee204298feb85ae4d2e4bcb06d96c01b2ba'>Commit.</a> </li>
<li>More tests for in-process config loading. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=053cee149f8635690bba3ef446761c6f104022e5'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=52dc42bfe5cda6854f7444e0401cd6a428f1f48b'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d759f21bf700e412810e9364873167d4c422bf11'>Commit.</a> </li>
<li>Cleanup debugging output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a6debc3ea499316c0fe4e9a5ffba4d947db0b611'>Commit.</a> </li>
<li>Tests for cached backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c9ebd6fb46a7a010087c6f1e5d50792395cac7c5'>Commit.</a> </li>
<li>Fix autotests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3b13ae587c224e67ad9e9869917167197edb7557'>Commit.</a> </li>
<li>Proper handling of multiple backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7df640c2578908d2a67f999963c2ab718001f9ae'>Commit.</a> </li>
<li>Switching between in-process and out-of-process works at runtime. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0fdcc602a63ce125130b6965274983dff9922e80'>Commit.</a> </li>
<li>Handle KSCREEN_BACKEND_INPROCESS exclusively in BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=11d9641280325a7fbcb21f4f6fd957a3cab19059'>Commit.</a> </li>
<li>Factor pluginloader into requesting class. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=565ac149526e27018503d05c9fbd02dff6e97cbd'>Commit.</a> </li>
<li>Refine inprocess test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5e58be4a735fad765edda9895836fe88940dbb0a'>Commit.</a> </li>
<li>Move loadBackend logic into BackendManager. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=16c4701357280787b6bbf9a43fea44bd657efb2b'>Commit.</a> </li>
<li>Clean unused var. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=da4e95c691b2fd985345da739d41fc914ba40323'>Commit.</a> </li>
<li>Clean up dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0d9cf5dca19db682dd043c4558ae9a264dc600e6'>Commit.</a> </li>
<li>Clean up when the operation is done, we basically only want to leave a ConfigPtr alive after this. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e37ed212c2454cd20ebd46877127b3032e313463'>Commit.</a> </li>
<li>Noise--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=27eb13dad49633020c24c01d3048fa370273d684'>Commit.</a> </li>
<li>Test switching back to in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bd9a67eb529ec45528f882874d85f04573366a69'>Commit.</a> </li>
<li>Test switching to out-of-process operation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9b5e11d2bb90e497a9765e08565a7abfe4eb8264'>Commit.</a> </li>
<li>Be less chatty. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1b1a4145ad75bf97b1eeb73718950168e6c03255'>Commit.</a> </li>
<li>Make sure to return after setting config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0160d5480910a2c6d01fe799e98bae932bbb9942'>Commit.</a> </li>
<li>Hackish way to set config in process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a759fbec2f3fb822fa55cc7e67541ce32686344b'>Commit.</a> </li>
<li>Reenable ASSERT in output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=de952da2d7147899cf3e888b51b1c4f1bff8c76c'>Commit.</a> </li>
<li>Clean header files. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0f80a3cf98682ef92b1771dc8a5a7e166c431c0c'>Commit.</a> </li>
<li>Remove wrong test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b24d69de6100f2ae0be2f310a8c6374d35eb6edc'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1cb4ae2979984d676457fb1c1c154dc1ada12451'>Commit.</a> </li>
<li>Fix BackendManager::config() for in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=136f79face2ff178aff567d16547284ed1c96699'>Commit.</a> </li>
<li>Configmonitor test supports in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2156eed27ff5f9ff252e9451ed19c9141b0da409'>Commit.</a> </li>
<li>Don't register on dbus when we're running in-process. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b023ee74d9035f7cbbd6c907c169d886f1b68f97'>Commit.</a> </li>
<li>Wayland test uses in-process backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1284ce410bf77870420e1ccf5cd5e88e809ce7b7'>Commit.</a> </li>
<li>Error handling when no plugin is found. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e9c7a725d0e8e00ef110756b1fc93223dba6661b'>Commit.</a> </li>
<li>Clean up in-progress test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d73ff5fc73c27fc9de13bb4250de80f44eafc42b'>Commit.</a> </li>
<li>Make sure we set d->config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a485ac6ded62265c4c7dc4446b54c425e28d0e2a'>Commit.</a> </li>
<li>Support options for both kinds of jobs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=baca5a2e278e5a495d427a2978831aead6428de5'>Commit.</a> </li>
<li>Consistent usage of configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8fc82763d53e84659ce38b65a4f6c182bfbbc194'>Commit.</a> </li>
<li>Clear pointer before init. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=78851893da741550f3f97139a0ac0f75ded9166e'>Commit.</a> </li>
<li>Try using the inprocess thing for the fake backend tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=919ce9c52b05f8d89c824a940169273981759783'>Commit.</a> </li>
<li>Fix typo in comment. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d2ed22062018224000b595696c6a03dcc71fddbd'>Commit.</a> </li>
<li>Some benchmarking code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a2b7cae966d293506548d5869d22745a69341a00'>Commit.</a> </li>
<li>Disable the assert here. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1c41154735405af70ea0459e148328e807741ff7'>Commit.</a> </li>
<li>Allow to use in-process loader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0cc019ad40d0b7663a53dc8fac06b11d5e496331'>Commit.</a> </li>
<li>Use in-process loader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9bb836ad02c44202789ac0dde4ee6b74b43e1df5'>Commit.</a> </li>
<li>Pass TEST_DATA argument to the backend pluginloader. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f7b631a4149f054b83d072ea4af96c03d6d64326'>Commit.</a> </li>
<li>Override config() methods. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b34c1aaf1cdd54b1e33a738e0a178918e19f53f1'>Commit.</a> </li>
<li>Add a static getter for the preferred configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=96c94121d9c0a69a0cdc91458c7252ad57dd06c7'>Commit.</a> </li>
<li>Reading, including edid now works. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=daeb8ef3f4e6be1efbff9cb63637f856c8f40cd9'>Commit.</a> </li>
<li>Load backend in InProcessConfigOperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=669f5e02f5a3de6f9c2ee2e466ae32f502084899'>Commit.</a> </li>
<li>Bare in-process configoperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d560d24a0b87253a6fab84dcaa5d7ed8df4a2ac7'>Commit.</a> </li>
<li>Bare-bones test for in-process backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ec0d7b2b51ce0d79ff1325cff2b4797fa52d1fc1'>Commit.</a> </li>
<li>Fix build against newest kwayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9dbb39f2a8879b0cedcff625530c912356276faa'>Commit.</a> </li>
<li>Fix up ID handling and enlisting the outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8460021cca6b8b1c49bd42d234026f4152731584'>Commit.</a> </li>
<li>Make id kind of work. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=45156cec52195bad0c545694826647a660952033'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ed2b08f802b0fc843f9094cd5239dd0e1a9a7771'>Commit.</a> </li>
<li>Experiment with output ids. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=966b87742f725b46cca5c4b0993c040818792255'>Commit.</a> </li>
<li>Read mode id from config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a64401ec3deca0138d78a1315eef9e53c51a9b26'>Commit.</a> </li>
<li>Build against latest KWayland[sebas/kwin] branch. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=196d8b008bb4cadb08eacc25f09c3fad474eff76'>Commit.</a> </li>
<li>Update README to reflect the latest changes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c2ec0162adf04581c7ee58ff2914265f1225b77d'>Commit.</a> </li>
<li>Clean up README. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=77e37537e520f66e6526f131277bb88994774710'>Commit.</a> </li>
<li>Update my notes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4fa7915d291f6979d29276c8d1d03ac14ebf2667'>Commit.</a> </li>
<li>Update readme. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=77bfa42d04b87c362699f7eaaba6d66941079335'>Commit.</a> </li>
<li>Update todo. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=3103ff0d9dbbe38e71b531949b7891df4243305e'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f04d15546b86a399d47153807fd79701d6bab9a2'>Commit.</a> </li>
<li>Update readme/todo. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b7bb5bb693e4126e3ef6d663988aada61ddd0cf9'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8919d7fed47840e8cfabd758f82f1b608129b502'>Commit.</a> </li>
<li>Test id, rotation and enabled. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ddd615437d6587125ca41b51eb9e48aa0c260f78'>Commit.</a> </li>
<li>Use id of OutputDevice for all internal accounting. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ff94189969ee11f58b84c1a62ca5ec56cb65a6e4'>Commit.</a> </li>
<li>Mappings for rotation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4a3c297acb37e3aac55ff57c4e54a6bb01975d69'>Commit.</a> </li>
<li>Convert between kHz and Hz when reading config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=605fc1246670ac7225544d9c32931bb064e0995a'>Commit.</a> </li>
<li>Use OutputDevice::done signal instead of own logic. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8c30ff17943a2c1b89f014686552b9ca2195d407'>Commit.</a> </li>
<li>Time out for wayland config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5d96ff9638e239bc1ce1239ed173498646ce7c39'>Commit.</a> </li>
<li>Disabled outputs may not have modes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=75c863d36d8193a8c564465a58af2013d61313de'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0c2f762c4f64bdbbb8aff2b541b7ef6c7a40a193'>Commit.</a> </li>
<li>Make basic tests pass. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=efff803550c4ee7d57df7c989228fcba3305623f'>Commit.</a> </li>
<li>Port Config to OutputManagement API. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c242f070b9ba6c797b126f15784cd83b7f66dfb2'>Commit.</a> </li>
<li>Adapt autotest to outputmanagement API. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fa086c1495d06707f9a7226e35ac9297bbbc4522'>Commit.</a> </li>
<li>Port autotest to outputmanagement. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c4a3e6c7bc338d04cecac78d95774c3cda0a2bd9'>Commit.</a> </li>
<li>Port wayland backend to outputdevice. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8fd2616854cd03b1132e407b83cd135952973f78'>Commit.</a> </li>
<li>Port to OutputDevice. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2a0a2b0f4353e7ceebcff2f7468f6db04aba6bfe'>Commit.</a> </li>
<li>Port to OutputDevice. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ca842dd5a89b3811cfaa8464de69ab19581a9727'>Commit.</a> </li>
<li>Update README. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=269e8d42bec98e9469ff02e3c2f7b3e837c5ea69'>Commit.</a> </li>
<li>New and improved plan for the protocol. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bd6c278edd6d26dfb66b2ce0a981d55123b1cbe9'>Commit.</a> </li>
<li>Correctly bind output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=03d9144b5d25242272b4daa7124c78496df19fab'>Commit.</a> </li>
<li>WaylandOutput is now a QObject. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=922672eb6c65ffa2e112ab872b868518bea2081e'>Commit.</a> </li>
<li>Refactor WaylandOutput - volume 0. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cf16c921e1fcb8c730c7b30f39b551eccae841a2'>Commit.</a> </li>
<li>Create disabled outputs from config as well. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d24af09550fae25b6a3f406d9ddfaaf0850993f7'>Commit.</a> </li>
<li>Compare against total outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4e68b9606308eaa89b1aaf9a8ccd4b5440caa134'>Commit.</a> </li>
<li>Add disabled outputs to our interface. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1ad34ec3470a974c0be5a33f986681492758cd65'>Commit.</a> </li>
<li>Update todo. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2385d051a260d0686f65f4922b61c3f911406ee5'>Commit.</a> </li>
<li>Add notes from talking to Martin. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=38a4f5c5abaab4921ccb4786d5058c6b751fde28'>Commit.</a> </li>
<li>Update readme. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=42ed947e55581b18461d980af880ca81e45299ed'>Commit.</a> </li>
<li>Fix configserializer test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=75b5ed9a1640c4814982c642801ed51f93c1c948'>Commit.</a> </li>
<li>Improve comments. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=59c09e646b975d2f61559106305935bfbef82c5c'>Commit.</a> </li>
<li>Only create outputs, no shell, seat, etc. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d1c950dffc7099b5786ba221fbb18efbdeb823fb'>Commit.</a> </li>
<li>Test skeletons for output removal. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6e8e9daa50c25d7e1726fffff65dc7a2f682d591'>Commit.</a> </li>
<li>Always create Edid, fixes crash. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=243fc527559983324a603d54be2eafda36ce4a02'>Commit.</a> </li>
<li>Shht. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=07db18159833d59e96230e2da4e3f23226398691'>Commit.</a> </li>
<li>Wayland backend reacts to output removal. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4e781d84a17bef4443d24947b2727069da910679'>Commit.</a> </li>
<li>Test server improvements. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0c16711256087a3d66a80ee907ce7ed1981fc497'>Commit.</a> </li>
<li>Comments++. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7275e757e91209e2c18e69e63d03d06590acd2bb'>Commit.</a> </li>
<li>No Output- leading group names. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=decd9e9832e76a0955f384dc9b989feade0974de'>Commit.</a> </li>
<li>Avoid abusing using;. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fa5adc073b0fe24c805a9f16b176325bac80a6f8'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=aeb47fdf625e399980fb37231c401ab8623c5c54'>Commit.</a> </li>
<li>Adjust autotests to new group naming. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=35d1d517318c006f1e428596444def6e98cf6b33'>Commit.</a> </li>
<li>Adjust config group naming to kwin's gusto. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d0d1510737de6702636783ecb7b4b5ab15a2f8ea'>Commit.</a> </li>
<li>Testserver reads config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dfa7fcb48f8a0dbe3b9da178a98183a262e1a241'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1a75ba22e8ad92345addcf30e3b11af77d5bf005'>Commit.</a> </li>
<li>Use static string to avoid typos. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f652d057a5e2d238cb564fab3ac4a9880d9fc7e0'>Commit.</a> </li>
<li>Add manufacturer and model fields to outputs in default.json. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=591b6b405af738604ac162e59a6994847267ad97'>Commit.</a> </li>
<li>Make xrandr backends optional in cmake. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ec6d6524ad138fd2dcccd10b3db25810984f40dc'>Commit.</a> </li>
<li>Cmake updates. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0e0c3bb19929bdd6ddcd900197d0af5aacb85374'>Commit.</a> </li>
<li>Test writing, reading back and changing the config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=15df48983747684fa2eee99fc9e3e61617fc22c1'>Commit.</a> </li>
<li>Fixes refresh rate in mode setting. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2899813499f1dd7d4b406c8dd2eea9e7e5d7858b'>Commit.</a> </li>
<li>Cleanup dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1b4123905189169a773bfc9df905dff4d4ca93cc'>Commit.</a> </li>
<li>Require KConfig for all wayland tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=729d534fb7b867d8d7258022a3a1fe3aa9134e42'>Commit.</a> </li>
<li>Allow building Wayland backend without KConfig. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=af08b48789008fa3a7d0b7b42cc09ddf8d468626'>Commit.</a> </li>
<li>Only build testwlwrite with KConfig. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f4bfd927b6e011521c2f8c73c381ef79d79161e4'>Commit.</a> </li>
<li>Adjust tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8793d22d4ded611836a6aad74c0d847946f661dc'>Commit.</a> </li>
<li>Method to write kconfig-style output config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0eae09c04250f9df6da897da1f7a4650adea494c'>Commit.</a> </li>
<li>KConfig-based settings write. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9949bd4c9f6132abf3226f25733533e0a66fe50b'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a325868950ab8a43b5d183d1e07cd42f67b7ea80'>Commit.</a> </li>
<li>KConfig as optional dependency. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5d52df5b0706218664da003cc8aa04f024a10bb5'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=13683f03fc342b56b9ab01134c17bf7aac121ea0'>Commit.</a> </li>
<li>Wayland backend only loaded when explicitely requested. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=82eac8d3bb9749067a5e43df43c0e68aa3825ecb'>Commit.</a> </li>
<li>Use ConfigSerializer to write out the minimal configuration. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e453f0542ccb6c3a9ec29b467927998f6e122806'>Commit.</a> </li>
<li>Clean up. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b1167c4fabfa733ef6ef5f6d747026f47bae6f4b'>Commit.</a> </li>
<li>Add tests for minimal config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=39cf5657533abe546b89aecfa30c1a6406615466'>Commit.</a> </li>
<li>Configserializer allows outputting minimal configs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0594616b35063db369d8e7a74b76819fe78c8c5e'>Commit.</a> </li>
<li>Mark both lines in debug output for easier grepping. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=96b965f6771a54403f2f465f3bb359e35fc040ff'>Commit.</a> </li>
<li>Start server -- test passes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6b2022626c26c9821361b129ac3f0776306982ea'>Commit.</a> </li>
<li>Namespace not needed. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2927917d839391b0b59b8eeb5101948023f3e2a0'>Commit.</a> </li>
<li>Noise--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=35f74a12520d8cd30e9e0f7fc2358e08d4aea0f4'>Commit.</a> </li>
<li>Testserver defaults to configs/default.json. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b7be1575e1943e63aef68d0c77d610b9aec6f167'>Commit.</a> </li>
<li>Add another small test for writing the config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ef8e149c8940da836221c9ba9136349df8c2df42'>Commit.</a> </li>
<li>Clean up. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0348660e626224c41ae802740a543fc305cf96a7'>Commit.</a> </li>
<li>Clean up. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a04142ededd074ee8e30c523708ddb1b31e56d8a'>Commit.</a> </li>
<li>Dunno why this file ended up here, but it doesn't belong here. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=08ce49328755f3064f5f94ea75b796a1c05e86e8'>Commit.</a> </li>
<li>Use shared wayland server in test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c1aa294211bce57f6e8e4c36f0ac141b10b93fe6'>Commit.</a> </li>
<li>[wayland] do not create outputs from disabled or disco'ed configs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9d32037c95f5d596c1d5ea3079c8afb396f6866b'>Commit.</a> </li>
<li>Improve wayland test server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6ba91efaac8735dbe134bc19ce42236a3b318459'>Commit.</a> </li>
<li>Improve tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c9d972cdc935db9d87369b836a73877a64e6e37b'>Commit.</a> </li>
<li>Use a sensible size in test data. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=49e9e37ae26584f94c120678474b5f0211d9f98f'>Commit.</a> </li>
<li>Fix sizeMm for invalid edids from config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c4c73490e3b3ffefe81f6d4261e1a38666d278b3'>Commit.</a> </li>
<li>This does hang after all, needs sorting out later. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9b4e6c57a5b92880461ecd18fd275902bf1231f6'>Commit.</a> </li>
<li>Delete the outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b08686b5f1e8a1b2b92f47e1e2d789c68839124f'>Commit.</a> </li>
<li>Neaten cmake. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b78d6476540ba1941e6ba929923fcbac7fad0567'>Commit.</a> </li>
<li>All configs have valid physical sizes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e49ee7937d5919a5759455578dbb46a07fa1a9f1'>Commit.</a> </li>
<li>Testwaylandbackend uses wayland setup from default.json. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=326db00e9f9f0612a5f98f35793bb84b6f318f1a'>Commit.</a> </li>
<li>Shush debug in primaryOutputChanged(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=836705605ebbfb15a1d57f3f349117748d6d1fff'>Commit.</a> </li>
<li>Improve wlsetup test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=49dfd5e3441354de1314d5961e9e338f63bf3137'>Commit.</a> </li>
<li>Spelling and grammar in comment. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ac9f5ddf1cbad8392bce6ecc7cc83c498427e503'>Commit.</a> </li>
<li>Update TODO. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=368d914a581f8dbe8ebd43e8dea836971c495fb0'>Commit.</a> </li>
<li>[wayland] Add and hookup configwriter. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a542a20b8347e5f03bfed8474e6ff43ed9e1bd41'>Commit.</a> </li>
<li>[wayland] test configwriter. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=97ddcf29b7418c3ebd5f68c56e6bf4a044afc07d'>Commit.</a> </li>
<li>Factor out config reading. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9f0da313b54daf9d04a5a3b90d6ee219dc50fe02'>Commit.</a> </li>
<li>[wayland] sensible screen's min and max sizes. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ff086470c1c598982d780d196c88bebdccd7d1ff'>Commit.</a> </li>
<li>Make config reading functions static. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1a4d9dc6caec35b2a2b3f3dfe38260547f6ed079'>Commit.</a> </li>
<li>Build new test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=05060f7ac3c924d4dfe5de4597aa6cfa1c33b614'>Commit.</a> </li>
<li>Omit checkout outputs, not relevant here. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c30b680b4fea763c11b41fa056091cfac5e1761a'>Commit.</a> </li>
<li>Add new test for writing the config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2c7678e2c8df34facb2b1c38b89b2640e215bca1'>Commit.</a> </li>
<li>Start and stop of wayland server don't qualify as tests here. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cd5cacb399bd26f8393aeb84a48ae7e7ce1ee7a4'>Commit.</a> </li>
<li>Block signals until registry is initialized. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=962015a1c0e43eefed07d5e45e6bca3aba3c9ba0'>Commit.</a> </li>
<li>Noise--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1f6e07a074cdd7ae00354de45cf6b5d6e42f7ff0'>Commit.</a> </li>
<li>Shut down wayland server on exit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ed961ca05ad39a48cbec2288b5555d9ed54d23cc'>Commit.</a> </li>
<li>Update wayland backend's todos. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=80d94f914d0d536a1b711f39fd8827a63809249a'>Commit.</a> </li>
<li>Show the backend's stderr as debug messages. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=eee4d6ae4046aa7208616237900577cde9305bbe'>Commit.</a> </li>
<li>Fix path. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b6a19acb1b6eae170e1fb4832aa3f392f71d60b5'>Commit.</a> </li>
<li>[wayland] Fix build of testserver. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=10807fe41f595f2ef4ca67572e07730bddfc4525'>Commit.</a> </li>
<li>[wayland] port autotest to new libkscreen API. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7426f3db34a3cdbf08dfd2f91e310cc47eb05ed5'>Commit.</a> </li>
<li>Wayland backend builds again. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d30062bef8775d702d4a196bba8bb675d682044a'>Commit.</a> </li>
<li>[wayland] Convert Screen* to ScreenPtr. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fac62cf560d28e00d3cde9bf3af3358ac08eebf2'>Commit.</a> </li>
<li>Start fixing the build against the new KScreen API. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=964164ccc5c4b0cc0902949d5fa27f15a03a4290'>Commit.</a> </li>
<li>Clean, this is not needed. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7927dea10f9435cc39addfa9e7920c8ec0bc7d6e'>Commit.</a> </li>
<li>More complete output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5f667fff0d2cd2b92a632458f864330d8967b6f8'>Commit.</a> </li>
<li>Small wayland test server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a8c065e77f0217fa6564bd9eace791b0d1a93614'>Commit.</a> </li>
<li>Allow disabling the internal server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=239e974317d383f99f484c46e8b9475537b5ec2e'>Commit.</a> </li>
<li>Conditionally link tests against wayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=62737794abc08a0f588943987101cb8e19e9ff08'>Commit.</a> </li>
<li>Properly test Wayland server going away. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=82ad6c56ada2b410821db1cb913aeb88bce6b8e2'>Commit.</a> </li>
<li>Handle disconnect. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8511c266a24041121cc78def57c4bdc71a41abe2'>Commit.</a> </li>
<li>Make sure the thread gets to end properly. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4f2978784f4ccab737fead51a8a5e85710099d23'>Commit.</a> </li>
<li>Header cleanup. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8ae27b0845ff1f0d276a45befcccf33e37b35677'>Commit.</a> </li>
<li>Header cleanups. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4c07d072c5346a002de5cf0c044ce6b5f9d64a64'>Commit.</a> </li>
<li>A bit of documentation for WaylandConfig. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=949df4c593f4fb2d67f088b29b31f5b9ee196eeb'>Commit.</a> </li>
<li>Initialize two outputs in test instead of one. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=236231fc4d5f27c158bd15893a8d33cc6a38a6a2'>Commit.</a> </li>
<li>Silence++. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ae00993fdfc600702e9260ca5f2393f33c1e11e3'>Commit.</a> </li>
<li>Update the screen's output list when new outputs appear. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b583190b99a6ffacb4d6483c01c93225bada06ad'>Commit.</a> </li>
<li>Fix screen, mostly. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=517181578380337c82ce04d598f964ae7f6f79cf'>Commit.</a> </li>
<li>Add basic APIDOX for Screen properties. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c42e66f5290e85e50d1ddae6eb7a6fcef1d0f3ec'>Commit.</a> </li>
<li>Improve test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=29dd2e1e20b1e2262bbdbd1da916506d64cfa4b2'>Commit.</a> </li>
<li>Set config to invalid if the connection is gone. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8636a3504c834c4252660f695fa8c2e510d4f366'>Commit.</a> </li>
<li>Set logging category in wayland test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5c23268f98e8152db75c46cb581512cfee86a256'>Commit.</a> </li>
<li>Improve test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4cdac4b7a73a3c540e02208772a9a6a0a3e00c10'>Commit.</a> </li>
<li>Do not quit the syncLoop prematurely. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f1142c51eb113f64c2327ec6132fc9d4fcd7e3c2'>Commit.</a> </li>
<li>Create and start our own Wayland Server. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2c81f85ac17a703005c848f34785a5042e22d26c'>Commit.</a> </li>
<li>Quit the event loop after initial output announcements. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=770976d94470be1fc14499518960ac884a8824a6'>Commit.</a> </li>
<li>This message is displayed after the backend has been loaded. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9f98a69a16d85525b792e169d4c30c6cb6281f48'>Commit.</a> </li>
<li>Run our own waylandserver for these tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=99e4cf19db1ebaeae1faab02b0cc88e9ebaaf732'>Commit.</a> </li>
<li>Registry::interfacesAnnounced is this signal's new name. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bc6f403841dca94cd6fd8979d9818c210253fa27'>Commit.</a> </li>
<li>Quit the event loop when the connection failed. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b87c20c525ee1859d19203314669da05977ba01e'>Commit.</a> </li>
<li>Use sync method in test, now that WaylandConfig supports that. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=f82d10fd1adf3ed6a6d56420f6aa29932576590b'>Commit.</a> </li>
<li>Block until all objects are initialized. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9dffb3305981f4af1050eb489f31d681683fba76'>Commit.</a> </li>
<li>Show debug only when we're really initializing our static config object. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=63702cbab824261acd7703034eb01dd0e049abed'>Commit.</a> </li>
<li>Make test work. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=853a443ec116e170ac5a3a7cf91c5be419517f4e'>Commit.</a> </li>
<li>Correctly handle output map. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=07d9d3ccc1a7f3c1a00e2fe85895c71ed08d024a'>Commit.</a> </li>
<li>Exec qApp, so signals work. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b05dcb22c4703fe6adf5a8520c3555f37ed460eb'>Commit.</a> </li>
<li>Clean up unneeded members of WaylandConfig. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=62870eeaad9736bd2c8f68a69aa3e2d1718789a5'>Commit.</a> </li>
<li>Async operation for the testwaylandbackend test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=6d206b89611f4c0ab9a90f73e08d239c566c694c'>Commit.</a> </li>
<li>Clean out debugging output and commented code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7b651c4dfeac00c1c5f02931e926744476a05d7f'>Commit.</a> </li>
<li>If there's only one output, we know it's the primary. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5993646e9c5a6d7fa78ea6229180eee55ef4c652'>Commit.</a> </li>
<li>Implement modes parsing for Wayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5a2da24f14b7d590ca0507585d12bd403a118ecb'>Commit.</a> </li>
<li>WaylandOutput is-a KWayland::Client::Output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=36d06f689f360b9860d78876b109c00d06360323'>Commit.</a> </li>
<li>Listing outputs works now, also cleanup. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=40f4a2f0d67b2f9856bc63ce81a22dccfe585768'>Commit.</a> </li>
<li>Initialize output from KWayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7552a7c72839830dc7dff36a1725c6ff9a6919c3'>Commit.</a> </li>
<li>Clean up. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7cca5db1dd05faa26f77ebf723818c2fa5955a84'>Commit.</a> </li>
<li>Hook up signals and slots in WaylandConfig with KWayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7f12e0e8ee3278bb8040d330b9c70e3a63c934ef'>Commit.</a> </li>
<li>Set up connection and registry through KWayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0aa1abfc320153bdc394c874c18a224c851f70a6'>Commit.</a> </li>
<li>Add KWayland to cmake optional dependencies. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ceb42e26fd2eec6ec580e5598f5cd8e4eb3ce9b6'>Commit.</a> </li>
<li>Merge frameworks branch after qscreen backend merge. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d141335cfa1e7f7f22fdb072dd9bde5ea53c3676'>Commit.</a> </li>
<li>Test Wayland backend's outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c23d6982a78cd66ee985dd0f8b13cd2ddb5e4c0a'>Commit.</a> </li>
<li>Details for Output and Mode. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=461400887e446cf17e3298a333cd2d68bc26ea4f'>Commit.</a> </li>
<li>Add some debug output to verify that the callbacks arrive. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=869a971579ecdb41309351df2fba74edce1d8a1f'>Commit.</a> </li>
<li>Don't flush events in readEvents. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=03b6a0de29d683f1d66b1ca02e245848cb3683bf'>Commit.</a> </li>
<li>Kill duplicate file. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e1243c597ee54a23f8a896600d4533d60f383c8d'>Commit.</a> </li>
<li>Merge printconfig and testpnp. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0eeb51a41b02810f04786696b9220d2947eb1d34'>Commit.</a> </li>
<li>Astyle for qscreen backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=10f4db73450b35133d0c01e0b4e37e7ee6967981'>Commit.</a> </li>
<li>Implement rotation in QScreen backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=323028503a9aebb14a808e282f6592303c1aeb60'>Commit.</a> </li>
<li>Cleanups in QScreen backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=05461065009621900a2c77f30fa2d6a5954b8d38'>Commit.</a> </li>
<li>Fix encoding in tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=923a444447caea7fd6cf8e4c59b1e3a160baf19e'>Commit.</a> </li>
<li>Hook up most of the Output's properties. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c579603aeb84dd0631cbcf0788ba57304789ba11'>Commit.</a> </li>
<li>Document Wayland backend's design. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=874de97cc5014459ce5c45e9ec8f4f1cd86a986c'>Commit.</a> </li>
<li>Clean up a bit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dccd2397752743938fd03833b3d0b4baefca23ec'>Commit.</a> </li>
<li>Add readme file with todo items. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8d68501987a4eb55e12fb10ae9a76e01d8027cfb'>Commit.</a> </li>
<li>Fix encoding. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=298b9e38e1c3308d179bdd606b545597be8e6f09'>Commit.</a> </li>
<li>More implementation, in detail:. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c43f59da76c58a86a08c5cb85cf936ab253a8735'>Commit.</a> </li>
<li>Load xrandr backend by default. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a8b1a131bb24c84dafc02a10e02ff4914bddf91'>Commit.</a> </li>
<li>We need to force a flush, otherwise the bind won't get to our callback. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=491184007e54487a9849812abd473ef8a8972640'>Commit.</a> </li>
<li>Connect to Wayland. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b861721694a4e057f95a50247f463ecc62d41afa'>Commit.</a> </li>
<li>Fix encoding. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=931968ae646456048ac51f4587ff85c1123ed495'>Commit.</a> </li>
<li>Autotest for wayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=31d67a0ba61e43a30e92c5986be195e7c1e240fa'>Commit.</a> </li>
<li>Start of a native Wayland backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=55ca25c1b3a571c5410aab9e6ed81f7468ec9bef'>Commit.</a> </li>
<li>Kill some dead code. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=330aaa5f1440e94ecdcdd72742840ae1135af407'>Commit.</a> </li>
<li>Small test app to test plug and play, especially for the QScreen backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ce6b19158298afa30ca073600454a0d1a2e2e2b9'>Commit.</a> </li>
<li>Test for double ids. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e51dc041df3ae91ca48d8410776e283746b15b80'>Commit.</a> </li>
<li>Make backend dynamic. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=31b3f31a3a99f45f5849920c1b6f2399a27e3126'>Commit.</a> </li>
<li>Add api docs to most methods. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=e2683e72cf552013b134e55706c75bae1f0b8c64'>Commit.</a> </li>
<li>Wire up updating of screen config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b4912d2b692a7b167a7ac1f97935a07ece00ef68'>Commit.</a> </li>
<li>Don't assume all outputs are at 0,0. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=caf8f33f6446f4caff320ed8c546b525636637fb'>Commit.</a> </li>
<li>Build++. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cfd8f93039dadb8b326d29e65339f07a3a74cc71'>Commit.</a> </li>
<li>Test Screen. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=37724e8542bfdc88d4c739d9b3e30f2050445796'>Commit.</a> </li>
<li>Cleanups and corrections after refactoring. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7a40d744719df6513b72d2e51eb7504e3402c3ff'>Commit.</a> </li>
<li>Test a common usage pattern. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0d1360dfe8b548e4f77581cb1f346d60b4e68bad'>Commit.</a> </li>
<li>Second portion of refactoring the memory management. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c0cf0b776f04a7f9d04fa60557e10887ca461156'>Commit.</a> </li>
<li>Create a new Config everytime config() is called. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=7597dc37bb28135990dcaf1a2402e071a36a370d'>Commit.</a> </li>
<li>Document memory-management expectations of backends. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=1c8981df51dc8e3c427014f9a4ef9d511a710216'>Commit.</a> </li>
<li>Declare logging category only once. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=53a79d5e6e0191d09a9ec5ce606855dac0b23f2c'>Commit.</a> </li>
<li>Correct screen position, fix debug area. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4fb978c7f70b13c9704c71cbce3899584cfb142e'>Commit.</a> </li>
<li>Factor out Screen. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8eacd4e3ef0b9895338176a59c3aec9947323d48'>Commit.</a> </li>
<li>Clean. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b6a122ee0a08f53ebc50db62a5dee8550ce09baf'>Commit.</a> </li>
<li>Astyle. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8af92374a35fd5148da83787bc9bb3ed5d499d99'>Commit.</a> </li>
<li>Ws--. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=8fff7f8b8093bb32f0722c055fa629ae6405ca39'>Commit.</a> </li>
<li>Test Edid isn't 0. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=89f61ae3b9e738be704b705dee9212e15502b385'>Commit.</a> </li>
<li>Handle Edid. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5c3653b7e1851838e55c5c9b4ebc71cae523ba28'>Commit.</a> </li>
<li>Improve autotest. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=261521a1e852b8cbed739e7903ddcc2500c98225'>Commit.</a> </li>
<li>Compute physical size from DPI and resolution. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=9e77bcc381038b7550330d340de23c373d3b9ed0'>Commit.</a> </li>
<li>Clean up dead code in test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=994470410dd90f26d4f6a011a2f7c2449cb7f721'>Commit.</a> </li>
<li>Test rotation and position. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=15be8145de10a99dc302083fcd26026d95ad870f'>Commit.</a> </li>
<li>Small cleanup. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=dceb1ec7e858c895f9a4114e75f65a4a02dc2a3b'>Commit.</a> </li>
<li>Rename library to camelcase filename. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=0c22be38a3407ee050ad1515511cb65c58d12f2c'>Commit.</a> </li>
<li>Automatically fall back to QScreen on non-X11. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=ff064da62172aad6314f42ec101c0e566b998180'>Commit.</a> </li>
<li>Factor out QScreenbackend's Output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=59ba1c54281e60bb125826de5bdc6bef13a270ec'>Commit.</a> </li>
<li>Factor out Config*. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=4fe9b02ed89dd4ed2896b56c62700e943cef1d6b'>Commit.</a> </li>
<li>Test all outputs. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=fb6549b011c5e2159d642b96ac19e454a5a0ca97'>Commit.</a> </li>
<li>Test all outputs in qscreen backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=915d2464130c59eb49da6a569c4bca466557b7a8'>Commit.</a> </li>
<li>SetConnected for primary output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=93a700470ee781bfa95a991112b91c76d6064868'>Commit.</a> </li>
<li>More autotests for QScreen backend: modes and output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2b6aeecd5ba9359bef0f18f6ef8375b6ae09763d'>Commit.</a> </li>
<li>More tests. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=62f8590d7e6f89f8838acffb601020586a996af4'>Commit.</a> </li>
<li>Property mappings. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b63198c6636a6463f36cb9e1e091434d20c187ce'>Commit.</a> </li>
<li>Basics for creating a config out of QScreens. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2acd2b6a8ded7e7cd9babe0333fe6d7996d2e249'>Commit.</a> </li>
<li>Add simple test for QScreenBackend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=461277f7f42d3e5185933d40f35cc2e7ece65021'>Commit.</a> </li>
<li>Rename QScreen to QScreenBackend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c4a0f2525b5ddd5b50d9953a2b727bcbeb26efcb'>Commit.</a> </li>
<li>Lie about validity for now. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=58e02c4413398b7d8682451a3d36c89bc501be4a'>Commit.</a> </li>
<li>QScreen backend skeleton. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=2a65e3149ce2e523627faeb8520ec9cfe8ecb76b'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='http://quickgit.kde.org/?p=libksysguard.git'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Signalplotter: Use std namespaced isnan and isinf. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=b0578798eb3f5bdad0a25ea9b4a52c53ebcfc020'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=45e660d4b5bd5a5566f4ff4376c60e0059924b87'>Commit.</a> </li>
<li>[processui] Safety checks for platform != x11. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=b57c7ca546098d372cf8a88a8a7c10a32d9ca331'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=b5a7796675e2b9b5f08bf9eee83c310bcf1d91ef'>Commit.</a> </li>
</ul>


<h3><a name='milou' href='http://quickgit.kde.org/?p=milou.git'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Improve section hiding logic. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=044de4fd5dad7d1b284dcaeca3a660c9a9bcf80b'>Commit.</a> See bug <a href='https://bugs.kde.org/360080'>#360080</a></li>
<li>Hide separator if the previous one is highlighted. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=71facea055215013870557e6f1215ea1bd89a919'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360080'>#360080</a></li>
<li>Elide subtext in the middle. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=944888b06066102815e5b0a0255a63942d9b4786'>Commit.</a> </li>
<li>[Milou] Support Drag and Drop and show separator lines only between sections. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=7fd8207a049dfc364f23b5c548d6c4198e7021b0'>Commit.</a> </li>
<li>[Milou] Expose single runner name and icon. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=efcdab0f7238762bfdabfd3ba3569676eb8890ba'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126877'>#126877</a></li>
<li>[Milou] Show runner actions only on current item. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=0d30ea8e570d42d6ac9e4a6d0cdb7e0da0a23d4e'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=ad838eefb1ed6c9dfc6e288e8cfd05faaa4e4dbf'>Commit.</a> </li>
<li>Support runner actions. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=f94c18a67aa6dc1c3732e08110e554faad491fa5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126514'>#126514</a></li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>[kstyle] Disable window move on Wayland. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d8f7ba81ff4690b84dc7bdd1e5719dbba4ea1519'>Commit.</a> </li>
<li>[kstyle] Add static bool Helper::isWayland(). <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=0ea786c637bd2a538db27fce8601b2775b9af3fd'>Commit.</a> </li>
<li>[kstyle] Cache whether we are on X11. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=5dd6a502fa4128f774df6c242c1dbf415c4fddea'>Commit.</a> </li>
<li>Use KDE_INSTALL_DATADIR to install lookandfeel directory. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d885ff0e5d9f363a8449b4648ebad93ef7f97f2a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126775'>#126775</a></li>
<li>Properly enable mouse-ove in tabbar arrows. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=bfc68f9bb27d0a2825a97d5545241571faddde12'>Commit.</a> </li>
<li>Added option to use window decoration colors rather than window colors. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=cc390b66fcbca10d5bede00a2a97bd188ae6399c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358257'>#358257</a></li>
<li>Sanitized background rendering:. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=199f00adee42db85b00d9b8f56b86dd354b2fd94'>Commit.</a> </li>
<li>Removed all background pixmap related code: it was not ported to kdecoration, and thus out of sync between style and decoration. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d44560b7c574297fa907e1953edc360c3d0409f3'>Commit.</a> </li>
<li>Removed obsolete config options. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=4968227a98b8b616dd4f13d0e790fc1c1b3d2220'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d796416ee8ba154211f327016b3764605e604a3b'>Commit.</a> </li>
<li>Fixed compilation for kde4. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=e1407e1e9bed4aa6622f909ea23ffa606ada99fb'>Commit.</a> </li>
<li>Removed useless oxygenutil. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=61272549dc0f02839bf2882a6230cd928f162954'>Commit.</a> </li>
<li>For standard configuration, rely on KCoreConfigSkeleton::load and ::save. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=af5f5feeb61f72acee206835d39f6bf7c5fe9475'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357135'>#357135</a></li>
<li>Call updateButtonGeometry after decorationButtonsLeftChanged and decorationButtonsRightChanged. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=4280d9b484af5315d9522c682d73462b4b98de7a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356869'>#356869</a></li>
<li>Cleanup shadowhelper's widget registration logic. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=bd68524bdb231a5bbaba1f2037cfd4205525448c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356625'>#356625</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix the krunner KCM UI. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d107292076cfcfb2df393ae1fd1e80f0cb65d13a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127389'>#127389</a></li>
<li>[Widget Explorer] Become transparent during widget drag. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=46f466bf12c6e6e2d8d50f53cfedcf48dd8a7eb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352527'>#352527</a></li>
<li>Properly initializing activity configuration dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8cbf5359f3d03fcdce2730c308bcce1d87e12587'>Commit.</a> </li>
<li>Allocating the dialog and loading QML only once per application instance. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e350ae8e33eb0130f17ad6656d81c604f67ed8c5'>Commit.</a> </li>
<li>Fixing background reloading. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=71df182b5d18efe32fc8cefe9eb024f1b8cce78d'>Commit.</a> </li>
<li>Fix config file sync when saving mouse settings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6686504011a2d3111f549ca5f5181dc55b505f0a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348946'>#348946</a></li>
<li>Don't try to scale the pager to Infinity. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a08f4178912a7f91c972808c1a6e21933505d29'>Commit.</a> </li>
<li>Fixed issue where a newly stopped activity does not pop up in the switcher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ad64230cf6944ab77ef21c08119843547cadcfbc'>Commit.</a> </li>
<li>KCM Look and Feel: Fix logic for HasColorsRole. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f707bb2055ea4a9331ce4b0813674619d68d397b'>Commit.</a> </li>
<li>LayoutManager: Restore rotation on remove applet undo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a59132ab3eaecd61328939ce9a230c688336f0ae'>Commit.</a> </li>
<li>Touchpad KCM: Fix synaptics driver issues. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=86b59198f1c60236034567dc804b578376f8d06c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127306'>#127306</a>. Fixes bug <a href='https://bugs.kde.org/359460'>#359460</a></li>
<li>Fix minimum size of applet handle when there is Open with button. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ad38d1ada6f0f56d62a4ce270623d2e9e24fe9c5'>Commit.</a> </li>
<li>Fix position of new applets added with drag and drop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f3f6867da8859a57385805ba46ff7cbfd98b4fef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359158'>#359158</a></li>
<li>[Panel Configuration] Make sure "More Settings" button gets unchecked whenever the window hides. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cd0d29d0309f4ce0a926dde0f600319f51baaf7f'>Commit.</a> </li>
<li>[Desktop Toolbox] Elide text rather than cutting it off. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=af861ab88fc545818995d3c3384791b7471bd19f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352790'>#352790</a></li>
<li>[Task Manager] Don't use plasma theme for media player icon fallback and don't animate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=67633b61575a2d1775e1388ab93ed6bb26987c19'>Commit.</a> </li>
<li>[Trash applet] Define icon declaratively. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=90c5ae1c6a792301974852f5d983eb5e5a21a730'>Commit.</a> </li>
<li>[Trash Applet] Fix "Empty Trash" enabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2cda395d7cab125809b9cfb3e15f53417f35b938'>Commit.</a> </li>
<li>[Trash Applet] Use drop shadow behind widget. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7e3edeef390d8139cd7634c8cc7dc6cd6f8deca2'>Commit.</a> </li>
<li>[Kickoff] Silence warning. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2095750b16d1598bf131c1d2393a37ddbd8f6ddc'>Commit.</a> </li>
<li>[Kickoff] Remove old arrow icon code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1018911cd77565b3950096abd6bdbb6374b705ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357411'>#357411</a></li>
<li>[Task Manager] Use bindings for tooltip information. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2fce1bc6a837fff8457e6e54566a39bf287a32f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360047'>#360047</a></li>
<li>Kimpanel: use Plasma.IconItem instead of QML Image. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b270e277599cc6812777e44c2ac9113bcce4c30f'>Commit.</a> </li>
<li>Update window title in tooltip in realtime. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d815d41460206a5555c58228fb1b6b0e7124e336'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360047'>#360047</a></li>
<li>Center contents of desktop toolbox. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3df205ea70f80d54c287f271fdcfc72cb1b59631'>Commit.</a> </li>
<li>Fix crash on item move with manual sorting strategy. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=04fc22dc6e20a1ec3ff4e4ba5835418f721191b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358871'>#358871</a></li>
<li>Don't show applet handle outside of desktop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b65ef8797d97d270be63a6f1425a65372fa34566'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353992'>#353992</a></li>
<li>Baloo doc: fix release info. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d4e9383fa4863090f21fca9d737ff98b4d15c56f'>Commit.</a> </li>
<li>Emit PaletteChanged when setting colors from look and feel package. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ec5e86a51764e2540ac3d01e589cceafe2c7d5a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359924'>#359924</a></li>
<li>REVIEW: 127197. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1f5eb9fa976253d411e1a2756f0d7939b6bba880'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127197'>#127197</a></li>
<li>[Task Manager] Don't show task is on all activities if there is just one running one. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f3c41c2725725d2b2cebbe0264e052b1e3057353'>Commit.</a> </li>
<li>Fix build properly by adding the lib dir to the includes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=35d718c20707c7dfb30754631fc08c84401cc900'>Commit.</a> </li>
<li>Replace cartouche around labels on the desktop with a drop shadow. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89e103ce6f7b4c5018a34bfe12037736189ff0de'>Commit.</a> </li>
<li>Add a fake libkonq_export.h to fix the build. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0aff2004d2ac7874fa774f35d2f89ddc3ad39d46'>Commit.</a> </li>
<li>Componentchooser-filemanager: clean out any kde-mimeapps.list which would take precedence any cancel our changes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=622a6d0a5a79c7919032c82e8e01127d8a4d1cdc'>Commit.</a> See bug <a href='https://bugs.kde.org/359850'>#359850</a></li>
<li>Removed X11 ifdefs and compile-time checks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=88194e15e09a52532e4d367085f28821e2300c30'>Commit.</a> </li>
<li>Also update KonqCopyToMenu from upstream libkonq, allows to get rid of KF5::KDELibs4Support here. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e90c0c9a4f41ed36a4f14e9bb30a98e23653b9b7'>Commit.</a> </li>
<li>Update KonqPopupMenu from upstream, adapt calling code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a00db7833250587c84d82f8957a70a0eb972e1e2'>Commit.</a> </li>
<li>Use updateCurrentState when touchpadReset happens. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f91a7c05afece3307c5d3ce83c44c024a95f9a67'>Commit.</a> </li>
<li>[Task Manager] Add Jump List actions to grouped tasks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6c25001147e1de38aa344d16b66c544be0222c41'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126593'>#126593</a></li>
<li>[FolderView] Hide tooltip when beginning drag. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=05de074748ab86d0131189909690a4f7ded3d68d'>Commit.</a> </li>
<li>[FolderModel] Simplify deleting existing drag image. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d59af0bff7b2117e6b16d7ec47e486f4712da17e'>Commit.</a> </li>
<li>[FolderView] Allow chosing list or icon view in popup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=28aae4bc188c8c89dbb00cf060aa34472787355c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359618'>#359618</a></li>
<li>Moving Activities KCM from kactivities to plasma-desktop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=715d3ff72ed6625abfff237c44babfd2711901fc'>Commit.</a> </li>
<li>[kcms] Restart Plasma, not KDE. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9f2ac27cff89ba2c8e6550b1ad335c4bb1da970e'>Commit.</a> </li>
<li>[kcms] Make cursortheme work on Wayland. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=73862dd5ba83799339076c023c3900fe03dcd33d'>Commit.</a> </li>
<li>KCM Runners: Use HighlightedText color for selected items. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6ea637881226c70793d9047f453aa23d127476d6'>Commit.</a> </li>
<li>Added indicator of whether there are windows in an activity to the switcher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2a38aec17df8e31ff0480f705da05cbdf599548b'>Commit.</a> </li>
<li>[kcms] Adjust keyboard kcm so that it works on Wayland. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=53a85a66809b4526ba0b006ef2a4a190aa4bed5b'>Commit.</a> </li>
<li>TaskManager: Don't use Plasma theme icons in task icon. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=310cffc34156b094c0c68d5002ecd2d2936ac546'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359387'>#359387</a></li>
<li>Map index. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9557cd4144e442881c1e49d057cd620d77321b3c'>Commit.</a> </li>
<li>Try to omit file extension from selection when opening rename editor. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a4541f6ea9cb36fc45eb50b606c15b099ce50157'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359595'>#359595</a></li>
<li>Sync with "upstream libkonq", removal of deprecated stuff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3e9498a09764b8222ad9dc07423592c11d0e1856'>Commit.</a> </li>
<li>Fix tooltip icon in showActivityManager applet. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=816f89f3d78a4b1c1b6c2712b1d0f0e697002713'>Commit.</a> </li>
<li>Accessing the item with .at(row) instead of going through the iterator. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=43c9c5f8e440615521d8b5b2e54076048d904f85'>Commit.</a> </li>
<li>Added parent index check to rowCount. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1ef79d33e6c10d423f7469b33952ced733db7c9a'>Commit.</a> </li>
<li>Revamp of the activity switcher backend. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1d2b8fbe6150ee15299f5b4b9863e4125c62e133'>Commit.</a> </li>
<li>Fix size of drop placeholder after moving larger applet. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f1ce45570e9fd98407fa8ac8d91c70de6dcd6e71'>Commit.</a> </li>
<li>Fix positioning applets in free space on desktop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3373696fe4cac70943f6258b916c64c8e1961fa4'>Commit.</a> </li>
<li>Some complementary colors. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0d48ec2337ca45469860d700d8de744746208a64'>Commit.</a> </li>
<li>Do slightly more useful stuff with the left/right  arrow keys in panel popups. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6be370975e63d3cf3dba359d29bcbb88ddd00376'>Commit.</a> </li>
<li>Add back button support to the Up button item. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9816d5ffcd8d822df564a3d09b6325c157c37816'>Commit.</a> </li>
<li>Even if page supports saving, try automated anyways. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=46c9fc028eee8ddeb164590aa075c0309f4ada52'>Commit.</a> </li>
<li>Drop usage of Qt4Support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=60c146648327a0f2d0c0f1ce6a6312e03d1c2260'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126996'>#126996</a></li>
<li>[Desktop Containment] Remove custom tooltip delegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d290ac0d0e071fa69cf07f1c3b95d140ecbfbf97'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126956'>#126956</a></li>
<li>Panel margins logic. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=05b3fd8b5a2f98a85e69a595315663eadd118967'>Commit.</a> </li>
<li>Take a stab at back button (mouse & keyboard) support in panel popups. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5f42b3b5dabfed2dd002d355a2c6ef05198d2752'>Commit.</a> See bug <a href='https://bugs.kde.org/358895'>#358895</a></li>
<li>Check for selection before handling Return/Menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d7cc4b9313d4e289f3e2669c9861f6d19ba764a3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358941'>#358941</a></li>
<li>Remove Theme Details KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=35bfaf2978ce4e24c07238b0230dd48b2ac6b566'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126953'>#126953</a></li>
<li>Clone useExtraRunners config option from kicker to kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a209560926cd29276e63a3e1a47c4ff144ebc5ac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358881'>#358881</a>. Code review <a href='https://git.reviewboard.kde.org/r/126951'>#126951</a></li>
<li>Set layout in kickoff config properly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a01c09b2df5ebb0c243792a6a3396e2bf34a555f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126952'>#126952</a></li>
<li>This plugin was never loaded as the plugin look-up was broken. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=552f02e5488a916b29fe684fb58ca7df40c7b15b'>Commit.</a> </li>
<li>Readability. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=480589b09a72a8a7c9ee6efc6fc68de212b38777'>Commit.</a> </li>
<li>Remove cruft. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e7dbcb4162faae0487ef2295e93bf87702b59861'>Commit.</a> </li>
<li>[kimpanel] Use popup instead of tooltip. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ebb5292b4e2948aabce20a7aacc30de5dceda7bd'>Commit.</a> </li>
<li>[FolderView] Show title in tooltip. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=954a4bc30b84b46d5cdfd9d5809dcd56fdff7dfd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126957'>#126957</a></li>
<li>[Widget Explorer] Show desktop on dragging widget. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1aab04cec1c479d68171216b877d1b91a7af5adc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126938'>#126938</a>. Fixes bug <a href='https://bugs.kde.org/327530'>#327530</a></li>
<li>Rename "New Session" to "Switch User". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=15de8513aa3eb8138c89202691743a6500ec1263'>Commit.</a> </li>
<li>Add getLayoutDisplayName to keyboard layouts DBus API. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ef79414c794c7343ed09d3714c741e3a080db71b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126943'>#126943</a></li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=832fe8c70173666cddf6253ef3288d232cff2383'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6049be4959037001af39be87fc38f006887852b9'>Commit.</a> </li>
<li>Plasma Search KCM: display the runner descriptions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3546406db06600ff61132085e08366d62cbab53f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126300'>#126300</a></li>
<li>[Task Manager] Prevent accidental click-through if a media control is disabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=295bf76b2624b253a7c785a87722de615c68ab20'>Commit.</a> </li>
<li>Add the complementary category in the color kcm. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=61ba89b41ad1a43cd914557231f6bedf1985b1e2'>Commit.</a> </li>
<li>Use PlasmaCore.IconItem in Kicker, Kickoff and Folder View. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=58193a3926be45219c3a01b50a39ce189180d75c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126841'>#126841</a></li>
<li>KCM Keyboard: Use ECM to generate debug category. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ac02735d9d5d4026fc94967794d870989273c3ed'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126840'>#126840</a></li>
<li>Remove the duplicate m_touchpadOffAtom member. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cda58f0e256d53e66ca44b77fa7307ee670b8313'>Commit.</a> See bug <a href='https://bugs.kde.org/357097'>#357097</a></li>
<li>Don't set available space when rotating applet. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=785cb63428cfe93f1456d03332581ffcda5eed10'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126825'>#126825</a></li>
<li>Add missing file. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ee1d076a2338a4763f65e4d53a02065425ac7a76'>Commit.</a> </li>
<li>[Task Manager] Provide media controls in tooltips. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f9e87f911bb7823b1ae1aca16606cc9b50adf7e0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126647'>#126647</a>. Fixes bug <a href='https://bugs.kde.org/352126'>#352126</a></li>
<li>Ignore drag events on containment/panel when immutable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=92665578f9e8190267af245952e8ace1be5e79a5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126801'>#126801</a></li>
<li>AppletConfig: Set width of ScrollView with Layout.preferredWidth. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e22a6d5f6ea420218fe0e099fcd11631d70319e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126802'>#126802</a></li>
<li>Add missing file. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e6adb9ee8ed626524af40c7cbd0c372a45174552'>Commit.</a> </li>
<li>AppletConfiguration: Use QIconItem for category icons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cf0906fdae6cf256bf747cac2e7569d1cf3b9f6a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126800'>#126800</a></li>
<li>Use PlasmaCore.IconItem in WidgetExplorer and AlternativesDelegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=32c6a35f0ae3b5074cf4ad3a9530627fbc2f6e0f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126794'>#126794</a></li>
<li>Use live directory icon by default for compact repr instead of hard-coding "folder". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c8d1de8e2009c63093ec2c91267a0a92f1c0efea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358167'>#358167</a></li>
<li>File drop support for Folder View compact representation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bae7180decd3f2ed653cc00434aa0ea2dab67ece'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358166'>#358166</a></li>
<li>Test against provide value, not containment name. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=68c132fa7cf7589ac3b7535699f5128dcda256d1'>Commit.</a> </li>
<li>[Pager] Use HiddenStatus instead of size hack. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eb3b3006b9e275688481be7eb57b798f73d12faf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126777'>#126777</a></li>
<li>[Panel Containment] Hide applet if its status is Hidden. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dc7c3c1a0f25ae4e6f9d66b9c8df300d0768c9f7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126776'>#126776</a></li>
<li>[Task Manager] Add support for Unity Launcher API and Application Jobs. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e284e9dc17051f22d05985e218fa44ddaba78de5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126621'>#126621</a>. Fixes bug <a href='https://bugs.kde.org/343632'>#343632</a></li>
<li>Auto-add Input Method Panel widget to Default Panel depending on locale's language. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=919262448b1644c9c86bb40fefee3f6968656818'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126752'>#126752</a></li>
<li>Fix building on Linux with clang/libc++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=284cc9b99b1864268a444f6faba5fe25638bc310'>Commit.</a> </li>
<li>Fix calculation of availableSpace for applets. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2c56715c93cb439df77f29260e3bfc9cbd76e806'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126731'>#126731</a>. Fixes bug <a href='https://bugs.kde.org/346815'>#346815</a></li>
<li>Refactor kcm touchpad to fix some issue related to hotplug touchpad. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a39677c1fd1e6303ddaf31da5ccd0a6454104066'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126513'>#126513</a>. Fixes bug <a href='https://bugs.kde.org/349545'>#349545</a>. Fixes bug <a href='https://bugs.kde.org/356923'>#356923</a></li>
<li>[Task Manager] Don't show on which virtual desktop a window is on if there is just one. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bb4010a80577b927432717616f2560d990457069'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126669'>#126669</a></li>
<li>Make Kicker transition into full repr on desktop. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=20ea3f6362e68eedb44ab93569301cee06574333'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357764'>#357764</a></li>
<li>Move kimpanel around and add to build. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=01fce13e7240fb8c266bd36931249a6d519b7f8a'>Commit.</a> </li>
<li>Importing kimpanel from kdeplasma-addons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=be302af6f24ecae383a6e5023a179925307fec0d'>Commit.</a> </li>
<li>Disable unused repeater. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b4eb5e4aa560bb8e016f5ef63d79f38bd8510a2d'>Commit.</a> </li>
<li>TaskManager: Fix tooltip overflowing screen size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1f670d643c68c16a176cfba33592d9c11d71e881'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126635'>#126635</a></li>
<li>Fix icon hover effect breaking after Dashboard was used. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0424f6beceb502b93d10e79976bf7ff0e3c179b2'>Commit.</a> </li>
<li>Use Oxygen sound instead of sound from kdelibs4's kde-runtime. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3be49d61fb7ca9f4545c17f2cb7e7a83f23bca5f'>Commit.</a> </li>
<li>[Kickoff/Kicker] Extract underlying URL from search result. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=30ab223050d1d57b5fb81de366dbb89e6e67a21c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355808'>#355808</a>. Code review <a href='https://git.reviewboard.kde.org/r/126579'>#126579</a></li>
<li>[Kickoff/Kicker] Add context menu actions for search results. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bede311c7c720a474dd65ed8b3b23aafbf9df237'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126578'>#126578</a></li>
<li>[compactApplet] Read ToolTipArea.mainItem from plasmoid.toolTipItem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6e0d0c49a3cf5881757b57f55c60033efd6cb3d1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126464'>#126464</a></li>
<li>[Kickoff] Ignore external drops. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7f47eb7bdd77c7bbe92f0bf2c5259f641a02a41d'>Commit.</a> </li>
<li>[Task Manager] Send service name and icon along when launching Jump List Action. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5bf1590a8a06e0a25253b747df2404d477fbeed5'>Commit.</a> </li>
<li>[Kicker] Send service name and icon along when launching Jump List Action. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=823872ed5c8459a18cb4efa02e8629199a1de6ff'>Commit.</a> </li>
<li>Don't duplicate checking content length is within user set bounds. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ba733d16fb2849b2b2cc390ed89b5200dffb5c3f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126573'>#126573</a></li>
<li>Add explanatory message why layout combo is disabled whilst widgets are locked. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1ff63c257ec57dcc5c13507981ea8ec1fc79544e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356874'>#356874</a>. Code review <a href='https://git.reviewboard.kde.org/r/126499'>#126499</a></li>
<li>[Task Manager] Drop application name from "Recent Documents" heading. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7e6bfdea58e1a5a89103114293f50403677f996b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126462'>#126462</a></li>
<li>Use tight bounding rect so we don't get a jump /up/ in size while scaling down. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d7813545fcd79b920e59aef8cff8fc0ea98b3e72'>Commit.</a> </li>
<li>Change taskmanager applet TextLabel's font size when switch to oxygen theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bffe3e321bf68f02afb9807063a93ec18fd3a2b1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126412'>#126412</a></li>
<li>Update baloo's D-Bus interface name in KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=93e437c684ed57fefcf2f5821edd52008e60c2a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125129'>#125129</a></li>
<li>Containments/desktop: Add shortcut support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b7589b259a941c28b972dc816110dfc907ff0c85'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126238'>#126238</a></li>
<li>Revert "Revert "Don't show favorite apps among recently used apps."". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f911e7c93fb0e7bf99783ff4a9e2b5cbaa880023'>Commit.</a> </li>
<li>Revert "Don't show favorite apps among recently used apps.". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=22e4a4af08a077045ee413f4abd92ff9c7fc5d7b'>Commit.</a> </li>
<li>Don't show favorite apps among recently used apps. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d2fdfc329ab95c6e6dfe0eb9879df2c2df1335d8'>Commit.</a> </li>
<li>Desktoptheme kcm: add i18n context to theme details. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3c6a140c679a7a5f7d96583b72f2df044227d997'>Commit.</a> </li>
<li>Allow launcher DND also in the default Task Manager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b8334388a048c8098597bfb0a87289d1ddd4ccaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333316'>#333316</a></li>
<li>Fix build caused by previous commit. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5cf51dbb2387381bcb26cdde9494c1f102611ae1'>Commit.</a> </li>
<li>Knetattach: Changing port doesn't enable connect button. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e8f8f1e375e0bcd33ee5b8434d89b0c7c2454988'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126250'>#126250</a></li>
<li>Remove a debug output using console.log. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=531eadfd365c94610f8f09824a75760771d80c12'>Commit.</a> </li>
<li>Kickoff thought that its source model is KAStats::ResultModel. It was mistaken. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3c1c89866f0301698097e48c7da3cf6b2978acbd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355672'>#355672</a></li>
<li>Make handleJumpListAction() a slot. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2d080b26e301deadea1198703891d8a84245fa64'>Commit.</a> </li>
<li>Kicker, Kickoff and KickerDash now support Desktop Actions aka "Jump Lists". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=49ca2ee6bbd9278bd80bbf4b192271efcbe81c8c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126178'>#126178</a></li>
<li>[Kickoff] Move "Add to / Remove from Favorites" option to the end of menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a0047ab5846f9adeb8e52e6ef849fb9589f4b32'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126179'>#126179</a></li>
<li>Fix the build - header file additions were missing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4d2fcda8c5d05a74fa83d64afe976e632075a0b4'>Commit.</a> </li>
<li>Task Manager now supports Desktop Actions aka "Jump Lists". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=77af7adc62fc184a09356eeeb58655d70f7f5f96'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126174'>#126174</a>. Fixes bug <a href='https://bugs.kde.org/339750'>#339750</a></li>
<li>Taskmanager: Make the windows in tooltip scrollable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ceb827322c3402ae08e88f60fbed7a09ed0a222b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125908'>#125908</a></li>
<li>Don't build kcms/input and kcms/touchpad if XInput is not present. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c3f3ced00441a3da4a6db6b009f5440c2cf1b95a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126014'>#126014</a></li>
<li>Kacess should not be built if xkb is not present. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8ebadcaf162142a8040aed589bb6232e2b186f0a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126013'>#126013</a></li>
<li>Kcm_formats: optimize loading of flag icons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7d99ff286eec9e3e1a632c3a622063e0d507c805'>Commit.</a> </li>
<li>Kimpanel: workaround window content not update. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=203ab7945aecc8c5b8c7d2687943ec489175872f'>Commit.</a> </li>
<li>Kimpanel: fix window position when coordinate is outside screen. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=64a2e63ba65f1d2ed93bb5485713a37b2d3c116f'>Commit.</a> </li>
<li>Kimpanel: workaround sometimes window content is not updated. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9f15ffa60f9f234584162ff407c07e1c7b19d1e1'>Commit.</a> </li>
<li>Replace reference to licence with the bsd licence as the reference often goes missing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a7821ac476dcf1030bf19b2b54724894eeb20637'>Commit.</a> </li>
<li>Disambiguate i18n string in button and title position. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3ed35bd695f124f89a6414a7e237caf52beffa1f'>Commit.</a> </li>
<li>Kimpanel: add font customization. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6a483d7bafb2eeef7582a7e5f9a53edc49900212'>Commit.</a> </li>
<li>Deprecate kservice_desktop_to_json(), use kcoreaddons_desktop_to_json(). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8dbef4fcbffbaf53f686abc3c2c973552666c4fb'>Commit.</a> </li>
<li>Hide preedit and aux if both of them are not visible. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=48c3b28ba8760144f8fdc4a05f04c86516f211d4'>Commit.</a> </li>
<li>Kimpanel: try some other way to fix layout flicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1f769241b0d5efab6813a9d077160be191062d52'>Commit.</a> </li>
<li>Kimpanel: tries to make size fit better. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=587dc0b69b400b2f8dd8658de1105f71ff01c1cc'>Commit.</a> </li>
<li>Kimpanel: make sure size is correct. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5454c81846aeddea7d1642f33ec8a8b4c1102b44'>Commit.</a> </li>
<li>Fix typo: window -> inputpanel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2559f322279d2ccee0abca6e7ab6732ae4be3b5a'>Commit.</a> </li>
<li>Don't install kimpanel.xml, ibus might pick panel randomly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2ff73eccf91bd1cfa24c8d6e6e95868460ad2f57'>Commit.</a> </li>
<li>Kimpanel: Include <locale.h> for LC_CTYPE. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0412c88f1ac88722a5e0908c7db5541f2439aa08'>Commit.</a> </li>
<li>Fix crash due to off-by-one. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c2e59eae73c37b4217559aee08f15d125e7523d2'>Commit.</a> </li>
<li>Kimpanel: don't show other icons when switch im in ibus. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=30d5dc5f0b734db33c24dd0a297f5c1b8c6452e6'>Commit.</a> </li>
<li>Kimpanel: add radio and check type support in ibus backend and dataengine. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=48b49555721398ddf273979f0c285565503f3a33'>Commit.</a> </li>
<li>Kimpanel: fix text icon size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4f16c37bf14b1b62e5bcb6cb46786df34ce2cee9'>Commit.</a> </li>
<li>Kimpanel: implement reverse engine navigate with shift. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d01b56db5c9ce67620c512ea5fe7616f3b9a233a'>Commit.</a> </li>
<li>Kimpanel: add a timer for property update, to bash some frequent update together. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=03bb4a02b73f3412fdc975520ef52f473e00c164'>Commit.</a> </li>
<li>Kimpanel: fix some gtk icon, and wrong logo update. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=18b22385b635ef2762d94523fdb884886b26c110'>Commit.</a> </li>
<li>Fix a property string typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3a93fc1d611c4b421dd55e7128f4fefe77bdf148'>Commit.</a> </li>
<li>Fix typo Apperance to Appearance. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f80a424e2a47161669efe02670d00b634cdf7369'>Commit.</a> </li>
<li>Fix position.h -> position.height. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=610a9cc87f53243c13da65d7712329806ea7b415'>Commit.</a> </li>
<li>Avoid using var typed property, might trigger a qt crash. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4b7a44d3ce4b3d6d76bcdc75f9b35d8fec4ee176'>Commit.</a> </li>
<li>Change IconItem to Image, to avoid animation when pixmap changed. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=388558b7eac5bc6d51592844e93fd7005d344a93'>Commit.</a> </li>
<li>Adopt new SetLookupTable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5373323ab215fc79bfdb1dde9a1a7f9a541e96f2'>Commit.</a> </li>
<li>Port ibus backend 1.5 to xcb and qt5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=06987171c22c4b3e669ac02e7807db46913ffb65'>Commit.</a> </li>
<li>Strength against possible strange ibus engine list (e.g. duplicate). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=93607bf56923fb0e815d4c73ffc6ec552a1af3bd'>Commit.</a> </li>
<li>Fix focus out and context. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=da71b9c24f1a276f59ea5db4de09856829a7c3d7'>Commit.</a> </li>
<li>Fix focus in event and nagivate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3b65c58bbe1e0c95558a426a0bb2d70199943d6b'>Commit.</a> </li>
<li>Add a check. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f6dc9f8bc601fcca2cf0d706cb60a61e52bf71df'>Commit.</a> </li>
<li>Remove some redundant code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6dd2fe108989940144d3b4b88fe018382242fb7b'>Commit.</a> </li>
<li>Add more check. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=02300154c5b104eadc31a2b9db531c2763fc8128'>Commit.</a> </li>
<li>Implement navigation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ba08d60ee8792d4f2417a04266ba2627147fe16b'>Commit.</a> </li>
<li>Complete ibus 1.5 port. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=88c472a6b82bf6496c9fd931bb2b77b4e83c37a2'>Commit.</a> </li>
<li>Tidy root CMakeLists.txt. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c97891f698c380f97072942ad3f96e6436ac16f9'>Commit.</a> </li>
<li>Fix Messages.sh. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5cad2b3c53468f4cfe40362d4638db2164fc3ec0'>Commit.</a> </li>
<li>Add text icon support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4a3b7d0cc8499a4c3bc924ec9023f66e3f0984ea'>Commit.</a> </li>
<li>Add private plugin for access screen geometry. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=187ab058c075a82e289c6cbcd199cc589f47b276'>Commit.</a> </li>
<li>Fix CMake error on kimpanel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e64a1acca97d7201d734237ee5d87485308ea10b'>Commit.</a> </li>
<li>I18n fixes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=851d5f494c8ce42d9df9b314d47ef378b4f11ea8'>Commit.</a> </li>
<li>Fix license. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=714994aa748f77d5e791d54daedd9ebb5baf8a0d'>Commit.</a> </li>
<li>Remove unused c++ implementation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=857fb1f48e8fe01330cdbe48204b72418530cc64'>Commit.</a> </li>
<li>Port kimpanel to qml. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a5e7bf8e98724d94c0d2cc62af09a7ecbc0bc953'>Commit.</a> </li>
<li>Port kimpanel dataengine and backend to KF5/Qt5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=845e9531105622835003d1ea714daa9d2c416e3b'>Commit.</a> </li>
<li>Adapt to changes in plasma-framework. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=967f689b40ccbd40adbf9513b026848d668e8704'>Commit.</a> </li>
<li>Check screen upper boundary for kimpanel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8b6697ced8eee43419ef0a1359ed7509087b0e1d'>Commit.</a> </li>
<li>Remove some KIconLoader::StdSizes usage. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b05e7098cea464b20e3a6551f8951a187be7c14b'>Commit.</a> </li>
<li>Revert 6267fba. ea3bdca1, 078a7fcf6 for 4.10. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ee3526f835d33f1433d232e3e9698de8437005f6'>Commit.</a> </li>
<li>Blacklist ibus until 1.4.2. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2478373d7a79001360cea698f91e5324fc97456a'>Commit.</a> </li>
<li>Blacklist ibus older than 1.4.0. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4d67c386567ef10b9b2ece34d6e2bee677866919'>Commit.</a> </li>
<li>This is the best what I can do for ibus backend. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=95ce16f259f922e121672d9c096fe62e6c5a5891'>Commit.</a> </li>
<li>Fix first status icon update. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=80945a430f8178489d2a57fc225f847cf95c2610'>Commit.</a> </li>
<li>Seems the offset break a little, should be more care for the size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=50e20a19000b2bf53388070b6de58cae1c3b0ca7'>Commit.</a> </li>
<li>Add small offset at the bottom. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eab293e39c5c4ab66be1d0591ee079d7e5775c90'>Commit.</a> </li>
<li>Port kimpanel to Plasma::Dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=df37165985d041803cf5179693b0db121734f09b'>Commit.</a> </li>
<li>Take shadow into account when process size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=558bd26f762d8d5752d1ffa50a1dcd4df4f02cb3'>Commit.</a> </li>
<li>Fix kimpanel shadow, use kwin shadow if possible. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a611dc9a308029791a6d5afd8eb5630728514152'>Commit.</a> </li>
<li>Add orientation hint support for kimpanel protocol. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=63f3d0b442ad0999584bb94aed5ebaadcccd942e'>Commit.</a> </li>
<li>Use two char for text icon in some case. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=68bb545c473c2e169bc1f9b9746e75ca2e543d3e'>Commit.</a> </li>
<li>Use two char for text icon in some case. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6ecb9e0578a103b981b95aaddeccfe7a46542040'>Commit.</a> </li>
<li>Update lookup table when set reverse. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=01b463475b7c717728461fe28ad5d99644f4a6c7'>Commit.</a> </li>
<li>Update lookup table when set reverse. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9f583ed3e27530c68a274a80d518d50bf2a08b5c'>Commit.</a> </li>
<li>Use a transparent widget to workaround background artifects bug. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c77434d2c7112d3da6dd49f7f1cb0b221a9696cd'>Commit.</a> </li>
<li>Use a transparent widget to workaround background artifects bug. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9b1b56ab1a2707e286b0b0214cb84030b9c29a32'>Commit.</a> </li>
<li>Fix update order, since we use lazy update. we need to ensure the order. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c4e721361c4cc1d441c854ab286826ea7a323034'>Commit.</a> </li>
<li>Fix update order, since we use lazy update. we need to ensure the order. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b3092291b08df1d6999eaec96be088627393d248'>Commit.</a> </li>
<li>Fix spacing problem with use vertical list. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8844f6b985bca893ed15d92b5c157a62fb7dff36'>Commit.</a> </li>
<li>Fix spacing problem with use vertical list. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=114dc36eee3cdb0218f76ac78832ee321d9f5d7e'>Commit.</a> </li>
<li>Fix more visual artifects. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1637197e0f33e7cda0e16ee18d1c578fb1967e35'>Commit.</a> </li>
<li>Fix more visual artifects. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2a2ce8f07d1975b9defa3513cb7c7315f33d99a4'>Commit.</a> </li>
<li>Reset cache content. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bfa9026379db462e2dc7688ec13366a9761fc3dd'>Commit.</a> </li>
<li>Reset cache content. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=66d6444626ea1a5e7c5a36dfae34b0050e1c7604'>Commit.</a> </li>
<li>Seems finally fix all size problem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e950c9d8cba6523aa377e17b4c61b348d23be89c'>Commit.</a> </li>
<li>Seems finally fix all size problem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e1e66833dd5b4b390e0eab3522a1310909e8e11e'>Commit.</a> </li>
<li>Use horizontal by default. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a9fdd4c000de77704f03fb24cbba552731d42e81'>Commit.</a> </li>
<li>Use horizontal by default. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a7c97409bb9106253ed758cd057d5f5de0d8cc17'>Commit.</a> </li>
<li>Fix blur region. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a40ddc2e191d071f46d3cc01313dabca85060401'>Commit.</a> </li>
<li>Fix some visual issue introduce in last time. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=782496fecb0c7767d0ceb1eadd1c7e937ebbddd9'>Commit.</a> </li>
<li>Update for new layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a89f41d6e7db527cff05e6c3d6908c49f4ec0c73'>Commit.</a> </li>
<li>Fix blur region. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8ae9ba541d6a0c708590c279fa7ae843a9c986a5'>Commit.</a> </li>
<li>Fix some visual issue introduce in last time. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=55e1aae2830ed31d1dbf505acf79db23b5f37d68'>Commit.</a> </li>
<li>Update for new layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=742811b905eb7edaa4279d17c4a461192c912f48'>Commit.</a> </li>
<li>Hopefully fix the size problem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=15700256eb516943bc5a6a4c394912baff358879'>Commit.</a> </li>
<li>Faster check for std::collection emptiness. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3a9edeff53cf38e922d317f5d6bd0c5791f49cf9'>Commit.</a> </li>
<li>Remove a debug message. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5329e946078ce2c10aed0c43bc4e8dc54d6b220a'>Commit.</a> </li>
<li>Watch if input method die/exit, then do some clean up. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=582ed602b2f2f4e142b9e14e6e444fa6b4570c21'>Commit.</a> </li>
<li>Change to vertical by default. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=206043839e5e2f84d1c4740c0d1ce51430d8e179'>Commit.</a> </li>
<li>Scim can also use update lookup table cursor. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9fa9e1af66ce2a027dd81337f5f840227ba19142'>Commit.</a> </li>
<li>Use new set spot rect. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=183b9b8ec80c82bb904a3011cb3613102383e715'>Commit.</a> </li>
<li>Implement lookup table cursor. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5f77a3f3751cd36eafb1f4a56fc2173d7f0a27d0'>Commit.</a> </li>
<li>Delay menu action to workaround gtk lose focus. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e35d650059b33d6bfac747a53dad5b82897a56b9'>Commit.</a> </li>
<li>Rename to avoid conflict. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=08b18fa2d284704192107d9eccee1efd02091eb5'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=65dd25e89e2770ca9bd6b888085e3a61b3db2162'>Commit.</a> </li>
<li>Make reverse can also work horizontal. fix a screen edge problem on the. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1ff8bda0ec71418229cb17bd9ec912a95fa4d468'>Commit.</a> </li>
<li>Add reverse'd vertical layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=35d83ea2944c1a5e67a600367db6b375d8a0612a'>Commit.</a> </li>
<li>Better layout. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7b8b285fd9ab9d9fa2e0b786131ca5f8de839c86'>Commit.</a> </li>
<li>Let's begin with protocol kimpanel2. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1ae2fb9c285abd937b1c79667519965edcf5d742'>Commit.</a> </li>
<li>Fix visible problem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=da31ce054867e3ff70d3342d4b8786516260fd1a'>Commit.</a> </li>
<li>Fix initial window size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3213c61aeabd8396beec1c42347387cb50e0ebe5'>Commit.</a> </li>
<li>Make kimpanel input panel higher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bf4fc5da89de378fc60d1aba678e34cbc3100067'>Commit.</a> </li>
<li>Use plasma style text as icon if icon field is empty. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7bc53fd912d7d0f9197b787d94148874a88637fd'>Commit.</a> </li>
<li>22 is kde standard icon size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b79025ef3cbf0aadb272741823193dd57cbf209d'>Commit.</a> </li>
<li>Add some function back. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=684fa8c9a65d83bc20ab8eb51416535d8b3b9ae8'>Commit.</a> </li>
<li>Further fix for 1.4.99. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=102e32a28f2e0ffd7a30611786be1bbf6ffe2507'>Commit.</a> </li>
<li>Fix for ibus 1.4.99 on fedora. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e177394dd3573eecd421f5b590d83ad69c4fe5e9'>Commit.</a> </li>
<li>Set icon hint. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=611960b4e48ed905780f3fecd0996ced8d9317e1'>Commit.</a> </li>
<li>Fix a label display problem. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5a0b3026f479b5c5e2e848fad8fa68f0f8fc09f3'>Commit.</a> </li>
<li>Fix ibus 1.4 compatible. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=98fec53d55cc689ca40815e9257d575c0da5e101'>Commit.</a> </li>
<li>Do not shadow the `property' variable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=202d84e89eed1424e06314baf96238b4340d7ad1'>Commit.</a> </li>
<li>Make krazy happy. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c917d8cad8ede5388b2acfb84917097712921b7f'>Commit.</a> </li>
<li>Some possible optimization, seems no harm. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a819174cd24cb9864fed4d945d30497090ca8aa5'>Commit.</a> </li>
<li>Dialogs/background will make text more readable while blur is not enabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=17b2cc47fa97f43d51d953dc80c1732acc58f0b2'>Commit.</a> </li>
<li>Add a copy of COPYING-CMAKE-SCRIPTS to the directory. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=add32d82d7f98da3a06100ecd3aee54473ddcd80'>Commit.</a> </li>
<li>Kimpanel is missing a category for a long time. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=248eef6429c02c9eda6dda9b07a7d6a1f3fd9c41'>Commit.</a> </li>
<li>Better text draw. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8853afa6344f98e4fbd35186cd4d41e5d6b1deb5'>Commit.</a> </li>
<li>-Wmissing-include-dirs. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1965f7861be9a72f3daba2844c60977356af6677'>Commit.</a> </li>
<li>Comment out the 'TextLabel' from extraction for translation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=39b84c78894600fd368cd9d538daf193962bd53d'>Commit.</a> </li>
<li>Add new kimpanel, with enchanced backend from kimtoy. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6f03be8abe5a66cfdc7e462beb3bfa1361ad3be4'>Commit.</a> </li>
<li>Merge KDE/4.7. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=16abae9463afb5cef11bd5bc4bc0e75a00bacafc'>Commit.</a> </li>
<li>Normalize slots/signals. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e563a7d50fa8aafff7afcbca4d8651cd6858b17a'>Commit.</a> </li>
<li>Microcleanup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cae01540f0332f5a07cae8a812bb19e26f77c292'>Commit.</a> </li>
<li>Cleanup some of this code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=08c773f8318d77f817dbdf6ac2d526b488d6202a'>Commit.</a> </li>
<li>Merge 4.6 into master. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=025a7147c8f56bd551b807179b630db914506cc8'>Commit.</a> </li>
<li>Build++ (and warning--). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6d232cc88c4648cfb1eeeb838ebfdebf0b726c6c'>Commit.</a> </li>
<li>This object is given to us, be sure it doesn't get deleted behind our backs before using it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fc66c63c11cf2ab0bfed8bf36ccdb1ab5c4fe565'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/250955'>#250955</a></li>
<li>Do not rely on extract-messages.sh doing the work for us. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=de699f966431bba76e2eec7e59030c76f4fa8a63'>Commit.</a> </li>
<li>Forward port 1162744:. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7d850ef5d4d98b5889e589e56a2dc6e7e9166e56'>Commit.</a> </li>
<li>Per http://reviewboard.kde.org/r/4602/ committing to trunk with 4.5 backports. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6e5127f4c4f3154a5f5dcf821bd6a34c15b0991b'>Commit.</a> </li>
<li>Fix initialisation order. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=09f4b80c45a732baf4d8370735aa03f02b7b1e80'>Commit.</a> </li>
<li>Don't -ever- create your own corona in a plasmoid. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a0aca219fd10f1e2f42bf0e634e48f6d5e918b4d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/214943'>#214943</a></li>
<li>Don't mess with scene rects. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f0786b4055e77c49e9658c6627fe28970d0e8ce7'>Commit.</a> </li>
<li>Never ever use your own scene in plasma. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=29d5d85d6fc1f8e34298b3502216e33d47b6a003'>Commit.</a> </li>
<li>Depend on kcmutils directly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=16852e652ff330c4e2a02825c2c7e8e20c2285e4'>Commit.</a> </li>
<li>QDeleteAll already deletes the values and does so by using less memory and being faster than calling values() and then qDeleteAll. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1c4857795fde0e223113d89bd14ef331bbfbd6ab'>Commit.</a> </li>
<li>Updated Kimpanel plasmoid animation to use Plasma::Animator::FadeAnimation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=69701490ec1eafbed67649d77c78ce082e53a4c2'>Commit.</a> </li>
<li>Rename catalog and make the applet + kimpanel translated, forward port from branch r1083716. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e85f823e978d8bda5b0ec2192d5869b347a3f99a'>Commit.</a> </li>
<li>Give it the proper name, ie plasma_applet_<foo>. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bbac3a510560a9005583dfaaef59f81069eb3ba1'>Commit.</a> </li>
<li>Fix compilation with -no-stl Qt. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f558ac1f1554d4e37c58e91c8e23cfd687552799'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/217551'>#217551</a></li>
<li>Get rid of hardcoded paths. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8ca88a073b19eaa3651bb8ec1b919878c977c7f9'>Commit.</a> </li>
<li>Move to addons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=49a4cc51bba31047e28b5cc395f91f458079e863'>Commit.</a> </li>
<li>Fix up the cmake files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=21fc6e4052ffc450ccf9a8d158fe91efee5fa21a'>Commit.</a> </li>
<li>Add missing i18n. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dc032965849f323f1fac564bf6d807aa85d18b9b'>Commit.</a> </li>
<li>Add missing licence file for cmake modules. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=23c00b46009a7a995edaf77cea307584f9583b99'>Commit.</a> </li>
<li>Clarify name and comment, IM can mean more than one thing (see the French translation). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=43aaaee2e9949c3b1e0d05b1c5803dc1743509d3'>Commit.</a> </li>
<li>Add missing licence file for LGPL code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d7f4fbfcb4ccbd5b251bdf52e6a681ecd7bcd11c'>Commit.</a> </li>
<li>Foreach already iterates over values so calling values explicitely is a waste both in memory and processing time. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2f315a16f6f665b9407a2f742bd5a29b5955bd38'>Commit.</a> </li>
<li>Add licence file since people are packaging this. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=67279415866adc045a86408a0423223e7f7dacc0'>Commit.</a> </li>
<li>Able to hide unneeded icons in context menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c24c5eed94f03dde2f8dc5e41c098380e85f336b'>Commit.</a> </li>
<li>Compile. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b9f2356172a58440c89474e6b89969c0d93c4d63'>Commit.</a> </li>
<li>Delete fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c08f26c4a17a37d5a710dbffece9d44b0de42f54'>Commit.</a> </li>
<li>Corrected typo: statusbar -> status bar. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4b8b39feba9a297e62ea49a96e5be4cac5343b68'>Commit.</a> </li>
<li>Disable page up/down button when can't go up/down,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c0d5d27fda00f260c58a036be64714e2805fd17b'>Commit.</a> </li>
<li>Corrected typo. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f4bdf517916aa6f7f3172a5fdc3efee96f76ffd9'>Commit.</a> </li>
<li>Krazy fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ab154432b12f5df713f89f352935c839e8594040'>Commit.</a> </li>
<li>Fix memory leak. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0949459001a4a7319025e2bb2938906e9cf76896'>Commit.</a> </li>
<li>Pos restore in statusbar. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5a1551f776f804612ca78ddfe6248e582c5c7d7b'>Commit.</a> </li>
<li>Fix a layout bug in vertical panel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d34e1369b37a27c0923ff40fd07861a702657f8d'>Commit.</a> </li>
<li>Tell user the external location of fcitx. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2bd605edd0b6e0ee5a5e08de76bfe0da4c48c36a'>Commit.</a> </li>
<li>Fcitx backend moved to:. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=edac334fee12454d9e04c6206527b665ab74194e'>Commit.</a> </li>
<li>Ibus backend: add panel xml description. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5477670633ea06163257300e02253dc6a61c7e6b'>Commit.</a> </li>
<li>Fix im factory status icon. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=388c58993c6295e67928637f9a605121f76e8f9f'>Commit.</a> </li>
<li>Compile fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3274d48746005f3d809af54b198700b7650ecb6c'>Commit.</a> </li>
<li>Add extra files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=65de2ae5fd69de89dda78c15b11c982124c45d4a'>Commit.</a> </li>
<li>Wrong :(. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0fac962642dcb09506a7f8c625b60fc9729dd723'>Commit.</a> </li>
<li>Sth. wrong.... <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=566413df7408d8efeacae03394d063e1d3a0c585'>Commit.</a> </li>
<li>SILENT_MSG. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f2c2bbf1cd560ac0de58ff7fc477c47ed8922992'>Commit.</a> </li>
<li>More files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2f8498b1949e6c4c753671dd92fa0e2ed109509e'>Commit.</a> </li>
<li>Tools files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a969489b61a3170c4f1b189f50815cb771d66105'>Commit.</a> </li>
<li>Lib files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c7db74dd8e2a71686e75a7455fa1ace9e4d0c046'>Commit.</a> </li>
<li>Clean up .. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4f229926ddef56bb84ed11e9d80ea66d2455cbd7'>Commit.</a> </li>
<li>Fix resize bug. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=84a9621c4fcb27cb345e8be3828c649cbee343a8'>Commit.</a> </li>
<li>Fix compile, use smaller icon size by default, minor fix on resize. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c6aeec03b9b215544c0ddad8e63be5b04364a1c5'>Commit.</a> </li>
<li>Remove all custom theme code, stick to Plasma::FrameSvg. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fd3ec84a295a2120807b8cb9e6e0307b72a958bf'>Commit.</a> </li>
<li>Not for it. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7f651f6e798c32605af0275b8c8de8e0dd12f946'>Commit.</a> </li>
<li>Proof-reading of kimpanel, along with build-system changes to get it to. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fc461e672c0c69d6aaa55101ffe960528879af27'>Commit.</a> </li>
<li>Move to kdereview. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d187ed89f99154a7de3c017744f0ee8a83c255b3'>Commit.</a> </li>
<li>Minor fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ae09d12f2ad1ac53e44ee0e3adf0ab0b28bcbe70'>Commit.</a> </li>
<li>Add a new theme, with irregular shape window. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bb2fa2cd0f30c270d199897b27a2b6ea4d40c633'>Commit.</a> </li>
<li>Show configure action in context menu when statusbar floating. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d952bca8bf39e9b26a8fb494c6c6cbb86b1db032'>Commit.</a> </li>
<li>Minor fixes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b7ebff54db8c3d15116df33f35777189184ec51f'>Commit.</a> </li>
<li>* GetHotNewStuff2 theme install support. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6474a0c036b3514a95680033ca3eb1a8238f8e27'>Commit.</a> </li>
<li>Use kcfg. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=23574602ac2379706ea274f5ce9271c3def6dc9d'>Commit.</a> </li>
<li>Fix mem leak. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7b61c8e38b03e71a65c2e91b8c7e88d2a33281ba'>Commit.</a> </li>
<li>Fix class. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e04276739a5b878182f066d90ba429e990aaf98d'>Commit.</a> </li>
<li>Fix include. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2f345dd5a5e98d6ad376f28cf1f126e650105f73'>Commit.</a> </li>
<li>Fix export. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=48c397d3c2ec1e28c04e0f970b53e674af08a224'>Commit.</a> </li>
<li>Scim backend finally build with cmake, cleanup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a11cbd67a48dac4404b520203088f3707a4dd076'>Commit.</a> </li>
<li>Fix a minor error, add kcfg file. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eb437bed5ed598b0df0d72caeaab588a10a049df'>Commit.</a> </li>
<li>Change to shared library build. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=247af7e4b902ef0b13b34984232cf38e2cf9893e'>Commit.</a> </li>
<li>Krazy fix++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4ef906ce05433dd053e5d97a15bf23c114f97c9f'>Commit.</a> </li>
<li>Krazy fix++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ead1d49e306c51d9ea85536b42ef7d1d917d4b52'>Commit.</a> </li>
<li>Krazy fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8cb167f49686df6412c3e3164171cf734282ad9d'>Commit.</a> </li>
<li>Lookup table also use the theme :). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ac8eeddc371b7d5f2ccc0064231756200f5921b0'>Commit.</a> </li>
<li>Porting statusbar to use the theme now. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ba3ae7ec53b66394ff0d05726e0116b903bd7741'>Commit.</a> </li>
<li>Finally build against x86_64 (it doesn't like to link with static lib). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ac3c1e1d1c2e80820a4f4fed9a2954dc192aaf73'>Commit.</a> </li>
<li>Pedantic. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=dfd65a59366e0b7642076747efef312c36276efa'>Commit.</a> </li>
<li>Add a svg theming and javascript dynamic layout architecture. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f71c73a3d69082a096315227bd1b7e38317ce0a4'>Commit.</a> </li>
<li>Change default orientation to horizontal. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a1915409eb46a5d764732b301a5a25be42cacb6c'>Commit.</a> </li>
<li>Fixing up gcc 4.3 compile. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d73b6fe8e9650c53afd28568eb8e546fb6f8550e'>Commit.</a> </li>
<li>Lookuptable layout fix++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c7843977dea6975ca96bac94bb7b89415618f770'>Commit.</a> </li>
<li>Layout fix++. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f61882ae19f2dd568871dc601147644a96493c4e'>Commit.</a> </li>
<li>Rename kimpaneltye.h to kimagenttype.h. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e9c93291a556e0b05cd77bf0813d24f3db823c50'>Commit.</a> </li>
<li>Move scim/ under backends/. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f1b8fb99484dbb03207a26117f6091d10cb06972'>Commit.</a> </li>
<li>Rewrite statusbar and candiate window code to use QGraphicsView. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0f4d0f7d2a07fd2345c59afa9fa6e5db1dacf960'>Commit.</a> </li>
<li>Finally fix transparent correctly (using Qt::WA_TranslucentBackground). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e85e1e7f951716d6eeeb0ab4386f3717e9b1499e'>Commit.</a> </li>
<li>Handle input styles which client can't display preedit text. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d5f1388bd94e00c03e469539626169ae277df31a'>Commit.</a> </li>
<li>Add scim backend install instruction. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=64b6ef5ac097e2c3aa4b3e29e5fc5d51208ec855'>Commit.</a> </li>
<li>Handle show/hide candidate window better. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=32bc73374040e452a06ead8b2c1f00f91aa58da7'>Commit.</a> </li>
<li>Add an ibus backend with main features done:. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b1426ea7f42002934a5842f6f18a8bb3b0b1cdf2'>Commit.</a> </li>
<li>Some minor fixes to the rendering code. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4341c8f45155464a214c4accb7d73b31e5936d78'>Commit.</a> </li>
<li>Extract messages. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d39bf299c3f28848253ea9920c64e3bffe7daebe'>Commit.</a> </li>
<li>Make lookup table's entry more visible and eye comfortable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8a435c7ae1d10b92837a1cf181e6c7d15c788bb9'>Commit.</a> </li>
<li>Minor fix,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=43354155b6505ffcf684ca8799d0791cfb35d23f'>Commit.</a> </li>
<li>Cmake seems use dangerous compiler flags on thread stuff,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=88ff61e372bb527a01debe48ceb336c2c82b63f2'>Commit.</a> </li>
<li>Support dynamically switch between Applet and standalone dialog state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6ecf60816ed60bf842274e43e42e155978c81bb4'>Commit.</a> </li>
<li>Layout icons smartly according to current size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=667b46eacc7de724e469bccc0ab1101218c21c0f'>Commit.</a> </li>
<li>Add layout class. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=00f7c759472b544d1afb7fc2dcc35c3205adacd6'>Commit.</a> </li>
<li>Broken. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3b9de41321f8924b3733d8bbe3e8e1e805d7164a'>Commit.</a> </li>
<li>Change scim backend's build system from qmake to cmake,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c3b673accfed3ca86260d30cc37bccf44b8aac2e'>Commit.</a> </li>
<li>Cache and delayed update to reduce flicker,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=14c0d8754d5ead4c3e21e3c109d315e82c312b6a'>Commit.</a> </li>
<li>Forgot sth. :(. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=66545611b4d5e97c43504c4f0b209e39ea367d53'>Commit.</a> </li>
<li>Add README for install instructions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8771ecbb7ea6e943797fb5d29bd9ea97f45f8a54'>Commit.</a> </li>
<li>Alpha status, scim seems fully working. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5a468e5770a43b0f3589d945b27cb84dbde939c6'>Commit.</a> </li>
<li>Wrap around more scim methods. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7ec24ccd8803a90e0d78aa95ba581e30a69f767a'>Commit.</a> </li>
<li>Scim-panel-dbus to replace scim-panel-gtk,. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b3b48d47279bd54699b805e7241273e430c2e331'>Commit.</a> </li>
</ul>


<h3><a name='plasma-integration' href='http://quickgit.kde.org/?p=plasma-integration.git'>plasma-integration</a></h3>
<ul id='ulplasma-integration' style='display: block'><li>New in this release</li></ul>
<h3><a name='plasma-mediacenter' href='http://quickgit.kde.org/?p=plasma-mediacenter.git'>Plasma Media Center</a> </h3>
<ul id='ulplasma-mediacenter' style='display: block'>
<li>REVIEW: 125993. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=cf570eb71014e517c91f718c02a48b7afaa96418'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125993'>#125993</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Use proper linear scaling for the traffic monitor. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4043c72d43ccb033b0d01cf230aa7b8d3efeca97'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359802'>#359802</a></li>
<li>Filter out virtual devices also from the network status visible in tooltip. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=0a89268a65a5cb6321b03d954421978493fca4fc'>Commit.</a> </li>
<li>Remove old legacy traffic monitor. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=8544070adf555467b5bd17c9cbb00d6914baddf2'>Commit.</a> </li>
<li>Improve l2tp vpn plugin:. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=b4c7cf28c3fea1ce54ca86d7d65494cc861b69bf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125266'>#125266</a></li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=65cae126f238e7abe459d6cbbbf4e9d94357942e'>Commit.</a> </li>
<li>Improve connection deactivated/activated messages when suspending. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d8edcc6ab77836127f3cef5833bcd9e8a88a6e9f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126748'>#126748</a></li>
<li>Split description about IPv6 required checkbox to two lines. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=8f0f9d5fb72ffc23f638302dbbfd11a3e9053693'>Commit.</a> </li>
<li>Workaround broken bindings when enabling/disabling devices using rfkill. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f4f0e01df52f07bfc8017b58e2193316ed9ac0d7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126795'>#126795</a>. Fixes bug <a href='https://bugs.kde.org/358028'>#358028</a></li>
<li>Fix punctuation typo. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=3fbcbd5bb43db19deac1b2ad03ca7031b27f8535'>Commit.</a> </li>
<li>Make sure we show correct icon when a VPN connection with type of generic gets default route. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=190e27a1acaa4469aaeca0653fd0a4368f730ecc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126714'>#126714</a>. Fixes bug <a href='https://bugs.kde.org/357816'>#357816</a></li>
<li>Autodetect wireless security in the connection editor once SSID is selected/written. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=e3927a3ea25c6b576600d5caf3aa54586ba7e3f0'>Commit.</a> </li>
<li>Set default authentication to PEAP in case of WPA/WPA2 Enterprise security. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=e4b546cd1f0342995c4472947a34978d8bf9fa03'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126399'>#126399</a></li>
<li>Filter out 'enterprise' devices for the applet. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ca903e1e1e07ef569e80b9d8fba8ed71a547096f'>Commit.</a> </li>
<li>Disable notification about failed re-scan by default. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=1d0905abafb51fcc956bdc32fdb0be7673608c89'>Commit.</a> See bug <a href='https://bugs.kde.org/341122'>#341122</a></li>
<li>Improve visibility of "import vpn" option. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=98f1f1287cee2d3dc77f0de2d8d607074cdaca81'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126386'>#126386</a></li>
<li>Fix openconnect dialog. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=c004d3563a08ed9519be20c98861999ea72faa42'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356587'>#356587</a>. Fixes bug <a href='https://bugs.kde.org/356622'>#356622</a></li>
<li>Set minimum size of popup dialog. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=79f971f137b1677580a9cd9cf3d285e14ef3e611'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356342'>#356342</a></li>
<li>Make all secrets for GSM connections as not required by default. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=80b46497934e304f67a364cc6ba87c78ff4f5416'>Commit.</a> </li>
<li>Allow to load "uninitialized" setting as it was before. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=914ca8ea676dc5fbe4655f9d9ce09779a2f4a095'>Commit.</a> </li>
<li>Revert: avoid using dialog->exec() in openconnect VPN plugin. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=42f0cbd57677cde47d671774fc099c33ab749c7e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348318'>#348318</a></li>
<li>Do not consider virtual devices for systray icon. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=1e840b9877286592a20cfba1f91b676d178e5ae8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126118'>#126118</a></li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9faf7bce791735407383ef1e2b1609a310deae8b'>Commit.</a> </li>
<li>- Avoid install in root directory. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=18f469ee47fc8809fe54f5a744b0e0794ecc9aa0'>Commit.</a> </li>
<li>Use KDE_INSTALL_DATADIR to install update script. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=2540b44daf40a172dbcc68a7f316c5915a7c2204'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126283'>#126283</a></li>
</ul>


<h3><a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Sync CompactApplet with plasma-workspace. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=6c5651e8c5e3084bab187bcac6e6c630179c3d9f'>Commit.</a> </li>
<li>Duration is in units, not in theme. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=c867eb4eff26fa14d637fb3685a2662aeaaad4ad'>Commit.</a> </li>
<li>Migrate to ContainmentView. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=4e96374e8a826c45b7ddc7e5ddfc524f6e39be63'>Commit.</a> </li>
<li>Sync with desktop. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=5933a6db4e80e8441e5df090c08bdc9ae81a22b5'>Commit.</a> </li>
<li>Sync config dialog. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=2aee875fdedcbc9025696dfd48e04956a76e95af'>Commit.</a> </li>
<li>Dark colorscheme option for cuttlefish. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=237f61f2cda36493dba96dbe3d22c3048ff88fd3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126559'>#126559</a></li>
<li>Port to CMake automoc. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=1a9e7d8e78ba6fc8221ace8a4b457150bdbe5ccb'>Commit.</a> </li>
<li>CMake: KDevplatform integration fixes. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=616cd9c3cabff9b826fdba935b65b8dc45b05ac0'>Commit.</a> </li>
<li>Write contrast effeect stuff to proper config group. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=e9b6344c7bca71968074cd25f841568656adf96a'>Commit.</a> </li>
<li>Possible to edit the license. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=e15639b75ec8f05575ffa1c3d359cea526193c37'>Commit.</a> </li>
<li>Make license editable. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=c148f9643285198713eb0b87640944e3994c08b2'>Commit.</a> </li>
<li>Adjust spacing and keyboard navigation. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=ad8edf131180eee4205fcfc4e92b2eb48b625bc6'>Commit.</a> </li>
<li>Dd the complementary area. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=2c145e284c3305e5812eb98cd65d058433e17e5b'>Commit.</a> </li>
<li>A checkbox for showing highlightColor. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=f8ffbc1d1b73987b4d0cf217b4b6e033a35e2a6d'>Commit.</a> </li>
<li>Better forms layout. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=b5de6a1fe40033d511d66a5fbdd5aa88d0a7847a'>Commit.</a> </li>
<li>Move out two of the fake controls. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=866f63e75511c2a5856a2adbe9db05f383eb8af2'>Commit.</a> </li>
<li>Bind all the colors. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=67f8de9dc56a6c48ee50ae254229c78accec5e14'>Commit.</a> </li>
<li>Basic colors editor working. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=ec2fb53f3fb5fbd3df9827ebd065281c11623e0b'>Commit.</a> </li>
<li>Stubs for button hover and focus. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=004816af20afff67133ea5fbbb6a01b00f9b212d'>Commit.</a> </li>
<li>A simple color editor. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=c4a3fc555c6ea3cd4518014aa1c860f849f02e22'>Commit.</a> </li>
<li>Ensure the folder doesn't have spaces or uppercase. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=233eebf982ee40264190f4c9b7932a683d868332'>Commit.</a> </li>
<li>Load default theme as default. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=1445a11e4ea14abb65c07d0c328bcb17ba4ac340'>Commit.</a> </li>
<li>Load the default theme by default. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=3e2f76707035096bf5ba1d033fb1c7fe0f4ab65a'>Commit.</a> </li>
<li>Resize the fields when resizing the window. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=64700c20299d9dcbede9f7ef5376fbb3bf1212f5'>Commit.</a> </li>
<li>Inline error message on duplicate names. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=214325a661da411faa239d473537ce501dba5dc2'>Commit.</a> </li>
<li>Fix the theme parameter. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=416a14c5f5ca38b4fdb68f5d8fec0b3dba41a67b'>Commit.</a> </li>
<li>Select new theme after creation. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=30a133709c17cdf9f70a5e402fcbc24685df467c'>Commit.</a> </li>
<li>Edit colors functionality. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=00deead903bef0e2dce24cd7c9b1fd2ba07bdd4e'>Commit.</a> </li>
<li>NewTheme.qml->MetadataEditor. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=a2a84195366c7683692bc4f6511bbf9fe7780d95'>Commit.</a> </li>
<li>Edit metadata functionality. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=bbf0c969477c79c15c4ebb984befdbbf0fc3cb23'>Commit.</a> </li>
<li>On label clicked: imitation of buddy widgets. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=41ef738c7d9460b0646365b34411a4582b1800a6'>Commit.</a> </li>
<li>Copy over the default color file. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=908a0f7ae230cc6c84b3f78275f55c3181201267'>Commit.</a> </li>
<li>Complete metadata in createNewTheme. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=97518feea1a4acb7ef706e23b9ff8507ce626fd4'>Commit.</a> </li>
<li>A rough New theme functionality. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=8257585be323a31411b27c53334d3b4f64178bbd'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[shell] Set FramelessWindowHint on DesktopView by default. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=12631ad5e810b5718ee42e7cdc4a369e076d5e73'>Commit.</a> </li>
<li>Improve plasmashell main. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2efc789f352d874aa2db9829baa52059d53aa54c'>Commit.</a> </li>
<li>Avoid accidentally inserting values into m_proxies. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a00f0a7cc9373655158f63f6298e0715452fba35'>Commit.</a> </li>
<li>Resize xembedsniproxy windows with a config notify and an actual resize. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c8ac2199cf4b1da7778df98c58d9f2ad15c99135'>Commit.</a> </li>
<li>Properly define overrides as suggested by the c++ standard. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96ff1c868e3cd6fa359b9830016e857fcda7e4ae'>Commit.</a> </li>
<li>[KRunner] Fix keep above. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fce16a6e4c2848ac681c4ab5f6f25936a80e02dc'>Commit.</a> </li>
<li>[Wallpaper configuration] Fix height of color button. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=740106b4885d9ec31ce0e3aa54c58c8de2e52ad5'>Commit.</a> </li>
<li>Use QDBusConnectionInterface::serviceOwnerChanged to watch mpris2. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=70326bdbed9b3e2f74990e170cb400f1e6769c8d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127285'>#127285</a></li>
<li>Handle KIO URLs which proxy for local URLs in Icon applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7d60594111ebc075bbd60d3e8d1fcdcdb0f3c139'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356548'>#356548</a></li>
<li>Watch for and notify about XDG_DESKTOP_DIR changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=48f260df95dfb0115d44ab73d0731119bea901e9'>Commit.</a> See bug <a href='https://bugs.kde.org/356548'>#356548</a></li>
<li>[krunner] Make it work on Wayland. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c6bb9347f0090066e5fa37fdbb6a2594408c3ec2'>Commit.</a> </li>
<li>[krunner] Don't call KWindowSystem::setState on every event. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1dbaf2b8c509e57916af368c1e38a5d70214ef7d'>Commit.</a> </li>
<li>[KRunner] Don't override user input in history. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=20b97073bb3f38328479ca613335988e9110d75d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360268'>#360268</a></li>
<li>[Battery Monitor] Just use "battery tooltip icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=abb3183c27cea308676bdb6a56b552a3a3a06dfd'>Commit.</a> See bug <a href='https://bugs.kde.org/360276'>#360276</a></li>
<li>[Battery Monitor] Drop "You don't have any brightness controls" text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=412b6f90402c0d2ccbcf5d8879a582ffaf67e171'>Commit.</a> See bug <a href='https://bugs.kde.org/360276'>#360276</a></li>
<li>[Calendar applet] A bit of cleanup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3bafbfaa08997120c40e8a8ef424f9f10175f697'>Commit.</a> </li>
<li>[Calendar applet] Align data source polling to minute. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a4089f4e36a486278d3fe72a5347a87dcc94ff24'>Commit.</a> </li>
<li>[Calendar applet] Ensure sane sizes for both popup and when on the desktop. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d8a796475d2572d276b4ce48fd48490cbcb4a28a'>Commit.</a> </li>
<li>Use connectedOutputs instead of outputs for ShellCorona::numScreens(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=33e91b85fd65e968c847345f41427fb6ad1cdfab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127274'>#127274</a></li>
<li>Cmake: avoid using pkg_get_variable in FindIsoCodes.cmake. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=09e58f7f33658fec6291213636989c715b32b7e2'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2e03e7a71a2af2bf291103890f5c2d28550498a9'>Commit.</a> </li>
<li>[Baloo Runner] Strip filename from subtext. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5611b9cf2c09e1cfed03c5c4ec3b52161fa3f273'>Commit.</a> </li>
<li>[Baloo Runner] Create QMimeDatabase only once. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cc5ac26687df8b29e6c8c7f9266b2caa1ebae388'>Commit.</a> </li>
<li>[Baloo Runner] Allow opening parent folder. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a18be0a6e61aec32b122a92a4f763e056dc9f644'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340302'>#340302</a></li>
<li>Drkonqi: Fix build with kcoreaddons < 5.20. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4abb289daf5dcc8774affc1c997254169a0d30c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127258'>#127258</a></li>
<li>[Weather Dataengine] port ion plugins away from KDELibs4Support. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6801fa9c2475887f4ca993aba5ae091a181d901b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127252'>#127252</a></li>
<li>Fix showing openGL compatability warning to user. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e22989011f50456068bc5db21915bb7275f38a5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358869'>#358869</a>. Code review <a href='https://git.reviewboard.kde.org/r/127254'>#127254</a></li>
<li>[Weather Dataengine] no module prefixes with includes (& unbreak build). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f85026fafa1c7593e0b3958c578407458e7a6b92'>Commit.</a> </li>
<li>[Weather Dataengine] Remove ion_debianweather, service no longer exists. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=961b588f61bd75b2c73db152f0987e56d284d7d8'>Commit.</a> </li>
<li>[Weather Dataengine] Port ion_bbcukmet to Plasma5 (needs a port of BBC API). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df0844dc562a194cd7750244dd4ef5f5c73f834c'>Commit.</a> </li>
<li>[Weather Dataengine] Port ion_envcan to Plasma5. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d1c4aeecc05f27a5cb803c89270d3688a57761ac'>Commit.</a> </li>
<li>[Icon Widget] Add drop shadow instead of solid background. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=23c82f391e93bd47d3d31a822bf8044f521481a1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127247'>#127247</a></li>
<li>[Icon applet] Enable eliding for text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a8f7c3fb47d7ab2a8780ded16744deaceac5ebbd'>Commit.</a> </li>
<li>Workaround by always reset model when insert and remove from model. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a26adb50c0bfce031df55bf200ebc7f44420301'>Commit.</a> See bug <a href='https://bugs.kde.org/352055'>#352055</a></li>
<li>ShellCorona is responsible for the memory management of views. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d0440dd8203a7bcb52a8d4e2e16c53efa5fa60da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127225'>#127225</a></li>
<li>Emit contextualAppletsAboutToShow in systemtray. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=109f6cfca5ef9c0266a555c6cd68babb3257ce2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358119'>#358119</a>. Code review <a href='https://git.reviewboard.kde.org/r/126811'>#126811</a></li>
<li>[DrKonqi] Avoid duplicate "Help" button. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=75a80e751bc04f6ce04bce500916012fb5b9013f'>Commit.</a> </li>
<li>Avoid blocking DBus calls in SNI startup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=49440a7ce0623d1bc8aca459eaed35612d384cfd'>Commit.</a> See bug <a href='https://bugs.kde.org/359611'>#359611</a>. Code review <a href='https://git.reviewboard.kde.org/r/127199'>#127199</a></li>
<li>[Media Controller] Fix layout when multiple player combo is shown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7eddda8ae84f0716618357fcb8cdaa44143ec417'>Commit.</a> </li>
<li>[Media Controller] Use Plasmoid.onContextualActionsAboutToShow to populate menu. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3429579c07354771731dfac77cb95a6a06d5f1d6'>Commit.</a> </li>
<li>[Media Controller] Add keyboard shortcuts to the applet itself. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df37fc86e507469bbb60889a108469784fd9377f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351146'>#351146</a></li>
<li>Add Frameworks version to system information in bug report. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0fa4ba8479ceb4fe1e139f6d3650e97fe56bbf38'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127032'>#127032</a></li>
<li>Fix logic. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a0151d686ced709eefaf3d067d12cae6aee45cd'>Commit.</a> </li>
<li>[Notifications] Fix tooltip sub text to indicate job and notification count. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=944bfe4617286ba816e64f8b1a91fbd42095517c'>Commit.</a> </li>
<li>Fix warning from 4819282d. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ac608568aac2e754e441880dcd8a738717243255'>Commit.</a> </li>
<li>Make it more QML friendly. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c95edb0defc8094326f909d3a38304efcfdf1f6f'>Commit.</a> </li>
<li>[Image Wallpaper] Make color picker visible for all positioning mode. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=71acecf2e737883cc8f68f4b361e1b3ea9cfe7b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351012'>#351012</a></li>
<li>Sddm theme: Add keyboard layout button. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bea1a457316756ef732aa8315376a2cf82519281'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126930'>#126930</a></li>
<li>Implement rename() in a way that avoids losing custom icon positions. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=49a979d0bf90ac77f2117b2490ee1baeff469620'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359596'>#359596</a></li>
<li>Rss dataengine: blindly ported to KIO::FavIconRequestJob, in case it's re-enabled one day. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e75da55bb85f126fc0424195cea1ee74eba1f8ef'>Commit.</a> </li>
<li>Disable fallback session management (requires Qt >= 5.6.0). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b5e814a7b2867914327c889794b1088027aaafd6'>Commit.</a> </li>
<li>Use PlasmaComponents.Label for analog clock timezone label. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=724bd005b9b3ae0300fdae40483effcd02c192b3'>Commit.</a> </li>
<li>Splashscreen tweak. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a56f6c9d98197541663713978d5a754fa667a15a'>Commit.</a> </li>
<li>Improve purpose description of isocodes package in digital-clock applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a62aae16e9e758a5fcf75f8a7704a03608de4dfc'>Commit.</a> </li>
<li>PanelShadow: Don't remove shadows from destroyed windows. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=88b668f8108a0e9cd368b069f57984cf5107924b'>Commit.</a> </li>
<li>[digital-clock] Use iso-codes for country names. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fea67bd2ae7549e88952ac6dbcf90392d0ee1458'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126879'>#126879</a></li>
<li>[Notifications] Align top notification body text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d24bc77a92871218c00dd32457f4868538937b7f'>Commit.</a> </li>
<li>[KRunner] Add placeholder text to search input. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8ef9db163f6bc92b1b74bb3bc142fac72f016beb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126878'>#126878</a></li>
<li>[Widget Explorer] Take into account the activity a widget is on. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d77e47c0889a95ef94db08369c435550858903ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/341154'>#341154</a></li>
<li>Don't call yourself recursively. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=939ba8f4eb7f993d2dcb9640be8ade58093006d0'>Commit.</a> </li>
<li>[PlasmaAppletItemModel] Don't call yourself recursively. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b979342b7e80ac366f3271163541c2c6280fca37'>Commit.</a> </li>
<li>[DrKonqi] Also close notification when activated. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2bd772c9802ceaac5f62700b987ba1fc2cf5856b'>Commit.</a> </li>
<li>[KRunner] Use time-based sorting rather than length for auto-completion. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=598f7c99e6ba73ba1849891017d2cc00f8413845'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358985'>#358985</a></li>
<li>[shell] Listen for QEvent::PlatformSurface to perform Wayland integration. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=92bf88c68289bab6e0bee3612b9a927f57c53808'>Commit.</a> </li>
<li>Check whether there is any BadWindow error before monitor the event. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=829158f830555c031755c6d4348e684779264342'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358719'>#358719</a>. Code review <a href='https://git.reviewboard.kde.org/r/127014'>#127014</a></li>
<li>Add transparency support for tray icon. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=49977bfb42007f56330a1ff0e9705d06709af0c2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127009'>#127009</a></li>
<li>Use ConfigureNotify instead of xcb_configure_window to change size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1a13806d74973137960e64aa347965525a7c626a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127010'>#127010</a></li>
<li>Small cleanup of KDELibs4Support uses. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a62c8d0b1b7a54cba7278ea4d53e9908412b3977'>Commit.</a> </li>
<li>Drop usage of Qt4Support. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d19b942775f321e64bb01e1ebb0e3f3b5960f4f1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126994'>#126994</a></li>
<li>Add DBusMenuShortcut type overload for QDBusArgument. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=51266c34af7d778f850f41334f5527293680419a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126691'>#126691</a></li>
<li>Animate lock screen initialization. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ce269183ca8e36c89f6247d399bdb40b83aa57a7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126962'>#126962</a></li>
<li>Remove accidental code paste into license header. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6971a29c2f1ceea93399aec35cd5a3e62de199da'>Commit.</a> </li>
<li>Fix build. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5cd3fd845bed6f99e1f95642374c2843595b816b'>Commit.</a> </li>
<li>[DesktopView] Allow Shift modifier for triggering KRunner. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d7c5c99ad05c891e37febd080404173279c718d8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353959'>#353959</a></li>
<li>[Baloo Runner] Add mime data to matches. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=25dab5d872c6b7f27c2f0932888ed77ebfe13fb1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/249621'>#249621</a></li>
<li>Drkonqi : fix build errors and build nongui. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=efdb5e97d975a0f86e42b37e81d69339df5b0e64'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126993'>#126993</a></li>
<li>Restore weather dataengine. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=638897d0432f69db73f152d30f83b06c4ed9657c'>Commit.</a> </li>
<li>[notifications] Make the text selectable in the popup too. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4819282d329f7462fdc457e63fec58ba4afa37d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358981'>#358981</a></li>
<li>Fix the Plasma::Applet::Actions crash. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5a1a411b41f950e97d80ff7c898052fb6d02fb1a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351777'>#351777</a>. Code review <a href='https://git.reviewboard.kde.org/r/126961'>#126961</a></li>
<li>Reset the model on list always shown/hide change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=13ad256cc40a657b54a236eef5c491fdf6b48ffc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357627'>#357627</a>. See bug <a href='https://bugs.kde.org/352055'>#352055</a></li>
<li>[DesktopView] Make Applet/ContainmentConfig transient for DesktopView. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=467565d4556b0481779e3aa0cbce47ed9a919318'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126947'>#126947</a></li>
<li>[Notifications] Fix Job finished persistency. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bec916168c269df8807678c61aeb551cc3aac10e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126954'>#126954</a></li>
<li>[kioslave/remote] fix knetattach .desktop filename. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6267c5a24b710bfd431d676c85277cd316b981dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358723'>#358723</a>. Code review <a href='https://git.reviewboard.kde.org/r/126893'>#126893</a></li>
<li>[kioslave/remote] check return of createWizardEntry. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f278e5c3858b978eebaa538e4a6c7ebc3e451fb0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126892'>#126892</a></li>
<li>LockScreen: Use layout display name in KeyboardLayoutButton. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=39327c21b8b51d3b29f7a01f58200dcf8f5d99bd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126944'>#126944</a></li>
<li>[notifications] Disable icon animation in notification popups. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4c00e6f286a9347b444d8aa446e69f615fb15377'>Commit.</a> </li>
<li>Show controller only if asked about actual panel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a2f519dd6a10ed5b35eeb62ae6daca6eac79b05c'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b99ea1fbfc89403b2d171de4e7f449b5de33e924'>Commit.</a> </li>
<li>Places data engine: Rename model role "index" to "placeIndex". <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8bedfde2894a60dc72a0bd40589dfb900776dace'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126851'>#126851</a></li>
<li>What exactly was meant by keyPressEvent(QMouseEvent *e)? :-). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=827c4c266fb76e62b3dcba1ce46d712eb25491a7'>Commit.</a> </li>
<li>Remove kwallet-pam integration code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=616691b25f58070f81a8881d07c33de7684b4581'>Commit.</a> </li>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3d2d01debcdbe198d774827b4e23094b78d87739'>Commit.</a> </li>
<li>[Device Notifier] Provide inline feedback. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1ed7209f08aa69cec7847c606da8a2fc091dc603'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126688'>#126688</a></li>
<li>Remove old CMake cruft. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cc2b33dcdbcd46ea3ec8f7cafc55d879ab541098'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120566'>#120566</a></li>
<li>Add a version check. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b710f889f040c582ae068db309162c2ad31322a3'>Commit.</a> </li>
<li>Move all the lockscreen ui in a separate file. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=880ece2645c14268a6935be745dbf36fc80fac01'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126837'>#126837</a></li>
<li>[notifications] Add a button tooltip to the expanding button. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d6baf450d3ff4cf87461f0b32eef73d12298cb0a'>Commit.</a> See bug <a href='https://bugs.kde.org/358282'>#358282</a></li>
<li>Don't give a view to containments that don't want. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a8eda763bca716eca0847588454ec08ac8f816f1'>Commit.</a> </li>
<li>Reset seek slider position when changing songs. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=399143457b60dea463ec11aba13f5575439b21c1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126779'>#126779</a>. Fixes bug <a href='https://bugs.kde.org/358135'>#358135</a></li>
<li>[Media Controller] Support multiple players. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f2f252899ec42533170057f41c18062213764002'>Commit.</a> </li>
<li>Don't list systray containment actions in systray context menu. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6725620e4438d7cb429eeded8165a627104178e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125972'>#125972</a></li>
<li>Remove completely duplicated function. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e67f33a8ec2bc61d470493f7a77f0c80ab9e9499'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125973'>#125973</a></li>
<li>Keep disabling Qt's high DPI whilst on 5.6. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3691ab0881c1fe1f4f6610b1f1dfb23f7ecfaac3'>Commit.</a> </li>
<li>Export wallpaper path as a URL. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d78bd07d2919243e2fbd442e97c4f0e4b44ce1c2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351089'>#351089</a></li>
<li>Expose BCP47 language id for system locale as property in the shell scripting interface. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e452cd3ea81f73cb2f6eb750fcfb7eb295b7b65d'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6885d1f4d5ce496642233395d616ecfdb5884621'>Commit.</a> </li>
<li>Matching splash for new wallpaper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=433f2255f8c36231a5194ff87b8d2649077a5025'>Commit.</a> </li>
<li>Add missing connect. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e38c7732ef75f2734ab1517e82d7583f0a949141'>Commit.</a> </li>
<li>[Device Notifier] Use childrenRect.height for height. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f04d3db480b6a0758e15d2d5915497c8c61ec881'>Commit.</a> </li>
<li>Change minimum Size of Calendar. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f6e9a4afabc72b68dcc79bdd6aafa746eefeaec8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126689'>#126689</a></li>
<li>[Device Notifier] Fix alignment of BusyIndicator. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=469004c752128b59cfbbf0bc4595476df0d8f500'>Commit.</a> </li>
<li>[Device Notifier] Simplify (non)removable devices filter. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=28195b85c39a568c8a1c3cba41d341ca015ac5d7'>Commit.</a> </li>
<li>[Device Notifier] Fix typos. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f056cc7af14d66c5a50edf4359de6f4b11831370'>Commit.</a> </li>
<li>[Device Notifier] Rewrite delegates to use Layouts. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9cc524e554b5fc449748e1e36fb33323757c37c3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126673'>#126673</a></li>
<li>Remove legacy session management support. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5f0ca1305db4a925dbdbf927f541497be334feff'>Commit.</a> </li>
<li>SystemTray: Fix height of lines in table in Entries config. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5219f7977e7b0391972c536aa71992f795205228'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126634'>#126634</a></li>
<li>[DrKonqi] Be less intrusive. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=555ff08dc1efaeb7d1f699856347f5e0e9aff1d0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126515'>#126515</a></li>
<li>[digital-clock] Improve clock's tooltip layout. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f5297033c4a0fba416b971f2bdbcb04c56858947'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357005'>#357005</a>. Fixes bug <a href='https://bugs.kde.org/351472'>#351472</a>. See bug <a href='https://bugs.kde.org/357004'>#357004</a>. Code review <a href='https://git.reviewboard.kde.org/r/124047'>#124047</a></li>
<li>[dataengines/notifications] Honor the SkipGrouping attribute. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=81c74b1efbb8a694335c79fecb7a83f3168b0b29'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126460'>#126460</a></li>
<li>[SNI DataEngine] ProtocolVersion is an int. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=36586dacd06fbd2af89c88aa1ea2ea54790608f4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126594'>#126594</a></li>
<li>[Icon Widget] Send service name and icon along when launching Jump List Action. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2f08d3b21d99afb4c6acaa3ddefbaae86e5c91b8'>Commit.</a> </li>
<li>Bump KDE Frameworks requirement. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=25160bac00a28feeac19efd33000223d49c93094'>Commit.</a> </li>
<li>When pressing the "configure" button on a notification, the actual. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=be4b0f5aacb9f6550d4e4d0f7226c9e78acd63b5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126570'>#126570</a></li>
<li>[Icon Widget] Add support for Jump List Actions. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4d88904c4f630ca42d33c2c303de37675fc576d1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126278'>#126278</a></li>
<li>Don't emit signals in another object from panel. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c62452c467e96756a4e5bcaddcee98caa7d27d49'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126576'>#126576</a></li>
<li>Add missing emit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e23a1814b0a11624c4970a890578ddfaf3beea51'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126575'>#126575</a></li>
<li>Don't adjust thickness twice. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ce58e59e471b6a0d5a29d1653fde6b8cd0499300'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126574'>#126574</a></li>
<li>[Media Controller] Load album art asynchronously. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bba0910933dd9f56f09810ebb3642b4f2f6f9e0a'>Commit.</a> </li>
<li>[Media Controller] Drop album art placeholder. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8fd4459a49d3e4ff4b6985a503f1649c5e0dd8c9'>Commit.</a> </li>
<li>Don't rely graphic objects in panel script engine. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0dc7f6c3e5d62f6d8211e5d024ee3080b2baae37'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355918'>#355918</a>. Code review <a href='https://git.reviewboard.kde.org/r/125921'>#125921</a></li>
<li>Don't duplicate a tonne of KCrash. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3ff5cdd3cf75c7d9d7edd72d88dd1c440fb1995d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126538'>#126538</a></li>
<li>Destroy the container window we create. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=60c84bd7cdb07cd46b294ff4e03034a151fe109f'>Commit.</a> </li>
<li>Fix errant logical-or in favor of bitwise-or. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4c16c5d533a919232d90d496ffcef663ce22c7ca'>Commit.</a> </li>
<li>Wrongly git rm kioslave/desktop files. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6b2b0cf9580b7ef2bcfc213f990fb156489f87ae'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126298'>#126298</a></li>
<li>Add HiddenStatus to systemtray for self-hiding plasmoid. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=30aecb47feb87ba17f4871ee4ab9c12ad8551e1e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126298'>#126298</a></li>
<li>Fix i18n. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=55d2efcfafc4b07e8076b815122647d78bb5b36e'>Commit.</a> </li>
<li>In manual sorting mode, always allow sorting of launchers, even if they're separated from tasks. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6a871a014e577555dd579104624fd097c4236107'>Commit.</a> </li>
<li>Do not produce negative struts on switching screens. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0059d87b6f74f950b2aac94763c2934ec710c6c4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126216'>#126216</a></li>
<li>Make geolocation engine build again when networkmanager-qt is there. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d4a3f0906d4baf89977b3819e7595c65171da740'>Commit.</a> </li>
<li>Make networkmanager-qt an optional dependency. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=de5ed6c4cdded1f2ba2ce43022be1d1a9b475bf0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126104'>#126104</a></li>
<li>Set dialogs location according to containments. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1667eaf6c82a6bd727c41c40795087f2f4fac66e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349887'>#349887</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=38facbd47536ca811d7c879fba84a7429d797ae1'>Commit.</a> </li>
<li>Disable ptrace. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=0b9a327bba3da0dab3c2962a486aaaaafbaa8264'>Commit.</a> </li>
<li>Remove unused dependencies. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=7665ca02a536faf6c2bb643b60c9060d54896889'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126272'>#126272</a></li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Don't consider Unknown output to be an external monitor. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=d3162725cfffb79da7bb8f276a31a915e3349dab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126721'>#126721</a>. See bug <a href='https://bugs.kde.org/357868'>#357868</a></li>
<li>[Activity Settings] Hide "Act like" setting if there's no batteries. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=1cf565eeb3ddc98a2df0066b4e5258aaf1e25008'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359535'>#359535</a></li>
<li>Add aboutToSuspend signal. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=ea9ad6cbd5f7d82f6323b905e9dee25f094fcb63'>Commit.</a> </li>
<li>Enable animated brightness change also for Sysfs helper. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=e3275e994d9b4e2b1676d90a07faed49a445792f'>Commit.</a> </li>
<li>Don't force reload profile when battery status changes. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=f5449a05a6e7fda8a00ace21bc7401ed33712a80'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127160'>#127160</a></li>
<li>Drop usage of deprecated Qt API. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=2c6a98d2f5922288ce8526eacf9492acc9b2fbff'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=dc76062a0616d1b64b516e4132cf9613d306770d'>Commit.</a> </li>
<li>Wait 5s before enforcing an inhibition. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=eca79138c15575f6f523a8680919b407f84da2e2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126145'>#126145</a>. Fixes bug <a href='https://bugs.kde.org/352235'>#352235</a></li>
<li>Add a couple of QLatin1Strings. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=525d5458ab7aee7948ab9c94ae3f683714fe0a90'>Commit.</a> </li>
<li>Add missing initializations (using Q_NULLPTR for pointers), minor string optimizations. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=5b0faeb67e02edb22bf79699c4ef2217f248a945'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126127'>#126127</a></li>
</ul>


<h3><a name='sddm-kcm' href='http://quickgit.kde.org/?p=sddm-kcm.git'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Remove * 1. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=a208c02d99b65e9412a8e9e75efb7420b15bd0d9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126730'>#126730</a></li>
<li>Cleanup sddm kcm from additional information. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=1ac26510cedba5696305d258ba2f92e0dddf3f91'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126730'>#126730</a></li>
<li>Update the sddm kcm style. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=0c200a345d262b7f7094e0e4e6009c06c60c6281'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126629'>#126629</a></li>
<li>Fix compilation with Qt5.6. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=053890d1d5a201e646b222749ba9c5baf7c96e38'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='http://quickgit.kde.org/?p=systemsettings.git'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://quickgit.kde.org/?p=systemsettings.git&amp;a=commit&amp;h=4054cc84b647ab5e4b60d4f2b38155a9baca90e0'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
