<?php
  $page_title = "New Kontact Suite Brings Next-Gen Groupware to Desktop and Mobile";
  $site_root = "../";
  include "header.inc";
?>

<p align="justify">
Also available in 
<a href="announce-4.6.4-ru.php">Русский</a>
<!-- // Boilerplate -->
</p>
<p align="justify">
June 10, 2011. KDE is proud to announce the release of the next generation Kontact Suite, based on the <a href="http://community.kde.org/KDE_PIM/Akonadi">Akonadi</a> framework. In addition to this, we are also proud to announce the June maintenance update of the KDE Software Compilation 4.6. Unsurprisingly, the port of Kontact to Akonadi is finally being released the same day as Duke Nukem Forever, making it relatively timely.
</p>

<h3>Known Kontact Receives More Powerful Foundation</h3>
<p align="justify">
<div style="display: inline; float: right; align: right;"><img src="4.6/akonadi.png" align="center" width="140" alt="Akonadi - The Personal Information Storage Service " title="Akonadi - The Personal Information Storage Service " /></div>
KDE’s Kontact Suite – a set of Personal Information Management applications – is receiving a major architectural boost.  The team has invested years of development in its new infrastructure layer, Akonadi, and in porting Kontact to the new foundation while keeping the familiar user experience.<br />
The new Kontact Suite 4.6 is a major milestone for KDE software to provide outstanding features for a wide variety of devices, from traditional desktops to the latest mobile platforms. Using the capabilities of the new scalable <a href="http://community.kde.org/KDE_PIM/Akonadi">Akonadi</a> groupware framework to build interconnected PIM-related applications, the new Kontact Suite is available in versions for desktop systems as well as touchscreen devices, such as tablets and smartphones.
</p>

<p align="justify">
Users who upgrade to the new Kontact Suite version will still recognise the applications. While the underlying internals have been updated, changes in the user interface have been kept to a minimum. Among the most noticable improvements are faster email notifications, vastly improved performance for IMAP email accounts, and interoperability with other applications that consume contacts, calendars and other groupware-related information.<br />
For most users, the upgrade should be seamless, as Kontact 2 automatically imports accounts and underlying data into Akonadi, leaving the old configuration and data in place in case a rollback is ever needed.
</p>
<p align="justify">
A wide variety of groupware servers are already supported natively by Akonadi, such as the <a href="http://www.kolab.org">Kolab</a> groupware, and backwards-compatibility with existing groupware servers is guaranteed through a bridge solution allowing legacy KDE PIM resources to be used by Akonadi.
</p>
<h3>Kontact Touch: Highly Secure Email and Groupware for Mobiles</h3>
Kontact Touch brings the powerful Kontact groupware client to mobile devices too. <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a> (<a href="http://userbase.kde.org/Kontact_Touch/Screenshots">screenshots</a>) provides a similar featureset to the desktop also on mobile devices, including email, calendaring, and address book. Kontact Touch makes on-the-move reading and writing of signed and encrypted emails as easy as possible, supporting multiple cryptographic technologies such as OpenPGP and S/MIME. Kontact Touch provides off-line support for all data, and access to the full set of PIM resources known from the desktop client. More information about Kontact Touch can be found in <a href="http://dot.kde.org/2010/06/10/kde-pim-goes-mobile">this article</a>.
</p>

<div align="center">
<iframe width="560" height="349" src="http://www.youtube.com/embed/SsWnfny61oI" frameborder="0" allowfullscreen></iframe>
<br />
<em>Kontact Touch Demo Video</em>
</div>

<h2>KDE SC Updates to 4.6.4</h2>
<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the fourth in a series of monthly stabilization updates to the 4.6 series. 4.6.4 updates bring many bugfixes and translation updates on top of the latest edition in the 4.6 series and are recommended updates for everyone running 4.6.3 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
To  download source code or packages to install go to the <a href="/info/4.6.4.php">4.6.4 Info Page</a>. The <a href="http://www.kde.org/announcements/changelogs/changelog4_6_3to4_6_4.php">changelog</a> lists more, but not all improvements since 4.6.3.
Note that the changelog is incomplete. For a complete list of changes that went into 4.6.4, you can browse the Subversion and Git logs. 4.6.4 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.6, please refer to the 4.6.0 release notes and its earlier versions.
</p>

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.6.4/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing 4.6.4 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.6.4.php#binary">4.6.4 Info
Page</a>.
</p>

<h4>
  Compiling 4.6.4
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for 4.6.4 may be <a
href="http://download.kde.org/stable/4.6.4/src/">freely downloaded</a>.
Instructions on compiling and installing 4.6.4
  are available from the <a href="/info/4.6.4.php">4.6.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
