<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12.7 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.12.7";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Fix scrollbars in LibreOffice. <a href='https://commits.kde.org/breeze-gtk/43f2cb7c2334f5438d20ebf1ac04432a18ce5edf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389709'>#389709</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15110'>D15110</a></li>
<li>[kconf_update] Add "gtk-primary-button-warps-slider=0" to generated GTK3 config. <a href='https://commits.kde.org/breeze-gtk/f5121ba89abf9eb50b42ab9c95f1f4c7e57bd044'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379773'>#379773</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14863'>D14863</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fix showing update notification. <a href='https://commits.kde.org/discover/8b2cf5d81a6133445e0bf838a7f6996391c5be8f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396426'>#396426</a>. Fixes bug <a href='https://bugs.kde.org/390322'>#390322</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15682'>D15682</a></li>
<li>Don't try to trigger an empty transaction. <a href='https://commits.kde.org/discover/31ea5cc11526d454e880337cc904ad6361d7eac4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397759'>#397759</a></li>
<li>PK: wait until we have fetched the packages that dont' come from appstream. <a href='https://commits.kde.org/discover/2155003d054e75d173307360c81fea41890b590f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397192'>#397192</a></li>
<li>Fix navigation after search. <a href='https://commits.kde.org/discover/e41df0e3b0b836c5dea3f0988ba713711127582b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391931'>#391931</a></li>
<li>Remove wrong assert. <a href='https://commits.kde.org/discover/310685b3db21d03aeca91c6df1753a2f9851287f'>Commit.</a> </li>
<li>KNS: Fix leak of resource ratings. <a href='https://commits.kde.org/discover/48b67cf568c6d427c6038fe763a3018b5b12d7c2'>Commit.</a> </li>
<li>Prevent crash. <a href='https://commits.kde.org/discover/ec234e949f5c4b1a41ac926c8e3128cb4a463324'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396564'>#396564</a></li>
<li>Readability. <a href='https://commits.kde.org/discover/1ce242f4a8211be7ae8f9e2d5cd560f64b698038'>Commit.</a> </li>
<li>Fix CMakeLists.txt to work with older versions of CMake. <a href='https://commits.kde.org/discover/d8ac72b45e9739fd6e1ba8a7d66f6cff197436cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14112'>D14112</a></li>
<li>Fix warning. <a href='https://commits.kde.org/discover/37c3f8683f65b31df4c1a0338c6eef0fc2079b62'>Commit.</a> </li>
<li>Silence false positive warning on --mode. <a href='https://commits.kde.org/discover/e43a191d0e0175f228de7ebef50a0f00e02941ae'>Commit.</a> </li>
<li>Proper status when removing snaps. <a href='https://commits.kde.org/discover/fa66dbf50b06b75a8bf41a6683212f60010c8f71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396351'>#396351</a></li>
<li>Properly check for pkqt. <a href='https://commits.kde.org/discover/470b3e232be2ae22da5a959f8c0f2bc3ad041a1c'>Commit.</a> </li>
<li>Use consistent spacing in the missing backends footer. <a href='https://commits.kde.org/discover/67bd517d3a563a92c2725dff9d1dd169f010ba22'>Commit.</a> </li>
<li>PK: Make sure we resolve the transaction application if it fails. <a href='https://commits.kde.org/discover/d4ed6e5ed987050d59b2b006b2cac6c765905b9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395994'>#395994</a></li>
<li>KNS: don't start searches before initializing. <a href='https://commits.kde.org/discover/11a05813b6e558f52459f98a4d1d15280c9ee842'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395966'>#395966</a></li>
<li>Report a progressing change when we finish to set up and there's nothing pending. <a href='https://commits.kde.org/discover/26dee5442da203f3f73b5292c50100bd21b76fae'>Commit.</a> </li>
<li>Recheck fetching after we remove a backend. <a href='https://commits.kde.org/discover/a42b130357bcd26ce8a16f5231cdd2552cd93371'>Commit.</a> </li>
<li>Flatpak: don't crash if we get null updates. <a href='https://commits.kde.org/discover/99c67ded1cf1f86ddd8f13a96a93c9168e351029'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394713'>#394713</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[POTD] Actually update the image every day. <a href='https://commits.kde.org/kdeplasma-addons/81e89f1ea830f278fdb6f086baa4741c9606892f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397914'>#397914</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15124'>D15124</a></li>
<li>[dict applet] Fix translation catalog name to match pluginid. <a href='https://commits.kde.org/kdeplasma-addons/076539340f5fb17c2836e3d9029980abb593ba4d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14398'>D14398</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Fix file indexer monitor Exec line. <a href='https://commits.kde.org/kinfocenter/11604cdf65d2d9d057af170fd6be2711750a9350'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396977'>#396977</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Fix preview widget appearing to scale twice. <a href='https://commits.kde.org/kscreen/5c45d27f00b4207d5db347350a81e8ecb6bdb108'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388218'>#388218</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14338'>D14338</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Prevent paste in screen locker. <a href='https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388049'>#388049</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14924'>D14924</a></li>
<li>Load QtQuickSettings for software rendering. <a href='https://commits.kde.org/kscreenlocker/c25251a7eb051c7e6914e188f39773d654cb7358'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14708'>D14708</a></li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Don't create salt file if user home directory does not exist. <a href='https://commits.kde.org/kwallet-pam/3dc73747c16f7d732e1d47a634c9e1875c3f4bfe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12909'>D12909</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Avoid global static for effects. <a href='https://commits.kde.org/kwin/3cfb7a30f00517e2d3a43dd422c7e72c749ea911'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15163'>D15163</a></li>
<li>[effects] Use more effectData() in BuiltInEffects. <a href='https://commits.kde.org/kwin/fa5242b3eefd02683d98b773f1c64da6c4379fbe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13587'>D13587</a></li>
<li>[kcmkwin] Use QtQuick.Controls 2.0 for Label and TextField. <a href='https://commits.kde.org/kwin/22d898399b38cadfd72474a2b50f1d41db7bd28e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366451'>#366451</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14495'>D14495</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Revert "Fix uninstalled run unit tests: set LIBRARY_OUTPUT_DIRECTORY for plugins". <a href='https://commits.kde.org/libkscreen/2ae23e70e988d617a576c7db48301b2d950b03c8'>Commit.</a> </li>
<li>Fix uninstalled run unit tests: set LIBRARY_OUTPUT_DIRECTORY for plugins. <a href='https://commits.kde.org/libkscreen/4c0b960bce2ce32020d17547950c051898b464b3'>Commit.</a> </li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Fixed drag pixmap not showing when dragging results off KRunner's window. <a href='https://commits.kde.org/milou/816a923f7c05047391834fc115c5da25563978f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14545'>D14545</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Improve arrow key navigation of Kicker search results. <a href='https://commits.kde.org/plasma-desktop/1692ae244bc5229df78df2d5ba2e76418362cb50'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397779'>#397779</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15286'>D15286</a></li>
<li>Don't use older buggier versions of AppStream Qt. <a href='https://commits.kde.org/plasma-desktop/b96651acdca4bb093bc7fcb23eb80456942ab6cc'>Commit.</a> See bug <a href='https://bugs.kde.org/391468'>#391468</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15263'>D15263</a></li>
<li>Revert "Fix bad model hygiene in Positioner::move()". <a href='https://commits.kde.org/plasma-desktop/f23b9dc0d57df80863e988208e60135a7f42ad12'>Commit.</a> </li>
<li>[Style KCM] Use "configure" icon. <a href='https://commits.kde.org/plasma-desktop/8ed77e53af334ab22834cc4a4740cdb9e46d99f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397358'>#397358</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14746'>D14746</a></li>
<li>Fontinst quits after KJob is done. <a href='https://commits.kde.org/plasma-desktop/690570a4cefd786db5113ca237e9bdb48cd50812'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379524'>#379524</a>. Fixes bug <a href='https://bugs.kde.org/379324'>#379324</a>. Fixes bug <a href='https://bugs.kde.org/349673'>#349673</a>. Fixes bug <a href='https://bugs.kde.org/361960'>#361960</a>. Fixes bug <a href='https://bugs.kde.org/392267'>#392267</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14493'>D14493</a></li>
<li>[Desktop Toolbox] Emit contextualActionsAboutToShow before opening. <a href='https://commits.kde.org/plasma-desktop/3485b7d30efa2fadf695391bd7947aac0cded819'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384862'>#384862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14535'>D14535</a></li>
<li>Fix bad model hygiene in Positioner::move(). <a href='https://commits.kde.org/plasma-desktop/1cb71a2cca8a46512f5ac0cb03da268dde8d9c7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396666'>#396666</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14243'>D14243</a></li>
<li>[Folder View] Improve file name text rendering. <a href='https://commits.kde.org/plasma-desktop/5fca2b32ce5bb04fd6ce614c979c7665db84a4a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14234'>D14234</a></li>
<li>Give file and folder names a bit more room in Folder View. <a href='https://commits.kde.org/plasma-desktop/02dadc40efb4fbe7425902fc236b06ca729d848b'>Commit.</a> See bug <a href='https://bugs.kde.org/379432'>#379432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11358'>D11358</a></li>
<li>[Kicker] Use KFilePlaces::convertedUrl for ComputerModel. <a href='https://commits.kde.org/plasma-desktop/3fe808ef8e9763edd5c349f8556b7ee2c0645702'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363337'>#363337</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14029'>D14029</a></li>
<li>Fix crash on post-initial refresh(). <a href='https://commits.kde.org/plasma-desktop/e1252c6e40ac540519e5088111e2f077546ada30'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13856'>D13856</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Fix QFileDialog not remembering the last visited directory. <a href='https://commits.kde.org/plasma-integration/4848bec177b2527662098f97aa745bb839bc15e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14437'>D14437</a></li>
<li>[QDBusMenuBar] Guard m_window with a QPointer. <a href='https://commits.kde.org/plasma-integration/73eeda3a7dfb3b155a6198ff733e5ab2f1a89f0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376340'>#376340</a>. Fixes bug <a href='https://bugs.kde.org/379719'>#379719</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13774'>D13774</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix ampersand and other characters in tooltip. <a href='https://commits.kde.org/plasma-nm/9e4db88fe6010b593cbcde433044aa5b20b1de68'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398225'>#398225</a></li>
<li>Make sure we change icon on NM connectivity change. <a href='https://commits.kde.org/plasma-nm/0491738ced86a970c4ac75057da2b8b4eb350cdd'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Media Player] Change int to double for positions. <a href='https://commits.kde.org/plasma-workspace/1bb02b98cfedfd6b51ac8de0c34a5778659433f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397591'>#397591</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15311'>D15311</a></li>
<li>[Solid Device Engine] Don't update free space info for inaccessible storage devices. <a href='https://commits.kde.org/plasma-workspace/e29ac5c3ff38f48d8b48bddf2e308dcc68dc9ba8'>Commit.</a> See bug <a href='https://bugs.kde.org/355258'>#355258</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14532'>D14532</a></li>
<li>[Notifications Engine] Guard "this". <a href='https://commits.kde.org/plasma-workspace/7a98c7d5e58a3afef1f85eafa92aed5ce1f30de9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397105'>#397105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14651'>D14651</a></li>
<li>[Service Runner] Look up relative entryPaths. <a href='https://commits.kde.org/plasma-workspace/cf7381d7fbff0c966b93b7c2df4cd748011689e2'>Commit.</a> See bug <a href='https://bugs.kde.org/397070'>#397070</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14546'>D14546</a></li>
<li>[Desktop Scripting ConfigGroup] Add more nullptr checks. <a href='https://commits.kde.org/plasma-workspace/79ac08108966f1e978203de71149861a18d6bae8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14576'>D14576</a></li>
<li>Fix blocky text on splash screen when using non-integer scale factor. <a href='https://commits.kde.org/plasma-workspace/1467427b6569f07f9496e049351a48c2c09ff2b5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14347'>D14347</a></li>
<li>[Notifications Engine] Never group notifications with URLs. <a href='https://commits.kde.org/plasma-workspace/c221f6315808a5c1287dcb095ad5043e49e9e4c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396741'>#396741</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14287'>D14287</a></li>
<li>Don't block startplasma sending DBus call to KSplash. <a href='https://commits.kde.org/plasma-workspace/0470689f03de1a0b4ef0ba9eb21be81cf34596b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13863'>D13863</a></li>
<li>[Places Runner] Add place category as subtext. <a href='https://commits.kde.org/plasma-workspace/f896fb554e8e52ca47216496f6828acc2b38e36a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13844'>D13844</a></li>
<li>[Places Runner] Fix opening search and timeline URLs. <a href='https://commits.kde.org/plasma-workspace/5a951e9a7eb8bdbf8530e7e8e24a26fda8603ebc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13841'>D13841</a></li>
<li>[Places Runner] Fix opening devices. <a href='https://commits.kde.org/plasma-workspace/12b3fb2e3b8165e1e15a1d501507bdf873b1dd98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13840'>D13840</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Remove empty Category from desktop file. <a href='https://commits.kde.org/polkit-kde-agent-1/f2c5c6d26fd9f90bc413a0f407d675189a648020'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Workaround QTBUG-47066 to fix crash on startup. <a href='https://commits.kde.org/sddm-kcm/a21dd3486b12a5aceffc58be81b819490be79089'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15498'>D15498</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Use correct settings icon for Configure action. <a href='https://commits.kde.org/systemsettings/e3f05daae1dd51408b56a7c29fbb6dc605f4efee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397888'>#397888</a></li>
<li>Fix sidebar search field with fractional scale factors. <a href='https://commits.kde.org/systemsettings/1cb8b2af21df94763f44cce757e84785ebf2d7ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14256'>D14256</a></li>
<li>Hide view selection if there is only one plugin to choose. <a href='https://commits.kde.org/systemsettings/86f0f92caf0f28fdce3df9e30fc22a67738f92ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374865'>#374865</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14247'>D14247</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
