<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.11.2 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.11.2";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Include the progress of transactions. <a href='https://commits.kde.org/discover/44f03fc0fbcae5767d0d50fe1d20175614523266'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384038'>#384038</a></li>
<li>Fix adding remote sources. <a href='https://commits.kde.org/discover/57c62928e8d39b74f3c75e129e48c6449a680419'>Commit.</a> </li>
<li>List items should be lighter than normal background. <a href='https://commits.kde.org/discover/b17e2780e7c601bba22203e8d7475718c78b16d7'>Commit.</a> </li>
<li>Use QQC2.Popup instead of Kirigami.OverlaySheet for screenshots. <a href='https://commits.kde.org/discover/3e0e3ce8db656595062279ad4792effcd1186ffd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385115'>#385115</a></li>
<li>Don't open a null application from the progress view (update transaction). <a href='https://commits.kde.org/discover/061360f1e6552709a5b3b13da043bf6326a72ffa'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Properly access the system's GTK settings. <a href='https://commits.kde.org/kde-gtk-config/efa8c4df5b567d382317bd6f375cd1763737ff95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382291'>#382291</a>.</li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Dictionary Engine: fix synchronization issues. <a href='https://commits.kde.org/kdeplasma-addons/02d8fbfb19285b09126d819e7da0c315e3b28d68'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8320'>D8320</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Default X font DPI to 96 on wayland. <a href='https://commits.kde.org/plasma-desktop/fae658ae90bf855b391061a5332a1a964045e914'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/8287'>8287</a></li>
<li>Kcm baloo: Fix extraction of folder basename for error message. <a href='https://commits.kde.org/plasma-desktop/5a8691900fea2b77ae194f509d17e7368235b4c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8325'>D8325</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Do not load initially selected connection twice. <a href='https://commits.kde.org/plasma-nm/6c57f972c20580e9c79714cc2ded7ae8f4d7307e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379756'>#379756</a></li>
<li>Fix focus in DNS servers and DNS domains dialogs. <a href='https://commits.kde.org/plasma-nm/13a513b55eb467f9f1d05945c95d45ecf88e4d86'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385839'>#385839</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Sync xwayland DPI font to wayland dpi. <a href='https://commits.kde.org/plasma-workspace/a09ddff824a076b395fc7bfa801f81eaf2b8ea42'>Commit.</a> </li>
<li>Dict Engine: various cleanups. <a href='https://commits.kde.org/plasma-workspace/51d8b5a1d0694afeeb7ac2f397c165ea820f5273'>Commit.</a> </li>
<li>Dict Engine: improve error handling. <a href='https://commits.kde.org/plasma-workspace/c2cf81f26bfebf8c11fe5bfd6695410369ca9519'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8419'>D8419</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Fix colours not updating in systemsettings. <a href='https://commits.kde.org/systemsettings/5f9243a8bb9f7dccc60fc1514a866095c22801b8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8399'>D8399</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
