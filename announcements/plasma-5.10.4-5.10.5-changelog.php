<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.10.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.10.5";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Auto generate desktop file for url handling. <a href='https://commits.kde.org/discover/b433b503016e5028e1c6daf752fe2a6c09b23b04'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7343'>D7343</a></li>
<li>Remove the navigationEnabled concept. <a href='https://commits.kde.org/discover/37cfd9b58e3bf1853e03f799e536e30ce755207c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383294'>#383294</a></li>
<li>Fix license, comply with our naming scheme. <a href='https://commits.kde.org/discover/217316656466f2b322116c23ac8da9e546b4d119'>Commit.</a> </li>
<li>Don't disable the updates action when there are no updates. <a href='https://commits.kde.org/discover/e28b98bb945b1067f64632159c91c2b0113b9c8d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383294'>#383294</a></li>
<li>Some elements aren't reporting a big preview. <a href='https://commits.kde.org/discover/036bc1d10dc2c52d9226507a1f9c0c611dcb51dd'>Commit.</a> </li>
<li>Fallback to the package icon if there isn't a stock icon as well. <a href='https://commits.kde.org/discover/98875579c1474acdf234dc10cd05df499aaf2f30'>Commit.</a> </li>
<li>Prevent memory leak. <a href='https://commits.kde.org/discover/a77b6089724ffa8ebcb9d4083fc6f6bdda0691dc'>Commit.</a> </li>
<li>Make sure we don't crash when opening flatpakrepo files. <a href='https://commits.kde.org/discover/01e9868e32a3ca71fde3ac8be13e5f49a62ee907'>Commit.</a> </li>
<li>Make it possible to prevent some resources from being Reviewed. <a href='https://commits.kde.org/discover/a33e952d4f91a02a01e4211f5e02f38d6198c89e'>Commit.</a> See bug <a href='https://bugs.kde.org/383036'>#383036</a></li>
<li>Don't show the version if there's none on the ApplicationPage. <a href='https://commits.kde.org/discover/0a39fce84ac421d5875d7a182bc44394389a0457'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383036'>#383036</a></li>
<li>Make sure we don't render HTML on the Discover page header. <a href='https://commits.kde.org/discover/a32130def58d6acc26093abaf32e2b60c502047d'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Update comic KNS providers to https. <a href='https://commits.kde.org/kdeplasma-addons/351b46a1852f4a003b41fdaabf12f736d8111ed5'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Make sure the dbus xml interface file exists before it's used. <a href='https://commits.kde.org/khotkeys/db3a04289d0f33285c3ca2d8fc05fc5bb45d608f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6792'>D6792</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Print plugin name in case of a loading error. <a href='https://commits.kde.org/kwin/bf2d7395880e626eee46017056bc352d90e2013f'>Commit.</a> </li>
<li>Update KNS providers to https. <a href='https://commits.kde.org/kwin/308ab764286090326ffe8304acaf50afa7e74b16'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a></li>
<li>Call Platform::setupActionForGlobalAccel on the Client shortcut. <a href='https://commits.kde.org/kwin/4c996a57d4c01f092f9ed8f98a9f476c14c0c777'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6802'>D6802</a></li>
<li>[effects] Exclude OSD windows from desktop grid. <a href='https://commits.kde.org/kwin/cf62ac8039a5821e52c6fc8e6a7d45a38f350dd2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376987'>#376987</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6835'>D6835</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Set focus in this FocusScope. <a href='https://commits.kde.org/plasma-desktop/001c89d18ab737dd48451c73137004134fdc51b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383667'>#383667</a></li>
<li>Compress size hint changes before releasing position and repositioning item. <a href='https://commits.kde.org/plasma-desktop/3e3f0decc98304949c1f7cbb196bdf0686e75a08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382759'>#382759</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7385'>D7385</a></li>
<li>Update size hints less often. <a href='https://commits.kde.org/plasma-desktop/b8c1e0e3e060ff415f04b50cd5b7bcfbb8890dea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7384'>D7384</a></li>
<li>Disable bouncy scrolling. <a href='https://commits.kde.org/plasma-desktop/49fb504149e3ea3690616510ee313d33c7233d53'>Commit.</a> See bug <a href='https://bugs.kde.org/378042'>#378042</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7322'>D7322</a></li>
<li>[Folder View] Don't process mime data if immutable. <a href='https://commits.kde.org/plasma-desktop/d4bd4bfaf7013b92c1caf4c65ee4c6d3d6b65d25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383371'>#383371</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7235'>D7235</a></li>
<li>Fix group dialog no longer resizing/closing as windows are closed and the group is dissolved. <a href='https://commits.kde.org/plasma-desktop/a1e2ddd67a1226aa796c70fdfce3a8cf81a3f2e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382638'>#382638</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7035'>D7035</a></li>
<li>Prefer using https for kns providers. <a href='https://commits.kde.org/plasma-desktop/d6da5e89338afa23aba050fedfbb149b5a04bb26'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a></li>
<li>Kimpanel: Reduce the inputpanel flicker. <a href='https://commits.kde.org/plasma-desktop/64f9791cce2c8b5630a97cf37e3885328dec0575'>Commit.</a> </li>
<li>Kimpanel: backport two commit not on 5.10 branch. <a href='https://commits.kde.org/plasma-desktop/4b9e4b651796a31cdaf1091f7c7a0cd1759c5197'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix QSortFilterProxyModelPrivate::updateChildrenMapping crash in libtaskmanager. <a href='https://commits.kde.org/plasma-workspace/d2f722a82ebeb213a89efc209ec726a8188de6f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381006'>#381006</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7139'>D7139</a></li>
<li>[Notifications] Clean up States. <a href='https://commits.kde.org/plasma-workspace/8f435a2dbde4c8f69800ed27878d89be0dfc8356'>Commit.</a> See bug <a href='https://bugs.kde.org/381105'>#381105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7319'>D7319</a></li>
<li>Don't change fillMode of image just before deleting it. <a href='https://commits.kde.org/plasma-workspace/a44d84ef47492ca60ee608996b5ab1f2849ef16e'>Commit.</a> See bug <a href='https://bugs.kde.org/381105'>#381105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7248'>D7248</a></li>
<li>Disable kuiserver debug output by default; make it appear in kdebugsettings. <a href='https://commits.kde.org/plasma-workspace/c67769e0963aef485b8e0d738ce67e63c532612c'>Commit.</a> </li>
<li>[Battery Monitor] Fix icon size for InhibitionHint. <a href='https://commits.kde.org/plasma-workspace/03f9ab0f9149050541d2852f94b508f746aac955'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383213'>#383213</a></li>
<li>[Notifications] Improve mouse handling. <a href='https://commits.kde.org/plasma-workspace/7e2a29b0b18abe31df68c2f176124acfbc15c438'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382263'>#382263</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7029'>D7029</a></li>
<li>Use correct easing type values. <a href='https://commits.kde.org/plasma-workspace/1511e486ae46789a34506461f2847d4a37f605cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382834'>#382834</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6970'>D6970</a></li>
<li>Prefer using https for kns providers. <a href='https://commits.kde.org/plasma-workspace/ae943198bf74d563adcb1f3d36ee4ba1b7b274a9'>Commit.</a> See bug <a href='https://bugs.kde.org/382820'>#382820</a></li>
<li>Improve unittest for renaming desktop icons. <a href='https://commits.kde.org/plasma-workspace/31bbfe5987698d39e806087e559e74b5fab70ed6'>Commit.</a> See bug <a href='https://bugs.kde.org/382341'>#382341</a></li>
<li>[AppMenu Applet] Pass "ctx" as context instead of "this". <a href='https://commits.kde.org/plasma-workspace/3ff8c1f65ebeb5c0b7c2d2af59c9330558fcdb5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382386'>#382386</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6951'>D6951</a></li>
<li>[Klipper ModelTest] Fix build. <a href='https://commits.kde.org/plasma-workspace/94f943b3ece7a5b14f9377ec5b3c43b55e9ac1c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6874'>D6874</a></li>
<li>[TasksModel] Use LauncherUrlWithoutIcon in move method. <a href='https://commits.kde.org/plasma-workspace/cc2f28ece16a8e83703ca0d1a074410f62d1c130'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6872'>D6872</a></li>
<li>[LauncherTasksModel] Emit dataChanged for LauncherUrlWithoutIcon on sycoca change. <a href='https://commits.kde.org/plasma-workspace/5bac69e51657dd3254d3aaf3cd6a543cd3079401'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6869'>D6869</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Fix implicit QString->QUrl conversion. <a href='https://commits.kde.org/plymouth-kcm/ee98d5bcc86188f69bda14ef295458c09ec10402'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
