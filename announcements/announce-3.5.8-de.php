<?php

  $page_title = "KDE 3.5.8 Pressemitteilung";
  $site_root = "../";
  include "header.inc";
?>

<p>ZUR SOFORTIGEN VERÖFFENTLICHUNG BESTIMMT</p>

<!-- Übersetzungen in andere Sprachen -->
Auch verfügbar auf:
<a href="announce-3.5.8.php">Englisch</a>
<a href="http://www.kdecn.org/announcements/announce-3.5.8.php">Chinesisch (vereinfacht)</a>
<a href="announce-3.5.8-ca.php">Katalanisch</a>
<a href="announce-3.5.8-es.php">Spanisch</a>
<a href="http://fr.kde.org/announcements/announce-3.5.8.php">Französisch</a>
<a href="announce-3.5.8-it.php">Italienisch</a>
<a href="announce-3.5.8-pt.php">Portugiesisch</a>
<a href="announce-3.5.8-sl.php">Slowenisch</a>
<a href="announce-3.5.8-sv.php">Schwedisch</a>

        <h3 align="center">
   Das KDE-Projekt veröffentlicht die siebente, so genannte Übersetzungs- und Wartungsversion der führenden, freien Arbeitsumgebung.
</h3>

<p align="justify">
  <strong>
     KDE 3.5.8 bringt Übersetzungen in 65 verschiedene Sprachen, sowie Verbesserungen in den Programmen zum persönlichen Informationsmanagement und anderen Anwendungen mit sich.
  </strong>
</p>

<p align="justify">
  16. Oktober 2007, das Internet. Das <a href="http://www.kde.org/">KDE-Projekt</a> hat heute die sofortige Verfügbarkeit von KDE 3.5.8, einer Wartungsversion der neuesten Generation der führenden und umfangreichsten <em>freien</em> Arbeitsumgebung für GNU/Linux und andere UNIX-Betriebssysteme, bekannt gegeben. Die K-Arbeitsumgebung unterstützt nun <a href="http://l10n.kde.org/stats/gui/stable/">65 verschiedene Sprachen</a>, um sie für noch mehr Menschen einfach zugänglich zu machen und kann ganz einfach, von anderen Gruppen die etwas zum Projekt beitragen möchten, erweitert werden.
</p>
<?php // Ab hier beginnen die Änderungen seit Version 3.5.7. ?>

<p align="justify">
    Während sich die Fertigstellung von KDE 4.0 weiterhin im Fokus der Entwickler befindet, bleibt die bereits verfügbare 3.5-Serie momentan die Arbeitsumgebung der Wahl, da sie bereits ausreichend getestet wurde, stabil ist und gut unterstützt wird. Die Version 3.5.8 mit ihren Wort wörtlich hunderten Fehlerkorrekturen trägt erneut dazu bei, den Anwendern die Arbeit zu erleichtern. Der Hauptaugenmerk der Verbesserungen in KDE 3.5.8 liegt dabei auf:
        
    <ul>
        <li>
        Verbesserungen im Konqueror und seiner Webbrowser-Komponente KHTML. Fehler im Umgang mit HTTP-Verbindungen wurden korrigiert und die Unterstützung für einige CSS-Funktionen in KHTML verbessert, um standardkonformer zu sein.
        </li>
        <li>
        Seitens des KDE-Grafik-Pakets flossen viele Korrekturen in Sachen PDF-Anzeige und Kolourpaint (ein Malprogramm) in diese KDE-Version ein.
        </li>
        <li>
        Die KDE-Programme zum persönlichen Informationsmanagement (PIM), insbesondere die E-Mail-Anwendung KMail und die Kalenderanwendung KOrganizer, erfuhren, wie gehabt, unzählige Korrekturen in puncto Stabilität.
        </li>
    </ul>
 
</p>
<?php // Hier endet die Passage mit den Änderungen seit Version 3.5.7. ?>

<p align="justify">
  Für eine ausführlichere Liste der Verbesserungen seit der
  <a href="http://www.kde.org/announcements/announce-3.5.7.php">KDE-Version 3.5.7 </a>,
  veröffentlicht am 22. Mai 2007, halten Sie sich bitte an die
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_7to3_5_8.php">Liste der Änderungen in KDE 3.5.8</a>.
</p>

<p align="justify">
  KDE 3.5.8 bringt standardmäßig eine Arbeitsumgebung und fünfzehn weiter Pakete (PIM, Administration, Netzwerk, unterhaltsames Lernen (Edutainment), Multimedia, Spiele, Illustration, Web-Entwicklung und noch mehr) mit sich. Die durch zahlreiche Preise ausgezeichneten KDE-Werkzeuge und -Anwendungen sind in <strong>65 verschiedenen Sprachen</strong> verfügbar.
</p>

<h4>
  Distributionen die KDE enthalten
</h4>
<p align="justify">
  Die meisten Linux-Distributionen und UNIX-Betriebssysteme nehmen neue KDE-Versionen nicht umgehend in ihr Software-Angebot auf, sondern tun dies erst in der jeweils nächsten Version der Distribution. Werfen Sie bitte einen Blick auf <a href="http://www.kde.org/download/distributions.php">diese Liste</a> um zu sehen, welche Distributionen KDE enthalten.
</p>

<h4>
  Installation von KDE 3.5.8 Binärpaketen
</h4>
<p align="justify">
  <em>Paketersteller</em>.
  Einige Betriebssystemhersteller, sowie andere freiwillige Helfer bieten Binärpakete von KDE 3.5.8 für einige Versionen ihrer Distributionen an. Einige dieser Binärpakete stehen gratis zum Herunterladen auf dem KDE-Server unter <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http://download.kde.org</a> bereit.
  Zusätzliche Binärpakete, sowie Aktualisierungen der jetzt verfügbaren Pakete, werden im Lauf der nächsten paar Wochen verfügbar sein.
</p>

<p align="justify">
  <a name="package_locations"><em>Binärpakete finden</em></a>.
  Für eine Liste der derzeit verfügbaren Binärpakete, über die das KDE-Projekt informiert wurde, werfen Sie bitte einen Blick auf die <a href="/info/3.5.8.php">KDE 3.5.8 Informationsseite</a>.
</p>

<h4>
  KDE 3.5.8 kompilieren
</h4>
<p align="justify">
  <a name="source_code"></a><em>Quelltext</em>.
  Der vollständige Quelltext von KDE 3.5.8 kann <a href="http://download.kde.org/stable/3.5.8/src/">frei heruntergeladen werden</a>. Hinweise bezüglich Kompilation und Installation von KDE 3.5.8 finden Sie auf der <a href="/info/3.5.8.php">KDE
  3.5.8 Informationsseite</a>.
</p>

<h4>
  KDE unterstützen
</h4>
<p align="justify">
KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Software-Projekt</a>, dass nur durch die Hilfe vieler Freiwilliger, die Zeit und Arbeit investieren, existiert und wächst. Das KDE-Projekt ist immer auf der Suche nach neuen Freiwilligen die bereit sind etwas beizutragen, unabhängig davon ob es sich dabei um Programmierarbeiten, Fehlerbehebung, das Berichten von Fehlern, schreiben von Dokumentationen, Übersetzungen, Werbung, Geld, usw. handelt. Wir begrüßen jeden Beitrag und nehmen jede Hilfe gerne an. Bitte lesen Sie die <a href="/community/donations/">Seite über die Unterstützung von KDE</a> für nähere Informationen.
</p>

<p align="justify">
Wir freuen uns darauf, bald etwas von Ihnen zu hören!
</p>

<h4>
  Über KDE
</h4>
<p align="justify">
  KDE ist ein durch <a href="/awards/">zahlreiche Preise</a> ausgezeichnetes, unabhängiges Projekt von <a href="/people/">hunderten</a>, auf der ganzen Welt verteilten Entwicklern, Übersetzern, Künstlern oder anderen Experten, die über das Internet zusammenarbeiten. Das Ziel dabei ist es, eine frei verfügbare, ausgeklügelte, konfigurierbare und stabile Arbeitsoberfläche und -umgebung unter der Verwendung einer flexiblen, auf Komponenten basierenden, netzwerktransparenten Architektur zu erstellen und gleichzeitig auch eine geeignete, ihres Gleichen suchende Entwicklungsumgebung zu bieten.
</p>

<p align="justify">
  KDE bietet eine stabile und durchdachte Arbeitsumgebung, inklusive eines modernen Webbrowsers (<a href="http://konqueror.kde.org/">Konqueror</a>), Programmen zum persönlichen Informationsmanagement (<a href="http://kontact.org/">Kontact</a>), eines vollständigen Pakets von Büroanwendungen (<a href="http://www.koffice.org/">KOffice</a>), einer großen Fülle von Netzwerkanwendungen und -werkzeugen und einer effizienten, intuitiven Entwicklungsumgebung mit der integrierten Entwicklungsumgebung (IDE) <a href="http://www.kdevelop.org/">KDevelop</a>.
</p>

<p align="justify">
  KDE ist der „lebende Beweis“, dass das Open-Source-Entwicklungsmodell für Software zu erstklassigen Technologien führen kann, die auch der komplexesten, kommerziellen Software gleichwertig oder überlegen sind.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Warenzeichen- und Urheberrechtshinweise</em>
  KDE<sup>&#174;</sup> und das "K Desktop Environment"- bzw. K-Arbeitsumgebung-<sup>&#174;</sup>Logo sind eingetragene Warenzeichen des KDE e.V.

  Linux ist ein eingetragenes Warenzeichen von Linus Torvalds.

  UNIX ist ein eingetragenes Warenzeichen von The Open Group in den Vereinigten Staaten von Amerika und anderen Ländern.

  Alle anderen, in dieser Pressemitteilung erwähnten Warenzeichen, sind Eigentum der jeweiligen Inhaber.
  </font>
</p>

<hr />

<h4>Pressekontakte</h4>

<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
