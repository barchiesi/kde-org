<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Plasma 5.5 Release");
  $site_root = "../";
  $release = 'plasma-5.5.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
iframe {text-align: center; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>


<figure style="float: none">
<a href="plasma-5.5/plasma-5.5.png">
<img src="plasma-5.5/plasma-5.5-wee.png" style="border: 0px" width="600" height="375" alt="<?php i18n("Plasma 5.5");?>" />
</a>
<figcaption><?php i18n("Plasma 5.5");?></figcaption>
</figure>

<p>
<?php i18n("Tuesday, 8 December 2015. "); ?>
<?php i18n("Today KDE releases a feature update to its desktop software, Plasma 5.5.
");?>
</p>

<div style="text-align: center;font-style: italic;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/3wFTo34mCj0?rel=0" frameborder="0" allowfullscreen></iframe>
<br />
<?php i18n("Video of Plasma 5.5 highlights");?>
</div>

<p> <?php i18n("We have been working hard over the last four months
to smooth off the rough edges, add useful new workflows, make
Plasma even more beautiful and build the foundations for the future.");?> </p>

<br clear="all" />
<figure>
<a href="plasma-5.5/breeze.png">
<img src="plasma-5.5/breeze-wee.png" style="border: 0px" width="400" height="220" alt="<?php i18n("Breeze Icons");?>" />
</a>
<figcaption><?php i18n("Breeze Icons");?></figcaption>
</figure>
<h3><?php i18n("Updated Breeze Plasma Theme") ?></h3>

<?php i18n("The Breeze Plasma widget theme has been updated to make it more consistent.");?></p>

<?php i18n("While the Breeze icons theme adds new icons and updates the existing icon set to improve the visual design.");?></p>

<h3><?php i18n("Plasma Widget Explorer") ?></h3>

<?php i18n("The Plasma Widget explorer now supports a two column view with new widget icons for Breeze, Breeze Dark and Oxygen");?></p>

<h3><?php i18n("Expanded Feature Set in Application Launcher") ?></h3>

<?php i18n("Context menus in Application Launcher ('Kickoff') can now list documents recently opened in an application, allow editing the application's menu entry and adding the application to the panel, Task Manager or desktop. Favorites now supports documents, directories and system actions or they can be created from search results. These features (and some others) were previously available only in the alternative Application Menu ('Kicker') and have now become available in the default Application Launcher by sharing the backend between both launchers.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/plasma-5.5-colorpicker.png">
<img src="plasma-5.5/plasma-5.5-colorpicker-wee.png" style="border: 0px" width="300" height="301" alt="<?php i18n("Color Picker Plasma Applet");?>" />
</a>
<figcaption><?php i18n("Color Picker Plasma Applet");?></figcaption>
</figure>
<h3><?php i18n("New Applets in Plasma Addons") ?></h3>
<h4><?php i18n("Color Picker") ?></h4>

<?php i18n("Not only have we restored support for the Color Picker applet, we've given it an entire new UI refresh to fit in with Plasma 5.");?></p>

<?php i18n("The color picker applet lets you pick a color from anywhere on the screen and automatically copies its color code to the clipboard in a variety of formats (RGB, Hex, Qt QML rgba, LaTeX).");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/plasma-5.5-user-switcher.png">
<img src="plasma-5.5/plasma-5.5-user-switcher-wee.png" style="border: 0px" width="300" height="301" alt="<?php i18n("User Switcher Plasma Applet");?>" />
</a>
<figcaption><?php i18n("User Switcher Plasma Applet");?></figcaption>
</figure>
<h4><?php i18n("User Switcher") ?></h4>

<p><?php i18n("
User switching has been updated and improved and is now accessible from the Application Launcher, the new User Switcher applet and in the lock screen.  It shows the user's full name and user  set avatar.  This is very useful for offices with shared desks.  More info in <a href='http://blog.broulik.de/2015/10/polish-polish-polish-5-5-edition/'>the developer blog</a>.");?>
</p>

<h4><?php i18n("Disk Quota") ?></h4>
<?php i18n("Plasma 5.5 sees a new applet designed for business environments or universities. This applet will show you usage assessed not around the real disk usage, but your allowed quota by your system administrator.");?></p>

<h4><?php i18n("Activity Pager") ?></h4>
<?php i18n("Done for users whose use case of activities partly overlaps with virtual desktops: it looks like a pager, it behaves like a pager but uses activities instead of virtual desktops. This gives a quick glimpse of what activities are running and how many windows are associated to each activity.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/systray.png">
<img src="plasma-5.5/systray.png" style="border: 0px" width="421" height="105" alt="<?php i18n("Legacy Systray Icons");?>" />
</a>
<figcaption><?php i18n("Legacy System Tray Icons");?></figcaption>
</figure>
<h3><?php i18n("Restored Legacy Icons in System Tray Support") ?></h3>

<?php i18n("In response to feedback, we've rewritten support for legacy applications not using the <a href='http://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/'>StatusNotifier</a> standard for system tray icons.");?></p>

<h3><?php i18n("Bug Stats") ?></h3>

<?php i18n("In the run up to the Plasma 5.5 beta an incredible <a href='https://goo.gl/mckdTF'>over 1,000 bugs were fixed</a>.") ?></h3>

<h3><?php i18n("OpenGL ES Support in KWin") ?></h3>
<?php i18n("Support for switching to OpenGL ES in KWin returns. So far only switching through an environment variable and restarting KWin is supported. Set environment variable KWIN_COMPOSE to 'O2ES' to force the OpenGL ES backend. Please note that OpenGL ES is not supported by all drivers. Because of that it's not exposed through a configuration mechanism. Please consider it as an expert mode.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/kscreenlocker.png">
<img src="plasma-5.5/kscreenlocker-wee.png" style="border: 0px" width="300" height="234" alt="<?php i18n("Screen Locker");?>" />
</a>
<figcaption><?php i18n("Screen Locker");?></figcaption>
</figure>
<h3><?php i18n("Wayland Progress:") ?></h3>

<?php i18n("With Plasma 5.5 a basic Wayland session is provided. Wayland is the successor of the dated X11 windowing system providing a modern approach. The system is more secure (e.g. key loggers are no longer trivial to implement) and follows the paradigm of 'every frame perfect' which makes screen tearing very difficult. With Plasma 5.4 the KDE community already provided a technology preview based on the feature set of the Phone project. With Plasma 5.5 this is now extended with more 'desktop style' usages. Important features like move/resize of windows is now supported as well as many integration features for the desktop shell. This allows for usage by early adopters, though we need to point out that it is not yet up to the task of fully replacing an X session. We encourage our more technical users to give it a try and report as many bugs as you can find.");?></p>

<p><?php i18n("A new <a href='http://vizzzion.org/blog/2015/11/screen-management-in-wayland/'>screen management protocol</a> has been created for configuring the connected screens of a Wayland session.");?></p>

<p><?php i18n("Also added are some protocols for controlling KWin effects in Wayland such as window background blur and windows minimize animation");?></p>

<p><?php i18n("Plasma on Wayland session now features secure screen locking, something never fully achievable with X. Read more about fixing this 11 year old bug on the <a href='https://bhush9.github.io/2015/11/17/screenlocker-in-wayland/'>screenlocker integration developer blog</a>.");?></p>

<p><?php i18n("Please also see the list of <a href='https://community.kde.org/Plasma/5.5_Errata#KWin_Wayland'>known issues with Wayland on the Errata page</a>.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/discover.png">
<img src="plasma-5.5/discover-wee.png" style="border: 0px" width="300" height="165" alt="<?php i18n("Discover");?>" />
</a>
<figcaption><?php i18n("Discover");?></figcaption>
</figure><h3><?php i18n("New Discover design") ?></h3>
<?php i18n("With the help of the KDE Visual Design Group we came up with a new design that will improve the usability of our software installer.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/noto.png">
<img src="plasma-5.5/noto-wee.png" style="border: 0px" width="300" height="103" alt="<?php i18n("Noto Font");?>" />
</a>
<figcaption><?php i18n("Noto Font");?></figcaption>
</figure><h3><?php i18n("New Default Font") ?></h3>
<?php i18n("Our default font has switched to <a href='https://www.google.com/get/noto/'>Noto</a> a beautiful and free font which aims to support all languages with a harmonious look and feel.");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.5/baloo-kinfocenter.png">
<img src="plasma-5.5/baloo-kinfocenter-wee.png" style="border: 0px" width="300" height="177" alt="<?php i18n("File Indexer Status");?>" />
</a>
<figcaption><?php i18n("File Indexer Status");?></figcaption>
</figure>
<h3><?php i18n("Info Center") ?></h3>
<?php i18n("A status module for the file indexer was added.");?></p>

<h3><?php i18n("Plasma Networkmanager") ?></h3>
<p><?php i18n("There have <a href='https://grulja.wordpress.com/2015/11/08/upcoming-news-in-plasma-5-5/'>been several improvements</a> to our network manager applet.  WPA/WPA2 Enterprise validation was added, it uses a new password field widget and OpenVPN has more options.");?></p>

<h3><?php i18n("Wallpapers") ?></h3>

<p><?php i18n("We have a new selection of <a href='https://kdeonlinux.wordpress.com/2015/11/13/wallpaper-contribution-for-plasma-5-5/'>wonderful wallpapers</a>
from RJ Quiralta, Martin Klapetek, Timothée Giet, Dmitri Popov, Maciej Wiklo and Risto S for the Plasma 5.5 release.");?></p>

<h3><?php i18n("Full Changelog") ?></h3>
<p>
<a href="plasma-5.4.3-5.5.0-changelog.php">
<?php i18n("Full Plasma 5.5.0 changelog");?></a>
</p>

<h3><?php i18n("Known Issues") ?></h3>

<p><?php i18n("Please see the <a
href='https://community.kde.org/Plasma/5.5_Errata'>Plasma 5.5 Errata
page</a> for some of the highest profile issues including some significant problems
caused by Intel drivers.</a>") ?></p>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
