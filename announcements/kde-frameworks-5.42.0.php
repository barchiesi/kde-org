<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.42.0");
  $site_root = "../";
  $release = '5.42.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
January 13, 2018. KDE today announces the release
of KDE Frameworks 5.42.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General changes");?></h3>

<ul>
<li><?php i18n("Fixes for cmake 3.10+ AUTOMOC warnings");?></li>
<li><?php i18n("More widespread use of categorized logging, to turn off debug output by default (use kdebugsetting to re-enable it)");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("balooctl status: process all arguments");?></li>
<li><?php i18n("Fix multiple word tag queries");?></li>
<li><?php i18n("Simplify rename conditions");?></li>
<li><?php i18n("Fix incorrect UDSEntry display name");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Fix icon name \"weather-none\" -&gt; \"weather-none-available\" (bug 379875)");?></li>
<li><?php i18n("remove Vivaldi icon cause the origin app icon fits perfect with breeze (bug 383517)");?></li>
<li><?php i18n("add Some missing mimes");?></li>
<li><?php i18n("Breeze-icons add document-send icon (bug 388048)");?></li>
<li><?php i18n("update album artist icon");?></li>
<li><?php i18n("add labplot-editlayout support");?></li>
<li><?php i18n("remove duplicates and update dark theme");?></li>
<li><?php i18n("add gnumeric breeze-icon support");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Use readelf to find project dependencies");?></li>
<li><?php i18n("Introduce INSTALL_PREFIX_SCRIPT to easily set up prefixes");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("avoid crash in kactivities if no dbus connection is available");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("API docs: explain how to use KWindowConfig from a dialog constructor");?></li>
<li><?php i18n("Deprecate KDesktopFile::sortOrder()");?></li>
<li><?php i18n("Fix the result of KDesktopFile::sortOrder()");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Extend CMAKE_AUTOMOC_MACRO_NAMES also for own build");?></li>
<li><?php i18n("Match license keys by spdx");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Fix absolute path detection for cmake 3.5 on Linux");?></li>
<li><?php i18n("Add cmake function 'kdbusaddons_generate_dbus_service_file'");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Qml controls for kcm creation");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file");?></li>
<li><?php i18n("Add used property to service file definition");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Inform the user if the module can not be registered with org.kde.kded5 and exit with error");?></li>
<li><?php i18n("Mingw32 compile fix");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("add entity for Michael Pyne");?></li>
<li><?php i18n("add entities for Martin Koller to contributor.entities");?></li>
<li><?php i18n("fix debian entry");?></li>
<li><?php i18n("add entity Debian to general.entities");?></li>
<li><?php i18n("add entity kbackup, it was imported");?></li>
<li><?php i18n("add entity latex, we have already 7 entities in different index.docbooks in kf5");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Add scheme (file://). It's necessary when we use it in qml and we added all");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("remove extractor based on QtMultimedia");?></li>
<li><?php i18n("Check for Linux instead of TagLib and avoid building the usermetadatawritertest on Windows");?></li>
<li><?php i18n("Restore # 6c9111a9 until a successful build and link without TagLib is possible");?></li>
<li><?php i18n("Remove the taglib dependency, caused by a left-over include");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Finally allow to disable debug output by using categorized logging");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("do not treat ts-pmap-compile as executable");?></li>
<li><?php i18n("Fix a memory leak in KuitStaticData");?></li>
<li><?php i18n("KI18n: fix a number of double lookups");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Remove impossible to reach code");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Properly parse dates in cookies when running in non-English  locale (bug 387254)");?></li>
<li><?php i18n("[kcoredirlister] Fix sub path creation");?></li>
<li><?php i18n("Reflect trash state in iconNameForUrl");?></li>
<li><?php i18n("Forward QComboBox signals instead of QComboBox lineedit signals");?></li>
<li><?php i18n("Fixed webshortcuts showing their file path instead of their human-readable name");?></li>
<li><?php i18n("TransferJob: fix for when the readChannelFinished has already been emitted before start is called() (bug 386246)");?></li>
<li><?php i18n("Fix crash, presumably since Qt 5.10? (bug 386364)");?></li>
<li><?php i18n("KUriFilter: don't return an error on non-existing files");?></li>
<li><?php i18n("Fix creation of paths");?></li>
<li><?php i18n("Implement a kfile dialog where we can add custom widget");?></li>
<li><?php i18n("Verify that qWaitForWindowActive doesn't fail");?></li>
<li><?php i18n("KUriFilter: port away from KServiceTypeTrader");?></li>
<li><?php i18n("API dox: use class names in titles of example screenshots");?></li>
<li><?php i18n("API dox: also deal with KIOWIDGETS export macros in QCH creation");?></li>
<li><?php i18n("fix handling of KCookieAdvice::AcceptForSession (bug 386325)");?></li>
<li><?php i18n("Created 'GroupHiddenRole' for KPlacesModel");?></li>
<li><?php i18n("forward socket error string to KTcpSocket");?></li>
<li><?php i18n("Refactor and remove duplicate code in kfileplacesview");?></li>
<li><?php i18n("Emit 'groupHiddenChanged' signal");?></li>
<li><?php i18n("Refactoring the hidding/showing animation use within KFilePlacesView");?></li>
<li><?php i18n("User can now hide an entire places group from KFilePlacesView (bug 300247)");?></li>
<li><?php i18n("Hidding place groups implementation in KFilePlacesModel (bug 300247)");?></li>
<li><?php i18n("[KOpenWithDialog] Remove redundant creation of KLineEdit");?></li>
<li><?php i18n("Add undo support to BatchRenameJob");?></li>
<li><?php i18n("Add BatchRenameJob to KIO");?></li>
<li><?php i18n("Fix doxygen code block not ending with endcode");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("keep the flickable interactive");?></li>
<li><?php i18n("proper prefix for icons as well");?></li>
<li><?php i18n("fix form sizing");?></li>
<li><?php i18n("read wheelScrollLines from kdeglobals if existing");?></li>
<li><?php i18n("add a prefix for kirigami files to avoid conflicts");?></li>
<li><?php i18n("some static linking fixes");?></li>
<li><?php i18n("move plasma styles to plasma-framework");?></li>
<li><?php i18n("Use single quotes for matching characters + QLatin1Char");?></li>
<li><?php i18n("FormLayout");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Offer QWindow API for KJobWidgets:: decorators");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Limit request cache size");?></li>
<li><?php i18n("Require the same internal version as you're building");?></li>
<li><?php i18n("Prevent global variables from been using after freeing");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[KStatusNotifierItem] Don't \"restore\" widget position on its first show");?></li>
<li><?php i18n("Use positions of legacy systray icons for Minimize/Restore actions");?></li>
<li><?php i18n("Handle positions of LMB clicks on legacy systray icons");?></li>
<li><?php i18n("do not make the context menu a Window");?></li>
<li><?php i18n("Add explanatory comment");?></li>
<li><?php i18n("Lazy-instanciate and lazy-load KNotification plugins");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("invalidate the runtime cache on install");?></li>
<li><?php i18n("experimental support for rcc files loading in kpackage");?></li>
<li><?php i18n("compile against Qt 5.7");?></li>
<li><?php i18n("Fix up package indexing and add runtime caching");?></li>
<li><?php i18n("new KPackage::fileUrl() method");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("[RunnerManager] Don't mess with ThreadWeaver thread count");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix wildcard matching for modelines");?></li>
<li><?php i18n("Fix a regression caused by changing backspace key behavior");?></li>
<li><?php i18n("port to non-deprecated API like already used at other place (bug 386823)");?></li>
<li><?php i18n("Add missing include for std::array");?></li>
<li><?php i18n("MessageInterface: Add CenterInView as additional position");?></li>
<li><?php i18n("QStringList initializer list cleanup");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Use correct service executable path for installing kwalletd dbus service on Win32");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Fix naming inconsistency");?></li>
<li><?php i18n("Create interface for passing server decoration palettes");?></li>
<li><?php i18n("Explicitly include std::bind functions");?></li>
<li><?php i18n("[server] Add a method IdleInterface::simulateUserActivity");?></li>
<li><?php i18n("Fix regression caused by backward compatibility support in data source");?></li>
<li><?php i18n("Add support for version 3 of data device manager interface (bug 386993)");?></li>
<li><?php i18n("Scope exported/imported objects to the test");?></li>
<li><?php i18n("Fix error in WaylandSurface::testDisconnect");?></li>
<li><?php i18n("Add explicit AppMenu protocol");?></li>
<li><?php i18n("Fix exclude generated file from automoc feature");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix crash in setMainWindow on wayland");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("API dox: make doxygen cover session related macros &amp; functions again");?></li>
<li><?php i18n("Disconnect shortcutedit slot on widget destruction (bug 387307)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("802-11-x: support for PWD EAP method");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Air theme] Add task bar progress graphic (bug 368215)");?></li>
<li><?php i18n("Templates: remove stray * from license headers");?></li>
<li><?php i18n("make packageurlinterceptor as noop as possible");?></li>
<li><?php i18n("Revert \"Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg\"");?></li>
<li><?php i18n("move kirigami plasma styles here");?></li>
<li><?php i18n("disappearing scrollbars on mobile");?></li>
<li><?php i18n("reuse KPackage instance between PluginLoader and Applet");?></li>
<li><?php i18n("[AppletQuickItem] Only set QtQuick Controls 1 style once per engine");?></li>
<li><?php i18n("Don't set a window icon in Plasma::Dialog");?></li>
<li><?php i18n("[RTL] - align properly the selected text for RTL (bug 387415)");?></li>
<li><?php i18n("Initialize scale factor to the last scale factor set on any instance");?></li>
<li><?php i18n("Revert \"Initialize scale factor to the last scale factor set on any instance\"");?></li>
<li><?php i18n("Don't update when the underlying FrameSvg is repaint-blocked");?></li>
<li><?php i18n("Initialize scale factor to the last scale factor set on any instance");?></li>
<li><?php i18n("Move if check inside #ifdef");?></li>
<li><?php i18n("[FrameSvgItem] Don't create unnecessary nodes");?></li>
<li><?php i18n("Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Also look for qrencode with debug suffix");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("simplify and don't try to block mouse events");?></li>
<li><?php i18n("if no wheel.pixelDelta, use global wheel scroll lines");?></li>
<li><?php i18n("desktop tabbars have different widths for each tab");?></li>
<li><?php i18n("ensure a non 0 size hint");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't export internal helper executables");?></li>
<li><?php i18n("Sonnet: fix wrong language for suggestions in mixed-language texts");?></li>
<li><?php i18n("Remove ancient and broken workaround");?></li>
<li><?php i18n("Don't cause circular linking on Windows");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Highlighting indexer: Warn about context switch fallthroughContext=\"#stay\"");?></li>
<li><?php i18n("Highlighting indexer: Warn about empty attributes");?></li>
<li><?php i18n("Highlighting indexer: Enable errors");?></li>
<li><?php i18n("Highlighting indexer: report unused itemDatas and missing itemDatas");?></li>
<li><?php i18n("Prolog, RelaxNG, RMarkDown: Fix highlighting issues");?></li>
<li><?php i18n("Haml: Fix invalid and unused itemDatas");?></li>
<li><?php i18n("ObjectiveC++: Remove duplicate comment contexts");?></li>
<li><?php i18n("Diff, ObjectiveC++: Cleanups and fix highlighting files");?></li>
<li><?php i18n("XML (Debug): Fix incorrect DetectChar rule");?></li>
<li><?php i18n("Highlighting Indexer: Support cross-hl context checking");?></li>
<li><?php i18n("Revert: Add GNUMacros to gcc.xml again, used by isocpp.xml");?></li>
<li><?php i18n("email.xml: add *.email to the extensions");?></li>
<li><?php i18n("Highlighting Indexer: Check for infinite loops");?></li>
<li><?php i18n("Highlighting Indexer: Check for empty context names and regexps");?></li>
<li><?php i18n("Fix referencing of non-existing keyword lists");?></li>
<li><?php i18n("Fix simple cases of duplicate contexts");?></li>
<li><?php i18n("Fix duplicate itemDatas");?></li>
<li><?php i18n("Fix DetectChar and Detect2Chars rules");?></li>
<li><?php i18n("Highlighting Indexer: Check keyword lists");?></li>
<li><?php i18n("Highlighting Indexer: Warn about duplicate contexts");?></li>
<li><?php i18n("Highlighting Indexer: Check for duplicate itemDatas");?></li>
<li><?php i18n("Highlighting indexer: Check DetectChar and Detect2Chars");?></li>
<li><?php i18n("Validate that for all attributes an itemData exists");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.42");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
