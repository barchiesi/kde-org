<?php
  $page_title = "KDE 4.0 Visual Guide";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>
<div style="direction:rtl;text-align:right;">
<p>
میزکار KDE نسخه ۴.۰ و برنامه‌های آن شایسته نگاه دقیق‌تری هستند.صفخات زیر نگاهی کلی بر روی KDE ۴.۰ و برخی برنامه‌های آن دارند.
تصاویری از بسیاری قسمت‌ها نیز وجود دارد.به یاد داشته باشید که این‌ها تنها قسمت‌های کوچکی از انچه KDE ۴.۰ به همراه دارد هستند.
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
	<td width="32">
		<a href="desktop-fa.php"><img src="images/desktop-32.png" /></a>
	</td>
	<td>
		<a href="desktop-fa.php"><strong>میزکار</strong>: Plasma, KRunner, KickOff and KWin</a>
	</td>
</tr>
<tr>
	<td>
		<a href="applications-fa.php"><img src="images/applications-32.png" /></a>
	</td>
	<td>
		<a href="applications-fa.php"><strong>برنامه‌ها</strong>: Dolphin, Okular, Gwenview, System Settings and Konsole</a>
	</td>
</tr>
<tr>
	<td>
		<a href="education.php"><img src="images/education-32.png" /></a>
	</td>
	<td>
		<a href="education.php"><strong>برنامه‌های کمک تحصیلی:</strong> Kalzium, Parley, Marble, Blinken, KStars and KTouch</a>
	</td>
</tr>
<tr>
	<td>
		<a href="games.php"><img src="images/games-32.png" /></a>
	</td>
	<td>
		<a href="games.php"><strong>بازی‌ها</strong>: KGoldrunner, KFourInLine, LSkat, KJumpingCube, KSudoku and Konquest</a>
	</td>
</tr>
</table>
</div>

<p>
<br /><br /><br />
<em>راهنمای تصویری، ترجمه شده توسط امیل صدق و سعید تقوی</em>
</p>
</div>
<?php
  include("footer.inc");
?>
