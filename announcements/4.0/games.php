<?php
  $page_title = "KDE 4.0 Visual Guide: Games";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
  guide_links();
?>

Also available in:
<a href="http://www.kdecn.org/announcements/4.0/games.php">Chinese</a>
<a href="http://fr.kde.org/announcements/4.0/games.php">French</a>

<p>
<img src="images/games.png" align="right"  hspace="5"/>
The KDE Games community has put in tremendous effort porting and reworking many games
from KDE 3 and also adding several new, exciting games to KDE 4.0.<br />
Highlights in the games include more intuitive gameplay, polished graphics and better
resolution independence. You can run games fullscreen as well as in small windows.<br />

Most of the artwork has been redone, giving the games in KDE 4.0 a very polished and
modern look.
</p>

<h2>KGoldrunner - an arcade game</h2>
<?php
	screenshot("kgoldrunner_thumb.jpg", "kgoldrunner.jpg", "center", "Collect gold nuggets in KGoldrunner");
?>
<p>
KGoldrunner is a retro arcade game with a puzzle flavor. 
It has hundreds of levels where pieces of gold must be 
collected, with enemies in hot pursuit.
</p>

<h2>KFourInLine - a board game</h2>
<?php
	screenshot("kfourinline_thumb.jpg", "kfourinline.jpg", "center", "Connect four pieces in a row in KFourInLine");
?>
<p>
KFourInLine is a board game for two players based on the 
Connect-Four game. The players try to build up a row of 
four pieces using different strategies.
</p>

<h2>LSkat - a card game</h2>
<?php
	screenshot("lskat_thumb.jpg", "lskat.jpg", "center", "Play Skat");
?>
<p>
Lieutenant Skat (from German "Offiziersskat") is a fun and 
engaging card game for two players, where the second player 
is either a live opponent, or a built-in artificial intelligence.
</p>

<h2>KJumpingCube - a dice game</h2>
<?php
	screenshot("kjumpingcube_thumb.jpg", "kjumpingcube.jpg", "center", "Play dice with KJumpingCube");
?>
<p>
KJumpingcube is a simple dice-driven tactical game. The playing 
area consists of squares containing points. Players move by 
clicking on either a vacant square, or on their own square.
</p>

<h2>KSudoku - a logic game</h2>
<?php
	screenshot("ksudoku_thumb.jpg", "ksudoku.jpg", "center", "Exercise your brain with KSudoku");
?>
<p>
KSudoku is a logic-based symbol placement puzzle. The player has 
to fill a grid so that each column, row as well as each square 
block on the game field contains only one instance of each symbol.
</p>

<h2>Konquest - a strategy game</h2>
<?php
	screenshot("konquest_thumb.jpg", "konquest.jpg", "center", "Konquer other planets in Konquest");
?>
<p>
Players conquer other planets by 
sending ships to them. The goal is to build an interstellar empire 
and ultimately conqueror all other player's planets.
</p>

<p>
You can find more information about the above games and many more on 
the newly updated <a href="http://games.kde.org">KDE Games</a> website.
</p>
<table width="100%">
	<tr>
		<td width="50%">
				<a href="education.php">
				<img src="images/education-32.png" />
				Previous page: Educational Applications
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="guide.php">Overview
				<img src="images/star-32.png" /></a>
		</td>
	</tr>
</table>

<?php
  include("footer.inc");
?>
