<?php

  $page_title = "KDE 4.0 Vrijgegeven";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Ook beschikbaar in:
<a href="index.php">Engels</a>
<a href="index-bn_IN.php">Bengaals (India)</a>
<a href="index-ca.php">Catalaans</a>
<a href="index-cz.php">Czech</a>
<a href="index-es.php">Spaans</a>
<a href="http://fr.kde.org/announcements/4.0/">Frans</a>
<a href="index-gu.php">Gujarati</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">German</a>
<a href="index-he.php">Hebreeuws</a>
<a href="index-hi.php">Hindi</a>
<a href="index-it.php">Italiaans</a>
<a href="index-lv.php">Lets</a>
<a href="index-ml.php">Maleisisch</a>
<a href="index-sl.php">Sloveens</a>
<a href="index-pt_BR.php">Portuguees (Braziliaans)</a>
<a href="index-pa.php">Punjabi</a>
<a href="index-sv.php">Zweeds</a>
<a href="index-ru.php">Russisch</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center">
   KDE-project levert vierde grote versie van moderne vrije desktop
</h3>
<p align="justify">
  <strong>
    Met de vierde versie maakt de KDE-gemeenschap een begin aan het KDE 4-tijdperk.
  </strong>
</p>
<p align="justify">
11 januari 2007 (het Internet).
</p>

<p>
De KDE-gemeenschap kondigt met grote vreugde de directe beschikbaarheid van <a href="http://www.kde.org/announcements/4.0/">KDE 4.0</a> aan. Deze belangrijke uitgave betekent het einde van een lange periode van intensieve ontwikkeling en het begin van het KDE 4-tijdperk.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
"De KDE 4.0-desktop");
?>

<p>
De <strong>bibliotheken</strong> van KDE 4 zijn op vrijwel alle gebieden verbeterd. Het multimedia-framework Phonon biedt alle KDE-applicaties ondersteuning voor platform-onafhankelijke multimedia. Het hardware-integratieframework Solid maakte het communiceren met apparaten eenvoudiger, en biedt hulpmiddelen voor betere energiebesparing.

<p />
De <strong>desktop</strong> van KDE 4 heeft een aantal belangrijke nieuwe mogelijkheden gekregen. De desktop-omgeving Plasma biedt een geheel nieuwe interface, inclusief paneel, menu, widgets voor op de desktop en een dashboard-functie. KWin, de window manager van KDE, biedt nu geavanceerde grafische effecten om het werken met vensters eenvoudiger te maken.

<p />
Ook een heleboel KDE-<strong>applicaties</strong> zijn verbeterd. Visuele verbeteringen dankzij het gebruik van vector-graphics, veranderingen in de onderliggende bibliotheken, verbeterde gebruikersinterfaces, nieuwe features en zelfs nieuwe applicaties -- KDE 4.0 heeft het allemaal. De 
nieuwe documentviewer Okular en de nieuwe bestandsbeheerder Dolphin zijn slechts twee voorbeelden van applicaties die gebruik maken van de nieuwe mogelijkheden van de KDE 4.0.

<p />
<img src="images/oxybann.png" align="right" />
Het Oxygen Artworkteam heeft gezorgd voor een fris nieuw <strong>uiterlijk</strong> van de desktop. Bijna alle zichtbare delen van de desktop en applicaties hebben een facelift gekregen. Het doel van Oxygen is een uiterlijk te creëren dat zowel mooi als consistent is.

</p>

<h3>Desktop</h3>
<ul>
        <li>Plasma is de nieuwe desktopomgeving, en biedt een paneel, een menu en andere intuïtieve mogelijkheden om de desktop en applicaties te gebruiken.
        </li>
        <li>KWin, de bewezen windowmanager van KDE, biedt nu geavanceerde compositing-mogelijkheden. Het opbouwen van de interface gaat nu indien mogelijk via de grafische kaart, waardoor die vlot en intuïtief werkt.
        </li>
        <li>Oxygen is de naam voor het artwork van KDE 4.0, en biedt een consistent, rustig en mooi uiterlijk.
        </li>
</ul>
U kunt meer te weten komen over de nieuwe interface van KDE in de <a href="desktop.php">KDE 4.0 
Visual Guide</a>.

<h3>Applicaties</h3>
<ul>
        <li>Konqueror is KDE's bewezen webbrowser. Konqueror is lichtgewicht en goed geïntegreerd, en ondersteunt de nieuwe standaarden, zoals CSS 3.</li>
        <li>Dolphin is de nieuwe bestandsbeheerder van KDE. Dolphin is ontwikkeld met gebruiksvriendelijkheid in gedachten, en is eenvoudig te gebruiken en tegelijkertijd een krachtig hulpmiddel.
        </li>
        <li>Met Systeeminstellingen wordt een nieuwe interface geïntroduceerd voor de configuratie van het systeem. De systeemmonitor KSysGuard maakt het eenvoudig om hulpbronnen en systeemactiviteit in de gaten te houden.
        </li>
        <li>Okular, de documentviewer van KDE 4, ondersteunt vele bestandsformaten. Okular is een van de KDE 4-applicaties die verbeterd zijn door de samenwerking met het <a href="http://openusability.org">OpenUsability Project</a>.
        </li>
        <li>De educatieve programma's behoren tot de eersten die met behulp van KDE 4-technologie zijn ontwikkeld. Twee prachtige voorbeelden daarvan zijn Kalzium, een grafisch periodiek systeem der elementen, en de Marble Desktop Globe. U kunt meer lezen over de educatieve programma's in de <a href="education.php">Visual Guide</a>
        </li>
        <li>Veel spellen van KDE zijn verbeterd, zoals KMines (mijnenveger) en KPat (patience). Dankzij het gebruik van vector-graphics zijn de spellen resolutie-onafhankelijk geworden.
        </li>
</ul>
Een aantal applicaties worden gedetailleerder uitgewerkt in de <a href="applications.php">KDE 4.0 
Visual Guide</a>.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center",
        "Bestandsbeheerder, Systeeminstellingen en Menu in actie");
?>

<h3>Bibliotheken</h3>
<p>
<ul>
        <li>Phonon biedt multimedia-mogelijkheden voor applicaties, zoals het afspelen van audio en video. Phonon kan gebruik maken van veel verschillende backends; de standaard backend in KDE 4.0 is Xine, dat veel formaten ondersteunt. Gebruikers kunnen kiezen welk uitvoerapparaat ze willen gebruiken afhankelijk van het type multimedia dat ze willen afspelen.
        </li>
        <li>Het hardware-integratieframework Solid integreert zowel vaste als verwijderbare apparaten met KDE-applicaties. Solid zorgt ook voor energiebesparing, netwerkverbindingen en de communicatie met Bluetooth-apparaten. Intern combineert Solid de krachten van HAL, NetworkManager en Bluez, maar die componenten kunnen vervangen worden zonder de werking van bestaande applicaties in gevaar te brengen, zodat die zo goed mogelijk voor verschillende systemen geschikt gemaakt kunnen worden.
        </li>
        <li>KHTML zorgt voor het renderen van webpagina's binnen Konqueror, de webbrowser van KDE. KHTML is lichtgewicht en ondersteunt moderne standaarden zoals CSS 3. KHTML was ook de eerste engine die slaagde voor de bekende Acid 2 test.
        </li>
        <li>De ThreadWeaver-bibliotheek (onderdeel van kdelibs) biedt mogelijkheden om optimaal gebruik te maken van multicore-systemen, zodat KDE-applicaties vlotter werken en beter gebruik maken van de mogelijkheden van het systeem.
        </li>
        <li>KDE 4.0 is gebouwd op de Qt 4-bibliotheek van Trolltech, die geavanceerde visuele mogelijkheden biedt en weinig geheugen gebruikt. kdelibs biedt een enorme uitbreiding op de Qt-bibliotheek door zijn grote hoeveelheid functionaliteit op hoog niveau, wat het makkelijker maakt om applicaties te ontwikkelen.
        </li>
</ul>
</p>
<p>Meer informatie over de KDE-bibliotheken is te vinden in KDE's <a href="http://techbase.kde.org">TechBase</a>.</p>


<h4>Neem een kijkje...</h4>
<p>
De <a href="guide.php">KDE 4.0 Visual Guide</a> geeft een globaal overzicht van verscheidene nieuwe en verbeterde KDE 4.0-technologieën. Het is geïllustreerd met vele schermafdrukken en leidt u door de verschillende delen van KDE 4.0, en laat zien wat de nieuwe technologieën voor de gebruiker inhouden. Er wordt gestart met de nieuwe mogelijkheden van de <a href="desktop.php">desktop</a>, gevolgd door een introductie van <a href="applications.php">applicaties</a> zoals Systeeminstellingen, documentviewer Okular en bestandsbeheerder Dolphin. Ook worden <a href="education.php">Educatieve programma's</a> en <a href="games.php">Spellen</a> getoond.
</p>


<h4>Probeer het eens uit...</h4>
<p>
Als u KDE 4.0 wilt testen en een bijdrage wilt leveren, kunt u gebruik maken van de pakketten die door verschillende distributies op of vlak na de uitgave worden geleverd. De complete en actuele lijst is te vinden op de <a href="http://www.kde.org/info/4.0.php">KDE 4.0 Info Page</a>, waar u ook koppelingen kunt vinden naar de broncode en informatie over compileren, beveiliging e.d.
</p>
<p>
De volgende distributies zullen in elk geval pakketten of live-cd's leveren voor KDE 4.0:

<ul>
        <li>
                Een alfa-versie van het op KDE 4 gebaseerde <strong>Arklinux 2008.1</strong> wordt kort na de uitgave verwacht, met een verwachte uiteindelijke uitgave binnen 3 of 4 weken.
        </li>
        <li>
                <strong>Fedora</strong> zal KDE 4.0 meeleveren in Fedora 9, dat zal worden <a
                href="http://fedoraproject.org/wiki/Releases/9">uitgegeven</a>
                in april, terwijl er al alfa-uitgaven beschikbaar zullen zijn vanaf 24 januari. KDE 4.0-pakketten zijn te vinden in de pre-alfa <a
                href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> pakketbron.
        </li>
        <li>
                KDE 4.0-pakketten voor <strong>Debian</strong> zijn beschikbaar in "experimental", en het KDE-ontwikkelingsplatform zal zelfs in <em>Lenny</em> terechtkomen. Let op aankondigingen door het <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
        </li>
        <li>
                <strong>Gentoo Linux</strong> biedt KDE 4.0-builds op
                <a href="http://kde.gentoo.org">http://kde.gentoo.org</a> aan.
        </li>
        <li>
                <strong>Mandriva</strong> zal pakketten hebben voor 2008.0 en heeft zich ten doel gesteld een live-cd te produceren met de laatste snapshot van 2008.1.
        </li>
        <li>
                <strong>openSUSE</strong>-pakketten <a href="http://en.opensuse.org/KDE4">zijn beschikbaar</a> 
                voor openSUSE 10.3 (
                <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">installatie met één klik</a>) en openSUSE 10.2. Er is ook een <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
                Four Live CD</a> met deze pakketten erop beschikbaar. KDE 4.0 zal deel uitmaken van de aanstaande uitgave van openSUSE 11.0.
        </li>
        <li>
                <strong>Ubuntu</strong>-pakketten zullen geleverd worden in het toekomstige "Hardy Heron" (8.04), en zullen ook als updates beschikbaar zijn voor het stabiele "Gutsy Gibbon" (7.10). Er is een live-cd beschikbaar om KDE 4.0 uit te proberen. Meer details zijn te vinden in de <a href="http://kubuntu.org/announcements/kde-4.0.php">
                aankondiging</a> op Ubuntu.org.
        </li>
</ul>
</p>

<h2>Over KDE 4</h2>
<p>
KDE 4.0 is de innovatieve vrije-software-desktop, en bevat zowel applicaties voor dagelijks gebruik als specifieke applicaties. Plasma is een nieuwe desktopomgeving die ontwikkeld is voor KDE 4, en biedt een intuïtieve interface voor desktop en applicaties. De webbrowser Konqueror integreert het web met de desktop. De bestandsbeheerder Dolphin, de documentviewer Okular en het configuratiescherm Systeeminstellingen completeren de basisdesktop.
<br />
KDE is gebouwd op de KDE-bibliotheken, waardoor hulpbronnen op het network eenvoudig toegankelijk zijn (met behulp van KIO), en geavanceerde visuele mogelijkheden beschikbaar zijn (door middel van QT4). Phonon en Solid, ook een onderdeel van de KDE-bibliotheken, bieden een multimediaframework en betere hardware-integratie voor alle KDE-applicaties.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Perscontacten</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>