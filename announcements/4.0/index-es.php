<?php

  $page_title = "Lanzamiento KDE 4.0";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>
<p>FOR IMMEDIATE RELEASE</p>

<!-- Traducido del inglés por Sergio Rodríguez Sansano -->

También disponible en:
<a href="index.php">Inglés</a>
<a href="index-bn_IN.php">Bengalí (Índia)</a>
<a href="index-ca.php">Catalán</a>
<a href="index-cz.php">Czech</a>
<a href="http://fr.kde.org/announcements/4.0/">Francés</a>
<a href="index-gu.php">Guyaratí</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">German</a>
<a href="index-he.php">Hebreo</a>
<a href="index-hi.php">Hindi</a>
<a href="index-it.php">Italiano</a>
<a href="index-lv.php">Letón</a>
<a href="index-ml.php">Malabar</a>
<a href="index-mr.php">Marathi</a>
<a href="index-ru.php">Russian</a>
<a href="index-pa.php">Punjabi</a>
<a href="index-pt_BR.php">Portuguese (Brazilian)</a>
<a href="index-nl.php">Holandés</a>
<a href="index-sl.php">Esloveno</a>
<a href="index-sv.php">Sueco</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center">
   El proyecto KDE lanza la cuarta versión principal del avanzado escritorio de Software Libre.
</h3>
<p align="justify">
  <strong>
    Con la cuarta versión principal, la Comunidad de KDE marca el principio de la era KDE 4.
  </strong>
</p>
<p align="justify">
11 de enero de 2008 (INTERNET)
</p>

<p>
La Comunidad de KDE está encantada de anunciar la inmediata disponibilidad de
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a>. Esta significativa
versión marca tanto el final del largo e intensivo período de desarrollo que lleva
a KDE 4.0 como el principio de la era KDE 4.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
"The KDE 4.0 desktop");
?>

<p>
Las <strong>bibliotecas</strong> de KDE 4 han sido objeto de grandes mejoras en la
mayoría de áreas. El <i>framework</i> multimedia Phonon proporciona soporte multimedia 
independiente de la plataforma a todas las aplicaciones KDE. El <i>framework</i> de
integración de hardware Solid hace la interacción con dispositivos (extraíbles) más 
fácil y proporciona herramientas para una mejor gestión de la energía.
<p />
El <strong>Escritorio</strong> KDE 4 ha obtenido algunas nuevas funcionalidades principales.
El gestor de escritorio Plasma ofrece un nuevo interfaz de escritorio, incluyendo panel,
menú y <i>widgets</i> en el escritorio, así como una función de <i>dashboard</i>. KWin, el
gestor de ventanas de KDE, ahora permite avanzados efectos gráficos para facilitar la
interacción con las ventanas.
<p />
Muchas de las <strong>Aplicaciones</strong> KDE han sido mejoradas también. Mejoras visuales
mediante el uso de gráficos vectoriales, cambios en las bibliotecas de niveles inferiores,
mejoras en el interfaz de usuario, nueva funcionalidad, incluso nuevas aplicaciones -- si lo
nombras, KDE 4.0 lo tiene. Okular, el nuevo visor de documentos y Dolphin, el nuevo gestor
de archivos son sólo dos aplicaciones que muestran la nueva tecnología de KDE 4.0.
<p />
El equipo del <strong>Estilo</strong> Oxygen proporciona una bocanada de aire fresco al escritorio.
Caso todas las partes visibles del escritorio KDE y sus aplicaciones han recibido un
lavado de cara. La belleza y la consistencia son dos de los conceptos básicos detrás
de Oxygen.
</p>

<h3>Escritorio</h3>
<ul>
	<li>Plasma es la nueva interfaz del escritorio. Proporciona un panel, un menú
	y otras maneras intuitivas de interactuar con el escritorio y las aplicaciones.
	</li>
	<li>KWin, el fiable gestor de ventanas ahora soporta avanzados efectos de composición.
	El dibujado acelerado por hardware se encarga de que haya una interacción más suave
	e intuitiva con las ventanas.
	</li>
	<li>Oxygen es el estilo de KDE 4.0. Proporciona un consistente, agradable y bonito
	concepto de estilo.
	</li>
</ul>
Aprende más acerca de la nueva interfaz de escritorio de KDE en la <a href="desktop.php">
Guía Visual de KDE 4.0</a>.

<h3>Aplicaciones</h3>
<ul>
	<li>Konqueror es el robusto navegador web de KDE. Es ligero, está bien integrado y soporta
		los últimos estándares, como CSS 3.
	</li>
	<li>Dolphin es el nuevo gestor de archivos de KDE. Ha sido desarrollado pensando en la
		usabilidad y es una herramienta fácil de usar, pero aun así potente.
	</li>
	<li>Con System Settings ha aparecido un nuevo interfaz de centro de control. El monitor
		KSysGuard hace que sea sencillo monitorizar y controlar los recursos del sistema y
		su actividad.
	</li>
	<li>Okular, el visor de documentos de KDE 4, soporta una gran cantidad de formatos de archivo.
		Okular es una de las muchas aplicaciones de KDE 4 que ha sido mejorada en colaboración
		con el <a href="http://openusability.org">Proyecto OpenUsability</a>.
	</li>
	<li>Las Aplicaciones Educativas están entre las primeras aplicaciones que han sido migradas
		y desarrolladas usando la tecnología KDE 4. Kalzium, una tabla periódica de elementos
		gráfica y el mundo de escritorio Marble son sólo dos de las muchas joyas dentro de las
		aplicaciones educativas. Lee más acerca de las Aplicaciones Educativas en su
		<a href="education.php">Guía Visual</a>.
	</li>
	<li>Montones de Juegos KDE han sido actualizados. Juegos de KDE como KMines, un juego de
		buscaminas y KPat, un juego de solitario, han recibido un lavado de cara. Gracias al
		nuevo estilo vectorial y a las capacidades gráficas, estos juegos se han hecho más
		independientes de la resolución de pantalla.
	</li>
</ul>
Algunas aplicaciones se presentan con más detalle en la <a href="applications.php">Guía Visual de KDE 4.0</a>.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center",
	"Filemanager, System Settings and Menu in action");
?>

<h3>Bibliotecas</h3>
<p>
<ul>
	<li>Phonon ofrece a las aplicaciones capacidades multimeda tales como reproducción de 
		sonido y vídeo. Internamente, Phonon hace uso de varios motores, intercambiables
		en tiempo de ejecución. El motor por defecto para KDE 4.0 será el motor Xine,
		que proporciona un impresionante soporte para distintos formatos. Phonon también permite
		al usuario escoger dispositivos de salida basándose en el tipo de multimedia.
	</li>

	<li>Solid, el <i>framework</i> de integración con el hardware, integra los dispositivos
		fijos y extraíbles en las aplicaciones KDE. Solid también hace de interfaz con las
		funciones de gestión de energía del sistema sobre el que corre, maneja la conectividad
		de red y la integración de dispositivos Bluetooth. Internamente, Solid combina los
		poderes de HAL, NetworkManager y la pila bluetooth Bluez, pero esos componentes son
		reemplazables sin romper las aplicaciones para proporcionar máxima portabilidad.
	</li>

	<li>KHTML es el motor de renderizado páginas web usado por Konqueror, el navegador web de KDE.
		KHTML es ligero y soporta estándares modernos como CSS 3. KHTML también fue el primer motor
		en pasar el famoso Acid 2 test.
	</li>

	<li>La biblioteca ThreadWeaver, que se incluye en kdelibs, proporciona una interfaz de
		alto nivel para hacer un mejor uso de los actuales sistemas multinúcleo, haciendo
		que las aplicaciones KDE funcionen con mayor suavidad y eficiencia, usando los recursos
		disponibles en el sistema.
	</li>

	<li>Construído sobre la biblioteca Qt 4 de Trolltech, KDE 4.0 puede hacer uso de las avanzadas
		habilidades visuales y del menor consumo de memoria de esta biblioteca. kdelibs proporciona
		una impresionante extensión de la biblioteca Qt, añadiendo mucha funcionalidad de alto nivel
		provechosa para el desarrollador.
	</li>
</ul>
</p>
<p>La biblioteca de conocimiento <a href="http://techbase.kde.org">TechBase</a> contiene más 
información acerca de las bibliotecas de KDE.</p>


<h4>Realiza una visita guiada...</h4>
<p>
La <a href="guide.php">Guía Visual de KDE 4.0</a> proporciona un breve resumen de varias
de las nuevas y mejoradas tecnologías de KDE 4.0. Ilustrada con multitud de capturas de
pantalla, te lleva a través de las distintas partes de KDE 4.0 y muestra algunas de las
fascinantes nuevas tecnologías y mejoras para el usuario. Comienzas con las nuevas
características del <a href="desktop.php">escritorio</a>, siendo presentadas después
<a href="applications.php">aplicaciones</a> como System Settings, el visor de documentos 
Okular y el gestor de archivos Dolphin. También se muestran <a href="education.php">
aplicaciones educativas</a>, así como <a href="games.php">juegos</a>.

</p>


<h4>Pruébalo...</h4>
<p>
Para aquellas personas interesadas en obtener paquetes para probar y ayudar,
varias distribuciones nos han notificado de que tendrán paquetes de KDE 4.0
disponibles en el momento del lanzamiento o poco tiempo después. La lista
completa y actualizada puede encontrarse en la <a href="http://www.kde.org/info/4.0.php">
página de información de KDE 4.0</a>, donde también puedes encontrar enlaces
al código fuente, información acerca de su compilación, seguridad y otros
aspectos.

</p>
<p>
Las siguientes distribuciones nos han notificado acerca de la disponibilidad de
paquetes o <i>Live CDs</i> de KDE 4.0:
The following distributions have notified us of the availability of packages or Live CDs for
KDE 4.0:

<ul>
	<li>
		Se espera en breve tras este lanzamiento una versión alfa de <strong>Arklinux 2008.1</strong>
		basada en KDE 4, con una versión final esperada en 3 o 4 semanas.
	</li>
	<li>
		<strong>Fedora</strong> incorporará KDE 4.0 en Fedora 9, que será <a
		href="http://fedoraproject.org/wiki/Releases/9">lanzada</a> en abril, con versiones
		alfa disponibles desde el 24 de enero. Los paquetes de KDE 4.0 están en el
		repositorio pre-alfa <a href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.

	</li>
	<li>
		<strong>Gentoo Linux</strong> proporciona ebuilds de KDE 4.0 en <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		<strong>Mandriva</strong> proporcionará paquetes para la versión 2008.0
		y trabaja en un Live CD con la última versión preliminar de 2008.1.
	</li>
	<li>
		Hay paquetes de <strong>openSUSE</strong> <a href="http://en.opensuse.org/KDE4">disponibles</a>
		para openSUSE 10.3 (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
		install</a>) y openSUSE 10.2. También está disponible un <a href="http://home.kde.org/~binner/kde-four-live/">
		Live CD de KDE 4</a> con estos paquetes. KDE 4.0 formará parte de la siguiente versión
		openSUSE 11.0.

	</li>
	<li>
		Los paquetes de <strong>Ubuntu</strong> se incluirán en la siguiente versión
		"Hardy Heron" (8.04) y también estarán disponibles como actualizaciones para la 
		estable "Gutsy Gibbon" (7.10). Está disponible un Live CD para probar KDE 4.0.
		Se pueden encontrar más detalles en el <a href="http://kubuntu.org/announcements/kde-4.0.php">
		anuncio</a> en Ubuntu.org.

	</li>
</ul>
</p>

<h2>Acerca de KDE 4</h2>
<p>
KDE 4.0 es un innovador escritorio de Software Libre que contiene cantidad de aplicaciones
de uso diario, así como para propósitos específicos. Plasma es un nuevo interfaz de escritorio
desarrollado para KDE 4, proporcionando una forma intuitiva de interactuar con el escritorio y
las aplicaciones. El navegador web Konqueror integra la web en el escritorio. El gestor de
archivos Dolphin, el lector de documentos Okular y el centro de control System Settings completan
el conjunto de escritorio básico. 

<br />
KDE está construido sobre las Bibliotecas KDE, las cuales proporcionan un acceso sencillo a los recursos
de la red mediante KIO y capacidades visuales avanzadas mediante Qt4. Phonon y Solid, que también son
parte de las Bibliotecas KDE, añaden un <i>framework</i> multimedia y mejor integración de hardware a
todas las aplicaciones KDE.

</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
