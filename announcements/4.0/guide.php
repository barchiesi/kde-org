<?php
  $page_title = "KDE 4.0 Visual Guide";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

Also available in:
<a href="http://www.kdecn.org/announcements/4.0/guide.php">Chinese</a>
<a href="http://fr.kde.org/announcements/4.0/guide.php">French</a>
<a href="guide-fa.php">Persian</a>


<p>
The KDE 4.0 Desktop and applications deserve a closer look. The pages below provide
an overview of KDE 4.0 and give some examples of its associated applications. Screenshots of many components are included. Be aware that this is just a small sample of what KDE 4.0
offers you.
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
	<td width="32">
		<a href="desktop.php"><img src="images/desktop-32.png" /></a>
	</td>
	<td>
		<a href="desktop.php"><strong>The Desktop</strong>: Plasma, KRunner, KickOff and KWin</a>
	</td>
</tr>
<tr>
	<td>
		<a href="applications.php"><img src="images/applications-32.png" /></a>
	</td>
	<td>
		<a href="applications.php"><strong>Applications</strong>: Dolphin, Okular, Gwenview, System Settings and Konsole</a>
	</td>
</tr>
<tr>
	<td>
		<a href="education.php"><img src="images/education-32.png" /></a>
	</td>
	<td>
		<a href="education.php"><strong>Educational Applications:</strong> Kalzium, Parley, Marble, Blinken, KStars and KTouch</a>
	</td>
</tr>
<tr>
	<td>
		<a href="games.php"><img src="images/games-32.png" /></a>
	</td>
	<td>
		<a href="games.php"><strong>Games</strong>: KGoldrunner, KFourInLine, LSkat, KJumpingCube, KSudoku and Konquest</a>
	</td>
</tr>
</table>
</div>

<p>
<br /><br /><br />
<em>The Visual Guide has been written by Sebastian K&uuml;gler and Jos Poortvliet.</em>
</p>
<?php
  include("footer.inc");
?>
