<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.7.0");
  $site_root = "../";
  $release = '5.7.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
February 14, 2015. KDE today announces the release
of KDE Frameworks 5.7.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<?php i18n("
<h3>General</h3>
");?>

<ul>
<li><?php i18n("A number of fixes for compiling with the upcoming Qt 5.5");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fixed starting and stopping activities");?></li>
<li><?php i18n("Fixed activity preview showing wrong wallpaper occasionally");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Create temporary files in the temp dir rather than in the current directory");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Fixed generation of KAuth DBus helper service files");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Fixed assert when dbus paths contain a '.'");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Added support for CP949 to KCharsets");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconf_update no longer processes *.upd file from KDE SC 4. Add \"Version=5\" to top of the upd file for updates that should be applied to Qt5/KF5 applications");?></li>
<li><?php i18n("Fixed KCoreConfigSkeleton when toggling a value with saves in between");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("KRecentFilesAction: fixed menu entry order (so it matches the kdelibs4 order)");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KAboutData: Call addHelpOption and addVersionOption automatically, for convenience and consistency");?></li>
<li><?php i18n("KAboutData: Bring back \"Please use http://bugs.kde.org to report bugs.\" when no other email/url is set");?></li>
<li><?php i18n("KAutoSaveFile: allStaleFiles() now works as expected for local files, fixed staleFiles() too");?></li>
<li><?php i18n("KRandomSequence now uses int's internally and exposes int-api for 64-bit unambiguity");?></li>
<li><?php i18n("Mimetype definitions: *.qmltypes and *.qmlproject files also have the text/x-qml mime type");?></li>
<li><?php i18n("KShell: make quoteArgs quote urls with QChar::isSpace(), unusual space characters were not handled properly");?></li>
<li><?php i18n("KSharedDataCache: fix creation of directory containing the cache (porting bug)");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Added helper method KDEDModule::moduleForMessage for writing more kded-like daemons, such as kiod");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Added a plotter component");?></li>
<li><?php i18n("Added overload method for Formats::formatDuration taking int");?></li>
<li><?php i18n("New properties paintedWidth and paintedHeight added to QPixmapItem and QImageItem");?></li>
<li><?php i18n("Fixed painting QImageItem and QPixmapItem");?></li>
</ul>

<h3><?php i18n("Kded");?></h3>

<ul>
<li><?php i18n("Add support for loading kded modules with JSON metadata");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Now includes the runtime component, making this a tier3 framework");?></li>
<li><?php i18n("Made the Windows backend work again");?></li>
<li><?php i18n("Re-enabled the Mac backend");?></li>
<li><?php i18n("Fixed crash in KGlobalAccel X11 runtime shutdown");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Mark results as required to warn when API is misused");?></li>
<li><?php i18n("Added BUILD_WITH_QTSCRIPT buildsystem option to allow a reduced feature-set on embedded systems");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("OSX: load the correct shared libraries at runtime");?></li>
<li><?php i18n("Mingw compilation fixes");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fixed crash in jobs when linking to KIOWidgets but only using a QCoreApplication");?></li>
<li><?php i18n("Fixed editing web shortcuts");?></li>
<li><?php i18n("Added option KIOCORE_ONLY, to compile only KIOCore and its helper programs, but not KIOWidgets or KIOFileWidgets, thus reducing greatly the necessary dependencies");?></li>
<li><?php i18n("Added class KFileCopyToMenu, which adds Copy To / Move To\" to popupmenus");?></li>
<li><?php i18n("SSL-enabled protocols: added support for TLSv1.1 and TLSv1.2 protocols, remove SSLv3");?></li>
<li><?php i18n("Fixed negotiatedSslVersion and negotiatedSslVersionName to return the actual negotiated protocol");?></li>
<li><?php i18n("Apply the entered URL to the view when clicking the button that switches the URL navigator back to breadcrumb mode");?></li>
<li><?php i18n("Fixed two progress bars/dialogs appearing for copy/move jobs");?></li>
<li><?php i18n("KIO now uses its own daemon, kiod, for out-of-process services previously running in kded, in order to reduce dependencies; currently only replaces kssld");?></li>
<li><?php i18n("Fixed \"Could not write to &lt;path&gt;\" error when kioexec is triggered");?></li>
<li><?php i18n("Fixed \"QFileInfo::absolutePath: Constructed with empty filename\" warnings when using KFilePlacesModel");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Fixed KRecursiveFilterProxyModel for Qt 5.5.0+, due to QSortFilterProxyModel now using the roles parameter to the dataChanged signal");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Always reload xml data from remote urls");?></li>
</ul>

<h3><?php i18n("KNotifications");?></h3>

<ul>
<li><?php i18n("Documentation: mentioned the file name requirements of .notifyrc files");?></li>
<li><?php i18n("Fixed dangling pointer to KNotification");?></li>
<li><?php i18n("Fixed leak of knotifyconfig");?></li>
<li><?php i18n("Install missing knotifyconfig header");?></li>
</ul>

<h3><?php i18n("KPackage");?></h3>

<ul>
<li><?php i18n("Renamed kpackagetool man to kpackagetool5");?></li>
<li><?php i18n("Fixed installation on case-insensitive filesystems");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Fixed Kross::MetaFunction so it works with Qt5's metaobject system");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Include unknown properties when converting KPluginInfo from KService");?></li>
<li><?php i18n("KPluginInfo: fixed properties not being copied from KService::Ptr");?></li>
<li><?php i18n("OS X: performance fix for kbuildsycoca4 (skip app bundles)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fixed high-precision touchpad scrolling");?></li>
<li><?php i18n("Do not emit documentUrlChanged during reload");?></li>
<li><?php i18n("Do not break cursor position on document reload in lines with tabs");?></li>
<li><?php i18n("Do not re(un)fold the first line if it was manually (un)folded");?></li>
<li><?php i18n("vimode: command history through arrow keys");?></li>
<li><?php i18n("Do not try to create a digest when we get a KDirWatch::deleted() signal");?></li>
<li><?php i18n("Performance: remove global initializations");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Fixed infinite recursion in Unit::setUnitMultiplier");?></li>
</ul>

<h3><?php i18n("KWallet");?></h3>

<ul>
<li><?php i18n("Automatically detect and convert old ECB wallets to CBC");?></li>
<li><?php i18n("Fixed the CBC encryption algorithm");?></li>
<li><?php i18n("Ensured wallet list gets updated when a wallet file gets removed from disk");?></li>
<li><?php i18n("Remove stray &lt;/p&gt; in user-visible text");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Use kstyleextensions to specify custom control element for rendering kcapacity bar when supported, this allow the widget to be styled properly");?></li>
<li><?php i18n("Provide an accessible name for KLed");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Fixed NETRootInfo::setShowingDesktop(bool) not working on Openbox");?></li>
<li><?php i18n("Added convenience method KWindowSystem::setShowingDesktop(bool)");?></li>
<li><?php i18n("Fixes in icon format handling");?></li>
<li><?php i18n("Added method NETWinInfo::icccmIconPixmap provides icon pixmap from WM_HINTS property");?></li>
<li><?php i18n("Added overload to KWindowSystem::icon which reduces roundtrips to X-Server");?></li>
<li><?php i18n("Added support for _NET_WM_OPAQUE_REGION");?></li>
</ul>

<h3><?php i18n("NetworkmanagerQt");?></h3>

<ul>
<li><?php i18n("Do not print a message about unhandled \"AccessPoints\" property");?></li>
<li><?php i18n("Added support for NetworkManager 1.0.0 (not required)");?></li>
<li><?php i18n("Fixed VpnSetting secrets handling");?></li>
<li><?php i18n("Added class GenericSetting for connections not managed by NetworkManager");?></li>
<li><?php i18n("Added property AutoconnectPriority to ConnectionSettings");?></li>
</ul>

<h4>Plasma framework</h4>

<ul>
<li><?php i18n("Fixed errorneously opening a broken context menu when middle clicking Plasma popup");?></li>
<li><?php i18n("Trigger button switch on mouse wheel");?></li>
<li><?php i18n("Never resize a dialog bigger than the screen");?></li>
<li><?php i18n("Undelete panels when applet gets undeleted");?></li>
<li><?php i18n("Fixed keyboard shortcuts");?></li>
<li><?php i18n("Restore hint-apply-color-scheme support");?></li>
<li><?php i18n("Reload the configuration when plasmarc changes");?></li>
<li><?php i18n("...");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Added energyFull and energyFullDesign to Battery");?></li>
</ul>

<h3><?php i18n("Buildsystem changes (extra-cmake-modules)");?></h3>

<ul>
<li><?php i18n("New ECMUninstallTarget module to create an uninstall target");?></li>
<li><?php i18n("Make KDECMakeSettings import ECMUninstallTarget by default");?></li>
<li><?php i18n("KDEInstallDirs: warn about mixing relative and absolute installation paths on the command line");?></li>
<li><?php i18n("Added ECMAddAppIcon module to add icons to executable targets on Windows and Mac OS X");?></li>
<li><?php i18n("Fixed CMP0053 warning with CMake 3.1");?></li>
<li><?php i18n("Do not unset cache variables in KDEInstallDirs");?></li>
</ul>

<h3><?php i18n("Frameworkintegration");?></h3>

<ul>
<li><?php i18n("Fix updating of single click setting at runtime");?></li>
<li><?php i18n("Multiple fixes to the systemtray integration");?></li>
<li><?php i18n("Only install color scheme on toplevel widgets (to fix QQuickWidgets)");?></li>
<li><?php i18n("Update XCursor settings on X11 platform");?></li>
</ul>

<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "$5.7");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
