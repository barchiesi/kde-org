<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.22.0");
  $site_root = "../";
  $release = '5.22.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
May 15, 2016. KDE today announces the release
of KDE Frameworks 5.22.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Properly check if a URL is a local file");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Compilation fixes for Windows");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Many new action and application icons.");?></li>
<li><?php i18n("Specify offered extensions as per change in kiconthemes");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Android deployment: support projects without things in share or lib/qml (bug 362578)");?></li>
<li><?php i18n("Enables KDE_INSTALL_USE_QT_SYS_PATHS if CMAKE_INSTALL_PREFIX Qt5 prefix");?></li>
<li><?php i18n("ecm_qt_declare_logging_category: improve error message when using without including");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Remove platformtheme plugin as it's in plasma-integration");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Provide a way to disable inotify use in KDirWatch");?></li>
<li><?php i18n("Fix KAboutData::applicationData() to init from current Q*Application metadata");?></li>
<li><?php i18n("Make clear that KRandom is not recommended for cryptography purposes");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("KDBusService: turn '-' into '_' in object paths");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Don't crash if we have no openGL context");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Provide a fallback MAXPATHLEN if not defined");?></li>
<li><?php i18n("Fix KDateTime::isValid() for ClockTime values (bug 336738)");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Added entity applications");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Merge branch 'externalextractors'");?></li>
<li><?php i18n("Fixed external plugins and tests");?></li>
<li><?php i18n("Added support for external writer plugins");?></li>
<li><?php i18n("Added writer plugin support");?></li>
<li><?php i18n("Add external extractor plugin support");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Implement toString for Uint8ArrayConstructor and friends");?></li>
<li><?php i18n("Merge in several Coverity-related fixes");?></li>
<li><?php i18n("Correctly use QCache::insert");?></li>
<li><?php i18n("Fix some memory leaks");?></li>
<li><?php i18n("Sanity check CSS web font parsing, avoid potential mem leak");?></li>
<li><?php i18n("dom: Add tag priorities for 'comment' tag");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("libgettext: Fix potential use-after-free using non-g++ compilers");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Use appropriate container for internal pointer array");?></li>
<li><?php i18n("Add opportunity to reduce unneeded disk accesses, introduces KDE-Extensions");?></li>
<li><?php i18n("Save some disk accesses");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("kurlnavigatortoolbutton.cpp - use buttonWidth in paintEvent()");?></li>
<li><?php i18n("New file menu: filter out duplicates (e.g. between .qrc and system files) (bug 355390)");?></li>
<li><?php i18n("Fix error message on startup of the cookies KCM");?></li>
<li><?php i18n("Remove kmailservice5, it can only do harm at this point (bug 354151)");?></li>
<li><?php i18n("Fix KFileItem::refresh() for symlinks. The wrong size, filetype and permissions were being set");?></li>
<li><?php i18n("Fix regression in KFileItem: refresh() would lose the file type, so a dir became a file (bug 353195)");?></li>
<li><?php i18n("Set text on QCheckbox widget rather than using a separate label (bug 245580)");?></li>
<li><?php i18n("Don't enable acl permissions widget if we don't own the file (bug 245580)");?></li>
<li><?php i18n("Fix double-slash in KUriFilter results when a name filter is set");?></li>
<li><?php i18n("KUrlRequester: add signal textEdited (forwarded from QLineEdit)");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Fix template syntax for test case generation");?></li>
<li><?php i18n("Fix linking with Qt 5.4 (wrongly placed #endif)");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Fix the layout of the BrowserOpenOrSaveQuestion dialogue");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Add a check for PersonData being valid");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Fix metainfo.yaml: KRunner is neither a porting aid nor deprecated");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Remove too-strict maximum string length in KSycoca database");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Use proper char syntax '\"' instead of '\\\"'");?></li>
<li><?php i18n("doxygen.xml: Use default style dsAnnotation for \"Custom Tags\" as well (less hard-coded colors)");?></li>
<li><?php i18n("Add option to show the counter of words");?></li>
<li><?php i18n("Improved foreground color contrast for search &amp; replace highlightings");?></li>
<li><?php i18n("Fix crash when closing Kate through dbus while the print dialog is open (bug #356813) (bug 356813)");?></li>
<li><?php i18n("Cursor::isValid(): add note about isValidTextPosition()");?></li>
<li><?php i18n("Add API {Cursor, Range}::{toString, static fromString}");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Inform the client if we don't know the conversion rate");?></li>
<li><?php i18n("Add ILS (Israeli New Shekel) currency (bug 336016)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("disable seession restore for kwalletd5");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KNewPasswordWidget: Remove size hint on spacer, which was leading to some always empty space in the layout");?></li>
<li><?php i18n("KNewPasswordWidget: fix QPalette when widget is disabled");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Fix generation of path to xcb plugin");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[QuickTheme] Fix properties");?></li>
<li><?php i18n("highlight/highlightedText from proper color group");?></li>
<li><?php i18n("ConfigModel: Don't try to resolve empty source path from package");?></li>
<li><?php i18n("[calendar] Only show the events mark on days grid, not month or year");?></li>
<li><?php i18n("declarativeimports/core/windowthumbnail.h - fix -Wreorder warning");?></li>
<li><?php i18n("reload icon theme properly");?></li>
<li><?php i18n("Always write the theme name to plasmarc, also if the default theme is chosen");?></li>
<li><?php i18n("[calendar] Add a mark to days containing an event");?></li>
<li><?php i18n("add Positive, Neutral, Negative text colors");?></li>
<li><?php i18n("ScrollArea: Fix warning when contentItem is not Flickable");?></li>
<li><?php i18n("Add a prop and method for aligning the menu against a corner of its visual parent");?></li>
<li><?php i18n("Allow setting minimum width on Menu");?></li>
<li><?php i18n("Maintain order in stored item list");?></li>
<li><?php i18n("Extend API to allow (re)positioning menu items during procedural insert");?></li>
<li><?php i18n("bind highlightedText color in Plasma::Theme");?></li>
<li><?php i18n("Fix unsetting associated application/urls for Plasma::Applets");?></li>
<li><?php i18n("Don't expose symbols of private class DataEngineManager");?></li>
<li><?php i18n("add an \"event\" element in the calendar svg");?></li>
<li><?php i18n("SortFilterModel: Invalidate filter when changing filter callback");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Install parsetrigrams tool for cross compiling");?></li>
<li><?php i18n("hunspell: Load/Store a personal dictionary");?></li>
<li><?php i18n("Support hunspell 1.4");?></li>
<li><?php i18n("configwidget: notify about changed config when ignored words updated");?></li>
<li><?php i18n("settings: don't immediately save the config when updating ignore list");?></li>
<li><?php i18n("configwidget: fix saving when ignore words updated");?></li>
<li><?php i18n("Fix failed to save ignore word issue (bug 355973)");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.22");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.4");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
