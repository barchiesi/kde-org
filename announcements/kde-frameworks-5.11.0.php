<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.11.0");
  $site_root = "../";
  $release = '5.11.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
June 12, 2015. KDE today announces the release
of KDE Frameworks 5.11.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("New arguments for ecm_add_tests(). (bug 345797)");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Use the correct initialDirectory for the KDirSelectDialog");?></li>
<li><?php i18n("Make sure the scheme is specified when overriding the start url value");?></li>
<li><?php i18n("Only accept existing directories in FileMode::Directory mode");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<p>(no changelog provided)</p>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Make KAUTH_HELPER_INSTALL_ABSOLUTE_DIR available to all KAuth users");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("KEmailAddress: Add overload for extractEmailAddress and firstEmailAddress which returns an error message.");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Fix unwanted selection when editing the filename in the file dialog (bug 344525)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Prevent crash if QWindow::screen() is null");?></li>
<li><?php i18n("Add KConfigGui::setSessionConfig() (bug 346768)");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("New KPluginLoader::findPluginById() convenience API");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("support creation of ConfigModule from KPluginMetdata");?></li>
<li><?php i18n("fix pressAndhold events");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Use QTemporaryFile instead of hardcoding a temporary file.");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update translations");?></li>
<li><?php i18n("Update customization/ru");?></li>
<li><?php i18n("Fix entities with wrong links");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Cache the theme in the integration plugin");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("[runtime] Move platform specific code into plugins");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Optimize KIconEngine::availableSizes()");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Do not try to complete users and assert when prepend is non-empty. (bug 346920)");?></li>
<li><?php i18n("Use KPluginLoader::factory() when loading KIO::DndPopupMenuPlugin");?></li>
<li><?php i18n("Fix deadlock when using network proxies (bug 346214)");?></li>
<li><?php i18n("Fixed KIO::suggestName to preserve file extensions");?></li>
<li><?php i18n("Kick off kbuildsycoca4 when updating sycoca5.");?></li>
<li><?php i18n("KFileWidget: Don't accept files in directory only mode");?></li>
<li><?php i18n("KIO::AccessManager: Make it possible to treat sequential QIODevice asynchronously");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add new method fillMenuFromGroupingNames");?></li>
<li><?php i18n("KMoreTools: add many new groupings");?></li>
<li><?php i18n("KMoreToolsMenuFactory: handling for \"git-clients-and-actions\"");?></li>
<li><?php i18n("createMenuFromGroupingNames: make url parameter optional");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix crash in NotifyByExecute when no widget has been set (bug 348510)");?></li>
<li><?php i18n("Improve handling of notifications being closed (bug 342752)");?></li>
<li><?php i18n("Replace QDesktopWidget usage with QScreen");?></li>
<li><?php i18n("Ensure KNotification can be used from a non-GUI thread");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Guard the structure qpointer access (bug 347231)");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Use QTemporaryFile instead of hardcoding /tmp.");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Use tcgetattr &amp; tcsetattr if available");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Fix loading of Kross modules \"forms\" and \"kdetranslation\"");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("When running as root preserve file ownership on existing cache files (bug 342438)");?></li>
<li><?php i18n("Guard against being unable to open stream (bug 342438)");?></li>
<li><?php i18n("Fix check for invalid permissions writing file (bug 342438)");?></li>
<li><?php i18n("Fix querying ksycoca for x-scheme-handler/* pseudo-mimetypes. (bug 347353)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Allow like in KDE 4.x times 3rdparty apps/plugins to install own highlighting XML files into katepart5/syntax");?></li>
<li><?php i18n("Add KTextEditor::Document::searchText()");?></li>
<li><?php i18n("Bring back use of KEncodingFileDialog (bug 343255)");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Add a method to clear decorator");?></li>
<li><?php i18n("Allow to use custom sonnet decorator");?></li>
<li><?php i18n("Implement \"find previous\" in KTextEdit.");?></li>
<li><?php i18n("Re-add support for speech-to-text");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KAssistantDialog: Re-add the Help button that was present in KDELibs4 version");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Add session management for KMainWindow (bug 346768)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Drop WiMAX support for NM 1.2.0+");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Calendar components can now display week numbers (bug 338195)");?></li>
<li><?php i18n("Use QtRendering for fonts in password fields");?></li>
<li><?php i18n("Fix AssociatedApplicationManager lookup when a mimetype has (bug 340326)");?></li>
<li><?php i18n("Fix panel background coloring (bug 347143)");?></li>
<li><?php i18n("Get rid of \"Could not load applet\" message");?></li>
<li><?php i18n("Capability to load QML kcms in plasmoid config windows");?></li>
<li><?php i18n("Don't use the DataEngineStructure for Applets");?></li>
<li><?php i18n("Port libplasma away from sycoca as much as possible");?></li>
<li><?php i18n("[plasmacomponents] Make SectionScroller follow the ListView.section.criteria");?></li>
<li><?php i18n("Scroll bars no longer automatically hide when a touch screen is present (bug 347254)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Use one central cache for the SpellerPlugins.");?></li>
<li><?php i18n("Reduce temporary allocations.");?></li>
<li><?php i18n("Optimize: Do not wipe dict cache when copying speller objects.");?></li>
<li><?php i18n("Optimise away save() calls by calling it once at the end if needed.");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
