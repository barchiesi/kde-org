<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.39.0");
  $site_root = "../";
  $release = '5.39.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
October 14, 2017. KDE today announces the release
of KDE Frameworks 5.39.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Only match real MIME types, not e.g. \"raw CD image\" (bug 364884)");?></li>
<li><?php i18n("Remove pf.path() from container before the reference got screwed up by it.remove()");?></li>
<li><?php i18n("Fix tags KIO-slave protocol description");?></li>
<li><?php i18n("Consider markdown files to be Documents");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("add overflow-menu icon (bug 385171)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Fix python bindings compilation after 7af93dd23873d0b9cdbac192949e7e5114940aa6");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Make KStandardGuiItem::discard match QDialogButtonBox::Discard");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Changed the default query limit to zero");?></li>
<li><?php i18n("Added the option to enable model tester");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Make KCMultiDialog scrollable (bug 354227)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Deprecate KStandardShortcut::SaveOptions");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Deprecate KStandardAction::PasteText and KPasteTextAction");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("desktoptojson: Improve legacy service type detection heuristic (bug 384037)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Relicense to LGPL2.1+");?></li>
<li><?php i18n("Added openService() method to KRunProxy");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("fix crash when more than one instances of ExtractorCollection are destructed");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Revert \"KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys\" (bug 384597)");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("add a method to reset the custom palette");?></li>
<li><?php i18n("use qApp-&gt;palette() when no custom one is set");?></li>
<li><?php i18n("allocate the proper buffer size");?></li>
<li><?php i18n("allow to set a custom palette instead of colorSets");?></li>
<li><?php i18n("expose the colorset for the stylesheet");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Windows: Fix 'klauncher uses absolute compile time install path for finding kioslave.exe'");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("kioexec: Watch the file when it has finished copying (bug 384500)");?></li>
<li><?php i18n("KFileItemDelegate: Always reserve space for icons (bug 372207)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("don't instantiate Theme file in BasicTheme");?></li>
<li><?php i18n("add a new Forward button");?></li>
<li><?php i18n("less contrast to the sheet scrollbar background");?></li>
<li><?php i18n("more reliable insert and remove from overflow menu");?></li>
<li><?php i18n("better context icon rendering");?></li>
<li><?php i18n("more careful to center the action button");?></li>
<li><?php i18n("use iconsizes for action buttons");?></li>
<li><?php i18n("pixel perfect icon sizes on desktop");?></li>
<li><?php i18n("selected effect to fake handle icon");?></li>
<li><?php i18n("fix color of handles");?></li>
<li><?php i18n("better color for the main action button");?></li>
<li><?php i18n("fix context menu for desktop style");?></li>
<li><?php i18n("better \"more\" menu for the toolbar");?></li>
<li><?php i18n("a proper menu for the intermediate pages context menu");?></li>
<li><?php i18n("add a text field which should bring up a keypad");?></li>
<li><?php i18n("don't crash when launched with non existent styles");?></li>
<li><?php i18n("ColorSet concept in Theme");?></li>
<li><?php i18n("simplify wheel management (bug 384704)");?></li>
<li><?php i18n("new example app with desktop/mobile main qml files");?></li>
<li><?php i18n("ensure currentIndex is valid");?></li>
<li><?php i18n("Generate the appstream metadata of the gallery app");?></li>
<li><?php i18n("Look for QtGraphicalEffects, so packagers don't forget it");?></li>
<li><?php i18n("Don't include the control over the bottom decoration (bug 384913)");?></li>
<li><?php i18n("lighter coloring when listview has no activeFocus");?></li>
<li><?php i18n("some support for RTL layouts");?></li>
<li><?php i18n("Disable shortcuts when an action is disabled");?></li>
<li><?php i18n("create the whole plugin structure in the build directory");?></li>
<li><?php i18n("fix accessibility for the gallery main page");?></li>
<li><?php i18n("If plasma isn't available, KF5Plasma isn't either. Should fix the CI error");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Require Kirigami 2.1 instead of 1.0 for KNewStuffQuick");?></li>
<li><?php i18n("Properly create KPixmapSequence");?></li>
<li><?php i18n("Don't complain the knsregistry file is not present before it's useful");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("kpackage: bundle a copy of servicetypes/kpackage-generic.desktop");?></li>
<li><?php i18n("kpackagetool: bundle a copy of servicetypes/kpackage-generic.desktop");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("KPartsApp template: fix install location of kpart desktop file");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Ignore default mark in icon border for single selectable mark");?></li>
<li><?php i18n("Use QActionGroup for input mode selection");?></li>
<li><?php i18n("Fix missing spell check bar (bug 359682)");?></li>
<li><?php i18n("Fix the fall-back \"blackness\" value for unicode &gt; 255 characters (bug 385336)");?></li>
<li><?php i18n("Fix trailing space visualization for RTL lines");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Only send OutputConfig sendApplied / sendFailed to the right resource");?></li>
<li><?php i18n("Don't crash if a client (legally) uses deleted global contrast manager");?></li>
<li><?php i18n("Support XDG v6");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KAcceleratorManager: set icon text on actions to remove CJK markers (bug 377859)");?></li>
<li><?php i18n("KSqueezedTextLabel: Squeeze text when changing indent or margin");?></li>
<li><?php i18n("Use edit-delete icon for destructive discard action (bug 385158)");?></li>
<li><?php i18n("Fix Bug 306944 - Using the mousewheel to increment/decrement the dates (bug 306944)");?></li>
<li><?php i18n("KMessageBox: Use question mark icon for question dialogs");?></li>
<li><?php i18n("KSqueezedTextLabel: Respect indent, margin and frame width");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix KToolBar repaint loop (bug 377859)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fix org.kde.plasma.calendar with Qt 5.10");?></li>
<li><?php i18n("[FrameSvgItem] Iterate child nodes properly");?></li>
<li><?php i18n("[Containment Interface] Don't add containment actions to applet actions on desktop");?></li>
<li><?php i18n("Add new component for the greyed out labels in Item Delegates");?></li>
<li><?php i18n("Fix FrameSVGItem with the software renderer");?></li>
<li><?php i18n("Don't animate IconItem in software mode");?></li>
<li><?php i18n("[FrameSvg] Use new-style connect");?></li>
<li><?php i18n("possibility to set an attached colorscope to not inherit");?></li>
<li><?php i18n("Add extra visual indicator for Checkbox/Radio keyboard focus");?></li>
<li><?php i18n("don't recreate a null pixmap");?></li>
<li><?php i18n("Pass item to rootObject() since it's now a singleton (bug 384776)");?></li>
<li><?php i18n("Don't list tab names twice");?></li>
<li><?php i18n("don't accept active focus on tab");?></li>
<li><?php i18n("register revision 1 for QQuickItem");?></li>
<li><?php i18n("[Plasma Components 3] Fix RTL in some widgets");?></li>
<li><?php i18n("Fix invalid id in viewitem");?></li>
<li><?php i18n("update mail notification icon for better contrast (bug 365297)");?></li>
</ul>

<h3><?php i18n("qqc2-desktop-style");?></h3>

<p>New module:
QtQuickControls 2 style that uses QWidget's QStyle for painting
This makes it possible to achieve an higher deree of consistency between QWidget-based and QML-based apps.</p>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[solid/fstab] Add support for x-gvfs style options in fstab");?></li>
<li><?php i18n("[solid/fstab] Swap vendor and product properties, allow i18n of description");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix invalid itemData references of 57 highlighting files");?></li>
<li><?php i18n("Add support for custom search paths for application-specific syntax and theme definitions");?></li>
<li><?php i18n("AppArmor: fix DBus rules");?></li>
<li><?php i18n("Highlighting indexer: factor out checks for smaller while loop");?></li>
<li><?php i18n("ContextChecker: support '!' context switching and fallthroughContext");?></li>
<li><?php i18n("Highlighting indexer: check existence of referenced context names");?></li>
<li><?php i18n("Relicense qmake highlighting to MIT license");?></li>
<li><?php i18n("Let qmake highlighting win over Prolog for .pro files (bug 383349)");?></li>
<li><?php i18n("Support clojure's \"@\" macro with brackets");?></li>
<li><?php i18n("Add syntax highlighting for AppArmor Profiles");?></li>
<li><?php i18n("Highlighting indexer: Catch invalid a-Z/A-z ranges in regexps");?></li>
<li><?php i18n("Fixing incorrectly capitalized ranges in regexps");?></li>
<li><?php i18n("add missing reference files for tests, looks ok, I think");?></li>
<li><?php i18n("Added Intel HEX file support for the Syntax highlighting database");?></li>
<li><?php i18n("Disable spell checking for strings in Sieve scripts");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Fix memory leak");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.39");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
