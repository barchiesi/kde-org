<?php
  $page_title = "KDE 4.3.0 Caizeni väljalasketeade";
  $site_root = "../";
  include "header.inc";
?>

Teistes keeltes:
<?php
  $release = '4.3';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE jätkab 4.3 väljalaskega uuenduste rajal
</h3>

<p align="justify">
  <strong>
    KDE 4.3 (koodnimetusega <i>"Caizen"</i>) pakub uuendusi nii vaba töökeskkonna kasutajatele kui ka tarkvaraarendajatele
  </strong>
</p>

<p align="justify">
4. august 2009. <a href=http://www.kde.org/>KDE kogukond</a> annab täna teada <i>"Caizeni"</i> (ehk KDE 4.3) kohesest kättesaadavusest. Uus väljalase pakub hulganisti parandusi nii KDE kasutajatele kui ka arendajatele. KDE 4.3 lihvib varasemate väljalasetega kaasnenud ainulaadseid võimalusi ning toob kaasa mitmeid uuendusi. Kuigi juba KDE 4.2 oli mõeldud kõigile kodukasutajatele, kujutab KDE 4.3 lisaks sellele ühtlasi senisest veel stabiilsemat ja täielikumat toodet ka kodu- ja väikekontoritele.
</p>
<p align=justify>
  KDE kogukond on viimase poole aastaga <strong>parandanud üle 10 000 vea</strong> ning <strong>täitnud peaaegu 2000 uute võimaluste soovi</strong>. Pea 700 inimest on teinud koodi ligikaudu 63 000 muudatust. Lugege edasi ja saate teada, millised muudatused kaasnevad KDE 4.3 töökeskkonnaga, selle raames pakutavate rakendustega ning arendusplatvormiga.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/desktop.png"><img src="screenshots/desktop_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>KDE 4.3 töölaud</em></div>

<h3>
  Töölaud: paranenud jõudlus ja hõlpsam kasutamine
</h3>
<br>
<p align=justify>
    KDE töökeskkond pakub kõikehõlmavat ja tõelist töölauakogemust, sobides suurepäraselt kokku kõigi Linuxi ja Unixi operatsioonisüsteemidega. KDE töökeskkonna tähtsaimad elemendid on järgmised:
<ul>
  <li>
    <strong>KWin</strong> - võimas aknahaldur, mis pakub moodsaid ruumilisi graafilisi efekte
  </li>
  <li>
    <strong>Plasma töölauakest</strong> - erutavalt uudne töölaua- ja paneelisüsteem, mille arvukad kohandatavad vidinad soodustavad edukat töötegemist nii oma arvutis kui ka koostöös teistega üle võrgu
  </li>
  <li>
    <strong>Dolphin</strong> - kasutajasõbralik ning võrgu- ja sisutundlik failihaldur
  </li>
  <li>
    <strong>KRunner</strong> - otsingu- ja käivitussüsteem käskude käivitamiseks ja vajaliku teabe otsimiseks ning leidmiseks
  </li>
  <li>
    töölaua ja kogu süsteemi lihtne kohandamine <strong>Süsteemi seadistuste</strong> abil
  </li>
</ul>
Allpool on lühidalt ära toodud tähtsamad KDE töökeskkonna uuendused ja parandused.
<ul>
  <li>
    <a href="http://plasma.kde.org">Plasma töölauakestale</a> on loodud <b>uus vaiketeema</b> Air. Air näeb välja palju õhulisem ning sobib paremini kokku rakenduste vaiketeemaga. Samuti on tunduvalt parandatud Plasma <b>jõudlust</b>. Mälukasutus on märksa väiksem, animatsioonid sujuvamad. <b>Tegevusi saab nüüd seostada virtuaalsete töölaudadega</b>, mis lubab kasutajal asetada igale töölauale erinevad vidinad. Lisaks on Plasma juures parandatud <b>tööde ja märguannete haldust</b>. Käimasolevad tööd on rühmitatud ühtse edenemisriba alla, et vältida liiga paljude dialoogide ilmumist. Animatsioonide abil antakse märku, et tööd veel käivad, libistades dialoogid sujuvalt süsteemisalve ja animeerides märguannete ikooni. Plasma väiksemate muudatuste hulka kuuluvad <b>täielikult seadistatavad kiirklahvid</b> ja senisest suuremad võimalused klaviatuuri abil liikumiseks, võimalus luua Plasma vidin vajalikku sisu töölauale lohistades ning terve hulk <b>uusi ja parandusi saanud Plasma vidinaid</b>. Kataloogivaate vidin võimaldab nüüd kasutajal <b>heita pilk kataloogi hiirekursorit selle kohale viies</b> ning <b>uus Translatoidi vidin</b> tõlgib Google Translate'i vahendusel sõnu ja lauseid otse töölaual. KRunneri <b>pluginate omadused on hõlpsamini leitavad</b> tänu tulemusalasse paigutatud abinupule, mis näitab käskude süntaksit. <b>Toimingutele on lisatud seadistamisvõimalus</b>, mis lubab näiteks käivitada rakendusi mõne teise kasutaja õigustes.<br>
    <br>
    </li>
  <li>
    Failihaldur Dolphin näitab <b>kataloogis leiduvate failide väikeseid eelvaatlusi</b>, samuti video-pisipilte, mis aitavad paremini aru saada, millega on tegemist. <b>Prügikasti saab nüüd seadistada</b> Dolphini seadistustemenüü abil ja mitmesuguste prügikasti suurusele määratavate seadistatavate piirangutega saab tagada, et arvuti ketas ei saaks failide kustutamisel ootamatult täis. Elemendil hiire parema nupu klõpsuga avanev menüü on nüüd seadistatav ning ka kogu seadistustedialoog on elanud üle muudatusi, mis peaksid <b>kasutamist lihtsustama</b>. Uus <b>network:/ asukoht</b> näitab teisi võrgus olevaid arvuteid ja teenuseid (praegu on see piiratud nendega, mille olemasolu teatatakse DNS-SD/zeroconf protokollidega, aga tulevastes versioonides toetus laieneb).<br>
    <br>
  </li>
  <li>
    Töökeskkonna mitmelaadne viimistlemine muudab arvuti kasutamise veel lihtsamaks. <b>Senisest kiiremad Süsteemi seadistused</b> pakuvad <b>lisavõimalusena puuvaadet</b> ning hulk parandusi on tehtud ka konkreetsetesse seadistusmoodulitesse. <b>Uued efektid</b>, näiteks "Ärakerimine" ja "Tagasiliuglemine", ning <b>KWini</b> üldine <b>parem jõudlus</b> muudavad akende haldamise märksa sujuvamaks, lõimimine Plasma teemadega tagab aga senisest ühtsema välimuse. Lõikepuhvrisse kopeeritud andmete ajalugu säilitav tööriist <b>Klipper</b> suudab nüüd <b>käituda nutikalt vastavalt sisule</b>. Tööriist määrab automaatselt kindlaks rakendused, mis suudavad käsitleda lõikepuhvrisse kopeeritud sisu, ning võimaldab kasutajal kohe vajaliku rakenduse käivitada.<br>
    <br>
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hIsigZW3agI" type="application/x-shockwave-flash" width="480" height="390" allowscriptaccess="always" allowfullscreen="true"></embed>
<br>
<a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">Ogg Theora versioon</a></div>
<h3>
  Rakendused: hüpe edasi
</h3>
<p align=justify>
KDE kogukond pakub paljusid võimekaid rakendusi, mis kasutavad täielikult ära KDE rakenduste raamistiku võimalusi. KDE tarkvarakomplekti on lisatud terve valik rakendusi, mis jagunevad kategooriati alarühmadeks. Olulisemad neist on järgmised:
</p>
<ul>
  <li>
    KDE võrgurakendused
  </li>
  <li>
    KDE multimeedia
  </li>
  <li>
    KDE graafikatööriistad
  </li>
  <li>
    KDE PIM-komplekt (personaalse teabe haldus ja suhtlemine)
  </li>
  <li>
    KDE õpirakendused
  </li>
  <li>
    KDE mängud
  </li>
  <li>
    KDE tööriistad
  </li>
  <li>
    KDE tarkvaraarenduse platvorm
  </li>
</ul>
<p align=justify>
Üheskoos moodustavad nad kõikehõlmava komplekti, mis täidab kasutajate mitmekülgseid vajadusi ja töötab suuremal osal tänapäeva operatsioonisüsteemidel. Allpool on toodud ära mõningate alarühmade tähtsamad uuendused ja parandused.
</p>
<ul>
  <li>
    <strong>KDE tööriistades</strong> on hulganisti parandusi. Nende seas võib ära mainida, et <b>KGpg</b>, failide ja e-kirjade krüptimiseks ja allkirjastamiseks mõeldud privaatsustööriist, on nüüd <b>lõimitud Solidiga</b>, mis aitab tuvastada võrguühenduse olemasolu, samuti on parandatud rakenduse <b>võtme importimise dialoogi</b>. Failide kokku- ja lahtipakkimise rakendus <b>Ark</b> on saanud <b>LZMA/XZ toetuse</b>, parandatud on zip-, rar- ja 7zip-failide toetust ning palju paremini tullakse toime ka failide lohistamisega. <b>Linuxi infrapuna kaugjuhtimissüsteemi</b> (LIRC) kasutajaliides KDELirc on porditud KDE 4 peale ning taas kasutatav. KDE binaarfailide redaktor <b>Okteta</b> sai endale <b>kontrollsumma tööriista</b>, failisüsteemi sirvija ja järjehoidjate külgriba. KDE tõlkimisrakendus <strong>Lokalize</strong> toetab nüüd skriptide kasutamist, uusi failivorminguid ning isegi <b>ODF-dokumentide tõlkimist</b>.<br>
    <br>
  </li>
  <li>
    <b>KDE mängud</b> kasutavad nüüd enamjaolt sarnast <b>Egiptuse stiilis teemat</b>. KGoldrunner sai uue mängu <b>"Muumia needus"</b> ning parandas tublisti intuitiivsust senisest täpsema pausi ja taas mängima hakkamise ning mängude salvestamise ja uuesti läbimängimise võimalustega. KMahjongg tõi sisse 70 uut kasutajate poolt annetatud taset ning lisandus ka <b>uus mäng</b> KTron. Mõneski mängus tekkis <b>uusi võimalusi</b>, näiteks aurustaja kasutamine Killbotsis ja senisest parem tehisaju Bovos. Töö failide laadimise ja skaleeritavate piltide oleku salvestamise kallal on aidanud paljudel mängudel <b>käivituda ja töötada kiiremini.</b><br>
    <br>
  </li>
  <li>
    <strong>KDE personaalse teabe haldamise</strong> rakendustes on tehtud märgatavaid parandusi väga mitmes valdkonnas, eelkõige muidugi jõudluse ja stabiilsuse alal. Kiirsuhtlusrakendus <b>Kopete</b> on saanud senisest parema kontaktide nimekirja, KOrganizer aga võib sünkroniseerida andmeid <b>Google'i kalendriga</b>. Kmail toetab nüüd kirjasiseste piltide lisamist ning <b>häirete rakendus</b> omandas eksportimise ja lohistamise võimalused ja on paremini seadistatav.<br>
    <br>
  </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/games.png"><img src="screenshots/games_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>Egiptuse teema näited</em></div>
<ul>
  <li>
    Kui mõne KDE rakendusega peaks midagi halba juhtuma ja seda tabab krahh, muudab <b>uus vigade teatamise tööriist</b> palju lihtsamaks KDE abistamise selle muutmiseks senisest stabiilsemaks. Veateadete tööriist hindab kolmetärnilisel skaalal krahhi ajal kogutud andmete kvaliteeti. Samuti leiab sealt nõuanded, kuidas muuta krahhiandmed ja seeläbi ka veateade ise teaberohkemaks. Uus tööriist aitab kasutajal veateate võimalikult lihtsalt esitada. Juba käesoleva väljalaske ettevalmistamisel osutus see suurepäraseks abiliseks, mis aitas veateadete kvaliteeti tunduvalt parandada.<br>
  </li>
</ul>
</p>
<div class="ii gt" id=:7r>
</div>
<h3>
  Platvorm aitab kiirendada arendustegevust
</h3>
<p align=justify>
KDE kogukond pakub KDE rakenduste raamistikus arendajatele hulgaliselt uuendusi. Nokia Qt teegi alusel loodud lõimitud ja ühtlustatud raamistik tuleb otseselt vastu rakenduste arendajate igapäevastele vajadustele.
</p>
<p align=justify>
KDE rakenduste raamistik aitab arendajatel luua töökindlaid rakendusi väga tõhusalt, vähendades nende vaeva keerukate ja tülikate ülesannete kallal, millega arendustegevuses nii või teisiti kokku puudutakse. Juba raamistiku kasutamine KDE rakendustes tõestab selle paindlikkust ja kasulikkust.
</p>
<p align=justify>
Paindlikult LGPL litsentsiga kaetud (see võimaldab nii avatud lähtekoodiga kui ka muulaadset arendustegevust) ja platvormiülene (Linux, UNIX, Mac ja MS Windows) raamistik sisaldab muu hulgas võimsat komponentmudelit (<b>KParts</b>), võrguläbipaistvusega andmete kasutamist (<b>KIO</b>) ja paindlikku seadistuste haldamist. Pruukida saab kümneid kasulikke vidinaid alates failidialoogidest ja lõpetades fondivalijatega, millele lisaks raamistik võimaldab semantilise otsingu lõimimist (<b>Nepomuk</b>), riistvarateadlikkust (<b>Solid</b>) ja multimeedia kasutamist (<b>Phonon</b>). Allpool on ära toodud KDE rakenduste raamistiku olulisemad uuendused ja parandused.
</p>
<ul>
  <li>
    KDE 4.3 rakenduste raamistik on alustanud <a href="http://www.socialdesktop.org/">sotsiaalse töölaua</a> lõimimisega, mis toob ülemaailmse vaba tarkvara kogukonna otse töölauale. <strong>Avatud koostöö-, jagamis- ja suhtlemisplatvormi</strong> pakkuva sotsiaalse töölaua algatuse eesmärk on võimaldada inimestel jagada oma teadmisi, ilma et nad peaksid end allutama mõne välise organisatsiooni juhtimisele ja kontrollile. Platvorm pakub hetkel <strong>andmemootorit</strong> Plasma vidinatele, mis toetavad sotsiaalse töölaua elemente.<br>
    <br>
  </li>
  <li>
    <strong>Uus süsteemisalve protokoll</strong>, mis on välja töötatud koos <a href="http://www.freedesktop.org/wiki/">Free Desktopi algatusega</a>, teostab ammuse soovi muuta tunduvalt tänaseks juba iganenud süsteemisalve iseloomu. Vana süsteemisalv, mis kasutas väikeseid põimitud aknaid, ei võimaldanud süsteemisalvel kuidagi kontrollida omaenda sisu, vähendades sel moel nii kasutajate kui ka arendajate võimalusi. Uus süsteemisalv toetab ka vana standardit, kuid rakenduste arendajatel on soovitatav uuendada oma rakendusi, et need arvestaksid uue spetsifikatsiooniga. Täpsemat teavet selle kohta leiab <a href="http://www.notmart.org/index.php/Software/Systray_finally_in_action">sellest ajaveebist</a> või <a href="http://techbase.kde.org/Projects/Plasma/NewSystemTray">TechBase'ist</a>.<br>
    <br>
  </li>
  <li>
    Plasma töölauakest on võtnud kasutusele <strong>geolokatsiooni andmemootori</strong>, mis toetub teekidele libgps ja HostIP ning võimaldab plasmoididel hõlpsasti tuvastada kasutaja asukoha. Veel mõned <strong>uued andmemootorid</strong> lubavad kasutada <strong>Akonadi ressursse</strong> (sealhulgas e-kirju ja kalendrit), <a href="http://nepomuk.semanticdesktop.org/">Nepomuki</a> metaandmeid ja klaviatuuriolekuid. Mõistagi on parandatud ka seniste andmemootorite tööd. Andmemootorite kasutamisest ja muust sellisest kõneldakse lähemalt <a href="http://techbase.kde.org/Development/Tutorials/Plasma/DataEngines">TechBase'is</a>.<br>
    <br>
  </li>
  <li>
    KDE rakenduste raamistik on võtnud kasutusele <strong>PolicyKiti kestprogrammi</strong>, mis lihtsustab tunduvalt arendajate tegevust, kes soovivad anda oma rakendustele võimaluse sooritada suuremaid õigusi nõudvaid toiminguid turvaliselt ja lihtsalt. Lisaks autentimishaldurile ja -agendile on arendajate käsutuses ka lihtne teek. Täpsemalt kõneldakse kõigest sellest <a href="http://techbase.kde.org/Development/Tutorials/PolicyKit/Introduction">TechBase'is</a>.<br>
    <br>
  </li>
  <li>
    <strong>Akonadi</strong> ehk PIM-andmete salvestite lahendus on nüüd <strong>valmis astuma laiema publiku ette</strong>. Lisaks kasutamisele Plasma andmemootorina tasub arendajatel uurida ka seda <a href="http://techbase.kde.org/Projects/PIM/Akonadi">TechBase'i lehekülge</a>, kui nende rakendused vajavad ligipääsu vestluste logidele, e-kirjadele, veebipäevikutele, kontaktidele ja muud laadi personaalsele teabele või soovivad selliseid andmeid salvestada. Platvormiülese tehnoloogiana tagab Akonadi ligipääsu igasugustele andmetele ning tuleb edukalt toime ka suure mahuga, mis lubab seda kasutada väga mitmesugustes olukordades.<br>
    <br>
  </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social.png"><img src="screenshots/social_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>Sotsiaalne töölaud ja teised võrguteenused praktikas</em></div>

<h4>
    Teabe levitamine
</h4>
<p align="justify">
KDE kogukond julgustab kõiki <strong>levitama teavet</strong> sotsiaalses veebis.
Kirjutage artikleid uudistelehekülgedele või kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter,
identi.ca. Laadige pilte sellistesse teenustesse, nagu Facebook, FlickR,
ipernity ja Picasa, ning postitage neid asjakohastesse uudistegruppidesse. Looge ekraanil toimuvast video ja 
laadige see sellistesse keskkondadesse, nagu YouTube, Blip.tv, Vimeo jt. Ärge unustage samas lisamast
üles laaditud materjalile <em>silti <strong>kde</strong></em>, et kõik leiaksid selle võimalikult
lihtsalt. Ühtlasi lubab see KDE meeskonnal koostada paremaid ülevaateid KDE 4.3
kajastamise kohta. <strong>Aidake meil levitada teavet ja andke oma panus!</strong></p>

<p align="justify">
Melu, mis käib KDE 4.3 väljalaske ümber, saab jälgida otse sotsiaalses veebis 
uhiuue <a href="http://buzz.kde.org"><strong>KDE kogukonna uudistevoo</strong></a> vahendusel. Sinna 
koonduvad reaalajas teated selle kohta, mis toimub mitmesugustes sotsiaalvõrgustikes, näiteks 
identi.ca, twitter, youtube, flickr, picasaweb, veebipäevikud jne. Uudistevoo leiab aadressilt 
<strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/software/KDE_4_3_released"><img src="buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com"><img src="buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde43/"><img src="buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde43"><img src="buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde43"><img src="buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">Levinupud</a></style>
</center>

<h4>
  KDE 4.3.0 paigaldamine
</h4>
<p align="justify">
 KDE, kaasa arvatud kõik teegid ja rakendused, on saadaval tasuta vastavalt
avatud lähtekoodiga tarkvara litsentsidele. KDE saab hankida nii lähtekoodina kui ka
mitmes binaarvormingus aadressilt <a
href="http://download.kde.org/stable/4.3.0/">http://download.kde.org</a>, samuti on
saadaval <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
ning mõistagi <a href="http://www.kde.org/download/distributions.php">tähtsamate
GNU/Linuxi ja UNIXi süsteemide</a> tarkvarapaketid.
</p>
<p align="justify">
  <em>Pakendajatele</em>.
  Mõned Linuxi/UNIXi operatsioonisüsteemi tarnijad on lahkelt pakkunud KDE 4.3.0
tarkvarapakette oma distributsiooni mõningate versioonide tarbeks, samuti on mõnel juhul
abikäe ulatanud nende kogukonna vabatahtlikud.
  Osa neist binaarpakettidest on võimalik tasuta alla laadida aadressilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.0/">http://download.kde.org</a>.
  Täiendavad binaarpaketid ning ka seniste pakettide uuendused muutuvad vastavalt 
võimalusele kättesaadavaks edaspidi.
</p>

<p align="justify">
  <a name="package_locations"><em>Pakettide asukohad</em></a>.
  Saadaolevate binaarpakettide nimekirja, millest KDE projekt on praeguseks teadlik,
leiab <a href="/info/4.3.0.php">KDE 4.3.0 infoleheküljelt</a>.
</p>

<p align="justify">
Jõudluseprobleemid <em>NVidia</em> binaarsete graafikadraiveritega on leidnud
<a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">lahenduse</a> 
NVidia pakutava draiveri uusimates väljalasetes.
</p>

<h4>
  KDE 4.3.0 kompileerimine
</h4>
<p align="justify">
  <a name="source_code"></a>
  KDE 4.3.0 täieliku lähtekoodi võib <a
href="http://download.kde.org/stable/4.3.0/src/">alla laadida tasuta</a>.
KDE 4.3.0 kompileerimise ja paigaldamise juhised leiab 
<a href="/info/4.3.0.php#binary">KDE 4.3.0 infoleheküljelt</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Teabekontaktid</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
