<?php
  $page_title = "Anonçaedje del rexhowe di KDE 4.3.0";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.3';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Li Cominålté di KDE ofere des noveatés todi pus amidrantes avou l' novele modêye di KDE 4.3
</h3>

<p align="justify">
  <strong>
    KDE 4.3 (no d' côde&nbsp;: <i>"Caizen"</i>) ofere des noveatés todi pus amidrantes ås uzeus do libe sicribanne eyet ås diswalpeus di programes
  </strong>
</p>

<p align="justify">
Li 4 d' awousse 2009. Li <a href=http://www.kde.org/>Cominålté KDE</a> anonce ådjourdu ki <i>"Caizen"</i> (dj' ô bén KDE 4.3) est disponibe do côp. Elle apoite bråmint des amidraedjes a l' esperyince di l' uzeu ey al platfôme di diswalpaedje. KDE 4.3 continouwe a afiner les unikès fonccions apoirtêyes ezès rexhowes di dvant tot berwetant des noveatés totes nouves. Li modêye 4.2 esteut fwaite pol pupårt des uzeus finås, KDE 4.3 ofere on prodût pus stocaesse eyet complet pol måjhon ou les ptitès eterprijhes.
</p>
<p align=justify>
  Li cominålté KDE a <strong>coridjî pus di 10&nbsp;000 bugs</strong> eyet <strong>metou èn ouve cåzu 2,&nbsp;000 dimandes di fonccionålités</strong> ezès 6 dierins moes. C' est dabôrd 63&nbsp;000 candjmints k' ont stî verifyîs eyet rverifyîs pa nos contribouweus ki sont moens di 700. Lijhoz pus lon pos awè ene esplikêye des candjmints dins l' espåce di boutaedjes do scribanne KDE 4.3, les shûtes d' aplicåcions eyet l' platfôme di diswalpaedjes.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/desktop.png"><img src="screenshots/desktop_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>Li scribanne KDE 4.3</em></div>

<h3>
  Li scribanne amidere li performance et l' eployåvisté
</h3>
<br>
<p align=justify>
    L' espåce di boutaedje do scribanne KDE dene ene esperyince poûxhante et complete ki s' met foirt bén avou les sistinmes d' operance Linux eyet UNIX. I gn a ezès componints clés ki fijhnut l' espåce di boutaedje do scribanne KDE&nbsp;:
<ul>
  <li>
    <strong>KWin</strong>, on poûxhant manaedjeu des purneas k' a des efets grafikes modiens e 3D.
  </li>
  <li>
    <strong>Plasma, li schåfe do scribanne</strong>, on sistinme pår novea di scribannes eyet d' paneas k' a des amidraedjes pol productivité eyet l' etegråcion so fyis åd triviè d' ahesses ki vs poloz mete a vosse môde
  </li>
  <li>
    <strong>Dolphin</strong>, on manaedjeu des fitchîs uzeu-amiståve, å corant di l' ådvins et del rantoele
  </li>
  <li>
    <strong>KRunner</strong>, on sistinme di cweraedje eyet d' enondaedje pos enonder des cmandes eyet trover des informåcions ahessåves.
  </li>
  <li>
    accès åjhey ås controles do scribanne et do sistinme avou les <strong>Apontiaedjes do sistinme</strong>.
  </li>
</ul>
Vos trovroz chal pa dzo ene coûte djivêye des amidraedjes fwaits a l' espåce di boutaedje do scribanne KDE.
<ul>
  <li>
    Li <a href="http://plasma.kde.org">Schåfe do scribanne Plasma</a> adrove on <b>novea tinme prémetou</b>, Air. Air a ene rivnance pus ledjire eyet s' met mia acou l' tinme prémetou po les programes. Plasma a yeu des grands <b>amidraedjes di performance</b> eto. L' eployaedje del memwere a stî raptiti et les animåcions sont pus doûces. <b>Les activités savèt asteure esse loyeyes avous les forveyous scribannes</b>, permetant inla ås uzeus d' awè des diferinnès ahesses so tchaekonk di leu scribannes. Et avou ça, Plasma a amidré s' <b>manaedjmint des bouyes et notifiaedjes</b>. Les bouyes ovrantes sont groupêyes dins ene seule båre des processus po n' nén aveur l' aspitaedje di trop di purneas di dvize. Des animåcions sont-st eployeyes po dire ki des bouyes sont co a l' ovraedje e ridant doûcetmint li purnea di dvize el boesse ås imådjetes sistinme ey animant l' imådjete di notifiaedje. I gn a des pus ptits candjmints dins Plasma come <b> saveur apontyî pår a vosse môde les racourtis del taprece</b> eyet on meyeus naiviaedje avou les tapes, saveur ahiver ene ahesse plasma cwand vs displaeçîz ou copyîz do contnou sol sicribanne eyet bråmint des <b>novelès et amidrêyès ahesses po Plasma</b>. L' ahesse Vuwe d' on ridant permet asteure a l' uzeu di <b>betchter dins on ridant e passant l' sori pa dzeu</b> eyet l' <b>novele ahesse Translatowide</b> ratoûne des mots et des fråzes direk so vosse sicribanne  e s' siervant di Google Translate. Avou ça, KRunner rind ses <b>fonccionålités di tchôkes-divins pus åjhey a discovri</b> avou on boton "aidance" ki mostere li sintake des comandes el redjon des rzultats. <b>Les faitindjes ont-st ossu on ptit apontiaedje</b> ki permet d' enonder des programes avou èn ôte conte d' uzeu, metans.<br>
    <br>
    </li>
  <li>
    Li manaedjeu des fitchîs Dolphin mostere des <b>ptits prévoeyaedjes des fitchîs dins on ridant</b> eyet des prévoeyaedjes des videyos pos aidî l' uzeu as idintifyîs les cayets. Li <b>batch sait asteure esse apontyî</b> a pårti del dressêye des Tchuzes di Dolphin eyet sacwantès limites a l' apontiaedje del grandeur do batch aidrè a ç' kel plake ni soeye nén forrimpleye avou des fitchîs disfacés. Li dressêye k' est mistrêye avou on clitche droet del sori sos on cayet est apontiåve eyet l' divize d' apontiaedje e djenerå a stî ratuzlêye pos esse <b>pus åjhey as eployî</b>. Li novea <b>eplaeçmint network:/</b> mostere les ôtès copiutreces eyet siervices sol rantoele (limité pol moumint ås cis ki sont-st anoncîs påzès protocoles DNS-SD/zeroconf, i nd årè pus di sopoirtés ezès modêyes ki vnèt).<br>
    <br>
  </li>
  <li>
    D' ôtes afinaedjes dins l' usteyes di l' espåce di boutaedje rindèt pus åjhey d' ovrer avou vosse copiutrece. Des <b>Apontiaedjes do sistinme pus roeds</b> adrovèt ene <b>vuwe e cowhlaedje opcionele</b> po l' apontiaedje eyet sacwants amidraedjes ås dvizes d' apontiaedjes. <b>Des noveas efets</b> come "Foye" et "Rider èn erî" eyet des <b>meyeusès performances</b> dins <b>KWin</b> rindèt l' manaedjmint des purneas pus doûs dabôrd kel etegråcion avou les tinmes di Plasma fjhèt ene rivnance ki s' met mia eshonne. <b>Klipper</b>, ene usteye ki s' sovént des afwaires copieyes e tchapea emacralé, sait asteure <b>ovrer tot tuzant sorlon l' ådvins</b>. I trove tot seu ene djivêye di programes ki savèt apougnî on cayet copyî e tchapea emacralé eyet permet a l' uzeu di les enonder direk.<br>
    <br>
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hIsigZW3agI" type="application/x-shockwave-flash" width="480" height="390" allowscriptaccess="always" allowfullscreen="true"></embed>
<br>
<a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">Modêye Ogg Theora</a></div>
<h3>
  Des programes potchnut d' on grand zoup
</h3>
<p align=justify>
I gn a beacôp des programes sofistikés ki sont dnés pal cominålté di KDE ki profitèt ttafwaitmint do poûxhant evironmint di progrmaedje d' aplicåcions di KDE. I gn a on tchoes di ces programes e cossemaedje di programes di KDE, pårti e categoreyes dins sacwantès shûtes di programes. I gn a ddins&nbsp;:
</p>
<ul>
  <li>
    Programes KDE pol Rantoele
  </li>
  <li>
    KDE Multimedia
  </li>
  <li>
    Usteyes grafikes di KDE
  </li>
  <li>
    Shûte KDE PIM (po manaedjmint et atôtchaedje des informåcions da vosse, en inglès "Personal Information Management and communication")
  </li>
  <li>
    Programes KDE pol Sicolaedje
  </li>
  <li>
    Djeus KDE
  </li>
  <li>
    Usteyes KDE
  </li>
  <li>
    Platfôme KDE di diswalpaedje di programes
  </li>
</ul>
<p align=justify>
I fôrmèt on nuk di compontins d' sicribanne ki s' metèt pår eshonne et ki rotèt sol pupårt des modienes sistinmes d' operance. Vos trovroz vaici pa dzo on tchoes des amidraedjes a des shûtes di programes k' i gn a.
</p>
<ul>
  <li>
    Les <strong>Usteyes KDE</strong> ont stî mo amidrés. Emey d' ôtès sacwès, <b>KGpg</b>, l' ustaye di privaceye eployî po l' ecriptaedje eyet l' sinaedje di fitchîs et emiles <b>eploye Solid</b> po trover s' i gn a on raloyaedje al rantoele ey a amidré s' <b>purnea d' abagaedje di clé</b>. <b>Ark</b>, on programe di rastrindaejde eyet d' disratrindaedje di fitchîs <b>sopoite asteure LZMA/XZ</b>, a amidré l' sopoirt pol zip, li rar et l' 7zip eyet rote mia avou l' bodjaedje/saetchaedje. KDELirc, èn eterface grafike pol <b>sistinme Linux di Controle då lon di l' Infrarodje</b> (LIRC), a stî poirté so KDE 4 eyet rest ddins. <b>Okteta</b>, l' aspougneu egzadecimå di KDE a wangnî ene <b>usteye di checksum</b>, on panea di costé di naiviaedje a sistinme di fitchî ey ene båre di costé des rmåkes. <strong>Lokalize</strong>, l' usteye di ratournaedje di KDE, adrove li sopoirt po les scripes, des novelès cognes di fitchî eyet l' <b>ratournaedje di documints ODF</b>.<br>
    <br>
  </li>
  <li>
    Les <b>djeus KDE</b> si siervèt asteure d' on minme <b>tinme stîle Edjipcyin</b> dins bråmint des djeus. KGoldrunner sititche on novea djeu, <b> "Coûsse des Mames"</b> eyet amidere li djouwaedje avou des djocaedjes pus spepieus, li rpurdaedje eyet l' edjîstraedje et ridjouwaedje des djeus. KMahjongg apoite 70 noveas liveas d' djeus propozés pås uzeus eyet on <b>novea djeu</b>, KTron, a stî stitchî. I gn a des djeus k' ont des <b>novelès fonccionålités</b> come li fiatindje Sipitchroûle e Killbots eyet ene meyeuse AI e Bovo. Gråce a l' ovraedje sol tcherdjaedje des fitchîs eyet l' schapaedje di l' estat des imådjes royreces metåves al schåle, beacôp des djeus <b>atakèt eyet enondèt pus rade</b>.<br>
    <br>
  </li>
  <li>
    Les programes di <strong>Manaedjmint des Informåcions da Vosse (PIM) di KDE</strong> ont stî amidrêyes dins bråmints des afwaires come li performance eyet l' sitocaessisté. Li messaedjeî sol moumint <b>Kopete </b> apoite on calpin d' adresse amidré eyet KOrganizer s' sait sincronijhî avou <b>Google Calendar</b>. KMail (KEmile) sopoite li stitchaedje d' imådjes dins l' tecse eyet l' <b>Notifieu d' adviertixhmint</b> a wangnî ene fonccionålité d' ebagaedje, li botchaedje/satchaedje ey a èn apontiaedje amidré.<br>
    <br>
  </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/games.png"><img src="screenshots/games_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>Kékes tinmes Edjipcyins</em></div>
<ul>
  <li>
    S' i gn a ene rujhe avou on programe KDE et ki s' sipotche, li <b>novele Usteye di rapoirtaedje di bug</b> rindrè pus åjhey li contribouwaedje di l' uzeu al sitocaessisté di KDE. L' usteye di rapoirt di bug dene on préjhaedje di troes stoeles sol cwålité des dnés revoyeyes å moumint do spotchaedje. Ele dene ossu des conseys so comint amidrer l' cwålité des dnêyes do spotchaedje e rapoirt di bug lyi-minme tot moennant l' uzeu ttavå l' processus di rapoirtaedje. Sol tins des Betas po cisse novele rexhowe, li novele usteye di rapoirtdi bug a ddja prouvé ki rote èn amidrant l' cwålité des rapoirts di bug.<br>
  </li>
</ul>
</p>
<div class="ii gt" id=:7r>
</div>
<h3>
  Diswalpaedje pus roed del platfôme
</h3>
<p align=justify>
Li cominålté KDE apoite ås programeus bråmint des amidraedjes e l' evironmint di KDE di diswalpaedje des programes. Basti sol foice del livreye Qt da Nokia, cist evironmint etegré eyet consistint a stî fwait come ene response direke ås mezåjhes di diswalpeus do vraiy monde.
</p>
<p align=justify>
L' evironmint di KDE di diswalpaedje des programes aide les diswalpeus as ahiver adroetmint des sgurs programes e rindant pus åjhey les spepieusès bouyes normålmint aloyeyes avou l' diswalpaedje di programes. Si eployaedje ezès programes di KDE apoite on clapant mostraedje di si apougnåvisté eyet ahessåvisté.
</p>
<p align=justify>
Metou dzo l' libe licinse LGPL (permetant li diswalpaedje, ambedeus, propietaire et open-source) eyet alant so sacwantès platfômes (Linux, UNIX, Mac et MS Windows), il a, emey d' ôtès sacwès, on poûxhant modele di componints (<b>KParts</b>), èn accès voeyoute ås dnêyes del rantoele (<b>KIO</b>) eyet on manaedjmint di l' apontiaedje ahessåve. Trinte-shijh ahesses ki vont des purneas di dvizes di fitchîs ås tchoezixheus d' fontes sont dnés eyet l' evironmint ofere èn usteye di cweraedje semantike metou dvins <b>(Nepomuk</b>), li deteccion di l' éndjolreye (<b>Solid</b>) eyet l' accès multimedia (<b>Phonon</b>). Lijhoz vaici pa dzo ene djivêye des amidraedjes di l' evironmint di KDE di diswalpaedje des programes.
</p>
<ul>
  <li>
    L' evironmint di KDE di diswalpaedje des programes atake l' etegråcion do <a href="http://www.socialdesktop.org/">Scribanne sociål</a>, en apoirtant l' cominålté daegnrece do Libe Programe sol sicribanne. Tot ofrant ene <strong>platfôme po l' ovraedje eshonne di manire drovowe, li pårtaedje eyet l' atôtchaedje</strong>, li Scribanne sociåle vout permete ås djins di pårtaedjî leus conoxhances sins l' abandner å controle d' ene difoûtrinne organizåcion. Li platfôme ofere pol moumint èn <strong>Édjin di dnêyes</strong> po des apliketes plasma sopoirtant des pårts do Scribanne sociål.<br>
    <br>
  </li>
  <li>
    Li <strong>novea protocole di boesse ås imådjetes sistinme</strong> diswalpêye avou l' côp d' sipale do <a href="http://www.freedesktop.org/wiki/">Libe sicribanne</a> est on complet metaedje a djoû do vî specifiaedje del boesse ås imådjetes ki ratindeut dispoy lontins d' esse fwaite. Li viye boesse ås imådjetes sistinme k' eployeut des ptits ravalés purneas n' permeteut nou controle pal boesse ås imådjetes so s' contnou, e metant insi ene limite a l' apougnåvisté po l' uzeu eyet l' diswalpeu di programe, ambedeus. Minme sel novele boesse ås imådjete sopoite ambedeus li vî eyet novea standård, les programeus sont priyîs di mete a djoû leus programes viè les noveas specifiaedjes. Po ndè sawè dpus, <a href="http://www.notmart.org/index.php/Software/Systray_finally_in_action">waitîz c' blogue</a> oudonbén trovez pus di pondants et di djondants <a href="http://techbase.kde.org/Projects/Plasma/NewSystemTray">so TechBase</a>.<br> 
    <br>
  </li>
  <li>
    Li schåfe do scribanne Plasma apoite on <strong>Éndjin di dnêyes di Djeyolocålijhaedje</strong> e s' siervant di libgps eyet HostIP ki permetèt ås plasmowides di responde åjheymint a l' eplaeçmint di l' uzeu. D' ôtes <strong>noveas éndjins di dnêyes</strong> dinèt accès ås <strong>rsoûce d' Akonadi</strong> (avou ddins l' corî eyet l' calindrî), ås meta-dnêyes di <a href="">Nepomuk</a> eyet a l' estat del taprece a costé di totes sôres d' amidraedjes ås Éndjins di dnêyes k' egzistèt ddja. Lijhoz åd fwait di s' siervi eyet d' discovri les Éndjins di dnêyes <a href="http://techbase.kde.org/Development/Tutorials/Plasma/DataEngines">so TechBase</a>.<br>
    <br>
  </li>
  <li>
    Li platfôme di programe di KDE adrove ene <strong>ewalpeure PolicyKit</strong> ki rind åjhey po les programeus ki vlèt leu programe fé des faitindjes priviledjîs d' ene façon såve, lodjike eyet åjheye. Il est dné on manaedjeu d' otorijhåcions eyet èn adjint d' otintifiaedje, mins ossu ene åjheye livreye po les diswalpeus s' endè siervi. Lére <a href="http://techbase.kde.org/Development/Tutorials/PolicyKit/Introduction">droci so TechBase</a> pos on tutoriel!<br>
    <br>
  </li>
  <li>
    <strong>Akonadi</strong>, li solucion di wårdaedje PIM do Libe sicribanne a stî tuzêye pos esse <strong>presse pos èn eployaedje pus lådje</strong>. A costé del disponibilité di l' Éndjin di dnêyes po plasma, les programeus sont priyîs di rwaitî après les djournås, emiles, blogs, soçons oudonbén tolminme kéne sôre di dnêyes da sinne. Ene tecnolodjeye po ttavå l' sicribanne come Akonadi pout dner accès a tolminme kéne sôre di dnêyes eyet est atuzlé pos apougnî des grands volumes et don d' permete ene grande fortchete d' eployaedjes.<br>
    <br>
  </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social.png"><img src="screenshots/social_thumb.png" align="center" width="504" height="315"  /></a><br />
<em>Sicribanne sociåle eyet ôtes siervices so fyis en alaedje</em></div>

<h4>
    Djåzez ndè totavå eyet voeyoz çou ki s' passe
</h4>
<p align="justify">
Li Cominålté di KDE ecoraedje tolmonde a <strong>ndè djåzer ttavå</strong> sol Waibe sociåle. Evoyîz des istweres so des novelès waibes, eployîz des canås come delicious, digg, reddit, twitter, identi.ca. Eberwetez des waitroûlêye viè des siervices come Facebook, FlickR, ipernity et Picasa eyet les poster dins les bons groupes. Ahivez des fimes-waitroûlêyes eyet ls eberweter soi YouTube, Blip.tv, Vime eyet ds ôtes. Èn rovyîz nén di taguer les cayets eberwetés avou l' <em>etikete<strong>kde</strong></em> po k' ça fuxhe pus åjhey po tolmonde del trover et po l' ekipe di KDE côpiler des rapoirts del covierteure di l' anonçaedje di KDE 4.3. <strong>Aidoz ns a ç' ki tolmonde endè cåze, cåzez ndè vos-minme!</strong></p>

<p align="justify">
Vos ploz shuve e direk çou ki s' passe åtoû del rexhowe di KDE 4.3 sol tote novele waibe sociåle <a href="http://buzz.kde.org"><strong>do floû e direk del Cominålté KDE</strong></a>. Cisse waibe rashonne e direk tot çk i s' passe so identi.ca, twitter, youtube, flickr, picasaweb, les blogues et bråmint ds ôtes waibes di rantoelaedje sociål. Li floû e direk est a l' adresse <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>
<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/software/KDE_4_3_released"><img src="buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com"><img src="buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde43/"><img src="buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde43"><img src="buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde43"><img src="buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbotons</a></style>
</center>

<h4>
  Astaler KDE 4.3.0
</h4>
<p align="justify">
 KDE, avou totes ses livreyes et ses programes, est disponibe po rén dins des libes licinces. On sait awè KDE e sourdants eyet e sacwantès ôtès cognes a pårti di <a
href="http://download.kde.org/stable/4.3.0/">http://download.kde.org</a> et sait ossu esse obtinou so <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
ou avou tolminme kéne <a href="http://www.kde.org/download/distributions.php">mwaissse distribucion
GNU/Linux eyet les sistinmes UNIX</a> ki rexhèt asteure.
</p>
<p align="justify">
  <em>Pacaedjeus</em>.
  Des vindeus d' SO Linux/UNIX ont djintimint dné des pacaedjes binaires di KDE 4.3.0 po des modêyes di leu distribucion. D' ôtes côps des bén-vlants del cominålté l' ont fwait.
On sait aberweter po rén des pacaedjes di ces binaires sol <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.0/">http://download.kde.org</a> da KDE.
D' ôtes pacaedjes binaires et des metaedjes a djoû des pacaedjes k' i gn a asteure seront disponibes dins les saminnes ki vnèt.
</p>

<p align="justify">
  <a name="package_locations"><em>Plaeces des pacaedjes</em></a>.
  Pos ene djivêye a djoû des pacaedjes binaires k' i gn a et kel Pordjet KDE est å courant, vizitez s' i vs plait l' <a href="/info/4.3.0.php">Pådje d' informåcion di KDE 4.3.0</a>.
</p>

<p align="justify">
Des problinmes di performance avou les mineus grafikes binaires di <em>NVidia</em> ont stî
<a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">rezoudous</a> ezès dierinnès modêyes do mineus disponibe pa NVidia.
</p>

<h4>
  Côpiler KDE 4.3.0
</h4>
<p align="justify">
  <a name="source_code"></a>
  Li côde sourdant complet po KDE 4.3.0 pout esse <a
href="http://download.kde.org/stable/4.3.0/src/">libmint aberweté po rén</a>.
Des esplikêyes so comint côpiler KDE 4.3.0 sont disponibes sol <a href="/info/4.3.0.php#binary">Pådje d' informåcion di KDE 4.3.0</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Atôtchaedje po les gazetes</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
