<?php
  $page_title = "KDE 4.3.0 Caizen Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.3';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
   Comunidade KDE libera novas melhorias com o novo KDE 4.3
</h3>

<p align="justify">
  <strong>
    KDE 4.3 (Codinome: "<i>Caizen</i>") libera novas melhorias para os usuários de desktop livre e desenvolvedores de software
  </strong>
</p>

<p align="justify">
4 de Agosto de 2009. A  <a href=http://www.kde.org/>comunidade KDE</a> anuncia hoje para disponibilidade imediata do "Caizen", (t.c.c KDE 4.3), trazendo diversas melhorias para a experiência do usuário e em sua plataforma de desenvolvimento. O KDE 4.3 continua a melhorar as ferramentas únicas antigas enquanto introduz inovações. Com o lançamento do 4.2 destinado a maioria dos usuários finais, o KDE 4.3 oferece um produto mais estável e completo para uso doméstico e pequenos escritórios.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/kde430-desktop.png"><img src="images/kde430-desktop_thumb.jpg" align="center" width="540" height="337"  /></a><br/>
<em>O ambiente de trabalho do KDE 4.3</em></div>

<p align=justify>
  A comunidade KDE <strong>corrigiu cerca de 10.000 erros e implementou quase 2.000 novas</strong> solicitações nos últimos 6 meses. Perto de 63.000 mudanças foram submetidas por mais ou menos 700 contribuidores. Continue lendo para uma geral das mudanças no ambiente de trabalho do KDE 4.3, nas suites de aplicativos e na plataforma de desenvolvimento.
</p>


<h3>
  Melhora de performance e usabilidade no ambiente de trabalho
</h3>
<br/>
<p align=justify>
    O ambiente de trabalho do KDE provê uma completa e poderosa experiência com excelente integração com sistemas operacionais Linux e UNIX. Os componentes chaves que fazem o ambiente de trabalho do KDE incluem:
<ul>
  <li>
    <strong>KWin</strong>,um poderoso gerenciador de janelas que provê efeitos gráficos 3D modernos
  </li>
  <li>
    <strong>Plasma</strong>, um desktop de última geração e sistema de painéis que possui melhorias de produtividade e integração online através de componentes (widgets) customizáveis
  </li>
  <li>
    <strong>Dolphin</strong>,um gerenciador de arquivos amigável e que funciona na rede
  </li>
  <li>
    <strong>KRunner</strong>,um sistema de busca e lançamento para executar comandos e procurar informações úteis
  </li>
  <li>
   fácil acesso ao controle do ambiente de trabalho e do sistema através do <strong>Configurações do sistema</strong>.
  </li>
</ul>
Abaixo você pode encontrar uma pequena lista das melhorias no Ambiente de trabalho do KDE.
<ul>
  <li>
    O <a href="http://plasma.kde.org">Plasma</a> introduz um <strong>novo tema padrão</strong>,o Air. Ele tem uma aparência muito mais leve e combina melhor com o tema padrão das aplicações. O Plasma também teve grandes <strong>melhorias de performance</strong>. O uso de memória foi reduzido, e as animações estão mais leves. As <strong>atividades agora podem ser extendidas para desktops virtuais</strong>, permitindo que usuários tenham diferentes widgets em cada um desses desktops. Além disso, o Plasma teve uma melhoria no seu <strong>gerenciador de trabalhos e notificações</strong>. Trabalhos em execução são agrupados numa única barra de progressão para prevenir o popup de muitas janelas. As animações são utilizadas para avisar que os trabalhos ainda estão em execução pela ação de deslizar suavemente as janelas no painel do sistema (systemtray) e animando o ícone de notificação. Pequenas modificações no Plasma incluem <strong>atalhos de teclado totalmente configuráveis</strong> e navegação pelo teclado mais extensa, a habilidade de criar widgets plasma quando você solta ou copia conteúdo no seu ambiente de trabalho e <strong>muitos widgets novos e melhorados</strong>. O widget "Visualização de pasta" agora permite que o usuário <strong>navegue em um diretório passando o mouse por cima dele</strong> e o <strong>novo widget Translatoid</strong> traduz palavras e sentenças no seu ambiente de trabalho usando Google Translate. Além disso, KRunner tornou as <strong>opções dos plugins fáceis de descobrir</strong> contando agora com um botão 'ajuda' mostrando a sintaxe do comando na área de resultado. As <strong>ações também possuem pequenas configurações</strong> permitindo por exemplo iniciar aplicações com outra conta de usuário.
    <br/>
    </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/desktop.png"><img src="screenshots/desktop_thumb.jpg" align="center" width="504" height="315"  /></a><br/>
<em>Integração Web no KDE 4.3</em></div>
<ul>
  <li>
    O gerenciador de arquivos Dolphin mostra <strong>pequenas pré-visualizações dos arquivos em um diretório</strong> e miniaturas dos vídeos ajudando o usuário a identifica-los. A <strong>lixeira agora pode ser configurada</strong> pelo menu de configurações do Dolphin, e várias limitações de configuração no tamanho da lixeira ajudam a ter certeza que o disco não vai encher com os arquivos apagados. O menu que é mostrado com um clique inverso do mouse em um item é configurável e o dialogo de configuração em geral foi redesenhado para ser <strong>fácil de usar</strong>. A nova localização <strong>network:/</strong> mostra outros computadores e serviços na rede (atualmente limitado para os que são anunciados pelos protocolos DNS-SD/zeroconf, mais protocolos serão suportados em versões futuras).<br/>
    <br/>
  </li>
  <li>
   Outras adaptações nas ferramentas do espaço de trabalho tornam mais fácil trabalhar com seu computador.
   O <strong>SystemSettings mais rápido</strong> introduz uma <strong> visualização em árvore </strong> para a configuração e vários melhorias
   das janelas.<strong>  Novos efeitos </strong> como "Folha" e "Deslizar para trás" e <strong> melhor desempenho </strong> na <strong> Kwin </strong> fazem o gerenciamento de janelas mais suave, enquanto que a integração com os temas do Plasma cria uma aparência mais consistente. O  <strong> Klipper </strong>, uma ferramenta que mantém um histórico de coisas copiadas para a área de transferência, agora podem <strong> agir com inteligência sobre o conteúdo </strong>. Ela determina automaticamente uma lista de aplicações que podem lidar com um objeto copiado para a área de transferência e permite ao usuário a iniciá-los imediatamente. <br/>
    <br/>
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hIsigZW3agI" type="application/x-shockwave-flash" width="480" height="390" allowscriptaccess="always" allowfullscreen="true"></embed>
<br/>
Um screencast apresentando algumas das melhorias mencionadas acima > <a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">(versão Ogg Theora)</a></div>
<h3>
 Um avanço nas aplicações
</h3>
<p align=justify>
  Um grande número de aplicações sofisticadas são fornecidos pela comunidade KDE que tirar o máximo partido do poderoso framework KDE. Uma seleção dessas aplicações estão incluídas no KDE, divididos por categoria em diversos pacotes de aplicações. Estes incluem:
</p>
<ul>
  <li>
    KDE aplicações para rede
  </li>
  <li>
    KDE Multimídia
  </li>
  <li>
    KDE ferramentas gráficas
  </li>
  <li>
    KDE pacote PIM (para manipulação de informações pessoais e comunicação)
  </li>
  <li>
    KDE educativos
  </li>
  <li>
    KDE jogos
  </li>
  <li>
    KDE utilitários
  </li>
  <li>
    KDE plataforma de desenvolvimento de software
  </li>
</ul>
<p align=justify>
Juntos eles formam um conjunto completo de coisas essenciais que funcionam na maioria dos sistemas operacionais modernos. Abaixo você encontrará uma seleção de melhorias para algumas aplicações desses pacotes.
</p>
<ul>
  <li>
   Os <strong> utilitários do KDE </strong> tiveram muitas melhorias. Entre outras coisas, o <strong> KGpg </strong>, a ferramenta de privacidade utilizada para a encriptação e assinatura de arquivos e e-mails <strong> integra o Solid </strong> para detectar a disponibilidade de uma conexão de rede e tem melhorado a sua  <strong> janela de importação de chave </strong>.O <strong> Ark </strong>, uma aplicação de compressão e descompressão de arquivos agora suporta <strong> LZMA / xz </strong>, tem um suporte melhorado para zip, rar e 7zip e funciona melhor com drag'n'drop. O KDELirc, um frontend para o <strong> sistema  de Linux Infrared Remote Control  </strong> (Lirc), foi portado para o KDE 4, e está incluído novamente. No <strong> Okteta </strong>, o editor hexadecimal do KDE ganhou uma <strong> ferramenta de checksum , </strong> um navegador de pastas e uma barra de favoritos lateral. O <strong> Lokalize </strong>, a ferramenta de tradução do KDE, introduz suporte para scripts, e os novos formatos de arquivos e a <strong> tradução de documentos ODF </strong>. <br/>
    <br/>
  </li>
  <li>
    Os  <strong> jogos do KDE </strong> agora usam um estilo semelhante ao <strong> tema egípcia </strong> em muitos dos jogos. O KGoldrunner é introduz um novo jogo, <strong> "Maldição da Múmia" </strong> e melhora a jogabilidade com uma pausa mais precisa , retomar a reprodução e gravação e de jogos. O KMahjongg introduz 70 novos níveis de usuário e um <strong>novo jogo </strong>,o  KTron. Alguns jogos <strong> ganharam novos recursos </strong> como o Vaporizer em Killbots e uma melhor IA em Bovo. Graças ao trabalho em arquivo de carregar e salvar o estado de muitas imagens escaláveis, muitos jogos vão <strong> iniciar e rodar mais rápido. </strong> <br/>
    <br/>
  </li>
  <li>
    As aplicações do <strong> KDE Personal Information Management (PIM) </strong> tiveram várias melhorias em diversas áreas como o desempenho e estabilidade. O mensageiro instantâneo <strong> Kopete </strong> introduz uma melhoria da lista de contatos e KOrganizer pode sincronizar com <strong> Google Agenda </strong>. o Kmail suporta  inserir imagens na mesma linha para o e-mail e <strong> Alarme notificador </strong> ganhou a funcionalidade de exportar , arrastar e soltar e tem uma melhor configuração. <br/>
    <br/>
  </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/games.png"><img src="screenshots/games_thumb.jpg" align="center" width="504" height="315"  /></a><br/>
<em>Alguns temas egípcios</em></div>
<ul>
  <li>
    Em caso de algo dar errado em uma aplicação do KDE e ele falhar, a <strong> nova ferramenta de reportar bugs </strong> tornará mais fácil para o usuário a contribuir para a estabilidade do KDE. A ferramenta fornece uma avaliação de três estrelas da qualidade dos dados que recolheu sobre o acidente. Ela também dá dicas sobre como melhorar a qualidade dos dados e bloqueia o bug em si, enquanto ajudar o usuário, através do processo de elaboração de relatórios. Durante os ciclos da versão beta, a ferramenta já provou a si mesmo por um aumento da qualidade dos relatórios de bugs recebidos. <br/>
  </li>
</ul>

<h3>
 Plataforma acelera o desenvolvimento
</h3>
<p align=justify>
A comunidade KDE traz muitas inovações para os desenvolvedores na vanguarda do framework de desenvolvimento do  KDE. Com base nos pontos fortes  da biblioteca Qt da Nokia, esse framework está integrado e consistente quando foi elaborado em resposta direta às necessidades das aplicações do mundo real.
</p>
<p align=justify>
O framework de desenvolvimento KDE ajuda os desenvolvedores a criar aplicações robustas de forma eficiente através da simplificação da complexidade e das tediosas tarefas, normalmente associadas ao desenvolvimento de aplicativos. A sua utilização por aplicações do KDE fornece uma amostra obrigatória para obrigar a sua flexibilidade e utilidade.
</p>
<p align=justify>
Liberalmente licenciado sob a LGPL (permitindo a ambos os proprietários e desenvolvimento de código aberto) e multi-plataforma (Linux, UNIX, MS Windows e Mac), que contém, entre outras coisas um componente poderoso modelo (<strong> KParts </strong>), uma rede transparente de acesso aos dados (<strong> KIO </strong>) e flexível de gerenciamento de configuração. Dezenas de widgets úteis variando de janelas de arquivos até selecionar fontes são oferecidos, e também oferece a pesquisa semântica integrada (<strong> Nepomuk </strong>), o sensor de hardware (<strong> Solid </strong>) e o acesso multimídia (<forte > Phonon </strong>). Leia sobre uma lista de melhorias para o framework de desenvolvimento do KDE.
</p>
<ul>
  <li>
    O framework de desenvolvimento de aplicações do KDE 4.3 introduz o único da integração com o <a href="http://www.socialdesktop.org/">Desktop Social</a>, trazendo a comunidade de Software Livre mundial para o ambiente de trabalho. Oferecendo uma <strong>plataforma para colaboração aberta, compartilhamento e comunicação</strong>, a iniciativa "Social Desktop" procura permitir que pessoas compartilhem o seu conhecimento sem ter que entregar o controle para uma organização externa. A plataforma atualmente oferece uma <strong>DataEngine</strong> para os applets plasma suportando o Desktop Social.
    <br/>
  </li>
  <li>
    O <strong>novo protocolo da system tray</strong> desenvolvido em colaboração com a <a href="http://www.freedesktop.org/wiki/">iniciativa Free Desktop</a> é um longo redesenho da antiga especificação. A antiga systemtray usava pequenas janelas embutidas não permitindo qualquer tipo de controle pela systemtray sobre os seus conteúdos, limitando a flexibilidade para o usuário e o desenvolvedor de aplicações ao mesmo tempo. Por mais que a nova systemtray suporte ambos os protocolos (antigo e novo), desenvolvedores de aplicações são encorajados a atualizar suas aplicações para as novas especificações. Para mais informações <a href="http://www.notmart.org/index.php/Software/Systray_finally_in_action">veja esse blog</a> ou procure mais informações no <a href="http://techbase.kde.org/Projects/Plasma/NewSystemTray">TechBase</a>.
    <br/>
  </li>
  <li>
  O ambiente shell do Plasma apresenta uma <strong> Geolocation DataEngine </strong> usando o libgps e HostIP, que permite facilmente plasmoids para responder à localização do usuário. Outros <strong> novos DataEngines </strong> para fornecer acesso aos <strong> recursos do Akonadi  </strong> (incluindo e-mail e calendário), os metadados do <a href="http://nepomuk.semanticdesktop.org/"> Nepomuk </ a > , o estado do teclado , além das diversas melhorias para DataEngines existentes. Leia sobre como usar e descobrir DataEngines <a href="http://techbase.kde.org/Development/Tutorials/Plasma/DataEngines"> sobre TechBase </a>. <br/>
   <br/>
  </li>
  <li>
   O framework de desenvolvimento do KDE introduz uma <strong> adaptador PolicyKit </strong>, tornando fácil para os desenvolvedores que desejam que suas aplicações realizem ações privilegiadas de forma segura, consistente e fácil. Existe disponível um gerenciador de autorização , além de um agente de autenticação, e uma biblioteca para programadores bem fácil de usar. Leia <a href="http://techbase.kde.org/Development/Tutorials/PolicyKit/Introduction"> aqui na TechBase </a> para mais um tutorial! <br/>
  </li>
  <li>
    O <strong> Akonadi </strong>, uma solução livre de PIM de armazenamento em desktop foi considerada <strong> pronto para o uso mais generalizado </strong>. Além da disponibilidade do DataEngine para o plasma, os desenvolvedores são incentivados a ter um olhar para <a href="http://techbase.kde.org/Projects/PIM/Akonadi"> a página do TechBase  </a> caso a sua aplicação necessite acesso a registros ou armazenar chat, e-mail, blogs, contatos ou qualquer outro tipo de dados pessoais. Sendo uma tecnologia de multi-plataformas, o Akonadi pode proporcionar o acesso a qualquer tipo de dados e é projetado para lidar com grandes volumes, permitindo, assim, uma vasta utilização. <br/>
    <br/>
  </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social.png"><img src="screenshots/social_thumb.jpg" align="center" width="504" height="315"  /></a><br/>
<em>Desktop Social e outros serviços online em ação</em></div>
<h4>
Mais mudanças
</h4>
<p align=justify>
Como mencionado, o que mostramos é só uma seleção das mudanças e melhorias no ambiente de trabalho do KDE, pacotes de aplicações e frameworks de desenvolvimento de aplicações. Uma lista mais compreensiva mas ainda incompleta pode ser encontrada no <a href="http://techbase.kde.org/Schedules/KDE4/4.3_Feature_Plan">Planejamento do KDE</a> 4.3 no <a href="http://techbase.kde.org">TechBase</a>. Informações sobre aplicações desenvolvidas pela comunidade KDE fora dos pacotes de aplicações podem ser encontradas na página da  <a href="https://www.kde.org/family/">familia KDE</a> e no site <a href="http://kde-apps.org">kde-apps</a>. Os desenvolvedores do Marble do time KDE Edu lançaram a versão 0.8 com o KDE 4.3 e criaram um relatório de mudanças visualmente estendido no <a href="http://edu.kde.org/marble/current.php">seu website</a>.
</p>
<h4>
    Espalhe a notícia e ver o que acontece
</h4>
<p align="justify">
 A Comunidade KDE encoraja todos <strong> para espalhar a notícia </strong> na Net.
Enviar notícias para sites de informação, utilizar canais como o delicious, digg, reddit, twister,
identi.ca. Enviar imagens para serviços como o Facebook, Flickr,
ipernity e Picasa e publicá-las adequadas para grupos. Criar e screencasts
carregá-las para o YouTube, Blip.tv, Vimeo e outros. Não se esqueça de aplicar a tag
no material com o kde <em>tag <strong> kde  </strong> </em>, por isso, é mais fácil para todo mundo encontrar
, e para a equipe do KDE para elaborar relatórios de cobertura para o anúncio do KDE 4.3.
<strong> Ajude-nos a espalhar a notícia, fazer parte dela! </strong> </ p>

<p align="justify">
Você pode acompanhar o que está acontecendo em torno do lançamento do KDE 4/3 na Net no
novo site da <a href="http://buzz.kde.org"> <strong> comunidade KDE </strong> </a>. Este site agrega o que acontece em
identi.ca, twister, youtube, flickr, picasaweb, blogs e muitos outros sites de rede social
em tempo real. O livefeed pode ser encontrado em <strong> <a href="http://buzz.kde.org"> buzz.kde.org </a> </strong>.
</p>

<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_KDE_4_3_0_Caizen_Release_Announcement"><img src="buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/97gdu/kde_430_caizen_released/"><img src="buttons/reddit.gif"><img src="buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde43/"><img src="buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde43"><img src="buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde43"><img src="buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</center>
<h4>
  Instalando o KDE 4.3.0
</h4>
<p align="justify">
O KDE, incluindo todas as bibliotecas e suas aplicações, está disponível gratuitamente sob licença Open Source. O software do KDE funciona em diversas configurações de hardware, sistemas operacionais e trabalha com qualquer tipo de gerenciador de janelas ou ambiente desktop. Além do Linux e outros sistemas operacionais baseados em UNIX,  você pode encontrar versões do  KDE para o Microsoft Windows no site <a href="http://windows.kde.org"> KDE no Windows </a> e para o Apple Mac OS X no site <a href="http://mac.kde.org/"> KDE no Mac site </a>. São desenvolvidas versões de teste do KDE para várias plataformas móveis como o MS Windows Mobile e Symbian, que podem ser encontradas na web, mas que não tem suporte.
<br/>
O KDE pode ser obtido na fonte e em vários formatos binários de <a
href = "http://download.kde.org/stable/4.3.0/"> http://download.kde.org </a> e pode
também ser obtido em CD-ROM <a href="http://www.kde.org/download/cdrom.php"> </a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions.php"> grandes
distribuições Linux</a>.
</p>
<p align="justify">
  <em>Pacotes</em>.
 Alguns distribuidores de Linux têm gentilmente fornecido pacotes binários do KDE 4.3.0
para algumas versões de sua distribuição, e em outros casos os voluntários da comunidade
tem feito o mesmo. <br/>
  Alguns desses pacotes binários estão disponíveis para download gratuito a partir do site do KDE: <um
href = "http://download.kde.org/binarydownload.html?url=/stable/4.3.0/"> http://download.kde.org </a>.
  Os pacotes binários adicionais, bem como atualizações para os pacotes disponíveis,
estarão disponíveis nas próximas semanas.
</p>
<p align="justify">
A maioria dos problemas de desempenho com o driver gráfico binário da <em>NVidia</em> foram  <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA"> resolvidos</a> nas últimas versões disponíveis da NVidia. No entanto, devido a recentes mudanças na <a href="http://x.org"> pilha de gráficos</a> no linux, algumas configurações de hardware e software podem ainda encontrar problemas com o desenho geral velocidade e uma lentidão geral. Entre em contato com o vendedor ou o desenvolvedor do driver da sua distribuição se você se deparar com problemas.
</p>
<p align="justify">
  <a name="package_locations"><em>Localizações dos pacotes</em></a>.
  Para uma lista atual dos pacotes binários disponíveis de que o projeto tem o KDE
sido informado, por favor visite a <a href="/info/4.3.0.php">página de informação do KDE 4.3.0</a>.
</p>

<h4>
  Compilando o KDE 4.3.0
</h4>
<p align="justify">
  <a name="source_code"></a>
  O código fonte completo para o KDE 4.3.0 pode ser <a
href="http://download.kde.org/stable/4.3.0/src/">baixado gratuitamente</a>.

Instruções sobre compilação e instalação do KDE 4.3.0
   estão disponíveis na <a href="/info/4.3.0.php#binary">página de informações do KDE 4.3.0</a>.
</p>
<p>
<br/><strong>*</strong> Os sistemas operacionais e marcas mencionados nesta página são propriedade de seus respectivos proprietários.<br/>
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contatos com a imprensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
