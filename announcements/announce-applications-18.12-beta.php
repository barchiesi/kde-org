<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Beta of KDE Applications 18.12");
  $site_root = "../";
  $release = "applications-18.11.80";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php i18n("November 16, 2018. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.");?>
</p>

<p align="justify">
<?php print i18n_var("Check the <a href='%1'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release", "https://community.kde.org/Applications/18.12_Release_Notes");?>
</p>

<p align="justify">
<?php i18n("The KDE Applications 18.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.");?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 18.12 Beta Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 18.12 Beta (internally 18.11.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 18.12 Beta");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 18.12 Beta may be <a href='http://download.kde.org/unstable/applications/18.11.80/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-18.11.80.php'>KDE Applications 18.12 Beta Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
