<?php

  $page_title = "KDE 3.5.7 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations -->
Also available in:
<a href="announce-3.5.7-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-3.5.7.php">Chinese (simplified)</a>
<a href="announce-3.5.7-es.php">Spanish</a>
<a href="announce-3.5.7-it.php">Italian</a>
<a href="announce-3.5.7-nl.php">Dutch</a>
<a href="announce-3.5.7-pt.php">Portuguese</a>

<h3 align="center">
   KDE Project Ships Seventh Translation and Service Release for Leading Free
   Software Desktop
</h3>

<p align="justify">
  <strong>
    KDE 3.5.7 features translations in 65 languages, improvements to KDE PIM
    suite and other applications.
  </strong>
</p>

<p align="justify">
  May 22, 2007 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.7,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. KDE now
  supports <a href="http://l10n.kde.org/stats/gui/stable/">65 languages</a>,
  making it available to more people than most non-free
  software and can be easily extended to support others by communities who wish
  to contribute to the open source project.
</p>

<p align="justify">
  This release has a renewed focus on <a href="http://pim.kde.org/">KDE PIM</a>
  applications.
  <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>,
  <a href="http://kontact.kde.org/korganizer/">KOrganizer</a> and
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a> received
  attention with bugfixes, while
  <a href="http://pim.kde.org/components/kmail.php">KMail</a> additionally
  witnessed new features and improvements with both interface work and
  IMAP handling: it can manage IMAP quota and copy and move all folders.
</p>

<p>
  Several other applications saw feature improvements:
</p>
<ul>
<li>
  <a href="http://kpdf.kde.org/">KPDF</a> shows tooltips for links when
  hovering on them, displays correctly more complex PDF files like this
  <a href="http://kpdf.kde.org/stuff/nytimes-firefox-final.pdf">Firefox
  advertisement</a> and reacts to the commands for opening the Table of Contents pane.
</li>

<li>
  <a href="http://uml.sourceforge.net/">Umbrello</a> now can generate and
  export C# Code and has added Java 5 generics support.
</li>

<li>
  <a href="http://www.kdevelop.org/">KDevelop</a> got a major version upgrade
  to version 3.4.1. New features include much improved Code Completion and
  Navigation, a more reliable debugger interface, Qt4 support and better
  Ruby and KDE4 development support.
</li>
</ul>

<p>
  In addition to the new features there were many bug fixes across the board,
  especially in the <a href="http://edu.kde.org/">Edutainment</a>
  and <a href="http://games.kde.org/">Games</a> packages and
  <a href="http://kopete.kde.org/">Kopete</a>. Besides bugfixes Kopete
  also got a major performance improvement on chat rendering.
</p>

<p>
  As KDE users have come to expect, this new release includes continued work
  on KHTML and KJS, KDE's HTML and Javascript engines. A new and interesting
  usability feature in KHTML makes the mouse pointer indicate if a link wants
  to open a new browser window or not.
</p>

<p align="justify">
  For a more detailed list of improvements since the
  <a href="http://www.kde.org/announcements/announce-3.5.6.php">KDE 3.5.6 release</a>
  on the 25th January 2007, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_6to3_5_7.php">KDE 3.5.7 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.7 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.7 packages in
  their next releases. Check 
  <a href="http://www.kde.org/download/distributions.php">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.7 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.7 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/3.5.7.php">KDE 3.5.7 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.7
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.7 may be
  <a href="http://download.kde.org/stable/3.5.7/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.7
  are available from the <a href="/info/3.5.7.php">KDE
  3.5.7 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>
<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
