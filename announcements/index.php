<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Announcements",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="releaseAnnouncment container">

<p>
To receive new announcements by e-mail subscribe to the
<a href="https://mail.kde.org/mailman/listinfo/kde-announce">KDE Announcements mailing list</a>.
</p>
<p />

<!-- INSERT NEW ENTRIES BELOW THIS LINE. KEEP THIS LINE, SCRIPTS RELY ON IT -->
<!-- Plasma 5.19.1 -->
<strong>Tuesday, 16 June 2020</strong> - <a href="plasma-5.19.1">Plasma 5.19.1</a>
<br />
"<em>KDE Ships Plasma 5.19.1.</em>"
<p />
<!-- KF 5.71.0 -->
<strong>13th June 2020</strong> - <a href="kde-frameworks-5.71.0">KDE Frameworks 5.71.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.71.0.</em>"
<p />
<!-- Plasma 5.19.0 -->
<strong>Tuesday, 09 June 2020</strong> - <a href="plasma-5.19.0">Plasma 5.19.0</a>
<br />
"<em>KDE Ships Plasma 5.19.0.</em>"
<p />
<!-- Plasma 5.18.90 -->
<strong>Thursday, 14 May 2020</strong> - <a href="plasma-5.18.90">Plasma 5.18.90</a>
<br />
"<em>KDE Ships Plasma 5.18.90.</em>"
<p />
<!-- KF 5.70.0 -->
<strong>09th May 2020</strong> - <a href="kde-frameworks-5.70.0.php">KDE Frameworks 5.70.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.70.0.</em>"
<p />
<!-- Plasma 5.18.5 -->
<strong>Tuesday, 5 May 2020</strong> - <a href="plasma-5.18.5.php">Plasma 5.18.5</a>
<br />
"<em>KDE Ships Plasma 5.18.5.</em>"
<p />
<!-- KF 5.69.0 -->
<strong>11th April 2020</strong> - <a href="kde-frameworks-5.69.0.php">KDE Frameworks 5.69.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.69.0.</em>"
<p />
<!-- Plasma 5.18.4 -->
<strong>Tuesday, 31 March 2020</strong> - <a href="plasma-5.18.4.php">Plasma 5.18.4</a>
<br />
"<em>KDE Ships Plasma 5.18.4.</em>"
<p />
<!-- KF 5.68.0 -->
<strong>15th March 2020</strong> - <a href="kde-frameworks-5.68.0.php">KDE Frameworks 5.68.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.68.0.</em>"
<p />
<!-- Plasma 5.18.3 -->
<strong>Tuesday, 10 March 2020</strong> - <a href="plasma-5.18.3.php">Plasma 5.18.3</a>
<br />
"<em>KDE Ships Plasma 5.18.3.</em>"
<p />
<!-- Releases 19.12.3 -->
<strong>5th March 2020</strong> - <a href="releases/2020-03-apps-update/">Releases 19.12.3</a>
<br />
"<em>KDE Ships 19.12.3 Releases.</em>"
<p />
<!-- Plasma 5.18.2 -->
<strong>Tuesday, 25 February 2020</strong> - <a href="plasma-5.18.2.php">Plasma 5.18.2</a>
<br />
"<em>KDE Ships Plasma 5.18.2.</em>"
<p />
<!-- Plasma 5.18.1 -->
<strong>Tuesday, 18 February 2020</strong> - <a href="plasma-5.18.1">Plasma 5.18.1</a>
<br />
"<em>KDE Ships Plasma 5.18.1.</em>"
<p />
<!-- Plasma 5.18.0 -->
<strong>Tuesday, 11 February 2020</strong> - <a href="plasma-5.18.0">Plasma 5.18.0</a>
<br />
"<em>KDE Ships Plasma 5.18.0.</em>"
<p />
<!-- KF 5.67.0 -->
<strong>09th February 2020</strong> - <a href="kde-frameworks-5.67.0.php">KDE Frameworks 5.67.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.67.0.</em>"
<p />
<!-- Releases 19.12.2 -->
<strong>6th February 2020</strong> - <a href="releases/2020-02-apps-update/">Releases 19.12.2</a>
<br />
"<em>KDE Ships 19.12.2 Releases.</em>"
<p />
<!-- Plasma 5.17.90 -->
<strong>Thursday, 16 January 2020</strong> - <a href="plasma-5.17.90.php">Plasma 5.17.90</a>
<br />
"<em>KDE Ships Plasma 5.17.90.</em>"
<p />
<!-- KF 5.66.0 -->
<strong>11th January 2020</strong> - <a href="kde-frameworks-5.66.0.php">KDE Frameworks 5.66.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.66.0.</em>"
<p />
<!-- Releases 19.12 -->
<strong>09th January 2020</strong> - <a href="releases/19.12.1">Releases 19.12.1</a>
<br />
"<em>KDE Ships 19.12.1 Releases.</em>"
<p />
<!-- Plasma 5.17.5 -->
<strong>Tuesday, 7 January 2020</strong> - <a href="plasma-5.17.5.php">Plasma 5.17.5</a>
<br />
"<em>KDE Ships Plasma 5.17.5.</em>"
<p />
<!-- Releases 19.12 -->
<strong>12th December 2019</strong> - <a href="releases/19.12">Releases 19.12</a>
<br />
"<em>KDE Ships 19.12 Releases.</em>"
<p />
<!-- Plasma 5.17.4 -->
<strong>Tuesday, 3 December 2019</strong> - <a href="plasma-5.17.4.php">Plasma 5.17.4</a>
<br />
"<em>KDE Ships Plasma 5.17.4.</em>"
<p />
<!-- KDE Applications 19.11.90 -->
<strong>29th November 2019</strong> - <a href="releases/19.12-rc">KDE Applications 19.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 19.12 Release Candidate.</em>"
<p />
<!-- Plasma 5.17.3 -->
<strong>Tuesday, 12 November 2019</strong> - <a href="plasma-5.17.3.php">Plasma 5.17.3</a>
<br />
"<em>KDE Ships Plasma 5.17.3.</em>"
<p />
<!-- KF 5.64.0 -->
<strong>10th November 2019</strong> - <a href="kde-frameworks-5.64.0.php">KDE Frameworks 5.64.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.64.0.</em>"
<p />
<!-- KDE Applications 19.08.3 -->
<strong>7th November 2019</strong> - <a href="announce-applications-19.08.3.php">KDE Applications 19.08.3 released</a>
<br />
"<em>KDE Ships Applications 19.08.3.</em>"
<p />
<!-- Plasma 5.17.2 -->
<strong>Tuesday, 29 October 2019</strong> - <a href="plasma-5.17.2.php">Plasma 5.17.2</a>
<br />
"<em>KDE Ships Plasma 5.17.2.</em>"
<p />
<!-- Plasma 5.17.1 -->
<strong>Tuesday, 22 October 2019</strong> - <a href="plasma-5.17.1.php">Plasma 5.17.1</a>
<br />
"<em>KDE Ships Plasma 5.17.1.</em>"
<p />
<!-- Plasma 5.17.0 -->
<strong>Tuesday, 15 October 2019</strong> - <a href="plasma-5.17.0.php">Plasma 5.17.0</a>
<br />
"<em>KDE Ships Plasma 5.17.0.</em>"
<p />
<!-- KF 5.63.0 -->
<strong>12th October 2019</strong> - <a href="kde-frameworks-5.63.0.php">KDE Frameworks 5.63.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.63.0.</em>"
<p /><!-- KDE Applications 19.08.2 -->
<strong>10th October 2019</strong> - <a href="announce-applications-19.08.2.php">KDE Applications 19.08.2 released</a>
<br />
"<em>KDE Ships Applications 19.08.2.</em>"
<p />
<!-- Plasma 5.16.90 -->
<strong>Thursday, 19 September 2019</strong> - <a href="plasma-5.16.90.php">Plasma 5.16.90</a>
<br />
"<em>KDE Ships Plasma 5.16.90.</em>"
<p />
<!-- KF 5.62.0 -->
<strong>14th September 2019</strong> - <a href="kde-frameworks-5.62.0.php">KDE Frameworks 5.62.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.62.0.</em>"
<p /><!-- Plasma 5.12.9 -->
<strong>Tuesday, 10 September 2019</strong> - <a href="plasma-5.12.9.php">Plasma 5.12.9</a>
<br />
"<em>KDE Ships Plasma 5.12.9.</em>"
<p />
<!-- KDE Applications 19.08.1 -->
<strong>5th September 2019</strong> - <a href="announce-applications-19.08.1.php">KDE Applications 19.08.1 released</a>
<br />
"<em>KDE Ships Applications 19.08.1.</em>"
<p />
<!-- Plasma 5.16.5 -->
<strong>Tuesday, 3 September 2019</strong> - <a href="plasma-5.16.5.php">Plasma 5.16.5</a>
<br />
"<em>KDE Ships Plasma 5.16.5.</em>"
<p />
<!-- KDE Applications 19.08.0 -->
<strong>15th August 2019</strong> - <a href="announce-applications-19.08.0.php">KDE Applications 19.08 released</a>
<br />
"<em>KDE Ships Applications 19.08.</em>"
<p />
<!-- KF 5.61.0 -->
<strong>10th August 2019</strong> - <a href="kde-frameworks-5.61.0.php">KDE Frameworks 5.61.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.61.0.</em>"
<p /><!-- KDE Applications 19.07.90 -->
<strong>2nd August 2019</strong> - <a href="announce-applications-19.08-rc.php">KDE Applications 19.08 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 19.08 Release Candidate.</em>"
<p />
<!-- Plasma 5.16.4 -->
<strong>Tuesday, 30 July 2019</strong> - <a href="plasma-5.16.4.php">Plasma 5.16.4</a>
<br />
"<em>KDE Ships Plasma 5.16.4.</em>"
<p />

<!-- KDE Applications 19.03.80 -->
<strong>19th July 2019</strong> - <a href="announce-applications-19.08-beta.php">KDE Applications 19.08 Beta released</a>
<br />
"<em>KDE Ships Applications 19.08 Beta.</em>"
<!-- KF 5.60.0 -->
<p />
<strong>13th July 2019</strong> - <a href="kde-frameworks-5.60.0.php">KDE Frameworks 5.60.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.60.0.</em>"
<p /><!-- KDE Applications 19.04.3 -->
<strong>11th July 2019</strong> - <a href="announce-applications-19.04.3.php">KDE Applications 19.04.3 released</a>
<br />
"<em>KDE Ships Applications 19.04.3.</em>"
<p />
<!-- Plasma 5.16.3 -->
<strong>Tuesday, 9 July 2019</strong> - <a href="plasma-5.16.3.php">Plasma 5.16.3</a>
<br />
"<em>KDE Ships Plasma 5.16.3.</em>"
<p />
<!-- Plasma 5.16.2 -->
<strong>Tuesday, 25 June 2019</strong> - <a href="plasma-5.16.2.php">Plasma 5.16.2</a>
<br />
"<em>KDE Ships Plasma 5.16.2.</em>"
<p />
<!-- Plasma 5.16.1 -->
<strong>Tuesday, 18 June 2019</strong> - <a href="plasma-5.16.1.php">Plasma 5.16.1</a>
<br />
"<em>KDE Ships Plasma 5.16.1.</em>"
<p />
<!-- Plasma 5.16.0 -->
<strong>Tuesday, 11 June 2019</strong> - <a href="plasma-5.16.0.php">Plasma 5.16.0</a>
<br />
"<em>KDE Ships Plasma 5.16.0.</em>"
<p />
<!-- KF 5.59.0 -->
<strong>08th June 2019</strong> - <a href="kde-frameworks-5.59.0.php">KDE Frameworks 5.59.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.59.0.</em>"
<p /><!-- KDE Applications 19.04.2 -->
<strong>6th June 2019</strong> - <a href="announce-applications-19.04.2.php">KDE Applications 19.04.2 released</a>
<br />
"<em>KDE Ships Applications 19.04.2.</em>"
<p />
<!-- Plasma 5.15.90 -->
<strong>Thursday, 16 May 2019</strong> - <a href="plasma-5.15.90.php">Plasma 5.15.90</a>
<br />
"<em>KDE Ships Plasma 5.15.90.</em>"
<p />
<!-- KF 5.58.0 -->
<strong>13th May 2019</strong> - <a href="kde-frameworks-5.58.0.php">KDE Frameworks 5.58.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.58.0.</em>"
<p /><!-- KDE Applications 19.04.1 -->
<strong>9th May 2019</strong> - <a href="announce-applications-19.04.1.php">KDE Applications 19.04.1 released</a>
<br />
"<em>KDE Ships Applications 19.04.1.</em>"
<p />
<!-- Plasma 5.15.5 -->
<strong>Tuesday, 7 May 2019</strong> - <a href="plasma-5.15.5.php">Plasma 5.15.5</a>
<br />
"<em>KDE Ships Plasma 5.15.5.</em>"
<p />
<!-- KDE Applications 19.04.0 -->
<strong>18th April 2019</strong> - <a href="announce-applications-19.04.0.php">KDE Applications 19.04 released</a>
<br />
"<em>KDE Ships Applications 19.04.</em>"
<p />
<!-- KF 5.57.0 -->
<strong>13th April 2019</strong> - <a href="kde-frameworks-5.57.0.php">KDE Frameworks 5.57.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.57.0.</em>"
<p /><!-- KDE Applications 19.03.90 -->
<strong>5th April 2019</strong> - <a href="announce-applications-19.04-rc.php">KDE Applications 19.04 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 19.04 Release Candidate.</em>"
<p />
<!-- Plasma 5.15.4 -->
<strong>Tuesday, 2 April 2019</strong> - <a href="plasma-5.15.4.php">Plasma 5.15.4</a>
<br />
"<em>KDE Ships Plasma 5.15.4.</em>"
<p />
<!-- KDE Applications 19.03.80 -->
<strong>22nd March 2019</strong> - <a href="announce-applications-19.04-beta.php">KDE Applications 19.04 Beta released</a>
<br />
"<em>KDE Ships Applications 19.04 Beta.</em>"
<p />
<!-- Plasma 5.15.3 -->
<strong>Tuesday, 12 March 2019</strong> - <a href="plasma-5.15.3.php">Plasma 5.15.3</a>
<br />
"<em>KDE Ships Plasma 5.15.3.</em>"
<p />
<!-- KF 5.56.0 -->
<strong>09th March 2019</strong> - <a href="kde-frameworks-5.56.0.php">KDE Frameworks 5.56.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.56.0.</em>"
<p /><!-- KDE Applications 18.12.3 -->
<strong>7th March 2019</strong> - <a href="announce-applications-18.12.3.php">KDE Applications 18.12.3 released</a>
<br />
"<em>KDE Ships Applications 18.12.3.</em>"
<p />
<!-- Plasma 5.12.8 -->
<strong>Tuesday, 05 March 2019</strong> - <a href="plasma-5.12.8.php">Plasma 5.12.8</a>
<br />
"<em>KDE Ships Plasma 5.12.8.</em>"
<p />
<!-- Plasma 5.15.2 -->
<strong>Tuesday, 26 February 2019</strong> - <a href="plasma-5.15.2.php">Plasma 5.15.2</a>
<br />
"<em>KDE Ships Plasma 5.15.2.</em>"
<p />
<!-- Plasma 5.15.1 -->
<strong>Tuesday, 19 February 2019</strong> - <a href="plasma-5.15.1.php">Plasma 5.15.1</a>
<br />
"<em>KDE Ships Plasma 5.15.1.</em>"
<p />
<!-- Plasma 5.15.0 -->
<strong>Tuesday, 12 February 2019</strong> - <a href="plasma-5.15.0.php">Plasma 5.15.0</a>
<br />
"<em>KDE Ships Plasma 5.15.0.</em>"
<p />
<!-- KF 5.55.0 -->
<strong>09th February 2019</strong> - <a href="kde-frameworks-5.55.0.php">KDE Frameworks 5.55.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.55.0.</em>"
<p /><!-- KDE Applications 18.12.2 -->
<strong>7th February 2019</strong> - <a href="announce-applications-18.12.2.php">KDE Applications 18.12.2 released</a>
<br />
"<em>KDE Ships Applications 18.12.2.</em>"
<p />
<!-- Plasma 5.14.90 -->
<strong>Thursday, 17 January 2019</strong> - <a href="plasma-5.14.90.php">Plasma 5.14.90</a>
<br />
"<em>KDE Ships Plasma 5.14.90.</em>"
<p />
<!-- KF 5.54.0 -->
<strong>12th January 2019</strong> - <a href="kde-frameworks-5.54.0.php">KDE Frameworks 5.54.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.54.0.</em>"
<p /><!-- KDE Applications 18.12.1 -->
<strong>10th January 2019</strong> - <a href="announce-applications-18.12.1.php">KDE Applications 18.12.1 released</a>
<br />
"<em>KDE Ships Applications 18.12.1.</em>"
<p />
<!-- Plasma 5.14.5 -->
<strong>Tuesday, 8 January 2019</strong> - <a href="plasma-5.14.5.php">Plasma 5.14.5</a>
<br />
"<em>KDE Ships Plasma 5.14.5.</em>"
<p />
<!-- KDE Applications 18.12.0 -->
<strong>13th December 2018</strong> - <a href="announce-applications-18.12.0.php">KDE Applications 18.12 released</a>
<br />
"<em>KDE Ships Applications 18.12.</em>"
<p />
<!-- KF 5.53.0 -->
<strong>09th December 2018</strong> - <a href="kde-frameworks-5.53.0.php">KDE Frameworks 5.53.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.53.0.</em>"
<p /><!-- KDE Applications 18.11.90 -->
<strong>30th November 2018</strong> - <a href="announce-applications-18.12-rc.php">KDE Applications 18.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 18.12 Release Candidate.</em>"
<p />
<!-- Plasma 5.14.4 -->
<strong>Tuesday, 27 November 2018</strong> - <a href="plasma-5.14.4.php">Plasma 5.14.4</a>
<br />
"<em>KDE Ships Plasma 5.14.4.</em>"
<p />
<!-- KDE Applications 18.11.80 -->
<strong>16th November 2018</strong> - <a href="announce-applications-18.12-beta.php">KDE Applications 18.12 Beta released</a>
<br />
"<em>KDE Ships Applications 18.12 Beta.</em>"
<p />
<!-- KF 5.52.0 -->
<strong>10th November 2018</strong> - <a href="kde-frameworks-5.52.0.php">KDE Frameworks 5.52.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.52.0.</em>"
<p /><!-- KDE Applications 18.08.3 -->
<strong>8th November 2018</strong> - <a href="announce-applications-18.08.3.php">KDE Applications 18.08.3 released</a>
<br />
"<em>KDE Ships Applications 18.08.3.</em>"
<p />
<!-- Plasma 5.14.3 -->
<strong>Tuesday, 6 November 2018</strong> - <a href="plasma-5.14.3.php">Plasma 5.14.3</a>
<br />
"<em>KDE Ships Plasma 5.14.3.</em>"
<p />
<!-- Plasma 5.14.2 -->
<strong>Tuesday, 23 October 2018</strong> - <a href="plasma-5.14.2.php">Plasma 5.14.2</a>
<br />
"<em>KDE Ships Plasma 5.14.2.</em>"
<p />
<!-- Plasma 5.14.1 -->
<strong>Tuesday, 16 October 2018</strong> - <a href="plasma-5.14.1.php">Plasma 5.14.1</a>
<br />
"<em>KDE Ships Plasma 5.14.1.</em>"
<p />
<!-- KF 5.51.0 -->
<strong>15th October 2018</strong> - <a href="kde-frameworks-5.51.0.php">KDE Frameworks 5.51.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.51.0.</em>"
<p /><!-- KDE Applications 18.08.2 -->
<strong>11th October 2018</strong> - <a href="announce-applications-18.08.2.php">KDE Applications 18.08.2 released</a>
<br />
"<em>KDE Ships Applications 18.08.2.</em>"
<p />
<!-- Plasma 5.14.0 -->
<strong>Tuesday, 09 October 2018</strong> - <a href="plasma-5.14.0.php">Plasma 5.14.0</a>
<br />
"<em>KDE Ships Plasma 5.14.0.</em>"
<p />
<!-- Plasma 5.12.7 -->
<strong>Tuesday, 25 September 2018</strong> - <a href="plasma-5.12.7.php">Plasma 5.12.7</a>
<br />
"<em>KDE Ships Plasma 5.12.7.</em>"
<p />
<!-- Plasma 5.13.90 -->
<strong>Thu, 13 September 2018</strong> - <a href="plasma-5.13.90.php">Plasma 5.13.90</a>
<br />
"<em>KDE Ships Plasma 5.13.90.</em>"
<p />
<!-- KF 5.50.0 -->
<strong>08th September 2018</strong> - <a href="kde-frameworks-5.50.0.php">KDE Frameworks 5.50.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.50.0.</em>"
<p /><!-- KDE Applications 18.08.1 -->
<strong>6th September 2018</strong> - <a href="announce-applications-18.08.1.php">KDE Applications 18.08.1 released</a>
<br />
"<em>KDE Ships Applications 18.08.1.</em>"
<p />
<!-- Plasma 5.13.5 -->
<strong>Tue, 4 September 2018</strong> - <a href="plasma-5.13.5.php">Plasma 5.13.5</a>
<br />
"<em>KDE Ships Plasma 5.13.5.</em>"
<p />
<!-- KDE Applications 18.08.0 -->
<strong>16th August 2018</strong> - <a href="announce-applications-18.08.0.php">KDE Applications 18.08 released</a>
<br />
"<em>KDE Ships Applications 18.08.</em>"
<p />
<!-- KF 5.49.0 -->
<strong>11th August 2018</strong> - <a href="kde-frameworks-5.49.0.php">KDE Frameworks 5.49.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.49.0.</em>"
<p /><!-- KDE Applications 18.07.90 -->
<strong>3rd August 2018</strong> - <a href="announce-applications-18.08-rc.php">KDE Applications 18.08 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 18.08 Release Candidate.</em>"
<p />
<!-- Plasma 5.13.4 -->
<strong>Tue, 31 Jul 2018</strong> - <a href="plasma-5.13.4.php">Plasma 5.13.4</a>
<br />
"<em>KDE Ships Plasma 5.13.4.</em>"
<p />
<!-- KDE Applications 18.03.80 -->
<strong>20th July 2018</strong> - <a href="announce-applications-18.08-beta.php">KDE Applications 18.08 Beta released</a>
<br />
"<em>KDE Ships Applications 18.08 Beta.</em>"
<p />
<!-- KF 5.48.0 -->
<strong>14th July 2018</strong> - <a href="kde-frameworks-5.48.0.php">KDE Frameworks 5.48.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.48.0.</em>"
<p /><!-- KDE Applications 18.04.3 -->
<strong>12th July 2018</strong> - <a href="announce-applications-18.04.3.php">KDE Applications 18.04.3 released</a>
<br />
"<em>KDE Ships Applications 18.04.3.</em>"
<p />
<!-- Plasma 5.13.3 -->
<strong>Tuesday, 10 July 2018</strong> - <a href="plasma-5.13.3.php">Plasma 5.13.3</a>
<br />
"<em>KDE Ships Plasma 5.13.3.</em>"
<p />
<!-- Plasma 5.12.6 -->
<strong>Wednesday, 27 June 2018</strong> - <a href="plasma-5.12.6.php">Plasma 5.12.6</a>
<br />
"<em>KDE Ships Plasma 5.12.6.</em>"
<p />
<!-- Plasma 5.13.2 -->
<strong>Tuesday, 26 June 2018</strong> - <a href="plasma-5.13.2.php">Plasma 5.13.2</a>
<br />
"<em>KDE Ships Plasma 5.13.2.</em>"
<p />
<!-- Plasma 5.13.1 -->
<strong>Tuesday, 19 June 2018</strong> - <a href="plasma-5.13.1.php">Plasma 5.13.1</a>
<br />
"<em>KDE Ships Plasma 5.13.1.</em>"
<p />
<!-- Plasma 5.13.0 -->
<strong>Tuesday, 12 June 2018</strong> - <a href="plasma-5.13.0.php">Plasma 5.13.0</a>
<br />
"<em>KDE Ships Plasma 5.13.0.</em>"
<p />
<!-- KF 5.47.0 -->
<strong>09th June 2018</strong> - <a href="kde-frameworks-5.47.0.php">KDE Frameworks 5.47.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.47.0.</em>"
<p />
<!-- KDE Applications 18.04.2 -->
<strong>7th June 2018</strong> - <a href="announce-applications-18.04.2.php">KDE Applications 18.04.2 released</a>
<br />
"<em>KDE Ships Applications 18.04.2.</em>"
<p />
<!-- Plasma 5.12.90 -->
<strong>Friday, 18 May 2018</strong> - <a href="plasma-5.12.90.php">Plasma 5.12.90</a>
<br />
"<em>KDE Ships Plasma 5.12.90.</em>"
<p />
<!-- KF 5.46.0 -->
<strong>12th May 2018</strong> - <a href="kde-frameworks-5.46.0.php">KDE Frameworks 5.46.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.46.0.</em>"
<p /><!-- KDE Applications 18.04.1 -->
<strong>10th May 2018</strong> - <a href="announce-applications-18.04.1.php">KDE Applications 18.04.1 released</a>
<br />
"<em>KDE Ships Applications 18.04.1.</em>"
<p />
<!-- Plasma 5.12.5 -->
<strong>Tuesday, 1 May 2018</strong> - <a href="plasma-5.12.5.php">Plasma 5.12.5</a>
<br />
"<em>KDE Ships Plasma 5.12.5.</em>"
<p />
<!-- KDE Applications 18.04.0 -->
<strong>19th April 2018</strong> - <a href="announce-applications-18.04.0.php">KDE Applications 18.04 released</a>
<br />
"<em>KDE Ships Applications 18.04.</em>"
<p />
<!-- KF 5.45.0 -->
<strong>14th April 2018</strong> - <a href="kde-frameworks-5.45.0.php">KDE Frameworks 5.45.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.45.0.</em>"
<p /><!-- KDE Applications 18.03.90 -->
<strong>6th April 2018</strong> - <a href="announce-applications-18.04-rc.php">KDE Applications 18.04 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 18.04 Release Candidate.</em>"
<p />
<!-- Plasma 5.12.4 -->
<strong>Tuesday, 27 March 2018</strong> - <a href="plasma-5.12.4.php">Plasma 5.12.4</a>
<br />
"<em>KDE Ships Plasma 5.12.4.</em>"
<p />
<!-- KDE Applications 18.03.80 -->
<strong>23rd March 2018</strong> - <a href="announce-applications-18.04-beta.php">KDE Applications 18.04 Beta released</a>
<br />
"<em>KDE Ships Applications 18.04 Beta.</em>"
<p />
<!-- KF 5.44.0 -->
<strong>10th March 2018</strong> - <a href="kde-frameworks-5.44.0.php">KDE Frameworks 5.44.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.44.0.</em>"
<p /><!-- KDE Applications 17.12.3 -->
<strong>8th March 2018</strong> - <a href="announce-applications-17.12.3.php">KDE Applications 17.12.3 released</a>
<br />
"<em>KDE Ships Applications 17.12.3.</em>"
<p />
<!-- Plasma 5.12.3 -->
<strong>Tuesday, 6 March 2018</strong> - <a href="plasma-5.12.3.php">Plasma 5.12.3</a>
<br />
"<em>KDE Ships Plasma 5.12.3.</em>"
<p />
<!-- Plasma 5.12.2 -->
<strong>Tuesday, 20 February 2018</strong> - <a href="plasma-5.12.2.php">Plasma 5.12.2</a>
<br />
"<em>KDE Ships Plasma 5.12.2.</em>"
<p />
<!-- Plasma 5.12.1 -->
<strong>Tuesday, 13 February 2018</strong> - <a href="plasma-5.12.1.php">Plasma 5.12.1</a>
<br />
"<em>KDE Ships Plasma 5.12.1.</em>"
<p />
<!-- KF 5.43.0 -->
<strong>12th February 2018</strong> - <a href="kde-frameworks-5.43.0.php">KDE Frameworks 5.43.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.43.0.</em>"
<p /><!-- KDE Applications 17.12.2 -->
<strong>8th February 2018</strong> - <a href="announce-applications-17.12.2.php">KDE Applications 17.12.2 released</a>
<br />
"<em>KDE Ships Applications 17.12.2.</em>"
<p />
<!-- Plasma 5.8.9 -->
<strong>Wednesday, 7 February 2018</strong> - <a href="plasma-5.8.9.php">Plasma 5.8.9</a>
<br />
"<em>KDE Ships Plasma 5.8.9.</em>"
<p />
<!-- Plasma 5.12.0 -->
<strong>Tuesday, 06 February 2018</strong> - <a href="plasma-5.12.0.php">Plasma 5.12.0</a>
<br />
"<em>KDE Ships Plasma 5.12.0.</em>"
<p />
<!-- Plasma 5.11.95 -->
<strong>Monday, 15 January 2018</strong> - <a href="plasma-5.11.95.php">Plasma 5.11.95</a>
<br />
"<em>KDE Ships Plasma 5.11.95.</em>"
<p />
<!-- KF 5.42.0 -->
<strong>13th January 2018</strong> - <a href="kde-frameworks-5.42.0.php">KDE Frameworks 5.42.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.42.0.</em>"
<p />
<!-- KDE Applications 17.12.1 -->
<strong>11th January 2018</strong> - <a href="announce-applications-17.12.1.php">KDE Applications 17.12.1 released</a>
<br />
"<em>KDE Ships Applications 17.12.1.</em>"
<p />
<!-- Plasma 5.11.5 -->
<strong>Tuesday, 2 January 2018</strong> - <a href="plasma-5.11.5.php">Plasma 5.11.5</a>
<br />
"<em>KDE Ships Plasma 5.11.5.</em>"
<p />
<!-- KDE Applications 17.12.0 -->
<strong>14th December 2017</strong> - <a href="announce-applications-17.12.0.php">KDE Applications 17.12 released</a>
<br />
"<em>KDE Ships Applications 17.12.</em>"
<p />
<!-- KF 5.41.0 -->
<strong>10th December 2017</strong> - <a href="kde-frameworks-5.41.0.php">KDE Frameworks 5.41.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.41.0.</em>"
<p /><!-- KDE Applications 17.11.90 -->
<strong>1st December 2017</strong> - <a href="announce-applications-17.12-rc.php">KDE Applications 17.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 17.12 Release Candidate.</em>"
<p />
<!-- Plasma 5.11.4 -->
<strong>Tuesday, 28 November 2017</strong> - <a href="plasma-5.11.4.php">Plasma 5.11.4</a>
<br />
"<em>KDE Ships Plasma 5.11.4.</em>"
<p />
<!-- KDE Applications 17.11.80 -->
<strong>17th November 2017</strong> - <a href="announce-applications-17.12-beta.php">KDE Applications 17.12 Beta released</a>
<br />
"<em>KDE Ships Applications 17.12 Beta.</em>"
<p />
<!-- KF 5.40.0 -->
<strong>11th November 2017</strong> - <a href="kde-frameworks-5.40.0.php">KDE Frameworks 5.40.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.40.0.</em>"
<p /><!-- KDE Applications 17.08.3 -->
<strong>9th November 2017</strong> - <a href="announce-applications-17.08.3.php">KDE Applications 17.08.3 released</a>
<br />
"<em>KDE Ships Applications 17.08.3.</em>"
<p />
<!-- Plasma 5.11.3 -->
<strong>Tuesday, 7 November 2017</strong> - <a href="plasma-5.11.3.php">Plasma 5.11.3</a>
<br />
"<em>KDE Ships Plasma 5.11.3.</em>"
<p />
<!-- Plasma 5.8.8 -->
<strong>Wednesday, 25 October 2017</strong> - <a href="plasma-5.8.8.php">Plasma 5.8.8</a>
<br />
"<em>KDE Ships Plasma 5.8.8.</em>"
<p />
<!-- Plasma 5.11.2 -->
<strong>Thursday, 24 October 2017</strong> - <a href="plasma-5.11.2.php">Plasma 5.11.2</a>
<br />
"<em>KDE Ships Plasma 5.11.2.</em>"
<p />
<!-- Plasma 5.11.2 -->
<strong>Thursday, 24 October 2017</strong> - <a href="plasma-5.11.2.php">Plasma 5.11.2</a>
<br />
"<em>KDE Ships Plasma 5.11.2.</em>"
<p />
<!-- Plasma 5.11.1 -->
<strong>Tuesday, 17 October 2017</strong> - <a href="plasma-5.11.1.php">Plasma 5.11.1</a>
<br />
"<em>KDE Ships Plasma 5.11.1.</em>"
<p />
<!-- KF 5.39.0 -->
<strong>14th October 2017</strong> - <a href="kde-frameworks-5.39.0.php">KDE Frameworks 5.39.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.39.0.</em>"
<p /><!-- KDE Applications 17.08.2 -->
<strong>12th October 2017</strong> - <a href="announce-applications-17.08.2.php">KDE Applications 17.08.2 released</a>
<br />
"<em>KDE Ships Applications 17.08.2.</em>"
<p />
<!-- Plasma 5.11.0 -->
<strong>Tuesday, 10 October 2017</strong> - <a href="plasma-5.11.0.php">Plasma 5.11.0</a>
<br />
"<em>KDE Ships Plasma 5.11.0.</em>"
<p />
<!-- Plasma 5.10.95 -->
<strong>Thursday, 14 September 2017</strong> - <a href="kde-purism-librem5.php">Purism and KDE to Work Together on World's First Truly Free Smartphone</a>
<br />
"<em>Purism and KDE to Work Together on World's First Truly Free Smartphone.</em>"
<p />
<strong>Thursday, 14 September 2017</strong> - <a href="plasma-5.10.95.php">Plasma 5.10.95</a>
<br />
"<em>KDE Ships Plasma 5.10.95.</em>"
<p />
<!-- KF 5.38.0 -->
<strong>09th September 2017</strong> - <a href="kde-frameworks-5.38.0.php">KDE Frameworks 5.38.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.38.0.</em>"
<p /><!-- KDE Applications 17.08.1 -->
<strong>7th September 2017</strong> - <a href="announce-applications-17.08.1.php">KDE Applications 17.08.1 released</a>
<br />
"<em>KDE Ships Applications 17.08.1.</em>"
<p />
<!-- Plasma 5.10.5 -->
<strong>Tuesday, 22 August 2017</strong> - <a href="plasma-5.10.5.php">Plasma 5.10.5</a>
<br />
"<em>KDE Ships Plasma 5.10.5.</em>"
<p />
<!-- KDE Applications 17.08.0 -->
<strong>17th August 2017</strong> - <a href="announce-applications-17.08.0.php">KDE Applications 17.08 released</a>
<br />
"<em>KDE Ships Applications 17.08.</em>"
<p />
<!-- KF 5.37.0 -->
<strong>13th August 2017</strong> - <a href="kde-frameworks-5.37.0.php">KDE Frameworks 5.37.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.37.0.</em>"
<p />
<!-- KDE Applications 17.07.90 -->
<strong>4th August 2017</strong> - <a href="announce-applications-17.08-rc.php">KDE Applications 17.08 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 17.08 Release Candidate.</em>"
<p />
<!-- KDE Applications 17.07.80 -->
<strong>21st July 2017</strong> - <a href="announce-applications-17.08-beta.php">KDE Applications 17.08 Beta released</a>
<br />
"<em>KDE Ships Applications 17.08 Beta.</em>"
<p />
<!-- Plasma 5.10.4 -->
<strong>Tuesday, 18 July 2017</strong> - <a href="plasma-5.10.4.php">Plasma 5.10.4</a>
<br />
"<em>KDE Ships Plasma 5.10.4.</em>"
<p />
<!-- KDE Applications 17.04.3 -->
<strong>13th July 2017</strong> - <a href="announce-applications-17.04.3.php">KDE Applications 17.04.3 released</a>
<br />
"<em>KDE Ships Applications 17.04.3.</em>"
<p />
<!-- KF 5.36.0 -->
<strong>08th July 2017</strong> - <a href="kde-frameworks-5.36.0.php">KDE Frameworks 5.36.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.36.0.</em>"
<p /><!-- Plasma 5.10.3 -->
<strong>Tuesday, 27 June 2017</strong> - <a href="plasma-5.10.3.php">Plasma 5.10.3</a>
<br />
"<em>KDE Ships Plasma 5.10.3.</em>"
<p />
<!-- Plasma 5.10.2 -->
<strong>Tuesday, 13 June 2017</strong> - <a href="plasma-5.10.2.php">Plasma 5.10.2</a>
<br />
"<em>KDE Ships Plasma 5.10.2.</em>"
<p />
<!-- KF 5.35.0 -->
<strong>10th June 2017</strong> - <a href="kde-frameworks-5.35.0.php">KDE Frameworks 5.35.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.35.0.</em>"
<p /><!-- KDE Applications 17.04.2 -->
<strong>8th June 2017</strong> - <a href="announce-applications-17.04.2.php">KDE Applications 17.04.2 released</a>
<br />
"<em>KDE Ships Applications 17.04.2.</em>"
<p />
<!-- Plasma 5.10.1 -->
<strong>Tuesday, 6 June 2017</strong> - <a href="plasma-5.10.1.php">Plasma 5.10.1</a>
<br />
"<em>KDE Ships Plasma 5.10.1.</em>"
<p />
<!-- Plasma 5.10.0 -->
<strong>Tuesday, 30 May 2017</strong> - <a href="plasma-5.10.0.php">Plasma 5.10.0</a>
<br />
"<em>KDE Ships Plasma 5.10.0.</em>"
<p />
<!-- Plasma 5.8.7 -->
<strong>Tuesday, 23 May 2017</strong> - <a href="plasma-5.8.7.php">Plasma 5.8.7</a>
<br />
"<em>KDE Ships Plasma 5.8.7.</em>"
<p />
<!-- Plasma 5.9.95 -->
<strong>Monday, 15 May 2017</strong> - <a href="plasma-5.9.95.php">Plasma 5.9.95</a>
<br />
"<em>KDE Ships Plasma 5.9.95.</em>"
<p />
<!-- KF 5.34.0 -->
<strong>13th May 2017</strong> - <a href="kde-frameworks-5.34.0.php">KDE Frameworks 5.34.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.34.0.</em>"
<p /><!-- KDE Applications 17.04.1 -->
<strong>11th May 2017</strong> - <a href="announce-applications-17.04.1.php">KDE Applications 17.04.1 released</a>
<br />
"<em>KDE Ships Applications 17.04.1.</em>"
<p />
<!-- Plasma 5.9.5 -->
<strong>Tuesday, 25 April 2017</strong> - <a href="plasma-5.9.5.php">Plasma 5.9.5</a>
<br />
"<em>KDE Ships Plasma 5.9.5.</em>"
<p />
<!-- KDE Applications 17.04.0 -->
<strong>20th April 2017</strong> - <a href="announce-applications-17.04.0.php">KDE Applications 17.04 released</a>
<br />
"<em>KDE Ships Applications 17.04.</em>"
<p />
<!-- KF 5.33.0 -->
<strong>08th April 2017</strong> - <a href="kde-frameworks-5.33.0.php">KDE Frameworks 5.33.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.33.0.</em>"
<p />
<!-- KDE Applications 17.03.90 -->
<strong>7th April 2017</strong> - <a href="announce-applications-17.04-rc.php">KDE Applications 17.04 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 17.04 Release Candidate.</em>"
<p />
<!-- KDE Applications 17.03.80 -->
<strong>24th March 2017</strong> - <a href="announce-applications-17.04-beta.php">KDE Applications 17.04 Beta released</a>
<br />
"<em>KDE Ships Applications 17.04 Beta.</em>"
<p />
<!-- Plasma 5.9.4 -->
<strong>Tuesday, 21 March 2017</strong> - <a href="plasma-5.9.4.php">Plasma 5.9.4</a>
<br />
"<em>KDE Ships Plasma 5.9.4.</em>"
<p />
<!-- KF 5.32.0 -->
<strong>11th March 2017</strong> - <a href="kde-frameworks-5.32.0.php">KDE Frameworks 5.32.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.32.0.</em>"
<p /><!-- KDE Applications 16.12.3 -->
<strong>9th March 2017</strong> - <a href="announce-applications-16.12.3.php">KDE Applications 16.12.3 released</a>
<br />
"<em>KDE Ships Applications 16.12.3.</em>"
<p />
<!-- Plasma 5.9.3 -->
<strong>Tuesday, 28 February 2017</strong> - <a href="plasma-5.9.3.php">Plasma 5.9.3</a>
<br />
"<em>KDE Ships Plasma 5.9.3.</em>"
<p />
<!-- Plasma 5.8.6 -->
<strong>Tuesday, 21 February 2017</strong> - <a href="plasma-5.8.6.php">Plasma 5.8.6</a>
<br />
"<em>KDE Ships Plasma 5.8.6.</em>"
<p />
<!-- Plasma 5.9.2 -->
<strong>Tuesday, 14 February 2017</strong> - <a href="plasma-5.9.2.php">Plasma 5.9.2</a>
<br />
"<em>KDE Ships Plasma 5.9.2.</em>"
<p />
<!-- KF 5.31.0 -->
<strong>11th February 2017</strong> - <a href="kde-frameworks-5.31.0.php">KDE Frameworks 5.31.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.31.0.</em>"
<p /><!-- KDE Applications 16.12.2 -->
<strong>9th February 2017</strong> - <a href="announce-applications-16.12.2.php">KDE Applications 16.12.2 released</a>
<br />
"<em>KDE Ships Applications 16.12.2.</em>"
<p />
<!-- Plasma 5.9.1 -->
<strong>Tuesday, 14 February 2017</strong> - <a href="plasma-5.9.1.php">Plasma 5.9.1</a>
<br />
"<em>KDE Ships Plasma 5.9.1.</em>"
<p />
<!-- Plasma 5.9.0 -->
<strong>Tuesday, 31 January 2017</strong> - <a href="plasma-5.9.0.php">Plasma 5.9.0</a>
<br />
"<em>KDE Ships Plasma 5.9.0.</em>"
<p />
<!-- KF 5.30.0 -->
<strong>14th January 2017</strong> - <a href="kde-frameworks-5.30.0.php">KDE Frameworks 5.30.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.30.0.</em>"
<p />
<!-- KDE Applications 16.12.1 -->
<strong>12th January 2017</strong> - <a href="announce-applications-16.12.1.php">KDE Applications 16.12.1 released</a>
<br />
"<em>KDE Ships Applications 16.12.1.</em>"
<p />
<!-- Plasma 5.8.95 -->
<strong>Thursday, 12 January 2017</strong> - <a href="plasma-5.8.95.php">Plasma 5.8.95</a>
<br />
"<em>KDE Ships Plasma 5.8.95.</em>"
<p />
<!-- Plasma 5.8.5 -->
<strong>Tuesday, 27 December 2016</strong> - <a href="plasma-5.8.5.php">Plasma 5.8.5</a>
<br />
"<em>KDE Ships Plasma 5.8.5.</em>"
<p />
<!-- KDE Applications 16.12.0 -->
<strong>15th December 2016</strong> - <a href="announce-applications-16.12.0.php">KDE Applications 16.12 released</a>
<br />
"<em>KDE Ships Applications 16.12.</em>"
<p />
<!-- KF 5.29.0 -->
<strong>12th December 2016</strong> - <a href="kde-frameworks-5.29.0.php">KDE Frameworks 5.29.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.29.0.</em>"
<p />
<!-- KDE Applications 16.11.90 -->
<strong>2nd December 2016</strong> - <a href="announce-applications-16.12-rc.php">KDE Applications 16.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 16.12 Release Candidate.</em>"
<p />
<!-- Plasma 5.8.4 -->
<strong>Tuesday, 22 November 2016</strong> - <a href="plasma-5.8.4.php">Plasma 5.8.4</a>
<br />
"<em>KDE Ships Plasma 5.8.4.</em>"
<p />
<!-- KDE Applications 16.11.80 -->
<strong>18th November 2016</strong> - <a href="announce-applications-16.12-beta.php">KDE Applications 16.12 Beta released</a>
<br />
"<em>KDE Ships Applications 16.12 Beta.</em>"
<p />
<!-- KF 5.28.0 -->
<strong>15th November 2016</strong> - <a href="kde-frameworks-5.28.0.php">KDE Frameworks 5.28.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.28.0.</em>"
<p /><!-- KDE Applications 16.08.3 -->
<strong>10th November 2016</strong> - <a href="announce-applications-16.08.3.php">KDE Applications 16.08.3 released</a>
<br />
"<em>KDE Ships Applications 16.08.3.</em>"
<p />
<!-- Plasma 5.8.3 -->
<strong>Tuesday, 1 November 2016</strong> - <a href="plasma-5.8.3.php">Plasma 5.8.3</a>
<br />
"<em>KDE Ships Plasma 5.8.3.</em>"
<p />
<!-- Plasma 5.8.2 -->
<strong>Tuesday, 18 October 2016</strong> - <a href="plasma-5.8.2.php">Plasma 5.8.2</a>
<br />
"<em>KDE Ships Plasma 5.8.2.</em>"
<p />
<!-- KDE Applications 16.08.2 -->
<strong>13th October 2016</strong> - <a href="announce-applications-16.08.2.php">KDE Applications 16.08.2 released</a>
<br />
"<em>KDE Ships Applications 16.08.2.</em>"
<p />
<!-- Plasma 5.8.1 -->
<strong>Tuesday, 11 October 2016</strong> - <a href="plasma-5.8.1.php">Plasma 5.8.1</a>
<br />
"<em>KDE Ships Plasma 5.8.1.</em>"
<p />
<!-- KF 5.27.0 -->
<strong>08th October 2016</strong> - <a href="kde-frameworks-5.27.0.php">KDE Frameworks 5.27.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.27.0.</em>"
<p /><!-- Plasma 5.8.0 -->
<strong>Tuesday, 04 October 2016</strong> - <a href="plasma-5.8.0.php">Plasma 5.8.0</a>
<br />
"<em>KDE Ships Plasma 5.8.0.</em>"
<p />
<!-- Kirigami 1.1 -->
<strong>Monday, 26 September 2016</strong> - <a href="kirigami-1.1.php">Kirigami 1.1</a>
<br />
"<em>KDE Ships Kirigami 1.1.</em>"
<p />
<!-- Plasma 5.7.95 -->
<strong>Thursday, 15 September 2016</strong> - <a href="plasma-5.7.95.php">Plasma 5.7.95</a>
<br />
"<em>KDE Ships Plasma 5.7.95.</em>"
<p />
<!-- Plasma 5.7.5 -->
<strong>Tuesday, 13 September 2016</strong> - <a href="plasma-5.7.5.php">Plasma 5.7.5</a>
<br />
"<em>KDE Ships Plasma 5.7.5.</em>"
<p />
<!-- KF 5.26.0 -->
<strong>10th September 2016</strong> - <a href="kde-frameworks-5.26.0.php">KDE Frameworks 5.26.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.26.0.</em>"
<p />
<!-- KDE Applications 16.08.1 -->
<strong>8th September 2016</strong> - <a href="announce-applications-16.08.1.php">KDE Applications 16.08.1 released</a>
<br />
"<em>KDE Ships Applications 16.08.1.</em>"
<p />
<!-- Plasma 5.7.4 -->
<strong>Tuesday, 23 August 2016</strong> - <a href="plasma-5.7.4.php">Plasma 5.7.4</a>
<br />
"<em>KDE Ships Plasma 5.7.4.</em>"
<p />
<!-- KDE Applications 16.08.0 -->
<strong>18th August 2016</strong> - <a href="announce-applications-16.08.0.php">KDE Applications 16.08.0 released</a>
<br />
"<em>KDE Ships Applications 16.08.0.</em>"
<p />
<!-- KF 5.25.0 -->
<strong>13th August 2016</strong> - <a href="kde-frameworks-5.25.0.php">KDE Frameworks 5.25.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.25.0.</em>"
<p />
<!-- KDE Applications 16.07.90 -->
<strong>5th August 2016</strong> - <a href="announce-applications-16.08-rc.php">KDE Applications 16.08 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 16.08 Release Candidate.</em>"
<p />
<!-- Plasma 5.7.3 -->
<strong>Tuesday, 2 August 2016</strong> - <a href="plasma-5.7.3.php">Plasma 5.7.3</a>
<br />
"<em>KDE Ships Plasma 5.7.3.</em>"
<p />
<!-- KDE Applications 16.07.80 -->
<strong>22nd July 2016</strong> - <a href="announce-applications-16.08-beta.php">KDE Applications 16.08 Beta released</a>
<br />
"<em>KDE Ships Applications 16.08 Beta.</em>"
<p />
<!-- Plasma 5.7.2 -->
<strong>Tuesday, 19 July 2016</strong> - <a href="plasma-5.7.2.php">Plasma 5.7.2</a>
<br />
"<em>KDE Ships Plasma 5.7.2.</em>"
<p />
<!-- KDE Applications 16.04.3 -->
<strong>12th July 2016</strong> - <a href="announce-applications-16.04.3.php">KDE Applications 16.04.3 released</a>
<br />
"<em>KDE Ships Applications 16.04.3.</em>"
<p />
<!-- Plasma 5.7.1 -->
<strong>Tuesday, 12 July 2016</strong> - <a href="plasma-5.7.1.php">Plasma 5.7.1</a>
<br />
"<em>KDE Ships Plasma 5.7.1.</em>"
<p />
<!-- KF 5.24.0 -->
<strong>09th July 2016</strong> - <a href="kde-frameworks-5.24.0.php">KDE Frameworks 5.24.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.24.0.</em>"
<p /><!-- Plasma 5.7.0 -->
<strong>Tuesday, 14 June 2016</strong> - <a href="plasma-5.7.0.php">Plasma 5.7.0</a>
<br />
"<em>KDE Ships Plasma 5.7.0.</em>"
<p />
<!-- Plasma 5.6.95 -->
<strong>Tuesday, 14 June 2016</strong> - <a href="plasma-5.6.95.php">Plasma 5.6.95</a>
<br />
"<em>KDE Ships Plasma 5.6.95.</em>"
<p />
<!-- Plasma 5.6.5 -->
<strong>14th June 2016</strong> - <a href="plasma-5.6.5.php">Plasma 5.6.5</a>
<br />
"<em>KDE Ships Plasma 5.6.5.</em>"
<p />
<!-- KDE Applications 16.04.2 -->
<strong>14th June 2016</strong> - <a href="announce-applications-16.04.2.php">KDE Applications 16.04.2 released</a>
<br />
"<em>KDE Ships Applications 16.04.2.</em>"
<p />
<!-- KF 5.23.0 -->
<strong>13th June 2016</strong> - <a href="kde-frameworks-5.23.0.php">KDE Frameworks 5.23.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.23.0.</em>"
<p /><!-- KF 5.22.0 -->
<strong>15th May 2016</strong> - <a href="kde-frameworks-5.22.0.php">KDE Frameworks 5.22.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.22.0.</em>"
<p /><!-- Plasma 5.6.4 -->
<strong>Tuesday, 10 May 2016</strong> - <a href="plasma-5.6.4.php">Plasma 5.6.4</a>
<br />
"<em>KDE Ships Plasma 5.6.4.</em>"
<p />
<strong>10th May 2016</strong> - <a href="announce-applications-16.04.1.php">KDE Applications 16.04.1 released</a>
<br />
"<em>KDE Ships Applications 16.04.1.</em>"
<p />
<strong>20th April 2016</strong> - <a href="announce-applications-16.04.0.php">KDE Applications 16.04 released</a>
<br />
"<em>KDE Ships Applications 16.04.</em>"
<p />
<!-- Plasma 5.6.3 -->
<strong>Tuesday, 19 April 2016</strong> - <a href="plasma-5.6.3.php">Plasma 5.6.3</a>
<br />
"<em>KDE Ships Plasma 5.6.3.</em>"
<p />
<!-- KF 5.21.0 -->
<strong>09th April 2016</strong> - <a href="kde-frameworks-5.21.0.php">KDE Frameworks 5.21.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.21.0.</em>"
<p /><strong>7th April 2016</strong> - <a href="announce-applications-16.04-rc.php">KDE Applications 16.04 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 16.04 Release Candidate.</em>"
<p />
<!-- Plasma 5.6.2 -->
<strong>Tuesday, 05 April 2016</strong> - <a href="plasma-5.6.2.php">Plasma 5.6.2</a>
<br />
"<em>KDE Ships Plasma 5.6.2.</em>"
<p />
<!-- Plasma 5.6.1 -->
<strong>29 March 2016</strong> - <a href="plasma-5.6.1.php">Plasma 5.6.1</a>
<br />
"<em>KDE Ships Plasma 5.6.1.</em>"
<p />
<strong>24th March 2016</strong> - <a href="announce-applications-16.04-beta.php">KDE Applications 16.04 Beta released</a>
<br />
"<em>KDE Ships Applications 16.04 Beta.</em>"
<p />

<!-- Plasma 5.6.0 -->
<strong>22 March 2016</strong> - <a href="plasma-5.6.0.php">Plasma 5.6.0</a>
<br />
"<em>KDE Ships Plasma 5.6.0.</em>"
<p />
<strong>15th March 2016</strong> - <a href="announce-applications-15.12.3.php">KDE Applications 15.12.3 released</a>
<br />
"<em>KDE Ships Applications 15.12.3</em>"
<p />
<!-- KF 5.20.0 -->
<strong>13th March 2016</strong> - <a href="kde-frameworks-5.20.0.php">KDE Frameworks 5.20.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.20.0.</em>"
<p /><!-- Plasma 5.5.95 -->
<strong>02 March 2016</strong> - <a href="plasma-5.5.95.php">Plasma 5.5.95</a>
<br />
"<em>KDE Ships Plasma 5.5.95.</em>"
<p />
<!-- Plasma 5.5.5 -->
<strong>01 March 2016</strong> - <a href="plasma-5.5.5.php">Plasma 5.5.5</a>
<br />
"<em>KDE Ships Plasma 5.5.5.</em>"
<p />
<strong>16th February 2016</strong> - <a href="announce-applications-15.12.2.php">KDE Applications 15.12.2 released</a>
<br />
"<em>KDE Ships Applications 15.12.2</em>"
<p />
<!-- KF 5.19.0 -->
<strong>13th February 2016</strong> - <a href="kde-frameworks-5.19.0.php">KDE Frameworks 5.19.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.19.0.</em>"
<p /><!-- Plasma 5.5.4 -->
<strong>26 January 2016</strong> - <a href="plasma-5.5.4.php">Plasma 5.5.4</a>
<br />
"<em>KDE Ships Plasma 5.5.4.</em>"
<p />
<strong>12th January 2016</strong> - <a href="announce-applications-15.12.1.php">KDE Applications 15.12.1 released</a>
<br />
"<em>KDE Ships Applications 15.12.1</em>"
<p />

<!-- KF 5.18.0 -->
<strong>09th January 2016</strong> - <a href="kde-frameworks-5.18.0.php">KDE Frameworks 5.18.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.18.0.</em>"
<p /><!-- Plasma 5.5.3 -->
<strong>06 January 2016</strong> - <a href="plasma-5.5.3.php">Plasma 5.5.3</a>
<br />
"<em>KDE Ships Plasma 5.5.3.</em>"
<p />
<!-- Plasma 5.5.2 -->
<strong>22 December 2015</strong> - <a href="plasma-5.5.2.php">Plasma 5.5.2</a>
<br />
"<em>KDE Ships Plasma 5.5.2.</em>"
<p />
<strong>16th December 2015</strong> - <a href="announce-applications-15.12.0.php">KDE Applications 15.12 released</a>
<br />
"<em>KDE Ships Applications 15.12.</em>"
<p />

<!-- Plasma 5.5.1 -->
<strong>15 December 2015</strong> - <a href="plasma-5.5.1.php">Plasma 5.5.1</a>
<br />
"<em>KDE Ships Plasma 5.5.1.</em>"
<p />
<!-- KF 5.17.0 -->
<strong>12th December 2015</strong> - <a href="kde-frameworks-5.17.0.php">KDE Frameworks 5.17.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.17.0.</em>"
<p /><!-- Plasma 5.5.0 -->
<strong>08 December 2015</strong> - <a href="plasma-5.5.0.php">Plasma 5.5.0</a>
<br />
"<em>KDE Ships Plasma 5.5.0.</em>"
<p />
<strong>3rd December 2015</strong> - <a href="announce-applications-15.12-rc.php">KDE Applications 15.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 15.12 Release Candidate.</em>"
<p />

<strong>20th November 2015</strong> - <a href="announce-applications-15.12-beta.php">KDE Applications 15.12 Beta released</a>
<br />
"<em>KDE Ships Applications 15.12 Beta.</em>"
<p />

<!-- Plasma 5.4.95 -->
<strong>19 November 2015</strong> - <a href="plasma-5.4.95.php">Plasma 5.4.95</a>
<br />
"<em>KDE Ships Plasma 5.4.95.</em>"
<p />
<!-- KF 5.16.0 -->
<strong>13th November 2015</strong> - <a href="kde-frameworks-5.16.0.php">KDE Frameworks 5.16.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.16.0.</em>"
<p /><!-- Plasma 5.4.3 -->
<strong>10 November 2015</strong> - <a href="plasma-5.4.3.php">Plasma 5.4.3</a>
<br />
"<em>KDE Ships Plasma 5.4.3.</em>"
<p />
<strong>10th November 2015</strong> - <a href="announce-applications-15.08.3.php">KDE Applications 15.08.3 released</a>
<br />
"<em>KDE Ships Applications 15.08.3.</em>"
<p />

<strong>13th October 2015</strong> - <a href="announce-applications-15.08.2.php">KDE Applications 15.08.2 released</a>
<br />
"<em>KDE Ships Applications 15.08.2.</em>"
<p />

<!-- KF 5.15.0 -->
<strong>10th October 2015</strong> - <a href="kde-frameworks-5.15.0.php">KDE Frameworks 5.15.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.15.0.</em>"
<p /><!-- Plasma 5.4.2 -->
<strong>06 October 2015</strong> - <a href="plasma-5.4.2.php">Plasma 5.4.2</a>
<br />
"<em>KDE Ships Plasma 5.4.2.</em>"
<p />

<strong>15th September 2015</strong> - <a href="announce-applications-15.08.1.php">KDE Applications 15.08.1 released</a>
<br />
"<em>KDE Ships Applications 15.08.1.</em>"
<p />

<!-- KF 5.14.0 -->
<strong>12th September 2015</strong> - <a href="kde-frameworks-5.14.0.php">KDE Frameworks 5.14.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.14.0.</em>"
<p /><!-- Plasma 5.4.1 -->
<strong>08 September 2015</strong> - <a href="plasma-5.4.1.php">Plasma 5.4.1</a>
<br />
"<em>KDE Ships Plasma 5.4.1.</em>"
<p />

<!-- Plasma 5.4.0 -->
<strong>25 August 2015</strong> - <a href="plasma-5.4.0.php">Plasma 5.4.0</a>
<br />
"<em>KDE Ships Plasma 5.4.0.</em>"
<p />

<!-- Applications 15.08.0 -->
<strong>19th August 2015</strong> - <a href="announce-applications-15.08.0.php">KDE Applications 15.08 released</a>
<br />
"<em>KDE Ships Applications 15.08.0.</em>"
<p />

<!-- KF 5.13.0 -->
<strong>12th August 2015</strong> - <a href="kde-frameworks-5.13.0.php">KDE Frameworks 5.13.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.13.0.</em>"
<p />

<!-- Plasma 5.3.95 -->
<strong>11 August 2015</strong> - <a href="plasma-5.3.95.php">Plasma 5.3.95</a>
<br />
"<em>KDE Ships Plasma 5.3.95.</em>"
<p />

<!-- Applications 15.07.90 -->
<strong>6th August 2015</strong> - <a href="announce-applications-15.08-rc.php">KDE Applications 15.08 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 15.08 Release Candidate.</em>"
<p />

<strong>28th July 2015</strong> - <a href="announce-applications-15.08-beta.php">KDE Applications 15.08 Beta released</a>
<br />
"<em>KDE Ships Applications 15.08 Beta.</em>"
<p />

<!-- KF 5.12.0 -->
<strong>10th July 2015</strong> - <a href="kde-frameworks-5.12.0.php">KDE Frameworks 5.12.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.12.0.</em>"
<p />

<!-- Applications 15.04.3 -->
<strong>1st July 2015</strong> - <a href="announce-applications-15.04.3.php">KDE Applications 15.04.3 released</a>
<br />
"<em>KDE Ships Applications 15.04.3.</em>"
<p />

<!-- Plasma 5.3.2 -->
<strong>30th June 2015</strong> - <a href="plasma-5.3.2.php">Plasma 5.3.2</a>
<br />
"<em>KDE Ships Plasma 5.3.2.</em>"
<p />
<!-- KF 5.11.0 -->
<strong>12th June 2015</strong> - <a href="kde-frameworks-5.11.0.php">KDE Frameworks 5.11.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.11.0.</em>"
<p />

<!-- Applications 15.04.2 -->
<strong>2nd June 2015</strong> - <a href="announce-applications-15.04.2.php">KDE Applications 15.04.2 released</a>
<br />
"<em>KDE Ships Applications 15.04.2.</em>"
<p />

<!-- Plasma 5.3.1 -->
<strong>26th May 2015</strong> - <a href="plasma-5.3.1.php">Plasma 5.3.1</a>
<br />
"<em>KDE Ships Plasma 5.3.1.</em>"
<p />

<!-- Applications 15.04.1 -->
<strong>12th May 2015</strong> - <a href="announce-applications-15.04.1.php">KDE Applications 15.04.1 released</a>
<br />
"<em>KDE Ships Applications 15.04.1.</em>"
<p />

<!-- KF 5.10.0 -->
<strong>08th May 2015</strong> - <a href="kde-frameworks-5.10.0.php">KDE Frameworks 5.10.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.10.0.</em>"
<p />
<!-- Plasma 5.3.0 -->
<strong>28th April 2015</strong> - <a href="plasma-5.3.0.php">Plasma 5.3.0</a>
<br />
"<em>KDE Ships Plasma 5.3.</em>"
<p />

<!-- Applications 15.04 -->
<strong>15th April 2015</strong> - <a href="announce-applications-15.04.0.php">KDE Applications 15.04 released</a>
<br />
"<em>KDE Ships Applications 15.04.</em>"
<p />

<!-- Plasma 5.2.95 -->
<strong>14th April 2015</strong> - <a href="plasma-5.2.95.php">Plasma 5.2.95</a>
<br />
"<em>KDE Ships Plasma 5.3 Beta.</em>"
<p />

<!-- KF 5.9.0 -->
<strong>10th April 2015</strong> - <a href="kde-frameworks-5.9.0.php">KDE Frameworks 5.9.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.9.0.</em>"
<p />

<!-- Applications 15.04 RC -->
<strong>26th March 2015</strong> - <a href="announce-applications-15.04-rc.php">KDE Applications 15.04 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 15.04 Release Candidate.</em>"
<p />

<!-- Plasma 5.2.2 -->
<strong>24 Mar 2015</strong> - <a href="plasma-5.2.2.php">Plasma 5.2.2</a>
<br />
"<em>KDE Ships Plasma 5.2.2.</em>"
<p />

<!-- Applications 15.04 Beta 3 -->
<strong>19th March 2015</strong> - <a href="announce-applications-15.04-beta3.php">KDE Applications 15.04 Beta 3 released</a>
<br />
"<em>KDE Ships Applications 15.04 Beta 3.</em>"
<p />

<!-- KF 5.8.0 -->
<strong>13th March 2015</strong> - <a href="kde-frameworks-5.8.0.php">KDE Frameworks 5.8.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.8.0.</em>"
<p />

<!-- Applications 15.04 Beta 2 -->
<strong>12th March 2015</strong> - <a href="announce-applications-15.04-beta2.php">KDE Applications 15.04 Beta 2 released</a>
<br />
"<em>KDE Ships Applications 15.04 Beta 2.</em>"
<p />

<!-- Applications 15.04 Beta1 -->
<strong>6th March 2015</strong> - <a href="announce-applications-15.04-beta1.php">KDE Applications 15.04 Beta 1 released</a>
<br />
"<em>KDE Ships Applications 15.04 Beta 1.</em>"
<p />

<!-- Applications 14.12.3 -->
<strong>3rd March 2015</strong> - <a href="announce-applications-14.12.3.php">KDE Applications 14.12.3 released</a>
<br />
"<em>KDE Ships Applications 14.12.3.</em>"
<p />

<!-- Plasma 5.2.1 -->
<strong>24 Feb 2015</strong> - <a href="plasma-5.2.1.php">Plasma 5.2.1</a>
<br />
"<em>KDE Ships Plasma 5.2.1.</em>"
<p />

<!-- KF 5.7.0 -->
<strong>14th February 2015</strong> - <a href="kde-frameworks-5.7.0.php">KDE Frameworks 5.7.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.7.0.</em>"
<p />

<!-- Applications 14.12.2 -->
<strong>3rd February 2015</strong> - <a href="announce-applications-14.12.2.php">KDE Applications 14.12.2 released</a>
<br />
"<em>KDE Ships Applications 14.12.2.</em>"
<p />

<!-- Plasma 5.2.0 -->
<strong>27th January 2015</strong> - <a href="plasma-5.2.0.php">Plasma 5.2</a>
<br />
"<em>KDE Ships Plasma 5.2.</em>"
<p />

<!-- Plasma 5.1.95 -->
<strong>13th January 2015</strong> - <a href="plasma-5.1.95.php">Plasma 5.1.95</a>
<br />
"<em>KDE Ships Beta release of Plasma 5.2.</em>"
<p />

<!-- Applications 14.12.1 -->
<strong>13th January 2015</strong> - <a href="announce-applications-14.12.1.php">KDE Applications 14.12.1 released</a>
<br />
"<em>KDE Ships Applications 14.12.1.</em>"
<p />

<!-- KF 5.6.0 -->
<strong>08th January 2015</strong> - <a href="kde-frameworks-5.6.0.php">KDE Frameworks 5.6.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.6.0.</em>"
<p />
<!-- Applications 14.12 -->
<strong>17th December 2014</strong> - <a href="announce-applications-14.12.0.php">KDE Applications 14.12 released</a>
<br />
"<em>KDE Ships Applications 14.12.</em>"
<p />

<!-- Plasma 5.1.2 -->
<strong>16th December 2014</strong> - <a href="plasma-5.1.2.php">Plasma 5.1.2</a>
<br />
"<em>KDE Ships Bugfix Release of Plasma 5.</em>"
<p />

<!-- KF 5.5.0 -->
<strong>11th December 2014</strong> - <a href="kde-frameworks-5.5.0.php">KDE Frameworks 5.5.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.5.0.</em>"
<p />

<!-- Applications 14.12 RC -->
<strong>27th November 2014</strong> - <a href="announce-applications-14.12-rc.php">KDE Applications 14.12 Release Candidate released</a>
<br />
"<em>KDE Ships Applications 14.12 Release Candidate.</em>"
<p />

<!-- Applications 14.12 Beta 3 -->
<strong>20th November 2014</strong> - <a href="announce-applications-14.12-beta3.php">KDE Applications 14.12 Beta 3 released</a>
<br />
"<em>KDE Ships Applications 14.12 Beta 3.</em>"
<p />

<!-- Applications 14.12 Beta 2 -->
<strong>13th November 2014</strong> - <a href="announce-applications-14.12-beta2.php">KDE Applications 14.12 Beta 2 released</a>
<br />
"<em>KDE Ships Applications 14.12 Beta 2.</em>"
<p />

<!-- Plasma 5.1.1 -->
<strong>11th November 2014</strong> - <a href="plasma-5.1.1.php">Plasma 5.1.1</a>
<br />
"<em>KDE Ships Bugfix Release of Plasma 5.</em>"
<p />

<!-- 4.14.3 released -->
<strong>11th November 2014</strong> - <a href="announce-4.14.3.php">KDE Announces 4.14.3</a>
<br />
"<em>KDE Ships Applications and Platform 4.14.3.</em>"
<p />

<!-- Applications 14.12 Beta 1 -->
<strong>6th November 2014</strong> - <a href="announce-applications-14.12-beta1.php">KDE Applications 14.12 Beta 1 released</a>
<br />
"<em>KDE Ships Applications 14.12 Beta 1.</em>"
<p />
<!-- KF 5.4.0 -->
<strong>6th November 2014</strong> - <a href="kde-frameworks-5.4.0.php">KDE Frameworks 5.4.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.4.0.</em>"
<p />
<!-- Plasma 5.1.0 -->
<strong>15th October 2014</strong> - <a href="plasma-5.1">Plasma 5.1</a>
<br />
"<em>Plasma 5.1 Brings Back Many Popular Features.</em>"
<p />

<!-- 4.14.2 released -->
<strong>14th October 2014</strong> - <a href="announce-4.14.2.php">KDE Announces 4.14.2</a>
<br />
"<em>KDE Ships Applications and Platform 4.14.2.</em>"
<p />

<!-- KF 5.3.0 -->
<strong>07th October 2014</strong> - <a href="kde-frameworks-5.3.0.php">KDE Frameworks 5.3.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.3.0.</em>"
<p />

<!-- Plasma 5.0.95 -->
<strong>30th September 2014</strong> - <a href="plasma-5.0.95.php">Plasma 5.1 Beta</a>
<br />
"<em>KDE Ships Beta for Plasma 5's Second Release.</em>"
<p />

<!-- Plasma 5.0.2 -->
<strong>17th September 2014</strong> - <a href="plasma-5.0.2.php">Plasma 5.0.2</a>
<br />
"<em>KDE Ships Second Bugfix Release of Plasma 5.</em>"
<p />

<!-- 4.14.1 released -->
<strong>16th September 2014</strong> - <a href="announce-4.14.1.php">KDE Announces 4.14.1</a>
<br />
"<em>KDE Ships Applications and Platform 4.14.1.</em>"
<p />

<!-- KF 5.2.0 released -->
<strong>12th September 2014</strong> - <a href="kde-frameworks-5.2.0.php">KDE Frameworks 5.2.0 released</a>
<br />
"<em>KDE Ships Frameworks 5.2.0.</em>"
<p />

<!-- 4.14.0 released -->
<strong>20th August 2014</strong> - <a href="4.14">KDE Announces 4.14</a>
<br />
"<em>KDE Ships Applications and Platform 4.14.</em>"
<p />

<!-- Plasma 5.0.1 -->
<strong>12th August 2014</strong> - <a href="plasma-5.0.1.php">Plasma 5.0.1</a>
<br />
"<em>KDE Ships First Bugfix Release of Plasma 5.</em>"
<p />

<!-- KF 5.1 released -->
<strong>8th August 2014</strong> - <a href="kde-frameworks-5.1.php">Second Release of Frameworks 5</a>
<br />
"<em>KDE Ships Second Release of Frameworks 5.</em>"
<p />

<!-- 4.14 rc released -->
<strong>31st July 2014</strong> - <a href="announce-4.14-rc.php">KDE Announces 4.14 Release Candidate</a>
<br />
"<em>KDE Ships Release Candidate of Applications and Platform 4.14.</em>"
<p />

<!-- 4.14 beta3 released -->
<strong>24th July 2014</strong> - <a href="announce-4.14-beta3.php">KDE Announces 4.14 Beta 3</a>
<br />
"<em>KDE Ships Third Beta of Applications and Platform 4.14.</em>"
<p />

<!-- 4.14 beta2 released -->
<strong>17th July 2014</strong> - <a href="announce-4.14-beta2.php">KDE Announces 4.14 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Applications and Platform 4.14.</em>"
<p />

<!-- 4.13.3 released -->
<strong>15th July 2014</strong> - <a href="announce-4.13.3.php">KDE Announces 4.13.3</a>
<br />
"<em>KDE Ships Applications and Platform 4.13.3.</em>"
<p />

<!-- Plasma 5.0.0 -->
<strong>15th July 2014</strong> - <a href="plasma5.0">Plasma 5.0</a>
<br />
"<em>KDE Ships Plasma 5.0.</em>"
<p />

<!-- 4.14 beta1 released -->
<strong>10th July 2014</strong> - <a href="announce-4.14-beta1.php">KDE Announces 4.14 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Applications and Platform 4.14.</em>"
<p />

<!-- Plasma 5.0 RC -->
<strong>8th July 2014</strong> - <a href="plasma-5.0-rc.php">Plasma 5 Release Candidate</a>
<br />
"<em>KDE Ships Release Candidate for Plasma 5.</em>"
<p />

<!-- KF  5.0 released -->
<strong>7th July 2014</strong> - <a href="kde-frameworks-5.0.php">KDE Announces First Release of Frameworks 5</a>
<br />
"<em>KDE Ships First Release of Frameworks 5.</em>"
<p />

<!-- Plasma 5 Beta 2 released -->
<strong>10 June 2014</strong> - <a href="plasma5.0-beta2/">KDE Announces Plasma 5 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Plasma 5.</em>"
<p />

<!-- 4.13.2 released -->
<strong>10th June 2014</strong> - <a href="announce-4.13.2.php">KDE Announces 4.13.2</a>
<br />
"<em>KDE Ships Applications and Platform 4.13.2.</em>"
<p />

<!-- Plasma Next Beta released -->
<strong>14 May 2014</strong> - <a href="announce-plasma-next-beta1.php">KDE Announces Plasma Next Beta 1</a>
<br />
"<em>KDE Ships First Beta of Plasma Next.</em>"
<p />

<!-- 4.13.1 released -->
<strong>13th May 2014</strong> - <a href="announce-4.13.1.php">KDE Announces 4.13.1</a>
<br />
"<em>KDE Ships Applications and Platform 4.13.1.</em>"
<p />

<!-- 4.12.5 released -->
<strong>29th April 2014</strong> - <a href="announce-4.12.5.php">KDE Announces 4.12.5</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.5.</em>"
<p />

<!-- 4.13.0 -->
<strong>16th April 2014</strong> - <a href="4.13">KDE Announces 4.13</a>
<br />
"<em>KDE Ships Applications and Platform 4.13.</em>"
<p />

<!-- Plasma 2014.6 Alpha 1 released -->
<strong>2nd April 2014</strong> - <a href="announce-plasma-2014.6-alpha1.php">KDE Announces Plasma Next Alpha 1</a>
<br />
"<em>KDE Ships First Alpha of Plasma 2014.6.</em>"
<p />

<!-- KF5 B1 released -->
<strong>1st April 2014</strong> - <a href="announce-frameworks5-beta1.php">KDE Announces Frameworks 5 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Frameworks 5.</em>"
<p />

<!-- 4.12.4 released -->
<strong>1st April 2014</strong> - <a href="announce-4.12.4.php">KDE Announces 4.12.4</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.4.</em>"
<p />

<!-- 4.13 rc released -->
<strong>27th March 2014</strong> - <a href="announce-4.13-rc.php">KDE Announces 4.13 Release Candidate</a>
<br />
"<em>KDE Ships Release Candidate of Applications and Platform 4.13.</em>"
<p />

<!-- 4.13 beta3 released -->
<strong>20th March 2014</strong> - <a href="announce-4.13-beta3.php">KDE Announces 4.13 Beta 3</a>
<br />
"<em>KDE Ships Third Beta of Applications and Platform 4.13.</em>"
<p />

<!-- 4.13 beta2 released -->
<strong>13th March 2014</strong> - <a href="announce-4.13-beta2.php">KDE Announces 4.13 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Applications and Platform 4.13.</em>"
<p />

<!-- 4.13 beta1 released -->
<strong>6th March 2014</strong> - <a href="announce-4.13-beta1.php">KDE Announces 4.13 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Applications and Platform 4.13.</em>"
<p />

<!-- 4.12.3 released -->
<strong>4th March 2014</strong> - <a href="announce-4.12.3.php">KDE Announces 4.12.3</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.3.</em>"
<p />

<!-- Frameworks 5 alpha 2 released -->
<strong>3rd March 2014</strong> - <a href="announce-frameworks5-alpha2.php">KDE Announces Frameworks 5 Alpha 2</a>
<br />
"<em>KDE Ships Alpha 2 of Frameworks 5.</em>"
<p />

<!-- Frameworks 5 alpha 1 released -->
<strong>14th February 2014</strong> - <a href="announce-frameworks5-alpha.php">KDE Announces Frameworks 5 Alpha 1</a>
<br />
"<em>KDE Ships Alpha 1 of Frameworks 5.</em>"
<p />

<!-- 4.12.2 released -->
<strong>4th February 2014</strong> - <a href="announce-4.12.2.php">KDE Announces 4.12.2</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.2.</em>"
<p />

<!-- 4.12.1 released -->
<strong>12th January 2014</strong> - <a href="announce-4.12.1.php">KDE Announces 4.12.1</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.1.</em>"
<p />

<!-- 4.11.5 released -->
<strong>7th January 2014</strong> - <a href="announce-4.11.5.php">KDE Announces 4.11.5</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.5.</em>"
<p />

<!-- Frameworks 5 TP released -->
<strong>7th January 2014</strong> - <a href="frameworks5TP">KDE Announces Frameworks 5 Technology Preview</a>
<br />
"<em>KDE Ships Frameworks 5 Technology Preview.</em>"
<p />

<!-- Plasma 2 TP released -->
<strong>20th December 2013</strong> - <a href="plasma2tp">KDE Announces Plasma 2 Technology Preview</a>
<br />
"<em>KDE Ships Plasma 2 Technology Preview.</em>"
<p />

<!-- 4.12.0 released -->
<strong>18th December 2013</strong> - <a href="4.12">KDE Announces 4.12</a>
<br />
"<em>KDE Ships Applications and Platform 4.12.</em>"
<p />

<!-- 4.11.4 released -->
<strong>3th December 2013</strong> - <a href="announce-4.11.4.php">KDE Announces 4.11.4</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.4.</em>"
<p />

<!-- 4.12 rc released -->
<strong>28th November 2013</strong> - <a href="announce-4.12-rc.php">KDE Announces 4.12 Release Candidate</a>
<br />
"<em>KDE Ships Release Candidate of Applications and Platform 4.12.</em>"
<p />

<!-- 4.12 beta3 released -->
<strong>21st November 2013</strong> - <a href="announce-4.12-beta3.php">KDE Announces 4.12 Beta 3</a>
<br />
"<em>KDE Ships Third Beta of Applications and Platform 4.12.</em>"
<p />

<!-- 4.12 beta2 released -->
<strong>14th November 2013</strong> - <a href="announce-4.12-beta2.php">KDE Announces 4.12 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Applications and Platform 4.12.</em>"
<p />

<!-- 4.12 beta1 released -->
<strong>7th November 2013</strong> - <a href="announce-4.12-beta1.php">KDE Announces 4.12 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Applications and Platform 4.12.</em>"
<p />

<!-- 4.11.3 released -->
<strong>5th November 2013</strong> - <a href="announce-4.11.3.php">KDE Announces 4.11.3</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.3.</em>"
<p />

<!-- 4.11.2 released -->
<strong>1th October 2013</strong> - <a href="announce-4.11.2.php">KDE Announces 4.11.2</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.2.</em>"
<p />

<!-- 4.11.1 released -->
<strong>3rd September 2013</strong> - <a href="announce-4.11.1.php">KDE Announces 4.11.1</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.1.</em>"
<p />

<!-- 4.11.0 released -->
<strong>14th August 2013</strong> - <a href="4.11">KDE Announces 4.11</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.11.</em>"
<p />

<!-- 4.11 rc2 released -->
<strong>25th July 2013</strong> - <a href="announce-4.11-rc2.php">KDE Announces 4.11 RC 2</a>
<br />
"<em>KDE Ships Second Release Candidate of Plasma Workspaces, Applications and Platform 4.11.</em>"
<p />

<!-- 4.11 rc1 released -->
<strong>16th July 2013</strong> - <a href="announce-4.11-rc1.php">KDE Announces 4.11 RC 1</a>
<br />
"<em>KDE Ships First Release Candidate of Plasma Workspaces, Applications and Platform 4.11.</em>"
<p />

<!-- 4.10.5 released -->
<strong>2th July 2013</strong> - <a href="announce-4.10.5.php">KDE Announces 4.10.5</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.5.</em>"
<p />

<!-- 4.11 beta2 released -->
<strong>27th June 2013</strong> - <a href="announce-4.11-beta2.php">KDE Announces 4.11 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Plasma Workspaces, Applications and Platform 4.11.</em>"
<p />

<!-- 4.11 beta1 released -->
<strong>13th June 2013</strong> - <a href="announce-4.11-beta1.php">KDE Announces 4.11 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Plasma Workspaces, Applications and Platform 4.11.</em>"
<p />

<!-- 4.10.4 released -->
<strong>4th June 2013</strong> - <a href="announce-4.10.4.php">KDE Announces 4.10.4</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.4.</em>"
<p />

<!-- 4.10.3 released -->
<strong>7th May 2013</strong> - <a href="announce-4.10.3.php">KDE Announces 4.10.3</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.3.</em>"
<p />

<!-- 4.10.2 released -->
<strong>2th April 2013</strong> - <a href="announce-4.10.2.php">KDE Announces 4.10.2</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.2.</em>"
<p />

<!-- 4.10.1 released -->
<strong>5th March 2013</strong> - <a href="announce-4.10.1.php">KDE Announces 4.10.1</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.1.</em>"
<p />

<!-- 4.10.0 released -->
<strong>6th February 2013</strong> - <a href="4.10/">KDE Announces 4.10</a>
<br />
"<em>KDE Ships Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.10 RC3 released -->
<strong>18th January 2013</strong> - <a href="announce-4.10-rc3.php">KDE Announces 4.10 RC3</a>
<br />
"<em>KDE Ships Third Release Candidate of Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.10 RC2 released -->
<strong>4th January 2013</strong> - <a href="announce-4.10-rc2.php">KDE Announces 4.10 RC2</a>
<br />
"<em>KDE Ships Second Release Candidate of Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.9.5 released -->
<strong>2nd January 2013</strong> - <a href="announce-4.9.5.php">KDE Ships January Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.9.5 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.10 RC1 released -->
<strong>19th December 2012</strong> - <a href="announce-4.10-rc1.php">KDE Announces 4.10 RC1</a>
<br />
"<em>KDE Ships First Release Candidate of Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.9.4 released -->
<strong>5th December 2012</strong> - <a href="announce-4.9.4.php">KDE Ships December Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.9.4 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.10 beta2 released -->
<strong>4th December 2012</strong> - <a href="announce-4.10-beta2.php">KDE Announces 4.10 Beta 2</a>
<br />
"<em>KDE Ships Second Beta of Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.10 beta1 released -->
<strong>21st November 2012</strong> - <a href="announce-4.10-beta1.php">KDE Announces 4.10 Beta 1</a>
<br />
"<em>KDE Ships First Beta of Plasma Workspaces, Applications and Platform 4.10.</em>"
<p />

<!-- 4.9.3 released -->
<strong>6th November 2012</strong> - <a href="announce-4.9.3.php">KDE Ships November Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.9.3 Workspaces, Applications and Platform.</em>"
<p />

<!-- plasma active two released -->
<strong>15th October 2012</strong> - <a href="plasma-active-three/">Plasma Active 3 Improves Performance, Brings New Apps</a>
<br />
"<em>KDE Releases Third Version of Cross-Device UX.</em>"
<p />

<!-- 4.9.2 released -->
<strong>2nd October 2012</strong> - <a href="announce-4.9.2.php">KDE Ships October Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.9.2 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.9.1 released -->
<strong>4th September 2012</strong> - <a href="announce-4.9.1.php">KDE Ships September Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.9.1 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.5 released -->
<strong>6th August 2012</strong> - <a href="announce-4.8.5.php">KDE Ships August Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.8.5 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.9 released -->
<strong>1st August 2012</strong> - <a href="4.9/">KDE Announces Plasma Workspaces, Applications and Platform 4.9</a>
<br />
"<em>KDE is delighted to announce its latest set of releases, providing major updates to
KDE Plasma Workspaces, KDE Applications, and the KDE Platform.
Version 4.9.0 provides many new features, along with improved stability and performance.</em>"
<p />

<!-- 4.9 RC2 released -->
<strong>11th July 2012</strong> - <a href="announce-4.9-rc2.php">KDE Announces 4.9 Release Candidate 2</a>
<br />
"<em>KDE Provides Candidate Release 2 for 4.9 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.9 RC1 released -->
<strong>26th June 2012</strong> - <a href="announce-4.9-rc1.php">KDE Announces 4.9 Release Candidate 1</a>
<br />
"<em>KDE Provides Candidate Release 1 for 4.9 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.9 Beta2 released -->
<strong>13th June 2012</strong> - <a href="announce-4.9-beta2.php">KDE Announces 4.9 Beta2</a>
<br />
"<em>KDE Provides Test Release for 4.9 Beta2 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.4 released -->
<strong>8th June 2012</strong> - <a href="announce-4.8.4.php">KDE Ships June Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.8.4 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.9 Beta1 released -->
<strong>4th June 2012</strong> - <a href="announce-4.9-beta1.php">KDE Announces 4.9 Beta1 and Testing Initiative</a>
<br />
"<em>KDE Provides Test Release for 4.9 Beta1 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.3 released -->
<strong>4th May 2012</strong> - <a href="announce-4.8.3.php">KDE Ships May Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.8.3 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.2 released -->
<strong>4th April 2012</strong> - <a href="announce-4.8.2.php">KDE Ships April Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.8.2 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.1 released -->
<strong>7th March 2012</strong> - <a href="announce-4.8.1.php">KDE Ships March Updates to Plasma Workspaces, Applications and Platform</a>
<br />
"<em>KDE Ships 4.8.1 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8.0 released -->
<strong>25th January 2012</strong> - <a href="4.8/">KDE Plasma Workspaces, Applications and Platform 4.8 Improve User Experience</a>
<br />
"<em>KDE Ships 4.8.0 Workspaces, Applications and Platform.</em>"
<p />


<!-- 4.8 rc2 released -->
<strong>4th January 2012</strong> - <a href="announce-4.8-rc2.php">KDE Makes Second 4.8 Release Candidate Available</a>
<br />
"<em>KDE Ships 4.8 RC2 Workspaces, Applications and Platform.</em>"
<p />


<!-- 4.8 rc1 released -->
<strong>22nd December 2011</strong> - <a href="announce-4.8-rc1.php">KDE Makes First 4.8 Release Candidate Available, Adds Secret Service</a>
<br />
"<em>KDE Ships 4.8 RC1 Workspaces, Applications and Platform.</em>"
<p />

<!-- plasma active two released -->
<strong>14th December 2011</strong> - <a href="plasma-active-two/">Plasma Active Two Boosts Performance, New Features</a>
<br />
"<em>KDE Releases Second Version of Cross-Device UX.</em>"
<p />

<!-- 4.8 beta2 released -->
<strong>7th December 2011</strong> - <a href="announce-4.8-beta2.php">KDE Makes 4.8 Beta2 Available for Testing</a>
<br />
"<em>KDE Ships 4.8 Beta2 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.7.4 released -->
<strong>7th December 2011</strong> - <a href="announce-4.7.4.php">KDE's December Updates Conclude 4.7 Series</a>
<br />
"<em>KDE Ships 4.7.4 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.8 beta1 released -->
<strong>2nd November 2011</strong> - <a href="announce-4.8-beta1.php">KDE Makes 4.8 Beta1 Available for Testing</a>
<br />
"<em>KDE Ships 4.8 Beta1 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.7.3 released -->
<strong>2nd November 2011</strong> - <a href="announce-4.7.3.php">KDE's November Updates Improve Nepomuk Stability</a>
<br />
"<em>KDE Ships 4.7.3 Workspaces, Applications and Platform.</em>"
<p />

<!-- plasma active one released -->
<strong>9th October 2011</strong> - <a href="plasma-active-one/">Plasma Active One Arrives</a>
<br />
"<em>KDE Releases First Version of Cross-Device UX.</em>"
<p />

<!-- 4.7.2 released -->
<strong>5th October 2011</strong> - <a href="announce-4.7.2.php">KDE's October Updates Improve Kontact Performance</a>
<br />
"<em>KDE Ships 4.7.2 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.7.1 released -->
<strong>7th September 2011</strong> - <a href="announce-4.7.1.php">KDE Ships September Updates</a>
<br />
"<em>KDE Ships 4.7.1 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.7.0 released -->
<strong>27th July 2011</strong> - <a href="4.7/">New KDE Applications, Workspaces and Development Platform Releases Bring New Features, Improve Stability</a>
<br />
"<em>KDE Ships 4.7.0 Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.7 RC2 released -->
<strong>11th July 2011</strong> - <a href="announce-4.7-rc2.php">KDE Shows Second Release Candidate of Summer Releases</a>
<br />
"<em>KDE Ships Second 4.7 Release Candidates of this summer's release of the Workspaces, Applications and Platform.</em>"
<p />

<!-- 4.6.5 released -->
<strong>7th July 2011</strong> - <a href="announce-4.6.5.php">KDE Ships July Updates</a>
<br />
"<em>KDE Updates Development Frameworks, Applications and Plasma Workspaces to version 4.6.5</em>"
<p />

<!-- 4.7 RC1 released -->
<strong>25th June 2011</strong> - <a href="announce-4.7-rc1.php">KDE Shows First Release Candidate of Summer Releases</a>
<br />
"<em>KDE Ships Second 4.7 Release Candidates of this summer's release of the Workspaces, Applications and Platform.</em>"
<p />

<!-- KDE 4.6.4 released -->
<strong>10th June 2011</strong> - <a href="announce-4.6.4.php">New Kontact Suite Brings Next-Gen Groupware to Desktop and Mobile</a>
<br />
"<em>KDE is proud to announce the release of the next generation Kontact Suite, based on the Akonadi framework. </em>"
<p />

<!-- KDE 4.6.3 released -->
<strong>6th May 2011</strong> - <a href="announce-4.6.3.php">KDE Ships May Updates</a>
<br />
"<em>KDE Updates Development Frameworks, Applications and Plasma Workspaces to version 4.6.3</em>"
<p />

<!-- KDE 4.6.2 released -->
<strong>6th April 2011</strong> - <a href="announce-4.6.2.php">KDE Ships April Updates</a>
<br />
"<em>KDE Updates Development Frameworks, Applications and Plasma Workspaces to version 4.6.2</em>"
<p />

<!-- KDE 4.6.1 released -->
<strong>4th March 2011</strong> - <a href="announce-4.6.1.php">KDE Ships March Updates</a>
<br />
"<em>KDE Updates Development Frameworks, Applications and Plasma Workspaces to version 4.6.1</em>"
<p />

<!-- KDE 4.6.0 released -->
<strong>26th January 2011</strong> - <a href="4.6">KDE SC 4.6.0 Released</a>
<br />
"<em>KDE Puts You In Control with New Workspaces, Applications and Platform</em>"
<p />

<!-- KDE 4.6 RC2 released -->
<strong>7th January 2011</strong> - <a href="announce-4.5.5.php">KDE SC 4.5.5 Released</a>
<br />
"<em>KDE Releases Development Frameworks, Applications and Plasma Workspaces 4.5.5</em>"
<p />

<!-- KDE 4.6 RC2 released -->
<strong>5th January 2011</strong> - <a href="announce-4.6-rc2.php">KDE SC 4.6 RC2 Released</a>
<br />
"<em>KDE Ships Second Release Candidate of KDE SC 4.6 Series</em>"
<p />

<!-- KDE 4.6 RC1 released -->
<strong>23rd December 2010</strong> - <a href="announce-4.6-rc1.php">KDE SC 4.6 RC1 Released</a>
<br />
"<em>KDE Ships First Release Candidate of KDE SC 4.6 Series</em>"
<p />

<!-- KDE 4.6 Beta2 released -->
<strong>8th December 2010</strong> - <a href="announce-4.6-beta2.php">KDE SC 4.6 Beta2 Released</a>
<br />
"<em>KDE Ships Second Test Release of KDE SC 4.6 Series</em>"
<p />

<!-- 4.5.4 released -->
<strong>2nd November 2010</strong> - <a href="announce-4.5.4.php/">December Updates Stabilize KDE 4.5 Further</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.4</em>"
<p />

<!-- 4.6 beta1 released -->
<strong>25th November 2010</strong> - <a href="announce-4.6-beta1.php/">4.6 Beta1 Brings Improved Search, Activities and Mobile Device Support
</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.6 Beta1</em>"
<p />

<!-- 4.5.3 released -->
<strong>3rd November 2010</strong> - <a href="announce-4.5.3.php/">KDE Ships November Updates</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.3</em>"
<p />

<!-- 4.5.2 released -->
<strong>5th October 2010</strong> - <a href="announce-4.5.2.php/">KDE Ships October Updates</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.2</em>"
<p />

<!-- KDE 4.5.1 released -->
<strong>31st August 2010</strong> - <a href="announce-4.5.1.php/">KDE SC 4.5.1 Released</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.1</em>"
<p />

<!-- KDE 4.5.0 released -->
<strong>10th August 2010</strong> - <a href="4.5/">KDE SC 4.5.0 Released</a>
<br />
"<em>KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.0.</em>"
<p />

<!-- KDE 4.4.5 released -->
<strong>30th June 2010</strong> - <a href="announce-4.4.5.php">KDE SC 4.4.5 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.5.</em>"
<p />

<!--KDE SC 4.5 RC2 released -->
<strong>26th July 2010</strong> - <a href="announce-4.5-rc3.php">KDE SC 4.5 RC3 Released</a>
<br />
"<em>KDE Ships Last Release Candidate of KDE SC 4.5 Series</em>"
<p />

<!--KDE SC 4.5 RC2 released -->
<strong>8th July 2010</strong> - <a href="announce-4.5-rc2.php">KDE SC 4.5 RC2 Released</a>
<br />
"<em>KDE Ships Second Release Candidate of KDE SC 4.5 Series</em>"
<p />

<!--KDE SC 4.5 RC1 released -->
<strong>27th June 2010</strong> - <a href="announce-4.5-rc1.php">KDE SC 4.5 RC1 Released</a>
<br />
"<em>KDE Ships First Release Candidate of KDE SC 4.5 Series</em>"
<p />

<!-- KDE 4.5 Beta2 released -->
<strong>9th June 2010</strong> - <a href="announce-4.5-beta2.php">KDE SC 4.5 Beta2 Released</a>
<br />
"<em>KDE Ships Second Test Release of KDE SC 4.5 Series</em>"
<p />

<!-- KDE 4.4.4 released -->
<strong>1st June 2010</strong> - <a href="announce-4.4.4.php">KDE SC 4.4.4 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.4.</em>"
<p />

<!-- KDE 4.5 Beta1 released -->
<strong>26th May 2010</strong> - <a href="announce-4.5-beta1.php">KDE SC 4.5 Beta1 Released</a>
<br />
"<em>KDE Ships First Test Release of KDE SC 4.5 Series</em>"
<p />

<!-- KDE 4.4.3 released -->
<strong>5th May 2010</strong> - <a href="announce-4.4.3.php">KDE SC 4.4.3 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.3.</em>"
<p />

<!-- KDE 4.4.2 released -->
<strong>30th March 2010</strong> - <a href="announce-4.4.2.php">KDE SC 4.4.2 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.2.</em>"
<p />

<!-- KDE 4.4.1 released -->
<strong>2nd March 2010</strong> - <a href="announce-4.4.1.php">KDE SC 4.4.1 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.1.</em>"
<p />

<!-- KDE 4.4.0 released -->
<strong>9th February 2010</strong> - <a href="4.4/">KDE SC 4.4.0 Released</a>
<br />
"<em>KDE Ships New KDE SC 4.4.0.</em>"
<p />

<!-- KDE 4.4 RC3 released -->
<strong>1st February 2010</strong> - <a href="announce-4.4-rc3.php">KDE SC 4.4 RC3 Released</a>
<br />
"<em>KDE Community Ships Third Release Candidate for KDE SC 4.4.0.</em>"
<p />

<!-- KDE 4.3.5 released -->
<strong>26th January 2010</strong> - <a href="announce-4.3.5.php">KDE SC 4.3.5 Released</a>
<br />
"<em>KDE Community Ships Fifth Maintainance Update for KDE SC 4.3 Series.</em>"
<p />

<!-- KDE 4.4 RC2 released -->
<strong>25th January 2010</strong> - <a href="announce-4.4-rc2.php">KDE SC 4.4 RC2 Released</a>
<br />
"<em>KDE Community Ships Second Release Candidate for KDE SC 4.4.0.</em>"
<p />

<!-- KDE 4.4 RC1 released -->
<strong>8th January 2010</strong> - <a href="announce-4.4-rc1.php">KDE SC 4.4 RC1 Released</a>
<br />
"<em>KDE Community Ships Release Candidate for KDE SC 4.4.0.</em>"
<p />

<!-- KDE 4.4 Beta2 released -->
<strong>21st December 2009</strong> - <a href="announce-4.4-beta2.php">KDE SC 4.4 Beta 2 Released</a>
<br />
"<em>KDE Community Ships Second Preview for KDE SC 4.4 Series.</em>"
<p />

<!-- KDE 4.4 Beta1 released -->
<strong>4th December 2009</strong> - <a href="announce-4.4-beta1.php">KDE SC 4.4 Beta 1 Released</a>
<br />
"<em>KDE Community Ships First Preview for KDE SC 4.4 Series.</em>"
<p />

<!-- KDE 4.3.4 released -->
<strong>1st December 2009</strong> - <a href="announce-4.3.4.php">KDE SC 4.3.4 Released</a>
<br />
"<em>KDE Community Ships Fourth Update to KDE SC 4.3 Series.</em>"
<p />

<!-- KDE 4.3.3 released -->
<strong>3rd November 2009</strong> - <a href="announce-4.3.3.php">KDE 4.3.3 Released</a>
<br />
"<em>KDE Community Ships Third Update to KDE 4.3 Series.</em>"
<p />

<!-- KDE 4.3.2 released -->
<strong>6th October 2009</strong> - <a href="announce-4.3.2.php">KDE 4.3.2 Released</a>
<br />
"<em>KDE Community Ships Second Update to KDE 4.3 Series.</em>"
<p />

<!-- KDE 4.3.1 released -->
<strong>1st September 2009</strong> - <a href="announce-4.3.1.php">KDE 4.3.1 Released</a>
<br />
"<em>KDE Community Ships First Update to KDE 4.3 Series.</em>"
<p />

<!-- KDE 4.3.0 released -->
<strong>4th August 2009</strong> - <a href="4.3/">KDE 4.3.0 Released</a>
<br />
"<em>KDE Community Ships Third Update to the KDE 4 Desktop, Applications and Platform.</em>"
<p />

<!-- KDE 4.3 RC3 released -->
<strong>22nd July 2009</strong> - <a href="announce-4.3-rc3.php">KDE 4.3 RC3 Released</a>
<br />
"<em>KDE Community Ships Third Release Candidate of New Desktop, Applications and Platform.</em>"
<p />

<!-- KDE 4.3 RC2 released -->
<strong>9th July 2009</strong> - <a href="announce-4.3-rc2.php">KDE 4.3 RC2 Released</a>
<br />
"<em>KDE Community Ships Second Release Candidate of New Desktop, Applications and Platform.</em>"
<p />

<!-- KDE 4.3 RC1 released -->
<strong>1st July 2009</strong> - <a href="announce-4.3-rc1.php">KDE 4.3 RC1 Released</a>
<br />
"<em>KDE Community Ships Release Candidate of New Desktop, Applications and Platform.</em>"
<p />

<!-- KDE 4.3 Beta2 released -->
<strong>10th June 2009</strong> - <a href="announce-4.3-beta2.php">KDE 4.3 Beta 2 Released</a>
<br />
"<em>KDE Community Ships Second Testing Version of New Desktop, Applications and Platform.</em>"
<p />

<!-- KDE 4.2.4 released -->
<strong>3rd June 2009</strong> - <a href="announce-4.2.4.php">KDE 4.2.4 Released</a>
<br />
"<em>KDE Community Ships Fourth Update to KDE 4.2 Series.</em>"
<p />

<!-- KDE 4.3 Beta1 released -->
<strong>13th May 2009</strong> - <a href="announce-4.3-beta1.php">KDE 4.3 Beta1 Released</a>
<br />
"<em>KDE Community Ships First Testing Version of New Desktop.</em>"
<p />

<!-- KDE 4.2.3 released -->
<strong>6th May 2009</strong> - <a href="announce-4.2.3.php">KDE 4.2.3 Released</a>
<br />
"<em>KDE Community Ships Second Update To Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.2.2 released -->
<strong>2nd April 2009</strong> - <a href="announce-4.2.2.php">KDE 4.2.2 Released</a>
<br />
"<em>KDE Community Ships Second Update To Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.2.1 released -->
<strong>4th March 2009</strong> - <a href="announce-4.2.1.php">KDE 4.2.1 Released</a>
<br />
"<em>KDE Community Ships First Update To Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.2.0 released -->
<strong>27th January 2009</strong> - <a href="4.2/">KDE 4.2.0 Released</a>
<br />
"<em>KDE Community Ships Second Major Update To Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1.96 released -->
<strong>13th January 2009</strong> - <a href="announce-4.2-rc.php">KDE 4.1.96 (KDE 4.2 RC) Released</a>
<br />
"<em>KDE Community Ships Release Candidate for Next-Gen Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1.4 released -->
<strong>13th January 2009</strong> - <a href="announce-4.1.4.php">KDE 4.1.4 Released</a>
<br />
"<em>KDE Community Ships Fourth Update for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1.85 released -->
<strong>16th December 2008</strong> - <a href="announce-4.2-beta2.php">KDE 4.1.85 (KDE 4.2 Beta2) Released</a>
<br />
"<em>KDE Community Ships Second Beta Version for Next-Gen Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1.80 released -->
<strong>26th November 2008</strong> - <a href="announce-4.2-beta1.php">KDE 4.1.80 (KDE 4.2 Beta1) Released</a>
<br />
"<em>KDE Community Ships First Beta Version for Next-Gen Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1.3 released -->
<strong>5th November 2008</strong> - <a href="announce-4.1.3.php">KDE 4.1.3 Released</a>
<br />
"<em>KDE Community Ships Third Maintenance Update for KDE 4.1.</em>"
<p />

<!-- KDE Forum launched -->
<strong>12th October 2008</strong> - <a href="forum.php">KDE Forum Launched</a>
<br />
"<em>KDE Launches Web-based Bulletin Board.</em>"
<p />

<!-- KDE 4.1.2 released -->
<strong>3rd October 2008</strong> - <a href="announce-4.1.2.php">KDE 4.1.2 Released</a>
<br />
"<em>KDE Community Ships Second Maintenance Update for KDE 4.1.</em>"
<p />

<!-- KDE 4.1.1 released -->
<strong>3rd September 2008</strong> - <a href="announce-4.1.1.php">KDE 4.1.1 Released</a>
<br />
"<em>KDE Community Ships First Maintenance Update for KDE 4.1.</em>"
<p />

<!-- KDE 3.5.10 released -->
<strong>26th August 2008</strong> - <a href="announce-3.5.10.php">KDE 3.5.10 Released</a>
<br />
"<em>KDE Community Ships Tenth Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1 released -->
<strong>29th July 2008</strong> - <a href="4.1/">KDE 4.1 Released</a>
<br />
"<em>KDE Community Ships Major Update to Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.1 rc1 released -->
<strong>15th July 2008</strong> - <a href="announce-4.1-rc1.php">KDE 4.1 Release Candidate Released</a>
<br />
"<em>KDE Community Ships Release Candidate of Major Update to Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.1 beta 2 released -->
<strong>24th June 2008</strong> - <a href="announce-4.1-beta2.php">KDE 4.1 Beta 2 Released</a>
<br />
"<em>KDE Community Ships Second Beta of Major Update to Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.0.5 released -->
<strong>4th June 2008</strong> - <a href="announce-4.0.5.php">KDE 4.0.5 Released</a>
<br />
"<em>KDE Community Ships Fourth Maintenance Update for Fifth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1 beta 1 released -->
<strong>27th May 2008</strong> - <a href="announce-4.1-beta1.php">KDE 4.1 Beta 1 Released</a>
<br />
"<em>KDE Community Ships First Beta of Major Update to Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.0.4 released -->
<strong>7th May 2008</strong> - <a href="announce-4.0.4.php">KDE 4.0.4 Released</a>
<br />
"<em>KDE Community Ships Fourth Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.1 Alpha 1 released -->
<strong>29th April 2008</strong> - <a href="announce-4.1-alpha1.php">KDE 4.1 Alpha1 Released</a>
<br />
"<em>KDE Community Ships First Preview Version for Next-Gen Leading Free
Software Desktop.</em>"
<p />

<!-- KDE and Wikimedia collaborate -->
<strong>4th April 2008</strong> - <a href="kde-and-wikimedia-collaborate.php">KDE and Wikimedia Collaborate</a>
<br />
"<em>KDE e.V and Wikimedia Deutschland have opened a shared office in Frankfurt, Germany.</em>"
<p />

<!-- KDE 4.0.3 released -->
<strong>2nd April 2008</strong> - <a href="announce-4.0.3.php">KDE 4.0.3 Released</a>
<br />
"<em>KDE Community Ships Third Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.0.2 released -->
<strong>5th March 2008</strong> - <a href="announce-4.0.2.php">KDE 4.0.2 Released</a>
<br />
"<em>KDE Community Ships Second Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 3.5.9 released -->
<strong>19th February 2008</strong> - <a href="announce-3.5.9.php">KDE 3.5.9 Released</a>
<br />
"<em>KDE Community Ships Ninth Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.0.1 released -->
<strong>5th February 2008</strong> - <a href="announce-4.0.1.php">KDE 4.0.1 Released</a>
<br />
"<em>KDE Community Ships First Maintenance Update for Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.0 released -->
<strong>11th January 2008</strong> - <a href="4.0/">KDE 4.0 Released</a>
<br />
"<em>KDE Community Ships Fourth Major Version for Leading Free
Software Desktop.</em>"
<p />

<!-- KDE 4.0-rc2 released -->
<strong>11th December 2007</strong> - <a href="announce-4.0-rc2.php">KDE 4.0 Release Candidate 2 Released</a>
<br />
"<em>KDE Project Ships Second Release Candidate for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 4.0-rc1 released -->
<strong>20th November 2007</strong> - <a href="announce-4.0-rc1.php">KDE 4.0 Release Candidate 1 Released</a>
<br />
"<em>KDE Project Ships First Release Candidate for Leading Free
   Software Desktop.</em>"
<p />


<!-- KDE 4.0-beta4 released -->
<strong>30th October 2007</strong> - <a href="announce-4.0-beta4.php">KDE 4.0 Beta 4 Released</a>
<br />
"<em>KDE Project Ships Fourth Beta Release for Leading Free
   Software Desktop.</em>"
<p />


<!-- KDE 4.0 Development Platform Release Candidate 1 (whattaname!) released -->
<strong>30th October 2007</strong> - <a href="announce-4.0-platform-rc1.php">KDE 4.0
Development Platform Release Candidate 1</a>
<br />
"<em>KDE Project Ships Release Candidate for the Development Platform.</em>"
<p />


<!-- KDE 4.0-beta3 released -->
<strong>17th October 2007</strong> - <a href="announce-4.0-beta3.php">KDE 4.0 Beta 3 Released</a>
<br />
"<em>KDE Project Ships Third Beta Release for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 3.5.8 released -->
<strong>16th October 2007</strong> - <a href="announce-3.5.8.php">KDE 3.5.8 Released</a>
<br />
"<em>Project Ships Eighth Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.0-beta2 released -->
<strong>6th September 2007</strong> - <a href="announce-4.0-beta2.php">KDE 4.0 Beta 2 Released</a>
<br />
"<em>KDE Project Ships Second Beta Release for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 4.0-beta1 released -->
<strong>2nd August 2007</strong> - <a href="announce-4.0-beta1.php">KDE 4.0 Beta 1 Released</a>
<br />
"<em>KDE Project Ships First Beta Release for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 4.0-alpha2 released -->
<strong>4th July 2007</strong> - <a href="announce-4.0-alpha2.php">KDE 4.0-alpha2 Released</a>
<br />
"<em>KDE Project Ships Second Alpha Release for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 3.5.7 released -->
<strong>22nd May 2007</strong> - <a href="announce-3.5.7.php">KDE 3.5.7 Released</a>
<br />
"<em>Project Ships Seventh Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 4.0-alpha1 released -->
<strong>11th May 2007</strong> - <a href="announce-4.0-alpha1.php">KDE 4.0 Alpha 1 Released</a>
<br />
"<em>KDE Project Ships First Alpha Release for Leading Free
   Software Desktop.</em>"
<p />

<!-- KDE 3.5.6 released -->
<strong>25th January 2007</strong> - <a href="announce-3.5.6.php">KDE 3.5.6 Released</a>
<br />
"<em>Project Ships Sixth Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 3.5.5 released -->
<strong>11th October 2006</strong> - <a href="announce-3.5.5.php">KDE 3.5.5 Released</a>
<br />
"<em>Project Ships Fifth Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- aKademy 2006 kicked off -->
<strong>23 September 2006</strong> - <a href="akademy2006-kicked-off.php">aKademy 2006 kicked off</a>
<br />
"<em>Fourth KDE World Summit started at the Trinity College in Dublin / Ireland.</em>"
<p />

<!-- aKademy 2006 schedule finalized -->
<strong>18 September 2006</strong> - <a href="akademy2006-schedule-finalized.php">aKademy 2006 schedule finalized</a>
<br />
"<em>Schedule for aKademy 2006 finalized.</em>"
<p />

<!-- KDE 3.5.4 released -->
<strong>2nd August 2006</strong> - <a href="announce-3.5.4.php">KDE 3.5.4 Released</a>
<br />
"<em>Project Ships Fourth Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 3.5.3 released -->
<strong>31 May 2006</strong> - <a href="announce-3.5.3.php">KDE 3.5.3 Released</a>
<br />
"<em>Project Ships Third Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- FSFE Membership -->
<strong>9 May 2006</strong> - <a href="fsfe-associate-member.php">KDE e.V. Becomes Associate Member of FSFE</a>
<br />
"<em>KDE e.V. and the Free Software Foundation Europe are proud to announce their associate status</em>"
<p />

<!-- KDE 3.5.2 released -->
<strong>28 March 2006</strong> - <a href="announce-3.5.2.php">KDE 3.5.2 Released</a>
<br />
"<em>Project Ships Second Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 3.5.1 released -->
<strong>31 January 2006</strong> - <a href="announce-3.5.1.php">KDE 3.5.1 Released</a>
<br />
"<em>Project Ships Translation and Service Release for Leading Free Software Desktop.</em>"
<p />

<!-- KDE 3.5 released -->
<strong>29 November 2005</strong> - <a href="announce-3.5.php">KDE 3.5 Released</a>
<br />
"<em>Many new features and refinements make KDE the complete free desktop environment.</em>"
<p />

<!-- KDE 3.4.3 released -->
<strong>13 October 2005</strong> - <a href="announce-3.4.3.php">KDE 3.4.3 Released</a>
<br />
"<em>KDE Project Ships Third Translation and Service Release for Free Software Desktop</em>"
<p />

<!-- KDE 3.4.2 released -->
<strong>28 July 2005</strong> - <a href="announce-3.4.2.php">KDE 3.4.2 Released</a>
<br />
"<em>KDE Project Ships Second Translation and Service Release for Free Software Desktop</em>"
<p />

<!-- KDE 3.4.1 released -->
<strong>31 May 2005</strong> - <a href="announce-3.4.1.php">KDE 3.4.1 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Free Software Desktop</em>"
<p />

<!-- KDE 3.4 released -->
<strong>16 March 2005</strong> - <a href="announce-3.4.php">KDE 3.4 Released</a>
<br />
"<em>KDE Project Ships New Major Release Of Leading Open Source Desktop Environment</em>"
<p />

<!-- KDE 3.3.2 released -->
<strong>8 December 2004</strong> - <a href="announce-3.3.2.php">KDE 3.3.2 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.3.1 released -->
<strong>12 October 2004</strong> - <a href="announce-3.3.1.php">KDE 3.3.1 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.3 released -->
<strong>19 August 2004</strong> - <a href="announce-3.3.php">KDE 3.3 Released</a>
<br />
"<em>KDE Project Ships New Major Release Of Leading Open Source Desktop Environment</em>"
<p />

<!-- KDE 3.2.3 released -->
<strong>09 June 2004</strong> - <a href="announce-3.2.3.php">KDE 3.2.3 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.2.2 released -->
<strong>19 April 2004</strong> - <a href="announce-3.2.2.php">KDE 3.2.2 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.2.1 released -->
<strong>09 March 2004</strong> - <a href="announce-3.2.1.php">KDE 3.2.1 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.2 released -->
<strong>03 February 2004</strong> - <a href="announce-3.2.php">KDE 3.2 Released</a>
<br />
"<em>KDE Project Ships New Major Release Of Leading Open Source Desktop Environment</em>"
<p />

<!-- KDE 3.1.5 released -->
<strong>14 January 2004</strong> - <a href="announce-3.1.5.php">KDE 3.1.5 Released</a>
<br />
"<em>KDE Project Ships Fifth and Final Translation and Service Release for the KDE 3.1 release</em>"
<p />

<!-- KDE 3.1.93 released -->
<strong>03 November 2003</strong> - <a href="announce-3.2beta1.php">KDE 3.2.0 Beta1 Released</a>
<br />
"<em>The first beta of the upcoming KDE 3.2 release.</em>"
<p />

<!-- KDE 3.1.4 released -->
<strong>16 September 2003</strong> - <a href="announce-3.1.4.php">KDE 3.1.4 Released</a>
<br />
"<em>KDE Project Ships Fourth Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.1.3 released -->
<strong>29 July 2003</strong> - <a href="announce-3.1.3.php">KDE 3.1.3 Released</a>
<br />
"<em>KDE Project Ships Third Translation and Service Release for Leading Open Source Desktop</em>"
<p />

<!-- KDE 3.1.2 released -->
<strong>19 May 2003</strong> - <a href="announce-3.1.2.php">KDE 3.1.2 Released</a>
<br />
"<em>KDE Project Ships Second Translation and Service Release for Leading Open Source Desktop</em>"
<p />


<!-- KDE 3.1.1 released -->
<strong>20 March 2003</strong> - <a href="announce-3.1.1.php">KDE 3.1.1 Released</a>
<br />
"<em>KDE Project Ships Translation and Service Release for Leading Open Source Desktop</em>"
<p />


<!-- CeBit 2003 annoucement-->
<strong>11 March 2003</strong> - <a href="cebit2003.php">See KDE at CeBit 2003</a>
<br />
"<em>The KDE Project will present itself at CeBIT, the world's largest computer trade show, taking place in Hannover from March 12th to March 19th.</em>"
<p />

<!-- KDE 3.1.0 released -->
<strong>28 January 2003</strong> - <a href="announce-3.1.php">KDE 3.1 Released</a>
<br />
"<em>KDE Project Ships Major Feature Upgrade of Third- Generation GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling Free and Open Desktop Solution</em>"
<p />

<!-- KDE 3.0.5a released -->
<strong>21 December 2002</strong> - <a href="announce-3.0.5a.php">KDE 3.0.5a Released</a>
<br />
"<em>KDE 3.0.5a is a security and maintenance update over KDE 3.0.5.</em>"
<p />

<!-- KOffice 1.2.1 released -->
<strong>12 December 2002</strong> - <a href="http://www.koffice.org/announcements/announce-1.2.1.phtml">KOffice 1.2.1 Released</a>
<br />
"<em>KOffice 1.2.1 is a stability release, with the principal improvements over KOffice 1.2 occurring in the spreadsheet program (KSpread).</em>"
<p />

<!-- KDE 3.0.5 released -->
<strong>18 November 2002</strong> - <a href="announce-3.0.5.php">KDE 3.0.5 Released</a>
<br />
"<em>KDE 3.0.5 is a security update over KDE 3.0.4.</em>"
<p />

<!-- KDE 3.0.4 released -->
<strong>9 October 2002</strong> - <a href="announce-3.0.4.php">KDE 3.0.4 Released</a>
<br />
"<em>KDE 3.0.4 is a bugfix and security update over KDE 3.0.3.</em>"
<p />

<!-- KDE 3.1 Beta2 released -->
<strong>2 October 2002</strong> - <a href="announce-3.1beta2.php">KDE 3.1 Beta2 Released</a>
<br />
"<em>The second beta of the upcoming KDE 3.1 release. A lot more stable.</em>"
<p />

<!-- KOffice 1.2 released -->
<strong>10 September 2002</strong> - <a href="http://www.koffice.org/announcements/announce-1.2.phtml">KOffice 1.2 Released</a>
<br />
"<em>An incredible number of improvements over KOffice 1.1.</em>"
<p />

<!-- KDE 3.1 Beta1 released -->
<strong>21 August 2002</strong> - <a href="announce-3.1beta1.php">KDE 3.1 Beta1 Released</a>
<br />
"<em>The first beta of the upcoming KDE 3.1 release.</em>"
<p />

<!-- KDE 3.0.3 released -->
<strong>19 August 2002</strong> - <a href="announce-3.0.3.php">KDE 3.0.3 Released</a>
<br />
"<em>KDE 3.0.3 is a bugfix and security update over KDE 3.0.2.</em>"
<p />

<!-- KDE 3.1 Alpha1 released -->
<strong>11 July 2002</strong> - <a href="announce-3.1alpha1.php">KDE 3.1 Alpha1 Released</a>
<br />
"<em>A first, experimental snapshot of the CVS tree of the upcoming KDE 3.1 release. Many
new features, and probably many new bugs as well. Have fun ;-)</em>"
<p />

<!-- KDE 3.0.2 released -->
<strong>2 July 2002</strong> - <a href="announce-3.0.2.php">KDE 3.0.2 Released</a>
<br />
"<em>KDE 3.0.2 is a regular maintenance update for the successful KDE 3.0 release.</em>"
<p />

<!-- KDE 3.0.1 released -->
<strong>22 May 2002</strong> - <a href="announce-3.0.1.php">KDE 3.0.1 Released</a>
<br />
"<em>KDE 3.0.1 is a bugfix and translation update for the successful KDE 3.0 release.</em>"
<p />

<!-- KDE 3.0 released -->
<strong>3 April 2002</strong> - <a href="announce-3.0.php">KDE 3.0 Released</a>
<br />
"<em>KDE 3.0 is a major step for the KDE project. A hundred words could
be written here, but to stop boring you with details, lets just say: Check it out!</em>"
<p />

<!-- KDE 3.0 Beta2 released -->
<strong>13 February 2002</strong> - <a href="announce-3.0beta2.php">KDE 3.0 Beta2 Released</a>
<br />
"<em>Beta2 has come a long way since beta1.  At this juncture a lot of
user testing would be very beneficial, as a large group of KDE developers
are meeting for a week at the end of February in N&uuml;rnberg, Germany
to ready the KDE 3.0 final release.  In addition, this release
provides one of the last great opportunities for developers to port
KDE 2 applications to KDE 3 prior to the final release.</em>"
<p />

<!-- KDE 3.0 Beta1 released -->
<strong>19 December 2001</strong> - <a href="announce-3.0beta1.php">KDE 3.0 Beta1 Released</a>
<br />
"<em>After two months of hard work on integrating new features as well
as improving the internationalisation support and further working on
the stability we're releasing this snapshot of the current development
tree to the public, in order to receive more constructive feedback and
ease interested developers and users to follow the development.</em>"
<p />

<!-- KOffice 1.1.1 released -->
<strong>17 December 2001</strong> - <a href="koffice-1.1.1.php">KOffice 1.1.1 Released</a>
<br />
"<em>Currently KOffice has functionality well-suited for home and
SOHO users, who generally write letters, faxes, memos and similar
documents.  KOffice 1.1.1 is a minor update release for KOffice 1.1.</em>"
<p />

<!-- KDE 2.2.2 released -->
<strong>21 November 2001</strong> - <a href="announce-2.2.2.php">KDE 2.2.2 Released</a>
<br />
<em>KDE 2.2.2 is a security and service release that marks the last scheduled release of the KDE 2 series, although further releases may occur.  The KDE Project encourages all users, and strongly encourages all users of KDE in multi-user environments, to upgrade to KDE 2.2.2.</em>
<p />

<!-- KDE 3.0 Alpha1 released -->
<strong>5 October 2001</strong> - <a href="announce-3.0alpha1.php">KDE 3.0 Alpha1 Released</a>
<br />
<em>As a result of our porting efforts we're happy to release 3.0 Alpha1
to the public as the first preview of KDE 3 for interested developers and users.
Alpha 1 is the first release based on the excellent toolkit Qt v3.0
and will continue to be improved and evolved in the near future to
a first stable release of KDE 3.0, which is planned for February 2002. </em>
<p />

<!-- KDE 2.2.1 released -->
<strong>19 September 2001</strong> - <a href="announce-2.2.1.php">KDE 2.2.1 Released</a>
<br />
<em>"In response to customer demand, we have made KDE the default desktop environment in the latest release of our Turbolinux Workstation product," said Dino Brusco, VP of Marketing at Turbolinux Inc. "Our customers really appreciate the features and stability that KDE provides and we will be offering this latest version of KDE in an upcoming release of our Turbolinux Server product."</em>
<p />

<!-- KOffice 1.1 released -->
<strong>28 August 2001</strong> - <a href="koffice-1.1.php">KOffice 1.1 Released</a>
<br />
"<em>Currently KOffice has functionality well-suited for home and
SOHO users, who generally write letters, faxes, memos and similar
documents.  As the import filters for proprietary document formats
continue to improve and the suite's features continue to evolve, we
expect that in the near future KOffice will be ready to meet the
needs of all but the most demanding areas of the enterprise.</em>"
<p />

<!-- KDE 2.2 released -->
<strong>15 August 2001</strong> - <a href="announce-2.2.php">KDE 2.2 Released</a>
<br />
"<em>Thanks to the high quality of the KDE 2 development framework and the
invaluable feedback of our users we are delivering a more polished,
better integrated and more feature-rich desktop experience in a relative
short time.</em>"
<p />

<!-- KDE 2.2 Beta 1 released -->
<strong>4 July 2001</strong> - <a href="announce-2.2-beta1.php">KDE 2.2 Beta1 Released</a>
<br />
KDE 2.2 Beta1 is a user and developer preview of KDE 2.2 incorporating major improvements, enhancements and fixes over KDE 2.1.2. "<em>KDE 2.2beta1 completely integrates the XFree anti-aliased font extensions and can provide a fully anti-aliased font-enabled desktop.</em>"
<p />

<!-- KDE 2.1.2 released -->
<strong>30 April 2001</strong> - <a href="announce-2.1.2.php">KDE libraries 2.1.2 Released</a>
<br />
"<em>The <a href="http://www.kde.org/">KDE
Project</a> today announced the release of kdelibs 2.1.2,
a security and bugfix release of the core KDE libraries.  The other
core KDE packages, including kdebase, have not been updated.</em>"
<p />

<!-- KDE 2.1.1 released -->
<strong>27 March 2001</strong> - <a href="announce-2.1.1.php">KDE 2.1.1 Released</a>
<br />
"<em>The primary goals of the 2.1.1 release are to improve documentation
and provide additional language translations for the user interface,
although the release includes a few bugfixes, and improvements to the HTML
rendering engine</em>"
<p />

<!-- KDE 2.1 released -->
<strong>26 February 2001</strong> - <a href="announce-2.1.php">KDE 2.1 Released</a>
<br />
"<em>KDE Ships Leading Desktop with Advanced Web Browser for GNU/Linux and Other UNIXes.
[...] This release marks a leap forward in GNU/Linux desktop stability, usability
and maturity and is suitable for enterprise deployment.</em>"
<p />

<!-- KDE 2.1 Beta 2 released -->
<strong>31 January 2001</strong> - <a href="announce-2.1-beta2.php">KDE 2.1 Beta2 Released</a>
<br />
"<em>New Beta Version of Leading GNU/Linux Desktop Offers New Theme Manager, Image Viewer and IDE</em>"
<p />

<!-- KDE 2.1 Beta 1 released -->
<strong>16 December 2001</strong> - <a href="announce-2.1-beta1.php">KDE 2.1 Beta1 Released</a>
<br />
"<em>KDE 2.1 will be the second major release of the KDE 2 series, the next generation of the award-winning K Desktop Environment.</em>"
<p />

<!-- KDE 2.0.1 released -->
<strong>05 December 2000</strong> - <a href="announce-2.0.1.php">KDE 2.0.1 released</a>
<br />
"<em>The KDE Team today announced the release of KDE 2.0.1,
KDE's powerful, modular, Internet-enabled desktop.
KDE 2.0.1 is a translation, documentation and bug-fix release and follows
six weeks after the release of KDE 2.0</em>"
<p />

<!-- KDE 2.0 released -->
<strong>23 October 2000</strong> - <a href="announce-2.0.php">KDE 2.0 released</a>
<br />
"<em>The KDE Team today announced the release of KDE 2.0,
KDE's powerful, modular, Internet-enabled desktop. This highly anticipated
release constitutes the next generation of the award-winning KDE 1
series, which culminated in the release of KDE 1.1.2 just over a year ago.
KDE 2.0 is the work product of hundreds of dedicated developers originating
from over 30 countries. </em>"
<p />


<!-- First translators and doc writers meeting -->
<strong>02 October 2000</strong> - <a href="doc-loc-meeting.php">KDE's first translators and doc writers meeting 7th Linux Congress</a>
<br />
"<em>The first meeting of KDE translation and documentation teams has been
a success. (...) This success demonstrates that there are strong energies
willing to bring Unix Operating Systems and Free Software to the citizen with a
documented, easy-to-use graphical interface, in their own language.  This is an
approach that proprietary operating systems are unable to sustain consistently.
Now that computers have come to everyday life, this is putting a threat on
cultural independance of many countries with respect to American language.
On the contrary, all the people attending the workshop are committed to
making computers - even using powerful Operating Systems like GNU/Linux - more
easy to use for everyone.</em>"
<p />

<!-- KDE Final Beta released -->
<strong>15 September 2000</strong> - <a href="announce-1.94.php">KDE Final Beta released</a>
<br />
"<em>The KDE Team today announced the release of KDE 1.94, the fifth and final beta preview of KDE 2.0, KDE's next-generation, powerful, modular desktop. Following the release of KDE 1.93 on August 23, 2000, the release, code-named "Kandidat", is based on Trolltech's Qt 2.2.0 and will include the core KDE libraries, the core desktop environment, the KOffice suite, as well as the over 100 applications from the other core KDE packages: Administration, Games, Graphics, Multimedia, Network, Personal Information Management (PIM), Toys and Utilities. The release is targeted at users who would like to help the KDE team make usability, speed and feature enhancements and fix the remaining set of bugs before the release of KDE 2.0, scheduled for early-fourth quarter 2000.</em>
<p />

<!-- Official Response to Stallman Editorial -->
<strong>5 September 2000</strong> - <a href="rmsresponse.php">Official Response to Stallman Editorial</a>
<br />
"<em>KDE Response to Stallman Editorial In a recent editorial on Linux Today, Richard Stallman claimed that KDE is still in violation of the GPL even though Qt is now covered under the GPL and all KDE code is compatible with the GPL. His rather absurd reasoning is that since KDE once violated the GPL, it will always be in violation unless the individual copyright holders "grant forgiveness." </em>
<p />

<!-- KDE Response to GNOME Foundation -->
<strong>31 August 2000</strong> - <a href="gfresponse.php">KDE Response to GNOME Foundation</a>
<br />
"<em>The recent announcements regarding the formation of a GNOME Foundation coupled with the Sun/Hewlett Packard decision to use GNOME as their standard desktop has resulted in a deluge of requests to the KDE Core Team asking what our "position" is. Well, this is it. We offer this position paper in the hope that we can put this behind us and get back to coding</em>"
<p />

<!-- KDE Desktop Is Show Favorite at LinuxWorld Expo -->
<strong>22 August 2000</strong> - <a href="lwe_08_2000.BW.press-release.php">KDE Desktop Is Show Favorite at LinuxWorld Expo</a>
<br />
"<em>August 22, 2000 (The INTERNET). KDE, the most advanced and user-friendly desktop for GNU/Linux - UNIX operating systems, was honored as Show Favorite in the Desktop category at the LinuxWorld Expo, held in San Jose, California last week.</em>"
<p />

<!-- KDE-2, the Launch Pad -->
<strong>29 July 2000</strong> - <a href="k2launchpad.php">KDE-2, the Launch Pad</a>
<br />
"<em>In a modest attempt to assist new users as well as devoted supporters, the developers of KDE Team prepared a series of documents presenting the most interesting technologies and the most important improvements that the KDE code acquired.</em>"
<p />

<!-- SourceForge hosts KDE CVS -->
<strong>10 February 2000</strong> - <a href="sfcvs.php">SourceForge hosts KDE CVS</a>
<br />
"<em>KDE and VA Linux announced at the recent Linux World Expo that the KDE development system (our CVS) will be hosted by VA Linux with their SourceForge setup.  SourceForge is a Open Source project hosting site located at <a href="http://www.sourceforge.net">http://www.sourceforge.net</a></em>"
<p />

<!-- Meet the KDE Team at CeBIT Show -->
<strong>14 September 1999</strong> - <a href="CeBIT2000.php">Meet the KDE Team at CeBIT Show</a>
<br />
"<em>This year's CeBIT appearance of the KDE Project will be hosted by SuSE on their main booth in hall 3, booth E045. SuSE staff as well as members of the KDE Core Team will be demonstrating KDE 1.1.2 including the new SuSE Desktop. The highlight will be a preview of the upcoming KDE 2.0 release including KOffice, the KDE Office Suite.</em>"
<p />

<!-- KDE-1.1.2 versus KDE-1.1.1  -->
<strong>6 September 1999</strong> - <a href="changelogs/changelog1_1_1to1_1_2.php">KDE-1.1.2 versus KDE-1.1.1</a>
<br />
"<em>This page tries to present as much as possible of the problem corrections and feature additions occurred in KDE between version 1.1.1 and the current, 1.1.2. The changes descriptions were kept brief. The main characteristic of the present release is stability, rock solid stability, as well as improved visual appearance. Thanks to all involved. Thanks to the users for their great moral support. Special thanks to the artists team!</em>"
<p />

<!-- KDE-1.1.1 versus KDE-1.1 -->
<strong>4 May 1999</strong> - <a href="changelogs/changelog1_1to1_1_1.php">KDE-1.1.1 versus KDE-1.1</a>
<br />
"<em>This page tries to present as much as possible of the very nummerous problem corrections and feature additions occurred in KDE between version 1.1 and the current, 1.1.1. The changes descriptions were kept brief. The main characteristic of the present release is stability, rock solid stability. Thanks to all involved. Thanks to the users for their great moral support</em>"
<p />

<!-- Innovation of the Year 1998/99 -->
<strong>19 March 1999</strong> - <a href="ZD-Innov-CeBIT99.php">Innovation of the Year 1998/99</a>
<br />
"<em>(Hanover, Germany) The K Desktop Environment(KDE), an advanced and user-friendly desktop for the increasingly popular GNU/Linux - Unix operating system, was awarded top honors at CeBIT, the worlds largest computer trade fair, as "Innovation of the Year 1998/99" in the category "Software".</em>"
<p />

<!-- KDE-1.1 versus KDE-1.0 - Most important differences  -->
<strong>4 March 1999</strong> - <a href="changelogs/changelog1_0to1_1.php">KDE-1.1 versus KDE-1.0 - Most important differences</a>
<br />
"<em>ATTENTION: this Change Log is nowhere near complete. The changes descriptions were kept brief. There are many applications for which the authors didn't have the time (or found it less essential than coding) to submit a list of changes</em>"
<p />

<!-- Corel Computer and KDE Announce Technology Relationship -->
<strong>25 November 1998</strong> - <a href="KDE-Corel.php">Corel Computer and KDE Announce Technology Relationship</a>
<br />
"<em>Corel Computer and the K Desktop Environment (KDE) Project today announced a technology relationship that will bring the KDE graphical user interface (GUI) to desktop versions of the NetWinder Linux-based thin-client and thin-server computers.</em>"
<p />


<!-- KDE 1.0 Announcement -->
<strong>12 July 1998</strong> - <a href="announce-1.0.php">KDE 1.0 Announcement</a>
<br />
"<em>An integrated Desktop Environment for the Unix Operating System. We are pleased to announce the availability of release 1.0 of the KDesktop Environment</em>"
<p />

<!-- End Announcements -->

<?php
    require('../aether/footer.php');
