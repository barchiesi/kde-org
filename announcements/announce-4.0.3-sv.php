<?php

  $page_title = "KDE 4.0.3 lanserat";
  $site_root = "../../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Också tillgängligt på:
<a href="announce-4.0.3.php">English</a>
<a href="announce-4.0.3-it.php">Italienska</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">Franska</a>
<a href="announce-4.0.3-pt_BR.php">Portugisiska (Braziliansk)</a>
<a href="announce-4.0.3-es.php">Spanish</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (Indien)</a>
<a href="announce-4.0.3-ca.php">Katalanska</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Kinesiska</a>
<a href="announce-4.0.3-cz.php">Tjeckiska</a>
<a href="announce-4.0.3-nl.php">Holländska</a>
<a href="announce-4.0.3.php">Engelska</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">Tyska</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebreiska</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Lettiska</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persiska</a>
<a href="announce-4.0.3-pl.php">Polska</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Rumänska</a>
<a href="announce-4.0.3-ru.php">Rysska</a>
<a href="announce-4.0.3-sl.php">Slovenska</a>
<a href="announce-4.0.3-es.php">Spanska</a>
<a href="announce-4.0.3-sv.php">Svenska</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
   KDE-projektet lanserar tredje service och översättningsversionen för den ledande fria skivbordsmiljön.
</h3>
<p align="justify">
  <strong>
    KDE lanserar den tredje serviceversionen av fjärde generationens fria skrivbordsmiljö. Den här versionen 
    innehåller ett stort antal buggfixar, prestandaförbättringar och uppdateringar av översättningar.
  </strong>
</p>
<p align="justify">
2 april 2008 (Internet).
KDE-projektet tillkännager idag stolt KDE 4.0.3 den tredje serviceversionen för den senaste generationen
av den mest avancerade och kraftfulla skrivbordsmiljön. KDE 4.0.3 är den tredje månatliga uppdateringen
av <a href="4.0/">KDE 4.0</a>. som levereras med den grundläggande skrivbordsmiljön
och ett antal andra paket såsom administration, nätverk, utbildning, tillbehör, multimedia, spel, grafik,
webbutveckling med mera. KDE:s prisbelönta verktyg och applikationer finns översatta till nästan 50 olika språk.
</p>
<p align="justify">
 KDE, inklusive dess bibliotek och applikationer är fritt tillgängliga under Öppen källkodslicenser
KDE är tillgängligt både som källkod och i diverse binära format från <a
href="http://download.kde.org/stable/4.0.3/">http://download.kde.org</a> och finns även tillgängligt 
på <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
eller inkluderat i något <a href="http://www.kde.org/download/distributions.php">
GNU/Linux and UNIX system</a>.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Förbättringar</a>
</h4>
<p align="justify">
KDE 4.0.3 innehåller ett stort antal buggfixar och förbättingar. De flesta av dom finns i
<a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">ändringsloggen</a>.
KDE fortsätter med de månatliga uppdateringarna av version 4.0. KDE 4.1, som kommer att innehålla
ett antal större <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">
förbättringar</a> kommer att släppas i juli i år.
<br />
Förändringarna i KDE 4.0.3 består till största delen av buggfixar och översättningsuppdateringar.
Dessa månatliga versioner innehåller endast kodförändringar som gjorts så att så få
regressionsfel som möjligt ska uppstå. 

Nästa alla moduler i KDE har fått fixar i denna version, men återigen är det KHTML 
som har lyckats få in flest förändringar för att förbättra Konqueror.

<ul>
  <li>Scrolloptimering i KHTML, KDE:s HTML renderingsmotor </li>
  <li>Förbättrad hantering av dialogfönster i KWin, KDE:s fönsterhanterare </li>
  <li>Renderingsoptimeringar i Okular, KDE:s dokumentvisare  </li>
</ul>

<h4>Extragear</h4>
<p align="justify">
Sedan KDE 4.0.0 ingår <a href="http://extragear.kde.org">Extragearapplikationerna</a> 
i KDE:s vanliga versioner.
De applikationer som ingår i Extragear är stabila program som dock inte ingår i de vanliga
KDE-paketen. De applikationer som ingår i Extragear för KDE 4.0.3 är:
<ul>
    <li><a href="http://en.wikipedia.org/wiki/KColorEdit">KColoredit</a> -  
	En editor för färgpaletter som stöder palettformatet i både KDE och Gimp.</li>
    <li>KFax - En faxvisare för KDE. </li>
    <li><a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> - 
        Ett mer avancerat skärmdumpsprogram. </li>
    <li><a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphviewer</a> - A 
        Ett program för att visa GraphVizfiler för KDE.</li>
    <li><a href="http://w1.1358.telia.com/~u135800018/prog.html#KICONEDIT">KIconedit</a> - 
        En ikoneditor.</li>
    <li><a href="http://kmldonkey.org/">KMldonkey</a> - Grafisk klient för EDonkeyprotokollet.</li>
    <li><a href="http://www.kpovmodeler.org/">KPovmodeler</a> - En 3D-modellerare.</li>
    <li>Libksane - Ett scannerbibliotek.</li>
    <li><a href="http://www.rsibreak.org">RSIbreak</a> - Ett program som hjälper till att förebygga RSI
	genom att tvinga dig att ta små pauser. </li>
</ul>
Nytt i den här versionen av Extragear är en KIO-slav för .
<a href="http://en.wikipedia.org/wiki/Gopher_(protocol)">Gopher-protokollet</a>
</p>

<h4>
KDE's Utbildningssvit.
</h4>
<p align="justify">
KDE 4.0.3 levereras med en programsvit av högkvalitativa <a href="http://edu.kde.org">utbildningsprogram</a>.
Här finna applikationer från <a href="http://edu.kde.org/marble/">Marble</a>, en mångsidig jordglob, till ett
litet spel för små barn.
<p align="justify">
Kalzium är ett grafiskt periodiskt system som på ett attraktivt sätt visualiserar koncept som
atomer. Kalzium kan även presentera detaljerad information om de olika grundämnena. Kalzium är 
ett program för att göra kemi enklare för grundskolan men passar även vuxna.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kalzium.png">
    <img src="announce_4.0.3/kalzium_thumb.png" align="center"  height="261"  />
  </a>
  <br /><em>Upptäck kemi med Kalzium</em>
</div>
</p>
<p align="justify">
Parley är ett program för att hjälpa till vid glosträning. Parley har stöd för många språkspecifika 
uppgifter men kan även användas vid annat inlärande. Det använder sig av repetitionsbaserad inlärning
med så kallade flashkort. Det är väldigt enkelt att skapa egna övningar med Parley men det finns
även ett stort antal tillgänglia filer på nätet till Parley.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/parley.png">
    <img src="announce_4.0.3/parley_thumb.png" align="center"  height="225"  />
  </a>
  <br /><em>Glosträning med Parley</em>
</div>
</p>
<p align="justify">

Kmplot är en matemaiskt funktionsritare som gör det enkelt att visualisera matematiska
funktioner. Det är bara att skriva in funktionen så ritar Kmplot upp den åt dig.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kmplot.png">
    <img src="announce_4.0.3/kmplot_thumb.png" align="center"  height="341"  />
  </a>
  <br /><em>Matematik med Kmplot</em>
</div>

För mer information om KDE:s utbildningsprogram se 
<a href="http://edu.kde.org/tour_kde4.0/">KDE Education Project Tour</a>.
</p>

<h4>Installera KDE 4.0.3</h4>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.0.3 och i 
vissa fall har andra personer i omgivningaen skapat paket.
  En del av dessa binära paket finns tillgängliga från KDE:s <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.3/">http://download.kde.org</a>.
    Ytterligare binära paket såväl som uppdatering till de redan tillgänliga kan
komma att dyka upp under de närmaste veckorna.
</p>

<p align="justify">
  <a name="package_locations"><em>Var hittar jag paketen</em></a>.
  För en uppdaterad lista över de binärpaketen KDE-projektet känner till se 
<a href="/info/4.0.2.php">KDE 4.0.3 Info Page</a>.
</p>

<h4>
  Kompilera KDE 4.0.3 själv
</h4>
<p align="justify">
  <a name="source_code"></a><em>Källkod</em>.
  All källkod till KDE 4.0.3 kan laddas ner  <a
href="http://download.kde.org/stable/4.0.3/src/">härifrån</a>.
Instruktioner hur man kompilerar och installerar KDE 4.0.3 finns på
<a href="/info/4.0.2.php#binary">KDE 4.0.3 Info Page</a>.
</p>

<h4>
  Stöd KDE
</h4>
<p align="justify">
 KDE är ett <a href="http://www.gnu.org/philosophy/free-sw.html">Fri Mjukvaru</a>
projekt som existerar och växer endast tack vare hjälp från volontärer som donerar
sin tid och sitt arbete. KDE behöver alltid mer hjälp från frivilliga vare sig det
gäller kodning, buggrättning eller rapportering, hjälp med dokumentation, översättning,
marknadsföring, ekonomisk hjälp etc. All hjälp uppskattas och accepteras gladeligen. 
Se <ahref="/community/donations/">Stöd KDE</a> för mer information. </p>

<p align="justify">
Vi ser fram emot att få höra av dig snart!
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
