<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Small code cleanup: use local var instead of pimItems.at(i) many times. <a href='http://commits.kde.org/akonadi/4b107f4889a842076cd696fd976f43f4e5e1252b'>Commit.</a> </li>
<li>Fix intermittent race in collectionattributetest. <a href='http://commits.kde.org/akonadi/3a062e6a47f292fd5da7b99552894ae6d6d4e323'>Commit.</a> </li>
<li>Use an emit syntax that QtCreator understands better. <a href='http://commits.kde.org/akonadi/6c7db042cb3cfaa0a2fe4b4267f22a030d835157'>Commit.</a> </li>
<li>Remove unneeded warning for killed subjobs, error string was just "". <a href='http://commits.kde.org/akonadi/94c50d3408264328af4ff10eb412ecd3137fc18f'>Commit.</a> </li>
<li>Add missing Q_EMIT before signal emission. <a href='http://commits.kde.org/akonadi/567aeb65d20def82f4b3cacb1df4bf55acd22d4e'>Commit.</a> </li>
<li>Collectionattributetest: small cleanups. <a href='http://commits.kde.org/akonadi/bb50935bd15fbc8ba66a8192859f0432059e8fd4'>Commit.</a> </li>
<li>Server: Improve debug, warning and error log messages. <a href='http://commits.kde.org/akonadi/51392718e804ac57193123b5442edbdc1bd8c9d5'>Commit.</a> </li>
<li>Destroy the Connection Through the Session Thread. <a href='http://commits.kde.org/akonadi/a9570303d08a1b2099d862f115c8f2b99fe0fbc7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381636'>#381636</a></li>
<li>Akonadi: fix racy code in (MimeType|Resource)::retrieveByNameOrCreate. <a href='http://commits.kde.org/akonadi/6692cb577e4ca95828642373be760ca511a38aeb'>Commit.</a> </li>
<li>Akonadi: fix timing-dependent failure of partstreamertest. <a href='http://commits.kde.org/akonadi/ff5c5c79a01d76c1cf5b346c1e391ddd66fdef0b'>Commit.</a> </li>
<li>Akonadi: add missing dependencies on included XSL files. <a href='http://commits.kde.org/akonadi/a9511393016e8f7cc1c5356070267d598c521a6c'>Commit.</a> </li>
<li>Fix SQLite backend foreign key PRAGMAs. <a href='http://commits.kde.org/akonadi/42d6c38c4dc619f23ca00ad42d9c538fc9ca2f78'>Commit.</a> See bug <a href='https://bugs.kde.org/402229'>#402229</a></li>
<li>Simplify AggregatedFetchScope code, removing all setters. <a href='http://commits.kde.org/akonadi/5099b3cfda53019633cc6b3efc63547127191b09'>Commit.</a> </li>
<li>Fix tag change notification shipping a bogus name. <a href='http://commits.kde.org/akonadi/861a6e8800cca81778db85af8d6b022f1af3d45d'>Commit.</a> </li>
<li>AggregatedFetchScope: fix fetchAllAttributes for tags. <a href='http://commits.kde.org/akonadi/ccea01c5cd15aa9f8f15ee9fc09b35af554145d0'>Commit.</a> </li>
<li>Properly clean up scopes when deregistering a subscriber. <a href='http://commits.kde.org/akonadi/a4ff4ce80087933b6b5ca27d81c0d0de45e63d58'>Commit.</a> </li>
<li>Fix "QIODevice::read" warnings. <a href='http://commits.kde.org/akonadi/87a155b850808bde5f885199c27b1ecc44f216b6'>Commit.</a> </li>
<li>Autotests: partstreamertest: show more info on failure. <a href='http://commits.kde.org/akonadi/4208803dd65e1b49748115cfbb92d2ede83ef35e'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Don't duplicate items. <a href='http://commits.kde.org/akonadi-contacts/93d1edf66cd42cc6b8d0ad50b3823a28e93822ae'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Fix loading of tar.zst archives. <a href='http://commits.kde.org/ark/91341d05e8f887dd7a30b17191485a64d7af9d78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404464'>#404464</a></li>
<li>Cli7ztest: add failing test case for zip folders with RDA attributes. <a href='http://commits.kde.org/ark/95bf2244e5b4c9fe7901fd50dd662dd514439407'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Remove incorrect <provides><library> from appdata, not a real library. <a href='http://commits.kde.org/audiocd-kio/3961bc53df85342e62b9637a47efeab59fdd8ce2'>Commit.</a> </li>
<li>Call KLocalizedString::setApplicationDomain only after QApp instance done. <a href='http://commits.kde.org/audiocd-kio/6cac200d673fd68313613afa46a000a3c7193127'>Commit.</a> </li>
<li>Fix Ogg Vorbis logic for Average and Min/Max bitrates. <a href='http://commits.kde.org/audiocd-kio/7a781a3bbb2126faed12dc422dca5a9e3abdf0c4'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix build with julia 1.1. <a href='http://commits.kde.org/cantor/499b9d90337ec24af590d0549f8c41acac08e4bf'>Commit.</a> </li>
<li>Fix bug with SAGE_ROOT. <a href='http://commits.kde.org/cantor/5d2c7c33f1be09b4f197f962923ba030f1ca5c1c'>Commit.</a> </li>
<li>Fix bug with visible entry cursor on dragable entry. <a href='http://commits.kde.org/cantor/af101f1ce0f2dfc56267935f668561ad0527c361'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix crash during shutdown. <a href='http://commits.kde.org/dolphin/b1ccec70f28fefca8fcd464ec21dd13070c72e5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402784'>#402784</a></li>
<li>Word-wrap KMessageWidget text. <a href='http://commits.kde.org/dolphin/cf2da56c5e76c168ffdcdf7e2bbbf3ae4f924f55'>Commit.</a> See bug <a href='https://bugs.kde.org/404232'>#404232</a></li>
<li>[DolphinView] Use correct color group. <a href='http://commits.kde.org/dolphin/585cc994e6c5d03d276cd06e93dd808cf0efac92'>Commit.</a> See bug <a href='https://bugs.kde.org/404053'>#404053</a></li>
<li>Sort preview plugins case-insensitively. <a href='http://commits.kde.org/dolphin/bf44548ad15437df3138e654636639046d8fa127'>Commit.</a> </li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Hide]</a></h3>
<ul id='uldragon' style='display: block'>
<li>Set StartupWMClass in desktop file. <a href='http://commits.kde.org/dragon/88735ba191bcfe7b63810c07ec8d883b8bad07e7'>Commit.</a> </li>
</ul>
<h3><a name='ffmpegthumbs' href='https://cgit.kde.org/ffmpegthumbs.git'>ffmpegthumbs</a> <a href='#ffmpegthumbs' onclick='toggle("ulffmpegthumbs", this)'>[Hide]</a></h3>
<ul id='ulffmpegthumbs' style='display: block'>
<li>Bail out if stream is negative. <a href='http://commits.kde.org/ffmpegthumbs/caf68da418658d99fa8e48db99b9733ef6697fa3'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>Fix use after free in RadialMap::Widget::invalidate(). <a href='http://commits.kde.org/filelight/4cc7c44589f07f0f5294f8ed6f0b71d20699cd1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389119'>#389119</a></li>
<li>Add .arcconfig. <a href='http://commits.kde.org/filelight/8df422b10b50e75b71f617a800605d5b6f33bad0'>Commit.</a> </li>
<li>Don't include <fstab.h> anymore. <a href='http://commits.kde.org/filelight/70fa1a80ce05680bc607212915b490861a4fc1de'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Fix displaying covers from cover.jpg files. <a href='http://commits.kde.org/juk/849e8dca4021c864a680cfd4ee7c0e72a6292db5'>Commit.</a> </li>
<li>Fix crash in filtering playlist to playing album/artist. <a href='http://commits.kde.org/juk/5c1470d2f7f4b3f8ba73644fbfa2206bc22063dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402355'>#402355</a></li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Fix taking pictures when the pictures directories didn't exist. <a href='http://commits.kde.org/kamoso/6f237a28ea5eba6ba5e9f7d0c7be45fa6f869a2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404456'>#404456</a></li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Hide]</a></h3>
<ul id='ulkcachegrind' style='display: block'>
<li>Set StartupWMClass in desktop file. <a href='http://commits.kde.org/kcachegrind/4b7a688281257e9f72684f101cf348905b7531c8'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Hide]</a></h3>
<ul id='ulkgeography' style='display: block'>
<li>Fix appstream data. <a href='http://commits.kde.org/kgeography/6a1827ee738894e1742e1e5554ada638f497e73d'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 403313 - Encoding problem on kmail2 5.10.1 when creating composer from command line. <a href='http://commits.kde.org/kmail/893be637804d4f1e3478685314ede211001a888d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403313'>#403313</a></li>
</ul>
<h3><a name='kompare' href='https://cgit.kde.org/kompare.git'>kompare</a> <a href='#kompare' onclick='toggle("ulkompare", this)'>[Hide]</a></h3>
<ul id='ulkompare' style='display: block'>
<li>Set StartupWMClass in desktop file. <a href='http://commits.kde.org/kompare/5776e86527a2066f85ea436f74424ce2610dd259'>Commit.</a> </li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Hide]</a></h3>
<ul id='ulkrdc' style='display: block'>
<li>Set StartupWMClass in desktop file. <a href='http://commits.kde.org/krdc/0cbe0097659649ffde352e4468cc9e3ecdffaaf5'>Commit.</a> </li>
</ul>
<h3><a name='ktp-common-internals' href='https://cgit.kde.org/ktp-common-internals.git'>ktp-common-internals</a> <a href='#ktp-common-internals' onclick='toggle("ulktp-common-internals", this)'>[Hide]</a></h3>
<ul id='ulktp-common-internals' style='display: block'>
<li>Fix escape/url filter URL placeholder backward substitution. <a href='http://commits.kde.org/ktp-common-internals/5c77cc29db5966a7ef227f20da8d54a264f28746'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384968'>#384968</a></li>
</ul>
<h3><a name='ktp-contact-list' href='https://cgit.kde.org/ktp-contact-list.git'>ktp-contact-list</a> <a href='#ktp-contact-list' onclick='toggle("ulktp-contact-list", this)'>[Hide]</a></h3>
<ul id='ulktp-contact-list' style='display: block'>
<li>Restore the "Offline" option in presence combobox. <a href='http://commits.kde.org/ktp-contact-list/6dcdbdc872c2d8fb8e7838dcf5880c5b27b93c47'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404350'>#404350</a></li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Hide]</a></h3>
<ul id='ulktp-text-ui' style='display: block'>
<li>[logviewer] Fix visual space between the MessageView and navigation. <a href='http://commits.kde.org/ktp-text-ui/8402201446b51b8373ee63fc851a5cca8be2a4bc'>Commit.</a> </li>
<li>[logviewer] Fix MessageView not being visible. <a href='http://commits.kde.org/ktp-text-ui/fb9ae6d15030887ba9c256afa0ccb9f870e6a900'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Fix building with akonadi search support. <a href='http://commits.kde.org/libkdepim/0e21571f1629b162fd63713c10102e654c9d23e5'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Fix googletalk protocol. <a href='http://commits.kde.org/libkgapi/9c714d7e2dda6182e43e685f4f3390841de10401'>Commit.</a> </li>
<li>Contacts: fix email type deserialization. <a href='http://commits.kde.org/libkgapi/2a85825b3a15a6d87fd9d79a61af31927d716641'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398847'>#398847</a></li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Update indonesian nplurals. <a href='http://commits.kde.org/lokalize/871d08cd2d3e848ca79fae68560f608e52d03560'>Commit.</a> </li>
<li>Do not launch pology on non-existent .po. <a href='http://commits.kde.org/lokalize/35fe0b7e562c27207bcc45e8fe348902c79e2c1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401393'>#401393</a></li>
<li>Revert "Add support for viewing the translation source with a custom editor". <a href='http://commits.kde.org/lokalize/1dde89e09a9b68c81e0a3254c84fb671e960e1e7'>Commit.</a> </li>
<li>Only display the first line of messages in the catalog. <a href='http://commits.kde.org/lokalize/d9ceed7f6ed42f49ee042a1132ab0f7efbf061da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402931'>#402931</a></li>
<li>Add support for viewing the translation source with a custom editor. <a href='http://commits.kde.org/lokalize/efd296e14f3b240364cc1b48375127c2b26c35b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403743'>#403743</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix runtime warning when calling deleteLater on a null object. <a href='http://commits.kde.org/messagelib/69a9a0256e1b62b7f71161903225b965f35bce79'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix 'No tree view update of entity unique and check constraint'. <a href='http://commits.kde.org/umbrello/aaf0740f13b310a143d26ca904e49ddb42a85308'>Commit.</a> See bug <a href='https://bugs.kde.org/350241'>#350241</a></li>
<li>Fix moving entities by property dialog. <a href='http://commits.kde.org/umbrello/cff43b124d50d4321ac58d46b9c38c9e13e45cf9'>Commit.</a> See bug <a href='https://bugs.kde.org/404892'>#404892</a></li>
<li>Fix 'Cannot move entities, components, subsystems and nodes from sub folder to root folder'. <a href='http://commits.kde.org/umbrello/a52e8b54c13bb6d7f21ed45d82fd38fd8c30907c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404892'>#404892</a></li>
<li>Fix allowing moving of ports to a sub folder in the tree view. <a href='http://commits.kde.org/umbrello/749d3eac3fdba753f512e9d50e0ea5a8665cebe5'>Commit.</a> See bug <a href='https://bugs.kde.org/404892'>#404892</a></li>
<li>Fix 'No tree view update of entity primary key constraint'. <a href='http://commits.kde.org/umbrello/eaad82bae258e3ad5aa2c23f8f744c8f3d84e02f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350241'>#350241</a></li>
<li>Add typedef to class UMLForeignKeyConstraintDialog. <a href='http://commits.kde.org/umbrello/f5dcba1b3f49551f7b642b55ea220f149ed40b62'>Commit.</a> See bug <a href='https://bugs.kde.org/350241'>#350241</a></li>
<li>Fix 'Cannot add foreign key to entity'. <a href='http://commits.kde.org/umbrello/be576384df1b2e8ba6b8fcecf6a2323ce3539413'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404891'>#404891</a></li>
<li>Release-windows-packages: auto detect version. <a href='http://commits.kde.org/umbrello/ee891a2124716e2f4aea32006ab3a94ce1a31c27'>Commit.</a> </li>
<li>Release-windows-packages: fix fetching source tar ball name. <a href='http://commits.kde.org/umbrello/43a0a5d8e0d680cc6349869bb7c931a922454e2e'>Commit.</a> </li>
<li>Fix 'Non-optimal vertical position of added messages to sequence diagrams'. <a href='http://commits.kde.org/umbrello/06425ca08ce44422ab2e16d957519e73db059777'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404422'>#404422</a></li>
<li>Handle default case in ChildWidgetPlacementPort::setNewPositionWhenMoved. <a href='http://commits.kde.org/umbrello/269549cef60204931d25b14f52cd3ed7d2f30cc4'>Commit.</a> </li>
<li>Fix crash on adding ports to component widget by context menu. <a href='http://commits.kde.org/umbrello/0d2f0021b63469c2a6011700796584dc9231e2e7'>Commit.</a> See bug <a href='https://bugs.kde.org/394230'>#394230</a></li>
</ul>
