
<h3>
<a href="platform.php">
Mobile Target Makes KDE Platform Lose Weight
</a>
</h3>

<p>
<a href="platform.php">
<img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.6.0"/>
</a>
The <b>KDE Platform</b>, on which the Plasma Workspaces and KDE Applications are built also gains new capabilities available to all KDE applications. The introduction of a "mobile build target" allows for easier deployment of applications on mobile devices. The Plasma framework gains support for writing desktop widgets in QML, the declarative Qt language, and provides new Javascript interfaces for interacting with data. Nepomuk, the technology behind metadata and semantic search in KDE applications, now provides a graphical interface to back up and restore data. UPower, UDev and UDisks can be used instead of the deprecated HAL. Bluetooth support is improved. The Oxygen widget and style set is improved and a new Oxygen theme for GTK applications allows them to seamlessly merge into the Plasma workspaces and look just like KDE applications. For more details read the <a href="platform.php">KDE Platform 4.6 announcement</a>.
</p>
