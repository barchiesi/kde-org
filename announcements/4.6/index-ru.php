<?php
  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "KDE придаёт вам власть над компьютером с новыми оболочками рабочего стола, приложениями и платформой разработки";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>На других языках:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
Сообщество KDE радо объявить о новом выпуске оболочек рабочего стола Plasma,
приложений KDE и платформы KDE с основными обновлениями.
Эти выпуски с номером версии 4.6 добавляют много новых возможностей в каждый
из трёх продуктов KDE. Краткое описание нововведений:
</p>
<?php
  centerThumbScreenshot("46-w09.png", "Рабочий стол KDE Plasma, Gwenview и диалог запуска KRunner в 4.6");
?>

<?php
include("trailer-plasma-ru.inc");
include("trailer-applications-ru.inc");
include("trailer-platform-ru.inc");

?>


<h4>
    Несите благую весть!
</h4>
<p align="justify">
KDE призывает всех <strong>делиться информацией об этом выпуске</strong>
на социальных сервисах в сети Интернет.
Высылайте материалы на новостные сайты, распространяйте
информацию через delicious, digg, reddit, twitter и identi.ca.
Загружайте снимки экрана на Facebook, Flickr, ipernity и Picasa
и добавляйте их в соответствующие группы. Создавайте видеоролики
и загружайте их на YouTube, Blip.tv, Vimeo и другие веб-сайты.
И не забывайте помечать загруженные материалы
<em>тегом <strong>kde</strong></em>, чтобы людям было легче их
найти, а команда разработчиков KDE могла составить отчёт о том,
насколько полно был освещён анонс нового выпуска программного обеспечения KDE.
<strong>Расскажите о нас миру!</strong></p>

<p align="justify">
Следите за развитием событий вокруг выпуска
<?php echo $release?> через
<a href="http://buzz.kde.org"><strong>ленту новостей сообщества KDE</strong></a>.
На этом сайте вы увидите всё, что происходит на
identi.ca, Twitter, YouTube, Flickr, PicasaWeb, в блогах и
других социальных сетях в реальном времени.
Лента новостей находится по адресу
<strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="">
            <a href="http://digg.com/search?s=kde46">
                <img src="buttons/digg.gif" alt="Digg" title="Digg" />
            </a>
        </a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde46">
            <img src="buttons/reddit.gif" alt="Reddit" title="Reddit" />
        </a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46">
            <img src="buttons/twitter.gif" alt="Twitter" title="Twitter" />
        </a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46">
            <img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" />
        </a>
    </td>
    <td>
        <a href="http://vkontakte.ru/club33239">
            <img src="buttons/vkontakte.gif" alt="В Контакте" title="В Контакте" />
        </a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46">
            <img src="buttons/flickr.gif" alt="Flickr" title="Flickr" />
        </a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46">
            <img src="buttons/youtube.gif" alt="Youtube" title="Youtube" />
        </a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts">
            <img src="buttons/facebook.gif" alt="Facebook" title="Facebook" />
        </a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46">
            <img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" />
        </a>
    </td>
    <td>
        <a href="http://fotki.yandex.ru/tags/kde">
            <img src="buttons/yandex-fotki.gif" alt="Яндекс.Фотки" title="Яндекс.Фотки" />
        </a>
    </td>
</tr>
</table>
<span style="font-size: 6pt">
    <a href="http://microbuttons.wordpress.com">микрокнопки</a>
</span>
</div>

<h4>Поддержите KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify">KDE e.V. открывает новую <a
href="http://jointhegame.kde.org/">акцию поддержки</a>. Платя &euro;25 в квартал, вы обеспечиваете
расширение сообщества KDE, которое создаёт свободное программное обеспечение мирового класса.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
