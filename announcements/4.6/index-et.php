<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "KDE uued töötsoonid, rakendused ja platvorm annavad kontrolli kasutaja kätte";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE-l on rõõm teatada uusimatest väljalasetest, mis uuendavad oluliselt KDE Plasma töötsoone, KDE rakendusi ja KDE platvormi. Kõik need järjekorranumbrit 4.6 kandvad väljalasked pakuvad hulganisti uusi omadusi ja võimalusi kõigis kolmes KDE tooteharus. Mõningad olulisemad neist on järgmised:
</p>
<?php
  centerThumbScreenshot("46-w09.png", "KDE Plasma töölaud, Gwenview ja KRunner 4.6-s");
?>

<?php
include("trailer-plasma-et.inc");
include("trailer-applications-et.inc");
include("trailer-platform-et.inc");

?>


<h4>
    Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline
</h4>
<p align="justify">
KDE julgustab kõiki <strong>levitama sõna</strong> sotsiaalvõrgustikes.
Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter,
identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr,
ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige
need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile
<em>silti <strong>kde</strong></em>, et kõik võiksid vajaliku materjali hõlpsamini üles leida
ja KDE meeskond saaks koostada aruandeid KDE SC <?php echo $release;?> väljalasketeate
 kajastamise kohta. <strong>Aidake meil sõna levitada ja andke sellesse ka oma panus!</strong></p>

<p align="justify">
Kõike seda, mis toimub seoses <?php echo $release?> väljalaskega sotsiaalvõrgustikes, võite jälgida
<a href="http://buzz.kde.org"><strong>KDE kogukonna otsevoos</strong></a>. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes
sotsiaalvõrgustikes. Otsevoo leiab aadressilt <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>KDE toetamine</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Astuge mängu" align="left"/> </a>
<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
