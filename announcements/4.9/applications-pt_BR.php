<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Novidades e Melhorias nos Aplicativos do KDE";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
O KDE está feliz em anunciar o lançamento de versões melhoradas de diversos aplicativos populares. Isto inclui muitas ferramentas básicas importantes e aplicativos como o Okular, o Kopete, o KDE PIM, aplicativos educacionais e jogos.
</p>
<p>
O visualizador de documentos Okular do KDE pode agora armazenar e imprimir anotações com documentos PDF. A busca, marcação e seleção de texto foi melhorada. O Okular pode ser configurado de modo que o laptop não entre em modo de economia de energia ou desligue durante uma apresentação, e ele pode agora reproduzir filmes incorporados em arquivos PDF. O visualizador de imagens Gwenview fornece uma nova opção de navegação em tela cheia, além de diversas correções de erro e pequenas melhorias.
</p>
<p>
O Juk, o reprodutor de músicas padrão do KDE, traz o suporte ao last.fm com scrobbling e obtenção de capas bem como suporte ao MPRIS2. Ele irá ler a arte de capa embutida em arquivos MP4 e AAC. O Dragon, o reprodutor de vídeo do KDE, também funciona com o MPRIS2 agora.
</p>
<p>
O versátil cliente de bate-papo Kopete pode agrupar todos os usuários off-line em um único grupo "Usuários Offline" e mostrar a mudança de status do contato na janela de bate-papo. Existe uma nova opção para 'renomear' o contato que permite personalizar a exibição do nome na própria janela.
</p>
<p>
O aplicativo de tradução Lokalize melhorou a pesquisa de mensagens aproximadas e possui uma aba de pesquisa melhorada. Ele pode agora manipular arquivos .TS. O Umbrello posuui suporte à layout automático para diagramas e pode exportar desenhos para o graphviz. O Okteta introduziu perfis de visualização, incluindo um gerenciador/editor.
</p>
<h2>Suíte do Kontact</h2>
<p>
A mais completa suíte PIM do mundo, o Kontact, recebeu diversas correções de erro e melhorias de desempenho. Este lançamento introduz um assistente de importação para obter configurações, correio, filtros, calendários e entradas do livro de endereçoes do Thunderbird e Evolution para o KDE PIM. Existe uma ferramenta que pode criar cópias de segurança de mensagens de correio, configurações e meta-dados e restaurá-las. O KTnef, o visualizador independente de anexos TNEF, foi trazido de volta à vida dos arquivos do KDE 3. Recursos do Google podem ser integrados ao KDE PIM, fornecendo aos usuários acesso aos seus contatos e calendários do Google.
</p>
<h2>Educação no KDE</h2>

<p>
O KDE-Edu apresenta o Pairs, um novo jogo de memória. O Rocs, o aplicativo de teoria dos grafos para estudantes e professores, recebeu várias melhorias. Algoritmos pode agora ser executados passo-a-passo, o sistema de desfazer e cancelar construção funciona melhor, e camadas sobrepostas de grafos são agora suportadas. O Kstars possui uma ordenação melhorada da hora de trânsito do meridiano / hora de observação e melhor <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">obtenção de imagem</a> do Reconhecimento do Céu Digital.
<div align="center" class="screenshot">
<a href="screenshots/kde49-pairs.png"><img src="screenshots/kde49-pairs-thumb.png" /></a></div>
</p>
<p>
O Marble recebeu otimizações e suporte a threads, e sua interface foi aprimorada. As extensões de rotação do Marble agora incluem o OSRM (Open Source Routing Machine), suporte para rotas de ciclistas e pedestres, e um modelo de dados offline para gerenciar roteamento e busca de dados offline. O Marble pode agora mostrar posições de um avião voando no simulador FlightGear.
</p>
<h2>Jogos do KDE</h2>
<p>
Os Jogos do KDE foram atualizados. Muito polimento foi aplicado ao Kajongg, o jogo de Mahjongg do KDE, incluindo dicas de jogo, IA melhorada e bate-papo se os jogadores estiverem no mesmo servidor (kajongg.org agora possui um!). O KGoldrunner tem alguns novos níveis (uma contribuição de Gabriel Miltschitzky) e o KPatience retém o histórico do jogo ao salvar. O KSudoku teve pequenas melhorias como dicas melhores, bem como vários novos quebra-cabeças bi-dimensionais e três novas formas em 3-D.
<div align="center" class="screenshot">
<a href="screenshots/kde49-ksudoku-3d-samurai.png"><img src="screenshots/kde49-ksudoku-3d-samurai-thumb.png" /></a></div>
</p>

<h4>Instalando aplicativos do KDE</h4>
<?php
  include("boilerplate-pt_BR.inc");
?>

<h2>Também anunciado hoje:</h2>
<h2><a href="plasma-pt_BR.php"><img src="images/plasma.png" class="app-icon" alt="Espaço de Trabalho do Plasma 4.9" width="64" height="64" />  Espaço de Trabalho do Plasma – Principais Melhorias</a></h2>
<p>
Os destaques para o Espaço de Trabalho do Plasma incluem melhorias substanciais no Gerenciador de Arquivos Dolphin, no Emulador de Terminal X do Konsole, nas Atividades e no Gerenciador de Janelas KWin. Leia os detalhes no <a href="plasma-pt_BR.php">'Anúncio do Espaço de Trabalho do Plasma'.</a>
</p>
<h2><a href="platform-pt_BR.php"><img src="images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento KDE 4.9"/> Plataforma KDE</a></h2>
<p>
O lançamento de hoje da Plataforma KDE inclui correções de erros, outras melhorias de qualidade, conectividade e preparação para o Framework 5
</p>
<?php
  include("footer.inc");
?>
