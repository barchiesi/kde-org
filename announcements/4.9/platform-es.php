<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "La Plataforma de KDE 4.9 – Mejor interoperabilidad, desarrollo más fácil";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>También disponible en:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE se enorgullece de anunciar el lanzamiento de la Plataforma de KDE 4.9. La Plataforma de KDE es la base para los Espacios de trabajo Plasma y las Aplicaciones de KDE. El equipo de KDE está trabajando en la actualidad en la siguiente generación de la Plataforma de Desarrollo de KDE, conocida como «Frameworks 5». Aunque Frameworks 5 será bastante compatible a nivel de software, estará basada en Qt5. Proporcionará más granularidad, haciendo que sea más fácil usar partes de la plataforma de forma selectiva y reduciendo considerablemente el número de dependencias. Como resultado, la Plataforma de KDE permanece sin cambios en su mayor parte. La mayor parte del trabajo realizado en la Plataforma ha consistido en corregir los errores y mejorar el rendimiento.
</p>
<p>
Ha progresado la separación de QGraphicsView de Plasma para facilitar la transición a un Plasma basado en QML puro. También hay mejoras en la implementación de Qt Quick en las bibliotecas de KDE y en los espacios de trabajo Plasma.
</p>
<p>
Se ha realizado algo de trabajo a bajo nivel en las bibliotecas de KDE dentro del área de las redes. El acceso a los recursos compartidos de una red se ha hecho más rápido para todas las aplicaciones de KDE. Las conexiones NFS, Samba y SSHFS, además de KIO, ya no cuentan los elementos en las carpetas, lo que hace que sean más rápidas. El protocolo HTTP también se ha acelerado al evitar interacciones innecesarias con el servidor. Esto se nota especialmente en aplicaciones en red, como Korganizer, que es alrededor de un 20% más rápido debido a estas correcciones. También se ha mejorado el almacenamiento de las contraseñas para los recursos compartidos en red.
</p>
<p>
Se han corregido muchos errores críticos en Nepomuk y en Soprano, haciendo que la búsqueda del escritorio sea más rápida y más fiable para las aplicaciones y los espacios de trabajo construidos sobre la Plataforma de KDE 4.9. La mejora del rendimiento y la estabilidad han sido los objetivos principales de estos proyectos para este lanzamiento.
</p>

<h4>Instalación de la Plataforma de Desarrollo de KDE</h4>
<?php
  include("boilerplate-es.inc");
?>

<h2>También se han anunciado hoy:</h2>
<h2><a href="plasma-es.php"><img src="images/plasma.png" class="app-icon" alt="Los espacios de trabajo Plasma de KDE 4.9" width="64" height="64" />Espacios de trabajo Plasma 4.9 – Mejoras globales</a></h2>
<p>
Lo más destacado de los espacios de trabajo Plasma incluye mejoras sustanciales en el gestor de archivos Dolphin, el emulador de terminal de X Konsole, las Actividades y el gestor de ventanas KWin. Lea el <a href="plasma-es.php">«Anuncio de los espacios de trabajo Plasma»</a>.
</p>
<h2><a href="applications-es.php"><img src="images/applications.png" class="app-icon" alt="Las aplicaciones de KDE 4.9"/>Aplicaciones de KDE 4.9 nuevas y mejoradas</a></h2>
<p>
Hoy se han liberado aplicaciones de KDE nuevas y mejoradas que incluyen Okular, Kopete, KDE PIM y aplicaciones y juegos educativos. Lea el <a href="applications-es.php">«Anuncio de aplicaciones de KDE»</a>.
</p>
<?php
  include("footer.inc");
?>
