<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE 4.9 – em memória de Claire Lotion";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";
	
?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
<img src="../../stuff/clipart/klogo-official-oxygen-128x128.png" style="float: left;" />O KDE está orgulhoso em anunciar seu último lançamento, que fornece atualizações importantes para o <a href="plasma-pt_BR.php">Espaço de Trabalho Plasma do KDE</a>, os <a href="applications-pt_BR.php">Aplicativos do KDE</a> e a <a href="platform-pt_BR.php">Plataforma KDE</a>. A versão 4.9 fornece novas funcionalidades, além de melhorar a estabilidade e o desempenho.
</p>
<p>
Este lançamento é dedicado à memória da contribuidora do KDE <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Claire Lotion</a>. A personalidade e o entusiasmo vibrantes de Claire foram uma inspiração para muitos de nossa comunidade, e seu trabalho pioneiro no formato, escopo e frequência de nossas reuniões de desenvolvedores mudou a maneira como nós cumprimos nossa missão nos dias de hoje, e nós estamos profundamente gratos pelo seu esforço.</p>
<div align="center" class="screenshot">
<a href="screenshots/kde49-desktop.png"><img src="screenshots/kde49-desktop-thumb.png" /></a>
</div>
<p>
A Equipe de Qualidade do KDE foi criada no início deste ano com o objetivo de melhorar os níveis de qualidade e estabilidade do KDE. Atenção especial foi dada à identificação e correção de regressões em relação às versões anteriores. Esta foi a maior prioridade pois isto garante as melhorias com cada nova versão.
</p>
<p>
A Equipe também definiu um processo de teste mais rigoroso para os lançamentos a partir das versões beta. Novos testadores voluntários receberam treinamento; e vários dias de testes intensivos foram realizados. Ao invés do teste exploratório tradicional, os testadores foram designados para focar áreas específicas que foram alteradas em relação à versão anterior. Para diversos componentes críticos, listas de verificação completas foram desenvolvidas e usadas. A equipe encontrou diversos problemas importantes com antecedência e trabalhou com os desenvolvedores para certificar-se de que eles foram corrigidos. A Equipe em si reportou mais de 160 erros com os lançamentos beta e RC, a maioria dos quais foi corrigido. Outros usuários do beta e do RC adicionaram um considerável número de erros reportados. Estes esforços foram importantes porque eles permitiram que os desenvolvedores se concentrassem na correção dos problemas.
</p>
<p>
Como resultado deste trabalho da Equipe de Qualidade do KDE, a versão 4.9 esté melhor do que nunca.
</p>
<p>
Uma correção de erro em particular merece uma atenção especial. Um erro do Okular reportado em 2007 recebeu aproximadamente 1100 votos; ele era importante para muitos usuários. Eles reclamavam sobre as anotações no Okular e que não era possível salvá-las ou imprimí-las. Com a ajuda de muitos comentaristas e usuários do canal IRC do Okular, Fabio D’Urso implementou uma solução que permite que as anotações do PDF no Okular sejam salvas e impressas. A correção exigiu algum trabalho nas bibliotecas do KDE e a atenção do design como um todo para certificar-se de que os documentos que não são PDF funcionassem corretamente. Esta foi a primeira experiência do Fabio como desenvolvedor do KDE, que surgiu quando ele encontrou o problema e decidiu fazer alguma coisa a respeito.
</p>
<h2><a href="plasma-pt_BR.php"><img src="images/plasma.png" class="app-icon" alt="Espaço de Trabalho do Plasma 4.9" width="64" height="64" /> Espaço de Trabalho do Plasma – Principais Melhorias</a></h2>
<p>
Os destaques para o Espaço de Trabalho do Plasma incluem melhorias substanciais no Gerenciador de Arquivos Dolphin, no Emulador de Terminal X do Konsole, nas Atividades e no Gerenciador de Janelas KWin. Leia os detalhes no <a href="plasma-pt_BR.php">'Anúncio do Espaço de Trabalho do Plasma'.</a>
</p>
<h2><a href="applications-pt_BR.php"><img src="images/applications.png" class="app-icon" alt="Os Aplicativos do KDE 4.9"/>Novidades e Melhorias nos Aplicativos do KDE</a></h2>
<p>
As novidades e melhorias nos Aplicativos do KDE lançados hoje incluem o Okular, o Kopete, o KDE PIM, os aplicativos educacionais e os jogos. Leia os detalhes no <a href="applications-pt_BR.php">'Anúncio dos Aplicativos do KDE'</a>
</p>
<h2><a href="platform-pt_BR.php"><img src="images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento KDE 4.9"/> Plataforma KDE</a></h2>
<p>
O lançamento de hoje da Plataforma KDE inclui correções de erros, outras melhorias de qualidade, conectividade e preparação para o Framework 5
</p>
<h2>Espalhe a notícia e veja o que acontece: Marque como "KDE"</h2>
<p>
O KDE encoraja as pessoas a espalhar a notícia na Web social. Envie estórias para os portais de notícias e canais do usuário como o delicious, digg, reddit, twitter, identi.ca. Carregue as imagens do novo KDE em serviços como o Facebook, Flickr, ipernity e Picasa, e poste-as nos grupos apropriados. Crie screencasts e carregue-os no YouTube, Blip.tv e Vimeo. Por favor, marque as postagens e os materiais enviados com "KDE". Isto os torna fáceis de encontrar e fornece à Equipe de Promoção do KDE uma maneira de analisar a cobertura do lançamento do KDE 4.9.
</p>
<p>
Siga o que está acontecendo com o KDE live feed. Este site agrega a atividade em tempo real do identi.ca, twitter, youtube, flickr, picasaweb, blogs e outras redes sociais. O live feed pode ser encontrado em <a href="http://buzz.kde.org">buzz.kde.org</a>.
</p>
<h2>Festas de lançamento</h2>
<p>
Como sempre, os membros da comunidade do KDE organizam festas de lançamento por todo o mundo. Várias delas já foram agendadas e mais surgirão a seguir. Encontre <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">uma lista de festas aqui</a>. Qualquer um é bem vindo a se juntar! Haverá uma combinação de companhia interessante e conversas inspiradoras, bem como comida e bebida. Esta é uma grande oportunidade de aprender mais sobre o que está acontecendo no KDE, envolver-se ou apenas encontrar outros usuários e contribuidores.
</p>
<p>
Nós encorajamos às pessoas a organizar suas próprias festas. Eles serão divertidas de hospedar e abertas a todos. Verifique as <a href="http://community.kde.org/Promo/Events/Release_Parties">dicas de como organizar uma festa</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>Sobre este anúncio de lançamento</h2>
<p>
Este anúncio de lançamento foi preparado por Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson, outros membros da Equipe de Promoção do KDE e a comunidade KDE. Ele cobre as principais mudanças feitas no KDE nos últimos seis meses.
</p>

<h4>Apóie o KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">O Programa de Suporte de Membros</a> está
agora aberto.  Por &euro;25 por trimestre você pode certificar-se de
que a comunidade internacional do KDE continue a criar este
Software Livre de qualidade mundial.</p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
