<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Plasma Workspaces 4.9 – Core Component Improvements";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE is happy to announce the immediate availability of version 4.9 of both the Plasma Desktop and Plasma Netbook Workspaces. There have been improvements to existing Plasma Workspace functionality, and significant new features have been introduced as well.
</p>
<h2>Dolphin File Manager</h2>
<p>
KDE's powerful file manager Dolphin now includes back and forward buttons and in-line file renaming is back. Dolphin can show metadata such as ratings, tags, image and file sizes, author, date, and more as well as  grouping and sorting by metadata properties. The new Mercurial plugin handles this versioning system in the same convenient way git, SVN and CVS are supported, so users can do pulls, pushes and commits right from the file manager. The Dolphin User Interface has seen several smaller improvements, including a better Places panel, improved search support and synchronization with the terminal location.
<div align="center" class="screenshot">
<a href="screenshots/kde49-dolphin_.png"><img src="screenshots/kde49-dolphin_thumb.png" /></a>
</div>
</p>
<h2>Konsole X Terminal Emulator</h2>
<p>
The workhorse Konsole now has the ability to search for a text selection using KDE Web Shortcuts. It offers the 'Change Directory To' context option when a folder is dropped on the Konsole window. Users have more control for organizing terminal windows by <strong>detaching tabs</strong> and dragging them to create a new window with just that tab. Existing tabs can be cloned into new ones with the same profile. Visibility of the menu and tab bars can be controlled when starting Konsole. For those handy with scripting, tab titles can be changed through an escape sequence.
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole1.png"><img src="screenshots/kde49-konsole1-cropped.png" /></a></div>
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole2.png"><img src="screenshots/kde49-konsole2-cropped.png" /></a></div>
</p>
<h2>KWin Window Manager</h2>
<p>
KDE's Window Manager KWin has seen a lot of work. Improvements include subtle changes like raising windows during window switching and help for Window Specific Settings, as well as more visible changes such as an improved KCM for box switching and better performance with Wobbly Windows. There are changes to make KWin deal better with Activities, including the addition of Activity-related window rules. There has been a general focus on improving KWin quality and performance.
<div align="center" class="screenshot">
<a href="screenshots/kde49-window-behaviour_settings.png"><img src="screenshots/kde49-window-behaviour_settings_thumb.png" /></a></div>
</p>
<h2>Activities</h2>
<p>
Activities are now integrated more thoroughly throughout the Workspaces. Files can be linked to Activities in Dolphin, Konqueror and Folder View. Folder View can also show only those files related to an Activity on the desktop or in a panel. An Activity KIO is new, and encryption for private activities is now possible.
<div align="center" class="screenshot">
<a href="screenshots/kde49-link-files-to-activities.png"><img src="screenshots/kde49-link-files-to-activities-cropped.png"/></a></div>
</p>
<p>
Workspaces introduce MPRIS2 support, with KMix having the ability to handle streams and a Plasma data engine for handling this music player control protocol. These changes tie in with MPRIS2 support in Juk and Dragon, KDE's music and video player.
</p>
<p>
There are many smaller changes in Workspaces, including several QML ports. The improved Plasma miniplayer includes a track properties dialog and better filtering. The Kickoff menu can now be used with only a keyboard. The Network Management plasmoid has seen layout and usability work. The Public Transport widget has also seen considerable changes.
</p>

<h4>Installing Plasma</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.9"/>New and Improved KDE Applications 4.9</a></h2>
<p>
New and improved KDE Applications released today include Okular, Kopete, KDE PIM, educational applications and games. Read the complete <a href="applications.php">'KDE Applications Announcement'</a>
</p>
<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.9"/> KDE Platform 4.9</a></h2>
<p>
Today’s KDE Platform release includes bugfixes, other quality improvements, networking, and preparation for Frameworks 5
</p>

<?php
  include("footer.inc");
?>
