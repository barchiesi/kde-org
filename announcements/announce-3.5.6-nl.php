<?php

  $page_title = "Aankondiging KDE 3.5.6";
  $site_root = "../";
  include "header.inc";
?>

<!-- Other languages translations -->
Ook beschikbaar in:
<a href="announce-3.5.6.php">Engels</a>
<a href="announce-3.5.6-ca.php">Catalaans</a>
<a href="announce-3.5.6-es.php">Spaans</a>
<a href="http://fr.kde.org/announcements/announce-3.5.6-fr.php">Frans</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.6.php">IJslands</a>
<a href="announce-3.5.6-it.php">Italiaans</a>
<a href="announce-3.5.6-pt.php">Portugees</a>
<a href="announce-3.5.6-pt-br.php">Portugees (Brazilië)</a>
<a href="announce-3.5.6-sl.php">Sloveens</a>
<a href="announce-3.5.6-sv.php">Zweeds</a>

<h3 align="center">
   K DESKTOP ENVIRONMENT 3.5.6 uitgebracht
</h3>

<p align="justify">
  <strong>KDE 3.5.6 bevat vertalingen naar 65 talen, verbeteringen in de HTML-rendering (KHTML) en andere programma's.</strong>
</p>

<p align="justify">
25 januari 2006 (Het internet) - Het <a href="http://www.kde.org">KDE-project</a> heeft vandaag de onmiddelijke beschikbaarheid van KDE 3.5.6 aangekondigd, een onderhoudsuitgave voor de laatste generatie van de meest krachtige en geavanceerde en <em>vrije</em> desktop environment voor GNU/Linux en andere Unices. KDE is nu vertaald naar 65 talen, wat inhoudt dat het voor meer mensen beschikbaar zal zijn dan veel andere niet-vrije software. Het kan eenvoudig nog worden uitgebreid door degenen die aan het open source-project willen bijdragen.
</p>

<p align="justify">
Deze versie bevat verbeteringen in de HTML-rendering (KHTML), <a href="http://kate.kde.org">Kate</a>, Kicker, KSysGuard en vele andere programma's. Onder de grote wijzigingen valt de Compiz-ondersteuning van Kicker, sessiebeheer wat betreft tabbladen in <a href="http://akregator.kde.org">Akregator</a>, sjablonen voor berichten in <a href="kmail.kde.org">KMail</a> en nieuwe menu's in het overzicht van <a href="http://kontact.kde.org">Kontact</a> zodat u gemakkelijker kunt werken met afspraken en taken.
</p>

<p align="justify">
Voor een meer gedetailleerde lijst verbeteringen sinds de uitgave van <a href="http://www.kde.org/announcements/announce-3.5.5.php">KDE 3.5.5</a>, raadpleeg dan de <a href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php">KDE 3.5.6 Changelog</a>
</p>

<p align="justify">
  KDE 3.5.6 komt met een standaard bureaubladomgeving met daarbij vijftien andere modules (PIM, administratie, netwerk, educatie, hulpmiddelen, multimedia, spellen, artwork, webontikkeling en meer). Al deze bekroonde toepassingen zijn beschikbaar in <strong>65 talen</strong>.
</p>

<h4>
  Distributies die KDE leveren
</h4>
<p align="justify">
  De meeste Linux-distributies en UNIX-besturingssystemen leveren niet meteen de nieuwste uitgaven van KDE, maar zullen deze aanbieden in hun volgende uitgaven. Raadpleeg <a href="http://www.kde.org/download/distributions.php">deze lijst</a> voor distributies die KDE leveren.
</p>

<h4>
  Het installeren van binaire pakketten van KDE 3.5.6
</h4>
<p align="justify">
  <em>Makers pakketten</em>. Sommige distributeurs hebben binaire pakketten aangeleverd van KDE 3.5.6, of vrijwilligers hebben dat in andere gevallen gedaan. Enkele van deze binaire pakketten zijn beschikbaar op <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">http://download.kde.org</a>. Meer binaire pakketten en eventuele updates daarop zullen in de komende weken beschikbaar komen.
</p>

<p align="justify">
  <em>Pakketlocaties</em>. Voor een actuele lijst van de beschikbare binaire pakketten waarvan het KDE-project op de hoogte is, is te vinden op <a href="http://www.kde.org/info/3.5.6.php">de KDE 3.5.6 informatiepagina</a> (Engels).
</p>

<h4>
  KDE 3.5.6 compileren
</h4>
<p align="justify">
  <a name="source_code"></a>
  De complete broncode van KDE 3.5.6 kan onvoorwaardelijk <a href="http://download.kde.org/stable/3.5.6/src/">gedownload</a> worden. Instructies over het compilatie- en installatieproces kunnen gevonden worden op <a href="http://www.kde.org/info/3.5.6.php">de KDE 3.5.6 informatiepagina</a> (Engels).
</p>

<h4>
  KDE ondersteunen
</h4>
<p align="justify">
KDE is een <a href="http://www.gnu.org/philosophy/free-sw.html" target="_blank">vrij softwareproject</a> dat alleen kan bestaan en groeien dankzij de hulp van vele vrijwilligers die daar tijd en moeite in steken. KDE is altijd op zoek naar nieuwe vrijwilligers en bijdragen, of het nu gaat om programmeren, oplossen of rapporteren van bugs, het schrijven van documentatie, vertalingen, promotie, financieel, etc. Iedere bijdrage wordt erg gewaardeerd en geaccepteerd. Lees de <a href="http://www.kde.org/support" target="_blank">supportpagina</a> (Engels) door voor meer informatie.
</p>

<h4>
  Informatie over het KDE-projects
</h4>
<p align="justify">
  KDE is een vrij onafhankelijk project van honderden ontwikkelaars, vertalers, grafische ontwerpers en professionals die wereldwijd samenwerken via het Internet om een vrij verkrijgbare desktop- en kantoor-omgeving te creëren. Hierbij maakt men gebruik van een flexibele, componenten-gebaseerde, netwerktransparante architectuur waarbij KDE fungeert als het perfecte ontwikkelplatform.
</p>

<p align="justify">
  KDE biedt een stabiele en volwassen desktop aan met een moderne browser (<a href="http://konqueror.kde.org/">Konqueror</a>), een manager voor persoonlijke informatie (PIM, <a href="http://kontact.org/">Kontact</a>), een compleet officepakket (<a href="http://www.koffice.org/">KOffice</a>), een rijke verzameling van netwerktoepassingen en hulpmiddelen en een effici&#235;nte ontwikkelplatform met de IDE <a href="http://www.kdevelop.org">KDevelop</a>.
</p>

<p align="justify">
  KDE, gebaseerd op de Qt-technologie van Trolltech,  is het bewijs dat het Open Source software-model "Bazaar-style" een eersteklas technologie kan voortbrengen die gelijkwaardig - zoniet beter - is dan de meeste complexe commerci&euml;le software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Handelsmerken.</em>
  KDE<sup>&#174;</sup> en het logo van de K Desktop Environment<sup>&#174;</sup> zijn geregistreerde handelsmerken van KDE e.V.

  Linux is een geregistreerd handelsmerk van Linus Torvalds.

  UNIX is een geregistreerd handelsmerk van The Open Group in de Verenigde Staten en andere landen.

  Alle andere handelsmerken en copyrights die aangehaald zijn in deze aankondiging behoren tot hun respectievelijke eigenaren.
  </font>
</p>

<hr />

<h4>Perscontacten voor meer informatie</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Afrika</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
telefoon: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Azië</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     telefoon : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
telefoon: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Noord-Amerika</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
telefoon: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceani&#235;</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
telefoon: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Zuid-Amerika</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
telefoon: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<p>
oorspronkelijke versie verkrijgbaar op <a href="http://www.kde.nl/uitgaven/aankondiging_kde3.5.6.html">http://www.kde.nl</a>

<?php

  include("footer.inc");
?>
