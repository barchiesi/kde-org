<?php
  $page_title = "Anuncio de lanzamiento de KDE 3.5.5";
  $site_root = "../";
  include "header.inc";
?>

<p>Octobre 11, 2006</p>
<!-- <p>FOR IMMEDIATE RELEASE</p> -->

También disponible en:
<a href="announce-3.5.5-de.php">Alemán</a>
<a href="http://cat.kde.org/index.php/Notes_de_premsa/KDE_3.5.5">Catalán</a>
<a href="http://fr.kde.org/announcements/announce-3.5.5.php">Francés</a>
<a href="http://www.kde.nl/uitgaven/aankondiging_kde3.5.5.html">Holandés</a>
<a href="announce-3.5.5.php">Inglés</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.5.php">Islandés</a>
<a href="http://www.kde-it.org/index.php?option=com_content&task=view&id=49&Itemid=2">Italian</a>
<a href="announce-3.5.5-pt.php">Portugués</a>
<a href="announce-3.5.5-sv.php">Sueco</a>


<h3 align="center">
El proyecto KDE lanza la quinta versión que mejora las traducciones y servicios del escritorio líder de software libre.
</h3>

<p align="justify">
  <strong>
KDE 3.5.5 incluye traducciones a 65 idiomas, mejoras en la aplicación de mensajería instantánea y en el motor de renderizado HTML (KHTML).
  </strong>
</p>

<p align="justify">
11 de octubre de 2006 (INTERNET). El <a href="http://www.kde.org/">Proyecto KDE</a> anunció hoy la inmediata disponibilidad de KDE 3.5.5, una versión de mantenimiento para la última generación del más avanzado y potente escritorio <em>libre</em> para GNU/Linux y otros UNIXes. KDE ahora admite 65 idiomas, haciéndolo disponible a más gente que la mayoría del software no libre y puede ser fácilmente extendido para admitir otros idiomas por comunidades que deseen contribuir al proyecto de código abierto.
</p>

<p align="justify">
Mejoras significativas incluidas:
</p>

<ul>
  <li>
La versión 0.12.3 de <a href="http://kopete.kde.org/">Kopete</a> sustituye a la 0.11.3 en KDE 3.5.5, e incluye soporte para temas de <a href="http://www.adiumx.com/">Adium</a>, mejoras en el rendimiento y mejor soporte para los protocolos de <a href="http://messenger.yahoo.com/">Yahoo!</a> y <a href="http://www.jabber.org/">Jabber</a>.
  </li>
  <li>
    Soporte para sudo en kdesu.
  </li>
  <li>
    Soporte para entradas con forma con XShape1.1 en KWin (gestor de ventanas de KDE).
  </li>
  <li>
	Cantidad de optimizaciones de velocidad y correcciones en el motor HTML de <a href="http://www.konqueror.org">Konqueror</a>, KHTML.
  </li>
  <li>
    Soporte para <a href="http://www.cups.org/">CUPS</a> 1.2 en <a href="http://printing.kde.org/">KDEPrint</a>.
  </li>
  <li>
	Importantes mejoras en la cantidad de elementos del interfaz en las traducciones al
    <!--Big improvements in the number of translated interface elements in-->
    <a href="http://l10n.kde.org/stats/gui/stable/zh_TW/index.php">Chino tradicional</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/fa/index.php">Persa</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/km/index.php">Jémer</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/nds/index.php">Bajo Sajón</a> y
    <a href="http://l10n.kde.org/stats/gui/stable/sk/index.php">Eslovaco</a>.
  </li>
</ul>

<p align="justify">
Para ver una lista más detallada de las mejoras realizadas desde 
<a href="http://www.kde.org/announcements/announce-3.5.4.php"> el lanzamiento de KDE 3.5.4</a> el 2 de agosto de 2006, por favor visite el <a href="http://www.kde.org/announcements/changelogs/changelog3_5_4to3_5_5.php">registro de cambios de KDE 3.5.5</a>.
</p>

<p align="justify">
KDE 3.5.5 incluye un escritorio básico y otros quince paquetes (PIM, administración, red, educación, utilidades, multimedia, juegos, material gráfico, desarrollo web y más). Las premiadas herramientas de KDE están disponibles en <strong>65 idiomas</strong>.
</p>

<h4>
Distribuciones que incluyen KDE
</h4>
<p align="justify">
La mayor parte de las distribuciones de Linux y sistemas operativos UNIX no incorporan
de forma inmediata las nuevas versiones de KDE, pero integrarán KDE 3.5.5 en sus
próximas versiones. Compruebe <a href="http://www.kde.org/download/distributions.php">esta
lista</a> para ver qué distribuciones incluyen KDE.
</p>

<h4>
Instalando los paquetes binarios de KDE 3.5.5
</h4>
<p align="justify">
<em>Creadores de paquetes</em>.
Algunos distribuidores de sistemas operativos han proporcionado generosamente
paquetes binarios de KDE 3.5.5 para algunas versiones de su distribución, y en
otros casos comunidades de voluntarios lo han hecho.
Algunos de estos paquetes binarios están disponibles para su libre descarga
en el servidor de descargas de KDE en 
<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.5/">http://download.kde.org</a>.
Paquetes binarios adicionales, así como actualizaciones de los paquetes ahora
disponibles, pueden estar disponibles en las próximas semanas.
</p>

<p align="justify">
<a name="package_locations"><em>Localizaciones de paquetes</em></a>.
Para ver una lista actualizada de paquetes binarios de los que el Proyecto
KDE ha sido informado, por favor visite la <a href="/info/3.5.5.php">página
de información de KDE</a>.
</p>

<h4>
Compilando KDE 3.5.5
<!--  Compiling KDE 3.5.5-->
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código fuente</em>.
  El código fuente completo de KDE 3.5.5 puede ser
  <a href="http://download.kde.org/stable/3.5.5/src/">descargado libremente</a>.
  Hay disponibles instrucciones acerca de compilar e instalar KDE 3.5.5
  en la <a href="/info/3.5.5.php">página de información de KDE</a>.
</p>

<h4>
Ayudar a KDE
<!--  Supporting KDE-->
</h4>
<p align="justify">
KDE es un proyecto de <a href="http://www.gnu.org/philosophy/free-sw.html">software libre</a>
que existe y crece sólo mediante la ayuda de muchos voluntarios que donan su tiempo y esfuerzo.
KDE siempre está buscando nuevos voluntarios y contribuidores, bien para ayudar programando,
arreglando fallos, escribiendo documentación, traducciones, promocionándolo, donando dinero,
etc. Todos los contribuidores son gratamente apreciados y esperados con mucho entusiasmo.
Por favor, lea la página <a href="/community/donations/">Ayudando a KDE</a> para más información.
</p>

<p align="justify">
¡Esperamos noticias suyas pronto!
</p>

<h4>
  Acerca de KDE
</h4>
<p align="justify">
KDE es un <a href="/awards/">premiado</a> proyecto independiente formado por cientos de
desarrolladores, traductores, artistas y otros profesionales de todo el mundo, que colaboran
a través de Internet para crear y distribuir libremente un entorno de escritorio y oficina
sofisticado, personalizable y estable, basado en componentes, con arquitectura transparente
a la red y que ofrece una excepcional plataforma de desarrollo.

</p>

<p align="justify">
KDE ofrece un escritorio estable y maduro, incluyendo un navegador de última generación
(<a href="http://konqueror.kde.org/">Konqueror</a>), una <i>suite</i> de gestión de 
información personal (<a href="http://kontact.org/">Kontact</a>), una completa <i>suite</i>
ofimática (<a href="http://www.koffice.org/">KOffice</a>), un gran conjunto de aplicaciones
de red y utilidades, y un entorno de desarrollo eficiente e intuitivo que incluye el
excelente IDE <a href="http://www.kdevelop.org/">KDevelop</a>.

</p>

<p align="justify">
KDE es una prueba en funcionamiento de que el modelo de desarrollo "estilo bazar" del
código abierto puede producir tecnologías de primer nivel, iguales o superiores al más
complejo software comercial.

</p>

<hr />

<p align="justify">
  <font size="2">
<em>Marcas registradas.</em>
KDE<sup>&#174;</sup> y el K Desktop Environment<sup>&#174;</sup> son marcas registradas de
KDE e.V.

Linux es una marca registrada de Linus Torvalds.

UNIX es una marca registrada de The Open Group en los Estados Unidos y otros países.

Todas las otras marcas registradas y copyrights mencionados en este anuncio son propiedad
de sus respectivos dueños.
  </font>
</p>

<hr />

<h4>Contactos de prensa</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia e India</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Suecia<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Norteamérica</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canadá<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceanía</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Sudamérica</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brasil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
