<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.15.0");
  $site_root = "../";
  $release = '5.15.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
October 10, 2015. KDE today announces the release
of KDE Frameworks 5.15.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix limit/offset handling in SearchStore::exec");?></li>
<li><?php i18n("Recreate the baloo index");?></li>
<li><?php i18n("balooctl config: add options to set/view onlyBasicIndexing");?></li>
<li><?php i18n("Port balooctl check to work with new architecture (bug 353011)");?></li>
<li><?php i18n("FileContentIndexer: fix emitting filePath twice");?></li>
<li><?php i18n("UnindexedFileIterator: mtime is quint32 not quint64");?></li>
<li><?php i18n("Transaction: fix another Dbi typo");?></li>
<li><?php i18n("Transaction: Fix documentMTime() and documentCTime() using wrong Dbis.");?></li>
<li><?php i18n("Transaction::checkPostingDbInTermsDb: Optimize code");?></li>
<li><?php i18n("Fix dbus warnings");?></li>
<li><?php i18n("Balooctl: Add checkDb command");?></li>
<li><?php i18n("balooctl config: Add \"exclude filter\"");?></li>
<li><?php i18n("KF5Baloo: Make sure D-Bus interfaces are generated before they are used. (bug 353308)");?></li>
<li><?php i18n("Avoid using QByteArray::fromRawData");?></li>
<li><?php i18n("Remove baloo-monitor from baloo");?></li>
<li><?php i18n("TagListJob: Emit error when failed to open database");?></li>
<li><?php i18n("Do not ignore subterms if not found");?></li>
<li><?php i18n("Cleaner code for failing Baloo::File::load() on DB open fail.");?></li>
<li><?php i18n("Make balooctl use IndexerConfig instead of manipulating baloofilerc directly");?></li>
<li><?php i18n("Improve i18n for balooshow");?></li>
<li><?php i18n("Make balooshow fail gracefully if database cannot be opened.");?></li>
<li><?php i18n("Fail Baloo::File::load() if the Database is not open. (bug 353049)");?></li>
<li><?php i18n("IndexerConfig: add refresh() method");?></li>
<li><?php i18n("inotify: Do not simulate a  closedWrite event after move without cookie");?></li>
<li><?php i18n("ExtractorProcess: Remove the extra \n at the end of the filePath");?></li>
<li><?php i18n("baloo_file_extractor: call QProcess::close before destroying the QProcess");?></li>
<li><?php i18n("baloomonitorplugin/balooctl: i18nize indexer state.");?></li>
<li><?php i18n("BalooCtl: Add a 'config' option");?></li>
<li><?php i18n("Make baloosearch more presentable");?></li>
<li><?php i18n("Remove empty EventMonitor files");?></li>
<li><?php i18n("BalooShow: Show more information when the ids do not match");?></li>
<li><?php i18n("BalooShow: When called with an id check if the id is correct");?></li>
<li><?php i18n("Add a FileInfo class");?></li>
<li><?php i18n("Add error checking in various bits so that Baloo doesn't crash when disabled. (bug 352454)");?></li>
<li><?php i18n("Fix Baloo not respecting \"basic indexing only\" config option");?></li>
<li><?php i18n("Monitor: Fetch remaining time on startup");?></li>
<li><?php i18n("Use actual method calls in MainAdaptor instead of QMetaObject::invokeMethod");?></li>
<li><?php i18n("Add org.kde.baloo interface to root object for backward compatibility");?></li>
<li><?php i18n("Fix date string displayed in address bar due to porting to QDate");?></li>
<li><?php i18n("Add delay after each file instead of each batch");?></li>
<li><?php i18n("Remove Qt::Widgets dependency from baloo_file");?></li>
<li><?php i18n("Remove unused code from baloo_file_extractor");?></li>
<li><?php i18n("Add baloo monitor or experimental qml plugin");?></li>
<li><?php i18n("Make \"querying for remaining time\" thread safe");?></li>
<li><?php i18n("kioslaves: Add missing override for virtual functions");?></li>
<li><?php i18n("Extractor: Set the applicationData after constructing the app");?></li>
<li><?php i18n("Query: Implement support for 'offset'");?></li>
<li><?php i18n("Balooctl: Add --version and --help (bug 351645)");?></li>
<li><?php i18n("Remove KAuth support to increase max inotify watches if count too low (bug 351602)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Fix fakebluez crash in obexmanagertest with ASAN");?></li>
<li><?php i18n("Forward declare all exported classes in types.h");?></li>
<li><?php i18n("ObexTransfer: Set error when transfer session is removed");?></li>
<li><?php i18n("Utils: Hold pointers to managers instances");?></li>
<li><?php i18n("ObexTransfer: Set error when org.bluez.obex crashes");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Update GTK icon cache when installing icons.");?></li>
<li><?php i18n("Remove workaround to delay execution on Android");?></li>
<li><?php i18n("ECMEnableSanitizers: The undefined sanitizer is supported by gcc 4.9");?></li>
<li><?php i18n("Disable X11,XCB etc. detection on OS X");?></li>
<li><?php i18n("Look for the files in the installed prefix rather the prefix path");?></li>
<li><?php i18n("Use Qt5 to specify what's Qt5 installation prefix");?></li>
<li><?php i18n("Add definition ANDROID as needed in qsystemdetection.h.");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Fix random file dialog not showing up problem. (bug 350758)");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Using a custom matching function instead of sqlite's glob. (bug 352574)");?></li>
<li><?php i18n("Fixed problem with adding a new resource to the model");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Fix crash in UnicodeGroupProber::HandleData with short strings");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Mark kconfig-compiler as non-gui tool");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KShell::splitArgs: only ASCII space is a separator, not unicode space U+3000 (bug 345140)");?></li>
<li><?php i18n("KDirWatch: fix crash when a global static destructor uses KDirWatch::self() (bug 353080)");?></li>
<li><?php i18n("Fix crash when KDirWatch is used in Q_GLOBAL_STATIC.");?></li>
<li><?php i18n("KDirWatch: fix thread safety");?></li>
<li><?php i18n("Clarify how to set KAboutData constructor arguments.");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("KCrash: pass cwd to kdeinit when auto-restarting the app via kdeinit. (bug 337760)");?></li>
<li><?php i18n("Add KCrash::initialize() so that apps and the platform plugin can explicitly enable KCrash.");?></li>
<li><?php i18n("Disable ASAN if enabled");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Small improvements in ColumnProxyModel");?></li>
<li><?php i18n("Make it possible for applications to know path to homeDir");?></li>
<li><?php i18n("move EventForge from the desktop containment");?></li>
<li><?php i18n("Provide enabled property for QIconItem.");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("kded: simplify logic around sycoca; just call ensureCacheValid.");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Call newInstance from the child on first invocation");?></li>
<li><?php i18n("Use kdewin defines.");?></li>
<li><?php i18n("Don't try to find X11 on WIN32");?></li>
<li><?php i18n("cmake: Fix taglib version check in FindTaglib.cmake.");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Qt moc can't handle macros (QT_VERSION_CHECK)");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("kWarning -&gt; qWarning");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("implement windows usermetadata");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Not looking for X11/XCB makes sense also for WIN32");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Replace std::auto_ptr with std::unique_ptr");?></li>
<li><?php i18n("khtml-filter: Discard rules containing special adblock features that we do not handle yet.");?></li>
<li><?php i18n("khtml-filter: Code reorder, no functional changes.");?></li>
<li><?php i18n("khtml-filter: Ignore regexp with options as we do not support them.");?></li>
<li><?php i18n("khtml-filter: Fix detection of adblock options delimiter.");?></li>
<li><?php i18n("khtml-filter: Clean up from trailing white spaces.");?></li>
<li><?php i18n("khtml-filter: Do not discard lines starting with '&amp;' as it is not a special adblock char.");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("remove strict iterators for msvc to make ki18n build");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KFileWidget: parent argument should default to 0 like in all widgets.");?></li>
<li><?php i18n("Make sure the size of the byte array we just dumped into the struct is big enough before calculating the targetInfo, otherwise we're accessing memory that doesn't belong to us");?></li>
<li><?php i18n("Fix Qurl usage when calling QFileDialog::getExistingDirectory()");?></li>
<li><?php i18n("Refresh Solid's device list before querying in kio_trash");?></li>
<li><?php i18n("Allow trash: in addition to trash:/ as url for listDir (calls listRoot) (bug 353181)");?></li>
<li><?php i18n("KProtocolManager: fix deadlock when using EnvVarProxy. (bug 350890)");?></li>
<li><?php i18n("Don't try to find X11 on WIN32");?></li>
<li><?php i18n("KBuildSycocaProgressDialog: use Qt's builtin busy indicator. (bug 158672)");?></li>
<li><?php i18n("KBuildSycocaProgressDialog: run kbuildsycoca5 with QProcess.");?></li>
<li><?php i18n("KPropertiesDialog: fix for ~/.local being a symlink, compare canonical paths");?></li>
<li><?php i18n("Add support for network shares in kio_trash (bug 177023)");?></li>
<li><?php i18n("Connect to the signals of QDialogButtonBox, not QDialog (bug 352770)");?></li>
<li><?php i18n("Cookies KCM: update DBus names for kded5");?></li>
<li><?php i18n("Use JSON files directly instead of kcoreaddons_desktop_to_json()");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't send notification update signal twice");?></li>
<li><?php i18n("Reparse notification config only when it changed");?></li>
<li><?php i18n("Don't try to find X11 on WIN32");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Change method for loading defaults");?></li>
<li><?php i18n("Send the appname whose config was updated along with the DBus signal");?></li>
<li><?php i18n("Add method to revert kconfigwidget to defaults");?></li>
<li><?php i18n("Don't sync the config n times when saving");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Use largest timestamp in subdirectory as resource directory timestamp.");?></li>
<li><?php i18n("KSycoca: store mtime for every source dir, to detect changes. (bug 353036)");?></li>
<li><?php i18n("KServiceTypeProfile: remove unnecessary factory creation. (bug 353360)");?></li>
<li><?php i18n("Simplify and speed up KServiceTest::initTestCase.");?></li>
<li><?php i18n("make install name of applications.menu file a cached cmake variable");?></li>
<li><?php i18n("KSycoca: ensureCacheValid() should create the db if it doesn't exist");?></li>
<li><?php i18n("KSycoca: make global database work after the recent timestamp check code");?></li>
<li><?php i18n("KSycoca: change DB filename to include language and sha1 of the dirs it's built from.");?></li>
<li><?php i18n("KSycoca: make ensureCacheValid() part of the public API.");?></li>
<li><?php i18n("KSycoca: add a q pointer to remove more singleton usage");?></li>
<li><?php i18n("KSycoca: remove all self() methods for factories, store them in KSycoca instead.");?></li>
<li><?php i18n("KBuildSycoca: remove writing of the ksycoca5stamp file.");?></li>
<li><?php i18n("KBuildSycoca: use qCWarning rather than fprintf(stderr, ...) or qWarning");?></li>
<li><?php i18n("KSycoca: rebuild ksycoca in process rather than executing kbuildsycoca5");?></li>
<li><?php i18n("KSycoca: move all of the kbuildsycoca code into the lib, except for main().");?></li>
<li><?php i18n("KSycoca optimization: only watch the file if the app connects to databaseChanged()");?></li>
<li><?php i18n("Fix memory leaks in the KBuildSycoca class");?></li>
<li><?php i18n("KSycoca: replace DBus notification with file watching using KDirWatch.");?></li>
<li><?php i18n("kbuildsycoca: deprecate option --nosignal.");?></li>
<li><?php i18n("KBuildSycoca: replace dbus-based locking with a lock file.");?></li>
<li><?php i18n("Do not crash when encountering invalid plugin info.");?></li>
<li><?php i18n("Rename headers to _p.h in preparation for move to kservice library.");?></li>
<li><?php i18n("Move checkGlobalHeader() within KBuildSycoca::recreate().");?></li>
<li><?php i18n("Remove code for --checkstamps and --nocheckfiles.");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("validate more regexp");?></li>
<li><?php i18n("fix regexps in HL files (bug 352662)");?></li>
<li><?php i18n("sync ocaml HL with state of https://code.google.com/p/vincent-hugot-projects/ before google code is down, some small bugfixes");?></li>
<li><?php i18n("add word-break (bug 352258)");?></li>
<li><?php i18n("validate line before calling folding stuff (bug 339894)");?></li>
<li><?php i18n("Fix Kate word count issues by listening to DocumentPrivate instead of Document (bug 353258)");?></li>
<li><?php i18n("Update Kconfig syntax highlighting: add new operators from Linux 4.2");?></li>
<li><?php i18n("sync w/ KDE/4.14 kate branch");?></li>
<li><?php i18n("minimap: Fix scrollbar handle not being drawn with scrollmarks off. (bug 352641)");?></li>
<li><?php i18n("syntax: Add git-user option for kdesrc-buildrc");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("No longer automatically close on last use");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix warning C4138 (MSVC): '*/' found outside of comment");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Perform deep copy of QByteArray get_stringlist_reply");?></li>
<li><?php i18n("Allow interacting with multiple X servers in the NETWM classes.");?></li>
<li><?php i18n("[xcb] Consider mods in KKeyServer as initialized on platform != x11");?></li>
<li><?php i18n("Change KKeyserver (x11) to categorized logging");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Make it possible to import/export shortcut schemes symmetrically");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Fix introspections, LastSeen should be in AccessPoint and not in ActiveConnection");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Make tooltip dialog hidden on the cursor entering the inactive ToolTipArea");?></li>
<li><?php i18n("if the desktop file has Icon=/foo.svgz use that file from package");?></li>
<li><?php i18n("add a \"screenshot\" file type in packages");?></li>
<li><?php i18n("consider devicepixelration in standalone scrollbar");?></li>
<li><?php i18n("no hover effect on touchscreen+mobile");?></li>
<li><?php i18n("Use lineedit svg margins in sizeHint calculation");?></li>
<li><?php i18n("Don't fade animate icon in plasma tooltips");?></li>
<li><?php i18n("Fix eliding button text");?></li>
<li><?php i18n("Context menus of applets within a panel no longer overlap the applet");?></li>
<li><?php i18n("Simplify getting associated apps list in AssociatedApplicationManager");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix hunspell plugin ID for proper loading");?></li>
<li><?php i18n("support static compilation on windows, add windows libreoffice hunspell dict path");?></li>
<li><?php i18n("Do not assume UTF-8 encoded Hunspell dictionaries. (bug 353133)");?></li>
<li><?php i18n("fix Highlighter::setCurrentLanguage() for the case when previous language was invalid (bug 349151)");?></li>
<li><?php i18n("support /usr/share/hunspell as dict location");?></li>
<li><?php i18n("NSSpellChecker-based plugin");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.15");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
