<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.18.3 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.18.3";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/bluedevil/e0e0c50a92d15e5d27ec66f5db7103ebe67bf2f7'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Flatpak: make sure we don't issue new queries after cancelling. <a href='https://commits.kde.org/discover/d55a38c935355c3b3e6ee8d6a1bad078113094d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404622'>#404622</a></li>
<li>Flatpak: prevent crash. <a href='https://commits.kde.org/discover/6af2c5b1fe40494a55423e9da3210bf816ef90de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418308'>#418308</a></li>
<li>Pk: only refetch update details once. <a href='https://commits.kde.org/discover/451605085006a18a7bf6503e9f84c52ec2545194'>Commit.</a> </li>
<li>Pk: only notify about state changes when state changes. <a href='https://commits.kde.org/discover/b9cbec73b23350974b2578660ebc68b9f1eb3a91'>Commit.</a> </li>
<li>Port away from own weird Qt class. <a href='https://commits.kde.org/discover/1ea61ddbb84917bb3941ef5e2dfe2d2796051421'>Commit.</a> </li>
<li>FeaturedModel: fix how we deal with errors. <a href='https://commits.kde.org/discover/24958506b7be5cd9cde237b6097402059d28dc16'>Commit.</a> </li>
<li>Remove unneeded declarations. <a href='https://commits.kde.org/discover/380b2689f7bc28bad484365603d2c6a38399a205'>Commit.</a> </li>
<li>Simplify Discover load. <a href='https://commits.kde.org/discover/664f2f76a01b577c634a89bae6a53d66278a672f'>Commit.</a> </li>
<li>Make sure we don't crash. <a href='https://commits.kde.org/discover/1b0992a5375f2243d1c8fdb2ac5efdb991d619bd'>Commit.</a> </li>
<li>Flatpak: build with older libflatpaks. <a href='https://commits.kde.org/discover/5cae00d1dbd94a584c9c63f7ff7fb5f893b228e4'>Commit.</a> </li>
<li>Flatpak: include more information on the transaction error message. <a href='https://commits.kde.org/discover/f6426073bc09757395dd3325a0185a7a9c5767fd'>Commit.</a> See bug <a href='https://bugs.kde.org/417078'>#417078</a></li>
<li>Flatpak: forgot to commit this method. <a href='https://commits.kde.org/discover/da6e56f8d0ef5787d00a7ccde42da2b15697bff5'>Commit.</a> </li>
<li>Flatpak: show the right icon for the source. <a href='https://commits.kde.org/discover/6ad0f3224844ea31351015ea4a085396d28cd57d'>Commit.</a> </li>
<li>Reviews: expose the reviewed package version on the delegate. <a href='https://commits.kde.org/discover/d58668698f5fd7556e9227fedd3318c22ecb868a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417675'>#417675</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27554'>D27554</a></li>
<li>Flatpak: simplify code. <a href='https://commits.kde.org/discover/2b90e06c69630be4abb55aed85ba83e204418c8c'>Commit.</a> </li>
<li>Polish the "removing this repo will uninstall these apps" message. <a href='https://commits.kde.org/discover/7c6405dc8bc585d615db686733d6774cf202b008'>Commit.</a> </li>
<li>Flatpak: improve the list of resources to remove toghether with the source. <a href='https://commits.kde.org/discover/db3cbccd15351671ed73482499119512aabb2527'>Commit.</a> </li>
<li>Improve how we report errors in removing sources. <a href='https://commits.kde.org/discover/25c0d5f7f3076c7742247ed070cd17e25ff254bd'>Commit.</a> </li>
<li>Flatpak: make it easier to remove a remote in use. <a href='https://commits.kde.org/discover/0937ac6e335b10cfa5f09c2d91fe3453a33a96cd'>Commit.</a> </li>
<li>Port QProcess::startDetached deprecated method. <a href='https://commits.kde.org/discover/d41ee4d88ba9df82522d939c072e663261547496'>Commit.</a> </li>
<li>Pk: simplify initialisation. <a href='https://commits.kde.org/discover/6f7f3c266658dad7579e2f6afcca51bba16fd39f'>Commit.</a> </li>
<li>Make status label on Updates page consistent. <a href='https://commits.kde.org/discover/41eefd8c799ce613abc9a61ae2e893301061ec0e'>Commit.</a> </li>
<li>Fix build by removing unintentionally-added non-public header include. <a href='https://commits.kde.org/discover/caad11b5a9b4d5a9fbc3b1c354e7268859526312'>Commit.</a> </li>
<li>Emit properties only when they change. <a href='https://commits.kde.org/discover/a48a3d7de72c12acda0d7706cf59e7b04570f8df'>Commit.</a> </li>
<li>Remove deprecated constructs. <a href='https://commits.kde.org/discover/22535a90b7137efa6a4b1381cd62395ae4f27c31'>Commit.</a> </li>
<li>Port Application Page's form layout to Kirigami FormLayout. <a href='https://commits.kde.org/discover/40742348cf052310bb74fceaec33f906ee0e4f26'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27183'>D27183</a></li>
<li>Reply->error()  is deprecated in qt5.15. <a href='https://commits.kde.org/discover/ea0a8ac949767db0b551023c5cafaee01f6b3768'>Commit.</a> </li>
<li>Change notification button text to "View Updates". <a href='https://commits.kde.org/discover/7126e11498ac6948ff3dca3f7b2c70b39de436de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26889'>D26889</a></li>
<li>Hopefully fix neon builds. <a href='https://commits.kde.org/discover/fe3a5c7c77908be1307692c9327f04adf1cd2275'>Commit.</a> </li>
<li>Flatpaknotifier: only refresh the right installation when it gets modified. <a href='https://commits.kde.org/discover/68aa2b1fd4922de6bdb3abed35b7eac7d8124337'>Commit.</a> </li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Fix windows compilation, QString.sprintf() is deprecated. <a href='https://commits.kde.org/drkonqi/9489b933e5bc76fc7bb4ac9dc072a06d387e14d0'>Commit.</a> </li>
<li>Focus email input line by default. <a href='https://commits.kde.org/drkonqi/ed33c2803511c2f9d6cb5d125b4be79821e903dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418309'>#418309</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27776'>D27776</a></li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Set correct version. <a href='https://commits.kde.org/kactivitymanagerd/e15056dfaa30f1fb5ece8acc94ddf206754809d5'>Commit.</a> </li>
<li>KF5 only deprecate up to target frameworks. <a href='https://commits.kde.org/kactivitymanagerd/c59d40946df8aaa6a42c66746b50864ebf11a4ea'>Commit.</a> </li>
</ul>


<h3><a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/kdecoration/3edddf86bc523078080988a1387b2aa2a35b023a'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[applets/notes] Fix drag&drop links and cursor shape regression. <a href='https://commits.kde.org/kdeplasma-addons/b03539a0d8d3b9e5a7c580c926c618bde0dbe374'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417953'>#417953</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27673'>D27673</a></li>
</ul>


<h3><a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/kgamma5/19e34eb93250f2fe81700a137a0898218356d518'>Commit.</a> </li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/kmenuedit/0d273e5d9efd44b602195ed7b8de0920c3e5df33'>Commit.</a> </li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/ksshaskpass/24e38756f73cae6e74faf8ead96122ffb369027a'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Linux/cpuinfo.c: grow buffer size as needed for 12+ core CPUs. <a href='https://commits.kde.org/ksysguard/031bd7603e39dad609a597c64ff66fbec418d999'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384515'>#384515</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27362'>D27362</a></li>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/ksysguard/8418095b5f308e62d2c2acd492a74c1f0c205a5c'>Commit.</a> </li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/kwallet-pam/bdd699d7998208340975ddfbd6a96ee2b4a8ee59'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Notify about decorations button order change. <a href='https://commits.kde.org/kwin/76c3174b019fe000f273f68df039088721f709e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27904'>D27904</a></li>
</ul>


<h3><a name='kwrited' href='https://commits.kde.org/kwrited'>kwrited</a> </h3>
<ul id='ulkwrited' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/kwrited/51c44e343f20da3eea2773b66cd89366c9541332'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix: handle when backend fails to load/initialize. <a href='https://commits.kde.org/libkscreen/ff98585ea5541012b68604e34b7fec383a487cd9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27625'>D27625</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/libksysguard/fcb50353aa33b14e61f119ff8dd663597ed2bdcb'>Commit.</a> </li>
<li>Only link to Qt5WebChannel if Qt5WebEngineWidgets available. <a href='https://commits.kde.org/libksysguard/ba7f78716af618db5556fc17e421397fe67e96af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27347'>D27347</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[applets/taskmanager] Show PA-related features even when audio indicators are disabled. <a href='https://commits.kde.org/plasma-desktop/3b2e4ddaa01ad067924f4efe0380bac1445232fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418164'>#418164</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27684'>D27684</a></li>
<li>[emojier] Set minimum window height and width. <a href='https://commits.kde.org/plasma-desktop/d0247bf5695413f8de83e5c48e4b05874d077752'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418195'>#418195</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27679'>D27679</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/plasma-integration/15812fb26dbd99343b3ddbbce031248ef6ec1282'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>[kded] Remove html tags from password dialog. <a href='https://commits.kde.org/plasma-nm/afc230d55362d3be52ac2d96701dfda3f604a635'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418625'>#418625</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>[KCM]Fix content below scrollbars. <a href='https://commits.kde.org/plasma-pa/552b0384ffd205a148df62239058cee9a4a6702e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416331'>#416331</a>. Fixes bug <a href='https://bugs.kde.org/417447'>#417447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27577'>D27577</a></li>
<li>[KCM]Set implicitWidth for main page. <a href='https://commits.kde.org/plasma-pa/8dea974eacf09855a8ba2a0a87b1b5e9f90becb4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27648'>D27648</a></li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/plasma-vault/131245f5c2c4b36a619fef462da07596cd51ce7f'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[gmenu-dbusmenu-proxy] Fix radio button state detection. <a href='https://commits.kde.org/plasma-workspace/ea358c896668a5c74ed8c405358c919e94adc8d7'>Commit.</a> See bug <a href='https://bugs.kde.org/418385'>#418385</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27885'>D27885</a></li>
<li>[gmenu-dbusmenu-proxy] Pass action "target" in invocation. <a href='https://commits.kde.org/plasma-workspace/503bf4f3a54267e22592c3e0ee246c20a2485a3d'>Commit.</a> See bug <a href='https://bugs.kde.org/418385'>#418385</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27884'>D27884</a></li>
<li>Degrade qCInfo to qCDebug. <a href='https://commits.kde.org/plasma-workspace/9a4f259c48205bf1137c829a899a1a6153bad01f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27879'>D27879</a></li>
<li>Demote jump list actions to PossibleMatches. <a href='https://commits.kde.org/plasma-workspace/18422ededcc9ec8f3e0b80e75228fe7b4a487829'>Commit.</a> See bug <a href='https://bugs.kde.org/418529'>#418529</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27886'>D27886</a></li>
<li>ItemContainer: disconnect signals in destructor. <a href='https://commits.kde.org/plasma-workspace/8f0da90f18ffa69efdc485f137096f36aedb3909'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417603'>#417603</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27650'>D27650</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/plymouth-kcm/0d2e9d064d25585c3ebb8ecccfc0dc9d9740aeae'>Commit.</a> </li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/polkit-kde-agent-1/6b3a6ebaff63ae2e1dc1597a18edcd56759e2ed8'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/sddm-kcm/49b5511cab6bebcddb091e2257975cb9b15f2bfe'>Commit.</a> </li>
<li>Align wallpaper dialog button to bottom. <a href='https://commits.kde.org/sddm-kcm/541d8adc5f14b8b5ab694d184ea0a8d82cfce6a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418173'>#418173</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27667'>D27667</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Fix connection to invalid signal. <a href='https://commits.kde.org/systemsettings/ead61e26ab7843f47ca932b8d22696b35125ad04'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27686'>D27686</a></li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/user-manager/61ad7f492297defa6a28d8b674979b25228e4bbd'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Use content_type as fallback in appchooser dialog. <a href='https://commits.kde.org/xdg-desktop-portal-kde/58af54b91de9fe4ab383c31c94ccd136cd7d6f06'>Commit.</a> </li>
<li>Only deprecate up to target frameworks. <a href='https://commits.kde.org/xdg-desktop-portal-kde/c21d6340a8401b9046e236dde18ec71863438604'>Commit.</a> </li>
<li>FileChooser: do not return empty file list when no local file is selected. <a href='https://commits.kde.org/xdg-desktop-portal-kde/4d4ff532be8b94634e3878bf2011a082a17bbe9b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418155'>#418155</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
