<?php
  $page_title = "KDE 2.2-beta1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE JULY 4, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Beta Released for Linux Desktop</H3>
<P><STRONG>KDE Ships Beta of Leading Desktop with Advanced Web Browser, Anti-Aliased Font Capabilities for Linux and Other UNIXes</STRONG></P>
<P>July 4, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of KDE 2.2beta1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <A HREF="http://konqueror.kde.org/">Konqueror</A>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
an advanced IDE, as a central component of KDE's powerful
development environment.  KDE 2.2beta1 completely integrates the
<A HREF="http://www.xfree.org/4.1.0/fonts5.html#30">XFree anti-aliased
font extensions</A> and can provide
a fully anti-aliased font-enabled desktop.
</P>
<P>
The primary goals of this release, which follows two months after the release
of KDE 2.1.2, are to provide a preview of KDE 2.2 and to involve
users and developers who wish to request/implement missing features or
identify problems. Code development is currently focused on stabilizing
KDE 2.2, scheduled for final release later this quarter.
Despite all the improvements, KDE 2.2 will be binary compatible with KDE 2.0.
</P>
<P>
Major changes to KDE since the last stable release are
<A HREF="#changelog">enumerated below</A>.  In addition, a
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">more
thorough list of changes</A> since the KDE 2.1.1 release,
<!-- NOT READY YET
and a <A HREF="http://www.kde.org/info/2.2beta1.html">FAQ about the release</A>, are
-->
as well as <A HREF="http://www.kde.com/Information/Fonts/index.php">information
on using anti-aliased fonts</A> with KDE, are available at the KDE
<A HREF="http://www.kde.org/">website</A>.
</P>
<P>
KDE 2.2beta1 and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/unstable/2.2beta1/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> (including
many <A HREF="#Package_Locations">precompiled packages</A>) and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
As a result of the dedicated efforts of hundreds of translators,
KDE 2.2beta1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">38 languages and
dialects</A>.  KDE 2.2beta1 ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (addons, administration, artwork, bindings, games,
graphics, multimedia, SDK, network, PIM and utilities).
</P>
<P>
<A NAME="changelog"></A><H4>Incremental Changelog</H4>
</P>
<P>
The following are the major improvements, enhancements and fixes since the
KDE 2.1 release earlier this year:
<UL>
<LI>KDE has added a new plugin-based printing framework, which features:
<UL>
  <LI>support for <A HREF="http://cups.sourceforge.net/">CUPS</A>,
      lpr and rlpr, though support for other printing systems can be
      easily added;</LI>
  <LI>a Control Center module for managing printers
      (add/remove/enable/disable/configure), print servers (currently
      only CUPS), and print jobs (cancel/hold/move);</LI>
  <LI>a configurable filter mechanism (for setting number of pages per
      sheet, etc.);</LI>
  <LI>a print job viewer for the KDE panel's system tray; and</LI>
  <LI>support for configurable "pseudo-printers", such as fax machines,
      email, etc.;</LI>
</UL>
<LI>Konqueror, the KDE file manager and web browser, sports a number of
    new features:</LI>
<UL>
  <LI>HTML and JavaScript handling has been improved and made faster;</LI>
  <LI>Ability to stop animated images;</LI>
  <LI>New file previews, including PDF, PostScript, and sound files;</LI>
  <LI>New "Send File" and "Send Link" options in the <TT>File</TT>menu;</LI>
  <LI>Added a number of new plugins:</LI>
  <UL>
    <LI>A web archiver for downloading and saving an entire web page, including
        images, in an archive for offline viewing;</LI>
    <LI><A HREF="http://babelfish.altavista.com/">Babelfish</A>
        translation of web pages;</LI>
    <LI>A directory filter for displaying only specified mimetypes (such as
        images);</LI>
    <LI>A quick User Agent changer to get Konqueror to work with websites
        that discriminate based on the browser you are using;</LI>
    <LI>An HTML validator using <A HREF="http://www.w3c.org/">W3C</A> to
        validate the XML/HTML of a webpage (useful for web developers);
        and</LI>
    <LI>A DOM tree-viewer for viewing the DOM structure of a web page (useful
        for web developers);</LI>
  </UL>
  <LI>New configuration for user-defined CSS stylesheets;</LI>
  <LI>Saving toolbar layout in the profile;</LI>
  <LI>A new "Most Often Visited" URL in the <TT>Go</TT> menu; and</LI>
  <LI>Many other enhancements, usability improvements and bug fixes.</LI>
</UL>
<LI>KDevelop, the KDE IDE, offers a number of new features:</LI>
<UL>
  <LI>Enhanced user interface with an MDI structure, which supports multiple
      views of the same file;</LI>
  <LI>Added new templates for implementing a KDE/Qt style library and Control
      Center modules;</LI>
  <LI>Updated the kde-common/admin copy (admin.tar.gz); and</LI>
  <LI>Extended the user manual to reflect the new GUI layout and added
      a chapter for using Qt Designer with KDevelop projects;</LI>
</UL>
<LI>KMail, the KDE mail client, has a number of improvements:</LI>
<UL>
  <LI>Added support for IMAP mail servers;</LI>
  <LI>Added support for SSL and TSL for POP3 mail servers;</LI>
  <LI>Added configuration of SASL and APOP authentication;</LI>
  <LI>Made mail-sending non-blocking;</LI>
  <LI>Improved performance for very large folders;</LI>
  <LI>Added message scoring;</LI>
  <LI>Improved the filter dialog and implemented automatic filter
      creation;</LI>
  <LI>Implemented quoting only selected parts of an email on a reply;</LI>
  <LI>Implemented forwarding emails as attachments; and</LI>
  <LI>Added support for multiple PGP (encryption) identities;</LI>
</UL>
<LI>New Control Center modules:</LI>
<UL>
  <LI>Listing USB information (attached devices);</LI>
  <LI>Configuring window manager decoration;</LI>
  <LI>Configuring application startup notification;</LI>
  <LI>Configuring user-defined CSS stylesheets;</LI>
  <LI>Configuring automatic audio-CD ripping (MP3, Ogg Vorbis); and</LI>
  <LI>Configuring key bindings;</LI>
</UL>
<LI>Added Kandy, a synchronization tool for mobile phones and the KDE address
    book, and improved KPilot address book synchronization;</LI>
<LI>KOrganizer, the KDE personal organizer, has a number of improvements:</LI>
<UL>
  <LI>Added a "What's Next" view;</LI>
  <LI>Added a journal feature;</LI>
  <LI>Switched to using the industry-standard iCalendar as the default file
      format;</LI>
  <LI>Added remote calendar support; and</LI>
  <LI>Added ability to send events using KMail, the KDE mail client;</LI>
</UL>
<LI>Noatun, the KDE multimedia player, sports a number of new features:</LI>
<UL>
  <LI>Improved the plugin architecture and added a number of new plugins:</LI>
  <UL>
    <LI>An Alarm plugin for playing music at a specified time;</LI>
    <LI>A Blurscope plugin which creates an SDL-based blurred monoscope;</LI>
    <LI>A Luckytag plugin for guessing titles based on filenames;</LI>
    <LI>A Noatun Madness plugin, which moves the Noatun window in sync with
        the music being played;</LI>
    <LI>A Synaescope plugin, based on
       <A HREF="http://yoyo.cc.monash.edu.au/~pfh/synaesthesia.html">Synaesthesia</A>,
       which provides an impressive SDL-based visualization; and</LI>
    <LI>A Tyler plugin, which is similar to
        <A HREF="http://www.xmms.org/">XMMS</A>'s
        <A HREF="http://www.xmms.org/plugins_vis.html">Infinity</A>;</LI>
  </UL>
  <LI>Added support for pre-amplification; and</LI>
  <LI>Added support for hardware mixers;</LI>
</UL>
<LI>Added a Personalization wizard (KPersonalizer) to configure the desktop
    settings easily;</LI>
<LI>Added <A HREF="http://www.rhrk.uni-kl.de/~gebauerc/kdict/">KDict</A>,
    a powerful graphical dictionary client;</LI>
<LI>Added KDE-wide scanning support with the application Kooka;</LI>
<LI>Replaced the default editor KWrite with the more advanced editor Kate,
    which provides split views and basic project management;</LI>
<LI>The window manager now supports Xinerama (multi-headed displays);</LI>
<LI>Improved the file dialog, including mimetype-based file previews;</LI>
<LI>Improved the configurability of the KDE panel;</LI>
<LI>Added IPv6 and socks support to the core libraries;</LI>
<LI>Improved application startup:
<UL>
  <LI>applications are now placed on the desktop from which they were
      launched; and</LI>
  <LI>startup notification can be configured with a new Control Center module,
      with options including a busy cursor next to the application's icon;</LI>
</UL>
<LI>Improved icons and added new 64x64 icons;</LI>
<LI>New window manager decoration styles (quartz, IceWM themes, MWM, Web);</LI>
<LI>Improved the help system, which is now XML-based;</LI>
<LI>Added support for the Meta and AltGr keys for shortcuts; and</LI>
<LI>Made many other usability improvements.</LI>
</UL>
</P>
<P>
For a much more complete list, please read the
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">official
ChangeLog</A>.
</P>
<P>
<H4>Downloading and Compiling KDE</H4>
</P>
<P>
<A NAME="Source_Code"></A><EM>Source Packages</EM>.
The source packages for KDE 2.2beta1 are available for free download at
<A HREF="http://ftp.kde.org/unstable/2.2beta1/src/">http://ftp.kde.org/unstable/2.2beta1/src/</A>
or in the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
</P>
<P>
<A NAME="Source_Code-Library_Requirements"></A><EM>Library Requirements.</EM>
KDE 2.2beta1 requires at least qt-x11-2.2.4, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.1.tar.gz">qt-2.3.1</A>
is recommended (for anti-aliased fonts,
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>
and <A HREF="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</A> or
newer is required).
KDE 2.2beta1 will not work with versions of Qt older than 2.2.4.
</P>
<P>
<EM>Compiler Requirements</EM>.  Please note that
<A HREF="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</A> is not
recommended for compilation of KDE 2.2beta1. Several known miscompilations
of production C++ code (such as virtual inheritance, which is used in <A HREF="http://www.arts-project.org/">aRts</A>) occur with this compiler.
The problems are mostly known and the KDE team is working with the gcc team
to fix them.
</P>
<P>
<EM>Further Instructions</EM>.
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
<EM>Binary Packages</EM>.
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for
KDE 2.2beta1 will be available for free download under
<A HREF="http://ftp.kde.org/unstable/2.2beta1/">http://ftp.kde.org/unstable/2.2beta1/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <A HREF="http://dot.kde.org/986933826/">KDE Binary Package
Policy</A>).
</P>
<P>
<EM>Library Requirements</EM>.
The library requirements for a particular binary package vary with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and/or KDE
than was included with the particular version of a distribution for
which the binary package is listed below (e.g., LinuxDistro 8.0 may have
shipped with qt-2.2.3 but the packages below may require qt-2.3.x).  For
general library requirements for KDE, please see the text at
<A HREF="#Source_Code-Library_Requirements">Source Code - Library
Requirements</A> above.
</P>
<P>
<A NAME="Package_Locations"><EM>Package Locations</EM></A>.
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
  <LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> (package "kde"):  <A HREF="ftp://ftp.debian.org/">ftp.debian.org</A>:  sid (devel) (see also <A HREF="http://http.us.debian.org/debian/pool/main/k/">here</A>)</LI>
  <LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A>
  <UL>
    <LI>8.0:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Mandrake/8.0/i586/">Intel i586</A></LI>
    <LI>7.2:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Mandrake/7.2/i586/">Intel i586</A></LI>
  </UL>
  <LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/README">README</A>):
  <UL>
    <LI>7.2:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.2/">Intel i386</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ia64/7.1/">HP/Intel IA-64</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>7.1:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.1/">Intel i386</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.1/">PowerPC</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/sparc/7.1/">Sun Sparc</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/axp/7.1/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>7.0:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.0/">Intel i386</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.0/">PowerPC</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/s390/7.0/">IBM S390</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>6.4:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/6.4/">Intel i386</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
  </UL>
  <LI>Tru64 Systems:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/unstable/2.2beta1/Tru64/README.Tru64">README.Tru64</A>)</LI>
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.  In particular,
<A HREF="http://www.redhat.com/">RedHat Linux</A> packages should be
available shortly.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
Please visit the KDE family of web sites for the
<A HREF="http://www.kde.org/documentation/faq/index.html">KDE FAQ</A>,
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>,
<A HREF="http://www.koffice.org/">KOffice information</A>,
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
Much more information about KDE is available from KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
PostScript is a registered trademark of Adobe Systems Incorporated.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;&#x72;&#x61;&#00110;&#x72;oth&#00064;kd&#x65;.or&#00103;<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
&#00112;our&#x40;&#00107;&#x64;e&#046;o&#00114;g<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#102;&#97;u&#x72;e&#64;k&#100;e.or&#103;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#x6f;no&#108;d&#00064;kd&#x65;.&#00111;r&#103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php include "footer.inc" ?>
