<?php
  $page_title = "Announcing KDE 3.3 Beta 1 (\"Klassroom\")";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE July 7, 2004</p>

<h3 align="center">KDE Project Ships First Beta of Next Major Release</h3>

<p>July 7, 2004 (The Internet) - The <a href="http://www.kde.org">KDE Project</a> is pleased to announce the immediate availability of KDE 3.3 Beta 1. As another step towards the <a href="http://conference2004.kde.org">aKademy</a> in late August, this release is named Klassroom.
</p>

<h4>Getting Klassroom</h4>

<p>KDE 3.3 Beta 1 can be downloaded over the Internet by visiting <a href="http://download.kde.org/unstable/3.2.91/src">download.kde.org</a>. Source code and vendor supplied binary packages are available.For additional information on package availability and to read further release notes, please visit the <a href="http://www.kde.org/info/3.3beta1.php">KDE 3.3 Beta 1 information page</a>.</p>

<p>This beta release shows astonishing stability, so the KDE team asks everyone to try the version and give feedback through <a href="http://bugs.kde.org">the bug tracking system</a>.
</p>


<h4>Supporting KDE</h4>

<p>KDE is supported through voluntary contributions of time, money and resources by individuals
and companies from around the world. To discover how you or your company can join in and help
support KDE please visit the <a href="http://www.kde.org/community/donations/">Supporting KDE</a> web page. There
may be more ways to support KDE than you imagine, and every bit of support helps make KDE
a better project and a better product for everyone. Communicate your support today with a monetary donation,
new hardware or a few hours of your time!</p>

<p>Especially for beta releases, we can need any helping hand that is offered us to categorize and fix problem reports</a>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;&#x61;ilt&#111;&#00058;i&#110;fo&#45;a&#00102;ric&#097;&#x40;k&#100;&#101;.o&#x72;&#x67;">&#x69;&#x6e;&#x66;o-a&#x66;&#x72;&#105;ca&#64;&#107;&#100;&#x65;.&#111;&#00114;&#0103;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="ma&#x69;&#0108;&#116;&#111;&#x3a;i&#x6e;&#102;&#0111;&#x2d;&#97;sia&#x40;k&#100;e.&#x6f;rg">in&#x66;&#x6f;-a&#115;i&#x61;&#64;k&#x64;e.&#111;r&#00103;</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#97;&#105;lt&#111;&#x3a;i&#110;&#00102;o&#x2d;eu&#00114;&#x6f;p&#101;&#0064;&#0107;&#00100;e.&#00111;&#0114;g">i&#110;f&#x6f;-&#x65;&#x75;&#114;&#x6f;pe&#064;&#x6b;d&#x65;&#x2e;o&#114;&#x67;</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="mai&#108;&#116;o&#00058;info-no&#114;t&#x68;&#00097;mer&#105;ca&#64;kd&#101;.or&#x67;">&#0105;&#x6e;fo&#0045;&#110;or&#x74;h&#x61;&#x6d;&#x65;ric&#x61;&#x40;&#x6b;&#x64;e.o&#114;g</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#x6d;&#97;&#00105;&#x6c;&#0116;&#0111;&#58;&#x69;&#x6e;fo&#x2d;&#111;cean&#105;&#x61;&#x40;k&#100;&#x65;.org">in&#102;&#x6f;&#x2d;oc&#101;&#00097;ni&#97;&#64;k&#100;e&#x2e;o&#114;g</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="mai&#108;to&#x3a;i&#x6e;fo&#x2d;&#x73;&#0111;&#x75;&#x74;h&#x61;mer&#105;&#00099;&#0097;&#x40;k&#x64;&#101;&#x2e;&#x6f;r&#103;">i&#110;fo&#x2d;&#115;&#x6f;u&#x74;&#x68;&#97;&#109;er&#00105;c&#97;&#00064;kd&#101;&#0046;&#111;r&#103;</a><br />
</td>


</tr></table>

<?php
  include("footer.inc");
?>

