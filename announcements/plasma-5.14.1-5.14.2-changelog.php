<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.14.2 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.14.2";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>GTK theme treeview style typo/bug fix. <a href='https://commits.kde.org/breeze-gtk/a742b49636d958ab310a71fec4454e7fb00da584'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16331'>D16331</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Snap: don't say there's no ratings yet if the rating system isn't supported. <a href='https://commits.kde.org/discover/09185a11757a998ec4b32ade757801b3527cf40a'>Commit.</a> </li>
<li>Remove unused variable. <a href='https://commits.kde.org/discover/4f763c506f8a2881e122f13703e25f7511b5cd9f'>Commit.</a> </li>
<li>Snap: don't announce any resources as supported. <a href='https://commits.kde.org/discover/0756d335cd0d712f115b450e4eefe53867f7c025'>Commit.</a> </li>
<li>Make sure resources aren't deleted before served. <a href='https://commits.kde.org/discover/391d57df0ae3ba4717f3c318c18fadbd7ac206d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397246'>#397246</a></li>
<li>Don't show download speet if it's 0. <a href='https://commits.kde.org/discover/7f2f2f5a2ce1be54714c32d70483daa0b602ddf0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400042'>#400042</a></li>
<li>Pk: use the file path as the origin for local file resources. <a href='https://commits.kde.org/discover/f66557f6f6799dbe03ba4808b7aaf08d2932d341'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400044'>#400044</a></li>
<li>Pk: make it possible to request the changelog two ways. <a href='https://commits.kde.org/discover/c85b7d676be55a969e8f999ac6f47a12721b452f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399062'>#399062</a></li>
<li>Pk: remove unused function. <a href='https://commits.kde.org/discover/1b3bd92cf220465c4712d1d43d8c99e3e525ff20'>Commit.</a> </li>
<li>Check in Flatpak logo icon. <a href='https://commits.kde.org/discover/b778a7bb0d8109e6985b7b303d686b3af2d72e21'>Commit.</a> </li>
<li>Only show the category label if the category is available. <a href='https://commits.kde.org/discover/12fd2c61c14190e4117edb0b00b3476027359927'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400045'>#400045</a></li>
<li>Notifier: don't show the view updates button simply because it needs a reboot. <a href='https://commits.kde.org/discover/e99e45b03f648d1d98d6f8d5fcd1ac621601b83d'>Commit.</a> </li>
<li>Flatpak: Better install an icon from Discover and use it. <a href='https://commits.kde.org/discover/8625acc1c3f602091279a7f8922d988a0e9832ac'>Commit.</a> </li>
<li>Ux: hide navigation arrows while downloading a screenshot. <a href='https://commits.kde.org/discover/a0faaed8bfbb6632314e8300886bbba4afd126cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391130'>#391130</a></li>
<li>Flatpak: Use an icon that actually exists. <a href='https://commits.kde.org/discover/73e365e1ac4a18a27e808e4cbcd43d337b453873'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399891'>#399891</a></li>
<li>Pk: include update detail fetching into the batch request. <a href='https://commits.kde.org/discover/0bd734e101da0a529a4733ef39a4c260f08a4318'>Commit.</a> </li>
<li>Pk: clear package names after fetching. <a href='https://commits.kde.org/discover/70956de7612a6f6a288b52f4a1b5d826fb49868e'>Commit.</a> </li>
<li>Pk: don't request packages while they're still not ready. <a href='https://commits.kde.org/discover/31b882045d85504e41ad95869873d726a3534ff7'>Commit.</a> </li>
<li>Pk: don't call getUpdates if we're going to refresh the cache. <a href='https://commits.kde.org/discover/bc1c1751e7d4a2a01d16e164061ef0a2f2707fb7'>Commit.</a> </li>
<li>Fwupd: populate installed devices with its current release. <a href='https://commits.kde.org/discover/7b8ae0a6460061be8c7a75b973252b041f0fa1c5'>Commit.</a> </li>
<li>Fwupd: don't instantiate elements we are not going to use. <a href='https://commits.kde.org/discover/39678655d082dfa9176fdb32cbb8ebb4d4e9d036'>Commit.</a> </li>
<li>Fwupd: packageName should be stable. <a href='https://commits.kde.org/discover/656392a0086fb6ab9e144ed182234c94014d90fb'>Commit.</a> </li>
<li>Fwupd: remove declaration without implementation. <a href='https://commits.kde.org/discover/81d35a1b6491db30f08383fa12579483ab9b3099'>Commit.</a> </li>
<li>Fwupd: remove weird conversion between uint64 and uint. <a href='https://commits.kde.org/discover/cc42f4a1a5886f2b15e0299e5559708186a503f7'>Commit.</a> </li>
<li>Fwupd: have FwupdResource a normal class with getters and setters. <a href='https://commits.kde.org/discover/852ddf0268a388ded709a54a6592349b945b2c93'>Commit.</a> </li>
<li>Fwupd: have a meaningful initial state for resources. <a href='https://commits.kde.org/discover/8a42d1f844d98d8f3006fcc21bb0c356d021f032'>Commit.</a> </li>
<li>Fwupd: remove dummy addons code copied from DummyResource. <a href='https://commits.kde.org/discover/b2525f98dabb6cd1bc33edc2148fc2df295073e6'>Commit.</a> </li>
<li>Fwupd: Move code where it belongs. <a href='https://commits.kde.org/discover/9f328a3dbaf3ae51c6a317ac0bceec017c998fdf'>Commit.</a> </li>
<li>Fwupd: don't leak replies. <a href='https://commits.kde.org/discover/eb109a1eb9000647beeb68a3539b33e71e6bf068'>Commit.</a> </li>
<li>Fwupd: style. <a href='https://commits.kde.org/discover/a4a43aab4ce5356073b0992d91bcccae968906e1'>Commit.</a> </li>
<li>All: make sure we don't keep references to old discarded resources. <a href='https://commits.kde.org/discover/29633ea8e388f4388b46c259b9851993668a55ad'>Commit.</a> </li>
<li>Fwupd: don't return resources while fetching. <a href='https://commits.kde.org/discover/8568df7193ba75ab0c2fdac7276885b3acb95b92'>Commit.</a> </li>
<li>Fix indentation. <a href='https://commits.kde.org/discover/bf73e9219da3b759a9ecb203463bb9978263be56'>Commit.</a> </li>
<li>Fwupd: make sure resources have at least a name. <a href='https://commits.kde.org/discover/64daf84e4ef2ae333646dd2fcf42edaefbccf3e4'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Minimize All Plasmoid] Import QtQuick 2.7. <a href='https://commits.kde.org/kdeplasma-addons/f328668559cc56a8a32f52932873554af8b4f5da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399947'>#399947</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16292'>D16292</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Lower Qt dependency to 5.9. <a href='https://commits.kde.org/plasma-browser-integration/9376116374efeb5bf704aafc4e542aa76c8279c9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Remove. <a href='https://commits.kde.org/plasma-desktop/0e1aa9e046c20dbc75a33eac368b1f0554ac0368'>Commit.</a> </li>
<li>Focus handling fixes. <a href='https://commits.kde.org/plasma-desktop/03b17ac5ec3e04aebe07e69d9240e035fa743c2e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399566'>#399566</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16106'>D16106</a></li>
<li>[Kicker] Rename id column to itemColumn. <a href='https://commits.kde.org/plasma-desktop/004d838c2718ed743d8a7c746cb605331d1b46c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16316'>D16316</a></li>
<li>Add accessibility information to desktop icons. <a href='https://commits.kde.org/plasma-desktop/498c42fed65df76ca457955bab18a252d63ca409'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16309'>D16309</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Show correct current download and upload speed. <a href='https://commits.kde.org/plasma-nm/c5593b02df3864fe7a720925492306734d0b94e6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16269'>D16269</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Plasmashell freezes when trying to get free space info from mounted remote filesystem after losing connection to it. <a href='https://commits.kde.org/plasma-workspace/be3b80e78017cc6668f9227529ad429150c27faa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397537'>#397537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14895'>D14895</a>. See bug <a href='https://bugs.kde.org/399945'>#399945</a></li>
<li>[Lock Screen] Do not try to unlock when unvisible. <a href='https://commits.kde.org/plasma-workspace/754efdedd4ee1b7dbafda06bd7858e2f51346ea0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395671'>#395671</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16219'>D16219</a></li>
<li>KRunner: remove no longer existant and unused column from SQL query. <a href='https://commits.kde.org/plasma-workspace/99fa6ccc57c5038ffb16d2e999893d55dc91f5b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398305'>#398305</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15305'>D15305</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
