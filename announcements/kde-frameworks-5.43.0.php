<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.43.0");
  $site_root = "../";
  $release = '5.43.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
February 12, 2018. KDE today announces the release
of KDE Frameworks 5.43.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("New modules");?></h3>

<p>KHolidays: Holiday calculation library</p>

<p>This library provides a C++ API that determines holiday and other
special events for a geographical region.</p>

<p>Purpose: Offers available actions for a specific purpose</p>

<p>This framework offers the possibility to create integrate services and actions
on any application without having to implement them specifically. Purpose will
offer them mechanisms to list the different alternatives to execute given the
requested action type and will facilitate components so that all the plugins
can receive all the information they need.</p>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("balooctl status: Produce parseable output");?></li>
<li><?php i18n("Fix KIO Slave tagged folder deep copies. This breaks listing tagged folders in the tag tree, but is better than broken copies");?></li>
<li><?php i18n("Skip queueing newly unindexable files and remove them from the index immediately");?></li>
<li><?php i18n("Delete newly unindexable moved files from the index");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add missing Krusader icons for folder sync (bug 379638)");?></li>
<li><?php i18n("Update list-remove icon with - instead cancel icon (bug 382650)");?></li>
<li><?php i18n("add icons for pulsaudio plasmoid (bug 385294)");?></li>
<li><?php i18n("use everywhere the same opacity 0.5");?></li>
<li><?php i18n("New virtualbox icon (bug 384357)");?></li>
<li><?php i18n("make weather-fog day/night neutral (bug 388865)");?></li>
<li><?php i18n("actually install the new animations context");?></li>
<li><?php i18n("QML file mime look now the same in all sizes (bug 376757)");?></li>
<li><?php i18n("Update animation icons (bug 368833)");?></li>
<li><?php i18n("add emblem-shared colored icon");?></li>
<li><?php i18n("Fix broken index.theme files, \"Context=Status\" was missing in status/64");?></li>
<li><?php i18n("Remove 'executable' permission from .svg files");?></li>
<li><?php i18n("Action icon download is linked to edit-download (bug 382935)");?></li>
<li><?php i18n("Update Dropbox systemtray icon theme (bug 383477)");?></li>
<li><?php i18n("Missing emblem-default-symbolic (bug 382234)");?></li>
<li><?php i18n("Type in mimetype filename (bug 386144)");?></li>
<li><?php i18n("Use a more specific octave logo (bug 385048)");?></li>
<li><?php i18n("add vaults icons (bug 386587)");?></li>
<li><?php i18n("scalle px status icons (bug 386895)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("FindQtWaylandScanner.cmake: Use qmake-query for HINT");?></li>
<li><?php i18n("Make sure to search for Qt5-based qmlplugindump");?></li>
<li><?php i18n("ECMToolchainAndroidTest doesn't exist anymore (bug 389519)");?></li>
<li><?php i18n("Don't set the LD_LIBRARY_PATH in prefix.sh");?></li>
<li><?php i18n("Add FindSeccomp to find-modules");?></li>
<li><?php i18n("Fall back to language name for translations lookup if locale name fails");?></li>
<li><?php i18n("Android: Add more includes");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Fix linking regression introduced in 5.42.");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Adds tooltips to the two buttons on each entry");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Fix incorrect emission of textEdited() by KLineEdit (bug 373004)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Use Ctrl+Shift+, as the standard shortcut for \"Configure &lt;Program&gt;\"");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Match also spdx keys LGPL-2.1 &amp; LGPL-2.1+");?></li>
<li><?php i18n("Use the much faster urls() method from QMimeData (bug 342056)");?></li>
<li><?php i18n("Optimize inotify KDirWatch backend: map inotify wd to Entry");?></li>
<li><?php i18n("Optimize: use QMetaObject::invokeMethod with functor");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[ConfigModule] Re-use QML context and engine if any (bug 388766)");?></li>
<li><?php i18n("[ConfigPropertyMap] Add missing include");?></li>
<li><?php i18n("[ConfigPropertyMap] Don't emit valueChanged on initial creation");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Don't export kded5 as a CMake target");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Refactor Solid::NetworkingPrivate to have a shared and platform specific implementation");?></li>
<li><?php i18n("Fix mingw compile error \"src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope\"");?></li>
<li><?php i18n("Fix kded dbus name in solid-networking howto");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Make kdoctools dependency optional");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("Make KDESU_USE_SUDO_DEFAULT mode build again");?></li>
<li><?php i18n("Make kdesu work when PWD is /usr/bin");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file (bug 382460)");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Fix linking of created QCH file into QtGui docs");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Fix finding libintl when \"cross\"-compiling native Yocto packages");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Fix cross-compiling with MinGW (MXE)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Repair copying file to VFAT without warnings");?></li>
<li><?php i18n("kio_file: skip error handling for initial perms during file copy");?></li>
<li><?php i18n("kio_ftp: don't emit error signal before we tried all list commands (bug 387634)");?></li>
<li><?php i18n("Performance: use the destination KFileItem object to figure out of it's writable instead of creating a KFileItemListProperties");?></li>
<li><?php i18n("Performance: Use the KFileItemListProperties copy constructor instead of the conversion from KFileItemList to KFileItemListProperties. This saves re-evaluating all items");?></li>
<li><?php i18n("Improve error handling in file ioslave");?></li>
<li><?php i18n("Remove PrivilegeExecution job flag");?></li>
<li><?php i18n("KRun: allow executing \"add network folder\" without confirmation prompt");?></li>
<li><?php i18n("Allow to filter places based on alternative application name");?></li>
<li><?php i18n("[Uri Filter Search Provider] Avoid double delete (bug 388983)");?></li>
<li><?php i18n("Fix overlap of the first item in KFilePlacesView");?></li>
<li><?php i18n("Temporarily disable KAuth support in KIO");?></li>
<li><?php i18n("previewtest: Allow specifying the enabled plugins");?></li>
<li><?php i18n("[KFileItem] Use \"emblem-shared\" for shared files");?></li>
<li><?php i18n("[DropJob] Enable drag and drop in a read-only folder");?></li>
<li><?php i18n("[FileUndoManager] Enable undoing changes in read-only folders");?></li>
<li><?php i18n("Add support for privilege execution in KIO jobs (temporarily disabled in this release)");?></li>
<li><?php i18n("Add support for sharing file descriptor between file KIO slave and its KAuth helper");?></li>
<li><?php i18n("Fix KFilePreviewGenerator::LayoutBlocker (bug 352776)");?></li>
<li><?php i18n("KonqPopupMenu/Plugin can now use the X-KDE-RequiredNumberOfUrls key to require a certain number of files to be selected before being shown");?></li>
<li><?php i18n("[KPropertiesDialog] Enable word wrap for checksum description");?></li>
<li><?php i18n("Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file (bug 388063)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("support for ColorGroups");?></li>
<li><?php i18n("no click feedback if the item doesn't want mouse events");?></li>
<li><?php i18n("work around for apps that use listitems incorrectly");?></li>
<li><?php i18n("space for the scrollbar (bug 389602)");?></li>
<li><?php i18n("Provide a tooltip for the main action");?></li>
<li><?php i18n("cmake: Use the official CMake variable for building as a static plugin");?></li>
<li><?php i18n("Update human-readable tier designation in API dox");?></li>
<li><?php i18n("[ScrollView] Scroll one page with Shift+wheel");?></li>
<li><?php i18n("[PageRow] Navigate between levels with mouse back/forward buttons");?></li>
<li><?php i18n("Ensure DesktopIcon paints with the correct aspect ratio (bug 388737)");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRearrangeColumnsProxyModel: fix crash when there's no source model");?></li>
<li><?php i18n("KRearrangeColumnsProxyModel: reimplement sibling() so it works as expected");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Code de-duplication in byteSize(double size) (bug 384561)");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Make kdoctools dependency optional");?></li>
</ul>

<h3><?php i18n("KJSEmbed");?></h3>

<ul>
<li><?php i18n("Unexport kjscmd");?></li>
<li><?php i18n("Make kdoctools dependency optional");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("The \"Run Command\" notification action has been fixed (bug 389284)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix: View jumps when Scroll past end of document is enabled (bug 306745)");?></li>
<li><?php i18n("Use at least the requested width for the argument hint tree");?></li>
<li><?php i18n("ExpandingWidgetModel: find the right-most column based on location");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KDateComboBox: fix dateChanged() not emitted after typing a date (bug 364200)");?></li>
<li><?php i18n("KMultiTabBar: Fix regression in conversion to new style connect()");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Define property in Units.qml for the Plasma styles");?></li>
<li><?php i18n("windowthumbnail: Fix the GLXFBConfig selection code");?></li>
<li><?php i18n("[Default Tooltip] Fix sizing (bug 389371)");?></li>
<li><?php i18n("[Plasma Dialog] Call window effects only if visible");?></li>
<li><?php i18n("Fix one source of log spam referenced in Bug 388389 (Empty filename passed to function)");?></li>
<li><?php i18n("[Calendar] Adjust the calendar toolbar anchors");?></li>
<li><?php i18n("[ConfigModel] Set QML context on ConfigModule (bug 388766)");?></li>
<li><?php i18n("[Icon Item] Treat sources starting with a slash as local file");?></li>
<li><?php i18n("fix RTL appearance for ComboBox (bug 387558)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Add BusyIndicator to the styled controls list");?></li>
<li><?php i18n("remove flicker when hovering scrollbar");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[UDisks] Only ignore non-user backing file if it is known (bug 389358)");?></li>
<li><?php i18n("Storage devices mounted outside of /media, /run/media, and $HOME are now ignored, as well as Loop Devices whose (bug 319998)");?></li>
<li><?php i18n("[UDisks Device] Show loop device with their backing file name and icon");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Find Aspell dictionaries on Windows");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix C# var regex");?></li>
<li><?php i18n("Support for underscores in numeric literals (Python 3.6) (bug 385422)");?></li>
<li><?php i18n("Highlight Khronos Collada and glTF files");?></li>
<li><?php i18n("Fix ini highlighting of values containing ; or # characters");?></li>
<li><?php i18n("AppArmor: new keywords, improvements &amp; fixes");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.43");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
