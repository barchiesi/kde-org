<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => i18n_noop("KDE Ships KDE Applications YY.MM.P"),
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $major_version = "YY.MM";
  $version = 'YY.MM.P';
  $release = 'applications-' . $version;
# Remember to adjust "first stability update" below
?>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Applications %1", $version)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<?php i18n("January 01, 20YY.")?>
<br />
<?php print i18n_var("Today KDE released the first stability update for <a href='%1'>KDE Applications %2</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.", "announce-applications-".$major_version.".0.php", $major_version);?>
<p/>
<p>
<?php i18n("More than a dozen recorded bugfixes include improvements to Kontact, Cantor, Gwenview, Okular, Umbrello, among others.");?>
</p>
<p>
<?php i18n("Improvements include:");?>
<ul>
<li><?php i18n("XXX (Add three user-visible improvements)");?></li>
<li><?php i18n("XXX");?></li>
<li><?php i18n("XXX");?></li>
</ul>
</p>
<p>
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications-aether.php?version=".$version);?>

<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Linux App Stores");?></h2>
        <p>
            <?php print i18n_var("Flathub contains <a href='https://flathub.org/apps/search/kde'>%1 packages from KDE applications</a> updated at release time.", 35);?>
        </p>
        <a class="text-center" href="https://flathub.org/apps/search/kde"><img alt="<?php i18n("Get it from the Flathub");?>" src="/announcements/flathub.png" class="img-fluid mb-2" /></a>
        <p class="mt-4">
            <?php print i18n_var("The Snap Store contains <a href='https://snapcraft.io/publisher/kde'>%1 packages from KDE applications</a> updated at release time.", 52);?>
        </p>
        <a class="text-center" href="https://snapcraft.io/publisher/kde"><img alt="<?php i18n("Get it from the Snap Store");?>" src="/announcements/snapcraft.png" class="img-fluid" /> </a>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='http://download.kde.org/stable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version, $version, $version, $version);?></p>
    </article>
</section>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
?>
