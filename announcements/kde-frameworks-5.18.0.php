<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.18.0");
  $site_root = "../";
  $release = '5.18.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
January 09, 2016. KDE today announces the release
of KDE Frameworks 5.18.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix several issue of mtime related search");?></li>
<li><?php i18n("PostingDB Iter: Do not assert on MDB_NOTFOUND");?></li>
<li><?php i18n("Balooctl status: Avoid showing 'Content indexing' about folders");?></li>
<li><?php i18n("StatusCommand: Show the correct status for folders");?></li>
<li><?php i18n("SearchStore: Gracefully handle empty term values (bug 356176)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("icon updates and additions");?></li>
<li><?php i18n("22px size status icons for 32px too as you need it in the system tray");?></li>
<li><?php i18n("Changed Fixed to Scalable value to 32px folders in Breeze Dark");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Make the KAppTemplate CMake module global");?></li>
<li><?php i18n("Silence CMP0063 warnings with KDECompilerSettings");?></li>
<li><?php i18n("ECMQtDeclareLoggingCategory: Include &lt;QDebug&gt; with the generated file");?></li>
<li><?php i18n("Fix CMP0054 warnings");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Streamlined the QML loading for KCM (bug 356832)");?></li>
<li><?php i18n("Work-around for the Qt SQL bug that does not clean up connections properly (bug 348194)");?></li>
<li><?php i18n("Merged a plugin that executes applications on activity state change");?></li>
<li><?php i18n("Port from KService to KPluginLoader");?></li>
<li><?php i18n("Port plugins to use kcoreaddons_desktop_to_json()");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Fully initialize DynMenuInfo in return value");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("KPluginSelector::addPlugins: fix assert if 'config' parameter is default (bug 352471)");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Avoid deliberately overflowing a full buffer");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Ensure group is unescaped properly in kconf_update");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)");?></li>
<li><?php i18n("Add KPluginMetaData::copyrightText(), extraInformation() and otherContributors()");?></li>
<li><?php i18n("Add KPluginMetaData::translators() and KAboutPerson::fromJson()");?></li>
<li><?php i18n("Fix use-after-free in desktop file parser");?></li>
<li><?php i18n("Make KPluginMetaData constructible from a json path");?></li>
<li><?php i18n("desktoptojson: make missing service type file an error for the binary");?></li>
<li><?php i18n("make calling kcoreaddons_add_plugin without SOURCES an error");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Adapt to Qt 5.6's dbus-in-secondary-thread");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[DragArea] Add dragActive property");?></li>
<li><?php i18n("[KQuickControlsAddons MimeDatabase] Expose QMimeType comment");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("kded: adapt to Qt 5.6's threaded dbus: messageFilter must trigger module loading in the main thread");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("kdelibs4support requires kded (for kdedmodule.desktop)");?></li>
<li><?php i18n("Fix CMP0064 warning by setting policy CMP0054 to NEW");?></li>
<li><?php i18n("Don't export symbols that also exist in KWidgetsAddons");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("Don't leak fd when creating socket");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Windows: remove kdewin dependency");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Document the first argument rule for plurals in QML");?></li>
<li><?php i18n("Reduce unwanted type changes");?></li>
<li><?php i18n("Make it possible to use doubles as index for i18np*() calls in QML");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix kiod for Qt 5.6's threaded dbus: messageFilter must wait until the module is loaded before returning");?></li>
<li><?php i18n("Change the error code when pasting/moving into a subdirectory");?></li>
<li><?php i18n("Fix emptyTrash blocked issue");?></li>
<li><?php i18n("Fix wrong button in KUrlNavigator for remote URLs");?></li>
<li><?php i18n("KUrlComboBox: fix returning an absolute path from urls()");?></li>
<li><?php i18n("kiod: disable session management");?></li>
<li><?php i18n("Add autocompletion for '.' input which offers all hidden files/folders* (bug 354981)");?></li>
<li><?php i18n("ktelnetservice: fix off by one in argc check, patch by Steven Bromley");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[Notify By Popup] Send along event ID");?></li>
<li><?php i18n("Set default non-empty reason for screen saver inhibition; (bug 334525)");?></li>
<li><?php i18n("Add a hint to skip notifications grouping (bug 356653)");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("[KNotifyConfigWidget] Allow selecting a specific event");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Make it possible to provide the metadata in json");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Fix possible double deletion in DeclarativePersonData");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Syntax h/l for pli: builtin functions added, expandable regions added");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("kwalletd: Fix FILE* leak");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Add xcb variant for static KStartupInfo::sendFoo methods");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("make it work with older NM versions");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[ToolButtonStyle] Always indicate activeFocus");?></li>
<li><?php i18n("Use the SkipGrouping flag for the \"widget deleted\" notification (bug 356653)");?></li>
<li><?php i18n("Deal properly with symlinks in path to packages");?></li>
<li><?php i18n("Add HiddenStatus for plasmoid self-hiding");?></li>
<li><?php i18n("Stop redirecting windows when item is disabled or hidden. (bug 356938)");?></li>
<li><?php i18n("Don't emit statusChanged if it hasn't changed");?></li>
<li><?php i18n("Fix element ids for east orientation");?></li>
<li><?php i18n("Containment: Don't emit appletCreated with null applet (bug 356428)");?></li>
<li><?php i18n("[Containment Interface] Fix erratic high precision scrolling");?></li>
<li><?php i18n("Read KPluginMetada's property X-Plasma-ComponentTypes as a stringlist");?></li>
<li><?php i18n("[Window Thumbnails] Don't crash if Composite is disabled");?></li>
<li><?php i18n("Let containments override CompactApplet.qml");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.18");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
