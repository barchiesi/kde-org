<!-- header goes into the specific page -->
<p align="justify">
El software de KDE, incluyendo todas sus bibliotecas y aplicaciones, est&aacute; disponible bajo licencias de c&oacute;digo abierto. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Adem&aacute;s de Linux y otros sistemas operativos basados en UNIX, en el sitio web del <a href="http://windows.kde.org">KDE para Windows</a> dispone de versiones para Microsoft Windows de la mayor&iacute;a de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. Tambi&eacute;n puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas m&oacute;viles como MeeGo, MS Windows Mobile y Symbian en la web, pero no est&aacute;n soportadas. 
KDE se puede obtener en c&oacute;digo fuente y varios formatos binarios en <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a> y tambi&eacute;n se puede obtener en <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> o con cualquiera de las <a href="http://www.kde.org/download/distributions.php">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales.
</p>
<p align="justify">
  <a name="packages"><em>Paquetes</em></a>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de <?php echo $release_full;?> para algunas versiones de su distribuci&oacute;n, y en otros casos lo han hecho voluntarios de la comunidad. <br />
  Algunos de estos paquetes binarios est&aacute;n disponibles para su descarga gratuita desde <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">http://download.kde.org</a>.
  En las pr&oacute;ximas semanas estar&aacute;n disponibles paquetes binarios adicionales, as&iacute; como actualizaciones de los ya disponibles.
</p>

<p align="justify">
  <a name="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/<?php echo $release_full;?>.php">p&aacute;gina de informaci&oacute;n de <?php echo $release;?></a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Equipo de Publicaci&oacute;n de KDE.
</p>

<h4>
  Compilaci&oacute;n de <?php echo $release_full;?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  El c&oacute;digo fuente completo de <?php echo $release_full;?> se puede <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE <?php echo $release_full;?> est&aacute;n disponibles en la <a href="/info/<?php echo $release_full;?>.php">p&aacute;gina de informaci&oacute;n de <?php echo $release;?></a>.
</p>



<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo m&aacute;ximo posible de nuestras publicaciones, le recomendamos encarecidamente el uso de la &uacute;ltima versi&oacute;n de Qt, que a d&iacute;a de hoy es la 4.6.3. Es necesaria para asegurarle una experiencia estable, as&iacute; como algunas mejoras del software KDE que se basan en el framework Qt subyacente.<br />
Algunos controladores gr&aacute;ficos pueden, bajo ciertas condiciones, utilizar XRender en lugar de OpenGL para la composici&oacute;n. Si experimenta una notable lentitud en cuanto al rendimiento gr&aacute;fico, puede que le ayude desactivar los efectos de escritorio, dependiendo del controlador gr&aacute;fico y de la configuraci&oacute;n utilizados. Para poder aprovechar todas las posibilidades del software KDE, le recomendamos que utilice los &uacute;ltimos controladores disponibles para su sistema, puesto que pueden mejorar su experiencia notablemente, tanto en cuesti&oacute;n de funciones opcionales como en rendimiento y estabilidad.
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
