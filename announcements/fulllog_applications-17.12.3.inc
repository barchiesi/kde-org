<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Properly workaround xsltproc not handling spaces in filepaths on Windows. <a href='http://commits.kde.org/akonadi/a381d31b66a1a73c1c09b18431070c7fc2434273'>Commit.</a> </li>
<li>Add warning in case an invalid mimetype is requested here. <a href='http://commits.kde.org/akonadi/36e868148accf6fc6bfadfe355be377071bfa614'>Commit.</a> </li>
<li>Restart DataStore's keep-alive query when opening connection. <a href='http://commits.kde.org/akonadi/e0afe3b6fc9ecf25a07c3b0b644824afb24516aa'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Remove duplicate category file. <a href='http://commits.kde.org/akonadi-contacts/860e908acb77fe09535a1e43ca85e9f7e4e3efc1'>Commit.</a> </li>
<li>Fix crash Bug 390965 - kaddressbook-5.7.2 without kdepim-addons crashes adding/editing contacts. <a href='http://commits.kde.org/akonadi-contacts/815064bb7d4caa00b7aafa216ca4093bd951d148'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390965'>#390965</a></li>
<li>Remove debug. <a href='http://commits.kde.org/akonadi-contacts/a34f2b79a5de047396861aedb544aa61485b0c0b'>Commit.</a> </li>
<li>Allow barcodes to be nullpointers. <a href='http://commits.kde.org/akonadi-contacts/7b1b2b76b9e0dbcdf6ad02e133affee75f94ac8a'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Prevent Akregator from overwriting feeds.opml with an emtpy feed list. <a href='http://commits.kde.org/akregator/6678efb711e2cc6ee852e7331ff5185ddeba872d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381929'>#381929</a></li>
<li>Don't call it twice. <a href='http://commits.kde.org/akregator/6dcc0769f61e38580b70a5c29db0d9920f263c6e'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Add expected failures for timestamp tests. <a href='http://commits.kde.org/ark/2fa96f4ca035575cd18d17cee81cbed3c63db555'>Commit.</a> </li>
<li>Fix build with Qt 5.11. <a href='http://commits.kde.org/ark/65966f41c3f3140340e4c0edc155b167055ab89f'>Commit.</a> </li>
<li>Mark canceled extractions as killed jobs. <a href='http://commits.kde.org/ark/92fe1c13bf506225e48914fa3517d7fd45ea9702'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382601'>#382601</a></li>
<li>Stop crashing when all read-write plugins are disabled. <a href='http://commits.kde.org/ark/2af8d35e362ba07d213855019a614a0c3d86a889'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390690'>#390690</a></li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Oxygen-icons.org got lost, so remove mention. <a href='http://commits.kde.org/artikulate/def6436d1af2560adfd07f01c0b8fa1f1839934d'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix resetting "View Modes" preferences to defaults. <a href='http://commits.kde.org/dolphin/a618383df3840d85dcd7e4ff62dc2ed02f55b0b7'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>LocalLister: always append trailing slash if it's a folder. <a href='http://commits.kde.org/filelight/c353bb21f2b5d56692ddafb673a66a5f49bb272c'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Enlarge small SVGs too. <a href='http://commits.kde.org/gwenview/9b1714fc7b50be746f39e835c8b3146c38a9ddd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364822'>#364822</a></li>
<li>Fix URL Navigator margins. <a href='http://commits.kde.org/gwenview/a267259bf5953c44c35af1229e9cd04be6381ac1'>Commit.</a> </li>
<li>Hide fullscreen thumbnailbar when on-canvas image tools are in use. <a href='http://commits.kde.org/gwenview/3e1680dc8691f7caadd2e92041552c5ad4042f3c'>Commit.</a> </li>
<li>Remove symbolic icons in favor of standard icons. <a href='http://commits.kde.org/gwenview/a4b8511d65c6962f090f6b7030b67e73e19310dc'>Commit.</a> </li>
<li>Fix thumbnail bar in View resetting to OFF when quitting in fullscreen. <a href='http://commits.kde.org/gwenview/75f1996cb3f455308f6e9779ff8887d08428cc25'>Commit.</a> </li>
<li>Fix background color for image view toolbar (crop/red-eye). <a href='http://commits.kde.org/gwenview/14e8e10f0a5a00ea69ceb91bc126d1a6094dfda1'>Commit.</a> </li>
<li>Update filename in fullscreen view on rename. <a href='http://commits.kde.org/gwenview/00ee923edea5196bbf11c046104d8a89d6331157'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390332'>#390332</a></li>
<li>Add icons for context and sidebar menu. <a href='http://commits.kde.org/gwenview/efaebe290f1b675ac8d13d8481d431c35cf282c5'>Commit.</a> </li>
<li>Improve thumbnail tooltip animation. <a href='http://commits.kde.org/gwenview/aa368aea25ba663a18d6f0cc45e5db89bcc38497'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/245091'>#245091</a></li>
<li>Save config when opening the config dialog. <a href='http://commits.kde.org/gwenview/e913496b73e1e6e105ed30e2d8498b6d7932e73b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390331'>#390331</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Extract Playlist::SharedSettings into a separate file. <a href='http://commits.kde.org/juk/90a6b862e9004b0ed85b46c176455eb71354acf3'>Commit.</a> </li>
<li>Stop playback before taking shutdown actions. <a href='http://commits.kde.org/juk/18eed01d473279ca2acac0851fb2d38df2ff73e1'>Commit.</a> </li>
<li>More column-handling bugfixes in Playlist. <a href='http://commits.kde.org/juk/2ea8a84d9b21c9f74612510529b175b5d39f8bfd'>Commit.</a> </li>
<li>Fix columns sorting. <a href='http://commits.kde.org/juk/cd8e92003e0bf4806af3d8bb8d7a1fba7ea5cdb7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389130'>#389130</a></li>
<li>Avoid crash on tree view mode by disabling tree view mode :(. <a href='http://commits.kde.org/juk/5982cede97993bf6cb81b80ef136b42c032382a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389937'>#389937</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Fix icon on Wayland. <a href='http://commits.kde.org/kalarm/4b9ef8e17202cb74147712fc8400795052b972b3'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Hide]</a></h3>
<ul id='ulkalzium' style='display: block'>
<li>Compoundviewer: use ${AvogadroLibs_INCLUDE_DIRS}. <a href='http://commits.kde.org/kalzium/8ed3c93e93c588be0f419ee493a17d3d4f6f6082'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Hide]</a></h3>
<ul id='ulkapptemplate' style='display: block'>
<li>Remove code duplicating work of KAboutData::setupCommandLine(). <a href='http://commits.kde.org/kapptemplate/e10b2d863de3dd05ff13d53b62acfee093ff29da'>Commit.</a> </li>
<li>Remove bogus Ui::%{APPNAMELC}ViewBase from window class template. <a href='http://commits.kde.org/kapptemplate/98e83f8d399a76795a43eac3d8babe9fc85f3824'>Commit.</a> </li>
<li>Fix templates' KAboutData code to use appname param for display app name. <a href='http://commits.kde.org/kapptemplate/3de1f5cfcd5614762fdce596c7eb8bbdca902c0f'>Commit.</a> </li>
<li>Remoe X-DocPath from desktop files of doc-less app template. <a href='http://commits.kde.org/kapptemplate/39f6f5016656f918cd5a549d32a9cce90d3eeb04'>Commit.</a> </li>
<li>Fix templates to use normal casing for app name in desktop/appdata files. <a href='http://commits.kde.org/kapptemplate/acad47e8e9aca0d99137f2d6e2057bdde2bfcacb'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Use KAboutData::setDesktopFileName instead of QGuiApplication version. <a href='http://commits.kde.org/kdenlive/0c456f7fb29d5794eb34fa7aa2bdb974565ccff4'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix Bug 389820 - spamassassin. <a href='http://commits.kde.org/kdepim-addons/70f99474e64b875bbf4f13920698f9c5a4d7c507'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389820'>#389820</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Fix method name. <a href='http://commits.kde.org/kdepim-runtime/9f3004cb5e2b32d639af7eaa7b9fd01c5084806a'>Commit.</a> </li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Fix porting bug in UrlChecker::destUrl(). <a href='http://commits.kde.org/kget/7d70344c095f1413283ce409c44b658e159a285c'>Commit.</a> </li>
<li>Fix wrong path conversions in NewTransferDialogHandler::handleUrls(). <a href='http://commits.kde.org/kget/2ef232b0f945881fb598c690476487001317886b'>Commit.</a> </li>
<li>Fix importing several links at once. <a href='http://commits.kde.org/kget/f06402fbe520c57b05069589f18ce3d973667780'>Commit.</a> </li>
<li>Fix destDir handling in KGet::addTransfer(). <a href='http://commits.kde.org/kget/ed694ff5f5c8418a93d59bae6b68e11b9816c1f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390612'>#390612</a></li>
<li>Fix blurry icons in HiDPI. <a href='http://commits.kde.org/kget/bc64bce6050eb173c2c0c6f4e6d0f35175539d19'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Ignore empty entries in key groups. <a href='http://commits.kde.org/kgpg/432f03e8f3eae5d7d0e3c70c5324006a2f2a381a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387408'>#387408</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Rename categories file name + follow kdepim categories name. <a href='http://commits.kde.org/kmail/14401f4886ceb325e36a2eaa8283acf48235c541'>Commit.</a> </li>
<li>Fix Bug 390620 Editing an Identity while New Message window is open causes the Signature to be updated in the New Message window. <a href='http://commits.kde.org/kmail/4c6890ae43c19576389f8d35e0d27df89bee03a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390620'>#390620</a></li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>Fix autotest smtpjobtest. <a href='http://commits.kde.org/kmailtransport/bdb9e3d0e2dce48eb151ba5355d69e6643d43e7a'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Fix autotest. <a href='http://commits.kde.org/libksieve/01cd24a30822414e71335887ee958b88dc8706fc'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Reactivate code now. <a href='http://commits.kde.org/messagelib/e6156c0849c40d1b7e8776cfeed8ebdb00c27bdb'>Commit.</a> </li>
<li>Reactivate scroll to. <a href='http://commits.kde.org/messagelib/8ccd2eee5386b32638cbc49ea298bfaa6cb1f216'>Commit.</a> </li>
<li>Remove some yellow style. <a href='http://commits.kde.org/messagelib/892e721f0ffc95d9882b7e5f7055612f786a5bef'>Commit.</a> </li>
<li>Activate it. <a href='http://commits.kde.org/messagelib/9ba0e026a618b5d49b5bf0b4713b1b160d65894b'>Commit.</a> </li>
<li>Fix add/remove yellow mark. <a href='http://commits.kde.org/messagelib/69ff8708e56957eb7926e9370f6a7c4545fcb213'>Commit.</a> </li>
<li>Workaround bug found by David. <a href='http://commits.kde.org/messagelib/c6b390c72ba99dfdcc4d3c860e68c8c0e33fc8c7'>Commit.</a> </li>
<li>Patch from aleksejshilin. <a href='http://commits.kde.org/messagelib/117ea8d28103e3a656113f11b27c9b8033f473f4'>Commit.</a> </li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Hide]</a></h3>
<ul id='ulminuet' style='display: block'>
<li>Fix Build when fluidsynth isn't found. <a href='http://commits.kde.org/minuet/78ba120903358178e964d5a309f7544bf27fb320'>Commit.</a> </li>
<li>Create FindFluidSynth.cmake. <a href='http://commits.kde.org/minuet/b49f3bd09d916521e7a96225cb2fb33233c43414'>Commit.</a> </li>
<li>Fix build with fluidsynth 1.1.9. <a href='http://commits.kde.org/minuet/f1bab8d022b79bbe49b0ca9faecafcee019505eb'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Hide]</a></h3>
<ul id='ulokteta' style='display: block'>
<li>Use QStyle::SP_DialogCloseButton instead of hardcoding "dialog-close". <a href='http://commits.kde.org/okteta/aae522ecbf91e9fbd1f3c10ffd4e2d6387301cf5'>Commit.</a> </li>
<li>Use icons with more BookmarksController actions. <a href='http://commits.kde.org/okteta/9fe714b610376a16107e0add71f58b79369052c9'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Add QVERIFY to QTest::qWaitForWindowExposed. <a href='http://commits.kde.org/okular/c45e2170ab11992155aff9332bc178f64d5b4a42'>Commit.</a> </li>
<li>Fix crash due to dangling pointer in MouseAnnotation. <a href='http://commits.kde.org/okular/3c4f16ea4b7e57b57e34830cd4ecf3f0ff80b399'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388228'>#388228</a></li>
<li>Build++. <a href='http://commits.kde.org/okular/77ee07cef58db3e40db6be6e59231ba75e79a95f'>Commit.</a> </li>
<li>Add test that produces bug 388288 crash. <a href='http://commits.kde.org/okular/5facb3514e23a5090294d210ef5fca35ce95c9c2'>Commit.</a> </li>
<li>Make it compile. <a href='http://commits.kde.org/okular/cecbc8b68ca4f7d0c18acfec517e6cecc14404d6'>Commit.</a> </li>
<li>Make Part::openUrl not discard OpenUrlArguments. <a href='http://commits.kde.org/okular/d19834f231acc31d058978b1f01b70c0eee332bf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386600'>#386600</a></li>
<li>Fix potential crash in document saving. <a href='http://commits.kde.org/okular/e8d9feed70d74ed3ac06a7b8d5b25eaac2d2b018'>Commit.</a> </li>
<li>PDF: Fix potential crash. <a href='http://commits.kde.org/okular/83374b97df101e3408ad0f8cc7b3327ddf805fda'>Commit.</a> </li>
<li>XPS: Fix crash opening some files. <a href='http://commits.kde.org/okular/42d4710198a76650ff2e117b5b1a966a3685f376'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390355'>#390355</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Hide]</a></h3>
<ul id='ulprint-manager' style='display: block'>
<li>Fix Plasma applet printer job count. <a href='http://commits.kde.org/print-manager/bf505de6ee5ec66fcb7639885e5518ae3f6196ab'>Commit.</a> </li>
</ul>
<h3><a name='sweeper' href='https://cgit.kde.org/sweeper.git'>sweeper</a> <a href='#sweeper' onclick='toggle("ulsweeper", this)'>[Hide]</a></h3>
<ul id='ulsweeper' style='display: block'>
<li>Adjust to "new" version of the thumbnail standard. <a href='http://commits.kde.org/sweeper/9c9586579f6f0dc7c0db73cf7683e94ab61526d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361482'>#361482</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix duplicated adding of floating text widgets to scene on loading an xmi file. <a href='http://commits.kde.org/umbrello/5e8ebda0d9424b33e30e39547beb4e61af1d9311'>Commit.</a> </li>
<li>Fix 'Imported classes from file do not show available associations'. <a href='http://commits.kde.org/umbrello/02696cb8fd716b7203ee7d0955a79ef4ee2688f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390901'>#390901</a></li>
<li>Fixup of 0f09ef6fb. <a href='http://commits.kde.org/umbrello/25c9df06b1df8fcdad0e7518f2b37cff3269cee2'>Commit.</a> </li>
<li>Fix 'Notes diagram link could not be changed'. <a href='http://commits.kde.org/umbrello/52fe3a6ebf00c89381301e14bbf868246a65a6ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390891'>#390891</a></li>
<li>Increase shape part for resize marker to make it easier to select in low zoom levels. <a href='http://commits.kde.org/umbrello/1ee663309689dca20f8bd593ec2fcc8e74145bb5'>Commit.</a> See bug <a href='https://bugs.kde.org/389357'>#389357</a></li>
<li>Fixup shape for alternate combined fragment widget. <a href='http://commits.kde.org/umbrello/0cdd90dd513d35c45386b5739b36610d5b2b4c6c'>Commit.</a> See bug <a href='https://bugs.kde.org/389357'>#389357</a></li>
<li>Fix 'Vertical position of combined fragment widget else part is moved to the vertical center after save/load cycle'. <a href='http://commits.kde.org/umbrello/7a39491306806cf9072cacac4d95686059dab591'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390782'>#390782</a></li>
<li>Fix 'Loading combined fragments from xmi file adds unwanted else part'. <a href='http://commits.kde.org/umbrello/4000ba1327de96091ca3ffd9df2611eca235ce91'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390780'>#390780</a></li>
<li>Fix writing out state information in ticket body. <a href='http://commits.kde.org/umbrello/7c0dc5f796cc0edf6bb342f93b433f3e82e2f86b'>Commit.</a> </li>
<li>Fix 'Instable vertical position of alternate combined fragment widget'. <a href='http://commits.kde.org/umbrello/62e20759adbe24d41a3df1bb8b9654655debd0ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390677'>#390677</a></li>
<li>Fix 'Box widgets inside a combined fragment are not selectable'. <a href='http://commits.kde.org/umbrello/1a3a5424fa2ee8613797dcc6238a54136e10ea9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389357'>#389357</a></li>
<li>Refactoring of global variables used by class CombinedFragmentWidget. <a href='http://commits.kde.org/umbrello/149407314e7cc360de6fdf312ae434c5da83df18'>Commit.</a> See bug <a href='https://bugs.kde.org/389357'>#389357</a></li>
<li>Fix 'Not possible to select notes under a fragment widget in sequence diagrams'. <a href='http://commits.kde.org/umbrello/45d667bbbf1989a511a5e99621e250007ed99270'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390509'>#390509</a></li>
<li>Fix 'Could not set fill color for message widgets'. <a href='http://commits.kde.org/umbrello/3571fa4e67b71cb204bed41e7981e542f2dadf98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390511'>#390511</a></li>
<li>Fix 'No context menu entry for changing colors of a single selected message widget'. <a href='http://commits.kde.org/umbrello/56c7b89c892ba9adcc9dc347bf019ef01cb787de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390508'>#390508</a></li>
</ul>
