<?php

  $page_title = "An&uacute;ncio de Lan&ccedil;amento do KDE 3.5.6";
  $site_root = "../";
  include "header.inc";
?>

<!-- Other languages translations -->
Tamb&eacute;m dispon&iacute;vel em:
<a href="announce-3.5.6.php">Ingl&ecirc;s</a>
<a href="announce-3.5.6-ca.php">Catal&atilde;o</a>
<a href="http://www.kdecn.org/announcements/announce-3.5.6.php">Chinese (simplified)</a>
<a href="announce-3.5.6-es.php">Espanhol</a>
<a href="http://fr.kde.org/announcements/announce-3.5.6-fr.php">Franc&ecirc;s</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.6.php">Island&ecirc;s</a>
<a href="announce-3.5.6-it.php">Italiano</a>
<a href="announce-3.5.6-nl.php">Holand&ecirc;s</a>
<a href="announce-3.5.6-pt-br.php">Portugu&ecirc;s (Brasil)</a>
<a href="announce-3.5.6-sl.php">Esloveno</a>
<a href="announce-3.5.6-sv.php">Sueco</a>

<h3 align="center">
   O Projecto KDE Lan&ccedil;a a Sexta Vers&atilde;o de
   Tradu&ccedil;&otilde;es e Servi&ccedil;os do Ambiente de
   Trabalho Livre
</h3>

<p align="justify">
   <strong>
     O KDE 3.5.6 disponibiliza tradu&ccedil;&otilde;es em 65
     l&iacute;nguas, melhorias no motor de
     visualiza&ccedil;&atilde;o de HTML (o KHTML) e&nbsp;outras
     aplica&ccedil;&otilde;es.
   </strong>
</p>

<p align="justify">
   25 de Janeiro de 2007 (INTERNET). O Projecto KDE anunciou hoje a
   disponibilidade imediata do KDE 3.5.6, uma vers&atilde;o de
   manuten&ccedil;&atilde;o da &uacute;ltima
   gera&ccedil;&atilde;o do ambiente de trabalho poderoso,
   avan&ccedil;ado e livre para o GNU/Linux e outros UNIXes. O KDE
   suporta agora 65 idiomas, tornando-se acess&iacute;vel para ainda
   mais pessoas que a maioria do software propriet&aacute;rio, podendo
   ser estendido facilmente pelas comunidades que desejem contribuir para
   o projecto c&oacute;digo-aberto.
</p>

<p align="justify">
Esta vers&atilde;o inclui
&nbsp;inumeras correc&ccedil;&otilde;es de defeitos do
KHTML, do <a href="http://kate.kde.org">Kate</a>,
do kicker, do ksysguard e muitas outras
aplica&ccedil;&otilde;es. As caracter&iacute;sticas
significativas incluem suporte adicional para o compiz como
gestor de janelas com o kicker, navegador de
gest&atilde;o&nbsp;de separadores para o&nbsp;<a
 href="http://akregator.kde.org">Akregator</a>, modelos
para mensagens do KMail, e novos menus de sum&aacute;rios para
o&nbsp;<a href="http://kontact.kde.org">Kontact</a>
tornando-o mais f&aacute;cil de trabalhar para apontamentos e lista
de a-fazeres. As tradu&ccedil;&otilde;es tamb&eacute;m
continuam, com as tradu&ccedil;&otilde;es&nbsp;<a
 href="http://l10n.kde.org/team-infos.php?teamcode=gl">Galicenses</a>
quase a dobrarem para 78%.
</p>

<p align="justify">
Para uma lista mais detalhada das melhorias desde a vers&atilde;o
KDE 3.5.5, de 11 de Outubro de 2006, veja por favor a&nbsp;<a
 href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php">lista de altera&ccedil;&otilde;es
do KDE 3.5.6</a>.
</p>

<p align="justify">
  O KDE 3.5.6 inclui com um ambiente de trabalho b&aacute;sico (Gestor de Informa&ccedil;&atilde;o 
  Pessoal, administra&ccedil;&atilde;o, redes, educa&ccedil;&atilde;o, utilit&aacute;rios, multim&eacute;dia, jogos, 
  trabalhos gr&aacute;ficos, desenvolvimento web, entre outras coisas). As ferramentas
  e aplica&ccedil;&otilde;es mais importantes est&atilde;o dispon&iacute;veis em <strong>65 idiomas</strong>.
</p>

<h4>
  Distribui&ccedil;&otilde;es que Disponibilizam o KDE
</h4>
<p align="justify">
  A maioria das distribui&ccedil;&otilde;es de Linux e dos sistemas operativos UNIX n&atilde;o
  incorporam imediatamente as vers&otilde;es novas do KDE, mas ir&atilde;o integrar os
  pacotes do KDE 3.5.6 nas suas pr&oacute;ximas vers&otilde;es. Veja 
  <a href="http://www.kde.org/download/distributions.php">esta lista</a>
  para ver as distribui&ccedil;&otilde;es que oferecem o KDE.
</p>

<h4>
  Instalar os Pacotes Bin&aacute;rios do KDE 3.5.6
</h4>
<p align="justify">
  <em>Criadores de Pacotes</em>.
   Alguns vendedores de sistemas operativos fornecem gentilmente pacotes
   bin&aacute;rios do KDE 3.5.6 para algumas vers&otilde;es das suas distribui&ccedil;&otilde;es e, 
   noutros casos, existem comunidades volunt&aacute;rias que tamb&eacute;m o fazem.
   Alguns desses pacotes bin&aacute;rios est&atilde;o dispon&iacute;veis gratuitamente para
   descarregar pelo servidor do KDE em 
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">http://download.kde.org</a>.
  Os pacotes bin&aacute;rios adicionais, assim como as actualiza&ccedil;&otilde;es aos pacotes
  dispon&iacute;veis neste momento, poder&atilde;o vir a ficar dispon&iacute;veis nas pr&oacute;ximas
  semanas.
</p>

<p align="justify">
  <a name="package_locations"><em>Localiza&ccedil;&otilde;es dos Pacotes</em></a>.
  Para uma lista actualizada com os pacotes bin&aacute;rios dispon&iacute;veis, dos
  quais o Projecto KDE tenha sido informado, v&aacute; por favor &agrave; 
  <a href="/info/3.5.6.php">P&aacute;gina de Informa&ccedil;&otilde;es do KDE 3.5.6</a>.
</p>

<h4>
  Compilar o KDE 3.5.6
</h4>
<p align="justify">
  <a name="source_code"></a><em>C&oacute;digo-Fonte</em>.
  Poder&aacute; 
  <a href="http://download.kde.org/stable/3.5.6/src/">obter de forma livre</a>
  o c&oacute;digo-fonte completo do KDE 3.5.6. As instru&ccedil;&otilde;es de compila&ccedil;&atilde;o e
  instala&ccedil;&atilde;o do KDE 3.5.6 est&atilde;o dispon&iacute;veis na 
  <a href="/info/3.5.6.php">P&aacute;gina de Informa&ccedil;&atilde;o do KDE 3.5.6</a>.
</p>

<h4>
  Suportar o KDE
</h4>
<p align="justify">
O KDE &eacute; um projecto de <a href="http://www.gnu.org/philosophy/free-sw.html">'Software' Livre</a> que existe e cresce somente com a ajuda dos v&aacute;rios volunt&aacute;rios
que doam o seu tempo e esfor&ccedil;o. O KDE est&aacute; sempre &agrave; procura de novos 
volunt&aacute;rios e colaboradores, que possam ajudar programando, corrigindo ou 
relatando erros, escrevendo alguma documenta&ccedil;&atilde;o, traduzindo, promovendo, 
doando dinheiro, etc. Todas as contribui&ccedil;&otilde;es s&atilde;o bem-vindas e ansiosamente
aceites. Para mais informa&ccedil;&otilde;es, leia por favor a <a href="/community/donations/">p&aacute;gina de Suporte do KDE</a>. </p>

<p align="justify">
Ficamos &agrave; espera de not&iacute;cias suas em breve!
</p>

<h4>
  Acerca do KDE
</h4>
<p align="justify">
  O KDE &eacute; um projecto <a href="/awards/">premiado</a>, independente e com
  <a href="/people/">centenas</a> de programadores, tradutores, artistas
  e outros profissionais que colaboram atrav&eacute;s da Internet para criar e
  distribuir gratuitamente um ambiente de trabalho e de escrit&oacute;rio 
  sofisticado, personalizado e est&aacute;vel. Este utiliza uma arquitectura 
  flex&iacute;vel, baseada em componentes, redes transparentes e oferece uma
  &oacute;ptima plataforma de desenvolvimento.</p>

<p align="justify">
  O KDE oferece um ambiente de trabalho est&aacute;vel e maduro, que inclui um 
  navegador Web magn&iacute;fico 
  (o <a href="http://konqueror.kde.org/">Konqueror</a>), um pacote de gest&atilde;o
  da informa&ccedil;&atilde;o pessoal (o <a href="http://kontact.org/">Kontact</a>),
  um pacote de escrit&oacute;rio completo (o 
  <a href="http://www.koffice.org/">KOffice</a>), um grande conjunto de
  aplica&ccedil;&otilde;es de rede e utilit&aacute;rios, assim como um ambiente de desenvolvimento
  eficiente e intuitivo, com o excelente IDE 
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  O KDE &eacute; a prova real que o modelo de desenvolvimento de <em>software de c&oacute;digo-aberto
  </em> "estilo-Bazaar" pode criar tecnologias, com um rendimento
  superior &agrave;s complexas aplica&ccedil;&otilde;es propriet&aacute;rias existentes.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Avisos de Marcas Registadas.</em>
  O KDE<sup>&#174;</sup> e o log&oacute;tipo do K Desktop Environment<sup>&#174;</sup>
  s&atilde;o marcas registadas do KDE e.V.

  O Linux &eacute; uma marca registada de Linus Torvalds.

  O UNIX &eacute; uma marca registada do The Open Group, nos Estados Unidos e noutros
  pa&iacute;ses.

  Todas as outras marcas registadas e direitos de c&oacute;pia, referenciados neste
  an&uacute;ncio, s&atilde;o propriedades dos seus respectivos donos.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Contactos de Imprensa</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>&Aacute;frica</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Telefone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>&Aacute;sia e &Iacute;ndia</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Telefone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Telefone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Am&eacute;rica do Norte</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Telefone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Telefone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Am&eacute;rica do Sul</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Telefone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
