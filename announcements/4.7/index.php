<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "New KDE Applications, Workspaces and Development Platform Releases Bring New Features, Improve Stability";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma Workspaces, KDE Applications, and the KDE Platform that provides the foundation for KDE software. Version 4.7 of these releases provide many new features and improved stability and performance.
</p>
<?php
  centerThumbScreenshot("general-desktop.png", "Plasma and Applications 4.7");
?>

<?php
include("trailer-plasma.inc");
include("trailer-applications.inc");
include("trailer-platform.inc");

?>

<h3>
New Instant Messaging integrated directly into desktop
</h3>
<p>
The KDE-Telepathy team is proud to announce the technical preview and historic first release of the new IM solution for KDE. Even though it's still in its early stages, you can already set up all sorts of accounts, including GTalk and Facebook Chat and use them in your everyday life. The chat interface lets you choose among many appearances with its support of Adium themes. You can also put the Presence Plasma widget right into your panel to manage your online status. As this project is not yet mature enough to be part of the big KDE family, it is packaged and released separately, alongside the other major parts of KDE. The source code for KDE-Telepathy 0.1.0 is available on <a href="http://download.kde.org/download.php?url=unstable/telepathy-kde/0.1.0/src/">download.kde.org</a>. Installation instructions are available on <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">community.kde.org</a>.
</p>

<h3>
Stability As Well As Features
</h3>
<p>
In addition to the many new features described in the release announcements, KDE contributors have closed over 12,000 bug reports (including over 2,000 unique bugs in the software released today) since the last major releases of KDE software. As a result, our software is more stable than ever before.
</p>

<h3>
Spread the Word and See What Happens: Tag as "KDE"
</h3>
<p>
KDE encourages everybody to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, Vimeo and others. Please tag uploaded material with "KDE", so it is easier to find, and so that the KDE team can compile reports of coverage for the <?php echo $release?> releases of KDE software.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.7/&amp;title=KDE%20releases%20version%204.7%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.7/" data-text="#KDE releases version 4.7 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.7/ #kde47" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.7/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.7%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.7/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde47"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde47"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde47"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde47"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
About these release announcements
</h3><p>
These release announcements were prepared by Algot Runeman, Dennis Nienhüser, Dominik Haumann, Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, Carl Symons and other members of the KDE Promotion Team and wider community. They cover only a few highlights of the many changes made to KDE software over the past six months.
</p>


<h4>Support KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
