
<h3>
<a href="platform-ru.php">
Улучшение поддержки семантических связей и мультимедиа в Платформе KDE
</a>
</h3>

<p>
<a href="platform-ru.php">
<img src="images/platform.png" class="app-icon" alt="Платформа разработки KDE 4.7"/>
</a>

Большое количество программ сообщества KDE и других разработчиков 
могут ощутить серьёзные улучшения в мультимедийной библиотеке Phonon 
и в компонентах, обеспечивающих работу с семантическими связями. 
Расширены программные интерфейсы (API), увеличена стабильность работы. 
Новая инфраструктура KDE-Telepathy реализует интеграцию обмена мгновенными 
сообщениями в приложения и рабочий стол. Почти во всех компонентах Платформы разработки 
в той или иной мере улучшена производительность и стабильность, что также 
отразится на требовательности к системным ресурсам приложений, 
использующих Платформу KDE 4.7, и на общем впечатлении пользователей.
Чтобы узнать подробности, прочитайте <a href="platform-ru.php">анонс выпуска Платформы KDE 4.7</a>.
<br /><br />
<br /><br />

</p>
