<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "Выпущен KDE SC 4.7.0";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- vkontakte.ru -->
<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?34"></script>
<script type="text/javascript">
  VK.init({apiId: 2425036, onlyWidgets: true});
</script>

<p>На других языках:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
Сообщество KDE радо объявить о новом выпуске оболочек рабочего стола Plasma, приложений KDE и платформы KDE с основными обновлениями. Эти выпуски с номером версии 4.7 добавляют много новых возможностей, а также улучшают стабильность и производительность.
</p>
<?php
  centerThumbScreenshot("general-desktop.png", "Оболочки Plasma и приложения 4.7");
?>

<?php
include("trailer-plasma-ru.inc");
include("trailer-applications-ru.inc");
include("trailer-platform-ru.inc");

?>

<h3>
Интеграция мгновенных сообщений в среду рабочего стола
</h3>
<p>
Команда разработчиков KDE-Telepathy представляет первый выпуск 
нового пакета для обмена мгновенными сообщениями в KDE,
пока ещё находящегося в стадии технологического прототипа.
Хоть он ещё находится на ранней стадии, уже можно использовать 
все типы учётных записей, включая GTalk и Facebook Chat и 
использовать их в повседневной жизни. Интерфейс окна разговоров 
позволяет использовать темы из Adium. Вы также можете поместить 
специальный виджет Plasma на панель, чтобы управлять вашим статусом в сети. 
Поскольку этот проект ещё не является достаточно зрелым, чтобы быть частью 
большого семейства KDE, он упакован и выпущен отдельно, наряду с другими основными частями KDE.
Исходный код KDE-Telepathy 0.1.0 доступен на сайте <a href="http:/
/download.kde.org/download.php?url=unstable/telepathy-kde/0.1.0/src/">download.kde.org</a>. 
Инструкции по установке приведены <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">здесь</a>.
</p>

<h3>
Стабильность &mdash; одна из особенностей
</h3>
<p>
Кроме большого количества новых возможностей, описанных в выпуске, разработчики KDE закрыли более 12000 отчётов об ошибках (включая 2000 ошибок в программах, выпущенных сегодня) с момента последнего релиза KDE. В результате, наше программное обеспечение стало более стабильным, чем раньше.
</p>

<h3>
Несите благую весть!
</h3>
<p>
KDE призывает всех делиться информацией об этом выпуске 
на социальных сервисах в сети Интернет. 
Высылайте материалы на новостные сайты, распространяйте 
информацию через delicious, digg, reddit, twitter и identi.ca. 
Загружайте снимки экрана на Facebook, Flickr, ipernity и Picasa 
и добавляйте их в соответствующие группы. Создавайте видеоролики 
и загружайте их на YouTube, Blip.tv, Vimeo и другие веб-сайты. 
И не забывайте помечать загруженные материалы 
тегом kde, чтобы людям было легче их 
найти, а команда разработчиков KDE могла составить отчёт о том, 
насколько полно был освещён анонс нового выпуска программного обеспечения KDE. 
Расскажите о нас миру!</p>
Следите за развитием событий вокруг выпуска 
<?php echo $release?> через 
<a href="http://buzz.kde.org">ленту новостей сообщества KDE</a>. 
На этом сайте вы увидите всё, что происходит на 
identi.ca, Twitter, YouTube, Flickr, PicasaWeb, в блогах и 
других социальных сетях в реальном времени. 
Лента новостей находится по адресу 
<a href="http://buzz.kde.org">buzz.kde.org</a>.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.7/&amp;title=KDE%20releases%20version%204.7%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.7/" data-text="#KDE releases version 4.7 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.7/ #kde47" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.7/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.7%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.7/"></g:plusone>
	</td>
	<td>
		<div id="vk_like"></div>
		<script type="text/javascript">
			VK.Widgets.Like("vk_like", {type: "mini"}, 470);
		</script>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde47"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde47"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde47"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde47"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
	<td>
		<a href="http://fotki.yandex.ru/tags/kde"><img src="buttons/yandex-fotki.gif" alt="Яндекс.Фотки" title="Яндекс.Фотки" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
Об этих анонсах
</h3><p>
Эти анонсы подготовлены Algot Runeman, Dennis Nienhüser, Dominik Haumann, 
Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, 
Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, 
Carl Symons и другими авторами из команды продвижения KDE (KDE Promo) и всего сообщества KDE. 
В них освещены лишь основные моменты из множества изменений, внесённых в KDE за последние полгода.
</p>


<h4></h4>Поддержите KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify"> KDE e.V. открывает новую <a
href="http://jointhegame.kde.org/">акцию поддержки</a>. Платя &euro;25 в квартал, вы обеспечиваете
расширение сообщества KDE, которое создаёт свободное программное обеспечение мирового класса.
</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
