
<h3>
<a href="platform-et.php">
KDE platvorm: täiustatud multimeedia-, kiirsuhtlus- ja semantilised võimalused
</a>
</h3>

<p>
<a href="platform-et.php">
<img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.7.0"/>
</a>

Suur hulk KDE ja kolmanda poole tarkvara saab tulu ulatuslikust arendustööst Phononi kallal ning olulistest täiustustest semantilise töölaua komponentide seas, mille API on tublisti täienenud ning stabiilsus suurenenud. Uus KDE Telepathy raamistik võimaldab lõimida kiirsuhtluse otse töötsoonidesse ja rakendustesse. Jõudluse ja stabiilsuse paranemine peaaegu kõigis komponentides tagab etema kasutajakogemuse ning vähendab KDE platvormi 4.7 kasutavate rakenduste koormust süsteemile. Täpsemalt kõneleb kõigest <a href="platform-et.php">KDE platvorm 4.7 väljalasketeade</a>.
<br /><br />
<br /><br />

</p>
