<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.15.90 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.15.90";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/bluedevil/d2d0a5ba095429eb856a96a461415b7c4f5eed92'>Commit.</a> </li>
<li>Plasmoid: Make it possible to connect to a device using a touchscreen. <a href='https://commits.kde.org/bluedevil/bc0b2e4a244c7383739d4a197ad8fb34251e2342'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19962'>D19962</a></li>
<li>Create QApplication before using I18N. <a href='https://commits.kde.org/bluedevil/777bbe6e85367c1ddfa00e137e91d4d4fef6c33f'>Commit.</a> </li>
<li>Create QApplication before using I18N. <a href='https://commits.kde.org/bluedevil/0b871355b3c9c3a45e5abf780b1bbe87f5d234a6'>Commit.</a> </li>
<li>Do not use SYSTEM when including header paths. <a href='https://commits.kde.org/bluedevil/9ee6488290a8bcf87921ee73d324506f834f5697'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19675'>D19675</a></li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Revert "Reduce the indicator arrow size for press-and-hold menus in QToolButtons". <a href='https://commits.kde.org/breeze/c9aa535863e09bf736b1fb64c182b93530780c5d'>Commit.</a> </li>
<li>Add missing link. <a href='https://commits.kde.org/breeze/5487826b1249471aba0baffba58a9e4cb211dcae'>Commit.</a> </li>
<li>Reduce the indicator arrow size for press-and-hold menus in QToolButtons. <a href='https://commits.kde.org/breeze/27bcd1be9c2c02067bf15c62fdb7f7e2ad394bd9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19890'>D19890</a></li>
<li>Sharpen ApplicationMenu, Shade, ContextHelp icons. <a href='https://commits.kde.org/breeze/be9f150abe2dbd1615a5323280f7c548eee5a93f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19605'>D19605</a></li>
<li>Make shadow sizes linear. <a href='https://commits.kde.org/breeze/8bfe474ac5854c92577b2aaea566d0638da7413a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19148'>D19148</a></li>
<li>Multiply radius by 0.5 in calculateBlurStdDev(). <a href='https://commits.kde.org/breeze/cd04f42432a6a036f2f6359d7ec716ad5febc484'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19134'>D19134</a></li>
<li>Change shadow color to 0,0,0 for KStyle. <a href='https://commits.kde.org/breeze/3e78fa28585e1237596aeb74a8269d40c0d3b790'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19125'>D19125</a></li>
<li>Change shadow color to 0,0,0. <a href='https://commits.kde.org/breeze/dce32ce1e0edcc1fb8283fe0c2daa84f629dd92f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19124'>D19124</a></li>
<li>Fix Breeze Light color scheme to match regular Breeze version, where appropriate. <a href='https://commits.kde.org/breeze/07e94543b2b09c83ecea8ef050964bbbdb160b76'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19091'>D19091</a></li>
<li>Make Breeze theme tooltip texts consistent and rename Breeze Snow cursors to Breeze Light. <a href='https://commits.kde.org/breeze/1befbfed8bbee4b9f1fcadcbe0d9e303d8cb09a4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19046'>D19046</a></li>
</ul>


<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Fix colours of header bar for acitve and inactive windows. <a href='https://commits.kde.org/breeze-gtk/df538d1492948eaf4b2239d6eef63041d59d2772'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403426'>#403426</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19603'>D19603</a></li>
<li>Remove border around menubars. <a href='https://commits.kde.org/breeze-gtk/1c66db4f34c984feb0f63bb827cdedc61053c961'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12055'>D12055</a></li>
<li>Explicitly set charset to UTF-8. <a href='https://commits.kde.org/breeze-gtk/6a6cc0d5568921d7abc08c6891f8d987f4d95b2e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19931'>D19931</a></li>
<li>Remove ignored style properties from CSS. <a href='https://commits.kde.org/breeze-gtk/7687c366c69fb31747de3c5321f5cd869ca5650d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18999'>D18999</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/discover/797dd88da7b71fac987c30cb2d889d278fbb0709'>Commit.</a> </li>
<li>Use a more conventional sidebar header apparance in desktop view. <a href='https://commits.kde.org/discover/75e24b05e76860f3bf4ae1062427801dada42f2d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21234'>D21234</a></li>
<li>Fix color deprecation warning. <a href='https://commits.kde.org/discover/f2369a2d5c1cd1deadcb1fbe95eef0ea414c7d8c'>Commit.</a> </li>
<li>Use view background color for sidebar to match other sidebars. <a href='https://commits.kde.org/discover/1658a360040c9cca24520b152639085ebde15b5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21229'>D21229</a></li>
<li>Flatpak: Follow redirects when getting flatpakref files. <a href='https://commits.kde.org/discover/0b4837cb9699556bffdd99173e3516c9ea7df8d3'>Commit.</a> </li>
<li>Actually remove that import entirely since it's not even used. <a href='https://commits.kde.org/discover/cab5d4dfa440dcfc068088f0903d91fb5bd0c7a6'>Commit.</a> </li>
<li>One more header guard needed. <a href='https://commits.kde.org/discover/f8575d215efa558b6eed207984c9ca6ae758749e'>Commit.</a> </li>
<li>Add ifdef guard around new Appstream spdx import. <a href='https://commits.kde.org/discover/3a463c267a7b80c1a5179b8c7aa5dc1e6014d7a0'>Commit.</a> </li>
<li>Fix license display. <a href='https://commits.kde.org/discover/9eb621e00af8838091857200fbd2578695591f60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402687'>#402687</a></li>
<li>StandardUpdater: improve how progress is calculated generically. <a href='https://commits.kde.org/discover/a86eb768544206bf7637f67833e644d617645eda'>Commit.</a> </li>
<li>Dummy: add more screenshots, make sure they all work. <a href='https://commits.kde.org/discover/03c0dbc797ecaffa8063ff1b1b9624c5fff3c2a6'>Commit.</a> </li>
<li>Flatpak: Clarify creating and cleanup of metadata fetching. <a href='https://commits.kde.org/discover/d3d3bba6817202aa84d89d85ff6c16c1608f140e'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/e2313b29679bcd4ff1cb1ade163102745f9308b0'>Commit.</a> </li>
<li>Remove custom parsers of the /etc/os-release file. <a href='https://commits.kde.org/discover/67baa6c63704a1df8bf55e7554872d995e5864e9'>Commit.</a> </li>
<li>Remove irrelevant part of the comment. <a href='https://commits.kde.org/discover/a0cdde8e168105435b48862fd58d96605d0e844f'>Commit.</a> </li>
<li>Flatpak: follow the umask recommended by flatpak upstream. <a href='https://commits.kde.org/discover/ec7920d6a1b26b1746303a9ce3838de659ceeca2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407115'>#407115</a></li>
<li>Add DesktopEntry to notifyrc. <a href='https://commits.kde.org/discover/b2a35f5abcc24e984c4418d1a13a20b691a5407e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20919'>D20919</a></li>
<li>Fix warning. <a href='https://commits.kde.org/discover/67e2831913860cb710b3e67d682b5cb285f8898a'>Commit.</a> </li>
<li>Flatpak: Make sure we build with older flatpak versions. <a href='https://commits.kde.org/discover/bfb956c892c6db4f79131ccfde4cb2e688c6346a'>Commit.</a> </li>
<li>Flatpak: also load runtimes at start. <a href='https://commits.kde.org/discover/fcdf9401ebef3693fec5fa7646b5f2644c23832c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403478'>#403478</a></li>
<li>Flatpak: also load runtimes at start. <a href='https://commits.kde.org/discover/67d858dd422728e8928d9488ca887bd04f7b899a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403478'>#403478</a></li>
<li>[Updates page] Set the busy indicator's size in a less verbose way. <a href='https://commits.kde.org/discover/98013cae6ca91ace389fe2f183b98cdcfbc125e6'>Commit.</a> </li>
<li>Flatpak: mark enums as Q_ENUM for optimal debugging. <a href='https://commits.kde.org/discover/6fbf0bcf16300e692f6417e35ce9db0803a8e54d'>Commit.</a> </li>
<li>Let QQuickShortcut decide what's the best string to offer. <a href='https://commits.kde.org/discover/d4c4f21bdf19b95c8478d7364e104e8406068621'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406035'>#406035</a></li>
<li>Increase snapd-glib version required for the latest features. <a href='https://commits.kde.org/discover/953e69bf845a1b2a0b9ad927b33a856cb029c1f7'>Commit.</a> </li>
<li>Snap: support finding packages by appstream id. <a href='https://commits.kde.org/discover/c169387b3b21beac9b4c77db3a1d24a2c1f01c74'>Commit.</a> </li>
<li>Snap: Stop using deprecated API. <a href='https://commits.kde.org/discover/4cdcd02ecffeae5272f53839318ce70b9b49f231'>Commit.</a> See bug <a href='https://bugs.kde.org/405004'>#405004</a></li>
<li>Snap: readability. <a href='https://commits.kde.org/discover/ffe1bddd07f2a9e38c57379d421456a84d9b77e0'>Commit.</a> </li>
<li>Snap: Use OdrsRatingsBackend for Snap as well. <a href='https://commits.kde.org/discover/cd52626a596eb268b09c6cfa513ef66d78103cd9'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/discover/96fa705d15f75d22ef927924ac2de0d8f44a61e2'>Commit.</a> </li>
<li>Improve dealing with updates. <a href='https://commits.kde.org/discover/817101c32a9a36b6bb7ee31dfe784dd5e05a7078'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406659'>#406659</a></li>
<li>Add flatpak mime types. <a href='https://commits.kde.org/discover/1f8acd37773dcc2b15c0b7fedebddcacfb9ebc5d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20242'>D20242</a></li>
<li>LoadingPage: Use Kirigami.Heading. <a href='https://commits.kde.org/discover/f061345ad6fe0e2e7f6d80573a098673d8fb90f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20675'>D20675</a></li>
<li>Word wrap "No backends found" error message. <a href='https://commits.kde.org/discover/4d644303be08125238ec6241f33ef237df7225dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20674'>D20674</a></li>
<li>Allow to close Discover when there's ongoing jobs. <a href='https://commits.kde.org/discover/3aadd3aa4c02bfa427050e8933d32e063cecabff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403465'>#403465</a></li>
<li>Improve startup sequence. <a href='https://commits.kde.org/discover/246ea726318282417bcdb7d6526521cd5a667c3d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403682'>#403682</a></li>
<li>Packagekit: Don't display packages that extend themselves. <a href='https://commits.kde.org/discover/9d49c268cf22cf209e1712dde1289c504077b4c3'>Commit.</a> See bug <a href='https://bugs.kde.org/403118'>#403118</a></li>
<li>Improve the updates page. <a href='https://commits.kde.org/discover/b7ebd04e894565a824b5d1f3ef11d00ad6597817'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404340'>#404340</a></li>
<li>Don't offer system updates while offline. <a href='https://commits.kde.org/discover/0ef1323345b9733bfdf295b3f33eef372ad5268b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404339'>#404339</a></li>
<li>Fix warning. <a href='https://commits.kde.org/discover/fc903987492c29315a47c75fc9139ad9ef5a5db2'>Commit.</a> </li>
<li>Add debug information when a backend is being slow. <a href='https://commits.kde.org/discover/24f71827a88d4c323176c83a4a5169e5ff4dac6e'>Commit.</a> </li>
<li>Use correct syntax for tag filters in KNS backend. <a href='https://commits.kde.org/discover/80bdde1ad0d746c4cd210cf15466197a59b2aa0a'>Commit.</a> </li>
<li>Make KNS applications act more like applications. <a href='https://commits.kde.org/discover/727d8e55e46c7765a8bb2e1704cc083376d0a115'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20378'>D20378</a></li>
<li>Adapt an x option for app-carrying knsrc files. <a href='https://commits.kde.org/discover/6589e22f08e79cd4c70f8f8a66e4dba00142dda7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20406'>D20406</a></li>
<li>Split up subcategories for KNS categories. <a href='https://commits.kde.org/discover/29be60b13647aa55170f05cfd7dfba08dd7f81ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19925'>D19925</a></li>
<li>Notifier: improve behavior on the system tray. <a href='https://commits.kde.org/discover/7765acd5a2db70f826f6c90ca84eac288cd0fa0e'>Commit.</a> </li>
<li>Fix showing update notification. <a href='https://commits.kde.org/discover/ace22790d6328eaa3abcb93b9f34793d4b93e550'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20161'>D20161</a></li>
<li>Update URLs to use https. <a href='https://commits.kde.org/discover/428f4171c4945478032ca9e14162c25c466fa941'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20014'>D20014</a></li>
<li>QNetworkReply was not deleted. <a href='https://commits.kde.org/discover/4adbb68bf40515b034952d73f8a3197035c2d7eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19799'>D19799</a></li>
<li>Adapt leaf Category names for kns backend, and ensure correct filters. <a href='https://commits.kde.org/discover/c4e8f04a24893b656ee0d1dc1847c7f17010960d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19899'>D19899</a></li>
<li>Fix crash in category comparison caused by incorrect filters. <a href='https://commits.kde.org/discover/356fe50194366f8003f77f93d870df71f72ef4c9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19880'>D19880</a></li>
<li>Ignore source packages when adding them to the PK list. <a href='https://commits.kde.org/discover/eccaa65c9672e91e5be7f9ce95670fc79b704127'>Commit.</a> </li>
<li>Fix clazy warnings that make sense. <a href='https://commits.kde.org/discover/954a5c3216d6725959e439c4b00a4eece86ad4c3'>Commit.</a> </li>
<li>Kns: Show subcategories for things that have one. <a href='https://commits.kde.org/discover/3dfd7cbc8e4b84c2d2cae5bc355189c11235a6bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19826'>D19826</a></li>
<li>Add support for applications in KNSBackend. <a href='https://commits.kde.org/discover/ffc6023c5412f17c376a828c72e404c9e12d8fad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19837'>D19837</a></li>
<li>Use ProgressBar to show progress for the Tasks item. <a href='https://commits.kde.org/discover/aa9fa36979d8f16652768d461f8070af4e379fce'>Commit.</a> </li>
<li>Improve update states' UI. <a href='https://commits.kde.org/discover/95c39a6f7f2bf7ddf69d8aef558012fe2dff305d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402897'>#402897</a>. See bug <a href='https://bugs.kde.org/396140'>#396140</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19825'>D19825</a></li>
<li>Updates: Don't show delegates after they are done. <a href='https://commits.kde.org/discover/d35df7250110c41fa6eb8d9fbfa054d8e664de59'>Commit.</a> </li>
<li>Switch to 2 progress bars on the UpdatesPage delegate. <a href='https://commits.kde.org/discover/4ef91fa6f7335385c359add0a1f22f37fd8808ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396140'>#396140</a></li>
<li>Kns: install knsrc file in the right new place. <a href='https://commits.kde.org/discover/6e20f97995b26ddbf1cf694cd2e25d45a86bd1ee'>Commit.</a> </li>
<li>--warning. <a href='https://commits.kde.org/discover/1bb2498835bf063136fa3ef4d97e38ebbf70dbd0'>Commit.</a> </li>
<li>--warning. <a href='https://commits.kde.org/discover/a952f199dcb870b4f38b8245a2a7244a83207670'>Commit.</a> </li>
<li>Notifier: open the updates page upon click. <a href='https://commits.kde.org/discover/8dcf0c3461faf23656cc819a5a45deff1be1d6e9'>Commit.</a> </li>
<li>Search new knsrc locations. <a href='https://commits.kde.org/discover/d617dd8e6a1ea0f255797c565cfe92b369d3d502'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19340'>D19340</a></li>
<li>Don't calculate updates on the updates notification plasmoid. <a href='https://commits.kde.org/discover/2d8c028cd3955055289f3109fce2e3068fd2dd7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385754'>#385754</a>. Fixes bug <a href='https://bugs.kde.org/405533'>#405533</a></li>
<li>Use the word "restart" instead of "reboot". <a href='https://commits.kde.org/discover/69a41b42db73299599020b72cd1d2fd6ed7fffb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405352'>#405352</a></li>
<li>Port deprecated qSort. <a href='https://commits.kde.org/discover/709f5d9cd53accf7fa401e4100dcc271be13c103'>Commit.</a> </li>
<li>Fix warning == vs ===. <a href='https://commits.kde.org/discover/8f6cfe788aade81a2c76e2bf7c3ff63e2cbeb546'>Commit.</a> </li>
<li>Don't crash if the fwupd error is null. <a href='https://commits.kde.org/discover/65d7fa3bbe07f32ad7d6ac7c8581729458519479'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19561'>D19561</a></li>
<li>Fix qml warning. <a href='https://commits.kde.org/discover/e9e262455599f2c472ec9e05c4e06203439a4d23'>Commit.</a> </li>
<li>ApplicationPage: include the available version when selecting the source. <a href='https://commits.kde.org/discover/f6ec0a29c42a746d28cecde7af7a06e5fb505057'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19119'>D19119</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/discover/b0296dbb6ba289d8ddbaf6148e9250a95da6899f'>Commit.</a> </li>
<li>Use Kirigami.ActionTextField for the SearchField. <a href='https://commits.kde.org/discover/0bb9bd345f0a0f3ed80d8efa51a3149606de5891'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19301'>D19301</a></li>
<li>Forward the full icon instance for action bridges. <a href='https://commits.kde.org/discover/f9b14da7d6d40ba90282b6cacdb07fa57a9f01e2'>Commit.</a> </li>
<li>I18n: Add context to "Cannot fetch sources". <a href='https://commits.kde.org/discover/43f8d067aa1025b962590c6631d8793a3c3a9c57'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19293'>D19293</a></li>
<li>Improve/correct string formatting from the prior commit. <a href='https://commits.kde.org/discover/09d70dc0d9c01b703b9872e866151a537a6613ff'>Commit.</a> </li>
<li>[Updates page] Improve selected/unselected explanation view. <a href='https://commits.kde.org/discover/5d254de6fa4f1c7608f3b01a5f582b3d13ef2760'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404603'>#404603</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19192'>D19192</a></li>
<li>Add (and use) a LinkLabel component. <a href='https://commits.kde.org/discover/917819ac26f320c9a8aaec2d871b54ceb978398d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404309'>#404309</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19118'>D19118</a></li>
<li>--verbose. <a href='https://commits.kde.org/discover/8875ed0b356a6c1b29e091b4f93fa0db42e18d0a'>Commit.</a> </li>
<li>Flatpak: allow matching resources by id. <a href='https://commits.kde.org/discover/f4496d65a5d94a3d3dfd2ab1c2131d7918d71f6c'>Commit.</a> </li>
<li>Link against AuthCore instead of Auth. <a href='https://commits.kde.org/discover/916ff7fa7a46a56323ae8f83f3d94828848083af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19062'>D19062</a></li>
<li>FeaturedModel: improve mobile check. <a href='https://commits.kde.org/discover/efe80215e5b1763874989fbbdd952dd4d8fb6ece'>Commit.</a> </li>
<li>Set parent on newly created fwupd resource. <a href='https://commits.kde.org/discover/8c673e79ab452254051d158c885b9b4bf127392e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402328'>#402328</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18946'>D18946</a></li>
<li>Have a separate featured apps list for mobile. <a href='https://commits.kde.org/discover/b41f8448dc229b899c6e7e811969ac9f861b9497'>Commit.</a> </li>
<li>Don't accept invalid KNS EntryInternal results. <a href='https://commits.kde.org/discover/b11cdd776c931d1720d28ac6eab7b8ddaec876e1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18760'>D18760</a></li>
<li>Prettify and search enable the error page. <a href='https://commits.kde.org/discover/9144f96baa5c9648c35e0abfb20ce7745623e484'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18318'>D18318</a></li>
<li>Search-enable the application page. <a href='https://commits.kde.org/discover/9aa0a7df13957446ebdf9fa7c3601392c079186c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18320'>D18320</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/drkonqi/326d0f3d49f6c3d8ef69f23d831bb16d41017fbd'>Commit.</a> </li>
<li>Add mappings for org_kde_powerdevil and kiod5 processes. <a href='https://commits.kde.org/drkonqi/59e8c5ed0f282babe304ceae5246a2cc0f15f6a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405840'>#405840</a></li>
<li>Always support all command line options. <a href='https://commits.kde.org/drkonqi/abc37a6aef2dcd1bc3e052bfcca052f98c5f3d48'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19940'>D19940</a></li>
<li>Don't connect to lambdas and call slots when you can just call the slots. <a href='https://commits.kde.org/drkonqi/d0974fdc18b4139a612054b505e3aa679ae7c637'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19937'>D19937</a></li>
<li>Use https for links in the UI. <a href='https://commits.kde.org/drkonqi/f6eb9ba3dd922921705f2d69d62e7e927560f04a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19894'>D19894</a></li>
<li>﻿Add application path as valid path for debuggers. <a href='https://commits.kde.org/drkonqi/8c74d90e28fb3474fe78ac449a49761f08dbd19f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19166'>D19166</a></li>
<li>Drop explicit disconnect in dtor. <a href='https://commits.kde.org/drkonqi/f7fbcff86ac49c97e38edeee286a83261b358458'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19684'>D19684</a></li>
<li>Move version definition into generated header instead of passing it on CLI. <a href='https://commits.kde.org/drkonqi/082d0f1bb10e81d3fb0528aaf73db0a6b7603dea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19543'>D19543</a></li>
<li>Map powerdevil daemon to product. <a href='https://commits.kde.org/drkonqi/3a77c89068f7134361383224e1e9a75d4cdaea52'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19180'>D19180</a></li>
<li>Typos--. <a href='https://commits.kde.org/drkonqi/4adb9cc1c9eda2361f59b918154687a589e55790'>Commit.</a> </li>
<li>Cdb: First working version. <a href='https://commits.kde.org/drkonqi/a32ec5e298a4e74a6fc300c8d05cd186b657d1f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18859'>D18859</a></li>
<li>CMakeLists: Fix bfd.h include path on windows. <a href='https://commits.kde.org/drkonqi/3102b477d03dc47a98aa1649731339c3d2614b6f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18801'>D18801</a></li>
<li>Fix pragma once problem with process class. <a href='https://commits.kde.org/drkonqi/e01d73c3c3be989414ee06253f552d1ed80a85d8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18802'>D18802</a></li>
<li>Mingw_generator: Fix iterator type in operator==. <a href='https://commits.kde.org/drkonqi/7106b832e9d458e24d9e216c8dd522ba0e01a7c4'>Commit.</a> </li>
<li>Mingw_generator: Fix argument type. <a href='https://commits.kde.org/drkonqi/c850a7c8180c369b373708298e0884df2c009df5'>Commit.</a> </li>
<li>Mingw_generator: Fix iterator type. <a href='https://commits.kde.org/drkonqi/35eba45f1ec43773bff633f81a18c40b125aa8c2'>Commit.</a> </li>
<li>Msvc_gnerator: Fix PCHAR cast to QString. <a href='https://commits.kde.org/drkonqi/aac1c1067eb53a57273c47dbf8934906139bf131'>Commit.</a> </li>
<li>QtCore is common between MINGW and MSVC. <a href='https://commits.kde.org/drkonqi/64b91d5c3f0c28c957214c92674f56381ef7be74'>Commit.</a> </li>
<li>Request change of ptrace scope from KCrash. <a href='https://commits.kde.org/drkonqi/9f3cfcff960c22d382714754aa5c9d29445d805d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11235'>D11235</a></li>
<li>Use nullptr. <a href='https://commits.kde.org/drkonqi/aa781f86780a71015021a75562941c60f1ba195c'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kactivitymanagerd/7d1710d4a0e5ac815477f1e87729d9d9f3f58fff'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kde-cli-tools/e2364e51a5ab34975bfe7a4271f7dfa0e68db140'>Commit.</a> </li>
<li>Add support for passing cursor information via URL parameters when running kioclient exec. <a href='https://commits.kde.org/kde-cli-tools/8072a6acf221b8b609a781290ee2870ca2eea3dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18296'>D18296</a>. Fixes bug <a href='https://bugs.kde.org/398998'>#398998</a></li>
<li>[KEditFileType] Make heading with mime type selectable. <a href='https://commits.kde.org/kde-cli-tools/9282d20d1b155821a1844a11a47d6c95864e5318'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18767'>D18767</a></li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Improve the look of the GTK Application Style KCM UI. <a href='https://commits.kde.org/kde-gtk-config/36dd29f8b65a4a97d036cafd963cd2a38d5831f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19790'>D19790</a></li>
<li>Use better KDE GTK Config KCM desktop file comment. <a href='https://commits.kde.org/kde-gtk-config/bceb50cb8ec39b44b259ad8b38be0cb84ba5f619'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19009'>D19009</a></li>
<li>Don't use British English spelling. <a href='https://commits.kde.org/kde-gtk-config/de23a94b5beb89ce28d20e260ed3c1cadeb1349c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18899'>D18899</a></li>
<li>Actually remove unused ksnrc file. <a href='https://commits.kde.org/kde-gtk-config/f43864feb2d0edf26c460906ce30c7450985b569'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18428'>D18428</a></li>
</ul>


<h3><a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kdecoration/f9f7e85749c8443a637faa9482cb0fe2794e9072'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kdeplasma-addons/8e46a1b59f081b1c3020810df441acc6fd3fbaca'>Commit.</a> </li>
<li>Remove QQC1 now that the bug has been fixed. <a href='https://commits.kde.org/kdeplasma-addons/13e07f69884f613eb1a7e08441fdd586b93c4e41'>Commit.</a> </li>
<li>[Notes] Fix bad merge and restore triple-equals comparison. <a href='https://commits.kde.org/kdeplasma-addons/61ab2de7bf19a496e11085245b2fa65a48bd6408'>Commit.</a> </li>
<li>[Notes] Port to QQC2 and use GridView KCM components for the color chooser. <a href='https://commits.kde.org/kdeplasma-addons/4f163efbc4c45bcb8fc2041267222282c9d3d616'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21221'>D21221</a></li>
<li>[Media Frame] Port "Paths" category to QQC2+Kirigami and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/e6ad673c26b2bef876c2476fedc8ab22fb20c506'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21191'>D21191</a></li>
<li>[Astronomical Calendar Plugin] Port to QQC2+Kirigami Formlayout. <a href='https://commits.kde.org/kdeplasma-addons/4b1607879f23d69d61bed10904164d9933da3fd6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21193'>D21193</a></li>
<li>[dict] Modernize configuration window. <a href='https://commits.kde.org/kdeplasma-addons/781e66cb2d051f16085bea0e951468dee665bd9a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20797'>D20797</a></li>
<li>Require KF5 5.58.0 like the rest of Plasma. <a href='https://commits.kde.org/kdeplasma-addons/2a2e73781481d19a5c4cd5a1f008aadc6524c356'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21129'>D21129</a></li>
<li>[quicklaunch] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/12eba77542c10c4616089d927df0c6cb19457c25'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21088'>D21088</a></li>
<li>[System Load Viewer] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/864c0bf28a09959e65e3164e11acb3ad0fab1d85'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21056'>D21056</a></li>
<li>[potd] Modernize configuration settings. <a href='https://commits.kde.org/kdeplasma-addons/13609c5ae7b9061125cdb62b79558fa863d3eff9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20969'>D20969</a></li>
<li>[Weather] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/007a86a603df58b1de709787ab56f3b491fe812a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20896'>D20896</a></li>
<li>[Timer] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/ad66b200036e0960d55197249e36afd1a46ba02b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20905'>D20905</a></li>
<li>[User Switcher] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/kdeplasma-addons/c097927dee2117f673a615e5ec6da28b8dce0764'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20899'>D20899</a></li>
<li>Remove the Show Desktop applet (it's in plasma-desktop now). <a href='https://commits.kde.org/kdeplasma-addons/00cd377ecc5754187f1f0fa02435802fcd6dc858'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20817'>D20817</a></li>
<li>[FifteenPuzzle] Port configuration window to QQC2 and Kirigami.FormLayout and improve UI. <a href='https://commits.kde.org/kdeplasma-addons/ad8f253d8c7656c4192c1ead4651dc8c71fb68e8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20747'>D20747</a></li>
<li>[comic] Modernize configuration windows. <a href='https://commits.kde.org/kdeplasma-addons/f2647deeeac7cc91dbc719e1456a978135fa39f7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19802'>D19802</a></li>
<li>For the moment this runner is disable but this reply was never deleted. <a href='https://commits.kde.org/kdeplasma-addons/cf7bc3371be4399fca2374a3f828d6f82d97ea41'>Commit.</a> </li>
<li>[color-picker] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/kdeplasma-addons/fb4fcae3f94ea97d1522e69700c7d5b09b3753a2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19789'>D19789</a></li>
<li>[fuzzy-clock] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/kdeplasma-addons/e32e12df50d30c2cfee7fded6102c0dd663e4355'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19747'>D19747</a></li>
<li>[quickshare] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/kdeplasma-addons/1a171706e7b396e70d801e28deb6b5437fb7209c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19664'>D19664</a></li>
<li>[binary-clock] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/kdeplasma-addons/7437b39cf8d0ed557ffc1454ec43bff4c0e6de34'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19669'>D19669</a></li>
<li>[Media frame] Modernize and improve settings window. <a href='https://commits.kde.org/kdeplasma-addons/d0c5e541d7673fa445eb4ec0bf51dd0d6821c74a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390571'>#390571</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19548'>D19548</a></li>
<li>[Web Browser] Use more appropriate panel icon and increase default size. <a href='https://commits.kde.org/kdeplasma-addons/47a8dd784daf1eab52fc60d0fcb38117e3a5477c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19417'>D19417</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/kdeplasma-addons/389e837fe71d760cf3a0d607d2c7a34527427ca0'>Commit.</a> </li>
<li>[Color Picker] Allow dragging color button to drag color. <a href='https://commits.kde.org/kdeplasma-addons/e0f0400dc7f263a29f86cf71924de6e9e24836d0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18700'>D18700</a></li>
<li>[potd] Remove explicit use of ECM_KDE_MODULE_DIR, part of ECM_MODULE_PATH. <a href='https://commits.kde.org/kdeplasma-addons/2a5461b480c5296d82e381a56a93f31570175aae'>Commit.</a> </li>
<li>Consistent arrow key handling in the Informative Alt+Tab skin. <a href='https://commits.kde.org/kdeplasma-addons/80bd32b12a01e29f3fe88f2a50c0a2a842872c19'>Commit.</a> See bug <a href='https://bugs.kde.org/370185'>#370185</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16093'>D16093</a></li>
</ul>


<h3><a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kgamma5/fa7aaa3094c845e8bdae6045012ecfcae4613396'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/kgamma5/988ba027ad36b36e0215984209bda0407c329596'>Commit.</a> </li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/khotkeys/d5f439f8d7550210f4a10063a5e80b086fce696f'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/khotkeys/866602c2f6e28ba2df398c90a1cc306c9588d0cc'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kinfocenter/e99ad8fad9cc1b8e10740c51e7b7f96e7a443359'>Commit.</a> </li>
<li>Drop internal os-release class in favor of KOSRelease of kcoreaddons 5.58. <a href='https://commits.kde.org/kinfocenter/a869a8b12f9a04def85668e46889fcb1240a8b7a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21209'>D21209</a></li>
<li>Extends keywords of device info module. <a href='https://commits.kde.org/kinfocenter/0dda3bf3b43898ed377273843a1fe42fbaadbda1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21186'>D21186</a></li>
<li>Kinfocenter. <a href='https://commits.kde.org/kinfocenter/ed22fbbacc5ff7077a46db45b7fdf78cb8a62759'>Commit.</a> </li>
<li>Correctly show memory sizes > 4 GiB on 32 bit Linux. <a href='https://commits.kde.org/kinfocenter/10d60170eaa46c3e4233c4eef966418d0065754d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406351'>#406351</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20828'>D20828</a></li>
<li>[about-distro] OSRelease to collect "extra" keys. <a href='https://commits.kde.org/kinfocenter/e65bcdc907191029a531c11118c537a103c1d503'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19976'>D19976</a></li>
<li>[about-distro] pimpl OSRelease privates. <a href='https://commits.kde.org/kinfocenter/620be5cd4dea5524058d50f2dc3bacdb5f70bc2b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19975'>D19975</a></li>
<li>Fix initial width/height in KCMs that use qml. <a href='https://commits.kde.org/kinfocenter/43bfb812df95ddddb3e33107e2ba5fc9e1680332'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398820'>#398820</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20027'>D20027</a></li>
<li>Color monochrome icons to tooltip colors. <a href='https://commits.kde.org/kinfocenter/23fa369831709f43a3100476b00ced10a1123f7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19596'>D19596</a></li>
<li>Remove deprecated methods. <a href='https://commits.kde.org/kinfocenter/4c325004fb7c790fed6a71fd12e7d67949d27119'>Commit.</a> </li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kmenuedit/c7e88c72cb4fa9b5ce19e1466d5c88d7d738f1c1'>Commit.</a> </li>
<li>Fix sorting to use locale-aware comparisons. <a href='https://commits.kde.org/kmenuedit/fb171dff39c7976d8891b6c9a848857439e660ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404393'>#404393</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20386'>D20386</a></li>
<li>Port to std::sort. <a href='https://commits.kde.org/kmenuedit/e9107e050ecd8d197e1b9d1676b7bec2252bb4f6'>Commit.</a> </li>
<li>Add search/filter bar. <a href='https://commits.kde.org/kmenuedit/3f6c86e1f26d32c6f0452676c10debff02bffbc4'>Commit.</a> Implements feature <a href='https://bugs.kde.org/57314'>#57314</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18939'>D18939</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kscreen/e06d28cab3f2e68da82bb1c6edfbdb8c62bb515d'>Commit.</a> </li>
<li>[kcm] Remove profiles code. <a href='https://commits.kde.org/kscreen/7283fb5630dfe51481c24077a6c4d4b901176c98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16898'>D16898</a></li>
<li>Port to std::sort. <a href='https://commits.kde.org/kscreen/ce2a797303662a861e0280858409caae40d0c7fe'>Commit.</a> </li>
<li>Fix warning (== vs ===). <a href='https://commits.kde.org/kscreen/2970915c48af3ba9057c2c8b7a46ec37279358f4'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/kscreen/82772aba3392c226495db7a9aff4c3554653eacd'>Commit.</a> </li>
<li>Don't reset the moved display position. <a href='https://commits.kde.org/kscreen/2c5af49d3de7d4e2306510f6e7dc843dba70e71b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18101'>D18101</a></li>
<li>Calculate screen scaling dynamically, so it always fits to the page. <a href='https://commits.kde.org/kscreen/ca4d74acb2aeba1a5fcddf0cfdbd095054158808'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18093'>D18093</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kscreenlocker/2ca49f8a461a0fce41317703c63998bb7c685cfc'>Commit.</a> </li>
<li>Add Meta+L shortcut for "Lock Session". <a href='https://commits.kde.org/kscreenlocker/505fedc37d7e7989421787939f9ec7a1c6b159d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20178'>D20178</a></li>
<li>Fix window height of Screen Locking KCM. <a href='https://commits.kde.org/kscreenlocker/35864f76ffe83b3fd1dea1aa24ec9c2f439088b2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400355'>#400355</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20086'>D20086</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/kscreenlocker/8aca84a30e4436c309f8f8ca9bbcbad3294e0a49'>Commit.</a> </li>
<li>Remove deprecated code. <a href='https://commits.kde.org/kscreenlocker/c5fd4eae2efbd35a1e6b8c243e13080d592de278'>Commit.</a> </li>
<li>Port test app to QQC2. <a href='https://commits.kde.org/kscreenlocker/029cb0b866a48a33fa6385f4df748e01717afcd7'>Commit.</a> </li>
<li>Fix test app. <a href='https://commits.kde.org/kscreenlocker/17636a69c06671c46b64d4c7aee415e8c03d8949'>Commit.</a> </li>
<li>Fix lock screen focus. <a href='https://commits.kde.org/kscreenlocker/2fdf002b11cf287bd7f244e884c9cd3da47aa693'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395639'>#395639</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17706'>D17706</a></li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/ksshaskpass/4f0b6a9ceee2eca6cf9507ae8229bae68a3fdeac'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/ksysguard/bbff7e6f09928c545ad31eb086bc72b6dfc02963'>Commit.</a> </li>
<li>Port deprecated method. <a href='https://commits.kde.org/ksysguard/722dc38aa64868045afb8d56f91a6f8653ee6e33'>Commit.</a> </li>
<li>Simpe C updates to ksysguardd code. <a href='https://commits.kde.org/ksysguard/b4bde28dc7afc489125ba18efc103dfc3ca244ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18576'>D18576</a></li>
<li>Reduce string objects. <a href='https://commits.kde.org/ksysguard/91b1f7115bf36e7428d6bfd6eff3fbc40cec1980'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18578'>D18578</a></li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Don't hardcode default kwallet path. <a href='https://commits.kde.org/kwallet-pam/fd556ab25365f953af2e6fc867e43292fd8e4b08'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20218'>D20218</a></li>
<li>Remove kwallet4 support. <a href='https://commits.kde.org/kwallet-pam/905b00d380408f54da3de0d8dcc2b14e1cb9f6e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20217'>D20217</a></li>
<li>Add readme explaining how kwallet-pam works. <a href='https://commits.kde.org/kwallet-pam/b0ac7bbd6adf15d9964f288d7bf25451e7beb374'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20081'>D20081</a></li>
</ul>


<h3><a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kwayland-integration/8e793132cf7aec4431e24bf0a666b6c408bc9972'>Commit.</a> </li>
<li>Track surface creation/destruction inside wayland window effects. <a href='https://commits.kde.org/kwayland-integration/0a0c3f23fce265f36e5b8fb2b4b8bd311c7c1beb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398899'>#398899</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20951'>D20951</a></li>
<li>Support CriticalNotification window type. <a href='https://commits.kde.org/kwayland-integration/e2db5ce32d7ba90261c10a3a42d23347c0d810d5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20631'>D20631</a></li>
<li>Add missing window types to WindowInfo. <a href='https://commits.kde.org/kwayland-integration/9436a2c1afe492c57ce14741722523ecc14dbc3a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20624'>D20624</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kwin/0c6705670821e1edc6652e40f07043260c1f403d'>Commit.</a> </li>
<li>[effects/thumbnailaside] Specify screen projection matrix. <a href='https://commits.kde.org/kwin/d94eb29d07c8e5720a99956dc53df4f254774664'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407485'>#407485</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21224'>D21224</a></li>
<li>Compute correct boundaries in checkWorkspacePosition. <a href='https://commits.kde.org/kwin/67b2746ecdfd4c343db9d3fca088ae86d80a57f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404837'>#404837</a>. Fixes bug <a href='https://bugs.kde.org/406573'>#406573</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20987'>D20987</a></li>
<li>Port away from deprecated qFind. <a href='https://commits.kde.org/kwin/0aa288bfae7972d6197ba3aeebbd1a05292841ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21217'>D21217</a></li>
<li>Revert "Handle XdgShell window geometry in configure request sizes". <a href='https://commits.kde.org/kwin/24cbe48656b7d4d6dffff86da8f81a522fff99e1'>Commit.</a> </li>
<li>Handle XdgShell window geometry in configure request sizes. <a href='https://commits.kde.org/kwin/bc83065cebd7ac855966da52edd1afcaf0fc2865'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403376'>#403376</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20937'>D20937</a></li>
<li>Support NET_WM_STATE_FOCUSED. <a href='https://commits.kde.org/kwin/ce1a5eae156a544da06a9a37ee00b6b55dd13bb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398832'>#398832</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19613'>D19613</a></li>
<li>Fix crash due to dangling reference. <a href='https://commits.kde.org/kwin/7804eb41d9548c36254097f235324a3c57c6514f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407199'>#407199</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21085'>D21085</a></li>
<li>[effects/cubeslide] Cancel active animation when number of desktops has changed. <a href='https://commits.kde.org/kwin/e8b45cce1168859e58fe98b45f8060052ed861a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406452'>#406452</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21064'>D21064</a></li>
<li>Don't crash when highlighted tabbox client is closed. <a href='https://commits.kde.org/kwin/c438ecbb702e970004e7e97e6734f57caef32ea5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406784'>#406784</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20916'>D20916</a></li>
<li>Make session loading/saving helpers local to sm.cpp. <a href='https://commits.kde.org/kwin/0fb09cf41313b24867544e7564d34f3dca986378'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20783'>D20783</a></li>
<li>[kcmkwin/kwindecoration] Refine drag-and-drop button UI. <a href='https://commits.kde.org/kwin/48cacae8cfb4f2c1e82d191a19dc4515f1c897f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407109'>#407109</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20968'>D20968</a></li>
<li>Support CriticalNotification type and place it in a CriticalNotificationLayer. <a href='https://commits.kde.org/kwin/df85907de3d096de492a86832dd22ab58bea6ea4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20629'>D20629</a></li>
<li>Improve behavior of rotated displays. <a href='https://commits.kde.org/kwin/559c2e68d34309eaeed0f8d25204789522058a12'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20255'>D20255</a></li>
<li>[kcmkwin/kwintabbox] Use a more appropriate icon. <a href='https://commits.kde.org/kwin/4725ece602902d86a6ff3a51157a21fb3adaa828'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20801'>D20801</a></li>
<li>Delete noop code. <a href='https://commits.kde.org/kwin/b37743d8b1b706db1c0ee7edf0196a102c08fe6d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20777'>D20777</a></li>
<li>Refactor getters in OutputScreens. <a href='https://commits.kde.org/kwin/70b9c3651aa5d6acd2bca87dee76e23197de1a08'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20588'>D20588</a></li>
<li>Avoid duplicating screensaver interface definition file. <a href='https://commits.kde.org/kwin/aa11e4f7772638d301949993ba5c11d935898a1e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20764'>D20764</a></li>
<li>Don't rescale every output every time outputs change. <a href='https://commits.kde.org/kwin/d3275784a85d5efb8460af4d82414f1ada91fa65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20521'>D20521</a></li>
<li>Initialise XdgOutput properties on creation. <a href='https://commits.kde.org/kwin/c4ab885616843c513770e1933a607acdbc6caacd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20800'>D20800</a></li>
<li>Use AbstractClient::Position instead of Client::Position. <a href='https://commits.kde.org/kwin/32863aac9b6907ed7d0eaa1a3d3a62e014d96706'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20778'>D20778</a></li>
<li>Make Compositor::finish more generic. <a href='https://commits.kde.org/kwin/fd20c59ada27448065866e985395b93001722af8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19879'>D19879</a></li>
<li>[scenes/opengl] Ensure there is current OpenGL context when destroying EffectFrame unstyled texture. <a href='https://commits.kde.org/kwin/b617613bf71f872b4b678cab75080ad4d89043c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20723'>D20723</a></li>
<li>Fix corner screen glows that suddenly pop up. <a href='https://commits.kde.org/kwin/e4456347c498bffbda1b4b458482b27014a34fe2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20707'>D20707</a></li>
<li>Initialise the orientation sensor at start. <a href='https://commits.kde.org/kwin/bf6f05bf3cd6493041b27c3936643cdbc8d487ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20622'>D20622</a></li>
<li>Use more accurate name for Workspace::getMovingClient method. <a href='https://commits.kde.org/kwin/abe128818cf30f2d8f1ddb55fd725e167bca9462'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20663'>D20663</a></li>
<li>[libkwineffects] Bump api version. <a href='https://commits.kde.org/kwin/20288555178caed0288cd89fc4b797f08c60c3ca'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20652'>D20652</a></li>
<li>[effects/screenedge] Support "hint-stretch-borders". <a href='https://commits.kde.org/kwin/1fb4baf365ea9fee2907016874c95205adc76c6f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20621'>D20621</a></li>
<li>Revert "Move keep-above clients to the Normal layer when showing desktop". <a href='https://commits.kde.org/kwin/8010e076ee2dbcaaa39f777b2ea6dc96a5495d6f'>Commit.</a> See bug <a href='https://bugs.kde.org/406101'>#406101</a></li>
<li>Remove redundant AbstractOutput::internal() method. <a href='https://commits.kde.org/kwin/af862a9caff4610a7b2e44b61dad8c19486de1f9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20587'>D20587</a></li>
<li>Move Unmanaged-specific hack away from Toplevel::setupCompositing. <a href='https://commits.kde.org/kwin/0db071c218aced78729ae46b9ee7b6e0b535aef6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20590'>D20590</a></li>
<li>[platforms/drm] EGLStream DRM Backend Initial Implementation. <a href='https://commits.kde.org/kwin/c898f96df3e3a2d418ca511fdb6610cf087ca464'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18570'>D18570</a></li>
<li>[qpa] Don't specify EGL_SURFACE_TYPE when choosing EGLConfig. <a href='https://commits.kde.org/kwin/a1e8541b49474efc00224ca1dd9a9acb529ef2c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20455'>D20455</a></li>
<li>[kcmkwin] Actually save effect settings. <a href='https://commits.kde.org/kwin/dd8a0407cafc32571e4b8f165c78e0a6462914e3'>Commit.</a> </li>
<li>Make the Keyboard Layout SNI passive. <a href='https://commits.kde.org/kwin/21a62f0b38708201cbb6e8f7bd03f7a6449308a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20369'>D20369</a></li>
<li>[wayland] Make sure that only the fading popups effect animates outline. <a href='https://commits.kde.org/kwin/3d46801e5fe8fe17fa6040b135a2b057dcc31512'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19886'>D19886</a></li>
<li>[platforms/drm] Rework ScopedDrmPointer. <a href='https://commits.kde.org/kwin/02a5a08a6c9556ec27f4844e616dee5737204361'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19905'>D19905</a></li>
<li>[wayland] Place all toplevels before the first configure. <a href='https://commits.kde.org/kwin/efc62941eeb6161b3b1e3645c5c12f68877dee5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20241'>D20241</a></li>
<li>[wayland] Queue XDG configure requests when resizing toplevel interactively. <a href='https://commits.kde.org/kwin/985601e0a41a84ffab51bc942824bd48bc8a6290'>Commit.</a> See bug <a href='https://bugs.kde.org/403376'>#403376</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20397'>D20397</a></li>
<li>[wayland] Finish active move-resize op when client is destroyed or unmapped. <a href='https://commits.kde.org/kwin/f10760d8a9bee194dd2d02008c6bb02f49ff2da3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405379'>#405379</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19708'>D19708</a></li>
<li>Fix plugins/qpa build with Qt 5.13. <a href='https://commits.kde.org/kwin/3cc39ba35eb2afa9a39925daf58354e500e4da2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406056'>#406056</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20239'>D20239</a></li>
<li>Move keep-above clients to the Normal layer when showing desktop. <a href='https://commits.kde.org/kwin/1e2a0028c3151cb38f252baa78a5f22b8cd7c7e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406101'>#406101</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20153'>D20153</a></li>
<li>Fix crash when using kwin on windowed mode. <a href='https://commits.kde.org/kwin/2e1880b85a02c716f4b1714fc800404c74b0b56b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20056'>D20056</a></li>
<li>Use Meta+D to Show Desktop by default. <a href='https://commits.kde.org/kwin/c795f1389b63803f71aafe33a956ed50c14e3bb4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20066'>D20066</a></li>
<li>Expose AbstractClient::setMaximize to scripting. <a href='https://commits.kde.org/kwin/1d4a9d24f86799b45994c545b054e205f371869f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20044'>D20044</a></li>
<li>Update URLs to use https. <a href='https://commits.kde.org/kwin/008143c9dbb0a74e42840f44dc4d2b440e09ab83'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20017'>D20017</a></li>
<li>Don't show the OrientationSensor if it's not supported. <a href='https://commits.kde.org/kwin/75c0c415cc27583f3014ba4fbb7d5a371ce94d59'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19955'>D19955</a></li>
<li>Make it possible to autodetect the tablet mode. <a href='https://commits.kde.org/kwin/40200565323f16e0f30ab80329fb277ca0854edf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19604'>D19604</a></li>
<li>RFC: Fix wayland backend initialisation. <a href='https://commits.kde.org/kwin/6d37ce7f363d7f22be00f0fb301957c172488137'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19921'>D19921</a></li>
<li>[platforms/x11] Force glXSwapBuffers to block with NVIDIA driver. <a href='https://commits.kde.org/kwin/22a441e071515e9c630f3bdac743c678052f88be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19867'>D19867</a></li>
<li>Don't try to resize desktop or fullscreen windows. <a href='https://commits.kde.org/kwin/d2820bf05efff197b3bd19c5adc4e4c8d91ce3d5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19922'>D19922</a></li>
<li>Fix crash on closing. <a href='https://commits.kde.org/kwin/1f3fd6379079f8d2f70c6a709765c6979de3e8f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19919'>D19919</a></li>
<li>Fix wrong command line argument name. <a href='https://commits.kde.org/kwin/ba22fe0d43a16861fc7f469d38a7bdbb2ba36c36'>Commit.</a> </li>
<li>[platforms/wayland] Multi output support. <a href='https://commits.kde.org/kwin/7b13393b647cb55d49fe5c390270f1c814ed9b03'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18465'>D18465</a></li>
<li>Orientation sensor: Add comments for translators. <a href='https://commits.kde.org/kwin/0ab907c7fdb8ead74a43ef8901184fdb77f799b2'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/kwin/f1fa3a2cab8d6f52bf0e0b48bfb5bc7236aa0504'>Commit.</a> </li>
<li>Virtualkeyboard: resize the focused window to make room for the keyboard. <a href='https://commits.kde.org/kwin/6bc2ddd56a092bd8a02b4ca30e64399b0a432879'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18818'>D18818</a></li>
<li>There's no qml folder here anymore. <a href='https://commits.kde.org/kwin/d51070223b0a5589361e9f06fe11234caa336b58'>Commit.</a> </li>
<li>Improve SNI text. <a href='https://commits.kde.org/kwin/c192d3519281d5fc5e954346d2a4a5610c1f30c7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19690'>D19690</a></li>
<li>Virtual Keyboard SNI: Improve text and usability. <a href='https://commits.kde.org/kwin/9e42ff2f4823e8fc34f54eeae3eac7476601c0a7'>Commit.</a> See bug <a href='https://bugs.kde.org/405397'>#405397</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19898'>D19898</a></li>
<li>[platforms/drm] Fix software cursors with drm backend. <a href='https://commits.kde.org/kwin/d9c79e36274c3347087c846704c3add03e435c87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18376'>D18376</a></li>
<li>Get rid of Deleted's properties. <a href='https://commits.kde.org/kwin/c5a3b47050637a1939ee9ef47d718106084184e1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19722'>D19722</a></li>
<li>[platforms/drm] Add DPI connector type. <a href='https://commits.kde.org/kwin/380c93ac19f560fdc99acf0851ae25cb1399c084'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19781'>D19781</a></li>
<li>Rotate cursors also on portrait mode. <a href='https://commits.kde.org/kwin/d8af2ee56617ac6e6e6e6ed8562c3b06cefc2a73'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19710'>D19710</a></li>
<li>[kcmkwin] Properly load effects. <a href='https://commits.kde.org/kwin/56b24f959f0b2ba9a273e8be05e8323906957af8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18728'>D18728</a></li>
<li>[kcmkwin] Rename EffectsModel::m_effectsList to EffectsModel::m_effects. <a href='https://commits.kde.org/kwin/dc0b11eea77f3ee90e4f183acaf2e2aae2bdbabb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18713'>D18713</a></li>
<li>[kcmkwin] Rename EffectModel to EffectsModel. <a href='https://commits.kde.org/kwin/b91cfae3080bb8aea883cda782250c0c723a9d32'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18712'>D18712</a></li>
<li>[kcmkwin] Remove some name duplication in EffectModel. <a href='https://commits.kde.org/kwin/dc3bc5f7b72b3824e7fd8ee555a8157245b04f92'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18707'>D18707</a></li>
<li>[kcmkwin] Simplify EffectModel::save. <a href='https://commits.kde.org/kwin/7a28a7442ecd9acff5b2d9225a8eb8f424df165a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18706'>D18706</a></li>
<li>[kcmkwin] Don't discard unsaved changes when reloading effects model. <a href='https://commits.kde.org/kwin/bc34a9d65379905ecebd190f4248ceed0a947594'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18705'>D18705</a></li>
<li>[kcmkwin] Add requestConfigure to effects model. <a href='https://commits.kde.org/kwin/1faa861bd58ec4195aa1bc161cc91d91a256718e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18704'>D18704</a></li>
<li>[kcmkwin] Split out Desktop Effects KCM. <a href='https://commits.kde.org/kwin/2211a951e795071acf01d9a7085a09dee405dd63'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18703'>D18703</a></li>
<li>Use new window decoration theme icon for the Window Decorations KCM. <a href='https://commits.kde.org/kwin/5eb28512dd077998a6be2ff8aa6262a3c4f4f9af'>Commit.</a> </li>
<li>Remove unused constant. <a href='https://commits.kde.org/kwin/d0604f99f286cd7563e9bd2c668dda406b82acbd'>Commit.</a> </li>
<li>[kcmkwin/kwindecoration] Use new API to set cell size properly. <a href='https://commits.kde.org/kwin/bc25e7c4748bc0dfc403901000febf974c3644e8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19729'>D19729</a></li>
<li>[kcmkwin/kwindecoration] Set implicit size. <a href='https://commits.kde.org/kwin/50e74c5517b8c6526e2b91de2772fff7700413c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19730'>D19730</a></li>
<li>Move package structure plugins to plugins dir. <a href='https://commits.kde.org/kwin/b106579d0ffe288a7cab446726d5c1c042a1576e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18685'>D18685</a></li>
<li>Verify that there is no scene windows when Scene is about to be destroyed. <a href='https://commits.kde.org/kwin/456c65fc19eb49afa14b3a75755199a8b35a11df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19540'>D19540</a></li>
<li>[wayland] Implement belongsToDesktop check. <a href='https://commits.kde.org/kwin/297557bde400a24f95e702d92f7d98fcb4070313'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404801'>#404801</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19386'>D19386</a></li>
<li>Resurrect show tooltips option. <a href='https://commits.kde.org/kwin/82b3e2a63a7d46b163315fd064ca25b6c02cc063'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19152'>D19152</a></li>
<li>Discard Deleted before Scene is destroyed. <a href='https://commits.kde.org/kwin/10fdf2a22097c1ccfa70a850ba7b590871ad7f35'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18914'>D18914</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/kwin/7b4ef76de62733054a5d2007725e310c813813a3'>Commit.</a> </li>
<li>[autotests] Fix race condition in ShellClient::testUnresponsiveWindow. <a href='https://commits.kde.org/kwin/768e5fb7f422f6249e9789876cc97cf9f745f216'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19406'>D19406</a></li>
<li>[autotests] Fail faster on test which fails anyway. <a href='https://commits.kde.org/kwin/6ddf50a77b1371a9bd27d70818eaff9ace9ddd80'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19351'>D19351</a></li>
<li>[wayland] Finish initialising ShellClient only when commited to the surface. <a href='https://commits.kde.org/kwin/2bad2b48fefd46ed36b78a698885336238626a4a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18583'>D18583</a></li>
<li>[autotests] Check the configure request sent to popups. <a href='https://commits.kde.org/kwin/dc7ea09e8d854117ad7932e88dbf78b79395f0f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18955'>D18955</a></li>
<li>[wayland] Squash reconfigure methods. <a href='https://commits.kde.org/kwin/56fb507a76442ca2312216e7da64e3c05e0540f7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19331'>D19331</a></li>
<li>[wayland] Send correct current time in the frame callback. <a href='https://commits.kde.org/kwin/a039f3ba80ab5702966a681cc7eb9713175a1151'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18656'>D18656</a></li>
<li>[libkwineffects] Make AnimationEffect::AniMap protected. <a href='https://commits.kde.org/kwin/dffd9777c74495d844bfd593d33ccfc4b8799fcb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17833'>D17833</a></li>
<li>[wayland] Keep application startup flow inside main_wayland. <a href='https://commits.kde.org/kwin/5e9023948efe9c6adffd46bf600441b96a428b53'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19232'>D19232</a></li>
<li>[wayland] Fix window sizing when restoring a window that was initially fullscreen. <a href='https://commits.kde.org/kwin/7bf2c1d73eb952bef8254320691488f081657c51'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16710'>D16710</a></li>
<li>Restrict available supported compositors based on first initialized one. <a href='https://commits.kde.org/kwin/cbbe94d76920f9c65c5003101dedb4b7ebe8f5ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19084'>D19084</a></li>
<li>[qpa] Drop PlatformContextWayland which uses wayland egl for OpenGL. <a href='https://commits.kde.org/kwin/e48d9df14f3099065c990e4d20287d7e2b022a38'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19012'>D19012</a></li>
<li>[scripting] Make DBus invokable run method return result after execution. <a href='https://commits.kde.org/kwin/47be4be0209fd786b19c5066b96c8550a08c8746'>Commit.</a> See bug <a href='https://bugs.kde.org/403038'>#403038</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18127'>D18127</a></li>
<li>[wayland] Handle sizes in ShellClient::transientPlacement. <a href='https://commits.kde.org/kwin/7014a33992149ef682990c6123516a099233d919'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18956'>D18956</a></li>
<li>Fix tests. <a href='https://commits.kde.org/kwin/e96da56f8ced14499de0e759ba94676d6a56055a'>Commit.</a> </li>
<li>[platforms/hwcomposer] Add scaling support. <a href='https://commits.kde.org/kwin/3dc22d7d8882b1035abf1140e92778611c835bfb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18810'>D18810</a></li>
<li>Reload Shm texture when buffer size changes. <a href='https://commits.kde.org/kwin/d6f98d1ecc3a08ce8cf5c8a994331e909b7a51ca'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18453'>D18453</a></li>
<li>Remove unnecessary flushs in new buffer size change test. <a href='https://commits.kde.org/kwin/d0422eb79f5202ce9331a83596cf4c3cfb3ecd09'>Commit.</a> </li>
<li>[autotests] Sub-surface resize test. <a href='https://commits.kde.org/kwin/06f64d5e567e951ffe355f4b12af8ba3f83225b8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18452'>D18452</a></li>
<li>[platform/virtual] Swap buffers in EGL GBM backend. <a href='https://commits.kde.org/kwin/1e6f6700f8a3299824a8ed29c39e85ec2c7c03bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18436'>D18436</a></li>
<li>Split out a dedicated InternalClient class. <a href='https://commits.kde.org/kwin/9b922f88332fc470dcb50e9b55c7d69ab7da4d4c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18569'>D18569</a></li>
<li>[platforms/hwcomposer] Port to AbstractOutput. <a href='https://commits.kde.org/kwin/062dfefafe06629117abf37b68ef59964fbb9654'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18808'>D18808</a></li>
<li>[wayland] Fix typo in method name. <a href='https://commits.kde.org/kwin/3cdea2f77cbb64b931b9c25dfd07eda688f217b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19177'>D19177</a></li>
<li>[xwl] Support stack optimizing X drag source clients. <a href='https://commits.kde.org/kwin/4c18d156e2fbf68b785d049aa9176d0686905ab1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15628'>D15628</a></li>
<li>[xwl] text/x-uri converter for selected X url list format targets. <a href='https://commits.kde.org/kwin/522d2935e661a368005d5ee20260f3f008b9e46f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15629'>D15629</a></li>
<li>[xwl] Drag and drop between Xwayland and Wayland native clients. <a href='https://commits.kde.org/kwin/548978bfe1f714e51af6082933a512d28504f7e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15627'>D15627</a></li>
<li>[xwl] Add Xwayland interface class. <a href='https://commits.kde.org/kwin/ad1bcbecc78981d9d55411b774e09fbeab414d42'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15419'>D15419</a></li>
<li>Remove X clipboard sync helper and rename its autotest. <a href='https://commits.kde.org/kwin/2776f829efbd0915039e8e4200de6625772b9734'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15063'>D15063</a></li>
<li>[xwl] Generic X selections translation mechanism with Clipboard support. <a href='https://commits.kde.org/kwin/6e08fb2fa5f6f2d03d9ad5ef74519295357c6ba2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394765'>#394765</a>. Fixes bug <a href='https://bugs.kde.org/395313'>#395313</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15061'>D15061</a></li>
<li>[autotests] Use Xwayland class in WaylandTestApplication. <a href='https://commits.kde.org/kwin/608a89a85b319803fca98a5cc8ac6e926917e40e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15022'>D15022</a></li>
<li>[xwl] Move Xwayland parts into separate class. <a href='https://commits.kde.org/kwin/050cf0451e2bea0616f29632148151637464ab37'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15020'>D15020</a></li>
<li>[qpa] Set binary output path for QPA plugin. <a href='https://commits.kde.org/kwin/1d8d7bd9f7ad0c46753059432cd8e8f462d424bb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19010'>D19010</a></li>
<li>Link to AuthCore instead of Auth. <a href='https://commits.kde.org/kwin/76c5371b57f509430d489d768e659a8adb0a5485'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19061'>D19061</a></li>
<li>[effects] Make sure that all effects reset the streaming buffer. <a href='https://commits.kde.org/kwin/0d7415336f0aeed4f85d28761cff78131d20fce6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356141'>#356141</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19034'>D19034</a></li>
<li>[kcmkwin/decorations] Use correct header text. <a href='https://commits.kde.org/kwin/c55568fd8089da1d5e4339c75b647197cb20fc7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19018'>D19018</a></li>
<li>Add support for fake pointer move with absolute coordinates. <a href='https://commits.kde.org/kwin/b4a8977b3ab69c97ee192670407bf607cf2600e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18115'>D18115</a></li>
<li>Add addToplevel and removeToplevel to Scene. <a href='https://commits.kde.org/kwin/d167935157e641e9331b67db47864b7163c27785'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18210'>D18210</a></li>
<li>[effects/logout] Hide configure button in Desktop Effects KCM. <a href='https://commits.kde.org/kwin/96a02c756092d6e1658b2637998a8941ee3e8cb3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18984'>D18984</a></li>
<li>[kcmkwin/kwindesktop] show only on desktop. <a href='https://commits.kde.org/kwin/051539606c29af45a645e36b431a6feb8a318da0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18978'>D18978</a></li>
<li>Overhaul doxygen comments. <a href='https://commits.kde.org/kwin/7b20e1f66f83657591001f473d31555c295e5569'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18683'>D18683</a></li>
<li>[kcmkwin/kwindecoration] Fix Get Hot New Stuff. <a href='https://commits.kde.org/kwin/31dcf51c88438ca85a053cab0ddbf78124078b39'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18935'>D18935</a></li>
<li>[autotests] Fix typo in autotest name. <a href='https://commits.kde.org/kwin/5fa18646b6b908b3316d152a08a2bdefd45c30a7'>Commit.</a> </li>
<li>Follow ECM documentation style. <a href='https://commits.kde.org/kwin/d920b323bd66bc5bde3c4b9e6dca8b289bb35058'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18944'>D18944</a></li>
<li>[platforms/drm] Use more ScopedDrmPointer. <a href='https://commits.kde.org/kwin/5047449a553f288dab9700a58ae7fd5b5c164f4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18863'>D18863</a></li>
<li>Use correct flag when checking whether a scene window is opaque. <a href='https://commits.kde.org/kwin/6006dab41c47da62db17ef2d5df846737a3daa76'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18236'>D18236</a></li>
<li>[kcmkwin/kwindecoration] Rewrite the KWin decorations settings as a ConfigModule. <a href='https://commits.kde.org/kwin/8350c0f2ce666e60ad468a0f2fdce6086d2210f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389431'>#389431</a>. Fixes bug <a href='https://bugs.kde.org/350122'>#350122</a>. Fixes bug <a href='https://bugs.kde.org/346222'>#346222</a>. Fixes bug <a href='https://bugs.kde.org/342816'>#342816</a>. Fixes bug <a href='https://bugs.kde.org/397595'>#397595</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18458'>D18458</a></li>
<li>[kcmkwin/compositing] Delete unused config headers. <a href='https://commits.kde.org/kwin/698b40dbbde327a6824c2c6495201a27b34bc4a2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18850'>D18850</a></li>
<li>Move lanczos filter away from KWin core. <a href='https://commits.kde.org/kwin/be8729105f62ffeebf0ccedce90d4995c9ad1107'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18774'>D18774</a></li>
<li>Make whitespace in Deleted consistent. <a href='https://commits.kde.org/kwin/261e3a768adb769aed513e13a99e764c791f3fdd'>Commit.</a> </li>
<li>Remove unused field in Deleted. <a href='https://commits.kde.org/kwin/e81bbfb086421453cb8c24823ba6f99c7a82b782'>Commit.</a> </li>
<li>Remove ExtraDesktop.sh. <a href='https://commits.kde.org/kwin/6481607565c21fdd8997305341ff68bdfe008ddc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18775'>D18775</a></li>
<li>Move icons to data dir. <a href='https://commits.kde.org/kwin/b06c1ea5a471b2c9f68c0424d999a5385e77547a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18771'>D18771</a></li>
<li>Drop COPYING.DOC. <a href='https://commits.kde.org/kwin/b40c702c76794d9215f3bb104b79b4a2f5e6bae2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18657'>D18657</a></li>
<li>Move ExtendedCursor::Shape's doc comment to the correct place. <a href='https://commits.kde.org/kwin/487990009a462806a0479fc2cd09cfacd445bff3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18669'>D18669</a></li>
<li>[autotests] Test maximize animation. <a href='https://commits.kde.org/kwin/5ad797d5fe935064dc4a32a63d35ec9b3ed392a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18166'>D18166</a></li>
<li>[autotests] Test minimize animations. <a href='https://commits.kde.org/kwin/621370d729512fa41b7c30ba0523b1028d9d916a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18162'>D18162</a></li>
<li>[autotests] Test desktop switching animations. <a href='https://commits.kde.org/kwin/872831fb15f9329bce54e1cb760d858ec6b2baa5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18156'>D18156</a></li>
<li>Drop ShellClient::shellSurface method. <a href='https://commits.kde.org/kwin/97cf32f9162ebb533e7ce88288602a731e3a2e74'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18635'>D18635</a></li>
<li>[libkwineffects] Adjust whitespace in AnimationEffect's header. <a href='https://commits.kde.org/kwin/5a98591fff5946acf86c2ee8c4cf916320d92086'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17835'>D17835</a></li>
<li>[libkwineffects] Use Q_DISABLE_COPY in AnimationEffect. <a href='https://commits.kde.org/kwin/09cf04eec4db5c7b00f1859d0bb19231f4af1db8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17838'>D17838</a></li>
<li>[effects/blur] Update blur to be more natural. <a href='https://commits.kde.org/kwin/3c2148a5fbc43255604e2d288f57c6304aed39d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18377'>D18377</a></li>
<li>[kcmkwin/rules] Fix line separators in the dialog. <a href='https://commits.kde.org/kwin/4853228bc558f87671d47c93ff5a84618e856da9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18616'>D18616</a></li>
<li>[wayland] Drop Qt extended surface. <a href='https://commits.kde.org/kwin/7d10ab0fe7a1d0e899e06591e32764f88ba43cc5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18594'>D18594</a></li>
<li>[wayland] Call ShellClient install interface methods consistently. <a href='https://commits.kde.org/kwin/4240c4af054f54c967392d60bb2afc84b4246e41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18592'>D18592</a></li>
<li>Call frameRendered for undamaged Wayland surfaces. <a href='https://commits.kde.org/kwin/f294e7a035675b402e9b3a217c961b3a3704ab0d'>Commit.</a> </li>
<li>[autotests] Untangle ShellClient::testMaximizedToFullscreen. <a href='https://commits.kde.org/kwin/18dd0327c2e78b5b32b0ff4511655abfdbeed37a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18589'>D18589</a></li>
<li>Trim whitespace. <a href='https://commits.kde.org/kwin/bd15c1185d0a2d0a00b7011425f852e35789fe95'>Commit.</a> </li>
<li>Fix unused param warning. <a href='https://commits.kde.org/kwin/07e394fbb7e61ec96eb9b9b5f3a7b58d7ddc74a6'>Commit.</a> </li>
<li>Fix reorder warning in AbstractScript. <a href='https://commits.kde.org/kwin/cfecb1e0770ca6c8fa879124e11b03081342b9ed'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18560'>D18560</a></li>
<li>Silence warnings in TabBoxHandler. <a href='https://commits.kde.org/kwin/2608026b8835127b8343e1ec0649284c8409e7e4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18406'>D18406</a></li>
<li>Switch ThumbnailItem to internal uuid instead of WId. <a href='https://commits.kde.org/kwin/932ccb2ac375fd5779f588daca492b296fe70e84'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18405'>D18405</a></li>
<li>Add windowsystem plugin for KWin's qpa. <a href='https://commits.kde.org/kwin/02a0561016c22be906350bca68973f2818b727d7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18228'>D18228</a></li>
<li>Remove superfluous code. <a href='https://commits.kde.org/kwin/ef510b4e76933a0cd04a40e62dc785362ea730f0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18348'>D18348</a></li>
<li>[effects] Port to new connect syntax. <a href='https://commits.kde.org/kwin/253ff428a70d6f873753e26de5cb27ab5d4d533a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18368'>D18368</a></li>
<li>Name Wayland socket automatically when no socket name was specified. <a href='https://commits.kde.org/kwin/4729a42c34b60429c752d0f2af2abaa81f08f45d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18522'>D18522</a></li>
<li>Send done event after the number of rows has been changed. <a href='https://commits.kde.org/kwin/260fa71d5d150ec7612cc2c7fc5c41ee24118425'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18518'>D18518</a></li>
<li>[kcmkwin] Use new icons for virtual desktops, touch screen, and screen edges KCMs. <a href='https://commits.kde.org/kwin/1d706d96e5f34b67be182e8937ed25a5cb75c828'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18492'>D18492</a></li>
<li>Separate clang and gcc compiler workarounds for kwin override policy. <a href='https://commits.kde.org/kwin/d4fce7a3ad0fb3738a7fb9fe84452491759b1660'>Commit.</a> </li>
<li>Apply -Wno-inconsistent-missing-override for all compilers. <a href='https://commits.kde.org/kwin/a088a7b1a48295456b01f773c67d2428c98ddb12'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18488'>D18488</a></li>
<li>[kcmkwin/compositing] Use new icon for Effects KCM. <a href='https://commits.kde.org/kwin/4f69dc6833c9d03a4049e44e1bf6bb441af33038'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18461'>D18461</a></li>
<li>Set rows in virtualdesktop protocol. <a href='https://commits.kde.org/kwin/94e09947f2c03851bdc41d766646543c45f2865a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18293'>D18293</a></li>
<li>[libkwineffects] Port AnimationEffect to new connect syntax. <a href='https://commits.kde.org/kwin/cd0f954ab13de41bc54861369ba06aa0157ebf2a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18393'>D18393</a></li>
<li>[autotests] Use unloadAllEffects. <a href='https://commits.kde.org/kwin/6740db7a6c72deb670c6c9a721053ef2bde8ad4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18383'>D18383</a></li>
<li>[autotests] Update tests to use stable version of xdg-shell. <a href='https://commits.kde.org/kwin/8877dff7157effd8e72269649ff673f63c979b4f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18292'>D18292</a></li>
<li>Remove executable bit from presentwindows.cpp. <a href='https://commits.kde.org/kwin/2458b95e8c863ff283b3f3ee206fdeb6e420218b'>Commit.</a> </li>
</ul>


<h3><a name='kwrited' href='https://commits.kde.org/kwrited'>kwrited</a> </h3>
<ul id='ulkwrited' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/kwrited/1b1e92c86653239b47903b3f048b15af2bb3e283'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/libkscreen/b1f6abdc234e84236ddb7058359f32f779998eba'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Update tests cmake to WebEngine port. <a href='https://commits.kde.org/libksysguard/58770026d43aa88913b40ccbcb52203f5cf1a996'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20943'>D20943</a></li>
<li>Process: add NoNewPrivileges. <a href='https://commits.kde.org/libksysguard/10ba98f7eebb2001e2e26ea147e31cbef9288060'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20645'>D20645</a></li>
<li>Add icon for "Resume Stopped Process" action. <a href='https://commits.kde.org/libksysguard/574e0b7d6c005403f66372a9b9c9d80fc33972d0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20179'>D20179</a></li>
<li>Fix compilation error when SA_TRACE is 1. <a href='https://commits.kde.org/libksysguard/55aa93600b61dc6798aad8c463080f1072102014'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19971'>D19971</a></li>
<li>Add tooltip for quick search. <a href='https://commits.kde.org/libksysguard/839be94eaf7b1023bf8bfeab443cc1fc32920ae4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403923'>#403923</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19312'>D19312</a></li>
<li>Use AuthCore instead of Auth. <a href='https://commits.kde.org/libksysguard/cd17327111d4330718d2db07041f985a7dcc7b9d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19063'>D19063</a></li>
<li>Remove unused include. <a href='https://commits.kde.org/libksysguard/1ba377b65bb0b590771175f07ab2f0d9a3d31477'>Commit.</a> </li>
<li>Better window title display. <a href='https://commits.kde.org/libksysguard/87c2028856a1b924cdf86b5b258b642394dd7b13'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18779'>D18779</a></li>
<li>Accept commas to separate processes on the Search Edit. <a href='https://commits.kde.org/libksysguard/f5dcd02c52fd00dab55f2081e71af303977ab8d7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18580'>D18580</a></li>
<li>Use new toolbox icon for "Tools" dropdown menu button. <a href='https://commits.kde.org/libksysguard/0ff5b31d5dd9422b2b12b01e1a8b133c86f8c714'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18395'>D18395</a></li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/milou/03f5904a2bf8533e8e989dd51ed3e55be8b53fd5'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/milou/94226fa82c22aa7e01de251491e7f963bdf04c87'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/milou/b542cff2df26796530012fc41f26937c89eaff6c'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Do no install .so symlinks for private libraries. <a href='https://commits.kde.org/oxygen/a3c095352bb1d15b9e26d95ca7b8a01c81747cfd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20377'>D20377</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/oxygen/13a88f9aecd9811a5ebb623a1d45f5de9a4e0363'>Commit.</a> </li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>[DownloadJob] Set processed amount when download finishes. <a href='https://commits.kde.org/plasma-browser-integration/80a45797913b37152fea1aafb6321ab6b8873055'>Commit.</a> </li>
<li>Revert "Update kf5 version requirement to 5.58.0" as previously KF5. <a href='https://commits.kde.org/plasma-browser-integration/53a2d262861d713d222ef4747c695b29795a4faa'>Commit.</a> </li>
<li>Remove kde connect context menu entries when host dies. <a href='https://commits.kde.org/plasma-browser-integration/7b11fee2b9062af79fcbd7e38731c8d5ade42bbb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21044'>D21044</a></li>
<li>Disallow setting a negative volume. <a href='https://commits.kde.org/plasma-browser-integration/324f54cf1d9831a3b81caef79155203af3ba18fa'>Commit.</a> </li>
<li>Also handle muted property on player. <a href='https://commits.kde.org/plasma-browser-integration/1ce07b1cec315f83369ab1c4edbd1d5aedee1d88'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20925'>D20925</a></li>
<li>Fix message extraction. <a href='https://commits.kde.org/plasma-browser-integration/35668edc67f8d5e99d9332a2224e533f574d46a0'>Commit.</a> </li>
<li>Make really sure we show an error message in case saving failed. <a href='https://commits.kde.org/plasma-browser-integration/6ae424de5d1366e5f1a039122da7a486f5502635'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20819'>D20819</a></li>
<li>No multi-line in JSON, oopsie. <a href='https://commits.kde.org/plasma-browser-integration/cf1c09ff372793416dd3156b04e0dd34e2d5651b'>Commit.</a> </li>
<li>Add store description to locale file for translations. <a href='https://commits.kde.org/plasma-browser-integration/aa1c7c35e4a3102afeb76f59cf714fed6604c34e'>Commit.</a> </li>
<li>[DownloadJob] Report total size only if known. <a href='https://commits.kde.org/plasma-browser-integration/3989f42ac5f0092e7ed47f0bdb289c72ce9622c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19857'>D19857</a></li>
<li>Initialize metadata with null. <a href='https://commits.kde.org/plasma-browser-integration/f4722bd0a6ec63b9e2c05d91d89d1ea58c5581ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406782'>#406782</a></li>
<li>Bump manifest version and copyright. <a href='https://commits.kde.org/plasma-browser-integration/2899a2c84c2d450f342e42a610c3c4170a9540b2'>Commit.</a> </li>
<li>Call into native Media Session browser API if available. <a href='https://commits.kde.org/plasma-browser-integration/4c5ee86a48cb4d851ddaf677f5598a2c769d0cc0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20726'>D20726</a></li>
<li>Keep player around when emptied but the website tells us it's actually just paused. <a href='https://commits.kde.org/plasma-browser-integration/eb583434a5611e5a3bb8046e6627469753115b3c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402324'>#402324</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20724'>D20724</a></li>
<li>Support "stop" Media Sessions action handler. <a href='https://commits.kde.org/plasma-browser-integration/44f251b4f74b0d840a6fdf0b2d997e682ba84bdd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20725'>D20725</a></li>
<li>Only consider player gone if really no longer part of the visible DOM. <a href='https://commits.kde.org/plasma-browser-integration/999e790c1eeb9ba711cc2160980caa6d684f797d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20582'>D20582</a></li>
<li>Minimize code duplication between node and its children. <a href='https://commits.kde.org/plasma-browser-integration/7cd776720613071383570aa0d5f91a06a8ee311e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20593'>D20593</a></li>
<li>Set Breeze scrollbars only on HTML tag. <a href='https://commits.kde.org/plasma-browser-integration/2a8ac45707bdc0fc11673a5b3a5b1f7e194cbe9d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20596'>D20596</a></li>
<li>Remove player from known players list when it disappears. <a href='https://commits.kde.org/plasma-browser-integration/5940d353366f62acaed61961ab3883b6699a6d9a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20227'>D20227</a></li>
<li>Avoid serializing base64 encoded favicon data twice. <a href='https://commits.kde.org/plasma-browser-integration/c3611dc98170f2db854eb51fc25385b6849fe739'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19963'>D19963</a></li>
<li>[DownloadJob] Set destUrl and total amount. <a href='https://commits.kde.org/plasma-browser-integration/b09312ad384b41c3eaaca0d11a75c8b07cd4fc2e'>Commit.</a> See bug <a href='https://bugs.kde.org/404182'>#404182</a>. See bug <a href='https://bugs.kde.org/402144'>#402144</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19589'>D19589</a></li>
<li>Remove unused dependencies. <a href='https://commits.kde.org/plasma-browser-integration/4ae87a3757ca62c82d35a70c4cc354831b49d6b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18886'>D18886</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Touchpad KCM] Load previous setting on reboot. <a href='https://commits.kde.org/plasma-desktop/5afc24db67ed032334e4edafe1b6fa4cdeac0f7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21228'>D21228</a></li>
<li>[Touchpad KCM] Fix libinput-less build and warnings. <a href='https://commits.kde.org/plasma-desktop/8b1c894effa08a530ec64a3b4563cfea3765b7de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21220'>D21220</a></li>
<li>[libinput-touchpad-kcm] Use wayland specific touchpad KCM UI when libinput is used on X11. <a href='https://commits.kde.org/plasma-desktop/ce84d6ab4babc80707f4e4c1deb02cfe3ac5b7bb'>Commit.</a> Implements feature <a href='https://bugs.kde.org/387153'>#387153</a>. Implements feature <a href='https://bugs.kde.org/392709'>#392709</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20186'>D20186</a></li>
<li>[Splash KCM] Use InlineMessage for testing error. <a href='https://commits.kde.org/plasma-desktop/bbd7fcb3a30010e391499c03061e02e5678fbbc4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19752'>D19752</a></li>
<li>[Notifications KCM] Set a sane implicitWidth for the dialog. <a href='https://commits.kde.org/plasma-desktop/5f10e0d8918f1e234088884d80843675ebc465a9'>Commit.</a> </li>
<li>Remove no longer existing directory. <a href='https://commits.kde.org/plasma-desktop/29d650d14575829ceb6c22c4e0fe3578601048c4'>Commit.</a> </li>
<li>[KColorSchemeEditor] Turn Save button into Save As. <a href='https://commits.kde.org/plasma-desktop/cc63a8af39138a0f3a875e28ff8e4afeb40d6017'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18519'>D18519</a></li>
<li>Remove old notification kcm (knotify). <a href='https://commits.kde.org/plasma-desktop/44aca277c03bfe9facf1393cf1cc192489949e79'>Commit.</a> </li>
<li>[Task Manager] Use libnotificationmanager for notification badges and application job reporting. <a href='https://commits.kde.org/plasma-desktop/00962d089177f36a7735ba890a65472495a2251e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21061'>D21061</a></li>
<li>Don't leak KNotification. <a href='https://commits.kde.org/plasma-desktop/e560fc2266cfd7bdca7cdc8e7c82d8805674cbc5'>Commit.</a> </li>
<li>Set proper transient. <a href='https://commits.kde.org/plasma-desktop/46ca7e00b607e2b84a96957408d4e3e7dbe4f11f'>Commit.</a> </li>
<li>Load source model only on source page but immediately there when needed. <a href='https://commits.kde.org/plasma-desktop/fc81acff30fdfdace72216602aa98d7b610b58e7'>Commit.</a> </li>
<li>[WidgetExplorer] Fix blurry previews. <a href='https://commits.kde.org/plasma-desktop/5519a0a5bff8557d3f316c93c5fbcbf7aeb489d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21030'>D21030</a></li>
<li>Simplify applet keyboard shortcut chooser's explanatory text. <a href='https://commits.kde.org/plasma-desktop/64e48e87656bb8bbdb37ce9ceadcb21fc463ece8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20997'>D20997</a></li>
<li>[Task Manager] Don't show redundant "Actions" header in context menu with nothing but actions in it. <a href='https://commits.kde.org/plasma-desktop/f235bdd8d74feb7fe42171bff68c30c43515534e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20846'>D20846</a></li>
<li>[Kickoff] Use simpler code for separator color. <a href='https://commits.kde.org/plasma-desktop/935f333874375522fa99d0459713422fccb7ac7e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20973'>D20973</a></li>
<li>[kimpanel] Port settings page to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/plasma-desktop/9dcc29663bd78901ff19bc998dd1e73ea3884d4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20906'>D20906</a></li>
<li>Rank Kickoff desktopsessions search results over services. <a href='https://commits.kde.org/plasma-desktop/5f5b21c562a866328871cb156456994f0ea726b5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405268'>#405268</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20923'>D20923</a></li>
<li>[Pager] Improve label legibility. <a href='https://commits.kde.org/plasma-desktop/d09d4d52e1619c504abb937b13e873d5f57d78ac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20851'>D20851</a></li>
<li>[desktoppackage] Port to QQC2 and fix "Get New Widgets..." button text. <a href='https://commits.kde.org/plasma-desktop/bf7a430da31d7fa856d5562aee8ca3e74944ca9e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20904'>D20904</a></li>
<li>Export proper size hints for the sidebar. <a href='https://commits.kde.org/plasma-desktop/34e5a40f3405e87babd259026507708394856d7f'>Commit.</a> </li>
<li>Flickables take the implicit size as contentwidth. <a href='https://commits.kde.org/plasma-desktop/3936f86449e5b8f06acd2281c5f5bb65d5b35f9f'>Commit.</a> </li>
<li>Provide alternatives for Show Desktop and Minimize All Windows widgets. <a href='https://commits.kde.org/plasma-desktop/8690a41b02adca276ba1ff28c98f0fbe2baa2cf1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20895'>D20895</a></li>
<li>[containments/desktop] Port desktop settings pages to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/plasma-desktop/4c6a6fe1870e5316f3eae30edc92becb58bd2574'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20878'>D20878</a></li>
<li>[kcolorschemeeditor] Remove useless printf. <a href='https://commits.kde.org/plasma-desktop/cfbb13858f019286752beef778d47a9390822c54'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20889'>D20889</a></li>
<li>Change "Download New Desktop Themes" to "Download New Plasma Themes", forgot to change this string when the KCM was renamed. <a href='https://commits.kde.org/plasma-desktop/b3cd81fca8168c1d02b5a4dcb49e21779d27bc0a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406967'>#406967</a></li>
<li>Add Show Desktop applet to default panel. <a href='https://commits.kde.org/plasma-desktop/4d3bfe3c0d31b0791f627b01217ffcd306612057'>Commit.</a> Implements feature <a href='https://bugs.kde.org/381295'>#381295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20820'>D20820</a></li>
<li>Add the Show Desktop applet (moved from kdeplasma-addons). <a href='https://commits.kde.org/plasma-desktop/b6d785370d53dbfbdbd3ee462ccdf34fead24ba5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20818'>D20818</a></li>
<li>Remove feature to delete seen apps. <a href='https://commits.kde.org/plasma-desktop/1d33ce06fd0e3541898f1957b7afb3c0675d77c5'>Commit.</a> </li>
<li>Fix launcher URL comparison. <a href='https://commits.kde.org/plasma-desktop/b6b008bbf0ec3ce93639e3c95de76daee84b33ac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20810'>D20810</a></li>
<li>[Pager] Port settings window to QQC2+Kirigami FormLayout and improve UI. <a href='https://commits.kde.org/plasma-desktop/b8d753f4f00fe2bf0cd96d00c2fd7cfb16e95289'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20796'>D20796</a></li>
<li>[Task Manager] Bring to current desktop with middle-click. <a href='https://commits.kde.org/plasma-desktop/a416f16b562b8eca364c38c7bb99f21bb1996c01'>Commit.</a> Implements feature <a href='https://bugs.kde.org/360250'>#360250</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20773'>D20773</a></li>
<li>[Kicker] Modernize settings window. <a href='https://commits.kde.org/plasma-desktop/2e2cfa891534f459d5e73e13e7a6d671599dfe1a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20743'>D20743</a></li>
<li>[Kickoff] Correct onPressed vs onClicked for opening the icon chooser menu. <a href='https://commits.kde.org/plasma-desktop/3024920b70d790eff4954e842e55085e371df9a3'>Commit.</a> </li>
<li>[Kickoff] Modernize settings window layout. <a href='https://commits.kde.org/plasma-desktop/7bbe5fc1e1fa40563f685de14094de2393ed51a4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20612'>D20612</a></li>
<li>Hook up the main form layout with child form layouts. <a href='https://commits.kde.org/plasma-desktop/1c6644a42b7abe14a25cc3d77aa12de2ac227394'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20313'>D20313</a></li>
<li>[Kickoff] Make the tabbar separator width consistent with tab selection line. <a href='https://commits.kde.org/plasma-desktop/60181eca4414554afb618d610542ae4d366f5642'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20676'>D20676</a></li>
<li>Add alternatives button to applet configuration in panel edit mode. <a href='https://commits.kde.org/plasma-desktop/c308122688ee1912b9630a2830d0a6cdeeabefd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405082'>#405082</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17410'>D17410</a></li>
<li>[Folder View] Allow icon size slider to horizontally stretch if space permits. <a href='https://commits.kde.org/plasma-desktop/7659a96e772c7b4cb580358ae4fab46b9d8e3b5d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381656'>#381656</a></li>
<li>System settings: make date/time format preview consistent. <a href='https://commits.kde.org/plasma-desktop/d4671edc8198d08e9a6cfa8d8818d2f019095859'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15423'>D15423</a></li>
<li>Finishing touches. <a href='https://commits.kde.org/plasma-desktop/be33b5f1665df6ed8b608a61ebf774a63e491856'>Commit.</a> </li>
<li>[Desktop Theme KCM] Sync clock code to applet one: smooth hand on animation. <a href='https://commits.kde.org/plasma-desktop/b0cb2d0f6fb2f3876ed7c9b2279ec9ab9ad6cd44'>Commit.</a> </li>
<li>[Desktop Theme KCM] Adapt clock to new hand rotation center options. <a href='https://commits.kde.org/plasma-desktop/dfdbc19d9fc911c3ce984dc2877c69769230e2e9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20113'>D20113</a></li>
<li>[Kickoff] Anchor right of subTitleElement in KickoffItem to arrow.right. <a href='https://commits.kde.org/plasma-desktop/e09cc99b5250e334b03160bf7288e3ef48afb0eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20399'>D20399</a></li>
<li>Desktoppackage: add "panelMask" property for Panel.qml. <a href='https://commits.kde.org/plasma-desktop/2af721b37741ff0f53733ae07bd9919723813919'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20203'>D20203</a></li>
<li>[Folder View] Implement a user-configurable setting for label width. <a href='https://commits.kde.org/plasma-desktop/bad5d2b8973c3f1d389227caf9926cc46e441ac5'>Commit.</a> Implements feature <a href='https://bugs.kde.org/403094'>#403094</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20370'>D20370</a></li>
<li>Fix incorrect vertical spacing between main layout and individual wallpaper plugins. <a href='https://commits.kde.org/plasma-desktop/230ac60a3195320ae69e502f525993b56c0d1833'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20257'>D20257</a></li>
<li>[Kickoff] Reduce the margins of KickoffItem, KickoffHighlight and use smallSpacing. <a href='https://commits.kde.org/plasma-desktop/51f7912368e678c3a10eb1d7d53494c399ba260f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19874'>D19874</a></li>
<li>Don't lose list position after installing KNS cursor themes. <a href='https://commits.kde.org/plasma-desktop/4545601adec06a63c7ba471422f11510ff7f1b51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406084'>#406084</a></li>
<li>Make location of "Lock Widgets" and "Remove Panel" buttons more obvious. <a href='https://commits.kde.org/plasma-desktop/da0adfedcab02b5925549ed5122ccc56bf8e1636'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406079'>#406079</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20144'>D20144</a></li>
<li>Foldermodel: store information about screen used. <a href='https://commits.kde.org/plasma-desktop/dcb4c158b41452d18e629b30b472b6d3294070ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401464'>#401464</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18979'>D18979</a></li>
<li>Remove traces of activities make removing auto-detected apps work. <a href='https://commits.kde.org/plasma-desktop/f210fb104ebaf812ee4a1435519074bd5e23e84b'>Commit.</a> </li>
<li>Cleanup and tidying. <a href='https://commits.kde.org/plasma-desktop/bf2be2c7370917a89ed64844222013c7a9047928'>Commit.</a> </li>
<li>Avoid calling QT_LSTAT and accessing recent documents. <a href='https://commits.kde.org/plasma-desktop/408f03ca989e4cc04a00dcf8a941805d43d83549'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19784'>D19784</a></li>
<li>Fix Default Applications KCM window size. <a href='https://commits.kde.org/plasma-desktop/cf1305a2fb40b96d4b57041bafd57ef44d0534b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398799'>#398799</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20072'>D20072</a></li>
<li>[application-dashboard] Use appropriate search string. <a href='https://commits.kde.org/plasma-desktop/dd1840f0496ff2d828701a4f81abbf8c34bb53a2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20073'>D20073</a></li>
<li>Support Kirigami.twinFormLayouts in individual wallpaper plugins. <a href='https://commits.kde.org/plasma-desktop/a3d4b38574fba3fde8f83da564a3f6ffe71990ad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19932'>D19932</a></li>
<li>Set autofocus to child KPluginSelector on UI load. <a href='https://commits.kde.org/plasma-desktop/763c0cf117bca5039a18c502cb6a6d88bde2281e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399516'>#399516</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20035'>D20035</a></li>
<li>Make some of the settings actually work. <a href='https://commits.kde.org/plasma-desktop/5c834677c2a2075698f09156502f4d4d71978999'>Commit.</a> </li>
<li>Properly place elements into the plasmoid explorer. <a href='https://commits.kde.org/plasma-desktop/842eeb5525823fbd8d85597c212734212db82cbb'>Commit.</a> </li>
<li>Change URLs to https where possible. <a href='https://commits.kde.org/plasma-desktop/fc6cdb16151687c97febafe8ba91a67745bac805'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19995'>D19995</a></li>
<li>[Desktop Theme KCM] "Normalize" margin around background in previews. <a href='https://commits.kde.org/plasma-desktop/34387d450ab234eb2f2da5f11db146cfed076ac6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19748'>D19748</a></li>
<li>Filter duplicate desktop entries. <a href='https://commits.kde.org/plasma-desktop/c6598a318093289e6a89ba5716def6cb6570ddaa'>Commit.</a> </li>
<li>Implement new Notifications KCM. <a href='https://commits.kde.org/plasma-desktop/0117b7c2a85d54aa118b65ae6e8eec810c1164cc'>Commit.</a> </li>
<li>[Application Dashboard] Improve mouse handler. <a href='https://commits.kde.org/plasma-desktop/0ecb1359eaca6f13cf103793fc238a9b8d76536f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390424'>#390424</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19800'>D19800</a></li>
<li>Port to the new install directory for knsrc files. <a href='https://commits.kde.org/plasma-desktop/67f0c43a3ad1b6921460892f8650f2f214f6be17'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19842'>D19842</a></li>
<li>[Task Manager] When closing apps, resize only when mouse is out. <a href='https://commits.kde.org/plasma-desktop/2861ae7cc67502b85c539dc0027e52df375be132'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18438'>D18438</a></li>
<li>Use new application style icon for the Application Style KCM. <a href='https://commits.kde.org/plasma-desktop/b86045278de8d611a94be04e50d8f66d1f70fd5a'>Commit.</a> </li>
<li>Port deprecated method. <a href='https://commits.kde.org/plasma-desktop/ae0939c0baffc8a3195bf7d0cee5ab05d9dc30a2'>Commit.</a> </li>
<li>[Task Manager] Do not crop album art in tooltip. <a href='https://commits.kde.org/plasma-desktop/e8af68ad76bb329a8bee8aa6f1a82653098eaa9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401234'>#401234</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17073'>D17073</a></li>
<li>[Task Manager] Reorganize and improve presentation of context menu. <a href='https://commits.kde.org/plasma-desktop/1daf42d9d734f969c31016825d8a7a93a6f95fbb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405040'>#405040</a>. Fixes bug <a href='https://bugs.kde.org/405071'>#405071</a>. Fixes bug <a href='https://bugs.kde.org/405070'>#405070</a>. Fixes bug <a href='https://bugs.kde.org/405065'>#405065</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19606'>D19606</a></li>
<li>Deprecated--. <a href='https://commits.kde.org/plasma-desktop/07178f286bfcdbe589a47c29ba2752381ae39109'>Commit.</a> </li>
<li>Add missing override. <a href='https://commits.kde.org/plasma-desktop/bed179d3ca472999def89a8db55d6210c1995499'>Commit.</a> </li>
<li>Improve the look of the Application Style KCM UI. <a href='https://commits.kde.org/plasma-desktop/a946944edb20e0a2943e57816f68a1db8787c886'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19651'>D19651</a></li>
<li>[Desktop Sessions KCM] Add Restart to BIOS/UEFI checkbox. <a href='https://commits.kde.org/plasma-desktop/0f5c7ede9da23ae60dbd24908bba65d911db26d8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19560'>D19560</a></li>
<li>Make QQC2 desktop style a RUNTIME dependency. <a href='https://commits.kde.org/plasma-desktop/737b114c2fa5bf0b01951eeb2eef22ce7fcdcfa1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19707'>D19707</a></li>
<li>[ConfigCategoryDelegate] Fix highlighting active category. <a href='https://commits.kde.org/plasma-desktop/9f1a3eca3911863edddaeceadb5ce5a19c4fb8db'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/plasma-desktop/00e86a0565be98c77e1a343ee6db7aa5b751b5c6'>Commit.</a> </li>
<li>Fix bad commit. <a href='https://commits.kde.org/plasma-desktop/0d5c93b7c5318600f53d214efca2a060c0f0eea7'>Commit.</a> </li>
<li>Fix warning == vs ===. <a href='https://commits.kde.org/plasma-desktop/6e36de2e1e92743751ddd5b735cdfe9b2e95e5e2'>Commit.</a> </li>
<li>Update documentation for kcontrol desktop theme (now plasma theme). <a href='https://commits.kde.org/plasma-desktop/245677bfb31a9e54956e51b44f67be098bf1372f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19618'>D19618</a></li>
<li>[Colors KCM] Add search and filter. <a href='https://commits.kde.org/plasma-desktop/a1fbeb96dc6c745604a70fad32a65456cac7e5a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18646'>D18646</a></li>
<li>[libinput-touchpad-kcm-click-method] Add click method Areas and Clickfinger for buttonless touchpads. <a href='https://commits.kde.org/plasma-desktop/8ca7335288fa621b987ae3fec4428c6601ceef67'>Commit.</a> Implements feature <a href='https://bugs.kde.org/387156'>#387156</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18912'>D18912</a></li>
<li>[kcm/componentchooser] Remove unused ktimerdialog.{h,cpp}. <a href='https://commits.kde.org/plasma-desktop/14d6402abc342627c74aa59f22c125fd4ed369bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19496'>D19496</a></li>
<li>Port to ECM's component/imported target based FindXCB. <a href='https://commits.kde.org/plasma-desktop/8c28ac839ad55f8b1f7ac85824634320c8fb38dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19465'>D19465</a></li>
<li>Fix QLatin1String usage. <a href='https://commits.kde.org/plasma-desktop/b0e84dbe2cb69246cf6dff96198b76c473951b7b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19423'>D19423</a></li>
<li>[Kicker, Kickoff & Dash] Change labels to match SDDM theme labels. <a href='https://commits.kde.org/plasma-desktop/604244b6cb6604cfa7b506e386e6820a6f15b392'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19459'>D19459</a></li>
<li>Hide panel toolbox tooltip when entering panel edit mode. <a href='https://commits.kde.org/plasma-desktop/03d9c7b5eb301524ea1df3d1dd432ea349f8335d'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/plasma-desktop/64c56e738a95ef024b3bebe307258a8a499d9b0f'>Commit.</a> </li>
<li>Remove superfluous inner layout. <a href='https://commits.kde.org/plasma-desktop/43804eb7f15ffc44f43f3edceb160f71b9d855cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395513'>#395513</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19391'>D19391</a></li>
<li>[Kicker, Kickoff & Dash] Use the word "Sleep" instead of "Suspend". <a href='https://commits.kde.org/plasma-desktop/360b565d8cab0818f03f773c2675f73a1d6a94d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19184'>D19184</a></li>
<li>[TaskManager] Modernize and reorganize configuration settings. <a href='https://commits.kde.org/plasma-desktop/5e905afb07e8cf7ea1a5dbc0def0507910feb752'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19280'>D19280</a></li>
<li>[Pager] Improve the string for the menu item that opens the KCM. <a href='https://commits.kde.org/plasma-desktop/cc59e2075340c19165c6c0eec62926d4e55c1c07'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404786'>#404786</a></li>
<li>[AppletConfiguration] Use title case for Keyboard Shortcuts label. <a href='https://commits.kde.org/plasma-desktop/61e9f7319a602c5cce9d827238691c762a6cef9e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19276'>D19276</a></li>
<li>Replace outdated FindX11_XCB copy with ECM and imported targets. <a href='https://commits.kde.org/plasma-desktop/45d35e88b3cde1e0b7bf2b5cb633b7614c7e353d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19247'>D19247</a></li>
<li>Remove outdated FindGLIB2 copy. <a href='https://commits.kde.org/plasma-desktop/f88abed5581f66a555d93ea540edea7d72bbb7ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19244'>D19244</a></li>
<li>[Colors KCM] Fix color scheme editor context. <a href='https://commits.kde.org/plasma-desktop/5c879b25839114c9a1a416767780492de120d822'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19206'>D19206</a></li>
<li>Fix warnings. <a href='https://commits.kde.org/plasma-desktop/d6a4e90f2faab475a1636ecaaf65a3826a78b5d9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18928'>D18928</a></li>
<li>Fix Font Management KCM dialog label text capitalization. <a href='https://commits.kde.org/plasma-desktop/91542c269ae2a88550583ac662c07d3097a6ac94'>Commit.</a> </li>
<li>Improve the look of the Emoticons KCM UI. <a href='https://commits.kde.org/plasma-desktop/8b413729da44a0275ca4862092d30210c1298d33'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19093'>D19093</a></li>
<li>Make look of Font Management KCM UI more consistent. <a href='https://commits.kde.org/plasma-desktop/8596ae90853356b92f9536ee51453670dd56f1e7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19079'>D19079</a></li>
<li>Use AuthCore instead of Auth. <a href='https://commits.kde.org/plasma-desktop/098596e4185eee3b956edebe54ede61fa586679b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19067'>D19067</a></li>
<li>Fix a few easy warnings. <a href='https://commits.kde.org/plasma-desktop/64912ac2225de98fafd1c7369a98e199a725bde7'>Commit.</a> </li>
<li>Make Appearance KCM tooltip texts consistent. <a href='https://commits.kde.org/plasma-desktop/6eb8333334d765c99ce845ba26147731247d572c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19038'>D19038</a></li>
<li>[Containment] Add ellipses to "Configure Desktop" menu item. <a href='https://commits.kde.org/plasma-desktop/cae70a71ac8e4db52d02ae3359415da1dfeb8a9d'>Commit.</a> </li>
<li>[containment] Show an inline message when the Desktop Toolbox is hidden. <a href='https://commits.kde.org/plasma-desktop/1a10e201e91948cadfc25becd2e2d5c6111a282c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18991'>D18991</a></li>
<li>[containment] Modernize tweaks page layout and port to QQC2. <a href='https://commits.kde.org/plasma-desktop/191a901d09c48ff1484541d3cc966470b508f159'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18990'>D18990</a></li>
<li>Move Look and Feel KCM to top level. <a href='https://commits.kde.org/plasma-desktop/0af980f5cab860258e3bc0d2d9de0516fefe5e1f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18892'>D18892</a></li>
<li>Make Appearance KCMs' labels consistent. <a href='https://commits.kde.org/plasma-desktop/089612d232fda40ef293d403a156b9d5e23895ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18903'>D18903</a></li>
<li>Have task managers follow the desktop for window visibility behavior by default. <a href='https://commits.kde.org/plasma-desktop/ce2cc63497329b2e614622918a3f1cb4afd3f51c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18929'>D18929</a></li>
<li>Fix Font Management KCM search bar placeholder text. <a href='https://commits.kde.org/plasma-desktop/190bea82b06f345d8f5b6d1b823ad167afd69b37'>Commit.</a> </li>
<li>[Folder View] Assume root of a protocol is always a folder. <a href='https://commits.kde.org/plasma-desktop/ca113afa5028c4be6935d3f2feed20ef918bda29'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18721'>D18721</a></li>
<li>Edit line 345 string to match that of line 283. <a href='https://commits.kde.org/plasma-desktop/42a86be841289459e9c075d58bd698dafad1b97f'>Commit.</a> </li>
<li>Improve the look of the Fonts KCM UI. <a href='https://commits.kde.org/plasma-desktop/9dac09af4a027e799982fc6b25d087f1fbc9dc00'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18861'>D18861</a></li>
<li>Use different Font Management KCM "All Fonts" icon. <a href='https://commits.kde.org/plasma-desktop/e5ef9c97e75e926091382c1d1b134ffc717d26b4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18785'>D18785</a></li>
<li>[Activities KCM] Properly vertically center the buttons. <a href='https://commits.kde.org/plasma-desktop/ccc6e24f9aab3da40f0ff2ba7b505022b485ed0e'>Commit.</a> </li>
<li>Revert "[Activities KCM] vertically center the buttons". <a href='https://commits.kde.org/plasma-desktop/ae3ddf82dd95dc9fc072cf90a02dc31e5847febc'>Commit.</a> </li>
<li>Disable KCMs while downloading file. <a href='https://commits.kde.org/plasma-desktop/366ec1b2ee591e622b6e22d4a283be5a68b662aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18497'>D18497</a></li>
<li>Fix Panel Toolbox Tooltip. <a href='https://commits.kde.org/plasma-desktop/01d0a4624c580d2ff964def605de19e0da3fd709'>Commit.</a> </li>
<li>[Cursors KCM] Use new icon. <a href='https://commits.kde.org/plasma-desktop/2dd8c74eb4ecc354604b384ea100215ea774cf85'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18687'>D18687</a></li>
<li>[Notifications KCM] Use nice new bell-style notification icon. <a href='https://commits.kde.org/plasma-desktop/1f06bfd8352ae23221f8a6bb326655949296ba48'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18678'>D18678</a></li>
<li>[Cursor Theme KCM] Fix 'Install from File...' button capitalization. <a href='https://commits.kde.org/plasma-desktop/ff078eeeef93cb8dc50866b257d9d5e67fdd2884'>Commit.</a> </li>
<li>Extract paneltoolbox messages. Patch by Victorr <victorr2007@yandex.ru>. <a href='https://commits.kde.org/plasma-desktop/99c27255722634f64137be57ca165f5bd0f6d0c9'>Commit.</a> </li>
<li>Change Activities KCM icon. <a href='https://commits.kde.org/plasma-desktop/6d5be4517b744ccb702b210f24b6023c083ee59f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18530'>D18530</a></li>
<li>[Desktop Toolbox] Use a clearer name when the label is shown. <a href='https://commits.kde.org/plasma-desktop/bb3a1fc1828ea972f1dd6b5ed27e8a70100101f1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18528'>D18528</a></li>
<li>[Colors KCM] Signal selectedSchemeIndexChanged when model reloads. <a href='https://commits.kde.org/plasma-desktop/2983bb2965a3c1203ae06c3395a1ae77c9f0ee51'>Commit.</a> </li>
<li>Remove custom view remove transition now that it's part of GridView itself. <a href='https://commits.kde.org/plasma-desktop/b53d1a822e3de1a23f0e5bf5160a23d0cbefa37b'>Commit.</a> </li>
<li>[Desktop theme KCM] Use new icon. <a href='https://commits.kde.org/plasma-desktop/cbe86e622f60c16eb7225beea50b7ad1b5e02326'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18441'>D18441</a></li>
<li>[Desktop Theme KCM] Support installing from remote locations. <a href='https://commits.kde.org/plasma-desktop/12f4d353f680876466f02f7a926d4c15793e9b60'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18498'>D18498</a></li>
<li>[Colors KCM] Port to new design. <a href='https://commits.kde.org/plasma-desktop/b52c0cebfb98a76089e0658d7e3824b4c0d34d63'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/307216'>#307216</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12278'>D12278</a></li>
<li>Fix the cursor KCM header text. <a href='https://commits.kde.org/plasma-desktop/9e51085df7b7f8135e8f526812397a48dd658bc8'>Commit.</a> </li>
<li>Fonts KCM: Port from kde_file.h to QFileInfo. <a href='https://commits.kde.org/plasma-desktop/768fa11c277b31c390865af94f47ea811d1ed944'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18454'>D18454</a></li>
<li>Remove dependency on KDELibs4Support for some KCM modules. <a href='https://commits.kde.org/plasma-desktop/ed840a5379c622f9c230b432df0099cc9c9c3049'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18449'>D18449</a></li>
<li>[Look And Feel KCM] Use new icon. <a href='https://commits.kde.org/plasma-desktop/d6669db9aec072b720ff46b804ea0b9de631a31f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18401'>D18401</a></li>
<li>Add tooltip to configure panel button. <a href='https://commits.kde.org/plasma-desktop/7a08f3d135c5c3b62532672aca6dc4ea1d50b0b5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18381'>D18381</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Handle apps which set the desktopFileName property with filename suffix. <a href='https://commits.kde.org/plasma-integration/0e50f23c4c4344f86036c0a25c5c785d3f3354c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21097'>D21097</a></li>
<li>Add missing file. <a href='https://commits.kde.org/plasma-integration/448e043514931e879d288849254dd194c833cb6a'>Commit.</a> </li>
<li>[KDEPlatformSystemTrayIcon] Use generated DBus XML. <a href='https://commits.kde.org/plasma-integration/4aa762c0a472f53bc834bf466428d9355375751c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20381'>D20381</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/plasma-integration/377895a38f764e63baf50d016518dfc4cb75e04c'>Commit.</a> </li>
<li>Bump to Qt5.12 - Drop kwin's custom ServerSideDecorationManager interface. <a href='https://commits.kde.org/plasma-integration/6d6cb17351d15375ac56abd2598d484be94aff2a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18515'>D18515</a></li>
<li>Autotests: remove KDE_FORK_SLAVES, to see if CI works better. <a href='https://commits.kde.org/plasma-integration/a94bbdaadff282a26dece12172234b8bec1a29c9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/plasma-nm/117652cd41bf557bf083c82783a61cfa67fc8e14'>Commit.</a> </li>
<li>Make tests to build. <a href='https://commits.kde.org/plasma-nm/f585fde4298ebf3166bf07d411b8333ca1a07edf'>Commit.</a> </li>
<li>Add WireGuard at the end of the list of VPNs to make it correctly sorted. <a href='https://commits.kde.org/plasma-nm/0feb3062a90f67bcb969fadc56a059a710b0db6f'>Commit.</a> </li>
<li>Add default value for parent in contructors of all validator classes. <a href='https://commits.kde.org/plasma-nm/557c415977ad1e4cfd3fc5274801a40e65f477c3'>Commit.</a> </li>
<li>Update WireGuard to match NetworkManager 1.16 interface. <a href='https://commits.kde.org/plasma-nm/751f4da01b42dbb3dde8924b45c186174f7f5c1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405501'>#405501</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20930'>D20930</a></li>
<li>PasswordField: add option to hide AlwaysAsk mode. <a href='https://commits.kde.org/plasma-nm/5986ffbb1584804f8c429f342f3db1c2752e8b75'>Commit.</a> </li>
<li>Added new option to use GlobalProtect with openconnect. <a href='https://commits.kde.org/plasma-nm/dfbcf8b18a4b6969a0eb026aa962d7973b37b459'>Commit.</a> Implements feature <a href='https://bugs.kde.org/405389'>#405389</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21111'>D21111</a></li>
<li>Mark "Connection Activated" notifications as low priority. <a href='https://commits.kde.org/plasma-nm/1ce4ea2f9af1ba247ff89b7e8b9f9ac0976e84bc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21102'>D21102</a></li>
<li>Port widget settings window to QQC2+Kirigami FormLayout. <a href='https://commits.kde.org/plasma-nm/48543b1b119253276069fd9ed867a190a970d3ee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21104'>D21104</a></li>
<li>Add OTP support for openconnect VPN. <a href='https://commits.kde.org/plasma-nm/8fb5c6192c1524089a823988b0274c41fee18a22'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18394'>D18394</a></li>
<li>Don't load the KSharedConfig instance on every read. <a href='https://commits.kde.org/plasma-nm/4408af0c1111fa220051a65883a70d552b570434'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20522'>D20522</a></li>
<li>No need to set height to implicitHeight. <a href='https://commits.kde.org/plasma-nm/5014b50b719f7ab9837520799b5d9cde2a8b5c46'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399155'>#399155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20436'>D20436</a></li>
<li>OpenVPN: reneg-sec property doesn't need limitation. <a href='https://commits.kde.org/plasma-nm/6772557eb0e9e281db1b838712b94d4fce00b690'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404614'>#404614</a></li>
<li>Inform about missing NetworkManager VPN plugin. <a href='https://commits.kde.org/plasma-nm/0903dc0178ede2ef96e8155b2732f947e37449c3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405088'>#405088</a></li>
<li>Change entry of Endpoint UI. <a href='https://commits.kde.org/plasma-nm/3dea89bad64a4e69ccd8e2d03e8e7ff71096553e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403548'>#403548</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19434'>D19434</a></li>
<li>Send a signal over dbus when the wrong password is provided. <a href='https://commits.kde.org/plasma-nm/63a541c4425fddea8d08d82e0be90e7a99b6bc8a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19306'>D19306</a></li>
<li>[plasma-nm] Refresh wifi networks as fast as possible. <a href='https://commits.kde.org/plasma-nm/1b49255d81f0949decf5f9e2a2874b26dac2c392'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18896'>D18896</a></li>
<li>Remove redundant DNS field. <a href='https://commits.kde.org/plasma-nm/29b6d0ee59001200de390d4926ada432670ccab8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403546'>#403546</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18867'>D18867</a></li>
<li>Do not check NM version++. <a href='https://commits.kde.org/plasma-nm/d77d4cb4bdce41aa8c07e834adcd47f75349124d'>Commit.</a> </li>
<li>Do not check NM version. <a href='https://commits.kde.org/plasma-nm/12ebc3fde7bd707c36a3472c62cd469824ebbdda'>Commit.</a> </li>
<li>Plasma-nm Connection Icon not showing correct icon when using a bridge. <a href='https://commits.kde.org/plasma-nm/bc4d82c1565791db704788e5825e800bb07fc544'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397352'>#397352</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18689'>D18689</a></li>
<li>Fix connections' context menus position and parent. <a href='https://commits.kde.org/plasma-nm/5000fc973c6d364887dd7ad6407a042109aa533a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18745'>D18745</a></li>
<li>Get rid of Q_FOREACH. <a href='https://commits.kde.org/plasma-nm/b7ac6986190443a47f583bea1084e25075a0cdc6'>Commit.</a> </li>
<li>We require NM 1.4.0+ so we can remove these conditions. <a href='https://commits.kde.org/plasma-nm/3a9706bafbf81749c2f9cc1b036aef8ffe4a8e23'>Commit.</a> </li>
<li>Add a popup search bar to the plasma-nm applet. <a href='https://commits.kde.org/plasma-nm/684ee0c13bfbafebf271cdd61a18e8590103323a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344789'>#344789</a>. Fixes bug <a href='https://bugs.kde.org/394290'>#394290</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18607'>D18607</a></li>
<li>[plasma-nm/applet] Use correct HIG-approved string for Configure... action. <a href='https://commits.kde.org/plasma-nm/4d1bafe924c57ec945af6de66e771b56f8461c9f'>Commit.</a> </li>
<li>Configuration to never show the passwors dialog. <a href='https://commits.kde.org/plasma-nm/1d78413999246cd1402fd5bbf965aa0e7a859f79'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18588'>D18588</a></li>
<li>[plasma-nm/applet] Add right-click context menu to directly customize a connection. <a href='https://commits.kde.org/plasma-nm/32d76665a694602a01392b11c612f27ef56fdb63'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18504'>D18504</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/plasma-pa/3b56868311a4e7d15032c44b7382601f1fbc044e'>Commit.</a> </li>
<li>Fix build where GObject & deps libraries are not in default library path. <a href='https://commits.kde.org/plasma-pa/fba8f7327aaf9869f2873068842970cabb1c8a0b'>Commit.</a> </li>
<li>Port widget settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/plasma-pa/9c3d0f2dffc27297dcb9b7fa113e09be3fdc24f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21105'>D21105</a></li>
<li>[Microphone Indicator] Avoid duplicate application names. <a href='https://commits.kde.org/plasma-pa/2a3313b749e5739a59672546aad6bfcf193c71ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21114'>D21114</a></li>
<li>[Microphone Indicator] Schedule update initially. <a href='https://commits.kde.org/plasma-pa/2225ea1570f2b3fc19328d370fca06dbc3c8b00d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21047'>D21047</a></li>
<li>[Microphone Indicator] There's no virtual sources, just source outputs. <a href='https://commits.kde.org/plasma-pa/cde3ecde25c743cb71c4d75a6f777390e35fbd8c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21048'>D21048</a></li>
<li>Don't play sound when muting. <a href='https://commits.kde.org/plasma-pa/ada2f9c75019e15c37dbfdbe8a4ae42c69ec6a83'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21045'>D21045</a></li>
<li>[plasma-pa] Add command to switch all applications to the selected device. <a href='https://commits.kde.org/plasma-pa/61259acda87bf3515f94c98c254d41e25f819d44'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19014'>D19014</a></li>
<li>Don't abort when org.freedesktop.pulseaudio.module-group schema isn't installed. <a href='https://commits.kde.org/plasma-pa/25de7eb8c0dc3a5f1935cedd0cae51064f57b73f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20915'>D20915</a></li>
<li>Define constants in cpp. <a href='https://commits.kde.org/plasma-pa/2dc4b94b02e72b63a456a0986dbdf1536e930ca8'>Commit.</a> </li>
<li>Use own headers. <a href='https://commits.kde.org/plasma-pa/a7a9b2d5da621ffc5c69d72cc7cb2ceafe1db444'>Commit.</a> </li>
<li>Add microphone indicator. <a href='https://commits.kde.org/plasma-pa/2d82429e5299bf3e6dd00f995d366fadfb3af665'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19994'>D19994</a></li>
<li>Port from GConf to GSettings. <a href='https://commits.kde.org/plasma-pa/e04e034c24b832fb7803c14e1f99f4b46beab831'>Commit.</a> See bug <a href='https://bugs.kde.org/386665'>#386665</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14147'>D14147</a></li>
<li>Use FindGLIB2 from ECM. <a href='https://commits.kde.org/plasma-pa/0b1559c47c3aff108329c91bacea08a4783848c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19463'>D19463</a></li>
<li>Port to ECM's FindCanberra. <a href='https://commits.kde.org/plasma-pa/dd7094b49b87bea7b8bffe23ae0818834754e975'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19382'>D19382</a></li>
<li>[plasma-pa] Increase minimum size of expanded plasmoid. <a href='https://commits.kde.org/plasma-pa/6e53d78136dff5f5bb395c2cc4ec93d2fbc0b1a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19013'>D19013</a></li>
<li>[KCM] Port to QQC2. <a href='https://commits.kde.org/plasma-pa/00026cee9ec8c5d04067cf892b46e5a5507b7f6d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397954'>#397954</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15304'>D15304</a></li>
<li>Bump to cmake requirement to 3.5. <a href='https://commits.kde.org/plasma-pa/908501f116e71053d8e6c3790cf9a5752ac1f06f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18806'>D18806</a></li>
<li>Find volume feedback's runtime requirements. <a href='https://commits.kde.org/plasma-pa/9cab61adecb1e8a8fc7bedc527122dfa473a33e7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18807'>D18807</a></li>
<li>Don't ignore default virtual devices. <a href='https://commits.kde.org/plasma-pa/5dc7cca58a72cc6c357ac2457de6868ad3a61305'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17784'>D17784</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/plasma-sdk/2b4c64d84e1cb6adb9a081f83bc3546e32b75c01'>Commit.</a> </li>
<li>Themeexplorer: sync analog clock code from applet: smooth hand on animation. <a href='https://commits.kde.org/plasma-sdk/22d0840936a5c9d94ca77e62e0c1fbc84cbc123f'>Commit.</a> </li>
<li>Themeexplorer: sync analog clock code from applet. <a href='https://commits.kde.org/plasma-sdk/3e6f1ee65c2ce8592ee12d357f3ce85a4b263889'>Commit.</a> </li>
<li>[Cuttlefish] Port toolbar to QQC2 which fixes combobox text being corrupted or invisible. <a href='https://commits.kde.org/plasma-sdk/6e6b8da30a9c9fecc610f3d1a92c1f0b2a36d26f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366543'>#366543</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20188'>D20188</a></li>
<li>Port away from deprecated KLineEdit::setClearButtonShown(). <a href='https://commits.kde.org/plasma-sdk/a343c547657d31907ad1c47e33185a82792d0b71'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/plasma-vault/7c3384ebad92dcf5ef16273cd5c41d9c9ea1363d'>Commit.</a> </li>
<li>Show Vaults action only for local folders. <a href='https://commits.kde.org/plasma-vault/b553905222dca735e5aa58a9535527de365f9f1e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20635'>D20635</a></li>
<li>Mark the source directory as busy while Vault is mounted. <a href='https://commits.kde.org/plasma-vault/9551fc218b95f94bcde6aba5ef4d2c6fb1f405bd'>Commit.</a> </li>
<li>Added support for vanishing vaults. <a href='https://commits.kde.org/plasma-vault/a13da523f08cf45f8526397a56f7a53751385366'>Commit.</a> </li>
<li>Restore network only if needed. <a href='https://commits.kde.org/plasma-vault/1a3726060cb5cb52ea2d2d944687a548dd26e622'>Commit.</a> </li>
<li>Never use non-resolved paths for mounts and devices. <a href='https://commits.kde.org/plasma-vault/dc21669bba0351291bd9baa7e0e2029c4fdbd093'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405527'>#405527</a></li>
<li>Change http URLs to https. <a href='https://commits.kde.org/plasma-vault/a73e803a39f385a17b0eb05b45fed1120e4ae33f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19997'>D19997</a></li>
<li>FileItem plugin for opening and closing Vaults from Dolphin. <a href='https://commits.kde.org/plasma-vault/a8210056def276ea1b41db666632466f801b17f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388602'>#388602</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19991'>D19991</a></li>
<li>Add a notification when mount failed because of a non empty mount point. <a href='https://commits.kde.org/plasma-vault/4574e80916ae4f4b29d60fd41dd52d3d52bb3e8f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401413'>#401413</a></li>
<li>Go offline as soon as the password prompt is shown. <a href='https://commits.kde.org/plasma-vault/54a7b6cba169af8c6a0c25f3bdc03a4922e85001'>Commit.</a> </li>
<li>Sort vaults in the applet by name. <a href='https://commits.kde.org/plasma-vault/fd3d8064222da4d24e24637b59518661a895549f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398964'>#398964</a></li>
<li>Do not load the payload from the first page when opening the wizard. <a href='https://commits.kde.org/plasma-vault/b099ea3d2be4905f2ae0040615a5129cb9414d36'>Commit.</a> </li>
<li>Disable the dialog while creating the vault. <a href='https://commits.kde.org/plasma-vault/1ba5dccb0bccdc757ad395d91597860c45efb1f9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[System tray] Modernize "Entries" category. <a href='https://commits.kde.org/plasma-workspace/fc086efe7bd21a4e66b64e9b68ba8d0e2d517ecd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21189'>D21189</a></li>
<li>[Holidays plugin] Port partially to QQC2 and modernize UI. <a href='https://commits.kde.org/plasma-workspace/cde0e90b2eb102a3311beab8ff8eb2bc530c561b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21194'>D21194</a></li>
<li>PanelView: update mask on window on change of panelMask property. <a href='https://commits.kde.org/plasma-workspace/f8d5e1a24e97afc8c40c36fe106cc2ffe99af0a9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21176'>D21176</a></li>
<li>Replace QUrl::fromUserInput by lighter heuristic. <a href='https://commits.kde.org/plasma-workspace/d38d7df3e170764296feb319fa73ed1a6aeac2ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21164'>D21164</a></li>
<li>[Notifications] Take the screen geometry into account when positioning popup notifications. <a href='https://commits.kde.org/plasma-workspace/1e175254dd8c7c5230fd5f41d2c4de55cb6d50d1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21184'>D21184</a></li>
<li>[Notifications] Fix oversight of not taking into account screen X when placing notifications on the right side of the screen. <a href='https://commits.kde.org/plasma-workspace/b220dcbbdf8bfa00abe5e7d26f1d434cc86d9aaf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21152'>D21152</a></li>
<li>Check if service is still registered before showing progress. <a href='https://commits.kde.org/plasma-workspace/01333913374e0a381382c139a9000972add29346'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21180'>D21180</a></li>
<li>Add Messages.sh. <a href='https://commits.kde.org/plasma-workspace/71dbe70d2c1568381e74d6856d953c51e8508744'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21167'>D21167</a></li>
<li>[Notifications] When un-dismissing job hide plasmoid popup. <a href='https://commits.kde.org/plasma-workspace/7d0a0a007746746be9d951c8eebd5ffd119072d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21165'>D21165</a></li>
<li>[Notifications] Rename device name to origin name. <a href='https://commits.kde.org/plasma-workspace/9d0f3b37fcb8344e3f66f9dd48b5265ff184f24f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21137'>D21137</a></li>
<li>[Lock, login & logout screen] Add visual feedback to action buttons when pressed. <a href='https://commits.kde.org/plasma-workspace/eaa5f94c0584db17b3b75a094097479ee80e6c51'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21062'>D21062</a></li>
<li>[Notifications] Fix bottom dialog padding. <a href='https://commits.kde.org/plasma-workspace/74000622121fd0c9f716512b944e2b05b8451b53'>Commit.</a> </li>
<li>Put a space between value and unit. <a href='https://commits.kde.org/plasma-workspace/51046ec6abf8c4e42d5afd0377940b5813c1542c'>Commit.</a> </li>
<li>Final round of review comments. <a href='https://commits.kde.org/plasma-workspace/972840c9232b1e31ffc8869bf4114ab0b288f05f'>Commit.</a> </li>
<li>[System Monitor] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/plasma-workspace/1a980bedc4d14ccb0fadbe2e6d56102691d19ff5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21074'>D21074</a></li>
<li>Let settings query for badge blacklist. <a href='https://commits.kde.org/plasma-workspace/0c54b26dc2ab7dd3690cc66fb146eea35dd2de69'>Commit.</a> </li>
<li>[PanelView] remove outdated hack to support mask without compositing. <a href='https://commits.kde.org/plasma-workspace/1f37ba3b81f2fb03214f361726263bf1591ab93d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21013'>D21013</a></li>
<li>Address review comments and other clean up. <a href='https://commits.kde.org/plasma-workspace/86260aba9a49adeb1f02c0ce108752ba42d8f811'>Commit.</a> </li>
<li>[PanelView] Update mask as last on resize event (& move one for consistency). <a href='https://commits.kde.org/plasma-workspace/b51fd4bab2a56266e338fb0f3a8becc914a4c43d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21012'>D21012</a></li>
<li>Panels: reset shadow pixmaps on theme change. <a href='https://commits.kde.org/plasma-workspace/93bdcda4970277d9d2ce0aac0a96331b29d949c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21033'>D21033</a></li>
<li>[color-wallpaper] Interlink child and parent form layout. <a href='https://commits.kde.org/plasma-workspace/a1903620f0f06ad83c487eb109c0130391d1cdfc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20965'>D20965</a></li>
<li>Add SDDM theme login load feedback. <a href='https://commits.kde.org/plasma-workspace/e9c72a0420c1dba130ba694f9f40f8c48e1c7ff2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20902'>D20902</a></li>
<li>[Calendar] Port settings window to QQC2+Kirigami FormLayout and modernize UI. <a href='https://commits.kde.org/plasma-workspace/e39aed6f167f5c6ee0b84e2a43021a26b14a7fd8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20829'>D20829</a></li>
<li>Address review comments and some more cleanups. <a href='https://commits.kde.org/plasma-workspace/4269a3b407f339a60b0ccee017278650d2681352'>Commit.</a> </li>
<li>[MPRIS Data Engine] Avoid blocking calls when changing volume. <a href='https://commits.kde.org/plasma-workspace/9b24060c632effe6d753b8e376ae552ee9c0fa7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20661'>D20661</a></li>
<li>[analog-clock] Render hand also smooth during animation. <a href='https://commits.kde.org/plasma-workspace/74ccb591c484134421364c28faffbed0ed0a32ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20561'>D20561</a></li>
<li>Kill kuiserver. <a href='https://commits.kde.org/plasma-workspace/08f894ca352954a29dd4eac2bc59331cab0b0c14'>Commit.</a> </li>
<li>More cleanups and finish. <a href='https://commits.kde.org/plasma-workspace/789eedc52da38a2fe0a914250031e0ea1083225c'>Commit.</a> </li>
<li>[analog-clock] Allow themes to define hand shadow offset & hand rot center. <a href='https://commits.kde.org/plasma-workspace/2558f362628021f8a4d5c6230faf942273bf52db'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20112'>D20112</a></li>
<li>Fix merge error. <a href='https://commits.kde.org/plasma-workspace/b65b360b0ad066663327c10d3b356b4277f59041'>Commit.</a> </li>
<li>[KRunner] Avoid writing history if addToHistory is idempotent. <a href='https://commits.kde.org/plasma-workspace/9686c05c7226602fd5e984fd7cbed52778e925b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20383'>D20383</a></li>
<li>[KRunner] Move trivial check in addToHistory to the front. <a href='https://commits.kde.org/plasma-workspace/4a5f62fee95284c2571dbb98b9e7d1a5f9060c75'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20382'>D20382</a></li>
<li>Fix always full rect blur mask for panels ignoring shape from Plasma themes. <a href='https://commits.kde.org/plasma-workspace/65367689ea5e6443eeea36398dda0bd48503e1bc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20204'>D20204</a></li>
<li>Fix build. <a href='https://commits.kde.org/plasma-workspace/8c0be01fad0a0302d4c02ee680526168a1784502'>Commit.</a> </li>
<li>Add some more API docs. <a href='https://commits.kde.org/plasma-workspace/bb3b555e5a9211b185dcdd63a8b4e6da02b504ae'>Commit.</a> </li>
<li>Implement PulseAudio for disabling notification sounds. <a href='https://commits.kde.org/plasma-workspace/02d2f32fab9b4e237a8c4dc656bb9ce05951b14d'>Commit.</a> </li>
<li>Update link to Kdelibs_Coding_Style. <a href='https://commits.kde.org/plasma-workspace/c69acc0973a5a58de5f9ea0a25fa183b09bfe6b8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20248'>D20248</a></li>
<li>Add --replace option in krunner. <a href='https://commits.kde.org/plasma-workspace/f546973b4ced7924e18b65ae8e3edd3df3aa5439'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20256'>D20256</a></li>
<li>Final touches. <a href='https://commits.kde.org/plasma-workspace/2d1b66fb88c54d5d212f9b6bee5144842c7b0f64'>Commit.</a> </li>
<li>[image-wallpaper] Port to Kirigami.FormLayout and use twinFormLayouts. <a href='https://commits.kde.org/plasma-workspace/4516ab53726cc8f206f6f34ac5f4421313bcc70b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19873'>D19873</a></li>
<li>Implement do not disturb mode and grouping collapse. <a href='https://commits.kde.org/plasma-workspace/9cee05eb24d6de40820fc373c678ee091840ad7d'>Commit.</a> </li>
<li>Implement do not disturb mode, more history work, cleanup. <a href='https://commits.kde.org/plasma-workspace/54b4fc09130627ac4260746e4983366b9103983e'>Commit.</a> </li>
<li>[color-wallpaper] Align with the master FormLayout. <a href='https://commits.kde.org/plasma-workspace/b40dabdc27e421e32138720432e7892119da2693'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20048'>D20048</a></li>
<li>Replace old notification plasmoid by new one. <a href='https://commits.kde.org/plasma-workspace/e93bba28c29907aefcd8391347dd7f0f47ba3947'>Commit.</a> </li>
<li>Completely wire up old dataengine and further touches. <a href='https://commits.kde.org/plasma-workspace/723b6d13d172e5b1879c36e406d9bc75b41153e9'>Commit.</a> </li>
<li>Allow single images to be excluded from the slideshow. <a href='https://commits.kde.org/plasma-workspace/b1ae890a6ee606a17824cc589b7b6bec2915370e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19687'>D19687</a></li>
<li>Do not try to set shadows for panels if theme does not provide elements. <a href='https://commits.kde.org/plasma-workspace/2a63696f57f46bfbcdfaf64712668a46e41119f8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20051'>D20051</a></li>
<li>Improvements and cleanups all over the place. <a href='https://commits.kde.org/plasma-workspace/1c4de1d4fe11e5c85b1ff28257c39ec92a9db80b'>Commit.</a> </li>
<li>Begin on inhibition / do not disturb stuff. <a href='https://commits.kde.org/plasma-workspace/eab8338950b2b9ae18492eeb2760d051d4eb9daf'>Commit.</a> </li>
<li>Enable blurbehind also for panel controls only if theme sets it. <a href='https://commits.kde.org/plasma-workspace/00dcb777918e1ad4e6f88a52fedaea2e941cd62d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20057'>D20057</a></li>
<li>Begin settings stuff and further cleanup. <a href='https://commits.kde.org/plasma-workspace/ff9ca8e4a9517fff70d5d3c24c719325875ddaba'>Commit.</a> </li>
<li>Cleanup, export, and start on settings and inhibition API. <a href='https://commits.kde.org/plasma-workspace/79427e3d4d676626ae73ecc3fb32f7affa9d9db9'>Commit.</a> </li>
<li>Systray: show a the context menu upon press an hold on the icon. <a href='https://commits.kde.org/plasma-workspace/890e91b29bfea52c10b13c055d4328528e9690f1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19946'>D19946</a></li>
<li>Enable blurbehind for panels only if theme sets it. <a href='https://commits.kde.org/plasma-workspace/8ef6104a2d0266280f45c55c0d88407cb60927f4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19909'>D19909</a></li>
<li>Implement more missing stuff and be more spec-compliant. <a href='https://commits.kde.org/plasma-workspace/3251c2806bb3febd75bd05ce3d8482b2d3266bf1'>Commit.</a> </li>
<li>Allow copying job source/dest and cleanup a bit. <a href='https://commits.kde.org/plasma-workspace/fb620e0747c171a7e0c821b992fc39e61b913a27'>Commit.</a> </li>
<li>Systray: show biger icons when on tablet mode. <a href='https://commits.kde.org/plasma-workspace/b2b4959a905e2ddfdfcdc637a943464ffa309ae5'>Commit.</a> </li>
<li>[sddm-theme] Update login screen preview image. <a href='https://commits.kde.org/plasma-workspace/94dcbf0500aaf81a9667276456f91f06cf3b81cb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19469'>D19469</a></li>
<li>[sddm-theme/lock screen] Move main stack to a more vertically centered position. <a href='https://commits.kde.org/plasma-workspace/154637e691197ca8257f425a5fd1c0bf4f46e38c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19916'>D19916</a></li>
<li>Implement all the things. <a href='https://commits.kde.org/plasma-workspace/35c575be402733c0038b4130807c414f0249a815'>Commit.</a> </li>
<li>Port to the new install directory for knsrc files. <a href='https://commits.kde.org/plasma-workspace/806a044da1d59f8a1b6196f0d051347ce1df6c65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19823'>D19823</a></li>
<li>[sddm-theme/lock screen] Use QQC2 for the clock labels. <a href='https://commits.kde.org/plasma-workspace/d7654042677a1856daaf4cd71b1965368a44ef0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404651'>#404651</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19647'>D19647</a></li>
<li>[sddm-theme/lock screen] Overhaul font sizes. <a href='https://commits.kde.org/plasma-workspace/d3aa8b07c1682e1bf7ff0f9e04b83caec15624fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19631'>D19631</a></li>
<li>[KSplashQML] Update stages comment and make code clearer. <a href='https://commits.kde.org/plasma-workspace/f899ccdc0fa3e28dd614c06f297d1d5fccceeb67'>Commit.</a> See bug <a href='https://bugs.kde.org/405446'>#405446</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19801'>D19801</a></li>
<li>[lock-logout] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/plasma-workspace/cef563e560ac45bea077a21e8cffde23f905d251'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19746'>D19746</a></li>
<li>[appmenu] Port configuration window to QQC2 and Kirigami.FormLayout. <a href='https://commits.kde.org/plasma-workspace/08feca1f6859ec8a643173a8760d37d3df352ec8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19670'>D19670</a></li>
<li>Adjust date string height to match time string with vertical panel. <a href='https://commits.kde.org/plasma-workspace/8f1b10b23a96eec525178d8ac314d6fd21ae44d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404611'>#404611</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19194'>D19194</a></li>
<li>[Logout Screen] Show info that rebooting will enter firmware setup. <a href='https://commits.kde.org/plasma-workspace/4c28cc3346d7671cfeaba7a18c565f3c3e230338'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19562'>D19562</a></li>
<li>Port foreach. <a href='https://commits.kde.org/plasma-workspace/0e4148bc01a02b9080967c8fd1a47abb4103eb53'>Commit.</a> </li>
<li>Image Wallpaper Slideshow - display the list of images that will be shown. <a href='https://commits.kde.org/plasma-workspace/2003b267d4bcc6d0d2bee585d9da0c0abde6e85d'>Commit.</a> Implements feature <a href='https://bugs.kde.org/403703'>#403703</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18809'>D18809</a></li>
<li>Fix warning == vs ===. <a href='https://commits.kde.org/plasma-workspace/19144ab096468493e541867fea4abab2cc13c312'>Commit.</a> </li>
<li>[sddm-theme/lock screen] Improve appearance in software rendering mode (outlines and icons). <a href='https://commits.kde.org/plasma-workspace/aca7fd3873070b32c2d03b90775d1f7e21b91e83'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19609'>D19609</a></li>
<li>[sddm-theme/lock screen] Remove username shadow and show clock shadow only when unfocused. <a href='https://commits.kde.org/plasma-workspace/28a6ef22350d5f1286f74b8f5656c0ec82bcfd49'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19468'>D19468</a></li>
<li>[Login, Lock, and Logout Screen] Make the avatar background circle more subtle. <a href='https://commits.kde.org/plasma-workspace/408c6d6319ee6d997f7025119ed1bc42c0686220'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19478'>D19478</a></li>
<li>Don't change font renderers. <a href='https://commits.kde.org/plasma-workspace/78e53ef625504f2e502009972f3acd95188d4018'>Commit.</a> </li>
<li>[sddm-theme] Replace login button label with icon. <a href='https://commits.kde.org/plasma-workspace/602e5533d348d2b81604494349348083ac5b8780'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19214'>D19214</a></li>
<li>[Lock, Login, and Logout screen] Adjust visual feedback of action buttons. <a href='https://commits.kde.org/plasma-workspace/99dd92635f27e6a64efe2ac6d803dcf8a3608752'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393048'>#393048</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19479'>D19479</a></li>
<li>[sddm-theme/lock screen] Fix "unable to assign [undefined]" error. <a href='https://commits.kde.org/plasma-workspace/06793f21d1ced7eb45c3d948dbfa7c26aa5222ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19577'>D19577</a></li>
<li>[Logout screen] Fix label opacity failing. <a href='https://commits.kde.org/plasma-workspace/076671638f1dbef1e20dc8498273abd2822a224f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19510'>D19510</a></li>
<li>[sddm-theme/lock screen] Use default font size adjustments instead of hardcoded sizes. <a href='https://commits.kde.org/plasma-workspace/9816994ecd0251523717c567e141e14783ecba49'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19547'>D19547</a></li>
<li>[sddm-theme/lock screen] Fix initial stage of avatar animation effect; fix trailing whitespace. <a href='https://commits.kde.org/plasma-workspace/164b2dcdaff74881917fd09174cc48d115e5acdf'>Commit.</a> </li>
<li>Fix trailing whitespace. <a href='https://commits.kde.org/plasma-workspace/026e11ffe42cc83a4cd8125823ffbd272e2b04d2'>Commit.</a> </li>
<li>Only match service text strings once. <a href='https://commits.kde.org/plasma-workspace/68a2807654c64747653811329a9b0e57eaa61693'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19475'>D19475</a></li>
<li>[KRunner, SDDM theme, Logout screen, Login widget] Use "Sleep" on username prompt screen. <a href='https://commits.kde.org/plasma-workspace/e588bebe78d27a92da2db0ba4260c26f76e28743'>Commit.</a> </li>
<li>[sddm-theme] Enlarge user avatar in focus. <a href='https://commits.kde.org/plasma-workspace/458576b2b5db01376d9d25066c4b72cf477790ae'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19372'>D19372</a></li>
<li>[Logout screen] Change labels to match SDDM theme labels. <a href='https://commits.kde.org/plasma-workspace/d8a7443605bddd17d2a3d090c169630ebb1539c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19458'>D19458</a></li>
<li>[sddm-theme/lock screen] Adjust login screen and lock screen font sizes. <a href='https://commits.kde.org/plasma-workspace/dd4176d1f669809cfe9e85b336413365a717d7cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19433'>D19433</a></li>
<li>[sddm-theme/lock screen] Adjust login and lock screen blur effect. <a href='https://commits.kde.org/plasma-workspace/2eec8cb1b5056cac670c2a29b718b250477eab2f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19409'>D19409</a></li>
<li>[Digital Clock] Only show Formats KCM button if the user has permission to use it. <a href='https://commits.kde.org/plasma-workspace/6652659c3e636c05648508cbe99d850d705e5ca0'>Commit.</a> </li>
<li>Remove deprecated method. <a href='https://commits.kde.org/plasma-workspace/fc36372523f59ca10e98ae1c2d8f80da5d2b832c'>Commit.</a> </li>
<li>[sddm-theme/lock screen] Tighten clock and username shadows. <a href='https://commits.kde.org/plasma-workspace/2286c89b9e314f1bd8f03e0caa06831c22bb8615'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19325'>D19325</a></li>
<li>[Digital Clock] Replace 12/24hr tri-state checkbox in config UI with combobox. <a href='https://commits.kde.org/plasma-workspace/74f0f259465543e52ad93aed2d2fb8471c9f9777'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402487'>#402487</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19230'>D19230</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/plasma-workspace/ee648c4c330136d74790300439538b14489f2f98'>Commit.</a> </li>
<li>[KRunner, SDDM theme, Logout screen, Login widget] Use the word "Sleep" instead of "Suspend". <a href='https://commits.kde.org/plasma-workspace/9524117322008ae6c12438ae7f77749651cd84ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19185'>D19185</a></li>
<li>[sddm-theme/lock screen] Render shadows before (and below) labels. <a href='https://commits.kde.org/plasma-workspace/3bdbf2ff87d49d8f2bd6c9588e6938660b9a9e51'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19366'>D19366</a></li>
<li>Guard virtualDesktopManagement being accessed before fetched. <a href='https://commits.kde.org/plasma-workspace/895aee564faebb113bd00cf45d42105b913ab2f9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19029'>D19029</a></li>
<li>[batterymonitor] Port Battery and Brightness settings to QQC2 and Kirigami. <a href='https://commits.kde.org/plasma-workspace/4c691ce09f182ddedd921c4c4d2220d11b3b2b34'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19295'>D19295</a></li>
<li>[DeviceNotifier] Port settings to QQC2 and Kirigami. <a href='https://commits.kde.org/plasma-workspace/6be6a8c003c634e334c346d4a67c67569cb8123d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19289'>D19289</a></li>
<li>Block screen filtering for global menu applet. <a href='https://commits.kde.org/plasma-workspace/39d220b4b6577c2c6e027f4b15ab011b4c7a5fbc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404500'>#404500</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19126'>D19126</a></li>
<li>[sddm-theme] Apply new username prompt and user list icons. <a href='https://commits.kde.org/plasma-workspace/8ee8ca17290e514a2d8966bcbd8b476b0e2cfec7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19129'>D19129</a></li>
<li>Fix a typo in a comment. <a href='https://commits.kde.org/plasma-workspace/99bd3b86f263a812fbab99f7a7ad1b6350fd9eaf'>Commit.</a> </li>
<li>Link to AuthCore instead of Auth. <a href='https://commits.kde.org/plasma-workspace/a748fb9e9be1e8b50c85adb9ebcb22fe5b16c9e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19066'>D19066</a></li>
<li>[sddm-theme] Change "Type User" to "Other...". <a href='https://commits.kde.org/plasma-workspace/ed20b9e3d4c940a54fdea04e11d03d71ee02bc05'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19050'>D19050</a></li>
<li>Make Breeze theme tooltip texts consistent. <a href='https://commits.kde.org/plasma-workspace/f6f08e97ce8d5841c44b3c65cd2704a157bd6ae3'>Commit.</a> </li>
<li>[sddm-theme] Add buttons to username prompt to make it a full-fledged login screen alternative. <a href='https://commits.kde.org/plasma-workspace/ccefa20ae859bf4ee6f0006be4e54f368ea9aed8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18893'>D18893</a></li>
<li>[Icon Applet] Parse jump list actions and open with actions on demand. <a href='https://commits.kde.org/plasma-workspace/b884003bebd734edb9a75f44901c96b91ce8452c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18925'>D18925</a></li>
<li>Drop pre Qt5.12 workarounds. <a href='https://commits.kde.org/plasma-workspace/66b734dfc611f5cd0a807c868b973ca251b26cdf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18514'>D18514</a></li>
<li>[sddm-theme] Use more precise labels for "Different User". <a href='https://commits.kde.org/plasma-workspace/5bdd05f27cf5ebf1e8ff360f6d7381efc08ccbc1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18831'>D18831</a></li>
<li>[kio_applications] Install binary into kf5/kio subdir, like other kio binaries. <a href='https://commits.kde.org/plasma-workspace/91fbb2e7ef04439419a27b2d954a109a3ab4d287'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18642'>D18642</a></li>
<li>Port away from deprecated KIO::UDSEntry::insert(). <a href='https://commits.kde.org/plasma-workspace/bcddc79246848890d85f66a1e80227a8517e831f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18632'>D18632</a></li>
<li>[image wallpaper] Simplify tooltip string and add context for translators. <a href='https://commits.kde.org/plasma-workspace/51b2c71308374dafd974937ca1f83fed4f49c1ea'>Commit.</a> </li>
<li>[Icon Applet] Use libTaskManager for startup feedback. <a href='https://commits.kde.org/plasma-workspace/4c211cbf885a525d0977d0bdd2ce6f0567a071e6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18727'>D18727</a></li>
<li>[Icon Applet] Show busy indicator when launching app. <a href='https://commits.kde.org/plasma-workspace/ac3fa8cbee19e0fdcdf5fa6b744d5657720b9576'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18701'>D18701</a></li>
<li>Use new Notifications KCM icon in other appropriate places too. <a href='https://commits.kde.org/plasma-workspace/36a85c5e56389bc5ecc30a258c587615a3a90cbe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18723'>D18723</a></li>
<li>[weather dataengine]: bbc: fix description, UK MET no longer the data provider. <a href='https://commits.kde.org/plasma-workspace/7e852a230ed8995f1db7c76b3209e50a5f03aed8'>Commit.</a> </li>
<li>Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH. <a href='https://commits.kde.org/plasma-workspace/311055e6663136ee09da0212058b651956a8c98f'>Commit.</a> </li>
<li>[wallpapers/image] Don't use PlasmaCore in desktop themed UI. <a href='https://commits.kde.org/plasma-workspace/98d9f681a37e2ac2feb6bf5cb5e8a54f4c7e874e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18347'>D18347</a></li>
<li>Change icons for Activities dataengine and runner. <a href='https://commits.kde.org/plasma-workspace/ffc47c6e226ea7f540e69fc700afff6af30c4e68'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18532'>D18532</a></li>
<li>Use Wayland protocol to handle rows; drop D-Bus code. <a href='https://commits.kde.org/plasma-workspace/7f8161c61337c7642bbaa1cab9c62593081ed613'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18517'>D18517</a></li>
<li>Remove duplicated file. <a href='https://commits.kde.org/plasma-workspace/0aee25b30f448074406e7d8ce3e932a3e63f3bd4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17920'>D17920</a></li>
<li>[DesktopView] Don't set a window icon. <a href='https://commits.kde.org/plasma-workspace/60d56d8803029a9d00f18f1ae6724d122c658dca'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18430'>D18430</a></li>
<li>Add shadow to Hour's hand. <a href='https://commits.kde.org/plasma-workspace/975df8f8930cec858215ca4a87be93f28db29afd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396612'>#396612</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17751'>D17751</a></li>
<li>[Device Notifier] Add a button to unmount all devices. <a href='https://commits.kde.org/plasma-workspace/81db74434cd596a7ed2ee6a3b0d39e6f2434065d'>Commit.</a> Implements feature <a href='https://bugs.kde.org/395644'>#395644</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16212'>D16212</a></li>
<li>Disable autotests dir for now. <a href='https://commits.kde.org/plasma-workspace/7eeb28ec85bb1388421126dec9e676fa7a0fc5e5'>Commit.</a> </li>
<li>WIP: libnotificationmanager and new notification applet. <a href='https://commits.kde.org/plasma-workspace/73c552846995066cc3d0a2a4989ff9d388a00e05'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace-wallpapers' href='https://commits.kde.org/plasma-workspace-wallpapers'>Plasma Workspace Wallpapers</a> </h3>
<ul id='ulplasma-workspace-wallpapers' style='display: block'>
<li>[plasma-workspace-wallpapers] Add the Elarun wallpaper. <a href='https://commits.kde.org/plasma-workspace-wallpapers/484ff7c7c3d3a9260871ed5714cf330dba2c1796'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19872'>D19872</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/plymouth-kcm/8912d237b99a80621fd9a4a075c6f129ae2634db'>Commit.</a> </li>
<li>Add .arcconfig file. <a href='https://commits.kde.org/plymouth-kcm/cd7472d13f6182bbe33119b8c3d452ad069be0cf'>Commit.</a> </li>
<li>Use AuthCore instead Auth. <a href='https://commits.kde.org/plymouth-kcm/03d4bab121eb7adcff1a1ffc6afba088e3112863'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19068'>D19068</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/polkit-kde-agent-1/d2911b2c2bf79891ee756b12c355a6cda4e5357a'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/powerdevil/0fe91e690fa17978424e1f5d781b2feaaf37b853'>Commit.</a> </li>
<li>Move files to their correct place. <a href='https://commits.kde.org/powerdevil/889a3d05bc37c6fe5a1d1bb06c89b37ce1ab5f60'>Commit.</a> </li>
<li>DDCUtil: Improved DDCUtil support for brightness control over DDC/CI channel for supported monitors. <a href='https://commits.kde.org/powerdevil/cbd6d0eaa6fcb31eb37e74752de7bce537b34f0a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8626'>D8626</a></li>
<li>Set Critical urgency for battery... critical notification. <a href='https://commits.kde.org/powerdevil/8b6faf400f031ae14b8c3d5040530e542c154628'>Commit.</a> </li>
<li>Remove critical error notification. <a href='https://commits.kde.org/powerdevil/a796d78625818d8d3d1d48da1e9c04b53d17b88a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20157'>D20157</a></li>
<li>Fixed initial size and content width of Activity Settings KCM. <a href='https://commits.kde.org/powerdevil/806c4c073ff4fa40924db692981963895f4562b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398793'>#398793</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20055'>D20055</a></li>
<li>[powerdevil] Touch up messages/prompts. <a href='https://commits.kde.org/powerdevil/22d725ca0361ac83cd292d15c1ffd0b95b10a487'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19838'>D19838</a></li>
<li>[powerdevil] Correct labels. <a href='https://commits.kde.org/powerdevil/85ae655bacbb9199b7650813b9e76e6a410b46cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19460'>D19460</a></li>
<li>Fix one last instance of the word "Suspend" in the UI. <a href='https://commits.kde.org/powerdevil/8ad3c3c8ba3d5b2c81bbc884f9a272a25da163c3'>Commit.</a> </li>
<li>[KCM & UI] Use the word "Sleep" instead of "Suspend". <a href='https://commits.kde.org/powerdevil/7ba43d4767f6e4e4ed707d97dbdf324392e472a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19186'>D19186</a></li>
<li>Add KAboutData. <a href='https://commits.kde.org/powerdevil/972eb26d0558d4c97062995eae2eb624d8cb1f38'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19162'>D19162</a></li>
<li>Use the new KAuthCore. <a href='https://commits.kde.org/powerdevil/a1a422c9f2529621d393b6cf02330f4672231aba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19055'>D19055</a></li>
<li>Change icon for Activities config. <a href='https://commits.kde.org/powerdevil/fc25c7f468bd2d77dea05e296e3139c4327691c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18531'>D18531</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/sddm-kcm/65386457ce43c88050c1e9a9b528fdd117fe52ab'>Commit.</a> </li>
<li>Crash in sddmthemeinstaller invalid use of errorString. <a href='https://commits.kde.org/sddm-kcm/7df456404c26200ff6e36fc3d65aa39f2c6e7c3c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406718'>#406718</a>. Fixes bug <a href='https://bugs.kde.org/404609'>#404609</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20859'>D20859</a></li>
<li>Redesign the theme preview window. <a href='https://commits.kde.org/sddm-kcm/9094e982ff0cf5e18bc4aa1caaec582cb30d603f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372844'>#372844</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19077'>D19077</a></li>
<li>[sddm-kcm] Adjust Background label and button. <a href='https://commits.kde.org/sddm-kcm/5a973f13fd6115e67c11ecf9f4624a42800b68d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19209'>D19209</a></li>
<li>Add tigrangab@gmail.com for his fix https://phabricator.kde.org/D19959. <a href='https://commits.kde.org/sddm-kcm/1f0a2a43972fab798a1fe168b95dfcebb8c7a371'>Commit.</a> </li>
<li>Fixed issue causing changed signal to be called with false argument. <a href='https://commits.kde.org/sddm-kcm/ed3decc06e387217bc1318597ba1e97183d3b9b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403366'>#403366</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19959'>D19959</a></li>
<li>Port to std::sort. <a href='https://commits.kde.org/sddm-kcm/90ab66c6759d67207495164936ec6599486324bb'>Commit.</a> </li>
<li>Use AuthCore instead of Auth. <a href='https://commits.kde.org/sddm-kcm/c784114c72eb9434534cee95088fcb9fea0fe273'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19069'>D19069</a></li>
<li>Fix pedantic warning. <a href='https://commits.kde.org/sddm-kcm/4156f4dd41f6f2f70e056f30e984609795cd3602'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>[Sidebar] Fix Kirigami viewBackgroundColor deprecation warnings. <a href='https://commits.kde.org/systemsettings/3196c5cf98d96f12dd31b437f415ae0c570721fd'>Commit.</a> </li>
<li>Remove unused files. <a href='https://commits.kde.org/systemsettings/bf4a7e45de8f47cc32718c926d00728e9a23212c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20894'>D20894</a></li>
<li>Minor: CMake: Use CONFIG search mode more. <a href='https://commits.kde.org/systemsettings/e3528f473993fdf8a4f7844ebe14bee95a53f49e'>Commit.</a> </li>
<li>[RFC] Reduce sidebar icon size/increase list information density. <a href='https://commits.kde.org/systemsettings/f8a9bdfdd7bf8576f0438f8a35bdfdf2ad22b3c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19694'>D19694</a></li>
<li>Remove redundant KIconLoader include (it was already included later). <a href='https://commits.kde.org/systemsettings/a5c0a519a44805410b54c206e806816e01646287'>Commit.</a> </li>
<li>Use new application style icon for the Application Style KCM category. <a href='https://commits.kde.org/systemsettings/fed2aa90d143c741a4416ad9623d43ce754587d8'>Commit.</a> </li>
<li>Port to std::sort. <a href='https://commits.kde.org/systemsettings/ac99f0628ed8137a475c130e86561d306d45ca37'>Commit.</a> </li>
<li>Use ActionTextField to implement the searchField in system settings. <a href='https://commits.kde.org/systemsettings/e80cbf128f7785ef7ee3fd875570872536ad1cff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19315'>D19315</a></li>
<li>Remove deprecated methods. <a href='https://commits.kde.org/systemsettings/6e422a78fed29daedf72cb81760411b426ef6929'>Commit.</a> </li>
<li>Use KCM name in KCM header. <a href='https://commits.kde.org/systemsettings/b6ad57fc5fa2163f2a9d315fd92e1aed5e609a0d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19006'>D19006</a></li>
<li>Change workspace theme category icon. <a href='https://commits.kde.org/systemsettings/4ef5a0b014212fc30ae319fbd818afb4c3a8e6f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18677'>D18677</a></li>
<li>Warnings--. <a href='https://commits.kde.org/systemsettings/6c40ed4c42abe3640382b6065ce3e197dffb6c74'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18489'>D18489</a></li>
<li>Default icons to 32x32 pixels. <a href='https://commits.kde.org/systemsettings/0f1f43c9897f531a7e9c6131946794b4c6131782'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403258'>#403258</a></li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/user-manager/9b6683c5fdf2dadf74d0f42a46d1e8dc635ffbb1'>Commit.</a> </li>
<li>Rename colorful user gallery avatar. <a href='https://commits.kde.org/user-manager/774c4c628d722586e7a0153afac16e21ee6c6d80'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20564'>D20564</a></li>
<li>Remove deprecated method. <a href='https://commits.kde.org/user-manager/90160837c281c2aeb58a39f793d830456005d457'>Commit.</a> </li>
<li>Use AuthCore instead of Auth. <a href='https://commits.kde.org/user-manager/1ea5e7ee503453e1931684757add1ca6f81e20e0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19070'>D19070</a></li>
<li>Use colorful icon in KCM. <a href='https://commits.kde.org/user-manager/a7141be3091f46a2cfac3b1a6cfc6c78b2d684ed'>Commit.</a> See bug <a href='https://bugs.kde.org/386748'>#386748</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18800'>D18800</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>AppChooser: put list of applications into a scrollarea. <a href='https://commits.kde.org/xdg-desktop-portal-kde/83773f6682154723d2a68e731c0e87c1f1a06893'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407559'>#407559</a></li>
<li>Bump Qt version to 5.12 as agreed at start of cycle. <a href='https://commits.kde.org/xdg-desktop-portal-kde/59c6ef2794b6b2974eb27d551704431cdda0dece'>Commit.</a> </li>
<li>Support default action, priority hints, set desktop-entry. <a href='https://commits.kde.org/xdg-desktop-portal-kde/532534a135e661827eced56ecb4b93c100012774'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20651'>D20651</a></li>
<li>Implement mouse support. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ecf84c4626c9d8f61f05b42a663c39cae9ec844f'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
