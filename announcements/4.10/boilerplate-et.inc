<!-- header goes into the specific page -->
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom.php">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions.php">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a name="packages"><em>Packages</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud <?php echo $release_full;?> 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
<a name="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?> infoleheküljel</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  Täieliku <?php echo $release_full;?> lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti <?php echo $release_full;?> kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?> infoleheküljelt.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldamaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.8.4. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>
