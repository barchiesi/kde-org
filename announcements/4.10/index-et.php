<?php

  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = "KDE väljalase 4.10";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php //showscreenshot("plasma-4.10.png", "KDE Software Compilation 4.10"); ?>
<p>
<img src="screenshots/plasma-4.10.png" style="float: right; border: 0; background-image: none; margin-top: -80px;" />
<!-- <img src="../../stuff/clipart/klogo-official-oxygen-128x128.png" style="float: right;" /> -->
<br /><br /><br />
<font size="+1">
KDE-l on rõõm teatada uusimatest Plasma töötsoonide, rakenduste ja arendusplatvormi väljalasetest. 4.10 väljalaskes on kodu- ja ärikasutajale mõeldud suurepärane vaba tarkvara täiustanud paljusid rakendusi ja pakub kasutajatele uusimat tehnoloogiat.<br />
</font>
</p>

<p>Teistes keeltes:
<br /><br />
<?php
  include "../announce-i18n-bar.inc";
?>
<br /><br /></p>

<p>
<h2><a href="plasma-et.php"><img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.10" width="64" height="64" /> Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja</a></h2>
Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.
</p>

<p>
<h2><a href="applications-et.php"><img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.10"/> KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile</a></h2>
KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.

</p>

<p>
<h2><a href="platform-et.php"><img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.10"/> KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile</a></h2>
Käesolev väljalase muudab veel lihtsamaks arendada KDE-d Plasma tarkvaraarenduse tööriistakomplektiga (SDK), kirjutada Plasma vidinaid ja vidinakogusid Qt märkekeeles (QML). Samuti võib märkida muudatusi teegis libKDEGames ja uusi skriptimisvõimalusi aknahalduris KWin.
</p>
<p><br />
KDE kvaliteedikontrolli meeskond korraldas väljalaske huvides <a href="http://www.sharpley.org.uk/blog/kde-testing">testimisprogrammi</a> aitamaks arendajatel tuvastada tegelikke vigu ja katsetamaks põhjalikult paljusid rakendusi. Nii võib öelda, et KDE puhul käivad uuenduslikkus ja kvaliteet käsikäes. Kui tuunete huvi kvaliteedikontrolli meeskonna tegemiste vastu, uurige <a href="http://community.kde.org/Get_Involved/Quality">meeskonna veebilehekülge</a>.<br />
</p>
<h2>Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline</h2>
<p>
KDE julgustab kõiki levitama sõna sotsiaalvõrgustikes. Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile silti "KDE", et kõik võiksid vajaliku materjali hõlpsamini üles leida ja KDE meeskond saaks koostada aruandeid KDE 4.10 väljalaske kajastamise kohta.
</p>
<p>
Kõike seda, mis toimub seoses väljalaskega sotsiaalvõrgustikes, võite jälgida KDE kogukonna otsevoos. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes sotsiaalvõrgustikes. Otsevoo leiab aadressilt <a href="http://buzz.kde.org">buzz.kde.org</a>.
</p>
<h2>Väljalaskepeod</h2>
<p>
Nagu ikka, korraldavad KDE kogukonna liikmed kõikjal maailmas väljalaskepidusid. Mõned on juba praegu ette teada, aga neid tuleb kindlasti rohkemgi. Nende kohta annab aimu meie <a href="http://community.kde.org/Promo/Events/Release_Parties/4.10">pidude nimekiri</a>. Kõik on oodatud osalema! Pidudelt leiab nii head seltskonda, vaimustavaid kõnesid kui ka veidi süüa-juua. See on suurepärane võimalus saada teada, mis toimub KDE-s, lüüa ise kaasa või ka lihtsalt kohtuda teiste kasutajate ja arendajatega.
</p>
<p>
Me innustame inimesi pidusid korraldama. Neil on lõbus viibida nii peoperemehe kui ka külalisena! Uuri lähemalt <a href="http://community.kde.org/Promo/Events/Release_Parties">näpunäiteid, kuidas pidu korraldada</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.10/&amp;title=KDE%20releases%20version%204.10%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.10/" data-text="#KDE releases version 4.10 of Plasma Workspaces, Applications and Platform â†’" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.10/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.10%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.10/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde410"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde410"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde410"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde410"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>Väljalasketeadetest</h2>
<p>
Käesolevad väljalasketeated koostasid Devaja Shah, Jos Poortvliet, Carl Symons, Sebastian Kügler ja teised KDE propageerimismeeskonna ning laiemagi kogukonna liikmed. Eesti keelde tõlkis need Marek Laane. Väljalasketeated tõstavad esile kõige tähtsamaid muudatusi suures arendustöös, mida KDE tarkvara on viimasel poolel aastal üle elanud.
</p>

<h4>KDE toetamine</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Astu mängu" align="left"/> </a>
<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer-et.inc");
?>