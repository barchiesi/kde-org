<?php

  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = "Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
Plasma töötsoone on märkimisväärselt parandatud ja täiustatud. Jätkuvalt uuendatakse vidinaid, viies neid üle <a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quicki</a> peale. See parandab töötsoonide ühtsust, ebakõlasid paigutuses, stabiilsust, kasutatavust ja jõudlust. Samuti on nüüd hõlpsam luua vidinaid, terveid uusi Plasma töötsooni paigutusi ja muid käepäraseid täiustusi. Uus <a href="http://doc.qt.digia.com/qt/qdeclarativeintroduction.html">QML</a>-põhine ekraani lukustaja muudab töötsoonid turvalisemaks. Ka taustapiltide mootor on üle viidud QML-i peale, mis muudab lihtsamaks kirjutada animeeritud taustapilte. (QML kuulub <a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quicki rakenduste raamistiku koosseisu</a>.)
</p>
<?php showscreenshot("plasma-empty.png", "KDE Plasma töötsoonid 4.10"); ?>
<p>
Lisaks Qt Quicki ja QML-iga seotud täiustustele sai tegumite vidin mõningaid parandusi, mis muutsid etemaks kasutatavuse, muu hulgas näevad nüüd <a href="http://aseigo.blogspot.com/2012/11/help-test-task-grouping-experiments.html">aknarühmad kenamad välja</a>. Samuti parandati märguannete süsteemi, eriti seoses toitehaldusega. Nüüd on toetatud kõrglahutusega ekraanid ning välja on pakkuda <a href="http://www.notmart.org/index.php/Graphics/Time_to_refresh_some_air">uus Airi teema</a>, mis vähendab visuaalset müra ja annab Plasma töötsoonidele klaarima välimuse.
</p>
<?php showscreenshot("plasma-tasks.png", "Rühmitamine tegumiribal näeb ilusam välja"); ?>

<h2>Aknahaldur KWin</h2>
<p>
KWini lõimimine uue kuuma kraami (GHNS) hoidlaga muudab lisaefektid ja -skriptid kättesaadavaks KWini seadistustedialoogis. Muidugi leiab neid endiselt ka veebileheküljel <a href="http://kde-look.org/index.php?xcontentmode=90">kde-look.org</a>, sealhulgas <a href="http://kde-look.org/index.php?xcontentmode=91">käitumist muutvad skriptid</a>. Ka kohandatud aknavahetajaid võib hankida nii KWini seadistustedialoogi vahendusel kui <a href="http://kde-look.org/index.php?xcontentmode=92">kde-look.org-i vastavast sektsioonist</a>. Üks lahe uus efekt animeerib akna maksimeerimise olekumuutust.
</p>
<?php showscreenshot("kwin-ghns.png", "KWini lisandeid on nüüd lihtne internetist paigaldada"); ?>
<p>
KWin oskab nüüd ära tunda mõningaid virtuaalmasinaid ning lülitab võimaluse korral sisse OpenGL komposiidi. Lisaks on omanduslikul AMD draiveril nüüd OpenGL 2 toetus.
</p>
<p>
Paanimise toetus on KWinist <a href="https://bugs.kde.org/show_bug.cgi?id=303090">eemaldatud</a>, sest see tekitas probleeme stabiilsusele, oli ilma mitme ekraani toeta ja läks konflikti teiste KWini komponentidega. Lühidalt, KWini arendajad leidsid, et vajalikku funktsionaalsust suudab paremini tagada JavaScripti API-ga plugin. Sel moel saab kasutaja kõige üle suurema kontrolli ning arendamine ja hooldamine muutub lihtsamaks. Plugin on saadaval järgmise väljalaske ajaks, kuid selle puhul on oodatud abi, sest ükski KWini arendaja ei tööta praegu paanimise toetamise kallal.
</p>
<?php showscreenshot("plasma-animated-wallpaper.png", "QML võimaldab luua Plasma animeeritud taustapilte"); ?>
<p>
Mitmed rakendused toetavad nüüd värvikorrektsiooni, olles võimelised kohanduma eri monitoride ja printerite värviprofiiliga. KDE deemoni moodul KolorServer toetab väljundipõhist värvikorrektsiooni, aknapõhine korrektsioon ootab mõnda järgmist väljalaset. Värvihalduse toetamine KWinis vabastab sellest ülesandest komposiidikomponendid. See lubab kasutajal soovi korral värvikorrektsiooni välja lülitada ja muudab koodi hooldamise lihtsamaks. Toetatud on ka mitme monitoriga süsteemid. Värvihalduse toetamisele andis suure hoo vastav <a href="http://skeletdev.wordpress.com/2012/08/20/gsoc-color-correction-in-kwin-final-report/">Google'i Summer of Code projekt</a>.
</p>
<p>
Uus KDE rakendusemenüü (<a href="http://gnumdk.blogspot.com/2012/11/appmenu-support-in-kde-410.html">appmenu</a>) lubab korraga kasutada mitmes rakenduses ühist menüüd. Lisatud on — vaikimisi peidetud — võimalus näidata üleservas menüüd, mis ilmub nähatavale alles siis, kui viia hiir ekraani ülaserva lähedusse. Menüüriba järgib akna fookust, nii et seda saab kasutada ka mitme ekraani korral. Samuti saab lasta menüüd näidata aknadekoratsiooni nupu alammenüüna. Menüüd võib lasta ekraanil näidata just seal, kus kasutaja seda soovib.
</p>
<div align="center" class="screenshot"><img src="screenshots/kwin-appmenu.gif" alt="Kate uued passiivsed märguanded ei sega enam nii palju töötegemist"/><br />Rakendusemenüü saab nüüd põimida akna tiitliribale</div>
<p>
KWini vigade parandamine on edenenud tänu <a href="http://blog.martin-graesslin.com/blog/2012/07/looking-for-kwin-bug-triagers/">suurele abile teatatud vigade kontrollimisel</a>. Aknahalduri KWini arendamisega saab jooksvalt tutvuda <a href="http://blog.martin-graesslin.com/blog/">Martin Gräßlini ajaveebis</a>.
</p>

<h2>Kiirem ja tõhusam metaandmete mootor</h2>
<p>
Blue Systemsi toetusel ette võetud tööga on kogu KDE-d hõlmava semantilise otsingu ja salvestamise taustaprogrammis parandatud üle 240 vea ning tehtud rohkelt muid parandusi. Nende seas on tähtsaim <a href="http://vhanda.in/blog/2012/11/nepomuk-without-strigi/">uus indekseerija</a>, mis muudab indekseerimise kiiremaks ja töökindlamaks. Üks kena omadus on see, et nüüd kõigepealt indekseeritakse kiiresti uute failide põhiteave (nimi ja MIME tüüp) ning täielik andmete hankimine lükatakse edasi ajale, mil süsteem on jõude (või sülearvuti on ühendatud toitejuhtmega), mistõttu see ei sega kasutajal tavapäraste asjadega tegeleda. Lisaks on nüüd palju hõlpsam kirjutada indekseerijaid uute failivormingute tarbeks. Üksikud vormingud, mida vana indekseerija toetas, ei ole veel uues kasutatavad, aga nende toetust võib peagi oodata. Samuti võimaldab uus indekseerija väga lihtsalt filtreerida faile tüübi järgi, mis kajastub ka kasutajaliideses: saab lasta indekseerida või mitte helifaile, pilte, dokumente, videoid ja lähtekoodi. Parandusi on saanud otsingu ja salvestamise kasutajaliides ning varundamine. Siltide <a href="http://en.wikipedia.org/wiki/KIO">KIO-moodul</a> laseb kasutajal sirvida faile siltide järgi kõigis KDE rakendustes.</p>
<p>
Nepomuki puhastaja on uus lihtne tööriist semantilise salvesti haldamiseks. Sellest on abi vananenud, vigaste või topeltandmete kõrvaldamisel. Puhastaja käivitamine pärast uuendamist võib tunduvalt parandada süsteemi kiirust. Täpsemat teavet selle ja teiste muudatuste kohta KDE otsingutehnoloogias leiab <a href="http://vhanda.in/blog/2013/01/what-new-with-nepomuk-4-10/">Vishesh Handa ajaveebis</a>.
</p>
<?php showscreenshot("dolphin-metadata.png", "Metaandmete käitlemine on paranenud"); ?>

<h2>Uus trükkimishaldur</h2>
<p>
Trükkimishalduri uus versioon parandab tunduvalt printerite seadistamist, hooldamist ja trükitööde käitlemist. Plasma aplett näitab saadaolevaid printereid ning võimaldab näha ja hallata järjekorras trükitöid. Süsteemi seadistuste mooduliga saab lisata ja eemaldada printereid ning saada ülevaate olemasolevatest printeritest, samuti printereid jagada ja määrata vaikimisi printer. Uus trükkimisnõustaja valib tuvastatud seadmetele automaatselt sobivad draiverid ja seadistused. Uued trükkimishalduri tööriistad ühilduvad täielikult trükkimise taustasüsteemi CUPS uusima versiooniga, mis tagab kiirema reageerimise ja usaldusväärsema aruandluse.
</p>
<!-- SCREENSHOT: print-manager.png -->
<h2>Failihaldur Dolphin</h2>
<p>
KDE failihalduris Dolphin on parandatud palju vigu ning see on saanud rohkelt täiustusi ja uuendusi. Failide vahetamine mobiilsete seadmetega on tunduvalt lihtsam tänu <a href="http://en.wikipedia.org/wiki/Media_Transfer_Protocol">MTP</a>-seadmete toetamisele, mida on näha otse asukohtade paneelil. Paneeli ikoonide suurust saab nüüd muuta. Kasutatavus on üldiselt paranenud, eriti võib märkida mitmeid uusi hõlbustusvalikuid. Dolphin võib nüüd anda aktiivse kataloogi ja failid teada tegevuste haldurile (seda saab määrata Süsteemi seadistustes). Ka jõudlus on mitmes mõttes paranenud. Kataloogide laadimine on tunduvalt kiirem nii eelvaatlusega kui ka ilma ja nõuab palju vähem mälu, sest kasutatakse ära kõiki saadaolevaid protsessorituumasid. Vähema tähtsusega parandused puudutavad otsingut, lohistamist ja veel mõningaid asju. Samuti on Dolphinile tulnud kasuks KDE semantilise salvestamise ja otsimise taustaprogrammi täiustamine, millega on kahanenud vajadus kasutada liigseid ressursse metaandmete tarbeks. Üksikasjalikumalt on kõigest sellest (inglise keeles) kirjutanud <a href="http://
freininghaus.wordpress.com/2012/11/27/on-the-way-to-dolphin-2-2/">Dolphini hooldaja Frank Reininghaus</a>.
</p>
<?php showscreenshot("kio-mtp.png", "Failide edastamine mobiilsetelt seadmetelt on hõlpsam"); ?>


<h4>Plasma paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>
<h2>Täna ilmusid veel:</h2>

<h2><a href="applications-et.php"><img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.10"/> KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile</a></h2>
<p>
KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.

</p>

<h2><a href="platform-et.php"><img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.10"/> KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile</a></h2>
<p>
Käesolev väljalase muudab veel lihtsamaks arendada KDE-d Plasma tarkvaraarenduse tööriistakomplektiga (SDK), kirjutada Plasma vidinaid ja vidinakogusid Qt märkekeeles (QML). Samuti võib märkida muudatusi teegis libKDEGames ja uusi skriptimisvõimalusi aknahalduris KWin.
</p>

<?php
  include($site_root . "/contact/about_kde-et.inc");
?>

<h4>Kontaktisikud</h4>

<?php
  include($site_root . "/contact/press_contacts-et.inc");
?>

<?php
  include("footer.inc");
?>