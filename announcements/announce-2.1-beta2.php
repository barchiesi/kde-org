<?php
  $page_title = "KDE 2.1-beta2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE JANUARY 31, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New Beta KDE Release for Linux Desktop Ships</H3>
<P><STRONG>New Beta Version of Leading Linux Desktop Offers New Theme
Manager, Image Viewer and IDE</STRONG></P>
<P>January 31, 2001 (The INTERNET).  The <A href="/">KDE
Team</A> today announced the release of KDE 2.1-beta2, a powerful, modular,
Internet-enabled desktop.  KDE 2.1 constitutes the second major release of
the KDE 2 series, which is the next generation of the
<A HREF="../awards">award-winning</A> KDE 1
series. KDE is the work product of hundreds of dedicated developers
originating from over 30 countries.
</P>
<P>
This is the last planned beta release before the scheduled release of
KDE 2.1 on February 19, 2001.  KDE 2.1 offers a number of additions,
enhancements and fixes over KDE 2.0.1, the last stable KDE release which
shipped on December 5, 2000.  The major additions and improvements are:
</P>
<UL>
<LI><A HREF="http://konqueror.kde.org/">Konqueror</A>, the modular,
standards-compliant file
manager and web browser, has improved significantly:</LI>
<UL>
<LI>It can now <A HREF="http://www.mieterra.com/konqueror/malte2.png">be
configured</A> to provide thumbnail previews for
<A HREF="http://www.mieterra.com/konqueror/preview.png">text and HTML
files</A>.</LI>
<LI><A HREF="http://developer.kde.org/kde2arch/khtml/index.html">KHTML</A>,
the HTML widget, now has a special 'transitional mode' which greatly improves
its handling of malformed HTML pages.</LI>
<LI>In additon, KHTML now has greatly
improved Java support.  Support for Java security (JDK 1.2 or
compatible is now required) as well as Java over SSL using the JSSE classes
have been added.</LI>
<LI>Drag'n'drop has been improved; now a URL can be dropped on a web page
and the Location label can be dragged.</LI>
<LI>"Favorite icon" support has been added, for displaying a website's icon
in the Location bar, in bookmarks and in the taskbar.</LI>
<LI>Devices can be displayed in the directory view and mounted on demand.</LI>
<LI>File-name completion has been improved and in-place file renaming
added.</LI>
<LI>Additional protocols supported include a LAN browser (lan:/ and rlan:/), a
floppy browser (floppy:/) and a CD browser (cd:/), which includes
CDDB support.</LI>
<LI>It now stores bookmarks
using the standard <A HREF="http://grail.sourceforge.net/info/xbel.html">XBEL
bookmark format</A>; a new bookmark editor complements the new standard.</LI>
<LI>Auto-proxy configuration and support for proxies requiring
authentication have been implemented.</LI>
</UL>
<LI><A HREF="http://www.kdevelop.org/">KDevelop</A>, a C/C++ integrated
development environment, has been added to the core KDE distribution.  The
version being shipped, 1.4beta2, is the first version of KDevelop to
make use of the KDE 2 libraries and integrate completely with the KDE 2
desktop.</LI>
<LI>A new and much-anticipated theme manager, as well as a LILO configuration
tool, have been added to KControl, the KDE control panel.  The control panel
now lists all available I/O slaves.</LI>
<LI>Many icons have been improved.  In addition, semi-transparency
(alpha-blending) has been implemented on small images and icons.</LI>
<LI>The panel (Kicker) has enjoyed significant improvements. 
An external taskbar has been included (familiar to
KDE 1 users), support for sub-panels has been added (which can be separately
sized and positioned), an improved external pager (Kasbar) has been added,
and support for applets has been improved (including support for
<A HREF="http://windowmaker.org/">WindowMaker</A> dock applets).</LI>
<LI><A HREF="http://www.arts-project.org/">ARts</A>, the KDE 2 multimedia
architecture, now offers a control module to configure sampling rate and
output devices, increased performance, improved user interfaces and a
number of additional effects and filters.</LI>
<LI><A HREF="http://www.mosfet.org/pixie/">Pixie</A>, an image viewer/editor, has been added to the Graphics package.</LI>
<LI>KAB, the KDE address book, now provides regular expression searching
of the address database and can export the database to HTML files.</LI>
<LI>For developers, a number of classes have been added to the core
libraries, including a class for undo/redo support (KCommand) and
a class for editing list boxes (KEditListBox).</LI>
<LI>Many additional improvements, particularly to KMail, the mail client,
and KNode, the news reader.  A more complete list of changes is
available
<A HREF="./changelogs/changelog2_0to2_1.html">here</A>.
</LI>
</UL>
<P>
KDE 2.1-beta2 includes the core KDE libraries, the core desktop environment,
as well as the over 100 applications from the other
standard base KDE packages: Administration, Games, Graphics, Multimedia,
Network, Personal Information Management (PIM), Toys and Utilities.  In
addition, this release includes the development packages KDevelop, Bindings,
an SDK and Documentation.
<A HREF="http://koffice.kde.org/">KOffice</A> is not included in this release.
</P>
<P>
All of KDE 2.1-beta2 is available for free under an Open Source license. 
Likewise,
<A HREF="http://www.trolltech.com/">Trolltech's</A> Qt 2.2.x, the GUI
toolkit on which KDE is based,
is also available for free under two Open Source licenses:  the
<A HREF="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</A> and the <A HREF="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</A>.
</P>
<P>
A more complete
<A HREF="./changelogs/changelog2_0to2_1.html">list of
major changes</A>, a <A HREF="../info/2.1.php">FAQ about
the release</A> and the
<A HREF="http://developer.kde.org/development-versions/kde-2.1-release-plan.html">KDE
2.1 release plan</A> are available at the KDE
<A href="../">website</A>.  More information about KDE 2
is available in a
<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</A> and on
<A href="../">KDE's web site</A>, including a number of
<A HREF="../screenshots/kde2shots.html">screenshots</A>,
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A> and a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.5">KDE 1 - KDE 2 porting guide</A>.
</P>
<P>
<H4>Downloading and Compiling KDE</H4>
</P>
<P>
The source packages for KDE 2.1-beta2 are available for free download at
<A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/tar/generic/src/">http://ftp.kde.org/unstable/distribution/2.1beta2/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>.  KDE 2.1-beta2 requires
qt-2.2.1, which is available from the above locations under the name
<A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.3.tar.gz">qt-2.2.3</A>
is recommended.  KDE 2.1-beta2 will not work with versions of Qt older
than 2.2.1.
</P>
<P>
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://developer.kde.org/build/index.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for KDE 2.1-beta2
will be available for free download under
<A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/">http://ftp.kde.org/unstable/distribution/2.1-beta2/rpm/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.1-beta2 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1, although
qt-2.2.3 is recommended.  KDE 2.1-beta2 will not work with versions of Qt
older than 2.2.1.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>

<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.1beta2/rpm/COL2.4/RPMS/">Caldera OpenLinux 2.4 (i386)</A></LI>
<LI><A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/tar/Tru64/">Tru64 Systems</A></LI>
<LI>Linux-Mandrake 7.2:  <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Mandrake/7.2/i586/">i586</A></LI>
<LI>RedHat Linux:
<UL>
<LI>7.0:  <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/7.0/i386/">i386</A>, <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/7.0/alpha/">Alpha</A>, <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/7.0/ia64/">IA64</A>, <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/7.0/sparc/">Sparc</A> and <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/7.0/s390/">S390</A></LI>
<LI>6.x:  <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/Redhat/6.x/sparc/">Sparc</A></LI>
</UL>
<LI>SuSE Linux 7.0:  <A HREF="http://ftp.kde.org/unstable/distribution/2.1beta2/rpm/SuSE/7.0-i386/">i386</A></LI>
<LI>Debian GNU/Linux: <A HREF="http://kde.debian.net/">2.2 (potato) i386/powerpc</a></LI>
</LI>
<!--
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/ppc-glibc21/">LinuxPPC (glibc 2.1)</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/potato/">Debian GNU/Linux 2.2 (potato)</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/Debian/dists/woody/">Debian GNU/Linux Devel (woody)</A></LI>
-->
</UL>
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="../whatiskde/">web site</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
Java is a trademark of Sun Microsystems, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;&#x72;an&#00114;o&#116;&#104;&#x40;kd&#101;&#046;o&#114;g<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
po&#x75;r&#x40;kd&#101;&#x2e;&#x6f;r&#x67;<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#00102;&#0097;ure&#064;&#107;&#100;e.o&#00114;&#103;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
k&#111;no&#x6c;d&#x40;kd&#00101;&#46;&#111;&#114;&#0103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>

<?php include "footer.inc" ?>
