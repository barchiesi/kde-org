<?php
  $page_title = "KDE 4.1 Beta 2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<?php
  $release = '4.1-beta2';
  include "announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Project Ships Second Beta of KDE 4.1
</h3>

<p align="justify">
  <strong>
KDE Community Announces Second Beta Release of KDE 4.1</strong>
</p>

<p align="justify">
June 24, 2008 (The INTERNET).
The <a href="http://www.kde.org/">KDE Community</a> is proud to announce the
second beta release of KDE 4.1. Beta 2 is aimed at testers, community
 members and enthusiasts in order to identify bugs and regressions, so that
4.1 can fully replace KDE 3 for end users.  KDE 4.1 beta 2 is available as
binary packages for a wide range of platforms, and as source packages.  KDE
4.1 is due for final release in late July 2008.
</p>


<h4>
  <a name="changes">KDE 4.1 Beta 2 Highlights</a>
</h4>

<p align="justify">
After one month has passed since the feature freeze on the KDE 4.1 branch,
the KDE hackers have been working on polishing the new features,
desktop integration, and documenting and translating the packages. Several
bugfixing sessions have been held and squashed bugs in the beta
software. While there are still bugs left that need to be fixed until the
release, KDE 4.1 Beta2 shapes up nicely. Testing and feedback on this
release is appreciated and needed to make KDE 4.1 a splash.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/desktop-folderview.png">
        <img src="announce_4.1-beta2/desktop-folderview_thumb.png" align="center"  height="375"  />
    </a><br /><em>KDE 4.1 Beta 2 with Plasma desktop</em>
</div>

<ul>
    <li>Language bindings for KDE 4.1 in several languages, such as Python,
    Ruby
    </li>
    <li>Usability improvements and support in Dolphin
    </li>
    <li>Improvements all over the place in Gwenview
    </li>
</ul>

A more complete list of new features in KDE 4.1 is up on
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>.
</p>

<h4>
  Language Bindings
</h4>
<p align="justify">
While most KDE 4.1 applications are written in C++, language bindings expose the
functionality of the KDE libraries to application developers that prefer
a different language. KDE 4.1 comes with support for several other languages,
such as Python and Ruby. The printer applet that has been added in KDE 4.1
is written in Python, fully transparent to the user.
</p>

<h4>
  Dolphin matures
</h4>
<p align="justify">
Dolphin, KDE4's filemanager has seen several improvements.

</p>


<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-tagging.png">
        <img src="announce_4.1-beta2/dolphin-tagging_thumb.png" align="center"  height="293"  />
    </a><br /><em>The Semantic Desktop with its tagging system</em>
</div>
The first bits and pieces of the <a href="http://nepomuk.kde.org/">NEPOMUK</a> Social
<p align="justify">
Semantic Desktop are becoming visible and more widely applicable. Support for tagging
in Dolphin shows first features coming from this technology, which is developed in
a European Research programme.

</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-selection.png">
        <img src="announce_4.1-beta2/dolphin-selection_thumb.png" align="center"  height="257"  />
    </a><br /><em>Seleting files in single click mode</em>
</div>
Selecting files is made easier by
<p align="justify">
a small + button in the top left corner that selects the file, rather than
opens it. This change makes using the filemanager in single-click mode much
easier and prevent accidentally opening files, while being intuitive and easy
to use.<br />

</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-treeview.png">
        <img src="announce_4.1-beta2/dolphin-treeview_thumb.png" align="center"  height="315"  />
    </a><br /><em>New treeview in Dolphin</em>
</div>
Many users have asked for a treeview mode in the detailed list mode. The
<p align="justify">
feature has been edited and makes combined with the single click improvments
for quick moving and copying of files.
</p>

<h4>
  Gwenview polished
</h4>
<p align="justify">
Gwenview's KDE4's default image viewer has been polished further as well. Options
such as the rotate and fullscreen view functions have been put into the direct
context of the image, making the application's user interface less cluttered,
minimizing mouse movements and thus making the application more intuitive and easy
to use. A

</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/gwenview-browse.png">
        <img src="announce_4.1-beta2/gwenview-browse_thumb.png" align="center"  height="276"  />
    </a><br /><em>Gwenview's overview of a directory</em>
</div>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/gwenview.png">
        <img src="announce_4.1-beta2/gwenview_thumb.png" align="center"  height="276"  />
    </a><br /><em>Gwenview's thumbnail bar</em>
</div>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/systemsettings-emoticons.png">
        <img src="announce_4.1-beta2/systemsettings-emoticons_thumb.png" align="center"  height="276"  />
    </a><br /><em>The new emoticons System Settings module</em>
</div>

<h4>
  KDE 4.1 Final Release
</h4>
<p align="justify">
KDE 4.1 is scheduled for final release on July 29, 2008.  This time based release falls six months after the release of KDE 4.0.
</p>

<h4>
  Get it, run it, test it
</h4>
<p align="justify">
  Community volunteers and Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.83 (Beta 2) for most Linux distributions, and Mac OS X and Windows.  Be aware that these packages are not considered ready for production use.  Check your operating system's software management system.
</p>
<h4>
  Compiling KDE 4.1 Beta 2 (4.0.83)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 4.0.83 may be <a
  href="http://www.kde.org/info/4.0.83.php">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.83
  are available from the <a href="/info/4.0.83.php">KDE 4.0.83 Info
  Page</a>, or on <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set.
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
