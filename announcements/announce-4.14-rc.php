<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Release Candidate of Applications and Platform 4.14");
  $site_root = "../";
  $release = "4.13.97";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php i18n("July 31, 2014. Today KDE released the Release Candidate of the new versions of Applications and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.");?>
</p>

<p align="justify">
<?php i18n("With the large number of changes, the 4.14 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.14 team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.");?>
</p>


<h3><?php i18n("KDE Software Compilation 4.14 Release Candidate");?></h3>
<p align="justify">
<?php i18n("The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.13.97/'>http://download.kde.org</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.");?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing 4.14 Release Candidate Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of 4.14 Release Candidate (internally 4.13.97) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.14_RC_.284.13.97.29'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling 4.14 Release Candidate");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for 4.14 Release Candidate may be <a href='http://download.kde.org/unstable/4.13.97/src/'>freely downloaded</a>. Instructions on compiling and installing 4.13.97 are available from the <a href='/info/4.13.97.php'>4.13.97 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
