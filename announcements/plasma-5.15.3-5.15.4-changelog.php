<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.15.4 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.15.4";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Compute correct position of drag button press on X11. <a href='https://commits.kde.org/breeze/1915c484dc9e0049c33e8bdb01461a103280e810'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20182'>D20182</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Ignore source packages when adding them to the PK list. <a href='https://commits.kde.org/discover/5da27c6c8b4571d935691c87d7017837c9badf5a'>Commit.</a> </li>
<li>Flatpak: disable dis/enabling sources. <a href='https://commits.kde.org/discover/07950ed287ac191bf33a93dd7bf0e24aeaad4cf4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404370'>#404370</a></li>
<li>Fwupd: No need to make the code hard to read for no reason. <a href='https://commits.kde.org/discover/6493803411b64f21ffbfbd81fc6cd184bbd15e69'>Commit.</a> </li>
<li>Sources: Fix QML error when un/selecting a source. <a href='https://commits.kde.org/discover/e1492f67e0220aa5ccb78537925b51a863bc7fad'>Commit.</a> See bug <a href='https://bugs.kde.org/404370'>#404370</a></li>
<li>Kns: wallpaperplugin.knsrc comes from plasma. <a href='https://commits.kde.org/discover/073c50bbd15fdbf37ccaafdd4c51e1c938ddbbba'>Commit.</a> </li>
<li>Fix warning, don't catch object we don't need. <a href='https://commits.kde.org/discover/8857d4fc32c7f9152fbd0eac71e9f339b997f839'>Commit.</a> </li>
<li>Don't accept invalid KNS EntryInternal results. <a href='https://commits.kde.org/discover/400b0d7e7596662a2efaa2a88f454f0accde8308'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18760'>D18760</a></li>
<li>Fix --categories with delayed categories. <a href='https://commits.kde.org/discover/48ff95b8a26890f5bb070b9086b67200554b47f9'>Commit.</a> </li>
<li>Restore proper background color on list and sources pages. <a href='https://commits.kde.org/discover/094c9d947e17768397aca8572865799f0650f594'>Commit.</a> See bug <a href='https://bugs.kde.org/404827'>#404827</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19712'>D19712</a></li>
<li>Don't allow refreshing during updates. <a href='https://commits.kde.org/discover/84f70236f4d98ee53c83ec7fa30a396754e332ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403333'>#403333</a></li>
<li>Use Attica forwarding headers. <a href='https://commits.kde.org/discover/712a7dfcab388b93dba7477eb55dc95d48bcafca'>Commit.</a> </li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Harmonize use of #if and #ifdef, using #if everywhere. <a href='https://commits.kde.org/drkonqi/d4ae04a78d0e22f1f142118509bcd2297ba8eb24'>Commit.</a> </li>
<li>The crash dialog no longer cuts off text after backtracing. <a href='https://commits.kde.org/drkonqi/cc640cea3e9cc1806000804bbf424cb611622e45'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/337319'>#337319</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19390'>D19390</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Media Frame] Set sourceSize for higher quality and lower memory consumption. <a href='https://commits.kde.org/kdeplasma-addons/4665d0b41aac346a334cb4770ab88ec935d559e7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19550'>D19550</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Clean up generated config-*.h files. <a href='https://commits.kde.org/kinfocenter/aaae700b2dc05df6e8e4780d47329f3923ae8c87'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Force server-side decoration if no borders are forced by user. <a href='https://commits.kde.org/kwin/dc552ee2ae898d69f4592922b0c39b959f47b20c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405385'>#405385</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19705'>D19705</a></li>
<li>[platforms/x11] Force glXSwapBuffers to block with NVIDIA driver. <a href='https://commits.kde.org/kwin/3ce5af5c21fd80e3da231b50c39c3ae357e9f15c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19867'>D19867</a></li>
<li>Properly restore current desktop from session. <a href='https://commits.kde.org/kwin/f7af113261f7e899e1d954b0558b2c637a9cced1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390295'>#390295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19520'>D19520</a></li>
<li>[platforms/fbdev] Use a better way to correct activating framebuffer devices. <a href='https://commits.kde.org/kwin/93b7eea67db418751e7fe4a86bc19430c153588b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19663'>D19663</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Update description before processing job state or error. <a href='https://commits.kde.org/plasma-browser-integration/88a1a9fb18fd9177a7c727b9f1c2ef6f74adf844'>Commit.</a> </li>
<li>[Host] Add Message.sh. <a href='https://commits.kde.org/plasma-browser-integration/832a18822ccaa3a12a636d12d53170b0270830c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19863'>D19863</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Harmonize use of HAVE_X11, using cmakedefine01. <a href='https://commits.kde.org/plasma-desktop/bcd8015eaf3638fca0e369955aa1a4290d079ef8'>Commit.</a> </li>
<li>Use camelcase KPeople headers, they do work. <a href='https://commits.kde.org/plasma-desktop/e9862c608ce64123479ec04062c9517b08be21b9'>Commit.</a> </li>
<li>[KickerDash] Add missed argument in addFavorite. <a href='https://commits.kde.org/plasma-desktop/af28e245070ef041db3dd02c34fe88686861442a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19893'>D19893</a></li>
<li>[Kicker] Fix "Tooltip can not be displayed". <a href='https://commits.kde.org/plasma-desktop/e7205d039d09bf31d81d129ee743d6b0f054c268'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390804'>#390804</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19096'>D19096</a></li>
<li>[Desktop Theme KCM] Emit current index change when resetting the model. <a href='https://commits.kde.org/plasma-desktop/fa99dfb2fd7ad6e710b383a065996efd11289835'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19763'>D19763</a></li>
<li>[Pager] Shrink label font size dynamically. <a href='https://commits.kde.org/plasma-desktop/de1c1c93c88334fef78d0d568fce1ad6f118dc79'>Commit.</a> See bug <a href='https://bugs.kde.org/405454'>#405454</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19751'>D19751</a></li>
<li>[Splash KCM] Sort "None" first. <a href='https://commits.kde.org/plasma-desktop/ac353717836159bbd04f89bdaa8f590d306fea31'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19701'>D19701</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Do not remove manually configured "route-metric" property from IPv4 and IPv6 setting. <a href='https://commits.kde.org/plasma-nm/269e5b29989b757ee87b074e8b52c8dd27b3b168'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406118'>#406118</a></li>
<li>Applet: fix size of the busy indicator. <a href='https://commits.kde.org/plasma-nm/371933b2680d9ca4b3eb63600bb9ed4963ecc9a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405962'>#405962</a></li>
<li>OpenVPN: reneg-sec property doesn't need limitation. <a href='https://commits.kde.org/plasma-nm/de649a3b3a219adb37b00f24329d0734c7e7a235'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404614'>#404614</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix prior commit that broke the build. <a href='https://commits.kde.org/plasma-workspace/b115a061e0c923a7e5b558a00c06d78106430d0d'>Commit.</a> </li>
<li>Change default Klipper behavior to accept images. <a href='https://commits.kde.org/plasma-workspace/6a178a5b4e65fc0b8cc8ade20e923c25f027c6c1'>Commit.</a> See bug <a href='https://bugs.kde.org/393708'>#393708</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19852'>D19852</a></li>
<li>The GTK+ settings module now correctly appears in krunner and kickoff. <a href='https://commits.kde.org/plasma-workspace/1f3ab6f7dddba56679645ed12ebc07ff1f187e84'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383287'>#383287</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19788'>D19788</a></li>
<li>Reduced likelihood of KSplash not being dismissed properly and only timing out after 30s. <a href='https://commits.kde.org/plasma-workspace/9a4bb220c162f031b4d45ed04f9e156b6d0b852e'>Commit.</a> See bug <a href='https://bugs.kde.org/405444'>#405444</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19753'>D19753</a></li>
<li>[notifications] Prevent subpixel font placement. <a href='https://commits.kde.org/plasma-workspace/fb868892fc9baa8c1c3b9409fe1a46195d8bbb8a'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Fixed issue causing changed signal to be called with false argument. <a href='https://commits.kde.org/sddm-kcm/0c76098e80c8ee61c7da9d85313461fc8a040651'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403366'>#403366</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19959'>D19959</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Fix monochrome icons in tooltips. <a href='https://commits.kde.org/systemsettings/8996c9bb8b248c23b4b96c633e5baefedffb6e15'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19834'>D19834</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
