<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.14.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.14.5";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Set the default cursor theme to breeze_cursors. <a href='https://commits.kde.org/breeze-gtk/dcacaa1d4f7e2a591de64889ae108e9c07054b1c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17187'>D17187</a></li>
<li>GTK theme treeview style typo/bug fix. <a href='https://commits.kde.org/breeze-gtk/765e5b21fb501ba11d9b909931eb9c0c86752eaf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16331'>D16331</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Don't enable the install button in the app page until we know the state. <a href='https://commits.kde.org/discover/6647d5d0084a0d19a1dc84695071bc0db982691a'>Commit.</a> </li>
<li>Fwupd: simplify logic that initialises files to download. <a href='https://commits.kde.org/discover/8b378496ca43330b239f1d13d8b36c22014bfd95'>Commit.</a> </li>
<li>Fwupd: fix leak on faulty devices. <a href='https://commits.kde.org/discover/cbb973b75d9179b0cf9c6b72657bdaa44a8ebf7d'>Commit.</a> </li>
<li>Don't inspect sources if the backend isn't confident about offering good results. <a href='https://commits.kde.org/discover/42ed98ba2cc132090a656ef5bcc18444615bf946'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401844'>#401844</a></li>
<li>Ux: Make sure we can reach the view from the appdelegate. <a href='https://commits.kde.org/discover/4c5db5cdd60496f2bce2a2b6bc72774a041a1816'>Commit.</a> </li>
<li>Pk: Don't the dependencies button if there aren't dependencies. <a href='https://commits.kde.org/discover/1fb9be36a38668c73d33186229b13adf78083d40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400781'>#400781</a></li>
<li>Snap: show more useful information for each channel. <a href='https://commits.kde.org/discover/1c40f2f6d542abcf28aaa603a3f0f24035e93306'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401907'>#401907</a></li>
<li>Don't show a hover event if there's nothing to open. <a href='https://commits.kde.org/discover/de81341e8afaed8dee3a3926b34ed5d4b9fba877'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401833'>#401833</a></li>
<li>Don't automatically refresh if the user already asked apt to do so. <a href='https://commits.kde.org/discover/30de60f05d183e185724e614aad67b06aec987f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401810'>#401810</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17375'>D17375</a></li>
<li>Source enabled checkbox value shouldn't change only because it was clicked. <a href='https://commits.kde.org/discover/21b72c1415a250bb124e518c394b1fa29ced057e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401663'>#401663</a></li>
<li>Fwupd: Don't get releases for unsupported devices. <a href='https://commits.kde.org/discover/528d82e147706c7ca90c9373b2cbd2f51b079bf4'>Commit.</a> </li>
<li>Fwupd: be more verbose when returning a faulty checksum because a file can't be read. <a href='https://commits.kde.org/discover/54509a34a7717de6db4e0b9303d885a4cd2ee0d2'>Commit.</a> </li>
<li>Fwupd: remove unused declaration. <a href='https://commits.kde.org/discover/44f5066ed1d21f5c45fbcdb01b99566eae4bcae6'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[comic dataengine] Do not cache identifier only requests. <a href='https://commits.kde.org/kdeplasma-addons/37d5fc1933c7fa56545023f9b593fbc9744673a8'>Commit.</a> </li>
<li>[comic] Update comic strip on current identifier only. <a href='https://commits.kde.org/kdeplasma-addons/1e12a9df8a7143a7c226999ad4b7b3badb0389a2'>Commit.</a> </li>
<li>[comic] Fix max cache limit. <a href='https://commits.kde.org/kdeplasma-addons/e909e66b30086451b187ec3b70e7c1a5c374ee23'>Commit.</a> </li>
<li>[comic] Request current identifier on data change. <a href='https://commits.kde.org/kdeplasma-addons/fd19c36ec4e48e05780b3e94b55f91936cd6bcb5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16873'>D16873</a></li>
<li>[comicupdater] Fix last day check. <a href='https://commits.kde.org/kdeplasma-addons/28493251921b30f89e140b50d6c7cd9eca6540cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16856'>D16856</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Increase default window size so nothing gets cut off by default. <a href='https://commits.kde.org/kinfocenter/9f546137ec6b8b92434136a7f74773e207196f98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364767'>#364767</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Redraw the slider if modes have changed. <a href='https://commits.kde.org/kscreen/32e95fad0d512d1ca57332640881fc4c21b744be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17686'>D17686</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Force software rendering when greeter crashed. <a href='https://commits.kde.org/kscreenlocker/e1d676c46e5060f06176cdc050b9edc23306812b'>Commit.</a> </li>
<li>Fix build with libc++. <a href='https://commits.kde.org/kscreenlocker/0a5233341ea7c50f38198c5010a9f4e694a223d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16611'>D16611</a></li>
<li>Prevent paste in screen locker. <a href='https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388049'>#388049</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14924'>D14924</a></li>
<li>Load QtQuickSettings for software rendering. <a href='https://commits.kde.org/kscreenlocker/c25251a7eb051c7e6914e188f39773d654cb7358'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14708'>D14708</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[platforms/x11/standalone] Pass kxkbconfig to Xkb prior to reconfigure. <a href='https://commits.kde.org/kwin/0f489c3521eb5f19459b4210911548237e0655b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402764'>#402764</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17967'>D17967</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Notify also if modes have changed. <a href='https://commits.kde.org/libkscreen/5422a4a8b120139b41febe3bb6a4ef01806ca509'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17685'>D17685</a></li>
<li>Revert "Fix uninstalled run unit tests: set LIBRARY_OUTPUT_DIRECTORY for plugins". <a href='https://commits.kde.org/libkscreen/2ae23e70e988d617a576c7db48301b2d950b03c8'>Commit.</a> </li>
<li>Fix uninstalled run unit tests: set LIBRARY_OUTPUT_DIRECTORY for plugins. <a href='https://commits.kde.org/libkscreen/4c0b960bce2ce32020d17547950c051898b464b3'>Commit.</a> </li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Fixed cancelling download in Firefox showing an "unknown error" notification. <a href='https://commits.kde.org/plasma-browser-integration/90400518efcd2861ce1b7008f63a5710ad69e58a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385530'>#385530</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17233'>D17233</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Activities KCM] Disable delete button when there's only one activity. <a href='https://commits.kde.org/plasma-desktop/eafb35076fa3e88951712a7f5bed2a33ccb7b4e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397887'>#397887</a></li>
<li>[Activities KCM] vertically center the buttons. <a href='https://commits.kde.org/plasma-desktop/9cdfe25910cbbfbef09364ce3adba53bcc292853'>Commit.</a> </li>
<li>Fix removing first item in languages list. <a href='https://commits.kde.org/plasma-desktop/7fca6ede19af7ae86ccc69b6a741d6188a22732e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401473'>#401473</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17218'>D17218</a></li>
<li>Make accessibility warning dialog usable again and fix event handling. <a href='https://commits.kde.org/plasma-desktop/36e724f1f458d9ee17bb7a66f620ab7378e5e05e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17536'>D17536</a></li>
<li>Make accessibility warning dialog usable again. <a href='https://commits.kde.org/plasma-desktop/611fc3079b243eb0e76230a07c8515523e69c797'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17557'>D17557</a></li>
<li>Make sure m_labelMode is initialised. <a href='https://commits.kde.org/plasma-desktop/a1f1f2ef0562e5870372c3a89bdd3764bad37662'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16711'>D16711</a></li>
<li>Round label width. <a href='https://commits.kde.org/plasma-desktop/3526334e82342dad221410cf3e8f839ed671bd7e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17365'>D17365</a></li>
<li>[Folder View] implement a minimum width for icon view to ensure that labels are never rendered useless. <a href='https://commits.kde.org/plasma-desktop/0b654afd88844c5a0ab8eb9a4f639ab52eac6d57'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379432'>#379432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16901'>D16901</a></li>
<li>Fix group popup dialog. <a href='https://commits.kde.org/plasma-desktop/ed34cc5f181e61d1fc98872866c5d7300a90af86'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401508'>#401508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17219'>D17219</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix a wrong check in previous patch. <a href='https://commits.kde.org/plasma-nm/cf11d51f2b7ac23f84e0a982f93d053cf79f6f5f'>Commit.</a> </li>
<li>Openconnect: do not reload dialog when group changes for Juniper protocol. <a href='https://commits.kde.org/plasma-nm/b2f6422c89c77d80852be1929ff6ca610328b3c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395157'>#395157</a></li>
<li>Revert "Openconnect: do not reload the auth dialog endlessly when group changes". <a href='https://commits.kde.org/plasma-nm/fe869b2ea306a794a238bff9e8f1953e25acc2a2'>Commit.</a> </li>
<li>Notify that kcm changed only when validity has changed. <a href='https://commits.kde.org/plasma-nm/d3b466a2da7706bfe0ba4b6ec80890ecded667b4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379756'>#379756</a></li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Fix use of QRegularExpressionMatch. <a href='https://commits.kde.org/plasma-vault/5d3face3c1bbe5a8a40927048b6f1c3280b26399'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17359'>D17359</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[weather dataengine] envcan: fix typo in forecast string "Mainly sunny". <a href='https://commits.kde.org/plasma-workspace/2097b614904c78ad1622bf3e77380a2d4db56d00'>Commit.</a> </li>
<li>[weather dataengine] envcan: support also "Partly cloudy" forecast. <a href='https://commits.kde.org/plasma-workspace/0ab3d161c69e64642add3eb4a7faca70f46fc249'>Commit.</a> </li>
<li>[weather dataengine] bbc: adapt to changed strings for same day in forecast. <a href='https://commits.kde.org/plasma-workspace/f0134f6441d1d8b356c62b6e537241c1ae983090'>Commit.</a> </li>
<li>[weather dataengine] noaa: add more forecast strings found in use. <a href='https://commits.kde.org/plasma-workspace/419097542fef18b285927202e3a1fe2fd004d2b9'>Commit.</a> </li>
<li>[weather dataengine] Remove duplicated condition/forecast strings. <a href='https://commits.kde.org/plasma-workspace/ad0f6834a29afd34d1432788a771f72d6d40cf90'>Commit.</a> </li>
<li>[weather dataengine] envcan: support also "Mainly cloudy" forecast. <a href='https://commits.kde.org/plasma-workspace/3a38a571061d0854914547d648cdcfc36459318d'>Commit.</a> </li>
<li>[weather dataengine] noaa: use https over http. <a href='https://commits.kde.org/plasma-workspace/a9c537dbda0c6fb4db11422f2ab00397a0073b09'>Commit.</a> </li>
<li>[weather dataengine] noaa: fix unknown icon for Flurries. <a href='https://commits.kde.org/plasma-workspace/075629b96e4fc8c613c19db7cd468560ce4a9ff4'>Commit.</a> </li>
<li>[kuiserver] Debug--. <a href='https://commits.kde.org/plasma-workspace/f02759d028a56919b86128816c63bca0630ef792'>Commit.</a> </li>
<li>[kuiserver] Avoid double warning on terminated jobs. <a href='https://commits.kde.org/plasma-workspace/c4dcba898a9711a165ab5d839730e44560a48055'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17711'>D17711</a></li>
<li>Fix crash when deactivating Klipper's URL grabber. <a href='https://commits.kde.org/plasma-workspace/fb918b6440cef1949266437218b07208b9caa038'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363771'>#363771</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17531'>D17531</a></li>
<li>Set error if a kjob host disappears from kuiserver with active jobs. <a href='https://commits.kde.org/plasma-workspace/2beb1a0ad23177f7dc2e5ee622bed3a70f671278'>Commit.</a> See bug <a href='https://bugs.kde.org/352761'>#352761</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17171'>D17171</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>[dimdisplay] Do not change screen brightness on invalid values. <a href='https://commits.kde.org/powerdevil/6f350690723a11e2fc592924e60019361369b04a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16918'>D16918</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Set parent window for dialogs. <a href='https://commits.kde.org/xdg-desktop-portal-kde/6f7b16e5bd595f155925d11438ffdf57f9d400ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17659'>D17659</a></li>
<li>Add workaround for gtk file chooser filter patterns. <a href='https://commits.kde.org/xdg-desktop-portal-kde/66eb2fe654b95eb721ed3b2b1cb500f55920d7e8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399889'>#399889</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17677'>D17677</a></li>
<li>FileChooser: make use of current_name property in Save dialog. <a href='https://commits.kde.org/xdg-desktop-portal-kde/fa5161c17e6acbe72efe833eb314a5c15183317b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402077'>#402077</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
