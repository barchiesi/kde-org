<?php
  $page_title = "KDE Software Compilation 4.5 Beta2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Software Compilation 4.5 Beta2 Released: Codename Cake
</h3>

<p align="justify">
  <strong>
KDE Community Ships Second Beta Release of the 4.5 Free Desktop, Applications and
Development Platform
</strong>
</p>

<p align="justify">
June 9th, 2010. Today, KDE has released the second beta version of what is to become KDE SC 4.5.0 in August 2010. KDE SC 4.5 Beta2 is targeted at testers and those that would like to have an early look at what's coming to their desktops and netbooks this summer. KDE is now firmly in beta mode, meaning that the primary focus is on fixing bugs and preparing the stable release of the software compilation this summer. Over the last two weeks, roughly since the first beta, 1459 new bugs have been reported, and 1643 bugs have been closed, so we're witnessing a lot of stabilization activity right now. More testing is in place, however, while the restless developers continue to create a rock-stable 4.5.0.

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.5-beta2.png">
<img src="announce-4.5-beta2_thumb.png" align="center" width="600" height="400" alt="The KDE Plasma Desktop in 4.5 Beta2" title="The KDE Plasma Desktop in 4.5 Beta2" />
</a>
<br />
<em>The KDE Plasma Desktop in 4.5 Beta2</em>
</div>

</p>
<p>
KDE SC 4.5 sports many improvements, among which are:
<ul>
    <li>
    A <strong>reworked notification</strong> area. Thanks to the new, D-Bus-based protocol that replaces the old "system tray", a <em>uniform look and consistent interaction</em> scheme can now be guaranteed across applications and toolkits.
    </li>
    <li>
    <strong>KWin-Tiling</strong> makes it possible to automatically place windows next to each other, employing the <em>window management paradigm</em> also found in window managers such as Ion. Advanced graphical effects, such as blurring the background of translucent windows make for a more pleasurable and usable experience.
    </li>
    <li>
    Users that prefer <strong>WebKit</strong> above the KHTML rendering engine currently used in Konqueror, KDE's web browser now can install the WebKit component and switch Konqueror to use WebKit as <em>rendering engine for web sites</em>.  The WebKit component for Konqueror is available from KDE's Extragear repository, is based on the popular KPart component technology and fully integrates with password storage, content-blocking and other features users already know and love in Konqueror.
    </li>
    <li>
    A special focus of this release cycle is the <strong>stability</strong> of the software delivered with KDE SC 4.5. While there are many exciting new features, developers have spent considerable amounts of time finishing off features and polishing those that haven't come to full bloom yet.
    </li>
</ul>
</p>
<p>
Initially planned for KDE SC 4.5.0, the KDE PIM team have decided to delay the release of the Akonadi-based KMail for one month. The new version of KMail will be delivered as part of one of KDE's monthly bugfix updates. In the meantime, the stable version of KMail from KDE SC 4.4 will be maintained. Akonadi will centralize syncing and caching of PIM data, deliver wider support for groupware servers and makes handling PIM data, such as contacts, calendaring and email more efficient by sharing it across applications.

<!--
<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE SC 4.5 Beta2, you can browse the Subversion log.
KDE SC 4.5 Beta2 also ships a more complete set of translations for many of the 50+ supported languages.
-->
</p>
<p>
To find out more about the KDE Plasma desktop and applications, please also refer to the
<a href="http://www.kde.org/announcements/4.4/">4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">4.0.0</a> release
notes.
</p>



<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/unstable/4.4.85/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing KDE SC 4.5 Beta2 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.5 Beta2
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.85/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.4.85.php">KDE SC 4.5 Beta2 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.5 Beta2
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for KDE SC 4.5 Beta2 may be <a
href="http://download.kde.org/stable/4.4.85/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.5 Beta2
  are available from the <a href="/info/4.4.85.php#binary">KDE SC 4.5 Beta2 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
