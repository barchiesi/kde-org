<?php
  $page_title = "KDE 1.0 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<CENTER><!-- ---------------- Main body ---------------------------- --></CENTER>
<BR>
<CENTER><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
<TR VALIGN=TOP>
<TD>

<pre>





           T H E   K   D E S K T O P   E N V I R O N M E N T

                          http://www.kde.org

                              Release 1.0

                             July 12, 1998


    An integrated Desktop Environment for the Unix Operating System


  We are pleased to announce the availability of release 1.0 of the K
  Desktop Environment.

  KDE is a network transparent, contemporary desktop environment  for
  UNIX  workstations.  KDE  seeks to fill the need for an easy to use
  desktop for Unix workstations, similar to the desktop  environments
  found  under  the  MacOS  or  Window95/NT. We believe that the UNIX
  operating system is the best operating system available  today.  In
  fact  UNIX  has been the undisputed choice of the information tech-
  nology professional for many years.  When it  comes  to  stability,
  scalability  and openness there is no competition to UNIX. However,
  the lack of an easy to use  contemporary  desktop  environment  for
  UNIX  has  prevented UNIX from finding its way onto the desktops of
  the typical computer user in offices and homes.

  With KDE there is now an easy to use, contemporary desktop environ-
  ment  available  for  UNIX.  Together with a free implementation of
  UNIX such as Linux, UNIX/KDE constitutes a completely free and open
  computing platform available to anyone free of charge including its
  source code for anyone to modify. While there will always  be  room
  for  improvement  we believe to have delivered a viable alternative
  to some of the more commonly found and  commercial  operating  sys-
  tems/desktops combinations available today. It is our hope that the
  combination UNIX/KDE will finally bring open, reliable, stable  and
  monopoly free computing to the average computer. Please visit:

                           http://www.kde.org

  for more information about the KDE Project.


  KDE The Application Development Framework

  Authoring  applications  under  UNIX/X11  used  to  be an extremely
  tedious and laborsome process. KDE recognizes the fact that a  com-
  puting platform is only as good as the number of first class appli-
  cations available to the users of that particular platform. In view
  of  these  circumstances the KDE Project has developed a first rate
  compound document application framework,  implementing  the  latest
  advances  in  framework  technology  and thus positioning itself in
  direct competition to such popular development  frameworks  as  for
  example  Mircrosoft's  MFC/COM/ActiveX  technology. KDE's KOM/Open-
  Parts compound document technology enables  developers  to  quickly
  create  first  rate applications implementing cutting edge technol-
  ogy. KDE's KOM/OpenParts technology is build upon the open industry
  standards such as the object request broker standard CORBA 2.0.


  KDE The Office Application Suite

  Leveraging the KDE application development framework a great number
  of applications have been built for the K  Desktop  Environment.  A
  selection  of  those applications is contained in the KDE base dis-
  tribution. At this moment KDE is developing an  office  application
  suite  based  on  KDE's  KOM/OpenParts  technology  consisting of a
  spread-sheet, a presentation applications, an organizer,  an  email
  and  news client and more. While some of those components are still
  in  alpha  developement  others  are  approaching  release   status
  rapidly.  KPresenter,  KDE's  presentation application was success-
  fully used at the 5th international Linux congress in Cologne, Ger-
  many  to  deliver a KDE presentation. A preview of KOffice has been
  made available at

                  http://www.kde.org/koffice/index.html

  For more information on KDE, its  components  and  architecture  as
  well as  KDE development, please visit:

                 http://www.kde.org/whatiskde


  Screen-shots:

          Screen-shots  of  a  typical K desktop and some of its core
          components can be found at

                     http://www.kde.org/screenshots/kde1shoots.php

          Screen-shots of KOffice, KDE's office application suite can
          be found at

                     http://www.kde.org/koffice/index.html



  Where to get KDE:

          Please visit

                        http://www.kde.org

          for a list of ftp sites holding release 1.0 of KDE.

          Caldera,  Delix,  SuSE  as well as FreeBSD have precompiled
          and customized version of KDE available for their  particu-
          lar  distributions.   For more information on these precom-
          piled version, please visit the above mentioned URL.


  Supported platforms:

          KDE was primarily developed under the Linux variant of  the
          Unix operating system. However it is known to compile with-
          out, or with very few, problems on most Unix variants.

          At the moment we explicitely support  Linux (Intel , Alpha,
          Sparc) and FreeBSD. We have success reports for  IRIX, OSF,
          SunOS, HP-UX. and others.


  Licensing:

          The KDE libraries are licensed under the LGPL, to allow for
          the  development  of  commercial  applications for KDE. KDE
          applications are licensed under the GPL.


  Bug Reports:

          Please submit your bug reports at http://buglist.kde.org  .
          Consider  subscribing to our mailing lists in order to stay
          in contact with the latest developments.


  Mailing Lists:

           kde-announce   (announcements)
           kde            (general discussion)
           kde-devel      (development issues)
           kde-look       (look and feel issues)
           kde-licensing  (licensing issues)
           kde-user       (end user discussion)
           kde-patches    (daily patches)

          To subscribe (unsubscribe), send mail to

                            [list]&#045;r&#x65;qu&#101;st&#x40;&#x6b;&#100;e&#x2e;&#x6f;r&#103;
          with:

                 subscribe (unsubscribe) [your-email-address]

          in the Subject: line of the message. A mailing list archive
          is provided at:

                              http://lists.kde.org

  Thanks:

          We  would  like  to thank Troll Tech AS http://www.troll.no
          for creating Qt  and  our  sponsors  for  making   KDE  ONE
          http://www.kde.org/kde-one.html in Arnsberg, Germany possi-
          ble.

  Epilogue:

          We sincerely believe that we have initiated a new  era  for
          the  Unix  operating  system  and  hope  you will share our
          excitement. We hope that KDE will allow  you  to  get  your
          work  done  faster  and  more efficiently than ever. Please
          consider joining and supporting the project.

                        http://www.kde.org/support.html



  The KDE Core Team (In alphabetic order):

          Roberto Alsina
          Kalle Dalheimer
          Mark Donohoe
          Christian Esken
          Matthias Ettrich
          Steffen Hansen
          Matthias Hoelzer
          Martin Jones
          Sirtaj Singh Kang
          Martin Konold
          Stephan Kulow
          Richard Moore
          Sven Radej
          Reginald Stadlbauer
          Stefan Taferner
          Uwe Thiem
          Mario Weilguni
          Torben Weis
          Robert David Williams
          Bernd Wuebben
          Markus Wuebben



  APPENDIX:


  FTP Mirrors:

          As during previous beta releases, expect ftp.kde.org to  be
          rather busy.  Please take note of the list of official mir-
          rors of ftp.kde.org available here:

               http://www.kde.org/mirrors/ftp.php





</pre>

</TD>
</TR>
</TABLE></CENTER>

<?php
  include "footer.inc"
?>
