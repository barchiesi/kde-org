<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Plasma 5.2");
  $site_root = "../";
  $release = 'plasma-5.2.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<!-- <p style="border: thick solid red">NOT RELEASED YET</p> -->

<p>
<?php i18n("Tuesday, 27 January 2015.
Today KDE releases Plasma 5.2.  This release adds a number
of new components, many new features and many more bugfixes.
");?>
</p>

<div style="text-align: center">
<a href="plasma-5.2/full-screen.png">
<img src="plasma-5.2/full-screen-wee.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="800" height="451" alt="<?php i18n("Plasma 5.2");?>" />
</a>
</div>

<h2><?php i18n("New Components");?></h2>

<div style="float: right; margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center ">
<a href="plasma-5.2/kscreen.png">
<img style="border: 0px;" src="plasma-5.2/kscreen-wee.png" width="400" height="394" alt="<?php i18n("Dual monitor setup");?>" />
</a><br />
<?php i18n("KScreen dual monitor setup");?>
</div>

<p><?php i18n("This release of Plasma comes with some new components to make your desktop even more complete:");?></p>

<p><?php i18n("<strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices.  It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.");?></p>

<p><?php i18n("<strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.");?></p>

<p><?php i18n("<strong>Muon</strong>: install and manage software and other addons for your computer.");?></p>

<p><?php i18n("<strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.");?></p>

<p><?php i18n("<strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.");?></p>

<p><?php i18n("<strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.");?></p>

<p><?php i18n("<strong>KDecoration</strong>: this new library makes it easier and
more reliable to make themes for KWin, Plasma's window manager. It has
impressive memory, performance and stability improvements. If you are
missing a feature don't worry it'll be back in Plasma 5.3.");?></p>

<h2><?php i18n("Other highlights");?></h2>

<p><?php i18n("<strong>Undo</strong> changes to Plasma desktop layout");?></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/output.gif" width="608" height="320" alt="<?php i18n("Undo desktop changes");?>" /><br />
<?php i18n("Undo changes to desktop layout");?>
</div>

<p><?php i18n("Smarter sorting of results in <strong>KRunner</strong>, press Alt-space to easily search through your computer");?></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/krunner.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="744" height="182" alt="<?php i18n("Smart sorting in KRunner");?>" /><br />
<?php i18n("Smart sorting in KRunner");?>
</div>

<p><?php i18n("<strong>Breeze window decoration</strong> theme adds a new look to your desktop and is now used by default");?></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/window_decoration.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="720" height="56" alt="<?php i18n("New Breeze Window Decoration");?>" /><br />
<?php i18n("New Breeze Window Decoration");?>
</div>

<p><?php i18n("The artists in the visual design group have been hard at work on many new <strong>Breeze icons</strong>");?></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/icons.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="552" height="337" alt="<?php i18n("New Breeze Window Decoration");?>" /><br />
<?php i18n("More Breeze Icons");?>
</div>

<p><?php i18n("They are have added a new white mouse <strong>cursor theme</strong> for Breeze.");?></p>

<p><?php i18n("<strong>New plasma widgets</strong>: 15 puzzle, web browser, show desktop");?></p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px;" src="plasma-5.2/new_plasmoid.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="563" height="316" alt="<?php i18n("Web browser plasmoid");?>" /><br />
<?php i18n("Web browser plasmoid");?>
</div>

<p><?php i18n("<strong>Audio Player controls</strong> in KRunner, press Alt-Space and type next to change music track");?></p>

<p><?php i18n("The Kicker alternative application menu can install applications from the menu and adds menu editing features.");?></p>

<p><?php i18n('Our desktop search feature Baloo sees optimisations on
startup. It now consumes 2-3x less CPU on startup.  The query parser
supports "type" / "kind" properties, so you can type "kind:Audio" in
krunner to filter out Audio results.');?></p>

<p><?php i18n("In the screen locker we improved the integration with
logind to ensure the screen is properly locked before suspend.  The
background of the lock screen can be configured.  Internally this uses
part of the Wayland protocol which is the future of the Linux
desktop.");?></p>

<p><?php i18n("There are improvements in the handling of multiple monitors. The
detection code for multiple monitors got ported to use the XRandR
extension directly and multiple bugs related to it were fixed.");?></p>

<p><?php i18n("<strong>Default applications in Kickoff</strong> panel menu have been updated to list Instant Messaging, Kontact and Kate.");?></p>

<p><?php i18n("There is a welcome return to the touchpad enable/disable feature for laptop keypads with these keys.");?></p>

<p><?php i18n("Breeze will <strong>set up GTK themes</strong> on first login to match.");?></p>

<p><?php i18n("Over 300 bugs fixed throughout Plasma modules.");?></p>

<p><a href="plasma-5.1.2-5.2.0-changelog.php"><?php i18n("Plasma modules 5.2 full changelog");?></a></p>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images which use Plasma 5.2 beta are available for
development versions of <a
href='http://cdimage.ubuntu.com/kubuntu/releases/vivid/alpha-2/'>Kubuntu
Vivid Beta</a> and <a
href='http://www.dvratil.cz/2015/01/plasma-5-2-beta-available-for-fedora-testers/'>Fedora
21 remix</a>.  We expect Plasma 5 to be picked up as the default
desktop in leading distributions in the coming months.
");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
