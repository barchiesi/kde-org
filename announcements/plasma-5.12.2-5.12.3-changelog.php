<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12.3 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.12.3";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Add explicit includes to fix build with Qt 5.11. <a href='https://commits.kde.org/bluedevil/178e5fadfb4fc01695bb14436c21734bdb83426c'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Render application icon at paintdevice DPR. <a href='https://commits.kde.org/breeze/c32eb41292752fd9a82fa258fb93f538f712cb91'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390639'>#390639</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10713'>D10713</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make sure there's a subject, otherwise i18n() goes mental. <a href='https://commits.kde.org/discover/3f0b8f91289741fbd16d30b8607463b2c3e644e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391189'>#391189</a></li>
<li>Remove unused Rating constructor. <a href='https://commits.kde.org/discover/93719d96e82de2528ce43ffe54dd2f5158a90f8b'>Commit.</a> </li>
<li>Fix sortable rating for KNS resources. <a href='https://commits.kde.org/discover/eac19e7424f411498e482815df6945993bcd737f'>Commit.</a> </li>
<li>Add a close button to AddSourceDialog. <a href='https://commits.kde.org/discover/2b9e1b5e1ebbfab384b3fe3febe63e0ef076c273'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390148'>#390148</a></li>
<li>Name argument. <a href='https://commits.kde.org/discover/b80e961ef5b9471cd16b85877f4d313b5e664228'>Commit.</a> </li>
<li>Close the resporce page when navigating categories too. <a href='https://commits.kde.org/discover/2c96311e28c4dfdf137b347d10062e7efb5dfc67'>Commit.</a> </li>
<li>Don't track errors when fetching dependencies. <a href='https://commits.kde.org/discover/e7cec342b0c2b570450c1b51694ecb36054f75bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391142'>#391142</a></li>
<li>Show a busy indicator when screenshots are fetching. <a href='https://commits.kde.org/discover/76b90f4069c8166bf037536ae9a98f77bee8ac4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391130'>#391130</a></li>
<li>Fix flatpak source filtering. <a href='https://commits.kde.org/discover/b38e157f3a0fb70e8b876089dca4cd9236a38465'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391126'>#391126</a></li>
<li>Don't look for extensions in flatpak. <a href='https://commits.kde.org/discover/e4ed41de5c8d6f8a247eed87e659cb743b682dff'>Commit.</a> </li>
<li>Fix installation of Discover backends. <a href='https://commits.kde.org/discover/523942d2fa0bc93362d49906d973d351f0d95ed1'>Commit.</a> </li>
<li>Don't show the "Add Source" option for PackageKit. <a href='https://commits.kde.org/discover/2990069ff4baddb62e52684c0e7b48720fb47a4b'>Commit.</a> </li>
<li>Don't close the add source dialog until a proper source has been offered. <a href='https://commits.kde.org/discover/c9bff67739e672454ec733377a64b0518c46fbdc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390148'>#390148</a></li>
<li>Don't try to add wrong URI as flatpak repositories. <a href='https://commits.kde.org/discover/b12b4e97e10d3ced5ac0c952390f7f154f4a3ab1'>Commit.</a> </li>
<li>Don't show error messages for files that shouldn't be there. <a href='https://commits.kde.org/discover/1d2b86a9202baf2f38715445c77adddc06956b5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390922'>#390922</a></li>
<li>Remove unneeded ifdef, set the right KF5 version as dependency. <a href='https://commits.kde.org/discover/6d97aa0d94455bd7dc2d1521ad04a7d7163fdb74'>Commit.</a> </li>
<li>Provide an alphabetically sorted list for updates. <a href='https://commits.kde.org/discover/2bddf51549db15130c2e96865fb52be31f338df1'>Commit.</a> See bug <a href='https://bugs.kde.org/390911'>#390911</a></li>
<li>Make AbstractResource::name const. <a href='https://commits.kde.org/discover/20baccd41cc29e58070632be18e4804c99508fe9'>Commit.</a> </li>
<li>If there's no appstream data, extract it from the bundle metadata. <a href='https://commits.kde.org/discover/0321226d19f12cb6fb03be799f81628946adc111'>Commit.</a> </li>
<li>No reason to clear something that was never initialized. <a href='https://commits.kde.org/discover/536a99716e89f8c5a0ba3d64889db6723550293f'>Commit.</a> See bug <a href='https://bugs.kde.org/390907'>#390907</a></li>
<li>Readability. <a href='https://commits.kde.org/discover/eb26cb5e47bce26b60ba389c22080d41d6e4c2f2'>Commit.</a> </li>
<li>Don't consider it a changelog if it doesn't have a description. <a href='https://commits.kde.org/discover/431ab3e7796405573261f0beb1a0f1121e8fdcf9'>Commit.</a> </li>
<li>Reduce sidebar width. <a href='https://commits.kde.org/discover/bfa25177805fcb3b57b5db3de063d96b5bfec2e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385992'>#385992</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10756'>D10756</a></li>
<li>Don't crash if there's no appstream release information. <a href='https://commits.kde.org/discover/e9c46d65d85ad72765af6c52e336befc41fd9262'>Commit.</a> </li>
<li>Show last AppStream release as flatpak changelog as well. <a href='https://commits.kde.org/discover/a8e9057eb4434204c7a9eb0282956a914c0dd916'>Commit.</a> </li>
<li>Show the last release as changelog. <a href='https://commits.kde.org/discover/fa438d2734503ccb87dcf938cf92cf2ce202ab24'>Commit.</a> </li>
<li>Make the AddSourceDialog a bit more flexible to long titles. <a href='https://commits.kde.org/discover/e4f816271b0f12cd7f221cd83c4722b3adb0707a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389911'>#389911</a></li>
<li>Remove unused include. <a href='https://commits.kde.org/discover/3d54198c33bacef71416ef9661697cd3e84a19c5'>Commit.</a> </li>
<li>Remove handbook menu entry. <a href='https://commits.kde.org/discover/4153dc99d2e9749fcfc0652575d9818171fb9f80'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390401'>#390401</a></li>
<li>Improve search field behavior. <a href='https://commits.kde.org/discover/fb0e2f7d538849f0f4c8b09ea7ae63cf95985a08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390909'>#390909</a></li>
<li>Get back the sortOrder after a search. <a href='https://commits.kde.org/discover/be5868a2e4ac03f065781be6c1f5a1d08ae11007'>Commit.</a> </li>
<li>Make sure we deliver consistently resources. <a href='https://commits.kde.org/discover/d601131ec71d82ef081106b29913ea81c87b9fd2'>Commit.</a> </li>
<li>Don't change sorting upon refresh in sortByRelevancy. <a href='https://commits.kde.org/discover/e261bb25c331c1d274d9e6cb9d5ab6ea75813180'>Commit.</a> </li>
<li>Fix ApplicationScreenshots shadow for Breeze Dark. <a href='https://commits.kde.org/discover/4e561fe2be3fed013b5b6331f76c0ece894aba93'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10701'>D10701</a></li>
<li>Readability. <a href='https://commits.kde.org/discover/08f68d3981abb78e0b885a8c216dd687844caa0f'>Commit.</a> </li>
<li>Simplify flatpak installation set up. <a href='https://commits.kde.org/discover/0b98bf7f427952dc191c501eddf4a4b0b1146642'>Commit.</a> </li>
<li>Move back to the first page when changing the search. <a href='https://commits.kde.org/discover/4e7c19704ec211b95eff7d93ce3cabdf538ad7a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390191'>#390191</a></li>
<li>Also mark as not fetching after fetching updates. <a href='https://commits.kde.org/discover/83cde4f08fab5ac4c32f0bbeea8a306bbece1353'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389072'>#389072</a></li>
<li>Fix weird behavior on the search field once it's cleared. <a href='https://commits.kde.org/discover/a263fccd2532f04160aa82743f4a0f8172f9af3f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390805'>#390805</a></li>
<li>Simplify resource state tracking in snap. <a href='https://commits.kde.org/discover/4593a765bf211fb7750038acfa20d94fa087be8c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390482'>#390482</a></li>
<li>Don't issue empty snap find calls. <a href='https://commits.kde.org/discover/1bcf7912490eeb27ec24b0d1b1a3af9460837fcb'>Commit.</a> </li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Add explicit includes to fix build with Qt 5.11. <a href='https://commits.kde.org/drkonqi/1a2c539bfaba8fe9422ef9bf45590dabd00291a9'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[grouping applet] Use "object-group" as icon. <a href='https://commits.kde.org/kdeplasma-addons/ef5008f654c6c51d40c31f32625cc1b94754aa3c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10795'>D10795</a></li>
<li>[grouping applet] Add missing Comment entry to metadata. <a href='https://commits.kde.org/kdeplasma-addons/1319b7bfe8920bbf849030a7de4a69f4af297eb4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10793'>D10793</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Use QQuickWidget instead of QQuickView for the main view in the kcm. <a href='https://commits.kde.org/kscreen/47521b011d33f4fac4f14813aa63a15ec0942765'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11045'>D11045</a></li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Silence CMake policy CMP0063 warning. <a href='https://commits.kde.org/ksshaskpass/94dd449aa185667e3c3ae943591cf5268fad1b78'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Explicitly include <QDoubleValidator> to fix build with Qt 5.11. <a href='https://commits.kde.org/ksysguard/093de8d449b47a34dc1b65a2245f93cdc8d62aa0'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix the build on armhf/aarch64. <a href='https://commits.kde.org/kwin/3fa287280b04746bebcee436f92545f9f8420452'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10762'>D10762</a></li>
<li>Convert EffectView to a QQuickWidget. <a href='https://commits.kde.org/kwin/68a2ec5d63652ddb7afbc646270c73e25a4a70f9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11066'>D11066</a></li>
<li>Sanity check WindowQuad before trying to create a grid out of it. <a href='https://commits.kde.org/kwin/e1afef3d4563803249dc25487bc086f753c4e0ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390953'>#390953</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11015'>D11015</a></li>
<li>Fix typo in config group name. <a href='https://commits.kde.org/kwin/b1bffa445823a956cd4dce072c92787ef83502de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11047'>D11047</a></li>
<li>[kcmkwin/rules] Disable detect button on Wayland. <a href='https://commits.kde.org/kwin/6d6576f819322dbaf0be30a1b43e21079637aa4d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10642'>D10642</a></li>
<li>Don't crash if the cursor theme fails to create. <a href='https://commits.kde.org/kwin/2ea5153e1c979760c56bd81f3d47f2fb373130c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390314'>#390314</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10549'>D10549</a></li>
<li>Support modifier mouse/scroll action on internal decorated windows. <a href='https://commits.kde.org/kwin/b554e54e87b5d1ccd94d5bdde761356dd760a2cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374880'>#374880</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10440'>D10440</a></li>
<li>Explicitly include <QAction> to fix build with Qt 5.11. <a href='https://commits.kde.org/kwin/067274151ff857554ffbf6d6165e1cd8b0099a63'>Commit.</a> </li>
<li>[autotests] Set breeze icon theme name in integration tests. <a href='https://commits.kde.org/kwin/e9c72e4e6217732a8621023e5b9b4049cc7c9d7d'>Commit.</a> </li>
<li>Send hoverLeave or hoverMotion after touch up on decoration. <a href='https://commits.kde.org/kwin/911176a887be639a1e123a3c633fb05870fda33d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386231'>#386231</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10308'>D10308</a></li>
<li>Init the icon in ShellClient::init. <a href='https://commits.kde.org/kwin/5795fc8cc04a422194e06d20fb395cbbf3ea2ad6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10294'>D10294</a></li>
<li>Disable window rule configuration for Wayland. <a href='https://commits.kde.org/kwin/7bf4a94286a71e6847854953d4196063e1bb86d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10594'>D10594</a></li>
<li>Properly handle move during touch events. <a href='https://commits.kde.org/kwin/338c7362c9fbb37f1fb8beb9a50644b585d8135e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390113'>#390113</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10431'>D10431</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[kaccess] Improve the port away from kdelibs4support. <a href='https://commits.kde.org/plasma-desktop/0f3e344f2317c25dea438e5575b0b76a0023574c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10955'>D10955</a></li>
<li>[taskmanager applet] Disable MPRIS Stop entry in context menu if stopped. <a href='https://commits.kde.org/plasma-desktop/e5c097363b057631d4d37f98d4f580ffea2f7b52'>Commit.</a> </li>
<li>[taskmanager applet] Fix MPRIS entries in context menu for CanPause==false. <a href='https://commits.kde.org/plasma-desktop/2b3f9cd9092f84081f92080053f0da66d00142f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11007'>D11007</a></li>
<li>[taskmanager applet] Fix MPRIS buttons in tooltip for CanPause==false. <a href='https://commits.kde.org/plasma-desktop/09eba0235b90e1170eed623a7d57a66bb8fcd8fc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11025'>D11025</a></li>
<li>Prevent duplicate entries in the Baloo kcm folders list. <a href='https://commits.kde.org/plasma-desktop/ae1b5a7ee55ca83ae9c0f756a64580606dd04430'>Commit.</a> </li>
<li>Improve detecting D&D between two screen showing the same URL. <a href='https://commits.kde.org/plasma-desktop/07001277a8a0893aeb54240d7f5cdd9825fb57ed'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10729'>D10729</a></li>
<li>[Task Manager] Limit badge label to a sane size. <a href='https://commits.kde.org/plasma-desktop/3b526a3a2aa9073660042198debc26842c465ea8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10586'>D10586</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Use ScrollView from QtQuickControls. <a href='https://commits.kde.org/plasma-nm/0267a6a016b396ae6adf38c7c68b19841d5428e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390846'>#390846</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>[cuttlefish] Fix unencoded & in appdata bugtracker link. <a href='https://commits.kde.org/plasma-sdk/02b7a56eca0add88833a5aa97e54e626dca00b04'>Commit.</a> </li>
<li>Remove unported and dead code for remote-widgets-browser. <a href='https://commits.kde.org/plasma-sdk/ecd8311b6d89386be488e3a9ce2720fa62578a9e'>Commit.</a> </li>
<li>[cuttlefish] Fix bugtracker link in appdata. <a href='https://commits.kde.org/plasma-sdk/45be2bad6ae220390d7c365fdf6e62d15830283e'>Commit.</a> </li>
<li>[plasmoidviewer] Set app metadata for a proper D-Bus service name. <a href='https://commits.kde.org/plasma-sdk/cc527f35f029da48cf57b655db710154a9c46196'>Commit.</a> </li>
<li>[plasmoidviewer] Create KDBusService only after processing cmdl args. <a href='https://commits.kde.org/plasma-sdk/478d7755c4a847958eb24ef5d401341d1443ebf7'>Commit.</a> </li>
<li>[engineexplorer] Remove extra margins around UI forms. <a href='https://commits.kde.org/plasma-sdk/38188212f79e083b38df0c305a8c1898282e729b'>Commit.</a> </li>
<li>Actually use KF5_MIN_VERSION with find_package(KF5). <a href='https://commits.kde.org/plasma-sdk/b345bc69c921545b2201fac8327b915345f09268'>Commit.</a> </li>
<li>[cuttlefish] Set translation domain for app in code, not as compile flag. <a href='https://commits.kde.org/plasma-sdk/dcccde99799b65701e7bdad64bc039a83cd4f7f2'>Commit.</a> </li>
<li>Use project() only once in the toplevel CMakeLists.txt. <a href='https://commits.kde.org/plasma-sdk/5befe1e22106b87a274c6b5ef8c0b0b76ac5dcfd'>Commit.</a> </li>
<li>Actually use QT_MIN_VERSION with find_package(Qt5). <a href='https://commits.kde.org/plasma-sdk/f5785c1b00783172c5355a838458601517bed02a'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/plasma-sdk/14a7893355b2296d85105fb607a749ebdd539a78'>Commit.</a> </li>
<li>[engineexplorer] Add missing handling of cmdl args by aboutData. <a href='https://commits.kde.org/plasma-sdk/5928723d583bcda450423fd784bf44d34c46361c'>Commit.</a> </li>
<li>[engineexplorer] Do not repeat work of  KAboutData::setApplicationData(). <a href='https://commits.kde.org/plasma-sdk/bc4e0274b3a56b6e20db993b2c25e7a8679051f4'>Commit.</a> </li>
<li>[engineexplorer] Fix unported setting of window icon. <a href='https://commits.kde.org/plasma-sdk/9c9ad31b288a437df51a984b0e07cc1e0a4b3137'>Commit.</a> </li>
<li>[plasmoidviewer] Add missing KLocalizedString::setApplicationDomain(). <a href='https://commits.kde.org/plasma-sdk/3177573af91064dfd730468cfcd58183f379ee11'>Commit.</a> </li>
<li>[plasmoidviewer] Add missing window icon, use "plasma". <a href='https://commits.kde.org/plasma-sdk/f26d327af258e884ed0dd9b409a9af370ae4fdff'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[media controller applet] Support MPRIS services where CanPause==false. <a href='https://commits.kde.org/plasma-workspace/06913fc2556a169a639557453ca256ee8adc09f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10973'>D10973</a></li>
<li>[mpris2 dataengine] Fix media key handling to match MediaPlayer2.Player. <a href='https://commits.kde.org/plasma-workspace/d62c50d5c678305366d0d52458f6b5c6939e40d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10991'>D10991</a></li>
<li>[media controller applet] Fix JS error on no xesam:url data set. <a href='https://commits.kde.org/plasma-workspace/c7f2d8e7331ab015bc98529ddf075f82cf856f8f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11028'>D11028</a></li>
<li>[mediacontroller applet] Remove de-funct "General" configuration page. <a href='https://commits.kde.org/plasma-workspace/82b01d551b08af6e7794be65ffd772232e5ff750'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10975'>D10975</a></li>
<li>Fix the userswitcher when using the mouse for switching. <a href='https://commits.kde.org/plasma-workspace/1eb9ae7e33e2b0cb14ab10bc81710fa4b8f19ef5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391007'>#391007</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10802'>D10802</a></li>
<li>Adapt to further nullpointers from prison. <a href='https://commits.kde.org/plasma-workspace/c6e678a274b6bb976bcdab67d69a70f9f80e061d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10628'>D10628</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Add explicit includes to fix build with Qt 5.11. <a href='https://commits.kde.org/powerdevil/407be42e37d4d0e16db87cd7839b244c0bb9178a'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Add explicit includes to fix build with Qt 5.11. <a href='https://commits.kde.org/systemsettings/e50be6ca5fbaa49eb84b81e5f649317599a2cdf0'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
