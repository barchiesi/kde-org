<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.2.0");
  $site_root = "../";
  $release = '5.2.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
September 12, 2014. KDE today announces the release
of KDE Frameworks 5.2.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<?php i18n("
<h3>KActivities</h3>

<ul>
<li>reimplementation of the file item plugin for linking files to activities</li>
</ul>

<h3>KArchive</h3>

<ul>
<li>fix handling of uncompressed files</li>
</ul>

<h3>KConfigWidgets</h3>

<ul>
<li>fix missing default shortcuts for standard actions, leading 
to many runtime warnings</li>
<li>better support for QGroupBox in KConfigDialogManager</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>Mark KAboutData::setProgramIconName() as deprecated, it did not 
do anything. Use QApplication::setWindowIcon(QIcon::fromTheme(\"...\")) instead.</li>
<li>new classes Kdelibs4ConfigMigrator and KPluginMetaData</li>
</ul>

<h3>KDeclarative</h3>

<ul>
<li>added org.kde.kio component.</li>
</ul>

<h3>KImageFormats</h3>

<ul>
<li>disable the DDS and JPEG-2000 plugins when Qt version is 5.3 
or later</li>
</ul>

<h3>KIO</h3>

<ul>
<li>now follows the mime-apps spec, for better interoperability
with gio when it comes to the user's preferred and default apps.</li>
<li>new classes EmptyTrashJob and RestoreJob.</li>
<li>new functions isClipboardDataCut and setClipboardDataCut.</li>
</ul>

<h3>KNewStuff</h3>

<ul>
<li>installing \"stuff\" works again (porting bug)</li>
</ul>

<h3>KWidgetsAddons</h3>

<ul>
<li>new class KColumnResizer (makes it easy to vertically align widgets across groups)</li>
</ul>

<h3>KWindowSystem</h3>

<ul>
<li>New method KWindowSystem::setOnActivities</li>
</ul>

<h3>KXmlGui</h3>

<ul>
<li>KActionCollection::setDefaultShortcuts now makes the shortcut
active too, to simplify application code.</li>
</ul>

<h3>Threadweaver</h3>

<ul>
<li>The maximum worker count will now decrease if a lower value is set
after workers have been created. Previously, workers would remain active
once they have been created.</li>
<li>Examples from the previous ThreadWeaverDemos Github repository are
being merged into the KF5 ThreadWeaver repo.</li>
<li>The maximum worker count can now be set to zero (the previous minimum
was 1). Doing so will effectively halt processing in the queue.</li>
<li>Documentation of various aspects of ThreadWeaver use is becoming part
of the KDE Frameworks Cookbook. Parts of it is located in the examples/
directory.</li>
</ul>

<h3>Buildsystem changes</h3>

<ul>
<li>Support for relative libexec dir.</li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>the file dialog now remembers its size correctly, and works better with remote URLs.</li>
</ul>
");?>

<br clear="all" />
<?php i18n("
<h2>Getting started</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<ul>
<li><a href='%1'>Binary package distro install instructions</a>.<br /></li>
</ul>
Building  from source is possible using the basic <em>cmake .; make; make  install</em> commands. For a single Tier 1 framework, this is often  the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to  <a href='%2'>use kdesrc-build</a>.
", "http://community.kde.org/Frameworks/Binary_Packages", "http://kdesrc-build.kde.org/");?>
</p>
<p>
<?php print i18n_var("
Frameworks 5.2.0 requires Qt 5.2.  It is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
<ul>
<li><a href='%1'>KDE Frameworks 5.2.0 Source Info page with known bugs and security issues</a></li>
</ul>
", "http://kde.org/info/kde-frameworks-5.2.0.php");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
<p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href='%5'>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
