<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = '4.13';
  $release_full = '4.13.0';
  $page_title = i18n_noop("KDE Applications 4.13 Benefit From The New Semantic Search, Introduce New Features");
  $site_root = "..";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>
<p>
<?php i18n("April 16, 2014");?>
</p>

<p>
<?php i18n("The KDE Community is proud to announce the latest major updates to the KDE Applications delivering new features and fixes. Kontact (the personal information manager) has been the subject of intense activity, benefiting from the improvements to KDE's Semantic Search technology and bringing new features. Document viewer Okular and advanced text editor Kate have gotten interface-related and feature improvements. In the education and game areas, we introduce the new foreign speech trainer Artikulate; Marble (the desktop globe) gets support for Sun, Moon, planets, bicycle routing and nautical miles. Palapeli (the jigsaw puzzle application) has leaped to unprecedented new dimensions and capabilities.");?>
</p>

<?php showscreenshotpng("applications.png", ""); ?>

<h2><?php i18n("KDE Kontact Introduces New Features And More Speed");?></h2>
<p>
<?php i18n("KDE’s Kontact Suite introduces a series of features in its various components. KMail introduces Cloud Storage and improved sieve support for server-side filtering, KNotes can now generate alarms and introduces search capabilities, and there have been many improvements to the data cache layer in Kontact, speeding up almost all operations.");?>
</p>

<h3><?php i18n("Cloud Storage Support");?></h3>
<p>
<?php i18n("KMail introduces storage service support, so large attachments on can be stored in cloud services and included as with links in email messages. Supported storage services include Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic and there is a generic WebDav option. A <em>storageservicemanager</em> tool helps with the managing of files on these services.");?>
</p>

<?php showscreenshotpng("CloudStorageSupport.png", ""); ?>

<h3><?php i18n("Much Improved Sieve Support");?></h3>
<p>
<?php i18n("Sieve Filters, a technology to let KMail handle filters on the server, can now handle vacation support for multiple servers. The KSieveEditor tool allows users to edit sieve filters without having to add the server to Kontact.");?>
</p>

<h3><?php i18n("Other Changes");?></h3>
<p>
<?php i18n("The quick filter bar has a small user interface improvement and benefits greatly from the improved search capabilities introduced in the KDE Development Platform 4.13 release. Searching has become significantly faster and more reliable. The composer introduces a URL shortener, augmenting the existing translation and text snippets tools.");?>
</p>

<p>
<?php i18n("Tags and annotations of PIM data are now stored in Akonadi. In future versions, they will be also stored in servers (on IMAP or Kolab), making it possible to share tags across multiple computers. Akonadi: Google Drive API support has been added. There is support for searching with 3rd party plugins (which means that results can be retrieved very quickly) and server-search (searching items not indexed by a local indexing service).");?>
</p>

<h3><?php i18n("KNotes, KAddressbook");?></h3>
<p>
<?php i18n("Significant work was done on KNotes, fixing a series of bugs and small annoyances. The ability to set alarms on notes is new, as is searching through notes. Read more <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>here</a>. KAdressbook gained printing support: more details <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>here</a>.");?>
</p>

<h3><?php i18n("Performance Improvements");?></h3>
<p>
<?php i18n("Kontact performance is noticeably improved in this version. Some improvements are due to the integration with the new version of KDE’s <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantic Search</a> infrastructure, and the data caching layer and loading of data in KMail itself have seen significant work as well. Notable work has taken place to improve support for the PostgreSQL database. More information and details on performance-related changes can be found in these links:
<ul> 
<li>Storage Optimizations: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprint report</a>;</li> 
<li>speed up and size reduction of database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>mailing list</a>;</li>
<li>optimization in access of folders: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>;</li>
</ul>");?>
</p>

<h3><?php i18n("KNotes, KAddressbook");?></h3>
<p>
<?php i18n("Significant work was done on KNotes, fixing a series of bugs and small annoyances. The ability to set alarms on notes is new, as is searching through notes. Read more <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>here</a>. KAdressbook gained printing support: more details <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>here</a>.");?>
</p>

<h2><?php i18n("Okular Refines User Interface");?></h2>
<p>
<?php i18n("This release of the Okular document reader brings a number of improvements. You can now open multiple PDF files in one Okular instance thanks to tab support. There is a new Magnifier mouse mode and the DPI of the current screen is used for PDF rendering, improving the look of documents. A new  Play button is included in presentation mode and there are improvements to Find and Undo/Redo actions.");?>
</p>

<?php showscreenshotpng("okular.png", ""); ?>

<h2><?php i18n("Kate introduces improved statusbar, animated bracket matching, enhanced plugins");?></h2>
<p>
<?php i18n("The latest version of the advanced text editor Kate introduces <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animated bracket matching</a>, changes to make <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>AltGr-enabled keyboards work in vim mode</a> and a series of improvements in the Kate plugins, especially in the area of Python support and the <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build plugin</a>. There is a new, much <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>improved  status bar</a> which enables direct actions like changing the  indent settings, encoding and highlighting, a new tab bar in each view, code completion support for <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>the D programming language</a> and <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>much more</a>. The team has <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>asked for feedback on what to improve in Kate</a> and is shifting some of its attention to a Frameworks 5 port.");?>
</p>

<h2><?php i18n("Miscellaneous features all over");?></h2>
<p>
<?php i18n("Konsole brings some additional flexibility by allowing custom stylesheets to control tab bars. Profiles can now store desired column and row sizes. See more <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>here</a>.");?>
</p>

<p>
<?php i18n("Umbrello makes it possible to duplicate diagrams and introduces intelligent context menus which adjust their contents to the selected widgets. Undo support and visual properties have been improved as well. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduces RAW preview support</a>.");?>
</p>

<?php showscreenshotpng("marble.png", ""); ?>

<p>
<?php i18n("The sound mixer KMix introduced remote control via the DBUS inter-process communication protocol (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>details</a>), additions to the sound menu and a new configuration dialog (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>details</a>), and a series of bug fixes and smaller improvements.");?>
</p>

<p>
<?php i18n("Dolphin's search interface has been modified to take advantage of the new search infrastructure and received further performance improvements. For details, read this <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>overview of optimization work during the last year</a>.");?>
</p>

<p>
<?php i18n("KHelpcenter adds alphabetical sorting for modules and category reorganization to make it easier to use.");?>
</p>
<h2><?php i18n("Games and educational applications");?></h2>
<p>
<?php i18n("KDE's game and educational applications have received many updates in this release. KDE's jigsaw puzzle application, Palapeli, has gained <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nifty new features</a> that make solving large puzzles (up to 10,000 pieces) much easier for those who are up to the challenge. KNavalBattle shows enemy ship positions after the game ends so that you can see where you went wrong.");?>
</p>

<?php showscreenshotpng("palapeli.png", ""); ?>

<p>
<?php i18n("KDE's Educational applications have gained new features. KStars gains a scripting interface via D-BUS and can use the astrometry.net web services API to optimize memory usage. Cantor has gained syntax highlighting in its script editor and its Scilab and Python 2 backends are now supported in the editor. Educational mapping and navigation tool Marble now includes the positions of the <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sun, Moon</a> and <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planets</a> and enables <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>capturing movies during virtual tours</a>. Bicycle routing is improved with the addition of cyclestreets.net support. Nautical miles are now supported and clicking a <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo URI</a> will now open Marble.");?>
</p>


<h4><?php i18n("Installing KDE Applications");?></h4>
<?php
  include("boilerplate.inc");
?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
