<!-- header goes into the specific page -->
<p align="justify">
<?php i18n("KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.");?>
<br />
<?php print i18n_var("KDE software can be obtained in source and various binary formats from <a
href='http://download.kde.org/stable/%1'>http://download.kde.org</a> and can
also be obtained on <a href='http://www.kde.org/download/cdrom.php'>CD-ROM</a>
or with any of the <a href='http://www.kde.org/download/distributions.php'>major
GNU/Linux and UNIX systems</a> shipping today.", $release_full);?>
</p>
<p align="justify">
  <a name="packages"><em><?php i18n("Packages");?></em></a>.
  <?php print i18n_var("Some Linux/UNIX OS vendors have kindly provided binary packages of %1
for some versions of their distribution, and in other cases community volunteers
have done so.", $release_full);?> <br />
<a name="package_locations"><em><?php i18n("Package Locations");?></em></a>.
<?php print i18n_var("For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_%1'>Community Wiki</a>.", "SC_".$release_full);?>
</p>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for %1 may be <a href='/info/%1.php'>freely downloaded</a>.
Instructions on compiling and installing KDE software %1
  are available from the <a href='/info/%1.php#binary'>%1 Info Page</a>.", $release_full);?>
</p>

<h4>
<?php i18n("System Requirements");?>
</h4>
<p align="justify">
<?php i18n("In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.");?>
</p>

