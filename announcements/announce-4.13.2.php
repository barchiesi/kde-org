<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships June Updates to Applications, Platform and Plasma Workspaces");
  $site_root = "../";
  $release = '4.13.2';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n("June 10, 2014. Today KDE released updates for its Applications and Development Platform, the second in a series of monthly stabilization updates to the 4.13 series. This release also includes an <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2013-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.11.10&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0='>updated Plasma Workspaces 4.11.10</a>. Both releases contain only bugfixes and translation updates, providing a safe and pleasant update for everyone.");?>
<br /><br />

<?php i18n("More than 40 recorded bugfixes include improvements to Personal Information Management suite Kontact, Umbrello UML Modeller, the Desktop search functionality, web browser Konqueror and the file manager Dolphin.");?>
<br /><br />
<?php print i18n_var("A more complete <a href='%1'>list of changes</a> can be found in KDE's issue tracker.", "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2014-01-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=".$release."&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=");?>
<br /><br />
<?php print i18n("Additional and noteworthy: this release of KDE Applications includes a number of important fixes for Kopete: a decrease in the exit time of Kopete with enabled statistics plugin, a fix for the compilation of jabber libjingle for non x86 architectures and another fix for voice call support in the jabber libjingle library. Without this last fix, voice calls worked only with the old Google Talk windows jingle client. Now after applying the patches it was tested with GMail web plugin, old Google Talk windows client, telepathy-gabble and other Kopete versions.");?>
<br /><br />
<?php print i18n_var("To download source code or packages to install go to the <a href='http://www.kde.org/info/%1.php'>%1 Info Page</a>. If you want to find out more about the 4.13 versions of KDE Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.13/'>4.13 release notes</a>.", $release);?>
</p>


<div align="center" class="screenshot">
<a href="4.13/screenshots/applications.png"><img src="4.13/screenshots/thumbs/applications.png" /></a>
</div>

<p align="justify">
<?php print i18n_var("KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a
href='http://download.kde.org/stable/%1/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.", $release);?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>

<h4>
  <?php print i18n_var("Installing %1 Binary Packages", $release);?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php print i18n_var("Some Linux/UNIX OS vendors have kindly provided binary packages of %1 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.", $release);?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php print i18n_var("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/%1.php#binary'>%1 Info Page</a>.", $release);?>
</p>

<h4>
  <?php print i18n_var("Compiling %1", $release);?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for %1 may be <a href='http://download.kde.org/stable/%1/src/'>freely downloaded</a>. Instructions on compiling and installing %1 are available from the <a href='/info/%1.php'>%1 Info Page</a>.", $release);?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
