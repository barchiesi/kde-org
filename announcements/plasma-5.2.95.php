<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Plasma 5.3 Beta, New Feature Release");
  $site_root = "../";
  $release = 'plasma-5.2.95';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<p>
<?php i18n("Tuesday, 14 April 2015."); ?> 
<?php i18n("Today KDE releases a beta release of Plasma 5, versioned 5.2.95.  
");?>
</p>

<figure style="float: none">
<a href="plasma-5.3/plasma-5.3.png">
<img src="plasma-5.3/plasma-5.3-ickle.png" style="border: 0px" width="600" height="337" alt="<?php i18n("Plasma 5.3 Beta");?>" />
</a>
<figcaption><?php i18n("Plasma 5.3 Beta");?></figcaption>
</figure>

<h2><?php i18n("Highlights"); ?></h2>

<br clear="all" />
<figure>
<a href="plasma-5.3/power-management-info.png">
<img src="plasma-5.3/power-management-info.png" style="border: 0px" width="465" height="115" alt="<?php i18n("Inform what is blocking power saving");?>" />
</a>
<figcaption><?php i18n("Battery applet now informs what is blocking power saving");?></figcaption>
<a href="plasma-5.3/kinfocenter-energy.png">
<img src="plasma-5.3/kinfocenter-energy-wee.png" style="border: 0px" width="299" height="240" alt="<?php i18n("Energy Usage monitor");?>" />
</a>
<figcaption><?php i18n("New energy usage monitor");?></figcaption>
</figure>

<h3><?php i18n("Enhanced Power Management");?></h3>
<ul>
<?php i18n("
<li>Power management settings can be configured
differently for certain activities</li>
<li>Laptop will not suspend when closing the lid while an external monitor is
connected ('cinema mode', by default, can be turned off)</li>
<li>Power management inhibitions block lock screen too</li>
<li>Screen brightness changes are now animated on most hardware</li>
<li>No longer suspends when closing the lid while shutting down</li>
<li>Support for keyboard button brightness controls on lock screen</li>
<li>KInfoCenter provides statistics about energy consumption</li>
<li>Battery monitor now shows which applications are currently holding a power management
inhibition for example ('Chrome is currently suppressing PM: Playing video')</li>
");?>
</ul>


<br clear="all" />
<figure>
<a href="plasma-5.3/bluedevil-applet3.png">
<img src="plasma-5.3/bluedevil-applet3-wee.png" style="border: 0px" width="299" height="309" alt="<?php i18n("Bluedevil");?>" />
</a>
<figcaption><?php i18n("The new Bluedevil Applet");?></figcaption>
</figure>

<h3><?php i18n("Better Bluetooth Capabilities") ?></h3>
<ul>
<?php i18n("
<li>New Bluetooth applet</li>
<li>Bluedevil was ported to a new library from KDE, BluezQt</li>
<li>Added support for blocking and unblocking Bluetooth</li>
<li>Connected devices with Browse Files (ObexFTP) support are now displayed in the file dialog's Places panel</li>
");
?>
</ul>

<br clear="all" />
<figure>
<a href="plasma-5.3/touchpad.png">
<img src="plasma-5.3/touchpad-wee.png" style="border: 0px" width="299" height="252" alt="<?php i18n("Configure your Touchpad");?>" />
</a>
<figcaption><?php i18n("Configure your Touchpad");?></figcaption>
</figure>

<p><?php i18n("
A <strong>touchpad configuration module</strong> has been added
");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.3/kicker-contacts.png">
<img src="plasma-5.3/kicker-contacts-wee.png" style="border: 0px" width="519" height="189" alt="<?php i18n("Application Menu can access contacts");?>" />
</a>
<figcaption><?php i18n("Application Menu can access contacts");?></figcaption>
<a href="plasma-5.3/kicker-docs.png">
<img src="plasma-5.3/kicker-docs-wee.png" style="border: 0px" width="249" height="244" alt="<?php i18n("Application Menu can show recent contacts");?>" />
</a>
<figcaption><?php i18n("Application Menu can show recent contacts");?></figcaption>
</figure>

<h3><?php i18n("
Improved Plasma Widgets
");?></h3>

<ul>
<?php i18n("
<li>Clipboard applet gains support for showing barcodes</li>
</li>The Desktop and Folder View containment codebases were
  unified, and have seen performance improvements</li>
<li>The Recent Documents and Recent Applications sections in
  Application Menu (Kicker) are now powered by KDE activities</li>
<li>Comics widget returns</li>
<li>System monitor plasmoids return, such as CPU Load Monitor and Hard Disk usage</li>
");?>
</ul>

<br clear="all" />
<h3 id="pmc"><?php i18n("
Plasma Media Center - Tech Preview
");?></h3>
<figure>
<a href="plasma-5.3/plasma-mediacenter.png">
<img src="plasma-5.3/plasma-mediacenter-wee.png" style="border: 0px" width="400" height="250" alt="<?php i18n("Plasma Media Center");?>" />
</a>
<figcaption><?php i18n("Plasma Media Center");?></figcaption>
</figure>

<p><?php i18n("
<strong>Plasma Media Center</strong> is added as a tech preview in this beta.  It is fully stable but misses a few features compared to version 1.  You can log directly into a Plasma Media Center session if you want to use it on a media device such as a television or projector or you can run it from Plasma Desktop.  It will scan for videos, music and pictures on your computer to let you browse and play them.
");?></p>

<br clear="all" />
<figure>
<a href="plasma-5.3/kwin-nested.png">
<img src="plasma-5.3/kwin-nested-wee.png" style="border: 0px" width="300" height="235" alt="<?php i18n("Plasma is now able to start a nested XWayland server");?>" />
</a>
<figcaption><?php i18n("Plasma is now able to start a nested XWayland server");?></figcaption>
</figure>

<h3><?php i18n("Big Steps Towards Wayland Support")?></h3>
<ul>
<li>
<?php i18n("
Plasma 5.3 makes a huge step towards to <strong>supporting the Wayland windowing system</strong> in addition to the default X11 windowing system. Plasma's window manager and compositor KWin is now able to start a nested XWayland server, which acts as a bridge between the old (X11) and the new (Wayland) world. X11 windows can connect to this server as if it were a normal X server, for KWin it looks like a Wayland window, though. This means that KWin learned to handle Wayland windows in this release, though full integration is only expected for Plasma 5.4.
");?>
</li>

<li><?php i18n("
In addition KWin gained new output modes for Wayland allowing to start a nested KWin on X11 and to start KWin directly on a framebuffer device, which will be the fallback for the case that OpenGL and/or <a href='https://en.wikipedia.org/wiki/Direct_Rendering_Manager'>kernel mode settings</a> are not supported. A rendering backend on kernel mode settings is expected for Plasma 5.4. More information about these new backends and how to test them can be found in <a href='https://community.kde.org/KWin/Wayland'>the KWin wiki pages</a>. Please keep in mind that this is only a development preview and highly experimental new code.
");?></li>
</ul>

<h3><?php i18n("Bug Fixes Galore");?></h3>

<a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;chfield=resolution&amp;chfieldfrom=2015-01-27&amp;chfieldto=Now&amp;known_name=AllPlasma&amp;list_id=1243469&amp;product=Breeze&amp;product=kde-cli-tools&amp;product=kde-gtk-config&amp;product=kded-appmenu&amp;product=kdeplasma-addons&amp;product=khelpcenter&amp;product=khotkeys&amp;product=kinfocenter&amp;product=kio-extras&amp;product=klipper&amp;product=kmenuedit&amp;product=knetattach&amp;product=krunner&amp;product=ksmserver&amp;product=ksplash&amp;product=ksshaskpass&amp;product=kstart&amp;product=ksysguard&amp;product=kwayland&amp;product=kwin&amp;product=kwrited&amp;product=muon&amp;product=Plasma%20Workspace%20Wallpapers&amp;product=plasma-nm&amp;product=plasmashell&amp;product=Powerdevil&amp;product=solid&amp;product=systemsettings&amp;query_based_on=AllPlasma&amp;query_format=advanced&amp;resolution=FIXED">
<p><?php i18n("
348 bugs were fixed</a> giving fewer crashes and more reliable use.</p>
");?></p>



<a href="plasma-5.2.2-5.2.95-changelog.php">
<?php i18n("Full Plasma 5.2.95 changelog");?></a>

<!-- // Boilerplate again -->

<br clear="all" />
<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try out Plasma is the with a live image booted off a USB disk.
");?></p>

<ul>
<li>
<?php print i18n("<a href='https://community.kde.org/Plasma/Live_Images'>Plasma Live Images</a>"
);?>
</li>
</ul>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='https://community.kde.org/Plasma/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
