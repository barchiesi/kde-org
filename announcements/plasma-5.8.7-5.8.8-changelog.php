<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.8.8 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.8.8";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Applet: Rename MediaPlayer to MediaPlayerItem. <a href='https://commits.kde.org/bluedevil/94ff9f296b6e9105e2deb07abaca3fc4ecd2f8cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6487'>D6487</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fix crash when using Discover during updates. <a href='https://commits.kde.org/discover/aa12b3c822354e0f526dccff7f1c52c26fb35073'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370906'>#370906</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Try to read CPU clock from cpufreq/scaling_cur_freq instead of /proc/cpuinfo. <a href='https://commits.kde.org/ksysguard/cbaaf5f4ff54e20cb8ec782737e04d540085e6af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382561'>#382561</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8153'>D8153</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Set focus in this FocusScope. <a href='https://commits.kde.org/plasma-desktop/001c89d18ab737dd48451c73137004134fdc51b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383667'>#383667</a></li>
<li>Fix up botched patch. <a href='https://commits.kde.org/plasma-desktop/83fee948950f694aef36fea8866f6700d806141b'>Commit.</a> </li>
<li>Disable bouncy scrolling. <a href='https://commits.kde.org/plasma-desktop/49fb504149e3ea3690616510ee313d33c7233d53'>Commit.</a> See bug <a href='https://bugs.kde.org/378042'>#378042</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7322'>D7322</a></li>
<li>Fix group dialog no longer resizing/closing as windows are closed and the group is dissolved. <a href='https://commits.kde.org/plasma-desktop/a1e2ddd67a1226aa796c70fdfce3a8cf81a3f2e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382638'>#382638</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7035'>D7035</a></li>
<li>Don't define behaviors for the same properies in 2 places. <a href='https://commits.kde.org/plasma-desktop/30ebb82b11b2ec696f56ec77b59fb2b489d01b19'>Commit.</a> </li>
<li>Backport fix from e5df3ded85c94f0a33afe12b18e6afad96f12639. <a href='https://commits.kde.org/plasma-desktop/6f3eaef11b1dad53ed8e3e1b4ab6bed13447f6e4'>Commit.</a> </li>
<li>Fix DND onto Task Manager for groups, group dialog scrollbar. <a href='https://commits.kde.org/plasma-desktop/84300b516b799fae94fefd7a9a418e5584d37987'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379888'>#379888</a>. Fixes bug <a href='https://bugs.kde.org/379037'>#379037</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6231'>D6231</a></li>
<li>Remove implicit use of QtQml in Qt 5.7. <a href='https://commits.kde.org/plasma-desktop/89a08418b3e8815a6eb610067f521cee6218b8b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380240'>#380240</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6007'>D6007</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Device Notifier] Set preferred size for ActionItem. <a href='https://commits.kde.org/plasma-workspace/b8f263cd9939530a6634a1ddb9533bf473a67805'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382541'>#382541</a></li>
<li>Fix QSortFilterProxyModelPrivate::updateChildrenMapping crash in libtaskmanager. <a href='https://commits.kde.org/plasma-workspace/d2f722a82ebeb213a89efc209ec726a8188de6f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381006'>#381006</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7139'>D7139</a></li>
<li>[Battery Monitor] Fix icon size for InhibitionHint. <a href='https://commits.kde.org/plasma-workspace/03f9ab0f9149050541d2852f94b508f746aac955'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383213'>#383213</a></li>
<li>[Klipper ModelTest] Fix build. <a href='https://commits.kde.org/plasma-workspace/94f943b3ece7a5b14f9377ec5b3c43b55e9ac1c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6874'>D6874</a></li>
<li>[TasksModel] Use LauncherUrlWithoutIcon in move method. <a href='https://commits.kde.org/plasma-workspace/cc2f28ece16a8e83703ca0d1a074410f62d1c130'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6872'>D6872</a></li>
<li>[LauncherTasksModel] Emit dataChanged for LauncherUrlWithoutIcon on sycoca change. <a href='https://commits.kde.org/plasma-workspace/5bac69e51657dd3254d3aaf3cd6a543cd3079401'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6869'>D6869</a></li>
<li>[Notification Item] Enforce PlainText for summary. <a href='https://commits.kde.org/plasma-workspace/ddc530ff14b61f3a508d53116f2b2cd7cb585b29'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6671'>D6671</a></li>
<li>[Notifications] Check for corona to avoid crash. <a href='https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378508'>#378508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6653'>D6653</a></li>
<li>Make attaching backtraces to existing bug reports work. <a href='https://commits.kde.org/plasma-workspace/774a23fc0a7c112f86193ee6e07947fee6282ef4'>Commit.</a> </li>
<li>Don't show logout when kauthorized doesn't want it. <a href='https://commits.kde.org/plasma-workspace/67904e212c4fde5870015c3b2bc3af41e648eb6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380344'>#380344</a></li>
<li>[TaskGroupingProxyModel] Use LauncherUrlWithoutIcon. <a href='https://commits.kde.org/plasma-workspace/8b39e1a5597681edec2daa3185b83725c4d0fbea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5581'>D5581</a></li>
<li>Always add data sources in SystemMonitorEngine::sourceRequestEvent(). <a href='https://commits.kde.org/plasma-workspace/f060ad5f1ad06359088887cb2f7e691c994ccfc5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380209'>#380209</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5973'>D5973</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Session file parser: Support sections and respect the Hidden property. <a href='https://commits.kde.org/sddm-kcm/992143c4bf5ad5219a20cc7c56efea5c941ef79b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381982'>#381982</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6626'>D6626</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
