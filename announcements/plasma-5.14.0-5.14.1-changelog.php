<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.14.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.14.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Snap: no need to have a notifier. <a href='https://commits.kde.org/discover/252558c13d57e3530a3dac5f3b2b700c61ba059d'>Commit.</a> </li>
<li>Expose sort by relevance to the UI. <a href='https://commits.kde.org/discover/5a01d44da1b08c2da5537bb5c48211b051b690db'>Commit.</a> See bug <a href='https://bugs.kde.org/399502'>#399502</a></li>
<li>Remove code that isn't used anymore. <a href='https://commits.kde.org/discover/4e96ed19e6a43337f3c2d0352398d7ac3dd6c28b'>Commit.</a> </li>
<li>Debugging: allow to pass just the backend name from the CLI. <a href='https://commits.kde.org/discover/4e7b78598a5426ded92c7fa0697d4cec63e84f2e'>Commit.</a> </li>
<li>Remove unused file. <a href='https://commits.kde.org/discover/1e62eb7db974e81c699e6c8766288ae517f5f133'>Commit.</a> </li>
<li>Flatpak: initialise the no-sources item as we construct the backend. <a href='https://commits.kde.org/discover/a1eb2839bc883bdf80751aad4ffc59bf19547aa0'>Commit.</a> </li>
<li>Remove unneeded code. <a href='https://commits.kde.org/discover/a28b86f4f00144e122540689e95e9f9af3dd067e'>Commit.</a> </li>
<li>Flatpak: make sure it's listed in the sources page. <a href='https://commits.kde.org/discover/3f17292dd3c1940dbcc5e585ae97bf7ee350095b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399718'>#399718</a></li>
<li>Pk: improve dependencies calculation. <a href='https://commits.kde.org/discover/813baa7e92ad7b688f5101ab43873ca7b5a74470'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399687'>#399687</a></li>
<li>[Discover Notifier] Open Updates page in notification. <a href='https://commits.kde.org/discover/f237e7c0ccc87d4c9ebc49b68736e98a5012e1f4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16152'>D16152</a></li>
<li>Pk: properly integrate sources details. <a href='https://commits.kde.org/discover/93da4cb91d700ff9f26d13c2a7472db308e8fe65'>Commit.</a> </li>
<li>Make sure the update process doesn't end before starting. <a href='https://commits.kde.org/discover/06f40140f594d816103118c9a2a3e3c69cb2efb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398078'>#398078</a></li>
<li>Kns: fix installing resources without linkids. <a href='https://commits.kde.org/discover/ea566a55d59be4a51fcb3f6493b1176c0fba1f52'>Commit.</a> </li>
<li>Fwupd: fix source management with older versions of fwupd. <a href='https://commits.kde.org/discover/b46d113aaf6852cdb5a62e8e78c20562768e5a3e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399653'>#399653</a></li>
<li>Snap: show the installed snaps when selecting the source. <a href='https://commits.kde.org/discover/ba4cb9a8397472cea54765157542e51759195485'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388912'>#388912</a></li>
<li>Fwupd: provide the origin when it's available. <a href='https://commits.kde.org/discover/b694fab9d4aa8b6fcd36e6aefc80879bcb2551cb'>Commit.</a> </li>
<li>Fwupd: don't show the same url for everything. <a href='https://commits.kde.org/discover/71f1f7c727abfa70e312544a289fef44341f9672'>Commit.</a> </li>
<li>Fwupd: return query results even if the search is empty. <a href='https://commits.kde.org/discover/7a734b3a3fe5ba22a70e74273194999b1895b217'>Commit.</a> </li>
<li>Fwupd: remove isTechnical configurability. <a href='https://commits.kde.org/discover/6231dd889a0a253a6d0cfd084345fcc6806ecb01'>Commit.</a> </li>
<li>Fwupd: consistent icon initialisation. <a href='https://commits.kde.org/discover/e9b2788266525f9c703ccc4282d43b8d4d2c3756'>Commit.</a> </li>
<li>Fwupd: don't leak resources. <a href='https://commits.kde.org/discover/1a6d546df4c4b47eff816a845bdc472c6484dc46'>Commit.</a> </li>
<li>Flatpak: wait for threads to finish before closing completely. <a href='https://commits.kde.org/discover/9a8f22b373095cabdb1c033709ac4c8d22ed5ea7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396730'>#396730</a></li>
<li>Fwupd: move blocking dbus call into the initialization thread. <a href='https://commits.kde.org/discover/2623dae7b9ff2f89887f8e9a5f3268a5a57deedd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399341'>#399341</a></li>
<li>Fwupd: move fetching of updates into a separate thread. <a href='https://commits.kde.org/discover/1e138d8014c8e6439c56f14a5c1e8cb765068d90'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399604'>#399604</a></li>
<li>Pk: Don't mark local packages as installed upon simulation. <a href='https://commits.kde.org/discover/b0867794275ba7957ab4071ae272665f285160e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397960'>#397960</a></li>
<li>Pk: fix upgrades on fedora. <a href='https://commits.kde.org/discover/074bc5e77ffef088e517ddf61e6ba36544677975'>Commit.</a> </li>
<li>Fwupd: remove false negative. <a href='https://commits.kde.org/discover/bb94101b783c5107d77395ac4581a314fd88c62f'>Commit.</a> </li>
<li>Never lie about flatpak being present when it's actually present. <a href='https://commits.kde.org/discover/1aa02b211b4cd5403c76bf21cbd467bc6b597c96'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399463'>#399463</a></li>
<li>Flatpak: add a fallback when the resource in the flatpakref file is already in. <a href='https://commits.kde.org/discover/344197f6cafeb6137af85475a4d3421ba9ce0539'>Commit.</a> See bug <a href='https://bugs.kde.org/399463'>#399463</a></li>
<li>Snap: Fix permissions logic, make sure user sees errors when they happen. <a href='https://commits.kde.org/discover/32e481b5c1ddee8f3c01b13ee07fdcf3d0d5cefa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399464'>#399464</a></li>
<li>Snap: open the resource rather than list it. <a href='https://commits.kde.org/discover/e4417ba9f2e8f2bbfdadc43d485efdeda1091c95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399466'>#399466</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Media Frame] Fix enabling Apply button when adding/removing paths. <a href='https://commits.kde.org/kdeplasma-addons/2bf932fe02cea35f5413ef7793ecd0179b45b18d'>Commit.</a> </li>
<li>[KonsoleProfiles applet] Fix navigating with the keyboard. <a href='https://commits.kde.org/kdeplasma-addons/91ee9cc72a490ccaf931c49229a9b8d2303b8e65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15877'>D15877</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[kcmkwin/ruleswidget] Disable "Detect" button when countdown is running. <a href='https://commits.kde.org/kwin/980e390743c153966dd364f4a0ebaadd8d1b03a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399644'>#399644</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16124'>D16124</a></li>
<li>[effects/diminactive] Delete active transitions when window is deleted. <a href='https://commits.kde.org/kwin/396f8f558c07e4c8b0d3090ebe5f65fab1d98f5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399612'>#399612</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16130'>D16130</a></li>
<li>[activities] Fix logic error in user menu blocking activity updates. <a href='https://commits.kde.org/kwin/273a3fabd0ee7aa1321c4636dc29e4c9b29fa5f1'>Commit.</a> See bug <a href='https://bugs.kde.org/335725'>#335725</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16101'>D16101</a></li>
<li>[qpa] Always keep a at least one screen. <a href='https://commits.kde.org/kwin/6724955a762413bcc6b87e4909eadab593376f2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399564'>#399564</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16096'>D16096</a></li>
<li>[effects/wobblywindows] Fix visual artifacts caused by maximize effect. <a href='https://commits.kde.org/kwin/c2ffcfdc218c94fc93e1960a8760883eef4e4495'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370612'>#370612</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15961'>D15961</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Focus handling fixes. <a href='https://commits.kde.org/plasma-desktop/480962c3ff93f9d3839a426b9ac053399d03e38d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399566'>#399566</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16106'>D16106</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Openconnect: add missing dependency Qt5Xml. <a href='https://commits.kde.org/plasma-nm/6919a84c70d83ce60d55bfdbcbda3a7267581ab3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16073'>D16073</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix build with gpsd 3.18. <a href='https://commits.kde.org/plasma-workspace/81a0fe6db9ac9044c2ec8a246878ec65651cb6dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16081'>D16081</a></li>
<li>[Global Menu Applet] Fix menu layout option being always disabled. <a href='https://commits.kde.org/plasma-workspace/824279fce90413d345ae0bda602e29611d7c0c30'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16149'>D16149</a></li>
<li>[Device Notifier] Auto-hide popup after device is unmounted. <a href='https://commits.kde.org/plasma-workspace/581889351e096b5b9190945ec24c10e516db30be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16045'>D16045</a></li>
<li>[Slideshow Wallpaper] Fix translation of context menu actions. <a href='https://commits.kde.org/plasma-workspace/a40d95212f17634c76e7055b005837272e913859'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16037'>D16037</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Improve debug and don't leak gbm handle. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ab4ee68e9822245786d017d6b813af64c04a9093'>Commit.</a> </li>
<li>Make sure we don't process more frames then configured framerate. <a href='https://commits.kde.org/xdg-desktop-portal-kde/dbabcd60f33ce970728d508e5b5d3e5825935330'>Commit.</a> </li>
<li>ScreenCast: properly return failure when user cancel the dialog. <a href='https://commits.kde.org/xdg-desktop-portal-kde/15fdb17e37e047eedb50238fa3f976a5151df907'>Commit.</a> </li>
<li>Make initialization of drm and egl non-fatal. <a href='https://commits.kde.org/xdg-desktop-portal-kde/24c6010aee6f0d48d837d57736184a5024a956fe'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
