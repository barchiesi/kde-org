<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Plasma 5.18 LTS Beta: More Convenient and with Long Term Stability",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.17.90'; // for i18n
    $version = "5.17.90";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">


    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", "5.18 LTS Beta")?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>

    <!--
    <figure class="videoBlock d-flex">
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" frameborder="0" allowfullscreen></iframe>
    </figure>
    -->

    <figure class="topImage">
        <a href="plasma-5.18/plasma-5.18.png" data-toggle="lightbox">
            <img src="plasma-5.18/plasma-5.18-wee.png" height="450" width="800" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.18 LTS Beta" />
        </a>
        <figcaption><?php print i18n_var("KDE Plasma %1", "5.18 LTS Beta")?></figcaption>
    </figure>

    <p><?php print i18n("Thursday, 16 January 2020.") ?></p>

    <p><?php i18n("The Plasma 5.18 LTS Beta is out!")?></p>

    <p><?php i18n("This new version of your favorite desktop environment adds neat new features that make your life easier, including clearer notifications, streamlined settings for your system and the desktop layout, much improved GTK integration, and more. Plasma 5.18 is easier and more fun, while at the same time allowing you to do more tasks faster.")?></p>

    <p><?php i18n("Apart from all the cool new stuff, Plasma 5.18 also comes with LTS status. LTS stands for \"Long Term Support\" and this means 5.18 will be updated and maintained by KDE contributors for the next couple of years (regular versions are maintained for 4 months). So, if you are thinking of updating or migrating your school, company or organization to Plasma, this version is your best bet. You get the most recent stable version of Plasma for the long term.")?></p>

    <p><?php i18n("Read on to discover everything that is new in Plasma 5.18 LTS…"); ?></p>

    <h2 id="desktop"><?php i18n("Plasma");?></h2>

    <figure class="float-right text-right">
        <a href="plasma-5.18/emoji.png" data-toggle="lightbox">
            <img src="plasma-5.18/emoji-wee.png" class="border-0 img-fluid" width="350" height="266" alt="<?php i18n("Emoji Selector");?>" />
        </a>
        <figcaption><?php i18n("Emoji Selector");?></figcaption>
        <br />
        <br />
        <a href="plasma-5.18/customize-layout.png" data-toggle="lightbox">
            <img src="plasma-5.18/customize-layout-wee.png" class="border-0 img-fluid" width="350" height="197" alt="<?php i18n("Customize Layout Global Settings");?>" />
        </a>
        <figcaption><?php i18n("Customize Layout Global Settings");?></figcaption>
        <br />
        <!--
        <a href="plasma-5.18/do-not-disturb.png" data-toggle="lightbox">
            <img src="plasma-5.18/do-not-disturb-wee.png" class="border-0 img-fluid" width="350" height="306" alt="<?php i18n("Do Not Disturb Shortcut");?>" />
        </a>
        <figcaption><?php i18n("Do Not Disturb Shortcut");?></figcaption>
        <br />
        -->
        <a href="plasma-5.18/gtk-csd-theme.png" data-toggle="lightbox">
            <img src="plasma-5.18/gtk-csd-theme.png" class="border-0 img-fluid" width="350" height="237" alt="<?php i18n("GTK Apps with CSD and Theme support");?>" />
        </a>
        <figcaption><?php i18n("GTK Applications with CSDs and Theme Integration");?></figcaption>
        <br />
        <a href="plasma-5.18/night-color-applet.png" data-toggle="lightbox">
            <img src="plasma-5.18/night-color-applet.png" class="border-0 img-fluid" width="350" height="153" alt="<?php i18n("Night Color System Tray Widget");?>" />
        </a>
        <figcaption><?php i18n("Night Color System Tray Widget");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("Emoji Selector that  can be opened through the application launcher or with the <kbd>Meta</kbd> + <kbd>.</kbd> keyboard shortcut"); ?></li>
<li><?php i18n("New global edit mode which replaces the desktop toolbox button and lets you easily customize your desktop layout"); ?></li>
<li><?php i18n("Improved touch-friendliness for the Kickoff application launcher and widget editing"); ?></li>
<li><?php i18n("Support for GTK applications which use Client Side Decorations, adding proper shadows and resize areas for them"); ?></li>
<li><?php i18n("GTK apps now also automatically inherit Plasma's settings for fonts, icons, cursors and more."); ?></li>
<li><?php i18n("There's a new System Tray widget for toggling the Night Color feature and by default it automatically appears when it's on"); ?></li>
<li><?php i18n("More compact design to choose the default audio device in the Audio Volume System Tray widget"); ?></li>
<li><?php i18n("Clickable volume indicator and tooltip item highlight indicators in the Task Manager"); ?></li>
<li><?php i18n("Circular Application Launcher menu user icon"); ?></li>
<li><?php i18n("Option to hide the lock screen clock"); ?></li>
<li><?php i18n("It's now possible to configure keyboard shortcuts that turn Night Color and Do Not Disturb mode on or off"); ?></li>
<li><?php i18n("Windy conditions shown in weather widget"); ?></li>
    </ul>

<br clear="all" />

    <h2 id="notifications"><?php i18n("Notifications");?></h2>

    <figure class="float-right text-right">
        <a href="plasma-5.18/draggable-download.png" data-toggle="lightbox">
            <img src="plasma-5.18/draggable-download-wee.png" class="border-0 img-fluid" width="350" height="144" alt="<?php i18n("Draggable Download File Icon");?>" />
        </a>
        <figcaption><?php i18n("Draggable Download File Icon");?></figcaption>
        <br />
        <a href="plasma-5.18/bluetooth-battery.png" data-toggle="lightbox">
            <img src="plasma-5.18/bluetooth-battery-wee.png" class="border-0 img-fluid" width="350" height="172" alt="<?php i18n("Low Bluetooth Battery");?>" />
        </a>
        <figcaption><?php i18n("Bluetooth Device Battery Low Notification");?></figcaption>
        <br />
    </figure>

<ul>
<li><?php i18n("The timeout indicator on notification popups has been made circular and surrounds the close button"); ?></li>
<li><?php i18n("A draggable icon in the \"file downloaded\" notification has been added, so you can quickly drag it to places"); ?></li>
<li><?php i18n("Plasma now shows you a notification warning when a connected Bluetooth device is about to run out of power"); ?></li>
</ul>
<br clear="all" />

    <h2 id="system-settings"><?php i18n("System Settings");?></h2>

    <figure class="float-right text-right">
        <a href="plasma-5.18/user-feedback.png" data-toggle="lightbox">
            <img src="plasma-5.18/user-feedback-wee.png" class="border-0 img-fluid" width="350" height="253" alt="<?php i18n("User Feedback");?>" />
        </a>
        <figcaption><?php i18n("User Feedback");?></figcaption>
        <a href="plasma-5.18/application-style.png" data-toggle="lightbox">
            <img src="plasma-5.18/application-style-wee.png" class="border-0 img-fluid" width="350" height="253" alt="<?php i18n("Application Style");?>" />
        </a>
        <figcaption><?php i18n("Application Style");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("Plasma gained optional User Feedback settings (disabled by default), allowing you to give us detailed system information and statistics on how often individual features of Plasma you use"); ?></li>
<li><?php i18n("Added a slider for the global animation speed"); ?></li>
<li><?php i18n("Redesigned Application Style settings with a grid view"); ?></li>
<li><?php i18n("Improved the search in the system settings sidebar"); ?></li>
<li><?php i18n("An option to scroll to clicked location in the scrollbar track has been added"); ?></li>
<li><?php i18n("The System Settings Night Color page has a clearer user interface now"); ?></li>
</ul>
<br clear="all" />
    
    <h2 id="discover"><?php i18n("Discover");?></h2>

    <figure class="float-right text-right">
        <a href="plasma-5.18/discover-comments.png" data-toggle="lightbox">
            <img src="plasma-5.18/discover-comments-wee.png" class="border-0 img-fluid" width="350" height="302" alt="<?php i18n("Reading and Writing Review Comments");?>" />
        </a>
        <figcaption><?php i18n("Reading and Writing Review Comments");?></figcaption>
    </figure>

    <ul>
    <li><?php i18n("Discover's default keyboard focus has been switched to the search field"); ?></li>
    <li><?php i18n("It's now possible to search for add-ons from the main page"); ?></li>
    <li><?php i18n("Added nested comments for addons"); ?></li>
    <li><?php i18n("Made design improvements to the sidebar header and reviews"); ?></li>
    </ul>

<br clear="all" />

    <h2 id="more"><?php i18n("More");?></h2>

    <figure class="float-right text-right">
        <a href="plasma-5.18/nvidia.png" data-toggle="lightbox">
            <img src="plasma-5.18/nvidia-wee.png" class="border-0 img-fluid" width="350" height="289" alt="<?php i18n("NVIDIA GPU stats");?>" />
        </a>
        <figcaption><?php i18n("NVIDIA GPU stats");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("Decreased the amount of visual glitches in apps when using fractional scaling on X11"); ?></li>
<li><?php i18n("Made it possible to show NVIDIA GPU stats in KSysGuard"); ?></li>
    </ul>

<br clear="all" />

    <h2 id="lts"><?php i18n("New Since 5.12 LTS");?></h2>

    <p><?php i18n("For those upgrading from our previous Long Term Support release here are some of the highlights from the last two years of development:"); ?></p>

    <ul>
<li><?php i18n("Completely rewritten notification system"); ?></li>
<li><?php i18n("Plasma Browser Integration"); ?></li>
<li><?php i18n("Many redesigned system settings pages, either using a consistent grid view or just an overhauled interface"); ?></li>
<li><?php i18n("Global menu support for GTK applications"); ?></li>
<li><?php i18n("Display Management improvements including new OSD and widget"); ?></li>
<li><?php i18n("Flatpak portal support"); ?></li>
<li><?php i18n("Night Color feature"); ?></li>
<li><?php i18n("Thunderbolt Device Management"); ?></li>
    </ul>
    
    <a href="plasma-5.17.5-5.17.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.18 LTS Beta"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');


