<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.8.2 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.8.2';
include "header.inc";
?>
<p><a href="plasma-5.8.2.php">Plasma 5.8.2</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='kactivitymanagerd' href='http://quickgit.kde.org/?p=kactivitymanagerd.git'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Backporting the fixes from master. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=5b42e9e7d57af6d19b1ade9c05beaf97c8f31d17'>Commit.</a> </li>
<li>Ensuring proper activity loading when the config file is not complete. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=127cb1df00e8b42aed758c373608231aa1290ba9'>Commit.</a> </li>
<li>Renamed the configuration transition checker to be explicit about KDE4. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=7e14df1d7b61b7ce61ca3f3aaff052a4b1b346a8'>Commit.</a> </li>
<li>Sync the dbus ActivityInfo structure with the framework. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=d51d86a83d122d7b4b926eb8c1fa70bdc0864c4f'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Disable unify button when only one output is connected. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=6dcc9b19be52fdf5b4c8148124758f7392646719'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360700'>#360700</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Properly specify libdrm dependency. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f642eb9d9dd8a2689866bf3a5f7c2ee302991d36'>Commit.</a> </li>
<li>[tabbox] Intercept QWheelEvents on QQuickWindow for scrolling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e9d20b80e9a79f514a219357d991092d83665350'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369661'>#369661</a></li>
<li>Fix shortcut triggering with shift+letter. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=78a2732a9a421b8554022ec5edcca30d95b45d1a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370341'>#370341</a></li>
<li>[kwinrules] Hide all autogroup related widgets. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=db95b96854e50b9182ea6e01a3806099ef378cb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370301'>#370301</a></li>
<li>Support LEDs in Xkb and libinput. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4c7752c965ab9e10ddb18b10077bdb4cdd52df6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369214'>#369214</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Disconnect instead. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7b2f1624126f1a014f7aa30c31d0964efe028935'>Commit.</a> </li>
<li>Test the QMetaObject::Connection instead of using Qt::UniqueConnection. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d888c454192ee5c5683c42f40be1193692376ef5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370516'>#370516</a></li>
<li>[Colors KCM] Father theme is reset while saving new theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=04ed0dbd2c3de5ec27337411c68f2f254935f729'>Commit.</a> </li>
<li>Fix "Default" color scheme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=613194c293b63004af5fc43762b92bd421ddf5b6'>Commit.</a> </li>
<li>Let kicker display executable items instead of empty buttons. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1d2984f8001de28cf25671d13a4be4b8322ec929'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>When importing an OpenVPN connection set default passwords to be stored in KWallet. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=3df939ea5db4f2b51cbf6f6001047f0fc14fa104'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Update SDDM theme preview. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=647e254d34d4664754a21985a89998edeee911f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370490'>#370490</a></li>
<li>Remove unused test asset. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e906cd86a08218db7def449bbccc2804752901d0'>Commit.</a> </li>
<li>Restore all panel properties. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aea33cddb547cc2ba98be5dd45dc7562b32b4b9a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368074'>#368074</a>. Fixes bug <a href='https://bugs.kde.org/367918'>#367918</a></li>
</ul>


<?php
  include("footer.inc");
?>
