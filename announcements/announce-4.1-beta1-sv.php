<?php
  $page_title = "KDE 4.1 Beta1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<?php
  $release = '4.1-beta1';
  include "announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Första betaversionen av KDE 4.1 lanserad
</h3>

<p align="justify">
  <strong>
KDE-projektet lanserar första betaversionen av KDE 4.1.</strong>
</p>

<p align="justify">
29 maj, 2008 (INTERNET).  
<a href="http://www.kde.org/">KDE Projektet</a> tillkännager stolt den första
betaversionen av KDE 4.1. Beta 1 är till för testare, projektmedlemmar och andra
entusiaster för att identifiera fel och regressioner, så att KDE 4.1 äntligen
kan ersätta KDE 3 för slutanvändare. KDE 4.1 beta 1 finns tillgänglig som binära
paket för ett stort antal plattformar och även som källkodspaket. Den slutliga versionen
av KDE 4.1 är planerad att släppas under juli 2008.
</p>


<h4>
  <a name="changes">KDE 4.1 Beta 1 höjdpunkter</a>
</h4>

<p align="justify">
<ul>
    <li>Skrivbordet har fått en kraftigt förbättrad funktionalitet och konfigurerbarhet.
    </li>
    <li>KDE's personliga kommunikationssvit har blivit portad till KDE 4.
    </li>
    <li>Många nya och nyportade applikationer.
    </li>
</ul>
</p>

<h4>
  Plasma växer upp
</h4>
<p align="justify">
Plasma, det innovativa nya systemet som skapar menyer, paneler och skrivbordet
som utgör skrivbordet har mognat snabbt. Det stöder nu multipla och flexibla paneler
vilket ger användaren möjlighet att att själva komponera sin miljö på ett så
flexibelt sätt som möjligt. Applikationsmenyn Kickoff, har blivit uppdaterad med
ett nytt rent utseende och många optimeringar. Kör program dialogen har fått en rejäl
omskrivning för att ge användare möjlighet att snabbt starta program, öppna
dokument och öppna sajter. Den kompositerande fönsterhanteraren har fått flera 
prestandaförbättringar för att ge bättre ergonomi och effekter inklusive den nya
Cover Switch alt-tab effekten och wobbly windows.
</p>
<div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-krunner.png">
						<img width="310" height="235" src="announce_4.1-beta1/plasma-krunner-small.png" title="Den nya KRunner Alt-F2 dialogen" alt="Den nya KRunner Alt-F2 dialogen"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/plasma-panelcontroller.png">
						<img width="462" height="84" src="announce_4.1-beta1/plasma-panelcontroller-small.png" title="Panelhanteringen återvänder" alt="Panelhanteringen återvänder"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-coverswitch.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-coverswitch-small.png" title="KWin:s Cover Switch effekt" alt="KWin:s Cover Switch effekt"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly1.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly1-small.png" title="Wobblande fönster" alt="Wobblande fönster"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/kwin-wobbly2.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly2-small.png" title="Fler wobblande fönster" alt="Fler wobblande fönster"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
  Kontact återvänder
</h4>
<p align="justify">
Kontact, KDE kommunikationssvit och dess associera verktyg har blivit portade till
KDE 4 och släpps därmed för första gången med KDE 4.1. Många funktioner från Enterprise
grenen av KDE 3 har inkluderats vilket gör Kontact än mer användbar i affärssammanhang. 
Förbättringar inkluderar nya komponenter som KTimeTracker och KJots för hantering av 
anteckningar, ett nytt utseende, bättre stöd för multipla kalendrar och tidszoner och en 
mer robust eposthantering.</p>
<center>
<a href="announce_4.1-beta1/kontact-calendar.png">
<img width="351" height="275" src="announce_4.1-beta1/kontact-calendar-small.png" title="Multipla kalendrar" alt="Multipla kalendrar"/>
</a>
<a href="announce_4.1-beta1/kontact-kjots.png">
<img width="351" height="271" src="announce_4.1-beta1/kontact-kjots-small.png" title="KJots" alt="KJots"/>
</a>
</center>

<h4>
KDE 4 applikationerna växer
</h4>
<p align="justify">
Över hela KDE gemenskapen har flera applikationer nu portats över till KDE 4
eller har förbättrats sedan KDE 4 lanserades. Dragon Player den snabba videospelaren
debuterar. KDE:s CD-spelare återvänder. En ny utskriftsapplet ger överlägset stöd för
att styra sin utskrifter. Konqueror får stöd för sessioner, ett Undoläge och 
förbättrad scrollning. Ett nytt läge för bildvisning inklusive ett fullskärmsläge
för Gwenview. Dolphin får stöd för flikar och ett antal andra funktioner från KDE 3.
Många applikationer har fått stöd för att hämta nya teman, ikoner m.m via Get New Stuff
som har fått ett nytt förbättrat gränssnitt. Stöd för Zeroconf nätverkshantering har
lagts till till flera spel och andra applikationer för att förenkla hanteringen för 
att sätta upp nätverksspel eller fjärråtkomst.
</p>
<div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/dolphin-treeview.png">
						<img width="265" height="234" src="announce_4.1-beta1/dolphin-treeview-small.png" title="Dolphin:s trädvy" alt="Dolphin:s trädvy"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/dragonplayer.png">
						<img width="442" height="317" src="announce_4.1-beta1/dragonplayer-small.png" title="Dragon mediaspelare" alt="Dragon mediaspelare"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/games-kdiamond.png">
						<img width="264" height="305" src="announce_4.1-beta1/games-kdiamond-small.png" title="Pusselspelet KDiamond" alt="Pusselspelet KDiamond"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/games-kubrick.png">
						<img width="215" height="200" src="announce_4.1-beta1/games-kubrick-small.png" title="80-talet på ditt skrivbord" alt="80-talet på ditt skrivbord"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/konqueror.png">
						<img width="302" height="294" src="announce_4.1-beta1/konqueror-small.png" title="Konqueror" alt="Konqueror"/>
				</a>
		</div>
		<div style='float:left;padding-bottom:10px;'>
				<a href="announce_4.1-beta1/marble-openstreetmap.png">
						<img width="324" height="281" src="announce_4.1-beta1/marble-openstreetmap-small.png" title="Marble med OpenStreetMaps" alt="Marble med OpenStreetMaps"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
Förbättringar av ramverken
</h4>
<p align="justify">
Utvecklarna har lagt ner mycket tid på att utöka och förbättra 
KDE:s kärnbibliotek. KHTML får en prestandraförbättring genom 
att inkludera 

Developers have been busy enriching the core KDE libraries and 
infrastructure too.  KHTML gets a speed boost from anticiperande resursladdning, 
och WebKit, dess avkomma, läggs till Plasma för att ge stöd för OSX 
Dashboard widgets i KDE.  Widgets on Canvas stödet i Qt 4.4  gör Plasma
mer stabilt och lättviktigt.  KDE:s karakteristiska en-klicks baserade gränssnitt 
får en ny markeringsmekanism för ökad hastighet och tillgänglighet.  Phonon det plattformsoberoende
mediaramverket får stöd för undertexter och GStreamer, DirectShow 9 and QuickTime 
implementationer.  Nätverkshantering får stöd för flera versioner av NetworkManager.  
Stöd för flera freedesktop.org standarder såsom popupnotifiering och bokmärken har påbörjats
så att andra applikationer passar in i KDE 4.1 skrivbordsmiljö.
</p>

<h4>
  KDE 4.1 slutlig lansering
</h4>
<p align="justify">
KDE 4.1 är planerad att släppas den 29 juli 2008, sex månader efter KDE 4.0.
</p>

<h4>Installera KDE 4.1 beta 1</h4>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.1 beta 1 och i 
vissa fall har andra personer i omgivningaen skapat paket.
  <ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> has KDE 4.1beta1 in <em>experimental</em>.</li>
<li><em>Kubuntu</em> packages are in preparation.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>
</p>


<h4>
  Kompilera KDE 4.1 beta 1 (4.0.80) själv
</h4>
<p align="justify">
  <a name="source_code"></a><em>Källkod</em>.
  All källkod till KDE 4.1 beta 1 kan laddas ner  <a
href="http://www.kde.org/info/4.0.80.php">härifrån</a>.
Instruktioner hur man kompilerar och installerar KDE 4.0.80 finns på
<a href="/info/4.0.80.php">KDE 4.0.80 Info Page</a>, eller på 
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>


<h4>
  Stöd KDE
</h4>
<p align="justify">
 KDE är ett <a href="http://www.gnu.org/philosophy/free-sw.html">Fri Mjukvaru</a>
projekt som existerar och växer endast tack vare hjälp från volontärer som donerar
sin tid och sitt arbete. KDE behöver alltid mer hjälp från frivilliga vare sig det
gäller kodning, buggrättning eller rapportering, hjälp med dokumentation, översättning,
marknadsföring, ekonomisk hjälp etc. All hjälp uppskattas och accepteras gladeligen. 
Se <ahref="/community/donations/">Stöd KDE</a> för mer information. </p>

<p align="justify">
Vi ser fram emot att få höra av dig snart!
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Presskontakt</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
