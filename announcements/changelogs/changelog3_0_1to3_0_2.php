<?php
  $page_title = "KDE 3.0.1 to 3.0.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.0.1 and 3.0.2 releases.
</p>
<p>
Please see the <a href="changelog3_0to3_0_1.html">3.0 to 3.0.1 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
<li>Again Build fixes.</li>
<li>Realtime priority configuration fix</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li>dcop: Make it possible to disable the Qt bridge.</li>
<li>make sure tooltip effects can be configured.</li>
<li>kbuildsycoca: avoid crash on unreadable directories.</li>
<li>kdockwidget: fixing reading of configuration.</li>
<li>kedittoolbar: fix leak.</li>
<li>khtml: many fixes.</li>
<li>kded: signal handling fixes.</li>
<li>kio-http slave: fixes related to form posts and cache handling.</li>
<li>kjs: added max recursion limit.</li>
<li>kjs: leak and compatibility fixes.</li>
</ul>

<h3>kdeaddons</h3>
<!-- <ul>
</ul> -->

<h3>kdeadmin</h3>
<ul>
</ul>

<h3>kdeartwork</h3>
<ul>
</ul>

<h3>kdebase</h3>
<ul> 
<li>kicker: &amp;-escaping fix</li>
<li>audiocd: fix cddb computation</li>
<li>thumbnail previewer: calculate the number of bytes to read instead of hardcoding it.</li>
</ul>

<h3>kdebindings</h3>
<ul>
</ul>

<h3>kdegames</h3>
<!-- <ul>
</ul> -->

<h3>kdegraphics</h3>
<ul>
</ul>

<h3>kdemultimedia</h3>
<!-- <ul>
</ul> -->

<a name="kdenetwork">&nbsp;</a>
<h3>kdenetwork</h3>
<ul>
</ul>

<h3>kdepim</h3>
<ul>
</ul>

<h3>kdesdk</h3>
<ul>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
</ul>

<h3>KDevelop (version 2.1.2)</h3>
<ul>
<li>fixed problems in Output toolview (newlines, scrolling, jump to error, line break), workarounds for some Qt bugs</li>
<li>improved printing (WYSIWYG), adjusting the print font size</li>
<li>giving the possibility to switch to color printing inside kdeveloprc (BlackAndWhitePrinting=false)</li>
<li>CFLAGS and CPPFLAGS don't apply in build process</li>
<li>improved indention in the editor</li>
<li>typo fixed in KDE MDI app wizard template</li>
<li>fixed gcc-3.1 compile errors</li>
</ul>


<?php include "footer.inc" ?>
