------------------------------------------------------------------------
r1004260 | trueg | 2009-07-29 18:13:18 +0000 (Wed, 29 Jul 2009) | 2 lines

Make sure the Soprano cmake macros are found.

------------------------------------------------------------------------
r1004777 | mart | 2009-07-30 20:41:16 +0000 (Thu, 30 Jul 2009) | 2 lines

backport findInCache fix

------------------------------------------------------------------------
r1004910 | scripty | 2009-07-31 03:15:44 +0000 (Fri, 31 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1005096 | teve | 2009-07-31 10:33:05 +0000 (Fri, 31 Jul 2009) | 11 lines

Backport r993958 by orlovich:
(won't be in 4.3.0 unless Dirk retags)

Adjust to a suddent bout of pedanticism in Qt --- now 16-bit unicode auto-detection 
creates codec successfully again.

Thanks to Dario Andres for collecting the info needed for this fix.

CCBUG: 199557


------------------------------------------------------------------------
r1005236 | winterz | 2009-07-31 15:50:24 +0000 (Fri, 31 Jul 2009) | 7 lines

Backport r1004456 by tilladam from trunk to the 4.3 branch:

Implementation of a timezone backend for Windows. It queries the registry for
all available and the current timezone and allows setting the system timezone
as well. Reviewed by David Jarvie.


------------------------------------------------------------------------
r1005238 | winterz | 2009-07-31 15:57:43 +0000 (Fri, 31 Jul 2009) | 4 lines

Backport r1004561 by sengels from trunk to the 4.3 branch:

msvc fixes for timezone backend

------------------------------------------------------------------------
r1005297 | mart | 2009-07-31 18:54:05 +0000 (Fri, 31 Jul 2009) | 2 lines

backport fixes to findInRectCache

------------------------------------------------------------------------
r1005305 | mart | 2009-07-31 18:59:45 +0000 (Fri, 31 Jul 2009) | 2 lines

backport disk cache sync in releaseRectsCache

------------------------------------------------------------------------
r1005407 | winterz | 2009-07-31 23:25:20 +0000 (Fri, 31 Jul 2009) | 6 lines

Backport r1005344 by winterz from trunk to the 4.3 branch:

add the new option SORT_MEMBERS_CONSTRUCTORS_FIRST provided by Michael's doxygen patch.



------------------------------------------------------------------------
r1005875 | mikearthur | 2009-08-02 12:46:37 +0000 (Sun, 02 Aug 2009) | 2 lines

Fix OSX compilation with Nokia Qt SDK.

------------------------------------------------------------------------
r1005967 | aseigo | 2009-08-02 18:27:44 +0000 (Sun, 02 Aug 2009) | 3 lines

return a dummy group when we have no description set up; prevents crashes when the operations file is not available
CCBUG:200475

------------------------------------------------------------------------
r1006185 | ppenz | 2009-08-03 05:27:41 +0000 (Mon, 03 Aug 2009) | 8 lines

Backport of SVN commit 1006184: When copying an URL, it should use /a/b/c for local URLs instead of
file:///a/b/c to make the URL usable outside of KDE apps. Thanks to Shane Gibbs
for the patch!

The fix will be part of KDE 4.3.1

CCBUG: 170608
CCMAIL: shane@hands.net.nz
------------------------------------------------------------------------
r1006836 | orlovich | 2009-08-04 15:36:01 +0000 (Tue, 04 Aug 2009) | 6 lines

Merged revision:r1002313 | orlovich | 2009-07-25 12:42:47 -0400 (Sat, 25 Jul 2009) | 5 lines

When adjusting insertion point due to anonymous wrappers, we may have
to skip non-blog (e.g. table) anonymous renderers as well.
Fixes crash on http://summit.ubuntu.com/media/lifestream.html
Testcase upcoming...
------------------------------------------------------------------------
r1006950 | aseigo | 2009-08-04 19:14:59 +0000 (Tue, 04 Aug 2009) | 2 lines

use value instead of [] to avoid empty or uninteresting elements in the collection

------------------------------------------------------------------------
r1006976 | mart | 2009-08-04 20:01:28 +0000 (Tue, 04 Aug 2009) | 2 lines

backport the proper behaviour for this function

------------------------------------------------------------------------
r1007006 | mart | 2009-08-04 21:21:54 +0000 (Tue, 04 Aug 2009) | 2 lines

backport the overlay caching fix

------------------------------------------------------------------------
r1007068 | aseigo | 2009-08-05 04:49:56 +0000 (Wed, 05 Aug 2009) | 4 lines

QDebug is not marked as re-entrant or thread safe and seems to cause crashses for some when used from the wallpaper rendering threads, so let's just remove t
hose lines :)
CCBUG:202538

------------------------------------------------------------------------
r1007294 | trueg | 2009-08-05 13:10:53 +0000 (Wed, 05 Aug 2009) | 2 lines

Backport: Handle Resource::resourceUri in toUrlList. This allows to convert QUrl, QList<QUrl>, Resource, and QList<Resource> into a QList<QUrl>

------------------------------------------------------------------------
r1007713 | jlayt | 2009-08-06 07:41:21 +0000 (Thu, 06 Aug 2009) | 16 lines

Fix the Y0K bug.  Backport commit 992292 from trunk.

The Julian/Gregorian Calendar does not have a Year 0, so all maths on a year
needs to adjust for this when moving over the BC/AD divide.  A new private
method addYearNumber() does this correctly and should be used in all parts
of the code that needs to add years.

The Hebrew, Hijri and Jalali calendars are unaffected by this as none of our
implementations are proleptic, i.e. allow dates before year 1.

In 4.4 when we get calendar systems that do have a Year 0 we will need to
modify the addYearNumber() method to repect this.

BUG: 154016


------------------------------------------------------------------------
r1008128 | dfaure | 2009-08-06 23:16:17 +0000 (Thu, 06 Aug 2009) | 2 lines

backport: adapt unittest to recent fix

------------------------------------------------------------------------
r1008142 | adawit | 2009-08-06 23:46:45 +0000 (Thu, 06 Aug 2009) | 1 line

Back port r1002257 to fix issues
------------------------------------------------------------------------
r1008147 | adawit | 2009-08-06 23:53:35 +0000 (Thu, 06 Aug 2009) | 1 line

Back port r1001978 from trunk
------------------------------------------------------------------------
r1008148 | adawit | 2009-08-06 23:56:18 +0000 (Thu, 06 Aug 2009) | 1 line

Back port API addition to this branch because it is needed to address crucial fixes for kdewebkit
------------------------------------------------------------------------
r1008187 | scripty | 2009-08-07 03:03:24 +0000 (Fri, 07 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1008406 | adawit | 2009-08-07 12:58:35 +0000 (Fri, 07 Aug 2009) | 2 lines

@since 4.3 -> @since 4.3.1

------------------------------------------------------------------------
r1008831 | mart | 2009-08-08 13:12:29 +0000 (Sat, 08 Aug 2009) | 2 lines

backport: don't draw the rectangle if there is no room for text

------------------------------------------------------------------------
r1008898 | darioandres | 2009-08-08 15:54:21 +0000 (Sat, 08 Aug 2009) | 6 lines

Backport to 4.3:
- Fix parameters on KAutoUnmount constructor. Mountpoint and desktopFile where switched
  on the Private object constructor, causing KIO::unmount() to trying to unmount the
  desktop file path instead of the mount point -> fail.


------------------------------------------------------------------------
r1008904 | darioandres | 2009-08-08 16:14:54 +0000 (Sat, 08 Aug 2009) | 5 lines

Backport to 4.3:
- Added a space between the executable name and the parameters in the system call command in unmount()
  otherwise it will simply fail with an "unknown command" error.


------------------------------------------------------------------------
r1008998 | darioandres | 2009-08-08 21:42:47 +0000 (Sat, 08 Aug 2009) | 3 lines

- Update kdepackages.h file (today version)


------------------------------------------------------------------------
r1009219 | cfeck | 2009-08-09 11:42:37 +0000 (Sun, 09 Aug 2009) | 4 lines

Fix enabled() state of combo box with no actions (backport r1009217)

CCBUG: 203114

------------------------------------------------------------------------
r1009266 | cfeck | 2009-08-09 14:52:55 +0000 (Sun, 09 Aug 2009) | 4 lines

Fix disabled actions selectable in combo box (backport 1009265)

CCBUG: 203114

------------------------------------------------------------------------
r1009408 | segato | 2009-08-09 22:22:33 +0000 (Sun, 09 Aug 2009) | 3 lines

backport r1009407
don't use visibility on windows with gcc 4.4

------------------------------------------------------------------------
r1009415 | segato | 2009-08-09 22:32:58 +0000 (Sun, 09 Aug 2009) | 4 lines

backport r1009414
add CMAKE_C_COMPILER_ARG1 as argument when executing the compiler, this way cmake doesn't fail if we set CC to something like 'distcc 
/path/to/gcc'

------------------------------------------------------------------------
r1009444 | djarvie | 2009-08-10 01:03:53 +0000 (Mon, 10 Aug 2009) | 2 lines

Minor apidox corrections

------------------------------------------------------------------------
r1009464 | alvarenga | 2009-08-10 05:12:29 +0000 (Mon, 10 Aug 2009) | 1 line

[KDE-pt_BR] updating translations
------------------------------------------------------------------------
r1009871 | scripty | 2009-08-11 03:16:01 +0000 (Tue, 11 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009919 | carewolf | 2009-08-11 08:35:20 +0000 (Tue, 11 Aug 2009) | 2 lines

Use HINTS not PATHS to suggest paths to be searched

------------------------------------------------------------------------
r1010073 | ervin | 2009-08-11 15:11:14 +0000 (Tue, 11 Aug 2009) | 7 lines

Add the flush option for mounting devices when supported.

(Backport from r1010071, seems harmless enough to go into
4.3 branch as well).

CCBUG: 202738

------------------------------------------------------------------------
r1010112 | lvsouza | 2009-08-11 16:37:06 +0000 (Tue, 11 Aug 2009) | 4 lines

Backport r1010110 by lvsouza from trunk to the 4.3 branch:

Implements support to tablet switch in solid. The switch informs if a tablet laptop has its screen in normal rotation, like normal laptops, or if it is rotated like a tabletPC. Patch from Daniel O. Nascimento.

------------------------------------------------------------------------
r1010273 | coates | 2009-08-12 05:27:51 +0000 (Wed, 12 Aug 2009) | 6 lines

Backport of r1010271.

Use a QPointer to keep track of KDialog's main widget. No more surprises if the main widget gets deleted elsewhere. See http://reviewboard.kde.org/r/1252/ for more details.

CCBUG:180064

------------------------------------------------------------------------
r1010516 | segato | 2009-08-12 18:29:43 +0000 (Wed, 12 Aug 2009) | 3 lines

backport r1010515
add kde qt plugins path only when the QCoreApplication is created otherwise qt won't read qt.conf

------------------------------------------------------------------------
r1010561 | jlayt | 2009-08-12 20:35:17 +0000 (Wed, 12 Aug 2009) | 8 lines

Disable the Print Range From/To entry in the print dialog as Kate does not pay
any attention to them anyway.

Backport from trunk of 1010552.

BUG 160760


------------------------------------------------------------------------
r1010637 | scripty | 2009-08-13 03:15:41 +0000 (Thu, 13 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1010795 | jacopods | 2009-08-13 13:21:57 +0000 (Thu, 13 Aug 2009) | 2 lines

Show the sd_mmc icon instead of the generic icon when appropriate

------------------------------------------------------------------------
r1011065 | jlayt | 2009-08-13 21:19:02 +0000 (Thu, 13 Aug 2009) | 8 lines

Disable Print Range From and To in the Print Dialog as this is not supported
by khtml.  This also applies to all khtmlpart based printing such as KMail.

Backport from trunk commit 1011062

BUG 194574


------------------------------------------------------------------------
r1011114 | sengels | 2009-08-13 23:46:06 +0000 (Thu, 13 Aug 2009) | 1 line

fix plugin loading on msvc as well as on mingw which broke in r994634
------------------------------------------------------------------------
r1011221 | scripty | 2009-08-14 03:18:42 +0000 (Fri, 14 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011302 | trueg | 2009-08-14 09:36:40 +0000 (Fri, 14 Aug 2009) | 4 lines

Backport:
* Ignore duplicate properties
* Do only load data from nrl:Ontology and nrl:KnowledgeBase graphs

------------------------------------------------------------------------
r1011762 | mrybczyn | 2009-08-15 20:09:10 +0000 (Sat, 15 Aug 2009) | 2 lines

Merged with 4.2 entities, sorting, adding some missing entries

------------------------------------------------------------------------
r1012108 | carewolf | 2009-08-16 19:59:00 +0000 (Sun, 16 Aug 2009) | 2 lines

Merge pkgconfig vs cmake fix from trunk

------------------------------------------------------------------------
r1012111 | mart | 2009-08-16 20:16:28 +0000 (Sun, 16 Aug 2009) | 2 lines

backport setorientation fix

------------------------------------------------------------------------
r1012341 | adawit | 2009-08-17 13:46:52 +0000 (Mon, 17 Aug 2009) | 1 line

Send ERR_USER_CANCELED if the user cancles the leaving ssl site warning dialog box
------------------------------------------------------------------------
r1012348 | adawit | 2009-08-17 14:12:38 +0000 (Mon, 17 Aug 2009) | 1 line

Schedule the jobs to prevent spawning of too many ioslaves
------------------------------------------------------------------------
r1012407 | adawit | 2009-08-17 16:12:52 +0000 (Mon, 17 Aug 2009) | 1 line

BACKPORT: Preserve all the SSL related meta data information to make it is available for the browser part
------------------------------------------------------------------------
r1012510 | adawit | 2009-08-17 18:52:32 +0000 (Mon, 17 Aug 2009) | 1 line

Removed unnecessary toLower() calls
------------------------------------------------------------------------
r1012564 | dfaure | 2009-08-17 20:26:42 +0000 (Mon, 17 Aug 2009) | 4 lines

Fix "Copy file after rename uses old file name". This was due to the "use of the cached item" speedup,
which was finding a kfileitem with an outdated UDS_NAME. Fix will be in KDE-4.3.1.
BUG: 195385

------------------------------------------------------------------------
r1012566 | dfaure | 2009-08-17 20:31:49 +0000 (Mon, 17 Aug 2009) | 2 lines

Fix compilation here as well

------------------------------------------------------------------------
r1012693 | aseigo | 2009-08-17 23:41:59 +0000 (Mon, 17 Aug 2009) | 2 lines

prevent full screen repaints when applet handles are removed

------------------------------------------------------------------------
r1013100 | dfaure | 2009-08-18 19:54:03 +0000 (Tue, 18 Aug 2009) | 4 lines

Use queued connection in order to fix crash when editing toolbars; the xmlgui-rebuilding was
deleting the menuitem that called this slot in the first place, and this is a nice and easy
way to decouple the two things. BUG 200815

------------------------------------------------------------------------
r1013128 | dfaure | 2009-08-18 22:10:22 +0000 (Tue, 18 Aug 2009) | 5 lines

Backport: Fix detection of Qt's phonon, patch by Pavel Volkovitskiy, approved by Thiago.
This needs Qt-4.5.3 when it's out or kde-qt (which has the fix backported).
For older versions of Qt, it didn't work before, and it still won't work;
we used standalone phonon instead (and most people still do).

------------------------------------------------------------------------
r1013228 | dfaure | 2009-08-19 10:46:44 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport fix for 204322: Giuseppe Della Bianca is completely right, this should be path(), not toLocalFile(). Wrong Windows porting...
So, renaming remote files during copy was broken in 4.3.0 too...

------------------------------------------------------------------------
r1013326 | tmcguire | 2009-08-19 15:21:59 +0000 (Wed, 19 Aug 2009) | 10 lines

Backport r1013072 by tmcguire from trunk to the 4.3 branch:

Remove the hack that fixed some bugs in Qt, since those bugs are now fixed, and
now this hack introduces its own bugs...

Add unit test to verify that it works.

CCBUG: 180612


------------------------------------------------------------------------
r1013463 | darioandres | 2009-08-20 00:01:56 +0000 (Thu, 20 Aug 2009) | 7 lines

Backport to 4.3 of:
SVN commit 1013462 by darioandres:

- Show an error message when a folder can't be accesed in the filedialog

CCBUG: 191016 

------------------------------------------------------------------------
r1013575 | dfaure | 2009-08-20 09:35:36 +0000 (Thu, 20 Aug 2009) | 2 lines

Backport r1013381: initialize crc member.

------------------------------------------------------------------------
r1013638 | dfaure | 2009-08-20 10:47:35 +0000 (Thu, 20 Aug 2009) | 2 lines

Fix ktar bug with large files. BUG 203414.

------------------------------------------------------------------------
r1013766 | rdale | 2009-08-20 18:20:31 +0000 (Thu, 20 Aug 2009) | 6 lines

* Add a Plasma::AppletScript::addStandardConfigurationPages() method that adds
standard pages to a configuration dialog such as the shortcuts one. In
conjunction with standardConfigurationDialog() this will allow script engines
to use the same dialogs as C++ applets


------------------------------------------------------------------------
r1013774 | rdale | 2009-08-20 18:26:37 +0000 (Thu, 20 Aug 2009) | 2 lines

* Don't return values from void methods..

------------------------------------------------------------------------
r1013847 | dfaure | 2009-08-20 21:35:42 +0000 (Thu, 20 Aug 2009) | 3 lines

Fix KMimeType::isDefault which could, well, never work, in the current form.
The docu (which I wrote!) says "Don't ever write code that compares mimetype pointers, compare names instead."

------------------------------------------------------------------------
r1013848 | dfaure | 2009-08-20 21:41:11 +0000 (Thu, 20 Aug 2009) | 2 lines

Good think André Woebbeking is watching over my shoulders :-)

------------------------------------------------------------------------
r1013881 | scripty | 2009-08-21 02:59:24 +0000 (Fri, 21 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013908 | mlaurent | 2009-08-21 07:41:01 +0000 (Fri, 21 Aug 2009) | 3 lines

Backport:
fix mem leak

------------------------------------------------------------------------
r1013909 | mlaurent | 2009-08-21 07:41:48 +0000 (Fri, 21 Aug 2009) | 3 lines

Backport:
fix mem leak

------------------------------------------------------------------------
r1014120 | orlovich | 2009-08-21 14:28:49 +0000 (Fri, 21 Aug 2009) | 6 lines

Need to check for clearing case here too, not just in processObjectRequest, as we may start 
a KRun directly from here.
BUG:202618
BUG:198123
BUG:199284

------------------------------------------------------------------------
r1014128 | rdale | 2009-08-21 14:51:58 +0000 (Fri, 21 Aug 2009) | 4 lines

* Add a '@since 4.3.1' tag to addStandardConfigurationPages().
Thanks to Stephan Binner for pointing out it was missing


------------------------------------------------------------------------
r1014444 | mart | 2009-08-22 20:50:23 +0000 (Sat, 22 Aug 2009) | 2 lines

backport sizing fixes

------------------------------------------------------------------------
r1014446 | alvarenga | 2009-08-22 20:52:35 +0000 (Sat, 22 Aug 2009) | 1 line

[KDE-pt_BR] add file
------------------------------------------------------------------------
r1014448 | mart | 2009-08-22 20:54:12 +0000 (Sat, 22 Aug 2009) | 2 lines

corect a problem in the last backport

------------------------------------------------------------------------
r1014492 | scripty | 2009-08-23 03:26:23 +0000 (Sun, 23 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014677 | pino | 2009-08-23 15:15:44 +0000 (Sun, 23 Aug 2009) | 5 lines

backport SVN commit 1006018 by pino:

use ktexteditor_insertfile instead of ktexteditor_dockwordcompletion (which is no more)
also check for ktexteditor_kdatatool as additional service when we want two of them

------------------------------------------------------------------------
r1014776 | orlovich | 2009-08-23 18:50:44 +0000 (Sun, 23 Aug 2009) | 5 lines

Don't preload libplasma if no hidden visibility; as it loads 
libQtWebKit which has symbol clashes with libkhtml, hence making 
konqueror understandable with about page
BUG: 201689

------------------------------------------------------------------------
r1014782 | orlovich | 2009-08-23 19:13:57 +0000 (Sun, 23 Aug 2009) | 8 lines

Major fixes to SVG CSS parsing + attribute/CSS mapping.
Also cleanup some compat layer bloat (I think I really 
should do it wholesale at some point, though, since 
even small tweaks tend to flow up)

This still needs RenderStyle computation fixes 
(and a few more parsing fixes) to really take effect, though.

------------------------------------------------------------------------
r1014804 | orlovich | 2009-08-23 20:31:58 +0000 (Sun, 23 Aug 2009) | 5 lines

Fixes for SVG text-anchor. Also some search-and-replace to help out fixing 
up the rest of RenderStyle construction for SVG

CCBUG:204817

------------------------------------------------------------------------
r1014806 | orlovich | 2009-08-23 20:36:47 +0000 (Sun, 23 Aug 2009) | 2 lines

Woops, forgot to commit this. Thanks reavertm for heads up.

------------------------------------------------------------------------
r1015143 | orlovich | 2009-08-24 16:18:39 +0000 (Mon, 24 Aug 2009) | 4 lines

Fix DOMString::toFloat. In particular helps with parsing of SVG 
gradient stops, though the gradient looks kinda... crooked.


------------------------------------------------------------------------
r1015177 | chehrlic | 2009-08-24 19:40:38 +0000 (Mon, 24 Aug 2009) | 5 lines

merge r1014678
when restoring a session make sure to select the correct encoding in the user-visible menu - internally the saved encoding was correctly set on the document but for the user it looked like no encoding was set because the corresponding menu entry was not selected

merge r1014717
save also the selected ReadOnly-Mode in the session config - when I marked a file readonly it should be readonly after session restore too
------------------------------------------------------------------------
r1015210 | aacid | 2009-08-24 21:40:08 +0000 (Mon, 24 Aug 2009) | 5 lines

Backport r1015209 | aacid | 2009-08-24 23:38:45 +0200 (Mon, 24 Aug 2009) | 3 lines

Make klauncher replace %c in the Exec line by the correct translated Name entry when launching .kdesktop files with a full path
acked by dfaure

------------------------------------------------------------------------
r1015422 | adawit | 2009-08-25 13:52:40 +0000 (Tue, 25 Aug 2009) | 1 line

Use only the default language in the default user-agent string
------------------------------------------------------------------------
r1015428 | ilic | 2009-08-25 14:03:58 +0000 (Tue, 25 Aug 2009) | 1 line

Another special ijek->ek form. (bport: 1015427)
------------------------------------------------------------------------
r1015434 | ilic | 2009-08-25 14:16:01 +0000 (Tue, 25 Aug 2009) | 1 line

ijek->ek mappings for other than all lowercase letters. (bport: 1015433.)
------------------------------------------------------------------------
r1015520 | orlovich | 2009-08-25 18:22:39 +0000 (Tue, 25 Aug 2009) | 2 lines

More SVG CSS parsing & used value computation fixes...

------------------------------------------------------------------------
r1015547 | rshah | 2009-08-25 20:32:23 +0000 (Tue, 25 Aug 2009) | 1 line

backporting to fix horizontal scrollbar bug (slider not drawn properly)
------------------------------------------------------------------------
r1015607 | orlovich | 2009-08-25 22:44:55 +0000 (Tue, 25 Aug 2009) | 3 lines

More extensive SVG CSS parsing fixes. The frontend should be mostly OK 
now, though quite a few rendering fixes are still needed

------------------------------------------------------------------------
r1015617 | orlovich | 2009-08-25 23:31:51 +0000 (Tue, 25 Aug 2009) | 4 lines

Fix handling of stop-opacity (the usual "QColor(QRgb) drops alpha" problem), 
and simplify a lot of code removing a leak...
Fixes rendering of application-exit, chemical and  energies, Oxygen icons, and probably others.

------------------------------------------------------------------------
r1015698 | tmcguire | 2009-08-26 08:46:34 +0000 (Wed, 26 Aug 2009) | 5 lines

Backport r1015676 by tmcguire from trunk to the 4.3 branch:

Fix regression from KDE3: Honor the dndEnabled() flag


------------------------------------------------------------------------
r1015874 | ehamberg | 2009-08-26 13:44:02 +0000 (Wed, 26 Aug 2009) | 5 lines

backport of r1007012:

silence the cstyle indenter

CCBUG: 192402
------------------------------------------------------------------------
r1016226 | mueller | 2009-08-27 07:57:27 +0000 (Thu, 27 Aug 2009) | 2 lines

bump version

------------------------------------------------------------------------
r1016302 | orlovich | 2009-08-27 13:25:21 +0000 (Thu, 27 Aug 2009) | 5 lines

Add an HTMLSelection::remove(HTMLOptionElement*) overload; ebay.fr frontpage needs it.
Also, make the silly DoS guard on .length actually cover all paths of changing it.

BUG:204044

------------------------------------------------------------------------
r1016323 | dfaure | 2009-08-27 14:25:11 +0000 (Thu, 27 Aug 2009) | 2 lines

backport socket error handling fix r1016321

------------------------------------------------------------------------
r1016329 | dfaure | 2009-08-27 14:34:54 +0000 (Thu, 27 Aug 2009) | 2 lines

backport "QCoreApplication::postEvent: Unexpected null receiver" fix

------------------------------------------------------------------------
r1016339 | dfaure | 2009-08-27 14:42:18 +0000 (Thu, 27 Aug 2009) | 2 lines

backport fix for bug 182384

------------------------------------------------------------------------
r1016340 | dfaure | 2009-08-27 14:45:30 +0000 (Thu, 27 Aug 2009) | 3 lines

The exitcode was 254 when passing wrong args to ksendbugmail indeed, so let's not just check for 1, but for != 0.
BUG: 182384

------------------------------------------------------------------------
r1016365 | mart | 2009-08-27 16:28:04 +0000 (Thu, 27 Aug 2009) | 2 lines

backport crash fix on setAttentionMovie(0)

------------------------------------------------------------------------
r1016509 | dfaure | 2009-08-27 23:23:50 +0000 (Thu, 27 Aug 2009) | 2 lines

Backport: fix crash when typing a device name that is not in the list.

------------------------------------------------------------------------
