<?php 
      # AUTOGENERATED FILE! ALL CHANGES MADE HERE WILL BE LOST.
      # Please edit the XML file instead
      $page_title = "KDE 4.7.4 Changelog";
      $site_root = "../../";
      include "header.inc";?>
  

  

  

  <h2>Changes in KDE 4.7.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_7_4/kdelibs.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="cmake">cmake</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Support intalling .po files with . in the middle of the name. See Git commits <a href="http://commits.kde.org/cmake/8ff107527eef1dfd3e67357d1127369a647fdc6e">8ff1075</a> and <a href="http://commits.kde.org/cmake/9872a43e854aa28c5ef4bcfe9d20e3c210f0ba7e">9872a43</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_7_4/kdeworkspace.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kwin">Kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix check for shader support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=278828">278828</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/4ed511fb7dcc8f099414157a85a11fb2f4f94449">4ed511f</a>. </li>
        <li class="bugfix normal">Fix tiling and maximized windows not always playing nice together. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=246153">246153</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/9b63684a92df4a8a59617ad7fa42b310622f7733">9b63684</a>. </li>
      </ul>
      </div>
        <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Make python scriptengine installable if python3 is your main python. See Git commit <a href="http://commits.kde.org/kde-workspace/23b6cb0404d586104dcbfac79476183a462829e8">23b6cb0</a>. </li>
        <li class="bugfix normal">Fix applications set to "all activities" no being shown in the pager at all. See Git commit <a href="http://commits.kde.org/kde-workspace/fd47389ca6718f917cc508a8df4216e40a395c8b">fd47389</a>. </li>
        <li class="bugfix crash">Fix crash when moving mouse about the clock widget. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=284462">284462</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=287958">287958</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/b13b460450f3acdebf40773ec3093c61a87697af">b13b460</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_7_4/kdegraphics.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix shortcut clash for those with a standard "Save As..." shortcut. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=268551">268551</a>.  See Git commit <a href="http://commits.kde.org/Okular/432b7376e8f9c8f1b77c6cb074ea828920167a81">432b737</a>. </li>
        <li class="bugfix normal">Fix repainting when repainting lots of small regions. See Git commit <a href="http://commits.kde.org/Okular/9f654a0029976965024a09bf3c095f9f07d031cd">9f654a0</a>. </li>
        <li class="bugfix normal">Improve djvu text extraction for some files. See Git commit <a href="http://commits.kde.org/Okular/9b35c4d2800528a3b29543d99891bae264ffb0aa">9b35c4d</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim-runtime"><a name="kdepim-runtime">kdepim-runtime</a><span class="allsvnchanges"> [ <a href="4_7_4/kdepim-runtime.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
        <h4><a name="pop3 resource">pop3 resource</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix tab ordering in the configure dialog. See Git commit <a href="http://commits.kde.org/kdepim-runtime/d26fd398da895d8b8b90269d519f2b146da88bf2">d26fd39</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_7_4/kdeutils.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix assertion when decrypting pasted text in editor. See Git commit <a href="http://commits.kde.org/kgpg/6a3e8d211c9629e81ab2ea3f095cfae05b5e2559">6a3e8d2</a>. </li>
        <li class="bugfix normal">Fix hang when trying to open a detached signature. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=286991">286991</a>.  See Git commit <a href="http://commits.kde.org/kgpg/de841cf6a13d5c2ff5bb3cdb9002acc3afae05c9">de841cf</a>. </li>
      </ul>
      </div>
    </div>
  
<?php include "footer.inc"?>
