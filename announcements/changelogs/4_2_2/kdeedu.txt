------------------------------------------------------------------------
r934757 | cniehaus | 2009-03-03 18:43:51 +0000 (Tue, 03 Mar 2009) | 1 line

new version for kde 4.2.2
------------------------------------------------------------------------
r935450 | scripty | 2009-03-05 07:52:55 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r936420 | coolo | 2009-03-07 14:54:39 +0000 (Sat, 07 Mar 2009) | 2 lines

fprintf needs stdio

------------------------------------------------------------------------
r936436 | gladhorn | 2009-03-07 16:09:28 +0000 (Sat, 07 Mar 2009) | 2 lines

backport r936435 synonyms fix

------------------------------------------------------------------------
r936444 | gladhorn | 2009-03-07 16:41:54 +0000 (Sat, 07 Mar 2009) | 2 lines

backport r936443 synonym and capitalization fixes

------------------------------------------------------------------------
r937022 | lueck | 2009-03-08 21:46:11 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r937563 | sengels | 2009-03-09 22:43:20 +0000 (Mon, 09 Mar 2009) | 1 line

submit some changes so that people at least think it is new
------------------------------------------------------------------------
r937905 | nielsslot | 2009-03-10 18:51:44 +0000 (Tue, 10 Mar 2009) | 4 lines

Backport commit 937904 which is a fix for bug 186792.

Ignore a 'return' when not in a function. KTurtle would crash otherwise.

------------------------------------------------------------------------
r938547 | scripty | 2009-03-12 07:47:23 +0000 (Thu, 12 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939397 | hdevalence | 2009-03-14 19:37:43 +0000 (Sat, 14 Mar 2009) | 2 lines

Backport of r939317; don't show night map on non-earth planets.

------------------------------------------------------------------------
r939843 | hdevalence | 2009-03-15 20:36:51 +0000 (Sun, 15 Mar 2009) | 2 lines

Backport r939827

------------------------------------------------------------------------
r940053 | jakselsen | 2009-03-16 14:04:49 +0000 (Mon, 16 Mar 2009) | 1 line

4 new svgs backp. for 4.2
------------------------------------------------------------------------
r941012 | chehrlic | 2009-03-18 17:38:52 +0000 (Wed, 18 Mar 2009) | 4 lines

save text document as utf-8 instead default locale - fix saving/restoring lectures on windows
backport from trunk

CCBUG: 185495
------------------------------------------------------------------------
r943283 | bholst | 2009-03-23 16:00:00 +0000 (Mon, 23 Mar 2009) | 1 line

* Added Marble config dialog for the qt-only version.
------------------------------------------------------------------------
r943292 | bholst | 2009-03-23 16:12:40 +0000 (Mon, 23 Mar 2009) | 1 line

Reverted #943283
------------------------------------------------------------------------
r943682 | scripty | 2009-03-24 08:21:15 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943996 | gladhorn | 2009-03-24 19:55:17 +0000 (Tue, 24 Mar 2009) | 2 lines

Backport r943995 - fix crash when inserting

------------------------------------------------------------------------
r944112 | reed | 2009-03-25 04:08:00 +0000 (Wed, 25 Mar 2009) | 1 line

fix opengl includes on Qt4/X11 on OSX
------------------------------------------------------------------------
r944635 | gladhorn | 2009-03-25 21:05:29 +0000 (Wed, 25 Mar 2009) | 2 lines

backport r944632 - wrong order in summary widget

------------------------------------------------------------------------
r944811 | scripty | 2009-03-26 08:14:23 +0000 (Thu, 26 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
