2006-07-24 15:15 +0000 [r565827]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/examples/template.py: BUG:
	  130231 This doesn't break anything in the files I've tested with
	  on my machine. Using text.ascii() is a Bad Thing all around.

2006-07-26 20:56 +0000 [r566720]  staikos

	* branches/KDE/3.5/kdeutils/kwallet/kwalleteditor.cpp: prompt and
	  allow overwrite can someone forward port this to trunk? BUG:
	  122946

2006-07-27 16:19 +0000 [r566944]  dfaure

	* branches/KDE/3.5/kdeutils/kwallet/kwalleteditor.cpp: Use KURL
	  properly (should fix escaping problems with non-latin chars and
	  with '#')

2006-08-03 13:59 +0000 [r569319]  mlaurent

	* branches/KDE/3.5/kdeutils/kgpg/kgpgeditor.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgeditor.h: Fix crash. When we
	  comment action better to comment in all code.

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-21 15:05 +0000 [r575438]  jriddell

	* branches/KDE/3.5/kdeutils/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-22 05:31 +0000 [r575800]  helio

	* branches/KDE/3.5/kdeutils/kwallet/konfigurator/konfigurator.cpp,
	  branches/KDE/3.5/kdeutils/kwallet/konfigurator/konfigurator.h: -
	  Setting module to use system defaults

2006-08-22 05:43 +0000 [r575805]  helio

	* branches/KDE/3.5/kdeutils/kmilo/thinkpad/kcmthinkpad/main.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/thinkpad/kcmthinkpad/main.h,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kcmkvaio/main.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kcmkvaio/main.h: -
	  Setting module to use system defaults

2006-08-22 06:09 +0000 [r575809]  helio

	* branches/KDE/3.5/kdeutils/klaptopdaemon/profile.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/power.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/buttons.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/warning.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/profile.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/warning.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/battery.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/acpi.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/apm.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/battery.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/acpi.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/apm.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/sony.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/power.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/sony.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/buttons.cpp: - Setting
	  module to use system defaults

2006-08-23 01:35 +0000 [r576069]  henrique

	* branches/KDE/3.5/kdeutils/ark/main.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: * Fix for a race
	  condition. Patch by jaguarwan <jaguarwan@yahoo.fr>. Thanks for
	  the patch! BUG: 127341

2006-08-26 15:02 +0000 [r577395]  henrique

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp: * Fix for
	  drag'n'drop problems. Patch by KDEfanboy <KDE.fanboy@gmail.com>.
	  Thank you! BUG: 91556

2006-08-29 12:06 +0000 [r578464]  lunakl

	* branches/KDE/3.5/kdeutils/kwallet/kwalletmanager.cpp:
	  QApplication::exit() kind of doesn't work when it's only later
	  followed by QApplication::exec(). CCMAIL: coolo@kde.org

2006-08-30 09:22 +0000 [r578797]  kling

	* branches/KDE/3.5/kdeutils/klaptopdaemon/daemon_state.cpp: Change
	  the "battery critical" default shutdown behavior to reflect the
	  UI. Thanks to Chantry Xavier for fix and testing. BUG: 113622

2006-08-30 20:37 +0000 [r579021]  kling

	* branches/KDE/3.5/kdeutils/khexedit/khexeditui.rc: Reorder "find
	  previous/next" actions in that order for consistency. BUG: 122378

2006-09-04 13:09 +0000 [r580779-580777]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp: BUG:
	  128925 More .ascii() fixing.

	* branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: Version bump
	  for 3.5.5 tagging.

2006-09-06 10:10 +0000 [r581381]  charis

	* branches/KDE/3.5/kdeutils/ark/main.cpp,
	  branches/KDE/3.5/kdeutils/ark/filelistview.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Update my email address

2006-09-06 10:23 +0000 [r581391]  charis

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Commit junk as
	  well,reverting

2006-09-06 19:34 +0000 [r581587]  wirr

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.h: Allow
	  themes to get the selected KDE user language

2006-09-06 20:28 +0000 [r581607]  helio

	* branches/KDE/3.5/kdeutils/kmilo/thinkpad/thinkpad.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/delli8k/delli8k.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kvaio.cpp: - kmix
	  call should be "hide", not "minimize", as pointed by Albert in:
	  http://lists.zerezo.com/kde-devel/msg00883.html

2006-09-10 21:33 +0000 [r582875]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalc.cpp: Fixes Bug 132446

2006-09-13 16:42 +0000 [r583838]  helio

	* branches/KDE/3.5/kdeutils/kmilo/thinkpad/thinkpad.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/delli8k/delli8k.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/kmilo_kvaio/kvaio.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.cpp: -
	  kmilo was calling first device on kmix, instead of correct master
	  one. Fixed but still have some doubts about "mute" call, since
	  still applies on first device, and can be sure if all sounds of
	  card will be muted.

2006-09-17 09:12 +0000 [r585462]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/version.h: Fixed Bug 134192

2006-09-17 10:55 +0000 [r585498]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/kcalc_core.cpp: This fixes
	  hopefully: Bug 133420, 133720

2006-09-20 00:40 +0000 [r586579]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp: This is
	  hopefully the real fix to BUG 133720 and 131482 (also on AMD64
	  and intel-clones)

2006-09-22 22:50 +0000 [r587473]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp: Work-around
	  for bug 134070 (Thanks to Philip Rodrigues and Ernst Bachmann);
	  real solution would be to implement arbitray precision for Exp
	  etc.

2006-10-02 11:08 +0000 [r591337]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: updates for 3.5.5

