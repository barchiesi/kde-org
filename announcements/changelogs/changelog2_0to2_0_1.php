<?php
  $page_title = "KDE 2.0 to 2.0.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 2.0 and 2.0.1 releases.
The primary goals of the 2.0.1 release being more complete documentation
and translations, the amount of code change is quite minimal - only the
most critical bugs have been fixed, the rest of the development is
currently being done in the branch that will lead to the 2.1 release.
</p>

<h3>General</h3>
<ul>
  <li>One new language is available : Japanese</li>
  <li>The Chinese big5 encoding should be better supported now</li>
  <li>The bug report dialog has better error reporting</li>
</ul>

<h3>aRts</h3>
<ul>
  <li>Made sampling rate configurable in kcontrol, so that owners of Yamaha
soundcards can have sound</li>
  <li>Moved .mcopclass files from $(libdir) to $(libdir)/mcop</li>
  <li>Fix for PAS16 cards</li>
</ul>

<h3>KControl</h3>
<ul>
  <li>Various crashes fixed</li>
  <li>Associating a program with a type of file sets it as the most preferred one</li>
</ul>

<h3>KDesktop</h3>
<ul>
  <li>Fixed crash some users were having when running a process as a different user (minicli)</li>
  <li>Fixed man:somepage (minicli)</li>
  <li>Fixed crash on Ctrl+Z, and problems with any other shortcut for a disabled action in KDE</li>
</ul>

<h3>KDM</h3>
<ul>
  <li>Don't create a "/.kde" directory</li>
  <li>Fixes for background settings (kdmdesktop)</li>
</ul>

<h3>KHexEdit</h3>
<ul>
  <li>Internationalisation fixes (non-latin1 encodings)</li>
</ul>

<h3>Kicker</h3>
<ul>
  <li>Fix for drag-and-drop with autohide</li>
  <li>Recent document menu: fix for special characters in filenames</li>
  <li>Applications menu: fix for duplicated entries being even more duplicated</li>
</ul>

<h3>KMail</h3>
<ul>
  <li>Fix a compatibility problem with some POP3 servers.</li>
  <li>Fix the problem that removing all addresses from an address header field didn't work.</li>
</ul>

<h3>KNode</h3>
<ul>
  <li>Bugfix for networking on solaris</li>
  <li>Bugfix for continuous reconnection on<br>news servers with authentication</li>
  <li>Sane default window sizes</li>
  <li>Correct default for the smtp-server port</li>
  <li>Bugfix for a problem with some smtp servers</li>
  <li>Fixes for non-iso-8859-x users</li>
  <li>Bugfix for the display of plain text attachments</li>
</ul>

<h3>KOffice</h3>
<ul>
  <li>Fix for duplicate keys in the configure keys dialog</li>
</ul>

<h3>Konqueror</h3>
<ul>
  <li>Improved enabling/disabling of menu items, and location bar not being filled on startup</li>
  <li>Fixes for wrong directory-view-mode being used in some cases</li>
  <li>Fixed crash when opening some FTP links in the directory tree</li>
  <li>KIO: Fix for non-disappearing progress window in some cases</li>
  <li>KHTML: various charset fixes (for viewing and for posting forms)</li>
  <li>KHTML: various crash fixes</li>
  <li>KHTML: support for deprecated ISINDEX</li>
  <li>KHTML: support for HTML4 &lt;BUTTON&gt;</li>
  <li>Javascript: fixes for crashes in memory management</li>
  <li>Javascript: fail safe protection against infinite loops</li>
  <li>Javascript: support for (non-standard) options.selectedIndex</li>
</ul>

<h3>Konsole</h3>
<ul>
  <li>Fix for openBSD</li>
</ul>

<h3>KPPP</h3>
<ul>
  <li>Compuserve 7 bit fix</li>
  <li>Fix for lost characters with some modems (usb)</li>
  <li>Merged in Alpha fix from latest KDE 1.1.x version</li>
  <li>Supress bogus exit codes from older versions of pppd</li>
</ul>

<h3>KSpell</h3>
<ul>
  <li>Many fixes and improvements, especially for non-latin1 users</li>
</ul>

<h3>kio_man</h3>
<ul>
  <li>Fixed for non-latin1 users</li>
</ul>

<h3>Screensavers</h3>
<ul>
  <li>Three of them where not saving their configuration correctly</li>
</ul>

<h3>kdvi/kview</h3>
<ul>
  <li>Fixed crash on bad config files</li>
</ul>

<h3>SysV-Init Editor (KSysV)</h3>
<ul>
  <li>Fixed drag-and-drop problem: entries deleted via dnd onto the trashcan
didn't set the "changed" flag, so the modified configuration couldn't be saved.
</li>
</ul>

<h3>XML-RPC to DCOP Daemon (kxmlrpcd)</h3>
<ul>
  <li>Fixed potential Denial of Service (DoS) attack.</li>
</ul>

<?php include "footer.inc" ?>
