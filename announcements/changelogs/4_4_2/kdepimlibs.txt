------------------------------------------------------------------------
r1096627 | tokoe | 2010-02-27 19:40:20 +1300 (Sat, 27 Feb 2010) | 2 lines

Use plugins from $INSTALL_DIR/lib/ and not $INSTALL_DIR/share/apps

------------------------------------------------------------------------
r1098004 | mlaurent | 2010-03-03 05:36:14 +1300 (Wed, 03 Mar 2010) | 3 lines

Backport:
Fix show label 'server doesn't support identification'

------------------------------------------------------------------------
r1098671 | scripty | 2010-03-04 15:17:35 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100118 | smartins | 2010-03-07 09:52:00 +1300 (Sun, 07 Mar 2010) | 4 lines

Forwardport r1100103 by smartins from e35 to branch4.4:

Only use the new date if it's valid.

------------------------------------------------------------------------
r1100946 | scripty | 2010-03-09 15:22:03 +1300 (Tue, 09 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1101293 | winterz | 2010-03-10 09:59:52 +1300 (Wed, 10 Mar 2010) | 9 lines

Backport r1101292 by winterz from trunk to the 4.4 branch:

compute a variable for the major and minor version numbers.

looks like we'll need to use these values for conditional compiling since
the next libical release is probably not going to be source compatibile.
MERGE: 4.4


------------------------------------------------------------------------
r1102748 | smartins | 2010-03-14 02:33:59 +1300 (Sun, 14 Mar 2010) | 6 lines

Backport r1102742 by smartins from trunk to 4.4:

dtStart() was returning dtDue's time instead of dtStart's

kolab/issue4109

------------------------------------------------------------------------
r1103858 | scripty | 2010-03-16 15:43:51 +1300 (Tue, 16 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104019 | tokoe | 2010-03-17 03:04:05 +1300 (Wed, 17 Mar 2010) | 4 lines

Parse vCards with ENCODING=8BIT correctly

BUG: 230968

------------------------------------------------------------------------
r1107214 | scripty | 2010-03-25 15:30:12 +1300 (Thu, 25 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107331 | tokoe | 2010-03-26 00:31:29 +1300 (Fri, 26 Mar 2010) | 6 lines

Backport for r1107329

Reinitialize the status message after online status is known

This fixes all the wrong 'Offline' status messages in akonadiconsole

------------------------------------------------------------------------
r1107335 | tokoe | 2010-03-26 00:38:57 +1300 (Fri, 26 Mar 2010) | 9 lines

Backport of r1107321

Send status message when finished synchronization.

This avoids empty status messages in akonadiconsole.

The message is copy&pasted from another file in libakonadi-kde,
so no new message is introduced.

------------------------------------------------------------------------
r1107743 | mueller | 2010-03-27 06:18:40 +1300 (Sat, 27 Mar 2010) | 2 lines

KDE 4.4.2

------------------------------------------------------------------------
