2005-08-06 22:45 +0000 [r443678]  mpyne

	* branches/KDE/3.4/kdeutils/ark/archiveformatinfo.cpp,
	  branches/KDE/3.4/kdeutils/ark/tar.cpp: Backport fix for
	  application/x-tbz2 mimetype to KDE 3.4.

2005-08-06 22:59 +0000 [r443681]  mpyne

	* branches/KDE/3.4/kdeutils/ark/ark.desktop,
	  branches/KDE/3.4/kdeutils/ark/ark_part.desktop: Backport ark
	  .desktop file fix to KDE 3.4.

2005-08-06 23:40 +0000 [r443691]  mpyne

	* branches/KDE/3.4/kdeutils/ark/ark.desktop,
	  branches/KDE/3.4/kdeutils/ark/archiveformatinfo.cpp,
	  branches/KDE/3.4/kdeutils/ark/tar.cpp,
	  branches/KDE/3.4/kdeutils/ark/ark_part.desktop: Revert Ark
	  changes to KDE 3.4 regarding the x-tbz2 mimetype, 3.4 doesn't
	  know anything about x-tbz2 so this isn't a bugfix by definition,
	  which means it doesn't belong in this branch.

2005-08-10 20:36 +0000 [r445300]  kniederk

	* branches/KDE/3.4/kdeutils/kcalc/kcalc_const_menu.cpp: Fixed BUG:
	  110532

2005-09-11 23:02 +0000 [r459817]  mueller

	* branches/KDE/3.4/kdeutils/ksim/monitors/disk/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/library/led.h,
	  branches/KDE/3.4/kdeutils/ksim/monitors/i8k/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/snmp/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/mail/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/filesystem/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/lm_sensors/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/net/Makefile.am,
	  branches/KDE/3.4/kdeutils/ksim/monitors/cpu/Makefile.am: unbreak
	  visibility.. also fix makefiles

2005-10-05 13:48 +0000 [r467525]  coolo

	* branches/KDE/3.4/kdeutils/kdeutils.lsm: 3.4.3

