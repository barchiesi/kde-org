------------------------------------------------------------------------
r854890 | reitelbach | 2008-08-30 16:56:34 +0200 (Sat, 30 Aug 2008) | 3 lines

remove wrong button text.
"Validate" is the wrong word. Instead use the widgets default "OK".

------------------------------------------------------------------------
r854904 | lueck | 2008-08-30 17:11:36 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r855912 | majewsky | 2008-09-01 19:49:46 +0200 (Mon, 01 Sep 2008) | 3 lines

Really disable pausing when the game is over. It could still be enabled by firing up the highscore dialog.

BUG: 168631
------------------------------------------------------------------------
r856060 | scripty | 2008-09-02 07:55:54 +0200 (Tue, 02 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r856335 | kleag | 2008-09-02 19:21:45 +0200 (Tue, 02 Sep 2008) | 1 line

Backport correction from rev 856334
------------------------------------------------------------------------
r856509 | scripty | 2008-09-03 08:34:35 +0200 (Wed, 03 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r858102 | mlaurent | 2008-09-07 13:37:59 +0200 (Sun, 07 Sep 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r859352 | scripty | 2008-09-10 08:47:03 +0200 (Wed, 10 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r859473 | coolo | 2008-09-10 14:31:12 +0200 (Wed, 10 Sep 2008) | 2 lines

backporting solver bugfixes

------------------------------------------------------------------------
r859480 | coolo | 2008-09-10 14:41:17 +0200 (Wed, 10 Sep 2008) | 2 lines

backport the bugfixes that did not involve i18n changes

------------------------------------------------------------------------
r861093 | scripty | 2008-09-15 08:08:13 +0200 (Mon, 15 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861427 | scripty | 2008-09-16 08:09:55 +0200 (Tue, 16 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r862398 | kbal | 2008-09-18 21:10:02 +0200 (Thu, 18 Sep 2008) | 2 lines

Fix typo found while translating - "loose armies" sounds really bad
CCMAIL: pelouas@hotmail.fr
------------------------------------------------------------------------
r862790 | kleag | 2008-09-19 23:17:56 +0200 (Fri, 19 Sep 2008) | 1 line

Update svnmerge data
------------------------------------------------------------------------
r862791 | kleag | 2008-09-19 23:19:36 +0200 (Fri, 19 Sep 2008) | 1 line

Merged from trunk, revision 862789: Correct strings extraction scripts
------------------------------------------------------------------------
r862802 | kleag | 2008-09-20 00:27:14 +0200 (Sat, 20 Sep 2008) | 1 line

Merge from trunk rev 862801: It seems that one should always append to rc.cpp in Messages.sh
------------------------------------------------------------------------
r862903 | kleag | 2008-09-20 13:18:00 +0200 (Sat, 20 Sep 2008) | 1 line

Merged from trunk rev 862900: Solve a lot of i18n problems
------------------------------------------------------------------------
r863174 | dimsuz | 2008-09-21 13:10:29 +0200 (Sun, 21 Sep 2008) | 2 lines

Backport a bugfix from trunk (-r863156)

------------------------------------------------------------------------
r863346 | kleag | 2008-09-21 23:31:25 +0200 (Sun, 21 Sep 2008) | 1 line

Insure to use the libkdegames catalog (backported from trunk
------------------------------------------------------------------------
r863414 | scripty | 2008-09-22 08:01:37 +0200 (Mon, 22 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863440 | mlaurent | 2008-09-22 09:00:21 +0200 (Mon, 22 Sep 2008) | 3 lines

Backport:
Add icon

------------------------------------------------------------------------
r863459 | mlaurent | 2008-09-22 09:15:34 +0200 (Mon, 22 Sep 2008) | 4 lines

Backport:
Add missing connection
Add cancel button

------------------------------------------------------------------------
r863462 | mlaurent | 2008-09-22 09:18:56 +0200 (Mon, 22 Sep 2008) | 2 lines

Remove to slotCancel doesn't exist here

------------------------------------------------------------------------
r863467 | mlaurent | 2008-09-22 09:41:16 +0200 (Mon, 22 Sep 2008) | 3 lines

Backport:
Fix extract messages

------------------------------------------------------------------------
