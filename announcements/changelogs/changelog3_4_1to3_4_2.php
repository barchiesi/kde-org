<?php
  $page_title = "KDE 3.4.1 to KDE 3.4.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page summaries additions
and corrections that occurred in KDE between the 3.4.1 and 3.4.2 releases.
It has been manually created by maintainers, for an automatically created
complete list of changes click on the 'all SVN changes' links.
</p>

<h3><a name="arts">arts</a> <font size="-2">[<a href="3_4_2/arts.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdelibs">kdelibs</a> <font size="-2">[<a href="3_4_2/kdelibs.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeaccessibility">kdeaccessibility</a> <font size="-2">[<a href="3_4_2/kdeaccessibility.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeaddons">kdeaddons</a> <font size="-2">[<a href="3_4_2/kdeaddons.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeadmin">kdeadmin</a> <font size="-2">[<a href="3_4_2/kdeadmin.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeartwork">kdeartwork</a> <font size="-2">[<a href="3_4_2/kdeartwork.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdebase">kdebase</a> <font size="-2">[<a href="3_4_2/kdebase.txt">all SVN changes</a>]</font></h3>
<ul>
   <li>Konsole: Make 'New Window' from tab bar popup menu work.</li>
   <li>Konsole: Fix inital tab too big with icon only. (<a href="http://bugs.kde.org/show_bug.cgi?id=106684">#106684</a>)</li>
   <li>Konsole: Fix crashes when action/settings=false. (<a href="http://bugs.kde.org/show_bug.cgi?id=106829">#106829</a>)</li>
   <li>Konsole: Fix Konsole doesn't save Encoding setting. (<a href="http://bugs.kde.org/show_bug.cgi?id=107329">#107329</a>)</li>
   <li>Ksysguard: Fix a segmentation fault on Centrino Laptops with Linux vanilla kernel 2.6 (<a href="http://bugs.kde.org/show_bug.cgi?id=105182 ">#105182</a>)</li>
</ul>

<h3><a name="kdebindings">kdebindings</a> <font size="-2">[<a href="3_4_2/kdebindings.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeedu">kdeedu</a> <font size="-2">[<a href="3_4_2/kdeedu.txt">all SVN changes</a>]</font></h3>
<ul>
	<li>Kalzium: Fix a wrong electronegativity</li>
	<li>libkdeedu: Fix gcc4-compilation-error</li>
</ul>

<h3><a name="kdegames">kdegames</a> <font size="-2">[<a href="3_4_2/kdegames.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdegraphics">kdegraphics</a> <font size="-2">[<a href="3_4_2/kdegraphics.txt">all SVN changes</a>]</font></h3>
<ul>
   <li>kpdf: Improve the rendering of pageview message on some decorations</li>
   <li>kpdf: Made TOC work with a single click <a href="http://bugs.kde.org/show_bug.cgi?id=103433">#103433</a></li>
   <li>kpdf: Make TOC links to external documents work</li>
   <li>kpdf: Make opening pdf from hyperlinks work again <a href="http://bugs.kde.org/show_bug.cgi?id=106767">#106767</a></li>
   <li>kpdf: Try fix for sizing problem <a href="http://bugs.kde.org/show_bug.cgi?id=94851">#94851</a></li>
   <li>kpdf: Try fix for pdf regeneration problems <a href="http://bugs.kde.org/show_bug.cgi?id=98891">#98891</a></li>
   <li>kpdf: Address bar does not update when changing pdf within inside kpdf <a href="http://bugs.kde.org/show_bug.cgi?id=106771">#106771</a></li>
   <li>kpdf: Some improvements in rendering</li>
   <li>KolourPaint: Enforce text box font height to prevent e.g. Chinese
       characters in buggy fonts from enlarging the text box and putting the
       cursor out of sync with the text</li>
   <li>KolourPaint: Clicking in a text box selects a character based on its
       midpoint - not leftmost point - to be consistent with all text editors
       (esp. noticeable with big fonts)</li>
   <li>KolourPaint: Return and Numpad 5 Key now draw</li>
   <li>KolourPaint: Tool Actions placed outside the Tool Box resize with
       their toolbars</li>
   <li>KolourPaint: Ensure Color Similarity maximum is 30, not 29 due to
       gcc4</li>
   <li>KolourPaint: Tool Box traps right clicks (for the RMB Menu) on top
       of tool options widgets and the empty part of the Tool Box</li>
</ul>
<h3><a name="kdemultimedia">kdemultimedia</a> <font size="-2">[<a href="3_4_2/kdemultimedia.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdenetwork">kdenetwork</a> <font size="-2">[<a href="3_4_2/kdenetwork.txt">all SVN changes</a>]</font></h3>
<ul>
   <li>kppp: completed i18n-related flow control fix</li>
   <li>kppp: fixed PPP interface check on FreeBSD and possibly other systems</li>
</ul>

<h3><a name="kdepim">kdepim</a> <font size="-2">[<a href="3_4_2/kdepim.txt">all SVN changes</a>]</font></h3>
<ul>
<li>KAlarm: Minimise KMix window if KMix is started by KAlarm when displaying a message. (<a href="http://bugs.kde.org/102315">#102315</a>)</li>
<li>KAlarm: Prevent session restoration displaying main windows which should be hidden. (<a href="http://bugs.kde.org/101877">#101877</a>)</li>
<li>KAlarm: Prevent alarm message windows being too large for screen. (<a href="http://bugs.kde.org/107165">#107165</a>)</li>
<li>KAlarm: Change --volume command line option short form from -v to -V to avoid clash with --version.</li>
<li>KNode: Fix %NAME and %EMAIL custom header macros (<a href="http://bugs.kde.org/22101">#22101</a>)</li>
<li>KNode: Support news server that don't understand the LIST OVERVIEW.FMT command (<a href="http://bugs.kde.org/104422">#104422</a>, <a href="http://bugs.kde.org/106390">#106390</a>)</li>
<li>KNode: Fix subject field cursor regression (<a href="http://bugs.kde.org/107032">#107032</a>)</li>
<li>Akregator: Improve rendering speed of the article list drastically (by about factor 10)</li>
<li>Akregator: Fix opening of non-HTML files in external applications</li>
<li>Akregator: Don't allow to add feed without url</li>
</ul>

<h3><a name="kdesdk">kdesdk</a> <font size="-2">[<a href="3_4_2/kdesdk.txt">all SVN changes</a>]</font></h3>
<ul>
<li>umbrello: Crash when deleting an attribute that is represented as an association (<a href="http://bugs.kde.org/72016">#72016</a>)</li>
<li>umbrello: Inline functions are generated in cpp file (<a href="http://bugs.kde.org/97188">#97188</a>)</li>
<li>umbrello: Crash when closing a tab (<a href="http://bugs.kde.org/103170">#103170</a>)</li>
<li>umbrello: Initial value of a new paramenter of a method cannot be set the first time (<a href="http://bugs.kde.org/106183">#106183</a>)</li>
<li>umbrello: Line Vertex hidden in class diagrams (<a href="http://bugs.kde.org/106356">#106356</a>)</li>
<li>umbrello: Relationships between interfaces and classes change over file reload (<a href="http://bugs.kde.org/106632">#106632</a>)</li>
<li>umbrello: Associations turn into generalisation over file reopen (<a href="http://bugs.kde.org/106673">#106673</a>)</li>
<li>umbrello: Code generator &quot;could not find active language&quot; (<a href="http://bugs.kde.org/107101">#107101</a>)</li>
<li>umbrello: Cannot drag members from one classifier to another (<a href="http://bugs.kde.org/107551">#107551</a>)</li>
<li>umbrello: Generating Javascript code from a class diagram hangs (<a href="http://bugs.kde.org/108688">#108688</a>)</li>
</ul>

<h3><a name="kdetoys">kdetoys</a> <font size="-2">[<a href="3_4_2/kdetoys.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdeutils">kdeutils</a> <font size="-2">[<a href="3_4_2/kdeutils.txt">all SVN changes</a>]</font></h3>
<ul>
  <li>Ark: Do not try to open the internal viewer in case the extraction of the file failed (<a href="http://bugs.kde.org/show_bug.cgi?id=105398">#105398</a>)</li>
  <li>KCalc: Fix 0 + 0 and 0 - 0 (<a href="http://bugs.kde.org/show_bug.cgi?id=106605">#106605</a>)</li>
</ul>

<h3><a name="kdevelop">kdevelop</a> <font size="-2">[<a href="3_4_2/kdevelop.txt">all SVN changes</a>]</font></h3>

<h3><a name="kdewebdev">kdewebdev</a> <font size="-2">[<a href="3_4_2/kdewebdev.txt">all SVN changes</a>]</font></h3>
<?php include "footer.inc" ?>
