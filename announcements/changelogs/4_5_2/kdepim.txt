------------------------------------------------------------------------
r1168979 | scripty | 2010-08-28 14:27:09 +1200 (Sat, 28 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1169213 | nlecureuil | 2010-08-29 05:56:07 +1200 (Sun, 29 Aug 2010) | 3 lines

Fix year string in list grouping headers
BUG: 247453

------------------------------------------------------------------------
r1169244 | tilladam | 2010-08-29 07:36:47 +1200 (Sun, 29 Aug 2010) | 8 lines

Avoid unnecessary collection tree syncs.

When a folder is modified that contains email, but is a parent of kolab
groupware folders (such as the INBOX), treat it as the structural node
in the kolab collection tree that it is, instead of a suddenly appeared
new kolab folder. To that end, don't just check if the folder is the
folder-type annotation, also make sure it's not "mail", to tell kolab
folders from structural ones.
------------------------------------------------------------------------
r1169608 | djarvie | 2010-08-30 08:06:30 +1200 (Mon, 30 Aug 2010) | 1 line

Set initial value for storage location radio buttons
------------------------------------------------------------------------
r1169610 | djarvie | 2010-08-30 08:12:08 +1200 (Mon, 30 Aug 2010) | 1 line

Update change log
------------------------------------------------------------------------
r1169695 | nlecureuil | 2010-08-30 09:18:22 +1200 (Mon, 30 Aug 2010) | 2 lines

Backport commit 1169694

------------------------------------------------------------------------
r1170108 | aheinecke | 2010-08-31 07:22:06 +1200 (Tue, 31 Aug 2010) | 5 lines

Check if a calendar object is set before trying to lock it.

BUG: 243788


------------------------------------------------------------------------
r1170131 | aheinecke | 2010-08-31 08:20:45 +1200 (Tue, 31 Aug 2010) | 2 lines

Only print kDebug output in case the calendar is not set while saving

------------------------------------------------------------------------
r1170140 | aheinecke | 2010-08-31 08:59:55 +1200 (Tue, 31 Aug 2010) | 5 lines

Do not crash if a task cannot be added because no calendar is set.
Print an error message instead.

CCBUG: 237926

------------------------------------------------------------------------
r1170331 | smartins | 2010-09-01 03:18:35 +1200 (Wed, 01 Sep 2010) | 11 lines

When an item is resized, the itemModified( AgendaItem ) signal is emitted.

When resizing a multi-day event ( has more than one AgendaItem ), the item that must go with the signal is the first one,
otherwise alll hell breaks loose inside KOAgendaView::updateEventDates().

updateEventDates() is already complicated enough and deals with lots of corner cases, so i wont adapt it to support
AgendaItems other than the first. Fix the signal emitter instead.

MERGE: trunk


------------------------------------------------------------------------
r1170343 | smartins | 2010-09-01 04:09:13 +1200 (Wed, 01 Sep 2010) | 1 line

Backport to 4.5 some --flicker patches
------------------------------------------------------------------------
r1170661 | lueck | 2010-09-02 07:28:08 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1170811 | lueck | 2010-09-02 19:33:25 +1200 (Thu, 02 Sep 2010) | 1 line

backport typo fix
------------------------------------------------------------------------
r1171372 | winterz | 2010-09-04 03:18:07 +1200 (Sat, 04 Sep 2010) | 7 lines

update the version string.
unfortuntately, the version has been stuck at "beta1" for beta2 and beta3.
drat.

we might call the next release "rc1".  not sure yet, so call it "beta4" for now.


------------------------------------------------------------------------
r1171642 | djarvie | 2010-09-05 06:50:10 +1200 (Sun, 05 Sep 2010) | 2 lines

Remove duplicate mutex unlock()

------------------------------------------------------------------------
r1171879 | smartins | 2010-09-06 03:43:31 +1200 (Mon, 06 Sep 2010) | 2 lines

Fix crash that can happen if insertIncidence() is called before fillAgenda()

------------------------------------------------------------------------
r1172028 | smartins | 2010-09-06 20:55:22 +1200 (Mon, 06 Sep 2010) | 5 lines

Backport r1172025 by smartins from trunk to branch 4.5:

Type ahead stoped working if you tried to use it without any calendar selected.
Even after selecting calendars.

------------------------------------------------------------------------
r1172071 | kudryashov | 2010-09-06 23:08:40 +1200 (Mon, 06 Sep 2010) | 1 line

Backport r1172069: Add missing include
------------------------------------------------------------------------
r1172873 | scripty | 2010-09-08 15:22:57 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173091 | tilladam | 2010-09-09 04:25:35 +1200 (Thu, 09 Sep 2010) | 1 line

Be robust against malformed Kolab incidences with missing attachments.
------------------------------------------------------------------------
r1173246 | scripty | 2010-09-09 14:47:30 +1200 (Thu, 09 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173842 | tokoe | 2010-09-11 03:09:56 +1200 (Sat, 11 Sep 2010) | 8 lines

Do not try to add every akonadi:// url as a forwarded email message but
use proper content and mimetype instead.

Currently 'attachment' is used as name, since I have no clue how to retrieve
a meaningfull name of the Akonadi item in a generic way.

BUG: 248288

------------------------------------------------------------------------
r1175075 | scripty | 2010-09-14 15:03:56 +1200 (Tue, 14 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176143 | djarvie | 2010-09-17 07:50:12 +1200 (Fri, 17 Sep 2010) | 2 lines

Bug 251317: Fix crash if alarm triggers while its deletion confirmation prompt is visible.

------------------------------------------------------------------------
r1176161 | djarvie | 2010-09-17 08:33:43 +1200 (Fri, 17 Sep 2010) | 1 line

Bug 251317: improved fix
------------------------------------------------------------------------
r1176162 | jlayt | 2010-09-17 08:36:51 +1200 (Fri, 17 Sep 2010) | 6 lines

Performance improvements to KOrganizer, use more efficient KHolidays calls

See http://reviewboard.kde.org/r/5343/

CCBUG: 251127

------------------------------------------------------------------------
r1176770 | djarvie | 2010-09-19 04:06:49 +1200 (Sun, 19 Sep 2010) | 3 lines

Fix icon texts not fitting into toolbar width, by replacing 4 New Alarm icons with a single menu i
con.

------------------------------------------------------------------------
r1177108 | smartins | 2010-09-20 01:10:32 +1200 (Mon, 20 Sep 2010) | 8 lines

Pass the calendar in the ctor, don't set it later, it's needed now.

Fixes crash

BUG: 250127

MERGE: trunk

------------------------------------------------------------------------
r1177267 | smartins | 2010-09-20 11:10:14 +1200 (Mon, 20 Sep 2010) | 1 line

SVN_SILENT: aDd this debug statement
------------------------------------------------------------------------
r1177272 | smartins | 2010-09-20 11:29:19 +1200 (Mon, 20 Sep 2010) | 1 line

SVN_SILENT: more debug statements
------------------------------------------------------------------------
r1177501 | skelly | 2010-09-21 00:03:19 +1200 (Tue, 21 Sep 2010) | 1 line

Backport 1177499
------------------------------------------------------------------------
r1177653 | lueck | 2010-09-21 07:46:02 +1200 (Tue, 21 Sep 2010) | 1 line

backport: change gui strings from ktts to Jovie
------------------------------------------------------------------------
r1177664 | lueck | 2010-09-21 08:17:39 +1200 (Tue, 21 Sep 2010) | 1 line

backport from trunk: rename the desktop file to KMail2 to launch the documentation named kmail properly. ack'ed by Ingo Klöcker
------------------------------------------------------------------------
r1178000 | winterz | 2010-09-22 11:14:15 +1200 (Wed, 22 Sep 2010) | 8 lines

backport SVN commit 1177997 by winterz:

don't fetch some feeds times and times again (if it cannot access them).
http://reviewboard.kde.org/r/5384/
Thanks Shlomi!

CCBUG: 225851

------------------------------------------------------------------------
r1178006 | cgiboudeaux | 2010-09-22 11:24:21 +1200 (Wed, 22 Sep 2010) | 5 lines

Backport r1178003 from trunk to 4.5
Let the search filter look for authors.
CCBUG: 242712


------------------------------------------------------------------------
r1178010 | winterz | 2010-09-22 11:45:18 +1200 (Wed, 22 Sep 2010) | 5 lines

backport SVN commit 1178008 by winterz:
use proper KColorSheme colors for unread and new article names in the list
http://reviewboard.kde.org/r/5211/
Thanks Ignat

------------------------------------------------------------------------
r1178826 | cgiboudeaux | 2010-09-24 12:50:42 +1200 (Fri, 24 Sep 2010) | 5 lines

Backport r1178825 from trunk to 4.5:
Set the focus on the feed url when adding a feed
CCBUG: 251856


------------------------------------------------------------------------
r1179298 | scripty | 2010-09-25 14:49:52 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179414 | djarvie | 2010-09-25 23:11:49 +1200 (Sat, 25 Sep 2010) | 4 lines

Fix error message which only showed a partial text.

CCMAIL:kde-i18n-doc

------------------------------------------------------------------------
r1179423 | tnyblom | 2010-09-25 23:43:32 +1200 (Sat, 25 Sep 2010) | 12 lines

Merged revisions 1179421 via svnmerge from 
svn+ssh://tnyblom@svn.kde.org/home/kde/trunk/KDE/kdepim

........
  r1179421 | tnyblom | 2010-09-25 13:37:02 +0200 (lör, 25 sep 2010) | 5 lines
  
  Fix maildirresource to write changes when the full payload is present and not just the body.
  Also change the mixedmaildirresource to use the same logic.
  
  MERGE: 4.5
........

------------------------------------------------------------------------
r1179915 | tokoe | 2010-09-27 03:00:29 +1300 (Mon, 27 Sep 2010) | 2 lines

Use a limit of 1 when searching the photo of a contact

------------------------------------------------------------------------
r1180083 | winterz | 2010-09-27 12:01:22 +1300 (Mon, 27 Sep 2010) | 6 lines

merge forward SVN commit 1180082 by winterz:
restore notes() dbus interface
CCBUG: 251914
thanks for the patch Tejas.


------------------------------------------------------------------------
r1180681 | alexmerry | 2010-09-29 04:27:03 +1300 (Wed, 29 Sep 2010) | 3 lines

Backport r1180679: Do not trigger kcmodule's change() signal when loading the saved settings for the appearance tab in kmail's configuration


------------------------------------------------------------------------
r1181045 | djarvie | 2010-09-30 10:51:06 +1300 (Thu, 30 Sep 2010) | 2 lines

Prevent a long file name in a file display alarm from expanding the message window width.

------------------------------------------------------------------------
