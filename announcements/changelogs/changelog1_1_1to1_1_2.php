<?php
  $page_title = "KDE 1.1.1 to 1.1.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<CENTER>
  <TABLE CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
    <tr>
      <td> <font size="+1"> <!--VAR COLS=60 ROWS=4--><!--EMPTY--><!--/VAR--> </font>
This page tries to present as much as possible of the problem
corrections and feature additions occurred in KDE between version 1.1.1 and the
current, 1.1.2. The changes descriptions were kept brief. The main
characteristic of the present release is stability, rock solid stability, as well as
improved visual appearance.
Thanks to all involved. Thanks to the users for their great moral support. Special
thanks to the artists team!

	    <br>
        <br>
      </td>
    </tr>
<tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>
<a name="kdebase"></a>
<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdebase
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>


<a name="kfm"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kfm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>large team</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed severe bug which allowed directory-in-itself recursive copy
		  <li> FTP uploading works again
		  <li> Short-URL support is fixed/improved
		  <li> Kfm doesn't hang when right-clicking on FIFOs and other non-regular files
		  <li> Important memory leak (esp. when browsing the web) fixed
		  <li> Error handling improved (fixes frequent crashes)
		</ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kpanel"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpanel

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich, Pietro Iglio</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed memory leaks (HP)
		</ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="krdb"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

krdb

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mark Donohoe, Dirk A. Mueller</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Primitive support for ~/.Xdefaults
		</ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kscreensaver"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kscreensaver

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>various</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed security hole in klock
		  <li> Fixed compile problem in kscience (J6t, HP)
		</ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kwm"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed color gradient on big endian machines
		</ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>
<a name="kdemultimedia"></a>
<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdemultimedia
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kscd"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kscd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed problem with qmail
		  <li> Silenced some compiler warnings
		</ul>

	      </TD></TR></TABLE>

<tr><TD>&nbsp;</TD></tr>
<a name="kdenetwork"></a>
<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdenetwork
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kmail"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmail

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner, Markus Wuebben</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
            <p>Changes since 1.1.1<p>
		<ul>
		  <li> New filter actions: execute (a shell command), forward-to. (ST)
		  <li> Updated documentation about PGP5 and new filter actions. (ST)
                  <li> Added settings option to turn off automatic sending of
	            receive/read confirmations (default on). (ST)
                  <li> Added settings option to compact all (needed) folders on exit. KMail
                    can now together with pine on same folders(SR)
                  <li> Added Compact All Folders menu entry. (SR)
                  <li> KMail now tries harder to determine attachment icon (SR)
                  <li> We create now proper In-Reply-To an References header.
                  <li> Alternative send method has accelerator Shift+Ctrl+Return (Default method
                    has Ctrl-Return). Also if default method is "Send Now" composer has
                    toolbar icon for "Send Later" (SR)
		</ul>
	    <p>Bugfixes since 1.1.1<p>
		<ul>
		  <li> Proper resizing of filter-action specific widgets (list of  folders, entry fields) when
		    filter is selected. (ST)
		  <li> Security fix according attachments: changed place of attachment temp directory
		    from /tmp to ~/.kde/share/apps/kmail/tmp (ST)
		  <li> Fixed displaying text formatted with spaces (TP)
		  <li> Fix against deleteing two messages at once when in trash (bug 1037) (DN)
		  <li> Corrected prepending of "--" before signature (DN)
		  <li> Button "Help" works now in filter dialog (DN)
		  <li> Cancel/Reject fix for kmfilterdialog.cpp (DN)
		  <li> Updown-bugfix in filter dialog: moving a filter up could corrupt the filter list (DN)
		  <li> Fixed wrongly sorted message list when sorting by subject was requested. (J6t)
                  <li> Added delayed update of folder list. This fixes the drastical
                    performance loss by the display of the number of unread messages (when
                    processing many messages). (ST)
                  <li> Filters: &lt;body&gt;, &lt;message&gt; and &lt;any header&gt; filter work. (DN)
                  <li> Added explanation of %_ to text in settings dialog. (ST)
                  <li> Fixed eat-last-char-of-message-bug in compacting folders. (SR)
                  <li> Fixed nasty bug reported by Daniel Naber: open composer, change
                    visible header fields, close composer, open it again -> crash. (DS)
                  <li> Fixed bug: move last message from folder, then try to set header style ->
                    segfault. Also, KMail now doesn't show contents of deleted mails in mail reader. (JB)
                  <li> Never, ever again send something to X-Loop header. (SR, thanks to DN)
                  <li> Edit + Send Later now really saves changes and updates edited message in
                    reader (SR, thanks JB)
                  <li> Right-click on headers can act on all selected messages (DN)
                  <li> Fix for bug no-windows-but-running-wild, which also prevented user
                    to start new KMail instance, unless killed first - in which case it used to
                    corrupt layout (SR, thanks to JB for finding this)
                  <li> Fixed bug reported by Jacek Stolarczyk (jacek@mer.chemia.polsl.gliwice.pl):
                    kmail could create attachment dirs with zero permissions and then couldn use them.
                  <li> Security: unlink lock and message files before writing
                    to them. (SR)
                  <li> KMail used to die after user deleted a filter. This was probably introduced after
                    1.1.1. Fixed. (SR)
                  <li> Fixed signing with PGP 5. (bugs #1353, #1543 and #1578) (SR)
                  <li> Fixed updating index files on exit. This bug sometimes caused deleted
                    messages to be visible as new or unread on next kmail start. (WB)
		</ul>
		<p>Who's who:<p>
		<ul>
                  <li> ST <b>Stefan Taferner</b> (&#116;&#x61;fe&#x72;ner&#064;k&#x64;&#x65;.&#111;r&#x67;) (Author/maintainer)
                  <li> DN Daniel Naber (dnaber@mini.gt.owl.de)
		  <li> DS Don Sanders (dsanders@cch.com.au)
		  <li> J6t Johannes Sixt (Johannes.Sixt@telecom.at)
		  <li> SR Sven Radej (ra&#100;e&#x6a;&#64;k&#x64;e&#x2e;&#x6f;rg) (Me, maintainer for this release)
		  <li> TP Toivo Pedaste (toivo@ucs.uwa.edu.au)
		  <li> JB jbb (jb.nz@writeme.com)
                  <li> WB Waldo Bastian (basti&#097;&#x6e;&#64;k&#x64;&#x65;&#46;o&#x72;g)
		</ul>
                <p>Also thanks to:<p>
                  <li> Roberto Alsina (ra&#108;&#115;&#x69;&#110;&#097;&#00064;&#107;&#00100;&#101;.&#111;&#114;g)
                  <li> Petter Reinholdtsen (pere@hungry.com)
                  <li> Rik Hemsley (rikkus@postmaster.co.uk) (Check out his Empath mailer too)
                  <li> Jacek Stolarczyk (jacek@mer.chemia.polsl.gliwice.pl)
                  <li> And everyone else who helped to make KMail more stable, secure, nice and usable
                     (in short to make KMail an KDE application :-)
                </ul>

	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kppp"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kppp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>B. Wuebben, M. Weilguni, H. Porten</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> Fixed a cause of those "Timeout expired" errors (the
			default speed was broken)
		  <li> Removal of stale lock files works again.
		  <li> Line Termination defaults to CR as the more
		    common case.
		  <li> Update statistic data even if window is not
		    visible.
		  <li> "PPP support missing" is no longer fatal. Just a warning.
		</ul>
	      </TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="ksirc"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ksirc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Andrew Stanley-Jones</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> fixed compilation warnings and errors (HP)
		</ul>
	      </TD></TR></TABLE>

            </td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kbiff"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kbiff

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Kurt Granroth</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
	    <p>Features added since 1.1.1<p>
		<ul>
		  <li> Support for MH mailboxes
		  <li> Support for NNTP (USENET/Newsgroups)
		  <li> Very simple support for Netscape Mail folders
		  <li> Added "tooltip" (Floating Status) that pops up the number of
		  new mails for each mailbox when the mouse is over the icon
		  <li> Can now launch your mail client from the "Notify" dialog
		  <li> Can test "new mail" sound from the setup dialog
		  <li> More tiny little features... See the ChangeLog
		</ul>
	    <p>Bugfixes since 1.1.1<p>
		<ul>
		  <li> Config files should no longer randomly disappear!!
		  <li> Will compile on HP-UX again
		  <li> PINE/IMAP/POP3 mailboxes that are also MBOXs are now handled
		  correctly (I hope)
		  <li> Lots of socket/network "leaks" are now gone.  Socket code is
		  also somewhat asynchronous, now.
		  <li> Usernames are now encoded.  Funky usernames like
		  '&#0103;&#0114;&#97;n&#x72;&#x6f;th&#00064;&#107;&#100;e.or&#x67;', '/users/granroth', and those with '(' in them
		  should now work
		  <li> Lots more little ones.  See the ChangeLog
		</ul>

	      </TD></TR></TABLE>

            </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>
<a name="kdeutils"></a>
<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdeutils
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kfloppy"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kfloppy

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>
            <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
		<ul>
		  <li> fix for 3.5" drives (HP)
		</ul>

	      </TD></TR></TABLE>

	</TABLE></CENTER>

<?php include "footer.inc" ?>
