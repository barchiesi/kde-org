------------------------------------------------------------------------
r1121276 | scripty | 2010-05-01 13:51:11 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1129325 | vandenoever | 2010-05-22 11:10:18 +1200 (Sat, 22 May 2010) | 1 line

Add checks to avoid reading out of bounds.
------------------------------------------------------------------------
r1129515 | pino | 2010-05-23 05:56:40 +1200 (Sun, 23 May 2010) | 7 lines

backport fixes for bookmarks sorting:
- r1129505, maintain the bookmark view sorted when adding new elements in the "selective" tree update
- r1129512, make sure actionsForUrl() returns actions sorted by page number

BUG: 205952
FIXED-IN: 4.4.4

------------------------------------------------------------------------
r1129749 | pino | 2010-05-24 04:10:57 +1200 (Mon, 24 May 2010) | 5 lines

percent-decode lilypond references

CCBUG: 236967
FIXED-IN: 4.4.4

------------------------------------------------------------------------
r1131528 | pino | 2010-05-28 22:18:19 +1200 (Fri, 28 May 2010) | 2 lines

bump version to 0.10.4

------------------------------------------------------------------------
