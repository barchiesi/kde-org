<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.8.6 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.8.6";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release ?>.php">Plasma <?php print $release ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Also expose YaST repository configuration if present. <a href='https://commits.kde.org/discover/4b128419571bb2f45f1dfbdab21ae5419e02d77b'>Commit.</a> </li>
<li>Fix arguments when running .desktop files with runservice. <a href='https://commits.kde.org/discover/2db9081b379614ecc19331634d31ca827b9cf9e6'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Properly retrieve all boolean properties. <a href='https://commits.kde.org/kde-gtk-config/557e20b961fe478ecf7c8a0192830eaf0894098b'>Commit.</a> </li>
<li>Don't break compatibility with old configs. <a href='https://commits.kde.org/kde-gtk-config/0a7fafc7e2f455a1c4f7ae9a104abce007374572'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4594'>D4594</a></li>
<li>Fix gtk-primary-button-warps-slider with GTK 2. <a href='https://commits.kde.org/kde-gtk-config/4a06c4d34f2dd8859058618b1dbe9dbd6df9e9b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376273'>#376273</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4557'>D4557</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Fix build with -fno-operator-names. <a href='https://commits.kde.org/khotkeys/bf57c786f01feee3737545c9c08882a2057ffbb8'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Fix crash in Screen Locker KCM on teardown. <a href='https://commits.kde.org/kscreenlocker/2601fa10725284feb5bcc78006250c0aa92f92dd'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4535'>D4535</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Fix array allocation for non-continuous cpu numbers. <a href='https://commits.kde.org/ksysguard/e4c58c2f6ef74bad065f7327c19c02a3dac88883'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376537'>#376537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4640'>D4640</a></li>
<li>Use unicode codepoint instead of UTF-8 literal. <a href='https://commits.kde.org/ksysguard/01a231bbfcf70f1ff91c510dc62aee1369b2a021'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Find minimum required Breeze version instead of current version. <a href='https://commits.kde.org/kwin/cb481b492271860586c69298395e2ba118239406'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4644'>D4644</a></li>
<li>Avoid a crash on Kwin decoration KCM teardown. <a href='https://commits.kde.org/kwin/70d2fb2378d636ef6d052da08417b27c99182fb0'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4533'>D4533</a></li>
<li>Support modifier-only-shortcuts when capslock is on. <a href='https://commits.kde.org/kwin/5a87fa3f92cfc29944b8dc66c2320e3062b532ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375355'>#375355</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4241'>D4241</a></li>
<li>Translate the layout name when passing to layout change OSD. <a href='https://commits.kde.org/kwin/a3be3e8e80b68c8e8ae93034aeb139df0ad7055a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4243'>D4243</a></li>
<li>[autotests] Add test case for modifier only trigger when capslock enabled. <a href='https://commits.kde.org/kwin/0acfebd96bace0fefd7c4291617f1d07f96ca03e'>Commit.</a> See bug <a href='https://bugs.kde.org/375355'>#375355</a></li>
<li>Fixed initial graying out options in Cube Effect settings. <a href='https://commits.kde.org/kwin/a43049f6880c2da7d71a6eb9bd7acf6dd95e7b20'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129194'>#129194</a></li>
<li>[TabBox] Remember current model index on SwitcherItem model set. <a href='https://commits.kde.org/kwin/e019c9f61edf20c433ac4bf2e3546ce9b5c1104a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4162'>D4162</a>. Fixes bug <a href='https://bugs.kde.org/333511'>#333511</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix type argument for get property call. <a href='https://commits.kde.org/libkscreen/57cd4d12b3c70495935675cb7df6532b98695fb1'>Commit.</a> </li>
<li>Isable logging to kscreen.log by default, re-enable with export KSCREEN_LOGGING=1. <a href='https://commits.kde.org/libkscreen/04a6483a01caaa17e476ac9e4a47725f0e1ca06f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3993'>D3993</a>. Fixes bug <a href='https://bugs.kde.org/361688'>#361688</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Use icon center for vertical hitscan on drop. <a href='https://commits.kde.org/plasma-desktop/c9a7741f2b8082f025b5eb2e53c3c489a95e1da8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4683'>D4683</a></li>
<li>Remove get new looks button from 5.8. <a href='https://commits.kde.org/plasma-desktop/4e0bb534345642730146b98da6ede00ee2c13ad4'>Commit.</a> </li>
<li>Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/61489110c597ad4e4cb69b004d30dbda8b4110ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4525'>D4525</a></li>
<li>[Folder View] Don't show script execution prompt on desktop:/. <a href='https://commits.kde.org/plasma-desktop/b088e451623758015f577e52d567761fb130e92b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4534'>D4534</a></li>
<li>Use proper version for baloo. <a href='https://commits.kde.org/plasma-desktop/07c05cc9f3df35dea4c72cb9c9581cc0b531e1d4'>Commit.</a> </li>
<li>[Solid Device Actions KCM] Encode action file name. <a href='https://commits.kde.org/plasma-desktop/acae3816b7ae686542bc4dfeac89ecba7ff5078a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344534'>#344534</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4474'>D4474</a></li>
<li>Add missing include in sortedactivitiesmodel to fix build with GCC 7. <a href='https://commits.kde.org/plasma-desktop/8f9fc02983b0b1e1a03cf8c673adbd459b182119'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4464'>D4464</a></li>
<li>Revamp (Activity) Pager wheel handling. <a href='https://commits.kde.org/plasma-desktop/fff65ad696f4dc34391a0e9eb4b9ef0ed09a9c42'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375769'>#375769</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4360'>D4360</a></li>
<li>[Folder View] show script execution prompt when clicking item. <a href='https://commits.kde.org/plasma-desktop/771e57f3b2c19f4e6f867c01c2457ec87531b4cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375793'>#375793</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4367'>D4367</a></li>
<li>And maybe don't undo the fix while fixing the fix ... <a href='https://commits.kde.org/plasma-desktop/84fadc3603f754a326c589fbc86b92d565e4875c'>Commit.</a> </li>
<li>Fix startup warning introduced in 3568d8e4. <a href='https://commits.kde.org/plasma-desktop/db0723ab7086016367196ad15fced7b4e7edcc9d'>Commit.</a> </li>
<li>Fix clearing selection when rectangle selection contains no items. <a href='https://commits.kde.org/plasma-desktop/3568d8e4a8a2cc4fac7fccb86132c47be37495f6'>Commit.</a> </li>
<li>Fix Plasmoid.busy visualization in desktop containment. <a href='https://commits.kde.org/plasma-desktop/37a5d9fb5d0b87b50756555348a17f5d1c523cf5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4341'>D4341</a></li>
<li>Call correct function and fix warning. <a href='https://commits.kde.org/plasma-desktop/f37514e2b551525464cddf08f865a2bbb093cf1f'>Commit.</a> </li>
<li>[Task Manager] Hide group dialog when opening applets settings. <a href='https://commits.kde.org/plasma-desktop/38fca064a1d0d4ec94543805e948f7771bc021a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4280'>D4280</a></li>
<li>Use KPluginMetaData::readStringList. <a href='https://commits.kde.org/plasma-desktop/7993cd433ca68a0136a990d3f1710a6360024fe9'>Commit.</a> </li>
<li>[kcm] Fix reset logic. <a href='https://commits.kde.org/plasma-desktop/be4cff0ed1dea4515751210b7db9fe3f29ddd037'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129875'>#129875</a></li>
<li>Silence warning. <a href='https://commits.kde.org/plasma-desktop/4af59a374bce56ee4be273b8397aff28953d483d'>Commit.</a> </li>
<li>Fix crash during crash acrobatics. <a href='https://commits.kde.org/plasma-desktop/fc8ce43ac026d1b9769d8bfe98b42d74a74bc8db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365206'>#365206</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4085'>D4085</a></li>
<li>Fix binding loop. <a href='https://commits.kde.org/plasma-desktop/05920ab34283dd91d6575e6f479c14b63a258c60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374786'>#374786</a></li>
<li>[Folder View] Fix right click erroneously opening files. <a href='https://commits.kde.org/plasma-desktop/d2fde361d3c8fb40fb6c1e53e4178042799b6691'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360219'>#360219</a></li>
<li>Clean up after a window delegate drag exits the window. <a href='https://commits.kde.org/plasma-desktop/180e1072e933cf13edb145dfeab2a62b7623a0eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3947'>D3947</a></li>
<li>Fix Enter/Return not running non-dir items in list view mode. <a href='https://commits.kde.org/plasma-desktop/c94dc901aa52ae7a3c605557a6f8e8814707c806'>Commit.</a> </li>
<li>Set current index to start of selection range when performing rectangle selection. <a href='https://commits.kde.org/plasma-desktop/2b9e0e7cc61aa4d3811cde8db92ddb2795e4e97c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3932'>D3932</a></li>
<li>[Panel Containment] Explicitly bind visible on both the container and the applet. <a href='https://commits.kde.org/plasma-desktop/d674ede6e5f22281fa2461546ddc25ff5f3a7015'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3897'>D3897</a></li>
<li>[Task Manager] Parent QAction to "parent" instead of this. <a href='https://commits.kde.org/plasma-desktop/58e932c607db27bea2872625661991fb16c79d13'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3816'>D3816</a></li>
<li>[Containment Appearance] Make sure to always load a config view. <a href='https://commits.kde.org/plasma-desktop/76ac242c37b71b7dcdfaa4f78afd365b43610d6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360862'>#360862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3911'>D3911</a></li>
<li>Don't clear hover state while a child dialog is open. <a href='https://commits.kde.org/plasma-desktop/e80a4b87cfa98708724942794ca6f051974bb5c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374291'>#374291</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Do not treat filename in selection as URL. <a href='https://commits.kde.org/plasma-integration/e70f8134a2bc4b3647e245c05f469aeed462a246'>Commit.</a> See bug <a href='https://bugs.kde.org/376365'>#376365</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4579'>D4579</a></li>
<li>QtQuick-based interfaces will now more reliably adapt to color scheme changes at runtime. <a href='https://commits.kde.org/plasma-integration/ab3298b3f5f728765d5afa8830aa7793140617c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4492'>D4492</a></li>
<li>Fix regression in which the Save dialog appears as an Open dialog. <a href='https://commits.kde.org/plasma-integration/87b27476cc8a3865994da066ce06a3e836462719'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129732'>#129732</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix crash caused by notifications use-after-free. <a href='https://commits.kde.org/plasma-nm/a19794a3efd4642a9475753bef034a3f5ee6602b'>Commit.</a> </li>
<li>OpenVPN: Remove previously configured secrets to avoid passing them back. <a href='https://commits.kde.org/plasma-nm/30226c538ff9b0c1e353a9303b873548ca3dfbff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375170'>#375170</a></li>
<li>Use KDE specific page for captive portal redirection. <a href='https://commits.kde.org/plasma-nm/407cb2cffaf628af2ccd447ca12a582a6e592674'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374026'>#374026</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Always check if context is valid when calling pa functions. <a href='https://commits.kde.org/plasma-pa/1c1a76775cb566b30aa412761494116570e7c49d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375872'>#375872</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4402'>D4402</a></li>
<li>VolumeSlider: Ignore all value changes until Component is completed. <a href='https://commits.kde.org/plasma-pa/1c9f50c88e969735cdf254c35ef700e34cc276ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375495'>#375495</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4269'>D4269</a></li>
<li>Fix compare for undefined argument in playFeedback(). <a href='https://commits.kde.org/plasma-pa/2b87302d9d016392edc6a4842f94411612ac34b0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4104'>D4104</a></li>
<li>Pass correct index to playFeedback. <a href='https://commits.kde.org/plasma-pa/27c8c9aa84b42640bbc65ea64f15cbdf4b6705e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374919'>#374919</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4105'>D4105</a></li>
<li>Applet: Don't delete delegates while drag is active. <a href='https://commits.kde.org/plasma-pa/f21aa35e9946722142026131e18b87bc1b8b1ff6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374288'>#374288</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3898'>D3898</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Update to KDevplatform API change. <a href='https://commits.kde.org/plasma-sdk/ad6e3b7f937a846ec8a5532bb62f6bf2e923b49b'>Commit.</a> </li>
<li>Fix handling of paths with whitespace in bash. <a href='https://commits.kde.org/plasma-sdk/a18d9d32455a82be1dd5001dcffb78583fa27906'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129013'>#129013</a></li>
<li>Update to KDevplatform API change. <a href='https://commits.kde.org/plasma-sdk/a655f87bd4ccfd72d8a1e5e109a8321747f396bf'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Port to QMultiHash. <a href='https://commits.kde.org/plasma-workspace/5632f4c38943b28829ce6013e3dd858b8d9e2a2f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4654'>D4654</a></li>
<li>Only remove + announce if leader change actually occured, avoid excessive loop. <a href='https://commits.kde.org/plasma-workspace/ee9f966dd55ce87f75376cae5b90549dace41ea8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4652'>D4652</a></li>
<li>Fix crash. <a href='https://commits.kde.org/plasma-workspace/b82d364536fab6bdeea0c9bc3f8eecfa988e7df3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4651'>D4651</a></li>
<li>[Notifications] Never manually hide() the NotificationPopup. <a href='https://commits.kde.org/plasma-workspace/6b833f1ca2bda69ed2c4984699f4e3ecdf2a5d4e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4632'>D4632</a></li>
<li>Add comment laying out goals. <a href='https://commits.kde.org/plasma-workspace/62b803b45dbe14dc24f7593c1c2907d1e4db3d74'>Commit.</a> </li>
<li>Fix crash when switching activities. <a href='https://commits.kde.org/plasma-workspace/05826bd5ba25f6ed7be94ff8d760d6c8372f148c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376055'>#376055</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4631'>D4631</a></li>
<li>Tweak alphabetic (default) sort behavior. <a href='https://commits.kde.org/plasma-workspace/410086d06bf0b6195030fec5e9ac848cc50f50a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373698'>#373698</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4469'>D4469</a></li>
<li>Turn the NotificationItem in a MouseArea. <a href='https://commits.kde.org/plasma-workspace/4c7335230f90b288fa875ba5d1a589e14bbc86fe'>Commit.</a> </li>
<li>A minimized config dialog on the desktop (e.g. wallpaper settings) will now be un-minimized when trying to open it multiple times. <a href='https://commits.kde.org/plasma-workspace/92601876d18c0974aa524a84c3765f279c581ed9'>Commit.</a> </li>
<li>[System Tray Containment] Drop useless Q_INVOKABLE from .cpp file. <a href='https://commits.kde.org/plasma-workspace/172ab88346683fcc0f1dff6b05b3a50b85659e98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4456'>D4456</a></li>
<li>[System Tray Containment] Ungrab mouse before opening context menu. <a href='https://commits.kde.org/plasma-workspace/bcb9ede5caabac3d5086dbcdcccfd7bc44211704'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4455'>D4455</a></li>
<li>[KRunner] In doubt use primary screen for view position. <a href='https://commits.kde.org/plasma-workspace/b0b31dee60defe4d7e9de8abc1dbbadfbced2783'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375574'>#375574</a></li>
<li>Drop legacy fixup rule for VirtualBox. <a href='https://commits.kde.org/plasma-workspace/6f99f0dd99efd2d913f18f6911e35c4f4a96d74d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350468'>#350468</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4346'>D4346</a></li>
<li>Fix isOutputRedundant logic. <a href='https://commits.kde.org/plasma-workspace/3792ef9e51dc0ba21740e69964f26e241df7aed2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375507'>#375507</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4309'>D4309</a></li>
<li>Fix group parents not appearing when disabling inline grouping. <a href='https://commits.kde.org/plasma-workspace/f5bf988c5bd50ec0d39e4ca4ad19ea3b25a4fbf5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4284'>D4284</a></li>
<li>Corona::screenGeometryChanged on qscreen resized. <a href='https://commits.kde.org/plasma-workspace/dd0d19d04b51fee0f173f3334874fb53a2112461'>Commit.</a> </li>
<li>MediaController: Update position while queuedPositionUpdate timer is running. <a href='https://commits.kde.org/plasma-workspace/b2c6aa5b3499ee020b37083d67e742d912502b33'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3904'>D3904</a></li>
<li>Better clean up of duplicate containments. <a href='https://commits.kde.org/plasma-workspace/19a88030d3de12a96402a1103c964e5a7363646c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371858'>#371858</a>. See bug <a href='https://bugs.kde.org/371991'>#371991</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3981'>D3981</a></li>
<li>Fix "Pinned Chrome disappears when all Chrome windows are closed". <a href='https://commits.kde.org/plasma-workspace/cf6d8ab0dff2a6ba29574636ccfbe23c7917c48f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365617'>#365617</a>. Phabricator Code review <a href='https://phabricator.kde.org/D3950'>D3950</a></li>
<li>[MPRIS Data Engine] Don't crash if "Metadata" is a map but of wrong type. <a href='https://commits.kde.org/plasma-workspace/03374b185f9bd3bc4f70726ae641014a33278fdd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374531'>#374531</a></li>
<li>Systray: Move all icon resolution to dataengine. <a href='https://commits.kde.org/plasma-workspace/e43b89e2b9f3ff9bf6299488e82a365cbfde2b2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356479'>#356479</a>. Phabricator Code review <a href='https://phabricator.kde.org/D2986'>D2986</a></li>
<li>Use a native event filter to notice the screen was swapped. <a href='https://commits.kde.org/plasma-workspace/44c703dcb41c7b6af58a55151e9132c0dcf66763'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3822'>D3822</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Drop unused dependency. <a href='https://commits.kde.org/sddm-kcm/5755bf5743758333efc1961a04aeae76163dd326'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129725'>#129725</a></li>
</ul>


</main>
<?php
require('../aether/footer.php');
?>
