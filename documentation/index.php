<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Documentation"
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="container">

<h1>KDE Documentation</h1>

<h2>Application documentation</h2>

<p>We provide documentation for many aspects of KDE suitable for users from
varying backgrounds. For more details about where to find information,
please refer to <a href="/info/faq">"Getting Answers to Your Questions"</a>.
</p>

<p>KDE provides application documentation for almost every program. This
documentation is translated into over 20 languages and is available from the
<a href="https://docs.kde.org">KDE Documentation</a> page.
</p>

<p>
<a href="https://userbase.kde.org">KDE Userbase</a> provides help for end users on how to use KDE and its applications.
</p>

<h2>System Administrator Documentation</h2>

<p>
<a href="https://userbase.kde.org/Special:MyLanguage/KDE_System_Administration">KDE for System Administrators</a> provides information for administrators who
are deploying KDE in their organisation.
</p>

<h2>How to help with the documentation</h2>

<p>If you want to contribute to the documentation project by writing new
documents or translating existing ones, please:</p>

<ul>
    <li>Contact the <a href="https://l10n.kde.org/docs/index-script.php">Editorial Team</a> if you are interested in writing new documentation.</li>
    <li>Contact one of the existing <a href="https://l10n.kde.org/teams-list.php">translation teams</a> if you want to translate documentation to an existing language.</li>
    <li>Take a look at the practical instructions at the <a href="https://l10n.kde.org/">internationalization and documentation server</a>.
    </li>
</ul>

<!-- END CONTENT -->
</main>
<?php
  require('../aether/footer.php');
